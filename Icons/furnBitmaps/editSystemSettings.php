<?php
include ("../../authconfig.php");
if (!class_exists ('auth'))
{
include ("../../auth.php");
}
include ("../../check.php");
require ("../include.php");
$conn=new Conn('');
$sysConn=new Conn('');
$conn2=new Conn('');


$getStr='sysID='.$_REQUEST['sysID'];
$getStr.='&siteID='.$_GET['siteID'];
if (isset($_GET['floorID'])) $getStr.='&floorID='.$_GET['floorID'];
if (isset($_GET['spaceID'])) $getStr.='&spaceID='.$_GET['spaceID'];

$site=new BIMSite($_GET['siteID']);
$siteInfo=new SiteInfo();
$siteInfo->load($site->getID());
$project=new BIMProject($site->getProjectID());
$bldgs=$site->getBIMBldgs();

// Check for user privileges
if (!( (($site->getShareEdit()==0) && ($check['id']==$project->getUserID())) || // If the site is NOT shared by teamwork and the user is the owner
	   (in_array($check['id'], $authorizedDebuggers)) || // If the user is an authorized debugger
	   (($site->getShareEdit()>0) && ($siteInfo->getBorrowID()==0) && ($project->getUserID()==$check['id'])) || // If the site is shared by teamwork and NOT borrowed by anyone and the user is the owner
	   (($site->getShareEdit()>0) && ($siteInfo->getBorrowID()==$check['id'])) )) { // If the site is shared by teamwork and is borrowed by the user
		print "<center><font face=\"Arial, Helvetica, sans-serif\" size=\"4\" color=\"#FF0000\">";
		print "<b>You don't have access to this page</b>";
		print "</font><br />";
		exit;	// Stop script execution
}
if (($site->getShareEdit()>0) && (!in_array($check['id'], $authorizedDebuggers))) {
	$sql="SELECT * FROM UserGroup_User LEFT JOIN
				EditGroup ON EditGroup.userGroupID=UserGroup_User.userGroupID
				where userID=".$check['id']."
				AND siteID=".$site->getID();
	$conn->execute($sql);
	$hasTeamworkPrivilege=($conn->next());
	if (!$hasTeamworkPrivilege) {
		$sql="select count(*) AS num from EditGroup where siteID=".$site->getID();
		$data=$conn->execRow($sql);
		$hasTeamworkPrivilege=$data['num']==0;
	}
	if (!$hasTeamworkPrivilege) {
		print "<center><font face=\"Arial, Helvetica, sans-serif\" size=\"4\" color=\"#FF0000\">";
		print "<b>You don't have access to this page</b>";
		print "</font><br />";
		exit;	// Stop script execution
	}
}
// End of Check for user privileges

if (isset($_GET['floorID'])) {
	$floor=new BIMFloor($_GET['floorID']);
	$bldg=new BIMBldg($floor->getBldgID());
	
	$bldgCostSetting=new BldgCostSetting();
	$bldgCostSetting->load($bldg->getID());
	
	$uscgBldgInfo=new USCG_BldgInfo();
	$uscgBldgInfo->load($bldg->getID());
}

// update DB
if (isset($_POST['submitBtn'])) {
	if (($_POST['action']=='addSystem') ||
		($_POST['action']=='editSystem')) {
		$system=new CobieSystem();
		if ($_POST['action']=='editSystem') {
			$system->load($_POST['systemID']);
		}
		$system->setFacilityID($bldg->getID());
		if ((isset($_POST['omniClass21_L5'])) && ($_POST['omniClass21_L5']!='')) {
			$system->setSystemFunctionNum($_POST['omniClass21_L5']);
			$sql="SELECT * FROM onuma_settings.OmniClass_21_L5 WHERE ID='".$_POST['omniClass21_L5']."'";
		} else if ((isset($_POST['omniClass21_L4'])) && ($_POST['omniClass21_L4']!='')) {
			$system->setSystemFunctionNum($_POST['omniClass21_L4']);
			$sql="SELECT * FROM onuma_settings.OmniClass_21_L4 WHERE ID='".$_POST['omniClass21_L4']."'";
		} else if ((isset($_POST['omniClass21_L3'])) && ($_POST['omniClass21_L3']!='')) {
			$system->setSystemFunctionNum($_POST['omniClass21_L3']);
			$sql="SELECT * FROM onuma_settings.OmniClass_21_L3 WHERE ID='".$_POST['omniClass21_L3']."'";
		} else if ((isset($_POST['omniClass21_L2'])) && ($_POST['omniClass21_L2']!='')) {
			$system->setSystemFunctionNum($_POST['omniClass21_L2']);
			$sql="SELECT * FROM onuma_settings.OmniClass_21_L2 WHERE ID='".$_POST['omniClass21_L2']."'";
		} else {
			$system->setSystemFunctionNum($_POST['omniClass21_L1']);
			$sql="SELECT * FROM onuma_settings.OmniClass_21_L1 WHERE ID='".$_POST['omniClass21_L1']."'";
		}
		$result=$conn->execRow($sql);
		$system->setSystemFunctionName($result['name']);
		$system->setSystemName($_POST['systemName']);
		//$system->setSystemDescription($_POST['systemDescription']);
		if ($_POST['action']=='addSystem') {
			$system->setCreatedBy($check['id']);
			$system->setCreatedDate(date('Y-m-d'));
			$system->setCreatedTime(date('H:i:s'));
			$system->setCreatedOn(time());
			$system->setWithdrawn("No");
			$system->insert();
		} else {
			$system->update();
		}
	}
	// Cascade update on modification date
	$bldg->setDateModified(time());
	$bldg->update();
	$site->setDateModified(time());
	$site->update();
	$project->setDateModified(time());
	$project->update();
	// End of Cascade update on modification date
	header("location: editSystemSettings.php?sysID=".$_REQUEST['sysID']."&siteID=".$site->getID()."&floorID=".$floor->getID());
	exit;
}


// update DB

$omniClass21Arr=array();
$sql="SELECT * FROM onuma_settings.OmniClass_21_L1";
$conn->execute($sql);
while ($conn->next()) {
	$omniClass21Arr[$conn->val('ID')]=$conn->val('name');
}
$sql="SELECT * FROM onuma_settings.OmniClass_21_L2";
$conn->execute($sql);
while ($conn->next()) {
	$omniClass21Arr[$conn->val('ID')]=$conn->val('name');
}
$sql="SELECT * FROM onuma_settings.OmniClass_21_L3";
$conn->execute($sql);
while ($conn->next()) {
	$omniClass21Arr[$conn->val('ID')]=$conn->val('name');
}
$sql="SELECT * FROM onuma_settings.OmniClass_21_L4";
$conn->execute($sql);
while ($conn->next()) {
	$omniClass21Arr[$conn->val('ID')]=$conn->val('name');
}
$sql="SELECT * FROM onuma_settings.OmniClass_21_L5";
$conn->execute($sql);
while ($conn->next()) {
	$omniClass21Arr[$conn->val('ID')]=$conn->val('name');
}



$omniClass23Arr=array();
$sql="SELECT * FROM onuma_settings.OmniClass_23_L1";
$conn->execute($sql);
while ($conn->next()) {
	$omniClass23Arr[$conn->val('ID')]=$conn->val('name');
}
$sql="SELECT * FROM onuma_settings.OmniClass_23_L2";
$conn->execute($sql);
while ($conn->next()) {
	$omniClass23Arr[$conn->val('ID')]=$conn->val('name');
}
$sql="SELECT * FROM onuma_settings.OmniClass_23_L3";
$conn->execute($sql);
while ($conn->next()) {
	$omniClass23Arr[$conn->val('ID')]=$conn->val('name');
}
$sql="SELECT * FROM onuma_settings.OmniClass_23_L4";
$conn->execute($sql);
while ($conn->next()) {
	$omniClass23Arr[$conn->val('ID')]=$conn->val('name');
}
$sql="SELECT * FROM onuma_settings.OmniClass_23_L5";
$conn->execute($sql);
while ($conn->next()) {
	$omniClass23Arr[$conn->val('ID')]=$conn->val('name');
}
$sql="SELECT * FROM onuma_settings.OmniClass_23_L6";
$conn->execute($sql);
while ($conn->next()) {
	$omniClass23Arr[$conn->val('ID')]=$conn->val('name');
}


$descArr=array();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="content-type" content="text/html;charset=ISO-8859-1" />
		<title>Building Systems</title>
		<link href="../style.css" rel="stylesheet" media="all" />
	</head>

	<body bgcolor="#d3d3d3" leftmargin="0" marginheight="0" marginwidth="0" topmargin="0">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td rowspan="2" width="10"><img src="../logo/bimsetting.jpg" alt="" width="323"  height="62" border="0"></td>
				<td bgcolor="#e7f391"><a title="Onuma, Inc." href="http://onuma.com" target="_blank"><img src="../logo/onumalogo_sm.gif" alt="Onuma Inc." width="47" height="47" border="0" /></a></td>
			</tr>
			<tr>
				<td bgcolor="#d3d3d3"><img src="../images/clearimage.gif" alt="" width="1" height="15" border="0"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr height="10">
				<td class="border" height="10"></td>
				<td class="border" align="center" width="600" height="10"><img src="../images/clearimage.gif" alt=" " height="10" width="600" border="0" /></td>
				<td class="border" height="10"></td>
			</tr>
			<tr height="16">
				<td class="border" height="16"></td>
				<td class="border" align="center" height="16">
					<table width="600" border="0" cellspacing="0" cellpadding="0">
						<tr height="1">
							<td align="center" width="120" height="1"><img src="../images/clearimage.gif" alt=" " height="1" width="120" border="0" /></td>
							<td align="center" width="120" height="1"><img src="../images/clearimage.gif" alt=" " height="1" width="120" border="0" /></td>
							<td align="center" width="120" height="1"><img src="../images/clearimage.gif" alt=" " height="1" width="120" border="0" /></td>
							<td align="center" width="120" height="1"><img src="../images/clearimage.gif" alt=" " height="1" width="120" border="0" /></td>
							<td align="center" width="120" height="1"><img src="../images/clearimage.gif" alt=" " height="1" width="120" border="0" /></td>
						</tr>
						<tr>
			<?php
				$sql="SELECT * FROM onuma_plan.ac_settings WHERE ID=".$_REQUEST['sysID'];;
				$sysConn->execute($sql);
				$sysConn->next();
				if($check['memberlevel'] >= $sysConn->val('admin_setting')){
			?>
							<td class="menua" align="center" width="120">
								<a href="accessFunctions.php?<?php echo $getStr; ?>">Admin Settings</a>
							</td>
			<?php } else {
				echo '<td class="menuaoff" align="center" width="120">';
				echo 'Admin Settings';
				echo '</td>';
			} ?>
							<td class="menua" align="center" width="120"><a href="generalSettings.php?<?php echo $getStr; ?>">General Settings</a></td>
							<td class="menua2" align="center" width="120"><a href="siteSettings.php?<?php echo $getStr; ?>">Site Settings</a></td>
							<td class="menua3on" align="center" width="120">&nbsp;&nbsp;Building Settings&nbsp;&nbsp;</td>
							<td class="menua4" align="center" width="120"><a href="spaceSettings.php?<?php echo $getStr; ?>">Space Settings</a></td>

						</tr>
					</table>
				</td>
				<td class="border" height="16"></td>
			</tr>
			<tr height="1">
				<td colspan="3" align="center" height="1"><img src="../images/clearimage.gif" alt=" " height="1" width="1" border="0" /></td>
			</tr>
			<tr height="16">
				<td class="menubbgrd" colspan="3" align="center" height="16">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="menub3" align="center" width="48%">&nbsp;</td>
							<td class="menub3" align="center" nowrap="nowrap"><a href="editBldgSettings.php?<?php echo $getStr; ?>">Buildings</a></td>
							<td class="menub3" align="center" nowrap="nowrap"><a href="editFloorSettingMatrix.php?<?php echo $getStr; ?>">Floors</a></td>
							<?php
								if($check['memberlevel'] >= $sysConn->val('bldg_uniformat')){
							?>
							<td class="menub3" align="center" nowrap="nowrap"><a href="editBldgSettingsUniformat.php?<?php echo $getStr; ?>">Uniformat</a></td>
							<?php
								}
							?>
							<?php
								if ($check['memberlevel'] >= $sysConn->val('bldg_leed')){
							?>
							<td class="menub3" align="center" nowrap="nowrap"><a href="editLeedSettings.php?<?php echo $getStr; ?>">LEED</a></td>
							<?php
								}
							?>
							<td class="menuon" align="center" nowrap="nowrap">&nbsp;&nbsp;Systems&nbsp;&nbsp;</td>
							<td class="menub3" align="center" nowrap="nowrap"><a href="editTypeSettings.php?<?php echo $getStr; ?>">Types</a></td>
							<?php
								if (($check['costEstSystem']=='flexibleCostEstimate') &&
									($check['memberlevel'] >= $sysConn->val('bldg_costest')) ){
							?>
							<td class="menub3" align="center" nowrap="nowrap"><a href="editBldgCostSettings.php?<?php echo $getStr; ?>">Cost Est</a></td>
							<?php
								}
							?>
							<td class="menub3" align="center" width="48%">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div align="center" id="loadingContent">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="border" align="center" valign="top"></td>
				<td align="center" valign="top" width="600">
					<table width="600" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top">
								<table width="600" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="level3"><img src="../images/clearimage.gif" alt="" height="1" width="10" border="0" /></td>
										<td class="level3"><img src="../images/clearimage.gif" alt="" height="10" width="1" border="0" /></td>
										<td class="level3"><img src="../images/clearimage.gif" alt="" height="1" width="10" border="0" /></td>
									</tr>
									<tr height="20">
										<td class="level3" height="20"><img src="../images/clearimage.gif" alt="" height="1" width="10" border="0" /></td>
										<td class="level3" align="center" height="20">
											<table width="580" border="0" cellspacing="0" cellpadding="4">
												<tr>
													<td bgcolor="white">
														<table width="576" border="0" cellspacing="0" cellpadding="2">
															<tr>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td class="bold" align="center"><br />
																		<br />
																		<img src="../images/thinking.gif" alt="Evaluating..." width="350" height="146" border="0">
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
										<td class="level3" height="20"><img src="../images/clearimage.gif" alt="" height="1" width="10" border="0" /></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td class="border" align="center" valign="top"></td>
			</tr>
			<tr height="10">
				<td class="border" height="10"><img src="../images/clearimage.gif" alt=" " height="10" width="1" border="0" /></td>
				<td class="level3" align="center" width="600" height="10"><img src="../images/clearimage.gif" alt=" " height="10" width="1" border="0" /></td>
				<td class="border" height="10"><img src="../images/clearimage.gif" alt=" " height="10" width="1" border="0" /></td>
			</tr>
		</table>
		</div>
		<div align="center" id="mainContent" style="display:<?php echo 'none' ?>;">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<script type="text/javascript" language="JavaScript" src="../communicatorS.js"></script>
			<script type="text/javascript" language="JavaScript" src="../jsLib.js"></script>
			<?php if (isset($_GET['floorID'])) { ?>
			<script type="text/javascript" language="JavaScript">
				<?php if (($_GET['action']=='addSystem')  || (isset($_GET['systemID']))) { ?>
				function listOmniClass21L2 () {
					if (document.forms['inputForm'].omniClass21_L1.value!='') {
						// Reset the L2 list
						document.forms['inputForm'].omniClass21_L2.options.length=0;
						document.forms['inputForm'].omniClass21_L2.options[0]=new Option('Omni Class 21 Level 2: select...', '', true, true);
						document.forms['inputForm'].omniClass21_L2.disabled=false;
						<?php
						$sql="SELECT * FROM onuma_settings.OmniClass_21_L1";
						$conn->execute($sql);
						while ($conn->next()) {
							?>if (document.forms['inputForm'].omniClass21_L1.value=='<?php echo $conn->val('ID') ?>') {
								<?php
								$sql="SELECT * FROM onuma_settings.OmniClass_21_L2 WHERE ID1=".$conn->val('ID1');
								$conn2->execute($sql);
								$ind=0;
								while ($conn2->next()) {
									$ind++;
									?>
										document.forms['inputForm'].omniClass21_L2.options[<?php echo $ind ?>]=new Option("<?php
											echo $conn2->val('ID')." ".$conn2->val('name'); ?>", "<?php echo $conn2->val('ID') ?>");
									<?php
								}
							?>}<?php
						}
						?>
					} else {
						// Reset the L2 list
						document.forms['inputForm'].omniClass21_L2.options.length=0;
						document.forms['inputForm'].omniClass21_L2.options[0]=new Option('Omni Class 21 Level 2: not specified...', '', true, true);
						document.forms['inputForm'].omniClass21_L2.disabled=true;
						
					}
					// Reset the L3 list
					document.forms['inputForm'].omniClass21_L3.options.length=0;
					document.forms['inputForm'].omniClass21_L3.options[0]=new Option('Omni Class 21 Level 3: not specified...', '', true, true);
					document.forms['inputForm'].omniClass21_L3.disabled=true;
					
					// Reset the L4 list
					document.forms['inputForm'].omniClass21_L4.options.length=0;
					document.forms['inputForm'].omniClass21_L4.options[0]=new Option('Omni Class 21 Level 4: not specified...', '', true, true);
					document.forms['inputForm'].omniClass21_L4.disabled=true;
					
					// Reset the L5 list
					document.forms['inputForm'].omniClass21_L5.options.length=0;
					document.forms['inputForm'].omniClass21_L5.options[0]=new Option('Omni Class 21 Level 5: not specified...', '', true, true);
					document.forms['inputForm'].omniClass21_L5.disabled=true;
				}
			
				function listOmniClass21L3 () {
					if (document.forms['inputForm'].omniClass21_L2.value!='') {
						// Reset the L3 list
						document.forms['inputForm'].omniClass21_L3.options.length=0;
						document.forms['inputForm'].omniClass21_L3.options[0]=new Option('Omni Class 21 Level 3: not specified...', '', true, true);
						document.forms['inputForm'].omniClass21_L3.disabled=false;
						<?php
						$sql="SELECT * FROM onuma_settings.OmniClass_21_L2";
						$conn->execute($sql);
						while ($conn->next()) {
							?>if (document.forms['inputForm'].omniClass21_L2.value=='<?php echo $conn->val('ID') ?>') {
								<?php
								$sql="SELECT * FROM onuma_settings.OmniClass_21_L3 WHERE ID1=".$conn->val('ID1')." AND ID2=".$conn->val('ID2');
								$conn2->execute($sql);
								$ind=0;
								while ($conn2->next()) {
									$ind++;
									?>
										document.forms['inputForm'].omniClass21_L3.options[<?php echo $ind ?>]=new Option("<?php
											echo $conn2->val('ID')." ".$conn2->val('name'); ?>", "<?php echo $conn2->val('ID') ?>");
									<?php
								}
							?>}<?php
						}
						?>
					} else {
						// Reset the L3 list
						document.forms['inputForm'].omniClass21_L3.options.length=0;
						document.forms['inputForm'].omniClass21_L3.options[0]=new Option('Omni Class 21 Level 3: not specified...', '', true, true);
						document.forms['inputForm'].omniClass21_L3.disabled=true;
						
					}
					// Reset the L4 list
					document.forms['inputForm'].omniClass21_L4.options.length=0;
					document.forms['inputForm'].omniClass21_L4.options[0]=new Option('Omni Class 21 Level 4: not specified...', '', true, true);
					document.forms['inputForm'].omniClass21_L4.disabled=true;
					
					// Reset the L5 list
					document.forms['inputForm'].omniClass21_L5.options.length=0;
					document.forms['inputForm'].omniClass21_L5.options[0]=new Option('Omni Class 21 Level 5: not specified...', '', true, true);
					document.forms['inputForm'].omniClass21_L5.disabled=true;
				}
			
				function listOmniClass21L4 () {
					if (document.forms['inputForm'].omniClass21_L3.value!='') {
						// Reset the L4 list
						document.forms['inputForm'].omniClass21_L4.options.length=0;
						document.forms['inputForm'].omniClass21_L4.options[0]=new Option('Omni Class 21 Level 4: not specified...', '', true, true);
						document.forms['inputForm'].omniClass21_L4.disabled=false;
						<?php
						$sql="SELECT * FROM onuma_settings.OmniClass_21_L3";
						$conn->execute($sql);
						while ($conn->next()) {
							?>if (document.forms['inputForm'].omniClass21_L3.value=='<?php echo $conn->val('ID') ?>') {
								<?php
								$sql="SELECT * FROM onuma_settings.OmniClass_21_L4 WHERE ID1=".$conn->val('ID1')." AND ID2=".$conn->val('ID2')." AND ID3=".$conn->val('ID3');
								$conn2->execute($sql);
								$ind=0;
								while ($conn2->next()) {
									$ind++;
									?>
										document.forms['inputForm'].omniClass21_L4.options[<?php echo $ind ?>]=new Option("<?php
											echo $conn2->val('ID')." ".$conn2->val('name'); ?>", "<?php echo $conn2->val('ID') ?>");
									<?php
								}
							?>}<?php
						}
						?>
					} else {
						// Reset the L4 list
						document.forms['inputForm'].omniClass21_L4.options.length=0;
						document.forms['inputForm'].omniClass21_L4.options[0]=new Option('Omni Class 21 Level 4: not specified...', '', true, true);
						document.forms['inputForm'].omniClass21_L4.disabled=true;
					}
					// Reset the L5 list
					document.forms['inputForm'].omniClass21_L5.options.length=0;
					document.forms['inputForm'].omniClass21_L5.options[0]=new Option('Omni Class 21 Level 5: not specified...', '', true, true);
					document.forms['inputForm'].omniClass21_L5.disabled=true;
				}
				
				function listOmniClass21L5 () {
					if (document.forms['inputForm'].omniClass21_L4.value!='') {
						// Reset the L4 list
						document.forms['inputForm'].omniClass21_L5.options.length=0;
						document.forms['inputForm'].omniClass21_L5.options[0]=new Option('Omni Class 21 Level 5: not specified...', '', true, true);
						document.forms['inputForm'].omniClass21_L5.disabled=false;
						<?php
						$sql="SELECT * FROM onuma_settings.OmniClass_21_L4";
						$conn->execute($sql);
						while ($conn->next()) {
							?>if (document.forms['inputForm'].omniClass21_L4.value=='<?php echo $conn->val('ID') ?>') {
								<?php
								$sql="SELECT * FROM onuma_settings.OmniClass_21_L5 WHERE ID1=".$conn->val('ID1')." AND ID2=".$conn->val('ID2')." AND ID3=".$conn->val('ID3')." AND ID4=".$conn->val('ID4');
								$conn2->execute($sql);
								$ind=0;
								while ($conn2->next()) {
									$ind++;
									?>
										document.forms['inputForm'].omniClass21_L5.options[<?php echo $ind ?>]=new Option("<?php
											echo $conn2->val('ID')." ".$conn2->val('name'); ?>", "<?php echo $conn2->val('ID') ?>");
									<?php
								}
							?>}<?php
						}
						?>
					} else {
						// Reset the L5 list
						document.forms['inputForm'].omniClass21_L5.options.length=0;
						document.forms['inputForm'].omniClass21_L5.options[0]=new Option('Omni Class 21 Level 5: not specified...', '', true, true);
						document.forms['inputForm'].omniClass21_L5.disabled=true;
					}
				}
				
				function checkSystemSubmit() {
					  if (document.forms['inputForm'].omniClass21_L1.value=="") {
							window.alert('Please select the system function');
							document.forms['inputForm'].omniClass21_L1.focus();
							return false;
					  }
					  if (trim(document.forms['inputForm'].systemName.value)=="") {
							window.alert('Please provide the system name');
							document.forms['inputForm'].systemName.focus();
							return false;
					  } else {
						  <?php
							$sql="SELECT *
									FROM CobieSystem
									WHERE facilityID=".$bldg->getID().((isset($_GET['systemID']))?' AND ID!='.$_GET['systemID']:'');
							$conn->execute($sql);
							$numSystem=0;
							$systemNameArr=array(0=>"(None)");
							while ($conn->next()) {
								$numSystem++;
								$system=new CobieSystem();
								$system->populate($conn->row);
								$systemNameArr[$system->getID()]=$system->getSystemName();
						  ?>
						  if (trim(document.forms['inputForm'].systemName.value)=="<?php echo $system->getSystemName(); ?>") {
								window.alert('System name can not be duplicated');
								document.forms['inputForm'].systemName.focus();
								return false;
						  }
						  <?php } ?>
					  }
					  return true;
				}
				<?php } ?>
			

			</script>
			<?php } ?>
			<tr height="1">
				<td colspan="3" align="center" height="1"><img src="../images/clearimage.gif" alt=" " height="1" width="1" border="0" /></td>
			</tr>
			<tr>
				<td class="border" align="center" valign="top"></td>
				<td valign="top" width="600" class="level3">
					<table width="600" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="level3"><img src="../images/clearimage.gif" alt="" height="1" width="10" border="0" /></td>
							<td class="level3"><img src="../images/clearimage.gif" alt="" height="10" width="1" border="0" /></td>
							<td class="level3"><img src="../images/clearimage.gif" alt="" height="1" width="10" border="0" /></td>
						</tr>
						<tr height="20">
							<td class="level3" height="20"><img src="../images/clearimage.gif" alt="" height="1" width="10" border="0" /></td>
							<td class="level3">
								<table width="580" border="0" cellspacing="0" cellpadding="4">
									<form name="inputForm" method="post" onsubmit="
										var obj;
										obj=MM_findObj('mainContent');
										obj.style.display='none';
										obj=MM_findObj('loadingContent');
										obj.style.display='inline';
									">
										<input type="hidden" name="loc" value="">
										<input type="hidden" name="action" value="">
										<input type="hidden" name="systemID" value="">
										<input type="submit" name="submitBtn" style="display:none; ">
									<tr>
										<td bgcolor="white">
											<table width="576" border="0" cellspacing="0" cellpadding="2">
												<tr>
													<td class="mainlisthead" colspan="<?php if (isset($_GET['floorID'])) { echo 2; } else { echo 5; } ?>">
															Building Systems
													</td>
													<?php if (isset($_GET['floorID'])) { ?>
													<td style="background-color:#808080; color:#ffffff; " align="right" colspan="2">
														<table width="120" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="menua3" style="color:#ffffff; " align="center" width="120"><a href="editSystemSettings.php?sysID=<?php echo $_REQUEST['sysID']; ?>&siteID=<?php echo $site->getID() ?>">All Buildings</a></td>
															</tr>
														</table>
													</td>
													<?php } ?>
												</tr>
								<?php if (!isset($_GET['floorID'])) { ?>
                                    <tr>
                                        <td colspan="5"><img src="../images/clearimage.gif" alt="" width="1" height="5" border="0"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" width="200">
                                            <table width="200" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td><img src="../images/clearimage.gif" alt="" width="20" height="1" border="0"/></td>
                                                    <td class="level2" align="center" width="160"><?php echo $site->getName() ?></td>
                                                    <td><img src="../images/clearimage.gif" alt="" width="20" height="8" border="0"/></td>
                                                </tr>
                                            </table>
                                        </td>
                                        
                                        <td valign="top" width="200">
                                            <table width="200" border="0" cellspacing="0" cellpadding="0">
                                              <?php
                                              if (count($bldgs)==0) {
                                              ?>
                                                        <tr>
                                                            <td valign="top" width="200" colspan="3" align="center">(No buildings to display)</td>
                                                        </tr>
                                              <?php 
                                                } else {
                                                    $ind=0;
                                                    foreach ($bldgs as $bldg) { 
                                                        $ind++;
                                                        $bldgInfo=new BldgInfo();
                                                        $bldgInfo->load($bldg->getID());
                                                        $sql=$sqlQueries["Floor"]." WHERE Floor.bldgID=".$bldg->getID()." ORDER BY Placement.z LIMIT ".$bldgInfo->getFirstFloor().",1";
                                                        $data=$conn->execRow($sql);
                                                        $bldgFirstFloor=new BIMFloor();
                                                        $bldgFirstFloor->set($data);
                                                        ?>
                                                        <tr>
                                                            <td><img src="../images/clearimage.gif" alt="" width="20" height="1" border="0"/></td>
                                                            <td valign="top" width="160" class="menua3" align="center">
                                                                <a href="editSystemSettings.php?sysID=<?php echo $_REQUEST['sysID']; ?>&siteID=<?php echo $site->getID() ?>&amp;floorID=<?php echo $bldgFirstFloor->getID() ?>"><?php echo $bldg->getName(); ?></a></td>
                                                            <td><img src="../images/clearimage.gif" alt="" width="20" height="8" border="0"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3"><img src="../images/clearimage.gif" alt="" width="20" height="8" border="0"/></td>
                                                        </tr>
                                                        <?php 	
                                                    }
                                                }
                                                ?>
                                            </table>
                                        </td>
                                        <td valign="top">&nbsp;</td>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="4" class="level3"><?php echo $bldg->getName() ?></td>
                                    </tr>
                                    <tr>
                                        <td class="listhead" width="160">System Name</td>
                                        <td class="listhead" width="120">Category Number</td>
                                        <td class="listhead" width="230">Category Name</td>
                                        <td class="listhead" width="15">&nbsp;</td>
                                    </tr>
						<?php
						$sql="SELECT *
								FROM CobieSystem
								WHERE facilityID=".$bldg->getID()." ORDER BY ID";
						$conn->execute($sql);
						$numSystem=0;
						$systemNameArr=array(0=>"(None)");
						while ($conn->next()) {
							$numSystem++;
							$system=new CobieSystem();
							$system->populate($conn->row);
							$systemNameArr[$system->getID()]=$system->getSystemName();
							?>
                                <tr>
                                	<td colspan="" valign="top"><?php echo $system->getSystemName();
											if ($system->getSystemDescription()!='') {
												array_push($descArr, "desc".$system->getID(), "desc2".$system->getID());
												echo '<span id="desc2'.$system->getID().'" style="display:none;">:</span>
													<div id="desc'.$system->getID().'" style="display:none;">
														<div class="leftblockindent">'.$system->getSystemDescription().'
														</div>
													</div>';
											 }
										?></td>
                                	<td valign="top"><?php
										$tokens=explode(" ", $system->getSystemFunctionNum());
										echo $tokens[0].' ';
										echo $tokens[1].' ';
										echo $tokens[2].' ';
										echo $tokens[3].' ';
										if ($tokens[4]!=0) { echo $tokens[4].' '; }

										 ?></td>
                                	<td valign="top"><?php echo $omniClass21Arr[$system->getSystemFunctionNum()]; ?></td>
                                	<td valign="top"><a href="editSystemSettings.php?sysID=<?php echo $_REQUEST['sysID']; ?>&siteID=<?php echo $site->getID() ?>&amp;floorID=<?php echo $floor->getID() ?>&amp;systemID=<?php echo $system->getID(); ?>#edit">
                                    <img src="../images/editbut.gif" alt="Edit" width="14" height="14" border="0"></a></td>
                                </tr>
                            <?php
						} 
						
						if ($numSystem==0){
							?>
							<tr>
								<td colspan="4" align="center">(No Systems defined for this Building)</td>
							</tr>
							<?php
						}
						?>
                                                <?php if ($_GET['action']=='addSystem') { ?>
                                                <tr>
                                                    <td colspan="4">&nbsp;</td>
                                                </tr>
												<tr>
													<td colspan="4" class="menuon" id="add">Add a new system:</td>
												</tr>
												<tr>
													<td colspan="4">
														<table border="0" cellspacing="0" cellpadding="2">
															<tr>
																<td class="bold" valign="top">
                                                                    System Name:<br />
                                                                    <input type="text" name="systemName" style="width:180px;" />
                                                                    <!--<br /><br />
                                                                    System Description:<br />
																	<textarea class="inputTxt" name="systemDescription" style="width:180px; height:80px;" wrap="virtual"></textarea>-->
                                                                </td>
																<td class="bold" valign="top">
                                                                	System Function:<br />
                                                                    <select name="omniClass21_L1" class="input" style="width:300px;" onChange="listOmniClass21L2(); window.focus();">
                                                                        <option value="">Omni Class 21 Level 1: select...</option>
                                                                        <?php
                                                                        $sql="SELECT * FROM onuma_settings.OmniClass_21_L1";
                                                                        $conn->execute($sql);
                                                                        while ($conn->next()) { ?>
                                                                        <option value="<?php echo $conn->val('ID') ?>"><?php
                                                                            echo $conn->val('ID')." - ".$conn->val('name');
                                                                        ?></option>
                                                                        <?php } ?>
                                                                    </select><br />
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <select name="omniClass21_L2" class="input" style="width:300px;"  onChange="listOmniClass21L3(); window.focus();" disabled="disabled">
                                                                        <option value="">Omni Class 21 Level 2: not specified...</option>
                                                                    </select><br />
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <select name="omniClass21_L3" class="input" style="width:300px;"  onChange="listOmniClass21L4(); window.focus();" disabled="disabled">
                                                                        <option value="">Omni Class 21 Level 3: not specified...</option>
                                                                    </select><br />
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <select name="omniClass21_L4" class="input" style="width:300px;"   onChange="listOmniClass21L5();  window.focus();" disabled="disabled">
                                                                        <option value="">Omni Class 21 Level 4: not specified...</option>
                                                                    </select><br />
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <select name="omniClass21_L5" class="input" style="width:300px;"   onChange=" window.focus();" disabled="disabled">
                                                                        <option value="">Omni Class 21 Level 5: not specified...</option>
                                                                    </select>
                                                                </td>
															</tr>
														</table>
													</td>
												</tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                        <table width="260" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td valign="top" width="110" class="menua3" align="center">
                                                                    <a href="editSystemSettings.php?sysID=<?php echo $_REQUEST['sysID']; ?>&siteID=<?php echo $site->getID() ?>&amp;floorID=<?php echo $floor->getID() ?>">Cancel</a></td>
                                                                <td><img src="../images/clearimage.gif" alt="" width="40" height="1" border="0"/></td>
                                                                <td valign="top" width="110" class="menua3" align="center">
                                                                    <a href="javascript:
                                                                        if (checkSystemSubmit()) {
                                                                            document.forms['inputForm'].action.value='addSystem';
                                                                            document.forms['inputForm'].submitBtn.click();
                                                                        };
                                                                    ">Add</a></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php } else if (isset($_GET['systemID'])) {
													$system=new CobieSystem();
													$system->load($_GET['systemID']);
													
													$tokens=explode(" ", $system->getSystemFunctionNum());
													$ID1=$tokens[0];
													$ID2=$tokens[1];
													$ID3=$tokens[2];
													$ID4=$tokens[3];
													$ID5=$tokens[4];
												?>
                                                <tr>
                                                    <td colspan="4">&nbsp;</td>
                                                </tr>
												<tr>
													<td colspan="4" class="menuon" id="edit">Edit existing system:</td>
												</tr>
												<tr>
													<td colspan="4">
														<table border="0" cellspacing="0" cellpadding="2">
															<tr>
																<td class="bold" valign="top">
                                                                    System Name:<br />
                                                                    <input type="text" name="systemName" value="<?php echo $system->getSystemName(); ?>" style="width:180px;" />
                                                                    <!--<br /><br />
                                                                    System Description:<br />
																	<textarea class="inputTxt" name="systemDescription" style="width:180px; height:80px;" wrap="virtual"><?php
																		echo $system->getSystemDescription(); ?></textarea>-->
                                                                </td>
																<td class="bold" valign="top">
                                                                	System Function:<br />
                                                                    <select name="omniClass21_L1" class="input" style="width:300px;" onChange="listOmniClass21L2(); window.focus();">
                                                                        <option value="">Omni Class 21 Level 1: select...</option>
                                                                        <?php
                                                                        $sql="SELECT * FROM onuma_settings.OmniClass_21_L1";
                                                                        $conn->execute($sql);
                                                                        while ($conn->next()) { ?>
                                                                        <option value="<?php echo $conn->val('ID') ?>"<?php
                                                                        	if ($conn->val('ID1')==$ID1) echo ' selected="selected"';
																		?>><?php
                                                                            echo $conn->val('ID')." - ".$conn->val('name');
                                                                        ?></option>
                                                                        <?php } ?>
                                                                    </select><br />
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <select name="omniClass21_L2" class="input" style="width:300px;"  onChange="listOmniClass21L3(); window.focus();"<?php
                                                                    	if ($ID1==0) echo ' disabled="disabled"'; ?>>
                                                                        <option value="">Omni Class 21 Level 2: not specified...</option>
                                                                        <?php
																		if ($ID1!=0) {
																			$sql="SELECT * FROM onuma_settings.OmniClass_21_L2 WHERE ID1=".$ID1;
																			$conn->execute($sql);
																			while ($conn->next()) { ?>
																			<option value="<?php echo $conn->val('ID') ?>"<?php
																				if ($conn->val('ID2')==$ID2) echo ' selected="selected"';
																			?>><?php
																				echo $conn->val('ID')." - ".$conn->val('name');
																			?></option>
																			<?php }
																		}
																		?>
                                                                    </select><br />
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <select name="omniClass21_L3" class="input" style="width:300px;"  onChange="listOmniClass21L4(); window.focus();"<?php
                                                                    	if ($ID2==0) echo ' disabled="disabled"'; ?>>
                                                                        <option value="">Omni Class 21 Level 3: not specified...</option>
                                                                        <?php
																		if ($ID2!=0) {
																			$sql="SELECT * FROM onuma_settings.OmniClass_21_L3 WHERE ID1=".$ID1." AND ID2=".$ID2;
																			$conn->execute($sql);
																			while ($conn->next()) { ?>
																			<option value="<?php echo $conn->val('ID') ?>"<?php
																				if ($conn->val('ID3')==$ID3) echo ' selected="selected"';
																			?>><?php
																				echo $conn->val('ID')." - ".$conn->val('name');
																			?></option>
																			<?php }
																		}
																		?>
                                                                    </select><br />
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <select name="omniClass21_L4" class="input" style="width:300px;"   onChange="listOmniClass21L5(); window.focus();"<?php
                                                                    	if ($ID3==0) echo ' disabled="disabled"'; ?>>
                                                                        <option value="">Omni Class 21 Level 4: not specified...</option>
                                                                        <?php
																		if ($ID2!=0) {
																			$sql="SELECT * FROM onuma_settings.OmniClass_21_L4 WHERE ID1=".$ID1." AND ID2=".$ID2." AND ID3=".$ID3;
																			$conn->execute($sql);
																			while ($conn->next()) { ?>
																			<option value="<?php echo $conn->val('ID') ?>"<?php
																				if ($conn->val('ID4')==$ID4) echo ' selected="selected"';
																			?>><?php
																				echo $conn->val('ID')." - ".$conn->val('name');
																			?></option>
																			<?php }
																		}
																		?>
                                                                    </select><br />
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <select name="omniClass21_L5" class="input" style="width:300px;"   onChange=" window.focus();"<?php
                                                                    	if ($ID3==0) echo ' disabled="disabled"'; ?>>
                                                                        <option value="">Omni Class 21 Level 5: not specified...</option>
                                                                        <?php
																		if ($ID2!=0) {
																			$sql="SELECT * FROM onuma_settings.OmniClass_21_L5 WHERE ID1=".$ID1." AND ID2=".$ID2." AND ID3=".$ID3." AND ID4=".$ID4;
																			$conn->execute($sql);
																			while ($conn->next()) { ?>
																			<option value="<?php echo $conn->val('ID') ?>"<?php
																				if ($conn->val('ID5')==$ID5) echo ' selected="selected"';
																			?>><?php
																				echo $conn->val('ID')." - ".$conn->val('name');
																			?></option>
																			<?php }
																		}
																		?>
                                                                    </select>
                                                                </td>
															</tr>
														</table>
													</td>
												</tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                        <table width="260" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td valign="top" width="110" class="menua3" align="center">
                                                                    <a href="editSystemSettings.php?sysID=<?php echo $_REQUEST['sysID']; ?>&siteID=<?php echo $site->getID() ?>&amp;floorID=<?php echo $floor->getID() ?>">Cancel</a></td>
                                                                <td><img src="../images/clearimage.gif" alt="" width="40" height="1" border="0"/></td>
                                                                <td valign="top" width="110" class="menua3" align="center">
                                                                    <a href="javascript:
                                                                        if (checkSystemSubmit()) {
                                                                            document.forms['inputForm'].action.value='editSystem';
                                                                            document.forms['inputForm'].submitBtn.click();
                                                                        };
                                                                    ">Update</a></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <input type="hidden" name="systemID" value="<?php echo $_GET['systemID']; ?>" />
                                                <?php } ?>
												<?php  if ((!isset($_GET['action'])) && (!isset($_GET['systemID'])) && (!isset($_GET['typeID']))) { 
                                                ?>
                                                <tr>
                                                    <td colspan="4">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                        <table width="110" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td valign="top" width="110" class="menua3" align="center">
                                                                    <a href="editSystemSettings.php?sysID=<?php echo $_REQUEST['sysID']; ?>&siteID=<?php echo $site->getID() ?>&amp;floorID=<?php echo $floor->getID() ?>&amp;action=addSystem#add">Add System</a></td>
                                                            <?php echo '</tr>'; ?>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php
                                                    }
                                                ?>
                                <?php } ?>
											</table>
										</td>
									</tr>
									<tr>
										<td bgcolor="white">&nbsp;</td>
									</tr>
									</form>
									
								</table>
							</td>
							<td class="level3" height="20"><img src="../images/clearimage.gif" alt="" height="1" width="10" border="0" /></td>
						</tr>
						<tr>
							<td class="level3"><img src="../images/clearimage.gif" alt="" height="1" width="10" border="0" /></td>
							<td class="level3"><img src="../images/clearimage.gif" alt="" height="10" width="1" border="0" /></td>
							<td class="level3"><img src="../images/clearimage.gif" alt="" height="1" width="10" border="0" /></td>
						</tr>
					</table>
				</td>
				<td class="border" align="center" valign="top"></td>
			</tr>
			<tr height="10">
				<td class="border" height="10"><img src="../images/clearimage.gif" alt=" " height="10" width="1" border="0" /></td>
				<td class="level3" align="center" width="600" height="10"><img src="../images/clearimage.gif" alt=" " height="10" width="1" border="0" /></td>
				<td class="border" height="10"><img src="../images/clearimage.gif" alt=" " height="10" width="1" border="0" /></td>
			</tr>
		</table>
        </div>
        <?php printCopyright(); ?>
	</body>

</html>
<script type="text/javascript" language="JavaScript">
	var obj;
	obj=MM_findObj('loadingContent');
	obj.style.display='none';
	obj=MM_findObj('mainContent');
	obj.style.display='inline';
</script>

//
//  TestUI3.m
//  ProjectView
//
//  Created by Alfred Man on 11/14/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "TestUI3.h"
#import "OPSModel.h"

#import "ProjectViewAppDelegate.h"
@implementation TestUI3
/*
 - (void)drawRect:(CGRect)rect{


//     CGMutablePathRef path = CGPathCreateMutable();  
//     CGPathMoveToPoint(path,NULL,300,300);  
//     
//     CGPathAddLineToPoint(path,NULL,700,300);
//     
//     CGPathAddLineToPoint(path,NULL,500,700);
//     //    CGPathAddLineToPoint(path,NULL,0,400);
//     
//     CGContextRef context0 = UIGraphicsGetCurrentContext();     
//     CGContextBeginPath(context0);
//     CGContextAddPath(context0,path);
//     
//     CGContextSetRGBStrokeColor(context0, 1.0, 1.0, 1.0, 1.0);
//     CGContextSetRGBFillColor(context0, 1.0, 0.0, 0.0, 0.6);    
//     
//     CGContextSetLineWidth(context0, 5.0);
//     
//     CGContextClosePath(context0);     
//     CGContextFillPath(context0);

     
     
     ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate]; 
     double frameWith=1024;
     double frameHeight=567;
     double fitZoom=[[appDele model] fitZoom];
     double minZoom=[[appDele model] minimumZoom];          
     
     
     
//     CGFloat canvasWidth0=self.frame.size.width;    
//     CGFloat canvasHeight0=self.frame.size.height;
     
     
     CGFloat canvasWidth=frameWith/minZoom;//self.frame.size.width;    
     CGFloat canvasHeight=frameHeight/minZoom;//self.frame.size.height;
     
     CGFloat screenFrameWidth=canvasWidth*minZoom;
     CGFloat screenFrameHeight=canvasHeight*minZoom;
     
     CGFloat screenFitFrameWidth=screenFrameWidth/fitZoom;
     CGFloat screenFitFrameHeight=screenFrameHeight/fitZoom;
     

//     CGFloat canvasWidth=frameWidth*    [[appDele model] canvasScaleForScreenFactor];
//     CGFloat canvasHeight=frameHeight*  [[appDele model] canvasScaleForScreenFactor];
//     CGRect canvasRect=CGRectMake(0,0,canvasWidth,canvasHeight );     
//     NSLog(@"DrawCanvasWid: %f DrawCanvasHit: %f", canvasWidth, canvasHeight);
     CGFloat modelWidth=screenFitFrameWidth;
     CGFloat modelHeight=screenFitFrameHeight;
     
     
     CGMutablePathRef path = CGPathCreateMutable();  
     
     double offsetX=0+canvasWidth/2-modelWidth/2;
     double offsetY=0+canvasHeight/2-modelHeight/2;     
     //     double offsetX=-canvasWidth/2+canvasWidth/2-modelWidth/2;
     //     double offsetY=-canvasHeight/2+canvasHeight/2-modelHeight/2;
     double wid=modelWidth;
     double hit=modelHeight;
     
     
     
     CGPathMoveToPoint(path,NULL,offsetX+0,offsetY+0);  
     
     CGPathAddLineToPoint(path,NULL,offsetX+wid,offsetY+0);
     
     CGPathAddLineToPoint(path,NULL,offsetX+wid/2,offsetY+hit);
     //    CGPathAddLineToPoint(path,NULL,0,400);
     
     
     
     
     NSLog(@"DrawOffsetX: %f DrawOffsetY: %f DrawCanvasWid: %f DrawCanvasHit: %f modelWidth: %f modelHit: %f",offsetX, offsetY,canvasWidth, canvasHeight, wid, hit);
     
     
     CGContextRef context0 = UIGraphicsGetCurrentContext();     
     CGContextBeginPath(context0);
     CGContextAddPath(context0,path);
     
     CGContextSetRGBStrokeColor(context0, 1.0, 1.0, 1.0, 1.0);
     CGContextSetRGBFillColor(context0, 1.0, 0.0, 0.0, 0.6);    
     
     CGContextSetLineWidth(context0, 5.0);
     
     CGContextClosePath(context0);     
     CGContextFillPath(context0);
     
// self.backgroundColor=[UIColor clearColor];
 */
/*
 
 
 NSLog(@"DrawCanvasWid: %f DrawCanvasHit: %f", canvasWidth, canvasHeight);
 CGFloat modelWidth=canvasWidth/[[appDele model] canvasScaleForScreenFactor];
 CGFloat modelHeight=canvasHeight/[[appDele model] canvasScaleForScreenFactor];
 
 CGMutablePathRef path = CGPathCreateMutable();  
     
     double offsetX=0+canvasWidth/2-modelWidth/2;
     double offsetY=0+canvasHeight/2-modelHeight/2;     
//     double offsetX=-canvasWidth/2+canvasWidth/2-modelWidth/2;
//     double offsetY=-canvasHeight/2+canvasHeight/2-modelHeight/2;
     double wid=modelWidth;
     double hit=modelHeight;
 
 CGPathMoveToPoint(path,NULL,offsetX+0,offsetY+0);  
 
 CGPathAddLineToPoint(path,NULL,offsetX+wid,offsetY+0);
 
 CGPathAddLineToPoint(path,NULL,offsetX+wid/2,offsetY+hit);
 //    CGPathAddLineToPoint(path,NULL,0,400);
 
 CGContextRef context0 = UIGraphicsGetCurrentContext();     
 CGContextBeginPath(context0);
 CGContextAddPath(context0,path);
 
 CGContextSetRGBStrokeColor(context0, 1.0, 1.0, 1.0, 1.0);
 CGContextSetRGBFillColor(context0, 1.0, 0.0, 0.0, 0.6);    
 
 CGContextSetLineWidth(context0, 5.0);
     
 CGContextClosePath(context0);     
 CGContextFillPath(context0);
      
    path = CGPathCreateMutable();  
//     double offsetX2=-canvasWidth/2;
//     double offsetY2=-canvasHeight/2;
     
     double offsetX2=0;
     double offsetY2=0;     
     double wid2=modelWidth;
     double hit2=modelHeight;
     
     CGPathMoveToPoint(path,NULL,offsetX2+wid2/2,offsetY2+0);  
     
     CGPathAddLineToPoint(path,NULL,offsetX2+wid2,offsetY2+hit2);
     
     CGPathAddLineToPoint(path,NULL,offsetX2,offsetY2+hit2);
     //    CGPathAddLineToPoint(path,NULL,0,400);
     
    context0 = UIGraphicsGetCurrentContext();     
     CGContextBeginPath(context0);
     CGContextAddPath(context0,path);
     
     CGContextSetRGBStrokeColor(context0, 1.0, 1.0, 1.0, 1.0);
     CGContextSetRGBFillColor(context0, 1.0, 0.0, 0.0, 0.6);    
     
     CGContextSetLineWidth(context0, 5.0);
     
     CGContextClosePath(context0);     
     CGContextFillPath(context0);
     
     
     
     
     
     
     
     path = CGPathCreateMutable();  
     //     double offsetX2=-canvasWidth/2;
     //     double offsetY2=-canvasHeight/2;
     
     double offsetX3=canvasWidth-modelWidth;
     double offsetY3=canvasHeight-modelHeight;     
     double wid3=modelWidth;
     double hit3=modelHeight;
     
     CGPathMoveToPoint(path,NULL,offsetX3,offsetY3);  
     
     CGPathAddLineToPoint(path,NULL,offsetX3+wid3,offsetY3);     
     CGPathAddLineToPoint(path,NULL,offsetX3+wid3,offsetY3+hit3);
     
     CGPathAddLineToPoint(path,NULL,offsetX3,offsetY3+hit3);

     //    CGPathAddLineToPoint(path,NULL,0,400);
     
     context0 = UIGraphicsGetCurrentContext();     
     CGContextBeginPath(context0);
     CGContextAddPath(context0,path);
     
     CGContextSetRGBStrokeColor(context0, 1.0, 1.0, 1.0, 1.0);
     CGContextSetRGBFillColor(context0, 1.0, 0.0, 0.0, 0.6);    
     
     CGContextSetLineWidth(context0, 5.0);
     
     CGContextClosePath(context0);     
     CGContextFillPath(context0);

 return;
 
 }
*/ 
-(id) initWithFrame:(CGRect)frame{
    [super initWithFrame:frame];
    if (self){
        self.backgroundColor=[UIColor cyanColor];
        self.alpha=0.7;
        [self setClipsToBounds:false];
    }else{
        return NULL;
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end

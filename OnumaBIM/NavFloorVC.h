//
//  NavFloorVC.h
//  ProjectView
//
//  Created by Alfred Man on 11/18/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NavFloorDelegate 
@end
@class Bldg;
@class ViewModelVC;

@interface NavFloorVC : UITableViewController{
    Bldg* selectedBldg;
    id <NavFloorDelegate> delegate;
}
+(BOOL) isPoopingFloor;
+(void) setIsPoppingFloor:(BOOL) bPopping;

@property (assign) id <NavFloorDelegate> delegate;
@property (nonatomic, assign) Bldg* selectedBldg;

- (id) init: (ViewModelVC*) viewModelVC;

//- (void)displayViewModelThread:(id)inData;//(NSConnection *)connection

//- (void)displayViewModelThreadDone:(id)inData;
@end

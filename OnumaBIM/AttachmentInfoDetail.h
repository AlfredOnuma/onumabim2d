//
//  AttachmentInfoDetail.h
//  ProjectView
//
//  Created by Alfred Man on 2/27/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AssetsLibrary/AssetsLibrary.h>
#import "AttachmentImageView.h"
#import "sqlite3.h"
@class OPSProduct;
@class AttachmentInfoData;
@class OPSLocalProjectSiteInAttachmentInfoDetail;
@class AttachmentInfoTableUploadComment;
@class AROverlayViewController;
@interface AttachmentInfoDetail : UIViewController < UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIScrollViewDelegate, UIAlertViewDelegate, NSURLConnectionDelegate,UITextViewDelegate>{
    
    uint _numFileToUpload;
    uint _numFileUploaded;
    AROverlayViewController* _arOverlayViewController;
    IBOutlet UIScrollView* imgScrollView;
    IBOutlet UITextField* uiTextFieldAttachTitle;    
    IBOutlet UITextField* uiTextFieldAttachURL;    
    IBOutlet UITextView* uiTextViewAttachComment;
    
    IBOutlet UIButton* buttonImgLibrary;
    IBOutlet UIButton* buttonCamera;
    IBOutlet UIButton* buttonBin;
    IBOutlet UILabel* labelImgStrs;
    UIImageView* preLoadImgView;
    UIImageView* postLoadImgView;
    UIPopoverController* imgLibraryPopOverController;
    uint pImg;
    
    NSMutableArray* aImg;    
    NSString* attachmentTitle;
    NSString* attachmentComment;
    NSString* attachmentDate;
    NSString* attachmentCreatedBy;
    NSString* attachmentURL;
    
    IBOutlet UILabel* labelAttachDate;
    IBOutlet UILabel* labelAttachUser;    
//    AttachmentInfoData* _attachmentInfoData;
//    OPSProduct* attachedProduct;
    NSString* _attachmentProductType;
    uint _attachmentProductID;
    AttachmentInfoData* _attachmentInfo;
    NSMutableArray* aDeleteImg;
    NSMutableArray* aInsertImg;
    
    NSMutableArray* _aAttachmentInfoUploadFile;
    AttachmentInfoTableUploadComment* _attachmentInfoTableUploadComment;
    OPSLocalProjectSiteInAttachmentInfoDetail* _projectSiteInAttachmentInfoDetail;
    
    
    
    NSMutableData* _connectionData;
    NSURLConnection* _theConnection;
}
@property (nonatomic, retain) AROverlayViewController* arOverlayViewController;
@property (nonatomic, assign) uint attachmentProductID;
@property (nonatomic, retain) NSMutableData* connectionData;
@property (nonatomic, retain) NSURLConnection* theConnection;

@property (nonatomic, assign) uint numFileToUpload;
@property (nonatomic, assign) uint numFileUploaded;
@property (nonatomic, retain) NSMutableArray* aAttachmentInfoUploadFile;
@property (nonatomic, retain) AttachmentInfoTableUploadComment* attachmentInfoTableUploadComment;

@property (nonatomic, retain) OPSLocalProjectSiteInAttachmentInfoDetail* projectSiteInAttachmentInfoDetail;
@property (nonatomic,retain) NSMutableArray* aInsertImg;
@property (nonatomic,retain) NSMutableArray* aDeleteImg;
@property (nonatomic,assign) AttachmentInfoData* attachmentInfo;
@property (nonatomic,retain) NSString* attachmentProductType;
@property (nonatomic,retain) NSString* attachmentTitle;
@property (nonatomic,retain) NSString* attachmentComment;

@property (nonatomic,retain) NSString* attachmentURL;

@property (nonatomic,retain) IBOutlet UITextField* uiTextFieldAttachTitle;
@property (nonatomic,retain) IBOutlet UITextField* uiTextFieldAttachURL;
@property (nonatomic,retain) IBOutlet UITextView* uiTextViewAttachComment;

@property (nonatomic,retain) IBOutlet UILabel* labelAttachDate;
@property (nonatomic,retain) IBOutlet UILabel* labelAttachUser;
@property (nonatomic,retain) NSString* attachmentDate;
@property (nonatomic,retain) NSString* attachmentCreatedBy;
@property (nonatomic,retain) UIImageView* preLoadImgView;
@property (nonatomic,retain) UIImageView* postLoadImgView;

@property (nonatomic,assign) uint pImg;
@property (nonatomic,retain) NSMutableArray* aImg;
@property (nonatomic,retain) IBOutlet UIScrollView* imgScrollView;
@property (nonatomic,retain) IBOutlet UIButton* buttonImgLibrary;
@property (nonatomic,retain) IBOutlet UIButton* buttonCamera;
@property (nonatomic,retain) IBOutlet UIButton* buttonBin;
@property (nonatomic,retain) IBOutlet UILabel* labelImgStrs;
@property (nonatomic,retain) UIPopoverController* imgLibraryPopOverController;
//@property (nonatomic,assign) OPSProduct* attachedProduct;


-(IBAction)addImageFromLibrary:(id)sender;
-(IBAction)addImageFromCamera:(id)sender;
-(IBAction)removeImage:(id)sender;
-(IBAction)displayAttachWebView:(id)sender;
//-(void) setupScrollViewImage;

- (NSString*) generateImgFileNameByDate;
//- (NSString*) generateImgFileNameByIndex:(uint) index;
//-(void) setupScrollViewWithImageFromMedia: (NSDictionary *)info ;
-(void) setupScrollView;
-(void)rebuildImgScrollView;

- (NSUInteger) wordCount:(NSString*) string;
//- (void)addImageWithName:(NSString*)imageString atPosition:(int)position;
- (void) trashAttachmentEntry;//:(id) sender;
- (void) alertTrashAttachmentEntry:(id) sender;
- (void)removeImage:(id) sender;
- (void)addImage:(AttachmentImageView*) image atPosition:(int)position;
- (void) executeDeleteImgViewFromStorageAndDB;
//-(UIImage*)findLargeImage:(NSURL*) mediaurl;
- (void) actionOK:(id) sender;

- (void) actionUpload:(id) sender;

- (void) actionCancel:(id) sender;
- (void) toDeleteImgArray:(AttachmentImageView*) imgView;

-(void) removeInsertImgArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil attachmentInfo:(AttachmentInfoData*) attachmentInfo selectedProduct:(OPSProduct*)selectedProduct;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil attachedProduct:(OPSProduct*) _attachedProduct attachmentInfo:(AttachmentInfoData*) _attachmentInfo;
- (void) updateRecord;
//- (void) insertNewRecord;

- (void) insertNewRecordWithDatabase: (sqlite3*)database;
- (void) disableEdit;
typedef void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *asset);
typedef void (^ALAssetsLibraryAccessFailureBlock)(NSError *error);

-(void) uploadFileFinishWithAttachmentImageView:(AttachmentImageView*) attachmentImageView;

-(void) uploadAttachmentCommentFinish;
- (void) removeImageWithAttachmentImageView:(AttachmentImageView*) imgView sender:(id) sender;

-(void) dealWithImageChosen:(UIImage*) image;

//-(IBAction) userNoteDidChange :(UITextField *)theTextField;
@end

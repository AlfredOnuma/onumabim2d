//
//  ViewPlanFurnInfoVC.m
//  ProjectView
//
//  Created by Alfred Man on 5/18/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewPlanFurnInfoVC.h"

@interface ViewPlanFurnInfoVC ()

@end

@implementation ViewPlanFurnInfoVC

static sqlite3 *database = nil;

/*
 
 */
@synthesize uiComponent;
@synthesize uiComponentName;
@synthesize uiSerialNumber;
@synthesize uiType;
@synthesize uiManufacturer;
@synthesize uiModelNumber;
@synthesize uiInstallationDate;
@synthesize uiPartsWarrantyGuarantor;
@synthesize uiPartsWarrantyDuration;
@synthesize uiLaborWarrantyGuarantor;
@synthesize uiLaborWarrantyDuration;

-(void) dealloc{
    [uiComponent release];uiComponent=nil;
    [uiComponentName release];uiComponentName=nil;
    [uiSerialNumber release];uiSerialNumber=nil;
    [uiType release];uiType=nil;
    [uiManufacturer release];uiManufacturer=nil;
    [uiModelNumber release];uiModelNumber=nil;
    [uiInstallationDate release];uiInstallationDate=nil;
    [uiPartsWarrantyGuarantor release];uiPartsWarrantyGuarantor=nil;
    [uiPartsWarrantyDuration release];uiPartsWarrantyDuration=nil;
    [uiLaborWarrantyGuarantor release];uiLaborWarrantyGuarantor=nil;
    [uiLaborWarrantyDuration release];uiLaborWarrantyDuration=nil;
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}





-(id) initWithViewModelVC:(ViewModelVC*) viewModelVC{
    self=[super initWithViewModelVC:viewModelVC];
    if (self){
        [self readFromSQL];
    }
    return self;
}
//-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
//    //    return self.testUI;
//    //    NSLog(@"ScrollView ZoomScale %f",scrollView.zoomScale);
//    //    NSLog(@"ScrollView MaxZoomScale %f", scrollView.maximumZoomScale);
//    return self.displayModelUI;
//}
-(void) readFromSQL{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
    OPSProjectSite* projectSite=[appDele activeProjectSite];    
    if ([projectSite dbPath]==Nil) {return;}    
    
    //    Bldg* bldg=[appDele currentBldg];
    
//    Space* space=nil;
//    
//    if ([appDele modelDisplayLevel]==1){
//        ViewProductRep* productRep=[[self.viewModelVC aSelectedProductRep] lastObject];
//        if (![[productRep product] isKindOfClass:[Space class]]){
//            return;
//        }
//        space=(Space*) [productRep product];
//    }else if ([appDele modelDisplayLevel]==2){
//        space=[appDele currentSpace];
//    }else{
//        return;
//    }
    
    
    NSMutableArray* aSelectedProductRep=[self.viewModelVC aSelectedProductRep];
    uint numProductRep=[aSelectedProductRep count];
    
    uint numFurn=0;    
    
    for (uint pCol=0; pCol<numProductRep; pCol++)
    {
        ViewProductRep* productRep=[[self.viewModelVC aSelectedProductRep] objectAtIndex:pCol];
        if ([[productRep product] isKindOfClass:[Furn class]]){
            numFurn++;
        }
    }
    if (numFurn<=0){
        return;
    }
    
    
    CGRect rootFrame=self.viewModelVC.view.frame;
    
    rootFrame.origin=CGPointMake(0.0,0.0);
    
    uint topBotInset=50;
    uint rowSpacing=10;
    
    uint labelHeight=20;
    uint labelWidth=210;
    uint uiLabelWidth=250;//350 //130
    uint spaceLabelUILabelWidth=10;
    uint contentWidth=labelWidth+(uiLabelWidth+spaceLabelUILabelWidth)*numFurn;
    uint widthPadding=100;
    
    
    uint scrollContentWidth=0;

    if ((widthPadding*2+contentWidth)<rootFrame.size.width){
        scrollContentWidth=rootFrame.size.width;
        widthPadding=(rootFrame.size.width-contentWidth)/2;
    }else{
        scrollContentWidth=widthPadding*2+contentWidth;
    }
    
    
    UIView* tmpRootView=[[UIView alloc] initWithFrame:rootFrame];
    self.rootView=tmpRootView;
    
    NSLog (@"Root View Size: %f x %f",self.rootView.frame.size.width,self.rootView.frame.size.height);
    [tmpRootView release];
    [self setView:self.rootView];    
    
    UIView* tmpView=[[UIView alloc] initWithFrame:CGRectMake(0,0,scrollContentWidth,rootFrame.size.height)]; //[[UIView alloc] initWithFrame:rootFrame];
//    [tmpView setBackgroundColor:[UIColor redColor]];
    self.infoView=tmpView;
    
    NSLog (@"Info View Size: %f x %f",tmpView.frame.size.width,tmpView.frame.size.height);
    [tmpView release];	
    
    UIScrollView* scrollView=[[UIScrollView alloc] initWithFrame:self.rootView.frame];
//    [scrollView setContentSize:self.infoView.frame.size];    

    [scrollView setScrollEnabled:TRUE];
    NSLog (@"Info Scroll View Size: %f x %f",scrollView.frame.size.width,scrollView.frame.size.height);
    NSLog (@"Info Scroll View Content Size: %f x %f",scrollView.contentSize.width,scrollView.contentSize.height);
    self.infoScrollView=scrollView;
    
    [scrollView release];
    
    [self.rootView addSubview:self.infoScrollView];
    
    [self.infoScrollView addSubview:self.infoView];    
    
    uint contentHeight=0;//vPos+topBotInset;
    
    
    for (uint pCol=0; pCol<numProductRep; pCol++)
    {
        ViewProductRep* productRep=[[self.viewModelVC aSelectedProductRep] objectAtIndex:pCol];
        if (![[productRep product] isKindOfClass:[Furn class]]){
            continue;
        }
        
        Furn* furn=(Furn*) [productRep product];
        
        uint pRow=0;
        uint vPos=0;
        uint hPos=0;        
        
        vPos=topBotInset;        
                
        sqlite3_stmt* infoStmt;
        if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {            
            
            NSString* reportPart1=[NSString stringWithFormat:@""
                                   @"SELECT Element.name,"
                                   @" Equipment.componentName,"
                                   @" Equipment.serialNumber,"
                                   @" CobieType.typeName,"
                                   @" Manufacturer.company AS ManufacturerCompany,"
                                   @" CobieType.modelNumber,"
                                   @" date(Equipment.installationDate , 'unixepoch') as installationDate,"
                                   @" PartsWarrantyGuarantor.company AS PartsWarrantyGuarantor,"
                                   @" cobieType.warrantyDurationUnit,"
                                   @" cobieType.partsWarrantyDuration,"
                                   @" LabourWarrantyGuarantor.company AS LabourWarrantyGuarantor,"
                                   @" cobieType.LabourWarrantyDuration"
                                   ];
            
            NSString* reportFrom=[NSString stringWithFormat:@""
                                  @" FROM Equipment"
                                  @" LEFT JOIN Element ON Equipment.elementID=Element.ID"
                                  @" LEFT JOIN CobieType ON Equipment.typeID=CobieType.ID"
                                  @" LEFT JOIN contact AS Manufacturer on CobieType.manufacturer=Manufacturer.ID"
                                  @" LEFT JOIN contact AS PartsWarrantyGuarantor on CobieType.partsWarrantyGuarantor=PartsWarrantyGuarantor.ID"
                                  @" LEFT JOIN contact AS LabourWarrantyGuarantor on CobieType.labourWarrantyGuarantor=LabourWarrantyGuarantor.ID"
                                  @" WHERE Equipment.ID=%d",[furn ID]];
            
            
            
            NSString* sqlStr=[NSString stringWithFormat:@"%@ %@",reportPart1,reportFrom];
            const char* sqlChar=[sqlStr UTF8String];
            if(sqlite3_prepare_v2(database, sqlChar, -1, &infoStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(infoStmt) == SQLITE_ROW) {
                    
                    

                    vPos+=rowSpacing;
//                    hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    hPos=widthPadding;// (self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    
                    
                    if (pCol==0){
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelComponent= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelComponent];
                        labelComponent.text = [NSString stringWithFormat: @"Component:"];
                        [self.infoView addSubview:labelComponent];
                        [labelComponent release];                        
                    }
                    
                    hPos=widthPadding+ labelWidth+ (uiLabelWidth+spaceLabelUILabelWidth)*pCol+spaceLabelUILabelWidth;//(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                    UILabel* uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    [self prepareUILabel:uiLabel];
                    
                    uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:0];
                    self.uiComponent=uiLabel;
                    [self.infoView addSubview:self.uiComponent];
                    [uiLabel release];
                    vPos+=labelHeight;
                    pRow++;
                    
                    
                    vPos+=rowSpacing;
                    hPos=widthPadding;// (self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    
                    
                    if (pCol==0){
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelComponentName= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelComponentName];
                        labelComponentName.text = [NSString stringWithFormat: @"Component Name (mark):"];
                        [self.infoView addSubview:labelComponentName];
                        [labelComponentName release];
                    }
                    
                    hPos=widthPadding+ labelWidth+ (uiLabelWidth+spaceLabelUILabelWidth)*pCol+spaceLabelUILabelWidth;//(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    
                    [self prepareUILabel:uiLabel];
                    uiLabel.text =[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:1];
                    self.uiComponentName=uiLabel;
                    [self.infoView addSubview:self.uiComponentName];
                    [uiLabel release];
                    vPos+=labelHeight;
                    pRow++;
                    
                    
                    
                    vPos+=rowSpacing;
                    hPos=widthPadding;//(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    if (pCol==0){
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelSerialNumber= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelSerialNumber];
                        labelSerialNumber.text = [NSString stringWithFormat: @"Serial Number:"];
                        [self.infoView addSubview:labelSerialNumber];
                        [labelSerialNumber release];
                    }
                    
                    
                    
                    hPos=widthPadding+ labelWidth+ (uiLabelWidth+spaceLabelUILabelWidth)*pCol+spaceLabelUILabelWidth;//(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    [self prepareUILabel:uiLabel];
                    
                    uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:2];
                    
                    self.uiSerialNumber=uiLabel;
                    [self.infoView addSubview:self.uiSerialNumber];
                    [uiLabel release];
                    
                    vPos+=labelHeight;
                    pRow++;
                    
                    
                    
                    
                    vPos+=rowSpacing;
                    hPos=widthPadding;//(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    if (pCol==0){
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelType= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelType];
                        labelType.text = [NSString stringWithFormat: @"Type:"];
                        [self.infoView addSubview:labelType];
                        [labelType release];
                    }
                    
                                        
                    hPos=widthPadding+ labelWidth+ (uiLabelWidth+spaceLabelUILabelWidth)*pCol+spaceLabelUILabelWidth;//(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    [self prepareUILabel:uiLabel];
                    
                    uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:3];
                    
                    self.uiType=uiLabel;
                    [self.infoView addSubview:self.uiType];
                    [uiLabel release];
                    
                    vPos+=labelHeight;
                    pRow++;
                    
                    
                    
                    
                    
                    
                    
                    
                    vPos+=rowSpacing;
                    hPos=widthPadding;// (self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    if (pCol==0){
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelManufacturer= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelManufacturer];
                        labelManufacturer.text = [NSString stringWithFormat: @"Manufacturer:"];
                        [self.infoView addSubview:labelManufacturer];
                        [labelManufacturer release];
                    }
                    
                    
                    
                    hPos=widthPadding+ labelWidth+ (uiLabelWidth+spaceLabelUILabelWidth)*pCol+spaceLabelUILabelWidth;//(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    [self prepareUILabel:uiLabel];
                    
                    uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:4];
                    
                    self.uiManufacturer=uiLabel;
                    [self.infoView addSubview:self.uiManufacturer];
                    [uiLabel release];
                    
                    vPos+=labelHeight;
                    pRow++;
                    
                    
                    
                    
                    vPos+=rowSpacing;
                    hPos=widthPadding;//(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    if (pCol==0){
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelModelNumber= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelModelNumber];
                        labelModelNumber.text = [NSString stringWithFormat: @"Model Number:"];
                        [self.infoView addSubview:labelModelNumber];
                        [labelModelNumber release];
                    }
                    
                    
                    hPos=widthPadding+ labelWidth+ (uiLabelWidth+spaceLabelUILabelWidth)*pCol+spaceLabelUILabelWidth;//(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    [self prepareUILabel:uiLabel];
                    
                    uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:5];
                    
                    self.uiModelNumber=uiLabel;
                    [self.infoView addSubview:self.uiModelNumber];
                    [uiLabel release];
                    
                    vPos+=labelHeight;
                    pRow++;
                    
                    
                    
                    
                    
                    vPos+=rowSpacing;
                    hPos=widthPadding;//(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    if (pCol==0){
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelInstallationDate= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelInstallationDate];
                        labelInstallationDate.text = [NSString stringWithFormat: @"Installation Date:"];
                        [self.infoView addSubview:labelInstallationDate];
                        [labelInstallationDate release];
                    }
                    
                    
                    hPos=widthPadding+ labelWidth+ (uiLabelWidth+spaceLabelUILabelWidth)*pCol+spaceLabelUILabelWidth;//(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    [self prepareUILabel:uiLabel];
                    
                    uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:6];
                    self.uiInstallationDate=uiLabel;
                    [self.infoView addSubview:self.uiInstallationDate];
                    [uiLabel release];
                    
                    vPos+=labelHeight;
                    pRow++;
                    
                    
                    
                    
                    
                    vPos+=rowSpacing;
                    hPos=widthPadding;// (self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    if (pCol==0){
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelPartsWarrantyGuarantor= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelPartsWarrantyGuarantor];
                        labelPartsWarrantyGuarantor.text = [NSString stringWithFormat: @"Parts Warranty Guarantor:"];
                        [self.infoView addSubview:labelPartsWarrantyGuarantor];
                        [labelPartsWarrantyGuarantor release];
                    }
                    
                    
                    hPos=widthPadding+ labelWidth+ (uiLabelWidth+spaceLabelUILabelWidth)*pCol+spaceLabelUILabelWidth;//(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    [self prepareUILabel:uiLabel];
                    
                    uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:7];
                    
                    self.uiPartsWarrantyGuarantor=uiLabel;
                    [self.infoView addSubview:self.uiPartsWarrantyGuarantor];
                    [uiLabel release];
                    
                    vPos+=labelHeight;
                    pRow++;
                    
                    
                    
                    
                    
                    NSString* warrantyDurationUnit=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:8];
                    vPos+=rowSpacing;
                    hPos=widthPadding;// (self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    if (pCol==0){
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelPartsWarrantyDuration= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelPartsWarrantyDuration];
                        labelPartsWarrantyDuration.text = [NSString stringWithFormat: @"Parts Warranty Duration:"];
                        [self.infoView addSubview:labelPartsWarrantyDuration];
                        [labelPartsWarrantyDuration release];
                    }
                    
                    
                    hPos=widthPadding+ labelWidth+ (uiLabelWidth+spaceLabelUILabelWidth)*pCol+spaceLabelUILabelWidth;//(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    [self prepareUILabel:uiLabel];
                    
                    NSString* partsWarrantyDurationStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:9];
                    uiLabel.text=[NSString stringWithFormat:@"%@ %@",partsWarrantyDurationStr,warrantyDurationUnit];
                    self.uiPartsWarrantyDuration=uiLabel;
                    [self.infoView addSubview:self.uiPartsWarrantyDuration];
                    [uiLabel release];
                    
                    vPos+=labelHeight;
                    pRow++;
                    
                    
                    
                    
                    vPos+=rowSpacing;
                    hPos=widthPadding;//(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    if (pCol==0){
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelLaborWarrantyGuarantor= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelLaborWarrantyGuarantor];
                        labelLaborWarrantyGuarantor.text = [NSString stringWithFormat: @"Labor Warranty Guarantor:"];
                        [self.infoView addSubview:labelLaborWarrantyGuarantor];
                        [labelLaborWarrantyGuarantor release];
                    }
                    
                    
                    
                    
                    hPos=widthPadding+ labelWidth+ (uiLabelWidth+spaceLabelUILabelWidth)*pCol+spaceLabelUILabelWidth;//(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    [self prepareUILabel:uiLabel];
                    
                    uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:10];
                    
                    self.uiLaborWarrantyGuarantor=uiLabel;
                    [self.infoView addSubview:self.uiLaborWarrantyGuarantor];
                    [uiLabel release];
                    
                    vPos+=labelHeight;
                    pRow++;
                    
                    
                    
                    
                    
                    
                    vPos+=rowSpacing;
                    hPos=widthPadding;//(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    if (pCol==0){
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelLaborWarrantyDuration= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelLaborWarrantyDuration];
                        labelLaborWarrantyDuration.text = [NSString stringWithFormat: @"Labor Warranty Duration:"];
                        [self.infoView addSubview:labelLaborWarrantyDuration];
                        [labelLaborWarrantyDuration release];
                    }
                    
                    
                    
                    hPos=widthPadding+ labelWidth+ (uiLabelWidth+spaceLabelUILabelWidth)*pCol+spaceLabelUILabelWidth;//(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    [self prepareUILabel:uiLabel];
                    
                    NSString* laborWarrantyDurationStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:11];
                    uiLabel.text=[NSString stringWithFormat:@"%@ %@",laborWarrantyDurationStr,warrantyDurationUnit];
                    self.uiLaborWarrantyGuarantor=uiLabel;
                    [self.infoView addSubview:self.uiLaborWarrantyGuarantor];
                    [uiLabel release];
                    
                    vPos+=labelHeight;
                    pRow++;
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    UILabel* labelCell=nil;
                    
                    
                    
                    vPos+=rowSpacing*3;
                    hPos=widthPadding;//(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                    //                hPos+=labelXSpacing;
                    //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
                    
                    if (pCol==0){
                        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];
                        [self prepareSelectedProductTitle:labelCell];
                        labelCell.text=@"Selected Element:";
                        [self.infoView addSubview:labelCell];
                        [labelCell release];
                        //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning
                        
                        vPos+=[self selectedProductTitleFontSize];
                        pRow++;
                        
                        
                        
                        if ([self.viewModelVC aSelectedProductRep] && [[self.viewModelVC aSelectedProductRep] count]>0){
                            for (uint pViewProductRep=0; pViewProductRep<[[self.viewModelVC aSelectedProductRep] count];pViewProductRep++){
                                ViewProductRep* viewProductRep=[[self.viewModelVC aSelectedProductRep] objectAtIndex:pViewProductRep];
                                OPSProduct* product=[viewProductRep product];
                                
                                vPos+=rowSpacing;
                                hPos=widthPadding;//(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                                //                hPos+=labelXSpacing;
                                //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
                                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];
                                [self prepareSelectedProductLabel:labelCell];
                                labelCell.text=[product productRepDisplayStr];
                                [self.infoView addSubview:labelCell];
                                [labelCell release];
                                //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning
                                
                                vPos+=labelHeight;
                                pRow++;
                                
                            }
                        }else{
                            
                            
                            OPSProduct* product=(OPSProduct*) [[self.viewModelVC model] root];
                            
                            vPos+=rowSpacing;
                            hPos=widthPadding;//(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                            //                hPos+=labelXSpacing;        
                            //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
                            labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
                            [self prepareSelectedProductLabel:labelCell];        
                            labelCell.text=[product productRepDisplayStr];
                            //                labelCell.text=[NSString stringWithFormat:@"%d - %@",[product ID],[product name]];
                            [self.infoView addSubview:labelCell];        
                            [labelCell release];  
                            //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
                            
                            vPos+=labelHeight;
                            pRow++;
                        }
                        
                        //                vPos+=labelHeight;
                        
                        uint newContentHeight=vPos+topBotInset;
                        if (newContentHeight>contentHeight){
                            contentHeight=newContentHeight;
                        }
                    }
                    
                }
            }        
            
            
        }
        sqlite3_close(database);
        


    }
    
    [self.infoView setFrame:CGRectMake(0,0,self.infoView.frame.size.width,contentHeight)];
    [self.infoScrollView setContentSize:CGSizeMake(self.infoView.frame.size.width, self.infoView.frame.size.height)];
    
    
    ////////////////////////////////////////--///////////////////////////////////////////////
    
    /*
    
    ViewProductRep* productRep=[[self.viewModelVC aSelectedProductRep] lastObject];
    if (![[productRep product] isKindOfClass:[Furn class]]){
        return;
    }    
    Furn* furn=(Furn*) [productRep product];

    
    uint topBotInset=50;
    uint rowSpacing=10;
        
    uint labelHeight=20;
    uint labelWidth=210;
    uint uiLabelWidth=350; //130
    uint spaceLabelUILabelWidth=10;
    uint contentWidth=labelWidth+uiLabelWidth+spaceLabelUILabelWidth;
    
    
    
    CGRect rootFrame=self.viewModelVC.view.frame;
    UIView* tmpRootView=[[UIView alloc] initWithFrame:rootFrame];
    self.rootView=tmpRootView;
    [tmpRootView release];
    [self setView:self.rootView];
    
    
    
    
    UIView* tmpView=[[UIView alloc] initWithFrame:rootFrame];
    self.infoView=tmpView;        
    [tmpView release];     
    
    
    UIScrollView* scrollView=[[UIScrollView alloc] initWithFrame:self.rootView.frame];
    [scrollView setContentSize:self.infoView.frame.size];
    self.infoScrollView=scrollView;
    [scrollView release];
    
    [self.rootView addSubview:self.infoScrollView];
    
    [self.infoScrollView addSubview:self.infoView];
    
    
    
    
    uint pRow=0;
    uint pCol=0;
    uint vPos=0;
    uint hPos=0;
    

    vPos=topBotInset;   
    
        
    
    
    sqlite3_stmt* infoStmt;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  

        
        NSString* reportPart1=[NSString stringWithFormat:@""
                               @"SELECT Element.name,"
                               @" Equipment.componentName,"
                               @" Equipment.serialNumber,"
                               @" CobieType.typeName,"
                               @" Manufacturer.company AS ManufacturerCompany,"
                               @" CobieType.modelNumber,"                               
                               @" date(Equipment.installationDate , 'unixepoch') as installationDate,"
                               @" PartsWarrantyGuarantor.company AS PartsWarrantyGuarantor,"                                
                               @" cobieType.warrantyDurationUnit,"                       
                               @" cobieType.partsWarrantyDuration,"
                               @" LabourWarrantyGuarantor.company AS LabourWarrantyGuarantor,"
                               @" cobieType.LabourWarrantyDuration"

                               ];
        
        NSString* reportFrom=[NSString stringWithFormat:@""                      
                              @" FROM Equipment"
                              @" LEFT JOIN Element ON Equipment.elementID=Element.ID"
                              @" LEFT JOIN CobieType ON Equipment.typeID=CobieType.ID"
                              @" LEFT JOIN contact AS Manufacturer on CobieType.manufacturer=Manufacturer.ID"
                              @" LEFT JOIN contact AS PartsWarrantyGuarantor on CobieType.partsWarrantyGuarantor=PartsWarrantyGuarantor.ID"
                              @" LEFT JOIN contact AS LabourWarrantyGuarantor on CobieType.labourWarrantyGuarantor=LabourWarrantyGuarantor.ID"
                              @" WHERE Equipment.ID=%d",[furn ID]];
        
        
        
        NSString* sqlStr=[NSString stringWithFormat:@"%@ %@",reportPart1,reportFrom];
        const char* sqlChar=[sqlStr UTF8String];  
        if(sqlite3_prepare_v2(database, sqlChar, -1, &infoStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(infoStmt) == SQLITE_ROW) {  
                

                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
                

                UILabel* labelComponent= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelComponent];        
                labelComponent.text = [NSString stringWithFormat: @"Component:"];
                [self.infoView addSubview:labelComponent];
                [labelComponent release];
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                UILabel* uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];                 
                [self prepareUILabel:uiLabel];
                
                uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:0];                  
                self.uiComponent=uiLabel;        
                [self.infoView addSubview:self.uiComponent];        
                [uiLabel release];       
                vPos+=labelHeight;
                pRow++;
                
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
                UILabel* labelComponentName= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelComponentName];        
                labelComponentName.text = [NSString stringWithFormat: @"Component Name (mark):"];
                [self.infoView addSubview:labelComponentName];
                [labelComponentName release];
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];                                 
                
                [self prepareUILabel:uiLabel];
                uiLabel.text =[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:1];                  
                self.uiComponentName=uiLabel;        
                [self.infoView addSubview:self.uiComponentName];        
                [uiLabel release];      
                vPos+=labelHeight;             
                pRow++;
                
                
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
                UILabel* labelSerialNumber= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelSerialNumber];        
                labelSerialNumber.text = [NSString stringWithFormat: @"Serial Number:"];
                [self.infoView addSubview:labelSerialNumber];
                [labelSerialNumber release];
                
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];   
                [self prepareUILabel:uiLabel];
                          
                uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:2];
                
                self.uiSerialNumber=uiLabel;        
                [self.infoView addSubview:self.uiSerialNumber];        
                [uiLabel release];
                
                vPos+=labelHeight;
                pRow++;
                
                
                
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
                UILabel* labelType= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelType];        
                labelType.text = [NSString stringWithFormat: @"Type:"];
                [self.infoView addSubview:labelType];
                [labelType release];
                
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ]; 
                [self prepareUILabel:uiLabel];
                               
                uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:3];
                
                self.uiType=uiLabel;        
                [self.infoView addSubview:self.uiType];        
                [uiLabel release];
                
                vPos+=labelHeight;
                pRow++;
                
                
                
                
                                
                
                
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
                UILabel* labelManufacturer= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelManufacturer];        
                labelManufacturer.text = [NSString stringWithFormat: @"Manufacturer:"];
                [self.infoView addSubview:labelManufacturer];
                [labelManufacturer release];
                
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ]; 
                [self prepareUILabel:uiLabel];
                                
                uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:4];
                
                self.uiManufacturer=uiLabel;        
                [self.infoView addSubview:self.uiManufacturer];        
                [uiLabel release];
                
                vPos+=labelHeight;
                pRow++;
                
                                
                
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
                UILabel* labelModelNumber= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelModelNumber];        
                labelModelNumber.text = [NSString stringWithFormat: @"Model Number:"];
                [self.infoView addSubview:labelModelNumber];
                [labelModelNumber release];
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ]; 
                [self prepareUILabel:uiLabel];
                
                uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:5];
                
                self.uiModelNumber=uiLabel;        
                [self.infoView addSubview:self.uiModelNumber];        
                [uiLabel release];
                
                vPos+=labelHeight;
                pRow++;
                
                
                
                                
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
                UILabel* labelInstallationDate= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelInstallationDate];        
                labelInstallationDate.text = [NSString stringWithFormat: @"Installation Date:"];
                [self.infoView addSubview:labelInstallationDate];
                [labelInstallationDate release];
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ]; 
                [self prepareUILabel:uiLabel];
                
                uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:6];                
                self.uiInstallationDate=uiLabel;        
                [self.infoView addSubview:self.uiInstallationDate];        
                [uiLabel release];
                
                vPos+=labelHeight;
                pRow++;
                
                
                
                
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
                UILabel* labelPartsWarrantyGuarantor= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelPartsWarrantyGuarantor];        
                labelPartsWarrantyGuarantor.text = [NSString stringWithFormat: @"Parts Warranty Guarantor:"];
                [self.infoView addSubview:labelPartsWarrantyGuarantor];
                [labelPartsWarrantyGuarantor release];
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                [self prepareUILabel:uiLabel];
                
                uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:7];
                
                self.uiPartsWarrantyGuarantor=uiLabel;        
                [self.infoView addSubview:self.uiPartsWarrantyGuarantor];        
                [uiLabel release];
                
                vPos+=labelHeight;
                pRow++;
                
                
                
                
                
                NSString* warrantyDurationUnit=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:8];
                
                
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight]; 
                
                UILabel* labelPartsWarrantyDuration= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelPartsWarrantyDuration];        
                labelPartsWarrantyDuration.text = [NSString stringWithFormat: @"Parts Warranty Duration:"];
                [self.infoView addSubview:labelPartsWarrantyDuration];
                [labelPartsWarrantyDuration release];
                
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];

                [self prepareUILabel:uiLabel];
                
                NSString* partsWarrantyDurationStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:9];
                uiLabel.text=[NSString stringWithFormat:@"%@ %@",partsWarrantyDurationStr,warrantyDurationUnit];
                self.uiPartsWarrantyDuration=uiLabel;        
                [self.infoView addSubview:self.uiPartsWarrantyDuration];        
                [uiLabel release];
                
                vPos+=labelHeight;
                pRow++;
                
                
                
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight]; 
                UILabel* labelLaborWarrantyGuarantor= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelLaborWarrantyGuarantor];        
                labelLaborWarrantyGuarantor.text = [NSString stringWithFormat: @"Labor Warranty Guarantor:"];
                [self.infoView addSubview:labelLaborWarrantyGuarantor];
                [labelLaborWarrantyGuarantor release];
                
                
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                [self prepareUILabel:uiLabel];
                
                uiLabel.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:10];
                
                self.uiLaborWarrantyGuarantor=uiLabel;        
                [self.infoView addSubview:self.uiLaborWarrantyGuarantor];        
                [uiLabel release];
                
                vPos+=labelHeight;
                pRow++;
                
                
                
                
                
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight]; 

                UILabel* labelLaborWarrantyDuration= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelLaborWarrantyDuration];        
                labelLaborWarrantyDuration.text = [NSString stringWithFormat: @"Labor Warranty Duration:"];
                [self.infoView addSubview:labelLaborWarrantyDuration];
                [labelLaborWarrantyDuration release];
                
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                [self prepareUILabel:uiLabel];
                
                NSString* laborWarrantyDurationStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:11];
                uiLabel.text=[NSString stringWithFormat:@"%@ %@",laborWarrantyDurationStr,warrantyDurationUnit];
                self.uiLaborWarrantyGuarantor=uiLabel;        
                [self.infoView addSubview:self.uiLaborWarrantyGuarantor];        
                [uiLabel release];
                
                vPos+=labelHeight;
                pRow++;
                
                
             
                
                
                
                
                
                
                
                
                
                
                UILabel* labelCell=nil;
                
                
                
                vPos+=rowSpacing*3;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                
                //                hPos+=labelXSpacing;        
                //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
                [self prepareSelectedProductTitle:labelCell];        
                labelCell.text=@"Selected Element:";
                [self.infoView addSubview:labelCell];        
                [labelCell release];  
                //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
                
                vPos+=[self selectedProductTitleFontSize];
                pRow++;
                
                
                
                if ([self.viewModelVC aSelectedProductRep] && [[self.viewModelVC aSelectedProductRep] count]>0){
                    for (uint pViewProductRep=0; pViewProductRep<[[self.viewModelVC aSelectedProductRep] count];pViewProductRep++){
                        ViewProductRep* viewProductRep=[[self.viewModelVC aSelectedProductRep] objectAtIndex:pViewProductRep];
                        OPSProduct* product=[viewProductRep product];                        
                        
                        vPos+=rowSpacing;
                        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                
                        //                hPos+=labelXSpacing;        
                        //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
                        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
                        [self prepareSelectedProductLabel:labelCell];                            
                        labelCell.text=[product productRepDisplayStr];
                        [self.infoView addSubview:labelCell];        
                        [labelCell release];  
                        //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
                        
                        vPos+=labelHeight;
                        pRow++;
                        
                    }
                }else{
                    
                    
                    OPSProduct* product=(OPSProduct*) [[self.viewModelVC model] root];
                    
                    vPos+=rowSpacing;
                    hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                
                    //                hPos+=labelXSpacing;        
                    //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
                    labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
                    [self prepareSelectedProductLabel:labelCell];        
                    labelCell.text=[product productRepDisplayStr];
                    //                labelCell.text=[NSString stringWithFormat:@"%d - %@",[product ID],[product name]];
                    [self.infoView addSubview:labelCell];        
                    [labelCell release];  
                    //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
                    
                    vPos+=labelHeight;
                    pRow++;
                }
  
//                vPos+=labelHeight;
                
                uint contentHeight=vPos+topBotInset;
                
                [self.infoScrollView setContentSize:CGSizeMake(rootFrame.size.width, contentHeight)];
                
            }
        }        
        
        
    }
    sqlite3_close(database);
     
    
    */
    
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end

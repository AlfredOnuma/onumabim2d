//
//  SitePolygon.m
//  ProjectView
//
//  Created by onuma on 09/05/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "SitePolygon.h"
#import "ProjectViewAppDelegate.h"
#import "ExtrudedAreaSolid.h"
#import "Representation.h"
#import "DisplayInfo.h"
@implementation SitePolygon
@synthesize color=_color;
@synthesize type=_type;

- (void)dealloc
{
    [_color release];_color=nil;
    [_type release];_type=nil;
    [super dealloc];
}

-(SitePolygon*) initWithSitePolygonSQLStmt: (OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform{
    self.model=model;
    NSInteger sitePolygonID = sqlite3_column_int(sqlStmt, 0);
    //    NSInteger slabPolylineID = sqlite3_column_int(sqlStmt, 1);
    
    NSString* sitePolygonGuid= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 2)!=nil){
        sitePolygonGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 2)];
    }else{
        sitePolygonGuid=[NSString stringWithFormat:@""];
    }
    
    NSString* name=[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:3];
    
    self=[super init:sitePolygonID guid:sitePolygonGuid name:name];
    if (self){
        
        //                                        [slabGuid autorelease];
        //        CGFloat slabThk=sqlite3_column_double(sqlStmt, 3);
        CGFloat placementX = sqlite3_column_double(sqlStmt, 4);//*multiFactor;
        CGFloat placementY = sqlite3_column_double(sqlStmt, 5);//*multiFactor;
        //        CGFloat placementZ = sqlite3_column_double(sqlStmt, 6);//*multiFactor;
        CGFloat placementAngle = sqlite3_column_double(sqlStmt, 7);
        
        char* pPolyStr=(char *) sqlite3_column_text(sqlStmt, 8);
        char* pArcStr=(char *) sqlite3_column_text(sqlStmt, 9);
        
        //        NSString* polyStr= [NSString stringWithUTF8String:(char *) sqlite3_column_text16(sqlStmt, 8)];
        CGPoint placementPt=CGPointMake(placementX, placementY);
        Placement* sitePolygonPlacement=[[Placement alloc] init:0 guid:nil name:nil pt:placementPt angle:placementAngle isMirrorY:false];
        
        [self setPlacement:sitePolygonPlacement];
        [sitePolygonPlacement release];
        
        //    NSLog(@"Slab -------------------------------");
        //    NSLog(@"Slab Placement x:%f y:%f angle:%f",placementX,placementY,placementAngle);
        
        
        CGAffineTransform sitePolygonTransform=CGAffineTransformConcat([sitePolygonPlacement toCGAffineTransform], parentTransform);
        
        if (pPolyStr!=0){
            NSString* polyStr= [NSString stringWithUTF8String:pPolyStr];
            
            NSString* arcStr=nil;
            if (pArcStr!=0){
                arcStr=[NSString stringWithUTF8String:pArcStr];
            }
            
            
            ExtrudedAreaSolid* extrudedAreaSolid=[[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr arcStr:arcStr parentTransform:sitePolygonTransform localFrameTransform:[placement toCGAffineTransform]];
            
            //        ExtrudedAreaSolid* extrudedAreaSolid= [[ExtrudedAreaSolid alloc] initWithExtrudedDatabase:database polylineID:slabPolylineID parentTransform:slabTransform];
            
            Representation* rep=[[Representation alloc] init:(0) guid:(nil) name:(nil)];
            [rep addARepresentationItem:(extrudedAreaSolid)];
            [extrudedAreaSolid release];
            [self setRepresentation:rep];
            [rep release];
        }
        
        
        
        
        NSString* typeStr=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:10]];
        self.type=typeStr;
        [typeStr release];
        
        
        NSString* colorStr=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:11]];
        self.color=colorStr;
        [colorStr release];
        
        
        CGFloat labelDisplacementX = sqlite3_column_double(sqlStmt, 12);
        CGFloat labelDisplacementY = sqlite3_column_double(sqlStmt, 13);
        DisplayInfo* _displayInfo=[[DisplayInfo alloc] initWithProduct:self displacement:CGPointMake(labelDisplacementX, labelDisplacementY)];
        self.displayInfo=_displayInfo;
        [_displayInfo release];

    }
    return self;
}

@end

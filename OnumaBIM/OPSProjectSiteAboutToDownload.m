//
//  OPSProjectSiteAboutToDownload.m
//  ProjectView
//
//  Created by Alfred Man on 7/6/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "OPSProjectSiteAboutToDownload.h"
#import "ProjectViewAppDelegate.h"
#import "LocalProjectListVC.h"
#import "LiveProjectListVC.h"
#import "LiveStudioListVC.h"
#import "OPSStudio.h"
#import "OPSLocalProjectSite.h"

@implementation OPSProjectSiteAboutToDownload
@synthesize liveProjectListVC=_liveProjectListVC;

@synthesize uploadLocalProjectSiteBeforeDownload=_uploadLocalProjectSiteBeforeDownload;
uint const OPSProjectSiteAboutToDownload_OVERWRITE_ALERT = 0;
uint const OPSProjectSiteAboutToDownload_LAUNCH_LOCAL_ALERT = 1;
uint const OPSProjectSiteAboutToDownload_UPLOADBEFOREOVERWRITE_ALERT=2;
-(void) dealloc{
    [_uploadLocalProjectSiteBeforeDownload release];_uploadLocalProjectSiteBeforeDownload=nil;
    [super dealloc];
}


-(id) initWithLiveOPSProjectList: liveProjectListVC{

    self=[super init];
    if (self){
        self.liveProjectListVC=liveProjectListVC;
    }
    return self;    
}




-(void) tryDownloadSite{
    
  
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    

    if (![appDele isLiveDataSourceAvailable]){     
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        return;
    }   
    
        
  
        
    OPSProjectSite* liveProjectSite=[appDele activeProjectSite];        
    OPSLocalProjectSite* localProjectSite=[[OPSLocalProjectSite alloc] initWithLiveOPSProjectSite:liveProjectSite];
    
    int localModifiedDate=-1;
    bool isLocalProjectAttachmentEmpty=true;
    
    if (localProjectSite){        
        
        localModifiedDate=[localProjectSite getLocalDatabaseModifiedDate];
        if (localModifiedDate>=0){            
            isLocalProjectAttachmentEmpty=[localProjectSite isLocalProjectAttachmentEmpty];  
        }else{
            [self downloadSite];     
            [localProjectSite release];
            return;
        }
    }else{
        [self downloadSite];
        [localProjectSite release];
        return;
    }        
    
    
    int liveModifiedDate=[localProjectSite liveModifiedDate];  
    [localProjectSite release];
    
    
              
    
    if (liveModifiedDate>localModifiedDate){
            if (isLocalProjectAttachmentEmpty){            
            UIAlertView *alert = [[UIAlertView alloc] 
                                  initWithTitle: @"You downloaded this scheme before. But it has been modified since download, overwrite?"
                                  message: @""
                                  delegate: self
                                  cancelButtonTitle: @"Overwrite"
                                  otherButtonTitles: @"Cancel", nil];
            alert.tag = OPSProjectSiteAboutToDownload_OVERWRITE_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
            [alert show];
            [alert release];                                
        }else{
            UIAlertView *alert = [[UIAlertView alloc] 
                                  initWithTitle: @"You downloaded this scheme before. But it has been modified since download. However, the local site has attachment yet to be uploaded"
                                  message: @""
                                  delegate: self
                                  cancelButtonTitle: @"Canel"
                                  otherButtonTitles: @"Upload First", @"Just Overwrite", nil];
            alert.tag = OPSProjectSiteAboutToDownload_UPLOADBEFOREOVERWRITE_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
            [alert show];
            [alert release];                       
        }

    }else{
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle: @"You downloaded this scheme before. It hasn't been modified since then. Launch your downloaded scheme?"
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"Yes"
                              otherButtonTitles: @"No", nil];
        alert.tag = OPSProjectSiteAboutToDownload_LAUNCH_LOCAL_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];    
    }

    
    
            
    
}





- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case OPSProjectSiteAboutToDownload_OVERWRITE_ALERT:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	     
                    
                    [self downloadSite];
                }
                    break;
                case 1: // delete
                {
                    
                    NSLog(@"Delete was cancelled by the user");
                    // do the delete
                }
                    break;
            }
            
            
        }     
            break;
        case OPSProjectSiteAboutToDownload_LAUNCH_LOCAL_ALERT:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	                    
                    [self.liveProjectListVC launchLocal];
                }
                    break;
                case 1: // delete
                {                    
                    //                    NSLog(@"Delete was cancelled by the user");
                    // do the delete
                }
                    break;
            }
            
            
        }     
            break;            
            
            
        case OPSProjectSiteAboutToDownload_UPLOADBEFOREOVERWRITE_ALERT:
        {
            switch (buttonIndex){                    
                case 0: // cancel
                {	     
                    
                    //                    NSLog (@"0");
                }
                    break;
                case 1: // Upload Before Overwrite
                {                                                     
                    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
                    
                    
                    
                    
                    OPSLocalProjectSite* opsLocalProjectSite=[[OPSLocalProjectSite alloc] initWithLiveOPSProjectSite:[appDele activeProjectSite] ];
                    self.uploadLocalProjectSiteBeforeDownload=opsLocalProjectSite;
                    [opsLocalProjectSite release];

                    //                    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[opsLocalProjectSite name]]  invoker:opsLocalProjectSite completeAction:@selector(trashAllAttachmentEntry) actionParamArray:nil];
                    
                    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[self.uploadLocalProjectSiteBeforeDownload name]]  invoker:self.uploadLocalProjectSiteBeforeDownload completeAction:@selector(uploadProjectSite:) actionParamArray:nil];
                    
                    
                    
                    
                    
                    /*                    
                     OPSLocalProjetSiteUploadBeforeDownlaod* opsLocalProjetSiteUploadBeforeDownlaod=[[OPSLocalProjetSiteUploadBeforeDownlaod alloc] initWithLiveOPSProjectSite:[appDele activeProjectSite] viewPlanSiteAndBldgInfoVC:self];
                     
                     
                     
                     [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[opsLocalProjetSiteUploadBeforeDownlaod name]]  invoker:opsLocalProjetSiteUploadBeforeDownlaod completeAction:@selector(uploadProjectSite:) actionParamArray:[NSArray arrayWithObjects: self,nil]];
                     
                     [opsLocalProjetSiteUploadBeforeDownlaod release];
                     */
                    
                    /*
                     
                     OPSLocalProjectSite* localProjectSite=[[OPSLocalProjectSite alloc] initWithLiveOPSProjectSite:[appDele activeProjectSite]];
                     
                     [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[localProjectSite name]]  invoker:appDele completeAction:@selector(uploadSelectedProjectSite:) actionParamArray:[NSArray arrayWithObjects: localProjectSite,localProjectSite.dbPath,self,nil]];
                     [localProjectSite release];
                     
                     
                     //                    [self downloadSite:nil];
                     */
                    //                    NSLog (@"1");
                }                    
                    break;
                case 2: // Overwrite 
                {
                    //                    NSLog (@"2");                                           
                    [self downloadSite];
                    //do nothing , cancel
                }
                    
                    
                    break;
            }
        }
            break;
        default:
            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");            
    }	
}



-(void) downloadSite{
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
        
    if (![appDele isLiveDataSourceAvailable]){     
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        return;
    }   
    
    
        
    
    OPSProjectSite* projectSite=[appDele activeProjectSite];
    
    
    
    NSFileManager* fileManager=[NSFileManager defaultManager];        
    [fileManager removeItemAtPath: projectSite.dbPath error:NULL]; 
    
    
    
    

    
//    LiveProjectListVC* liveProjectListVC=[[[self navigationController] viewControllers] objectAtIndex:( [[[self navigationController] viewControllers] count]-2)];
    
    
    
    
    bool bNoGraphicData=false;
    [self.liveProjectListVC setIsOnlyLoadingSiteInfo:false];
    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Generating File from Server for site: %@",projectSite.name] invoker:self.liveProjectListVC completeAction:@selector(copyModelFromServer:) actionParamArray:[NSArray arrayWithObjects:[NSNumber numberWithBool:bNoGraphicData], nil]];
                        
    
}



@end

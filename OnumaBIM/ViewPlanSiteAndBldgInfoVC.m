//
//  ViewPlanSiteAndBldgInfoVC.m
//  ProjectView
//
//  Created by Alfred Man on 6/22/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewPlanSiteAndBldgInfoVC.h"
#import "ViewProductRep.h"
//#import "ProjectViewAppDelegate.h"
//#import "OPSProjectSite.h"
//#import "ViewModelToolbar.h"
//#import "ViewModelVC.h"
#import "Site.h"
//#import "ViewProductRep.h"
//#import "OPSProduct.h"
//#import "OPSModel.h"
#import "Floor.h"
#import "Bldg.h"
//#import "RelAggregate.h"
#import "OPSProduct.h"
#import "OPSLocalProjectSite.h"

#import <QuartzCore/QuartzCore.h>
#import "LocalProjectListVC.h"
#import "LiveProjectListVC.h"
#import "LiveStudioListVC.h"
#import "OPSStudio.h"
#import "OPSProjectSite.h"


@implementation ViewPlanSiteAndBldgInfoVC

static sqlite3 *database = nil;


@synthesize dateModified;


@synthesize infoView;
@synthesize infoScrollView;
@synthesize liveSiteReportButton;
@synthesize viewButton;
@synthesize downloadButton;

@synthesize siteAreaTextField;
@synthesize numBldgTextField0;
@synthesize numBldgsNetCalculatedTextField0;
@synthesize grossCalculatedTextField0;
@synthesize grossReportedTextField0;
@synthesize capacityTextField0;
@synthesize occupancyTextField0;
@synthesize occupancyNumberTextField0;
@synthesize totalEstimatedBuildingCostTextField0;

@synthesize tmpNumBldg;        
@synthesize tmpSumBldgNetArea;
@synthesize tmpSumBldgGrossArea;
@synthesize tmpSumReportedGrossArea;
@synthesize tmpSumCapicity;
@synthesize tmpSumOccupancy;
@synthesize tmpSumOccupancyNumber;
@synthesize tmpSumEstimateCost;



//
//
//@synthesize numExisting;        
//@synthesize sumExistingBldgNetArea;
//@synthesize sumExistingBldgGrossArea;
//@synthesize sumExistingReportedGrossArea;
//@synthesize sumExistingCapicity;
//@synthesize sumExistingOccupancy;
//@synthesize sumExistingOccupancyNumber;
//@synthesize sumExistingEstimateCost;
//

@synthesize numBldgTextField1;
@synthesize numBldgsNetCalculatedTextField1;
@synthesize grossCalculatedTextField1;
@synthesize grossReportedTextField1;
@synthesize capacityTextField1;
@synthesize occupancyTextField1;
@synthesize occupancyNumberTextField1;
@synthesize totalEstimatedBuildingCostTextField1;

@synthesize numBldgTextField2;
@synthesize numBldgsNetCalculatedTextField2;
@synthesize grossCalculatedTextField2;
@synthesize grossReportedTextField2;
@synthesize capacityTextField2;
@synthesize occupancyTextField2;
@synthesize occupancyNumberTextField2;
@synthesize totalEstimatedBuildingCostTextField2;


@synthesize numBldgTextField3;
@synthesize numBldgsNetCalculatedTextField3;
@synthesize grossCalculatedTextField3;
@synthesize grossReportedTextField3;
@synthesize capacityTextField3;
@synthesize occupancyTextField3;
@synthesize occupancyNumberTextField3;
@synthesize totalEstimatedBuildingCostTextField3;
/*  
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];                
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *databaseFolderPath = [paths objectAtIndex:0]; 
    
    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], [appDele activeStudio].ID, [appDele activeStudio].name, [appDele activeStudio].iconName ]]; 	    
    

    
//    NSFileManager *manager = [NSFileManager defaultManager];    
//    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:databaseFolderPath];        
//    NSString *filename;
    OPSProjectSite* activeLiveProjectSite=[appDele activeProjectSite];
    
    NSString* filename = [NSString stringWithFormat:@"%@.sqlite",[activeLiveProjectSite name]];
    
//    while ((filename = [direnum nextObject] )) {
        
        
        
        
//        if ([filename hasSuffix:@".sqlite"]) { 
    
    
            
    NSString *filenameOnly=[activeLiveProjectSite name];//[NSMutableString stringWithString:filename];
//            [filenameOnly replaceCharactersInRange: [filenameOnly rangeOfString: @".sqlite"] withString: @""];            
            NSMutableDictionary *dictionary=nil;            
            NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
            dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];  
            NSString* pListSt=[dictionary objectForKey:filenameOnly];                        
            OPSProjectSite* projectSite=[[OPSProjectSite alloc] init:pListSt];
            
            NSString* dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:filename ] ];
            projectSite.dbPath=dbPath;
            [dbPath release];    

            
            
//            OPSProjectSite* projectSite=[appDele activeProjectSite];
            

            UINavigationController* nvc=self.navigationController;
            NSArray* aVC=[nvc viewControllers];
            uint numVC=[aVC count];
            LiveProjectListVC* liveProjectListVC=[aVC objectAtIndex:( numVC-2)];
            
//            [[self navigationController] popViewControllerAnimated:false]; 

            [liveProjectListVC openProjectToDisplay:projectSite];
            [projectSite release];
//        }
//    }
}
     
*/   


-(void) tryDownloadSite:(id)sender{                    
    LiveProjectListVC* liveProjectListVC=[[[self navigationController] viewControllers] objectAtIndex:( [[[self navigationController] viewControllers] count]-2)];

    [liveProjectListVC tryDownloadSite];
}



-(id) initWithViewController:(UIViewController*) viewController{
    self=[super initWithViewController:viewController];
    if (self){
        [self readFromSQL];
    }
    return self;
}

-(void) readFromSQL{    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
    OPSProjectSite* projectSite=[appDele activeProjectSite];    
    if ([projectSite dbPath]==Nil) {return;}    

//    ViewProductRep* productRep=[[self.viewModelVC aSelectedProductRep] lastObject];
//    if (![[productRep product] isKindOfClass:[Furn class]]){
//        return;
//    }    
//    Furn* furn=(Furn*) [productRep product];
    
    
    
   
    
    
    uint vPos=0;
    
    
    uint topBotInset=50;
    
    uint rowSpacing=10;
    uint pRow=0;
    
    
    uint labelHeight=20;
//    uint labelWidth=400;//210;
    uint labelTitleWidth=400;
    uint labelCellWidth=130;
    
    
//    uint uiLabelWidth=260; //130
    uint labelXSpacing=10;
    uint contentWidth=labelTitleWidth+(labelXSpacing+labelCellWidth)*4;
    //        uint contentHeight=labelHeight*numRow+rowSpacing*(numRow-1);
    
    
    
    CGRect rootFrame=self.vc.view.frame;
    rootFrame.origin=CGPointMake(0.0,0.0);
    
    UIView* tmpRootView=[[UIView alloc] initWithFrame:rootFrame];
    self.rootView=tmpRootView;
    [tmpRootView release];
    [self setView:self.rootView];
    
    
    
    
    UIView* tmpView=[[UIView alloc] initWithFrame:rootFrame];
    self.infoView=tmpView;        
    [tmpView release];     
    
    
    UIScrollView* scrollView=[[UIScrollView alloc] initWithFrame:self.rootView.frame];
    [scrollView setContentSize:self.infoView.frame.size];
    self.infoScrollView=scrollView;
    [scrollView release];
    
    [self.rootView addSubview:self.infoScrollView];
    
    [self.infoScrollView addSubview:self.infoView];
    
    
    
    uint hPos=0;
    CGSize topButtonSize=CGSizeMake(200, 35);  
    UIButton* button=nil;
    button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTitle:@"Tab Here for Live Report" forState:UIControlStateNormal];
    
    hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width;
    
    
    vPos=topBotInset;        
    button.frame = CGRectMake(hPos, vPos, topButtonSize.width, topButtonSize.height) ;        
    [button addTarget:self action:@selector(displayAttachWebView:) forControlEvents:UIControlEventTouchUpInside];
    [self.infoView addSubview:button];
    vPos+=topButtonSize.height;
    

    
    
    if (self.viewModelVC==nil){
        if ([self.vc isKindOfClass:[LocalProjectListVC class]]){
//            [self.downloadButton setHidden:true];
//            [self.viewButton setHidden:false];
            
            
            
            

            CGSize topButtonSize=CGSizeMake(200, 35);  
            UIButton* button=nil;
            button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            [button setTitle:@"View" forState:UIControlStateNormal];
            
            hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width-topButtonSize.width-labelXSpacing;
            
            
            vPos=topBotInset;        
            button.frame = CGRectMake(hPos, vPos, topButtonSize.width, topButtonSize.height) ;        
            [button addTarget:self action:@selector(viewSite:) forControlEvents:UIControlEventTouchUpInside];
            [self.infoView addSubview:button];
            vPos+=topButtonSize.height;
            
            
        }else{
            
            
//            
//            
//            CGSize topButtonSize=CGSizeMake(200, 35);  
//            UIButton* button=nil;
//            button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
//            [button setTitle:@"Download" forState:UIControlStateNormal];
//            
//            hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width-topButtonSize.width-labelXSpacing;
//            
//            
//            vPos=topBotInset;        
//            button.frame = CGRectMake(hPos, vPos, topButtonSize.width, topButtonSize.height) ;        
//            [button addTarget:self action:@selector(tryDownloadSite:) forControlEvents:UIControlEventTouchUpInside];
//            [self.infoView addSubview:button];
//            vPos+=topButtonSize.height;
//            
//            
//            [self.viewButton setHidden:true];
//            [self.downloadButton setHidden:false];
//            NSLog(@"hide.....");
        }
    }else{
//        [self.downloadButton setHidden:true];
//        [self.viewButton setHidden:true];
    }
    

    
    
    
    
    
//    sqlite3_stmt* infoStmt;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  
        
        
        
        sqlite3_stmt* siteInfoStmt;
        
        NSString* siteInfoSQLStr=[NSString stringWithFormat:@""
                                  @"SELECT siteInfo.siteArea"
                                  @" FROM siteinfo"                                ];
        const char* siteInfoSQLChar=[siteInfoSQLStr UTF8String];    
        double siteArea=0.0;
        if(sqlite3_prepare_v2(database, siteInfoSQLChar, -1, &siteInfoStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(siteInfoStmt) == SQLITE_ROW) {                     
                siteArea = sqlite3_column_double(siteInfoStmt,0);
                
            }
        }        
        
        
        
        
        sqlite3_stmt* spatialStructStmt;        
        NSString* spatialStructStr=[NSString stringWithFormat:@""
                                  @"SELECT spatialstructure.dateModified"
                                  @" FROM spatialstructure where spatialStructureType=\"Site\""                                ];
        
        const char* spatialStructSQLChar=[spatialStructStr UTF8String];    
//        uint siteDateModified=0.0;
        if(sqlite3_prepare_v2(database, spatialStructSQLChar, -1, &spatialStructStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(spatialStructStmt) == SQLITE_ROW) {                     
                self.dateModified = sqlite3_column_double(spatialStructStmt,0);
                
            }
        }        
        
        
        
        NSString* reportPart1=[NSString stringWithFormat:@""
                               @"SELECT bldgInfo.bldgNetArea"
                               @", bldgInfo.bldgGrossArea"
                               @", bldgInfo.reportedGrossArea"
                               @", BldgReport.spaceCapacity" //@", SUM(USCG_SpaceInfo.occupMax)"
                               @", BldgReport.spaceOccupancy"//@", SUM(USCG_SpaceInfo.occup)"                               
                               @", bldgInfo.occupancy"
                               @", estimateCost"
                               @", USCG_BldgInfo.useNsfFactor"
                               @", USCG_BldgInfo.nsfFactor"
                               @" FROM Bldg"
                               @" LEFT JOIN BldgInfo ON Bldg.ID=BldgInfo.bldgID"
                               @" LEFT JOIN USCG_BldgInfo ON Bldg.ID=USCG_BldgInfo.bldgID"
                               @" LEFT JOIN BldgReport ON Bldg.ID=BldgReport.bldgID"
                               @" LEFT JOIN Floor ON Bldg.ID=Floor.bldgID"
                               @" LEFT JOIN Space ON Floor.ID=Space.floorID"
                               @" LEFT JOIN USCG_SpaceInfo ON Space.ID=USCG_SpaceInfo.spaceID"                                 ];
        
        
        
        NSString* reportPart2=[NSString stringWithFormat:@""
                               @"WHERE siteID=%d",[[appDele activeProjectSite] ID]];
        
        
        
        NSString* reportPart2SiteBldg=[NSString stringWithFormat:@""
                                       @"WHERE "];
        
        uint pSelected=0;
        if (self.viewModelVC!=nil&&[self.viewModelVC aSelectedProductRep]!=nil){
            for (ViewProductRep* viewProductRep in [self.viewModelVC aSelectedProductRep]){
                OPSProduct* product=[viewProductRep product];  
                if ([product isKindOfClass:[Floor class]]){
                    RelAggregate* relAgg=(RelAggregate*) [product linkedRel];
                    
                    product=(OPSProduct*) [relAgg relating];
                }
                if (pSelected==0){
                    reportPart2SiteBldg=[NSString stringWithFormat:@"%@ (Bldg.ID=%d",reportPart2SiteBldg, product.ID];
                }else{                    
                    reportPart2SiteBldg=[NSString stringWithFormat:@"%@ OR Bldg.ID=%d",reportPart2SiteBldg, product.ID];
                }
                pSelected++;
            }
            reportPart2SiteBldg=[NSString stringWithFormat:@"%@)",reportPart2SiteBldg];
        }
        
        
        
        
        
        NSString* reportExisting=[NSString stringWithFormat:@""
                                  @"AND USCG_BldgInfo.existing=-1"];
        
        
        
        NSString* reportRenovation=[NSString stringWithFormat:@""
                                    @"AND USCG_BldgInfo.existing=1"];
        
        
        NSString* reportNew=[NSString stringWithFormat:@""
                             @"AND USCG_BldgInfo.existing=0"];
        
        
        NSString* reportPart4=[NSString stringWithFormat:@""
                               @"GROUP BY Bldg.ID" ];
        
        
        
        NSString* siteReportExisting=[NSString stringWithFormat:@""
                                      @"%@ %@ %@ %@",reportPart1,reportPart2,reportExisting,reportPart4];
        
        NSString* siteBldgReportExisting=[NSString stringWithFormat:@"%@ %@ %@ %@",reportPart1,reportPart2SiteBldg,reportExisting,reportPart4];
        
        
        
        
        if (self.viewModelVC!=nil&&[self.viewModelVC didSelectOneLevelCascadedProduct]){            
            [self readSiteBldgReportWithSiteReportSQLStr:siteBldgReportExisting];
        }else{
            [self readSiteBldgReportWithSiteReportSQLStr:siteReportExisting] ;//] resultDict:resultDict];    
        }
        
        
        
        uint numExisting=[self.tmpNumBldg intValue];        
        double sumExistingBldgNetArea=[self.tmpSumBldgNetArea doubleValue];
        double sumExistingBldgGrossArea=[self.tmpSumBldgGrossArea doubleValue];
        double sumExistingReportedGrossArea=[self.tmpSumReportedGrossArea doubleValue];
        double sumExistingCapicity=[self.tmpSumCapicity intValue];        
        double sumExistingOccupancyNumber=[self.tmpSumOccupancyNumber intValue];
        double sumExistingOccupancy=[self.tmpSumOccupancy intValue];
        double sumExistingEstimateCost=[self.tmpSumEstimateCost doubleValue];
        
        
        //        self.numBldgTextField0.text=[ProjectViewAppDelegate intToIntStr:numExisting];
        //        self.numBldgsNetCalculatedTextField0.text=[ProjectViewAppDelegate doubleToAreaStr:sumExistingBldgNetArea];
        //        self.grossCalculatedTextField0.text=[ProjectViewAppDelegate doubleToAreaStr:sumExistingBldgGrossArea];
        //        self.grossReportedTextField0.text=[ProjectViewAppDelegate doubleToAreaStr:sumExistingReportedGrossArea]; 
        //        self.capacityTextField0.text=[ProjectViewAppDelegate intToIntStr:sumExistingCapicity];
        //        self.occupancyTextField0.text=[ProjectViewAppDelegate intToIntStr:sumExistingOccupancy];                        
        //        self.occupancyNumberTextField0.text=[ProjectViewAppDelegate intToIntStr:sumExistingOccupancyNumber];
        //        self.totalEstimatedBuildingCostTextField0.text=[ProjectViewAppDelegate doubleToCurrenyStr:sumExistingEstimateCost];
        
        
        NSString* siteReportRennovation=[NSString stringWithFormat:@""
                                         @"%@ %@ %@ %@",reportPart1,reportPart2,reportRenovation,reportPart4];
        
        NSString* siteBldgReportRennovation=[NSString stringWithFormat:@"%@ %@ %@ %@",reportPart1,reportPart2SiteBldg,reportRenovation,reportPart4];
        
        
        
//        NSLog(@"\n\n%@",siteBldgReportRennovation);
        
        
        if (self.viewModelVC!=nil&&[self.viewModelVC didSelectOneLevelCascadedProduct]){            
            [self readSiteBldgReportWithSiteReportSQLStr:siteBldgReportRennovation];
        }else{
            [self readSiteBldgReportWithSiteReportSQLStr:siteReportRennovation] ;//] resultDict:resultDict];    
        }
        
        
        
        
        
        uint numRenovation=[self.tmpNumBldg intValue];        
        double sumRenovationBldgNetArea=[self.tmpSumBldgNetArea doubleValue];
        double sumRenovationBldgGrossArea=[self.tmpSumBldgGrossArea doubleValue];
        double sumRenovationReportedGrossArea=[self.tmpSumReportedGrossArea doubleValue];
        double sumRenovationCapicity=[self.tmpSumCapicity intValue];        
        double sumRenovationOccupancy=[self.tmpSumOccupancy intValue];        
        double sumRenovationOccupancyNumber=[self.tmpSumOccupancyNumber intValue];        
        double sumRenovationEstimateCost=[self.tmpSumEstimateCost doubleValue];
        
        
        
        
        //        self.numBldgTextField1.text=[ProjectViewAppDelegate intToIntStr:numRenovation];
        //        self.numBldgsNetCalculatedTextField1.text=[ProjectViewAppDelegate doubleToAreaStr:sumRenovationBldgNetArea];
        //        self.grossCalculatedTextField1.text=[ProjectViewAppDelegate doubleToAreaStr:sumRenovationBldgGrossArea];
        //        self.grossReportedTextField1.text=[ProjectViewAppDelegate doubleToAreaStr:sumRenovationReportedGrossArea]; 
        //        self.capacityTextField1.text=[ProjectViewAppDelegate intToIntStr:sumRenovationCapicity];
        //        self.occupancyTextField1.text=[ProjectViewAppDelegate intToIntStr:sumRenovationOccupancy];                        
        //        self.occupancyNumberTextField1.text=[ProjectViewAppDelegate intToIntStr:sumRenovationOccupancyNumber];
        //        self.totalEstimatedBuildingCostTextField1.text=[ProjectViewAppDelegate doubleToCurrenyStr:sumRenovationEstimateCost];
        //        
        
        
        
        
        
        
        
        
        
        NSString* siteReportNew=[NSString stringWithFormat:@""
                                 @"%@ %@ %@ %@",reportPart1,reportPart2,reportNew,reportPart4];
        
        
        NSString* siteBldgReportNew=[NSString stringWithFormat:@"%@ %@ %@ %@",reportPart1,reportPart2SiteBldg,reportNew,reportPart4];
        
        
        
        
//        NSLog(@"\n\n%@",siteBldgReportNew);
        
        
        if (self.viewModelVC!=nil&&[self.viewModelVC didSelectOneLevelCascadedProduct]){            
            [self readSiteBldgReportWithSiteReportSQLStr:siteBldgReportNew];
        }else{
            [self readSiteBldgReportWithSiteReportSQLStr:siteReportNew] ;//] resultDict:resultDict];    
        }
        
        
        
        
        
        //        [self readSiteBldgReportWithSiteReportSQLStr:siteReportNew] ;//] resultDict:resultDict];
        
        uint numNew=[self.tmpNumBldg intValue];        
        double sumNewBldgNetArea=[self.tmpSumBldgNetArea doubleValue];
        double sumNewBldgGrossArea=[self.tmpSumBldgGrossArea doubleValue];
        double sumNewReportedGrossArea=[self.tmpSumReportedGrossArea doubleValue];
        double sumNewCapicity=[self.tmpSumCapicity intValue];        
        double sumNewOccupancy=[self.tmpSumOccupancy intValue];        
        double sumNewOccupancyNumber=[self.tmpSumOccupancyNumber intValue];        
        double sumNewEstimateCost=[self.tmpSumEstimateCost doubleValue];
        
        
        
        //        self.numBldgTextField2.text=[ProjectViewAppDelegate intToIntStr: numNew];
        //        self.numBldgsNetCalculatedTextField2.text=[ProjectViewAppDelegate doubleToAreaStr:sumNewBldgNetArea];
        //        self.grossCalculatedTextField2.text=[ProjectViewAppDelegate doubleToAreaStr:sumNewBldgGrossArea];
        //        self.grossReportedTextField2.text=[ProjectViewAppDelegate doubleToAreaStr:sumNewReportedGrossArea]; 
        //        self.capacityTextField2.text=[ProjectViewAppDelegate intToIntStr: sumNewCapicity];
        //        self.occupancyTextField2.text=[ProjectViewAppDelegate intToIntStr: sumNewOccupancy];                        
        //        self.occupancyNumberTextField2.text=[ProjectViewAppDelegate intToIntStr: sumNewOccupancyNumber];
        //        self.totalEstimatedBuildingCostTextField2.text=[ProjectViewAppDelegate doubleToCurrenyStr:sumNewEstimateCost];
        //        
        //        
        
        
        
        
        
        
        
        uint numTotal=numExisting+numRenovation+numNew;        
        double sumTotalBldgNetArea=sumExistingBldgNetArea+ sumRenovationBldgNetArea+sumNewBldgNetArea;
        double sumTotalBldgGrossArea=sumExistingBldgGrossArea+sumRenovationBldgGrossArea+ sumNewBldgGrossArea;        
        double sumTotalReportedGrossArea=sumExistingReportedGrossArea+sumRenovationReportedGrossArea+sumNewReportedGrossArea;
        double sumTotalCapicity=sumExistingCapicity+sumRenovationCapicity+sumNewCapicity;        
        double sumTotalOccupancy=sumExistingOccupancy+sumRenovationOccupancy+sumNewOccupancy;        
        double sumTotalOccupancyNumber=sumExistingOccupancyNumber+sumRenovationOccupancyNumber+sumNewOccupancyNumber;        
        double sumTotalEstimateCost=sumExistingEstimateCost+sumRenovationEstimateCost+sumNewEstimateCost;
        
        
        
//        //        self.numBldgTextField3.text=[ProjectViewAppDelegate intToIntStr: numExisting+numRenovation+numNew];        
//        
//        //        self.numBldgsNetCalculatedTextField3.text=[ProjectViewAppDelegate doubleToAreaStr:( sumExistingBldgNetArea+ sumRenovationBldgNetArea+sumNewBldgNetArea) ];    
//        
//        //        self.grossCalculatedTextField3.text=[ProjectViewAppDelegate doubleToAreaStr:(sumExistingBldgGrossArea+sumRenovationBldgGrossArea+ sumNewBldgGrossArea) ];
//        
//        //        self.grossReportedTextField3.text=[ProjectViewAppDelegate doubleToAreaStr:(sumExistingReportedGrossArea+sumRenovationReportedGrossArea+sumNewReportedGrossArea) ]; 
//        //        self.capacityTextField3.text=[ProjectViewAppDelegate intToIntStr:sumExistingCapicity+sumRenovationCapicity+sumNewCapicity];
//        //        self.occupancyTextField3.text=[ProjectViewAppDelegate intToIntStr:sumExistingOccupancy+sumRenovationOccupancy+sumNewOccupancy];                        
//        //        self.occupancyNumberTextField3.text=[ProjectViewAppDelegate intToIntStr: sumExistingOccupancyNumber+sumRenovationOccupancyNumber+sumNewOccupancyNumber];
//        //        self.totalEstimatedBuildingCostTextField3.text= [ProjectViewAppDelegate doubleToCurrenyStr:(sumExistingEstimateCost+sumRenovationEstimateCost+sumNewEstimateCost) ];
        

        
        
        
        
        
        
        
        UILabel* labelTitle=nil;
        UILabel* labelCell=nil;
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);        
        
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];        
        
        labelTitle= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelTitleWidth, labelHeight) ];
        [self prepareLabel:labelTitle];        
        labelTitle.text = [NSString stringWithFormat: @"Site Area:"];
        [self.infoView addSubview:labelTitle];
        [labelTitle release];
        hPos+=labelTitleWidth;
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate doubleToAreaStr:siteArea];        
        self.siteAreaTextField=labelCell;        
        [self.infoView addSubview:self.siteAreaTextField];        
        [labelCell release];
        vPos+=labelHeight;
        pRow++;

        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);        
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];        
        hPos+=(labelTitleWidth+labelXSpacing);
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=@"Existing";               
        [self.infoView addSubview:labelCell];        
        [labelCell release];
        hPos+=labelCellWidth;
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=@"Renovation";               
        [self.infoView addSubview:labelCell];        
        [labelCell release];    
        hPos+=labelCellWidth;
       
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=@"New";               
        [self.infoView addSubview:labelCell];        
        [labelCell release];     
        hPos+=labelCellWidth;     
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=@"Total";               
        [self.infoView addSubview:labelCell];        
        [labelCell release];  
//        hPos+=labelCellWidth;    //Temporary commented caused of value stored never read warning         
        
        vPos+=labelHeight;        
        pRow++;
        
        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
        
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
        
        labelTitle= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelTitleWidth, labelHeight) ];
        [self prepareLabel:labelTitle];        
        labelTitle.text = [NSString stringWithFormat: @"Number of Buildings:"];
        [self.infoView addSubview:labelTitle];
        [labelTitle release];  
        hPos+=labelTitleWidth;
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate intToIntStr:numExisting];        
        self.numBldgTextField0=labelCell;        
        [self.infoView addSubview:self.numBldgTextField0];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
   
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate intToIntStr:numRenovation];        
        self.numBldgTextField1=labelCell;        
        [self.infoView addSubview:self.numBldgTextField1];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate intToIntStr:numNew];        
        self.numBldgTextField2=labelCell;        
        [self.infoView addSubview:self.numBldgTextField2];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate intToIntStr:numTotal];        
        self.numBldgTextField3=labelCell;        
        [self.infoView addSubview:self.numBldgTextField3];        
        [labelCell release];  
//        hPos+=labelCellWidth;  //Temporary commented caused of value stored never read warning   
        
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
        
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
        
        labelTitle= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelTitleWidth, labelHeight) ];
        [self prepareLabel:labelTitle];        
        labelTitle.text = [NSString stringWithFormat: @"Net Calculated (=modelled spaces):"];
        [self.infoView addSubview:labelTitle];
        [labelTitle release];  
        hPos+=labelTitleWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate doubleToAreaStr:sumExistingBldgNetArea];        
        self.numBldgsNetCalculatedTextField0=labelCell;        
        [self.infoView addSubview:self.numBldgsNetCalculatedTextField0];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  doubleToAreaStr:sumRenovationBldgNetArea];        
        self.numBldgsNetCalculatedTextField1=labelCell;        
        [self.infoView addSubview:self.numBldgsNetCalculatedTextField1];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate doubleToAreaStr:sumNewBldgNetArea];        
        self.numBldgsNetCalculatedTextField2=labelCell;        
        [self.infoView addSubview:self.numBldgsNetCalculatedTextField2];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  doubleToAreaStr:sumTotalBldgNetArea];        
        self.numBldgsNetCalculatedTextField3=labelCell;        
        [self.infoView addSubview:self.numBldgsNetCalculatedTextField3];        
        [labelCell release];  
//        hPos+=labelCellWidth;  //Temporary commented caused of value stored never read warning   
        
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
        
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
        
        labelTitle= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelTitleWidth, labelHeight) ];
        [self prepareLabel:labelTitle];        
        labelTitle.text = [NSString stringWithFormat: @"Gross Calculated (default calculated from slab area):"];
        [self.infoView addSubview:labelTitle];
        [labelTitle release];  
        hPos+=labelTitleWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate doubleToAreaStr:sumExistingBldgGrossArea];        
        self.grossCalculatedTextField0=labelCell;        
        [self.infoView addSubview:self.grossCalculatedTextField0];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  doubleToAreaStr:sumRenovationBldgGrossArea];        
        self.grossCalculatedTextField1=labelCell;        
        [self.infoView addSubview:self.grossCalculatedTextField1];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        
        labelCell.text=[ProjectViewAppDelegate doubleToAreaStr:sumNewBldgGrossArea];        
        self.grossCalculatedTextField2=labelCell;        
        [self.infoView addSubview:self.grossCalculatedTextField2];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  doubleToAreaStr:sumTotalBldgGrossArea];        
        self.grossCalculatedTextField3=labelCell;        
        [self.infoView addSubview:self.grossCalculatedTextField3];        
        [labelCell release];  
//        hPos+=labelCellWidth  //Temporary commented caused of value stored never read warning   ;
        
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
        
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
        
        labelTitle= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelTitleWidth, labelHeight) ];
        [self prepareLabel:labelTitle];        
        labelTitle.text = [NSString stringWithFormat: @"Gross Reported:"];
        [self.infoView addSubview:labelTitle];
        [labelTitle release];  
        hPos+=labelTitleWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate doubleToAreaStr:sumExistingReportedGrossArea];        
        self.grossReportedTextField0=labelCell;        
        [self.infoView addSubview:self.grossReportedTextField0];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  doubleToAreaStr:sumRenovationReportedGrossArea];        
        self.grossReportedTextField1=labelCell;        
        [self.infoView addSubview:self.grossReportedTextField1];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        
        labelCell.text=[ProjectViewAppDelegate doubleToAreaStr:sumNewReportedGrossArea];        
        self.grossReportedTextField2=labelCell;        
        [self.infoView addSubview:self.grossReportedTextField2];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  doubleToAreaStr:sumTotalReportedGrossArea];        
        self.grossReportedTextField3=labelCell;        
        [self.infoView addSubview:self.grossReportedTextField3];        
        [labelCell release];  
//        hPos+=labelCellWidth;  //Temporary commented caused of value stored never read warning   
        
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
        
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
        
        labelTitle= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelTitleWidth, labelHeight) ];
        [self prepareLabel:labelTitle];        
        labelTitle.text = [NSString stringWithFormat: @"Capacity (aggregated from Space Attributes):"];
        [self.infoView addSubview:labelTitle];
        [labelTitle release];  
        hPos+=labelTitleWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate intToIntStr:sumExistingCapicity];        
        self.capacityTextField0=labelCell;        
        [self.infoView addSubview:self.capacityTextField0];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  intToIntStr:sumRenovationCapicity];        
        self.capacityTextField1=labelCell;        
        [self.infoView addSubview:self.capacityTextField1];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        
        labelCell.text=[ProjectViewAppDelegate intToIntStr:sumNewCapicity];        
        self.capacityTextField2=labelCell;        
        [self.infoView addSubview:self.capacityTextField2];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  intToIntStr:sumTotalCapicity];        
        self.capacityTextField3=labelCell;        
        [self.infoView addSubview:self.capacityTextField3];        
        [labelCell release];  
//        hPos+=labelCellWidth;  //Temporary commented caused of value stored never read warning   
        
        vPos+=labelHeight;
        pRow++;
        
        

        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
        
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
        
        labelTitle= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelTitleWidth, labelHeight) ];
        [self prepareLabel:labelTitle];        
        labelTitle.text = [NSString stringWithFormat: @"Occupancy (aggregated from Space Attributes):"];
        [self.infoView addSubview:labelTitle];
        [labelTitle release];  
        hPos+=labelTitleWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate intToIntStr:sumExistingOccupancy];        
        self.occupancyTextField0=labelCell;        
        [self.infoView addSubview:self.occupancyTextField0];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  intToIntStr:sumRenovationOccupancy];        
        self.occupancyTextField1=labelCell;        
        [self.infoView addSubview:self.occupancyTextField1];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        
        labelCell.text=[ProjectViewAppDelegate intToIntStr:sumNewOccupancy];        
        self.occupancyTextField2=labelCell;        
        [self.infoView addSubview:self.occupancyTextField2];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  intToIntStr:sumTotalOccupancy];        
        self.occupancyTextField3=labelCell;        
        [self.infoView addSubview:self.occupancyTextField3];        
        [labelCell release];  
//        hPos+=labelCellWidth;  //Temporary commented caused of value stored never read warning   
        
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
        
        labelTitle= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelTitleWidth, labelHeight) ];
        [self prepareLabel:labelTitle];        
        labelTitle.text = [NSString stringWithFormat: @"Occupancy Number (Reported per Building):"];
        [self.infoView addSubview:labelTitle];
        [labelTitle release];  
        hPos+=labelTitleWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate intToIntStr:sumExistingOccupancyNumber];        
        self.occupancyNumberTextField0=labelCell;        
        [self.infoView addSubview:self.occupancyNumberTextField0];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  intToIntStr:sumRenovationOccupancyNumber];        
        self.occupancyNumberTextField1=labelCell;        
        [self.infoView addSubview:self.occupancyNumberTextField1];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        
        labelCell.text=[ProjectViewAppDelegate intToIntStr:sumNewOccupancyNumber];        
        self.occupancyNumberTextField2=labelCell;        
        [self.infoView addSubview:self.occupancyNumberTextField2];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  intToIntStr:sumTotalOccupancyNumber];        
        self.occupancyNumberTextField3=labelCell;        
        [self.infoView addSubview:self.occupancyNumberTextField3];        
        [labelCell release];  
//        hPos+=labelCellWidth;  //Temporary commented caused of value stored never read warning   
        
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
        
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
        labelTitle= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelTitleWidth, labelHeight) ];
        [self prepareLabel:labelTitle];        
        labelTitle.text = [NSString stringWithFormat: @"Total Estimated Building Cost:"];
        [self.infoView addSubview:labelTitle];
        [labelTitle release];  
        hPos+=labelTitleWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate doubleToCurrenyStr:sumExistingEstimateCost];        
        self.totalEstimatedBuildingCostTextField0=labelCell;        
        [self.infoView addSubview:self.totalEstimatedBuildingCostTextField0];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  doubleToCurrenyStr:sumRenovationEstimateCost];        
        self.totalEstimatedBuildingCostTextField1=labelCell;        
        [self.infoView addSubview:self.totalEstimatedBuildingCostTextField1];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        
        labelCell.text=[ProjectViewAppDelegate doubleToCurrenyStr:sumNewEstimateCost];        
        self.totalEstimatedBuildingCostTextField2=labelCell;        
        [self.infoView addSubview:self.totalEstimatedBuildingCostTextField2];        
        [labelCell release];  
        hPos+=labelCellWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
        labelCell.text=[ProjectViewAppDelegate  doubleToCurrenyStr:sumTotalEstimateCost];        
        self.totalEstimatedBuildingCostTextField3=labelCell;        
        [self.infoView addSubview:self.totalEstimatedBuildingCostTextField3];        
        [labelCell release];  
//        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
        
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        
        
     /*
        
        uint bottomCellWidth=240;
        
        
        //Local Database Date:

        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
        
//        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
        labelTitle= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelTitleWidth, labelHeight) ];
        [self prepareLabel:labelTitle];        
        labelTitle.text = [NSString stringWithFormat: @"Downloaded Scheme Last Modified Date:"];
        [self.infoView addSubview:labelTitle];
        [labelTitle release];  
        hPos+=labelTitleWidth;
        
        
        hPos+=labelXSpacing;        
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, bottomCellWidth, labelHeight) ];
        [self prepareUILabel:labelCell];        
//        labelCell.text=[ProjectViewAppDelegate doubleToCurrenyStr:sumExistingEstimateCost]; 
        
        int localModifiedDate=[projectSite getLocalDatabaseModifiedDate];
        NSString* dateStr=nil;
        if (localModifiedDate>=0){
            NSDate* date=nil;
    //        if (self.viewModelVC){
    //            
    //            date=[NSDate dateWithTimeIntervalSince1970: [projectSite getLocalDatabaseModifiedDate]] ;
    //        }else{
    //            if ([self.vc isKindOfClass:[LiveProjectListVC class]]){ 
    //                date=[NSDate dateWithTimeIntervalSince1970: [projectSite liveModifiedDate]];
    //            }else{
                    date=[NSDate dateWithTimeIntervalSince1970: localModifiedDate];
    //            }
    //        }

            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
            [dateFormatter setDateStyle:NSDateFormatterLongStyle];
            [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    //        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];

            
            dateStr=[dateFormatter stringFromDate:date];            
            [dateFormatter release];
        }else{
            dateStr=@"Site not yet downloaded";
        }
        
        
        labelCell.text=dateStr;//[NSString stringWithFormat:@"%@",date];
        
//        self.totalEstimatedBuildingCostTextField0=labelCell;        
//        [self.infoView addSubview:self.totalEstimatedBuildingCostTextField0];        
        [self.infoView addSubview:labelCell];
        
        
        
        [labelCell release];  
//        hPos+=labelCellWidth;
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        
        
        if ([self.vc isKindOfClass:[LiveProjectListVC class]]){ 
            //Local Database Date:
            
            vPos+=rowSpacing;
            hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
            
            //        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
            labelTitle= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelTitleWidth, labelHeight) ];
            [self prepareLabel:labelTitle];        
            labelTitle.text = [NSString stringWithFormat: @"Live Model Last Modified Date:"];
            [self.infoView addSubview:labelTitle];
            [labelTitle release];  
            hPos+=labelTitleWidth;
            
            
            hPos+=labelXSpacing;        
            labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, bottomCellWidth, labelHeight) ];
            [self prepareUILabel:labelCell];        
            //        labelCell.text=[ProjectViewAppDelegate doubleToCurrenyStr:sumExistingEstimateCost]; 
            
            int liveModifiedDate=[projectSite liveModifiedDate];
            NSString* dateStr=nil;
//            if (liveModifiedDate>=0){
                NSDate* date=nil;
//                //        if (self.viewModelVC){
//                //            
//                //            date=[NSDate dateWithTimeIntervalSince1970: [projectSite getLocalDatabaseModifiedDate]] ;
//                //        }else{
//                //            if ([self.vc isKindOfClass:[LiveProjectListVC class]]){ 
//                //                date=[NSDate dateWithTimeIntervalSince1970: [projectSite liveModifiedDate]];
//                //            }else{
                date=[NSDate dateWithTimeIntervalSince1970: liveModifiedDate];
                //            }
                //        }
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                //        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
                [dateFormatter setDateStyle:NSDateFormatterLongStyle];
                [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
                //        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
                
                
                dateStr=[dateFormatter stringFromDate:date];            
                [dateFormatter release];
//            }else{
//                dateStr=@"Site not yet downloaded";
//            }
            
            
            labelCell.text=dateStr;//[NSString stringWithFormat:@"%@",date];
            
            //        self.totalEstimatedBuildingCostTextField0=labelCell;        
            //        [self.infoView addSubview:self.totalEstimatedBuildingCostTextField0];        
            [self.infoView addSubview:labelCell];
            
            
            
            [labelCell release];  
//            hPos+=labelCellWidth;
            vPos+=labelHeight;
            pRow++;

        }
        
        
        */
//        getLocalDatabaseModifiedDate
        

        
        
        if (self.viewModelVC==nil){
            
            if ([self.vc isKindOfClass:[LiveProjectListVC class]]){                                    
                CGSize topButtonSize=CGSizeMake(200, 35);  
                UIButton* button=nil;                                                                    
                
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                [button setTitleColor:[UIColor orangeColor] forState:UIControlStateHighlighted];
                button.backgroundColor = [UIColor lightGrayColor];
                button.layer.borderColor = [UIColor blackColor].CGColor;
                button.layer.borderWidth = 0.5f;
                button.layer.cornerRadius = 10.0f;

                [button setTitle:@"Download" forState:UIControlStateNormal];
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width;
                
                
                vPos+=rowSpacing;        
                button.frame = CGRectMake(hPos, vPos, topButtonSize.width, topButtonSize.height) ;        
                [button addTarget:self action:@selector(tryDownloadSite:) forControlEvents:UIControlEventTouchUpInside];
                [self.infoView addSubview:button];
                vPos+=topButtonSize.height;
            
            }                                    
        }else{
            
            vPos+=rowSpacing*3;
            hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                
            //                hPos+=labelXSpacing;        
            //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
            labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
            [self prepareSelectedProductTitle:labelCell];        
            labelCell.text=@"Selected Element:";
            [self.infoView addSubview:labelCell];        
            [labelCell release];  
            //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
            
            vPos+=[self selectedProductTitleFontSize];
            pRow++;
            
            
            if ([self.viewModelVC aSelectedProductRep] && [[self.viewModelVC aSelectedProductRep] count]>0){
                for (uint pViewProductRep=0; pViewProductRep<[[self.viewModelVC aSelectedProductRep] count];pViewProductRep++){
                    ViewProductRep* viewProductRep=[[self.viewModelVC aSelectedProductRep] objectAtIndex:pViewProductRep];
                    OPSProduct* product=[viewProductRep product];
//                    if ([product isKindOfClass:[Floor class]]){
//                        Floor* floor=(Floor*) product;
//                        Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
//                        product=bldg;
//                    }
//                    
//                                
                    
                    
                    
                    vPos+=rowSpacing;
                    hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                
    //                hPos+=labelXSpacing;        
    //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
                    labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
                    [self prepareSelectedProductLabel:labelCell];                            
                    labelCell.text=[product productRepDisplayStr];
                    [self.infoView addSubview:labelCell];        
                    [labelCell release];  
                    //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
                    
                    vPos+=labelHeight;
                    pRow++;
                    
                }
            }else{
                

                OPSProduct* product=(OPSProduct*) [[self.viewModelVC model] root];
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                
                //                hPos+=labelXSpacing;        
                //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
                [self prepareSelectedProductLabel:labelCell];        
                labelCell.text=[product productRepDisplayStr];
//                labelCell.text=[NSString stringWithFormat:@"%d - %@",[product ID],[product name]];
                [self.infoView addSubview:labelCell];        
                [labelCell release];  
                //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
                
                vPos+=labelHeight;
                pRow++;
            }
        }
        


        
        
        
        uint contentHeight=vPos+topBotInset;     
        
        
        
        
        
        
        [self.infoScrollView setContentSize:CGSizeMake(rootFrame.size.width, contentHeight)];
        
        
    }
    sqlite3_close(database);
    
    
}






-(void)viewSite:(id)sender{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    OPSProjectSite* projectSite=[appDele activeProjectSite];
    

    LocalProjectListVC* vc=(LocalProjectListVC*) self.vc;
    
    
    
    uint appCurrentVersion=[appDele readAppCurrentVersion];
    if (projectSite.version<appCurrentVersion){
        [projectSite readVersionLagStr];
        if ([projectSite bNeedUpdate]){
            [vc alertSiteNeedUpdate:nil];
        }else{
            [vc openProjectToDisplay:projectSite];
        }
        
        
      
    }else{
        [vc openProjectToDisplay:projectSite];
    }
 
//    
//    [vc openProjectToDisplay:projectSite];
}


-(UIView*) viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return [self infoView];
}

-(void) dealloc{
    
    [tmpNumBldg release];tmpNumBldg=nil;
    [tmpSumBldgNetArea release]; tmpSumBldgNetArea=nil;
    [tmpSumBldgGrossArea release]; tmpSumBldgGrossArea=nil;
    [tmpSumReportedGrossArea release]; tmpSumReportedGrossArea=nil;
    [tmpSumCapicity release]; tmpSumCapicity=nil;
    [tmpSumOccupancy release]; tmpSumOccupancy=nil;
    [tmpSumOccupancyNumber release];tmpSumOccupancyNumber=nil;
    [tmpSumEstimateCost release];tmpSumEstimateCost=nil;
    
    
    [infoView release]; infoView=nil;
    [infoScrollView release];infoScrollView=nil;
    [liveSiteReportButton release];liveSiteReportButton=nil;    
    [siteAreaTextField release];siteAreaTextField=nil;
    
    
    [numBldgsNetCalculatedTextField0 release];numBldgsNetCalculatedTextField0=nil;
    [grossCalculatedTextField0 release];grossCalculatedTextField0=nil;
    [grossReportedTextField0 release];grossReportedTextField0=nil;
    [capacityTextField0 release]; capacityTextField0=nil;
    [occupancyTextField0 release]; occupancyTextField0=nil;
    [occupancyNumberTextField0 release]; occupancyNumberTextField0=nil;
    [totalEstimatedBuildingCostTextField0 release];totalEstimatedBuildingCostTextField0=nil;
    
    
    [numBldgsNetCalculatedTextField1 release];numBldgsNetCalculatedTextField1=nil;
    [grossCalculatedTextField1 release];grossCalculatedTextField1=nil;
    [grossReportedTextField1 release];grossReportedTextField1=nil;
    [capacityTextField1 release]; capacityTextField1=nil;
    [occupancyTextField1 release]; occupancyTextField1=nil;
    [occupancyNumberTextField1 release]; occupancyNumberTextField1=nil;
    [totalEstimatedBuildingCostTextField1 release];totalEstimatedBuildingCostTextField1=nil;    
    
    
    [numBldgsNetCalculatedTextField2 release];numBldgsNetCalculatedTextField2=nil;
    [grossCalculatedTextField2 release];grossCalculatedTextField2=nil;
    [grossReportedTextField2 release];grossReportedTextField2=nil;
    [capacityTextField2 release]; capacityTextField2=nil;
    [occupancyTextField2 release]; occupancyTextField2=nil;
    [occupancyNumberTextField2 release]; occupancyNumberTextField2=nil;
    [totalEstimatedBuildingCostTextField2 release];totalEstimatedBuildingCostTextField2=nil;
    
    
    [numBldgsNetCalculatedTextField3 release];numBldgsNetCalculatedTextField3=nil;
    [grossCalculatedTextField3 release];grossCalculatedTextField3=nil;
    [grossReportedTextField3 release];grossReportedTextField3=nil;
    [capacityTextField3 release]; capacityTextField3=nil;
    [occupancyTextField3 release]; occupancyTextField3=nil;
    [occupancyNumberTextField3 release]; occupancyNumberTextField3=nil;
    [totalEstimatedBuildingCostTextField3 release];totalEstimatedBuildingCostTextField3=nil;
    
    [super dealloc];
}



-(void)displayAttachWebView:(id)sender{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];        
    
    
    
    
    if (![appDele isLiveDataSourceAvailable]){
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];
        return;
    }
    uint err=[appDele reCheckUserNameAndPW];
    if (err!=0) {return;}
    
    
        
    OPSProjectSite* projectSite=[appDele activeProjectSite];
    
    
    
    
    if (self.viewModelVC){
        int liveModifiedDate=[projectSite liveModifiedDate];
        int localModifiedDate=[[appDele currentSite] dateModified];
        if (liveModifiedDate>localModifiedDate){        
            UIAlertView *liveAttachmentFromEditedSchemeMsg = [[UIAlertView alloc] initWithTitle:@"Scheme has been edited since download. Please download this scheme later for more accurate live info" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [liveAttachmentFromEditedSchemeMsg show];
            [liveAttachmentFromEditedSchemeMsg release];        
            
        }
    }
    
    
    
    
    NSString* urlStr=nil;
    if (self.viewModelVC!=nil && [self.viewModelVC didSelectOneLevelCascadedProduct]){
        ViewProductRep* viewProductRep=[[self.viewModelVC aSelectedProductRep] objectAtIndex:0];
        Bldg* bldg=nil;
        OPSProduct* product=[viewProductRep product];  
        if ([product isKindOfClass:[Floor class]]){
            RelAggregate* relAgg=(RelAggregate*) [product linkedRel];            
            bldg=(Bldg*) [relAgg relating];
        }                    
        urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS/report/reportBldg.php?sysID=%d&projectID=%d&siteID=%d&bldgID=%d&u=%@&p=%@",[appDele activeStudioID],[projectSite projectID ], [projectSite ID],[bldg ID], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]]];
        
    }else{
        urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS/report/reportSite.php?sysID=%d&projectID=%d&siteID=%d&u=%@&p=%@",[appDele activeStudioID],[projectSite projectID ], [projectSite ID],[ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]]];
    }
    //    NSString *mystring =urlStr;// @"Hello World!";
    //    NSString *regex = @"http://.*";
    //    NSPredicate *regextest = [NSPredicate
    //                              predicateWithFormat:@"SELF MATCHES %@", regex];
    //    
    //    if ([regextest evaluateWithObject:mystring] == YES) {
    //        NSLog(@"Match!");
    //    } else {
    //        NSLog(@"No match!");
    //        urlStr=[NSString stringWithFormat:@"http://%@",urlStr];
    //    }
//    NSLog(@"%@",urlStr);
    
    if ([self.vc isKindOfClass:[ViewModelVC class]]){
        [appDele displayInfoWebView:urlStr];
    }else{           
        [appDele pushInfoWebViewToNavCntr:urlStr navCntr:[self.vc navigationController]];
    }
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil viewController:(UIViewController *)viewController
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil viewController:viewController];
    if (self) {
        //        if (self.viewModelVC!=nil){
        //            [self.viewButton setHidden:true];
        //            [self.downloadButton setHidden:true];
        //        }else{
        //            
        ////            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        ////            UINavigationController* navCntr=viewController.navigationController;
        ////            NSArray* aVC=navCntr.viewControllers;
        //            if ([viewController isKindOfClass:[LocalProjectListVC class]]){
        //                [self.downloadButton setHidden:true];
        //                [self.viewButton setHidden:false];
        //            }else{
        //                [self.viewButton setHidden:true];
        //                [self.downloadButton setHidden:false];
        //                NSLog(@"hide.....");
        //            }
        ////            UIViewController* prevVC=[aVC objectAtIndex:[aVC count]-2];
        ////            if ([prevVC isKindOfClass: [LocalProjectListVC class]]){
        //                
        ////            }
        //        }
        //        self.viewModelVC=viewModelVC;
        // Custom initialization
    }
    return self;
}

-(void) readSiteBldgReportWithSiteReportSQLStr:(NSString*) siteReportSQLStr{// resultDict:(NSMutableDictionary*)resultDict{        
    
    
    
    
    
    self.tmpNumBldg =  [NSNumber numberWithInt:0 ];
    self.tmpSumBldgNetArea = [NSNumber numberWithDouble:0.0f];
    self.tmpSumBldgGrossArea = [NSNumber numberWithDouble:0.0f];
    self.tmpSumReportedGrossArea = [NSNumber numberWithDouble:0.0f];
    self.tmpSumCapicity=[NSNumber numberWithInt:0];    
    self.tmpSumOccupancyNumber=[NSNumber numberWithInt:0];
    self.tmpSumOccupancy=[NSNumber numberWithInt:0];
    self.tmpSumEstimateCost=[NSNumber numberWithDouble:0.0f];
    //    
    
    
    
    //-(void) readSiteBldgReportWithSiteReportSQLStr:(NSString*) siteReportSQLStr numBldg:(NSNumber*)pNumBldg bldgNetArea:(NSNumber*) pBldgNetArea bldgGrossArea:(NSNumber*)pBldgGrossArea reportedGrossArea:(NSNumber*) pReportedGrossArea occupancyNumber:(NSNumber*)pOccupancyNumber capicity:(NSNumber*)pCapicity occupancy:(NSNumber*) pOccupancy estimateCost:(NSNumber*)pEstimateCost{
    
    sqlite3_stmt* infoStmt;
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    bool isImperial=[[[appDele currentSite] model] isImperial];
    const char* siteReportSQLChar=[siteReportSQLStr UTF8String];              
    //    NSLog (@"%@",siteReportSQLStr);
    uint sumBldg=0;
    
    double sumBldgNetArea=0.0;
    double sumBldgGrossArea=0.0;
    double sumReportedGrossArea=0.0;
    double sumOccupancyNumber=0.0;
    double sumCapicity=0.0;
    double sumOccupancy=0.0;
    double sumEstimateCost=0.0;
    
    if(sqlite3_prepare_v2(database, siteReportSQLChar, -1, &infoStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(infoStmt) == SQLITE_ROW) {       
            double bldgNetArea = sqlite3_column_double(infoStmt,0);
            //            if (isImperial) {bldgNetArea=[ProjectViewAppDelegate sqMeterToSqFeet:bldgNetArea];}
            double bldgGrossArea=sqlite3_column_double(infoStmt,1);
            //            if (isImperial) {bldgGrossArea=[ProjectViewAppDelegate sqMeterToSqFeet:bldgGrossArea];}
            double reportedGrossArea=sqlite3_column_double(infoStmt,2);
            //            if (isImperial) {reportedGrossArea=[ProjectViewAppDelegate sqMeterToSqFeet:reportedGrossArea];}
            
            
            
            double capicity=0.0;
            NSString* capacityStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:3];                
            if (capacityStr!=nil && (![capacityStr isEqualToString:@""])){
                capicity=[capacityStr doubleValue];
            }                                                
            
            double occupancy=0.0;
            NSString* occupancyTextStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:4];                 
            if (occupancyTextStr!=nil && (![occupancyTextStr isEqualToString:@""])){
                occupancy=[occupancyTextStr doubleValue];
                
            }
            
            double occupancyNumber=sqlite3_column_double(infoStmt,5);
            //                self.occupancyTextField0.text=(     occupancyTextStr==nil ||
            //                                               ( [occupancyTextStr isEqualToString:@""] 
            //                                                )?@"0":occupancyTextStr);   
            
            //                self.occupancyNumberTextField0.text =[NSString stringWithFormat:@"%.3f",occupancyNumber];
            
            double estimateCost=sqlite3_column_double(infoStmt,6);
            
            
            int useNsfFactor=sqlite3_column_int(infoStmt,7);
            double nsfFactor=sqlite3_column_double(infoStmt, 8);
            
            
            if (useNsfFactor==0){
                bldgGrossArea*=nsfFactor;
            }else{
                bldgGrossArea=bldgNetArea*nsfFactor;
            }
            
            
            
            sumBldg++;
            
            sumBldgNetArea+=bldgNetArea;
            sumBldgGrossArea+=bldgGrossArea;
            sumReportedGrossArea+=reportedGrossArea;
            
            
            sumCapicity+=capicity;
            sumOccupancy+=occupancy;
            
            sumOccupancyNumber+=occupancyNumber;                
            sumEstimateCost+=estimateCost;

            
        }
    }
    
    //    [resultDict setValue:[NSNumber numberWithInt:sumBldg ] forKey:@"NumBldg"];
    
    
    
    self.tmpNumBldg =  [NSNumber numberWithInt:sumBldg ];
    self.tmpSumBldgNetArea = [NSNumber numberWithDouble:sumBldgNetArea];
    self.tmpSumBldgGrossArea = [NSNumber numberWithDouble:sumBldgGrossArea];
    self.tmpSumReportedGrossArea = [NSNumber numberWithDouble:sumReportedGrossArea];
    self.tmpSumCapicity=[NSNumber numberWithInt:sumCapicity];    
    self.tmpSumOccupancyNumber=[NSNumber numberWithInt:sumOccupancyNumber];
    self.tmpSumOccupancy=[NSNumber numberWithInt:sumOccupancy];
    self.tmpSumEstimateCost=[NSNumber numberWithDouble:sumEstimateCost];
    //    
    //                          
    //    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.viewModelVC==nil){
        if ([self.vc isKindOfClass:[LocalProjectListVC class]]){
            [self.downloadButton setHidden:true];
            [self.viewButton setHidden:false];
        }else{
            [self.viewButton setHidden:true];
            [self.downloadButton setHidden:false];
            NSLog(@"hide.....");
        }
    }else{
        [self.downloadButton setHidden:true];
        [self.viewButton setHidden:true];
    }
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
    OPSProjectSite* projectSite=[appDele activeProjectSite];    
    if ([projectSite dbPath]==Nil) {return;}    
    
    sqlite3_stmt* siteInfoStmt;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  
        
        
        NSString* siteInfoSQLStr=[NSString stringWithFormat:@""
                                  @"SELECT siteInfo.siteArea"
                                  @" FROM siteinfo"                                ];
        const char* siteInfoSQLChar=[siteInfoSQLStr UTF8String];    
        double siteArea=0.0;
        if(sqlite3_prepare_v2(database, siteInfoSQLChar, -1, &siteInfoStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(siteInfoStmt) == SQLITE_ROW) {                     
                siteArea = sqlite3_column_double(siteInfoStmt,0);
                
            }
        }        
        
        self.siteAreaTextField.text=[ProjectViewAppDelegate doubleToAreaStr:siteArea];
        
        NSString* reportPart1=[NSString stringWithFormat:@""
                               @"SELECT bldgInfo.bldgNetArea"
                               @", bldgInfo.bldgGrossArea"
                               @", bldgInfo.reportedGrossArea"
                               @", BldgReport.spaceCapacity" //@", SUM(USCG_SpaceInfo.occupMax)"
                               @", BldgReport.spaceOccupancy"//@", SUM(USCG_SpaceInfo.occup)"                               
                               @", bldgInfo.occupancy"
                               @", estimateCost"
                               @", USCG_BldgInfo.useNsfFactor"
                               @", USCG_BldgInfo.nsfFactor"
                               @" FROM Bldg"
                               @" LEFT JOIN BldgInfo ON Bldg.ID=BldgInfo.bldgID"
                               @" LEFT JOIN USCG_BldgInfo ON Bldg.ID=USCG_BldgInfo.bldgID"
                               @" LEFT JOIN BldgReport ON Bldg.ID=BldgReport.bldgID"
                               @" LEFT JOIN Floor ON Bldg.ID=Floor.bldgID"
                               @" LEFT JOIN Space ON Floor.ID=Space.floorID"
                               @" LEFT JOIN USCG_SpaceInfo ON Space.ID=USCG_SpaceInfo.spaceID"                                 ];
        
        
        
        NSString* reportPart2=[NSString stringWithFormat:@""
                               @"WHERE siteID=%d",[[appDele activeProjectSite] ID]];
        
        
        
        NSString* reportPart2SiteBldg=[NSString stringWithFormat:@""
                                       @"WHERE "];
        
        uint pSelected=0;
        if (self.viewModelVC!=nil&&[self.viewModelVC aSelectedProductRep]!=nil){
            for (ViewProductRep* viewProductRep in [self.viewModelVC aSelectedProductRep]){
                OPSProduct* product=[viewProductRep product];  
                if ([product isKindOfClass:[Floor class]]){
                    RelAggregate* relAgg=(RelAggregate*) [product linkedRel];
                    
                    product=(OPSProduct*) [relAgg relating];
                }
                if (pSelected==0){
                    reportPart2SiteBldg=[NSString stringWithFormat:@"%@ (Bldg.ID=%d",reportPart2SiteBldg, product.ID];
                }else{                    
                    reportPart2SiteBldg=[NSString stringWithFormat:@"%@ OR Bldg.ID=%d",reportPart2SiteBldg, product.ID];
                }
                pSelected++;
            }
            reportPart2SiteBldg=[NSString stringWithFormat:@"%@)",reportPart2SiteBldg];
        }
        
        
        
        
        
        NSString* reportExisting=[NSString stringWithFormat:@""
                                  @"AND USCG_BldgInfo.existing=-1"];
        
        
        
        NSString* reportRenovation=[NSString stringWithFormat:@""
                                    @"AND USCG_BldgInfo.existing=0"];
        
        
        NSString* reportNew=[NSString stringWithFormat:@""
                             @"AND USCG_BldgInfo.existing=1"];
        
        
        NSString* reportPart4=[NSString stringWithFormat:@""
                               @"GROUP BY Bldg.ID" ];
        
        
        
        NSString* siteReportExisting=[NSString stringWithFormat:@""
                                      @"%@ %@ %@ %@",reportPart1,reportPart2,reportExisting,reportPart4];
        
        NSString* siteBldgReportExisting=[NSString stringWithFormat:@"%@ %@ %@ %@",reportPart1,reportPart2SiteBldg,reportExisting,reportPart4];
        
        if (self.viewModelVC!=nil&&[self.viewModelVC didSelectOneLevelCascadedProduct]){            
            [self readSiteBldgReportWithSiteReportSQLStr:siteBldgReportExisting];
        }else{
            [self readSiteBldgReportWithSiteReportSQLStr:siteReportExisting] ;//] resultDict:resultDict];    
        }
        
//        NSLog(@"\n\n%@",siteBldgReportExisting);
        
        
        
        uint numExisting=[self.tmpNumBldg intValue];        
        double sumExistingBldgNetArea=[self.tmpSumBldgNetArea doubleValue];
        double sumExistingBldgGrossArea=[self.tmpSumBldgGrossArea doubleValue];
        double sumExistingReportedGrossArea=[self.tmpSumReportedGrossArea doubleValue];
        double sumExistingCapicity=[self.tmpSumCapicity intValue];        
        double sumExistingOccupancyNumber=[self.tmpSumOccupancyNumber intValue];
        double sumExistingOccupancy=[self.tmpSumOccupancy intValue];
        double sumExistingEstimateCost=[self.tmpSumEstimateCost doubleValue];
        
        
//        self.numBldgTextField0.text=[ProjectViewAppDelegate intToIntStr:numExisting];
//        self.numBldgsNetCalculatedTextField0.text=[ProjectViewAppDelegate doubleToAreaStr:sumExistingBldgNetArea];
//        self.grossCalculatedTextField0.text=[ProjectViewAppDelegate doubleToAreaStr:sumExistingBldgGrossArea];
//        self.grossReportedTextField0.text=[ProjectViewAppDelegate doubleToAreaStr:sumExistingReportedGrossArea]; 
//        self.capacityTextField0.text=[ProjectViewAppDelegate intToIntStr:sumExistingCapicity];
//        self.occupancyTextField0.text=[ProjectViewAppDelegate intToIntStr:sumExistingOccupancy];                        
//        self.occupancyNumberTextField0.text=[ProjectViewAppDelegate intToIntStr:sumExistingOccupancyNumber];
//        self.totalEstimatedBuildingCostTextField0.text=[ProjectViewAppDelegate doubleToCurrenyStr:sumExistingEstimateCost];
//        
//        
        
        
        
        NSString* siteReportRennovation=[NSString stringWithFormat:@""
                                         @"%@ %@ %@ %@",reportPart1,reportPart2,reportRenovation,reportPart4];
        
        NSString* siteBldgReportRennovation=[NSString stringWithFormat:@"%@ %@ %@ %@",reportPart1,reportPart2SiteBldg,reportRenovation,reportPart4];
        
        
        
//        NSLog(@"\n\n%@",siteBldgReportRennovation);
        
        
        if (self.viewModelVC!=nil&&[self.viewModelVC didSelectOneLevelCascadedProduct]){            
            [self readSiteBldgReportWithSiteReportSQLStr:siteBldgReportRennovation];
        }else{
            [self readSiteBldgReportWithSiteReportSQLStr:siteReportRennovation] ;//] resultDict:resultDict];    
        }
        
        
        
        
        uint numRenovation=[self.tmpNumBldg intValue];        
        double sumRenovationBldgNetArea=[self.tmpSumBldgNetArea doubleValue];
        double sumRenovationBldgGrossArea=[self.tmpSumBldgGrossArea doubleValue];
        double sumRenovationReportedGrossArea=[self.tmpSumReportedGrossArea doubleValue];
        double sumRenovationCapicity=[self.tmpSumCapicity intValue];        
        double sumRenovationOccupancy=[self.tmpSumOccupancy intValue];        
        double sumRenovationOccupancyNumber=[self.tmpSumOccupancyNumber intValue];        
        double sumRenovationEstimateCost=[self.tmpSumEstimateCost doubleValue];
        
        
        self.numBldgTextField1.text=[ProjectViewAppDelegate intToIntStr:numRenovation];
        self.numBldgsNetCalculatedTextField1.text=[ProjectViewAppDelegate doubleToAreaStr:sumRenovationBldgNetArea];
        self.grossCalculatedTextField1.text=[ProjectViewAppDelegate doubleToAreaStr:sumRenovationBldgGrossArea];
        self.grossReportedTextField1.text=[ProjectViewAppDelegate doubleToAreaStr:sumRenovationReportedGrossArea]; 
        self.capacityTextField1.text=[ProjectViewAppDelegate intToIntStr:sumRenovationCapicity];
        self.occupancyTextField1.text=[ProjectViewAppDelegate intToIntStr:sumRenovationOccupancy];                        
        self.occupancyNumberTextField1.text=[ProjectViewAppDelegate intToIntStr:sumRenovationOccupancyNumber];
        self.totalEstimatedBuildingCostTextField1.text=[ProjectViewAppDelegate doubleToCurrenyStr:sumRenovationEstimateCost];
        
        
        
        
        
        
        NSString* siteReportNew=[NSString stringWithFormat:@""
                                 @"%@ %@ %@ %@",reportPart1,reportPart2,reportNew,reportPart4];
        
        
        NSString* siteBldgReportNew=[NSString stringWithFormat:@"%@ %@ %@ %@",reportPart1,reportPart2SiteBldg,reportNew,reportPart4];
        
        
        
        
//        NSLog(@"\n\n%@",siteBldgReportNew);
        
        
        if (self.viewModelVC!=nil&&[self.viewModelVC didSelectOneLevelCascadedProduct]){            
            [self readSiteBldgReportWithSiteReportSQLStr:siteBldgReportNew];
        }else{
            [self readSiteBldgReportWithSiteReportSQLStr:siteReportNew] ;//] resultDict:resultDict];    
        }
        
        
        
        
        
        //        [self readSiteBldgReportWithSiteReportSQLStr:siteReportNew] ;//] resultDict:resultDict];
        
        uint numNew=[self.tmpNumBldg intValue];        
        double sumNewBldgNetArea=[self.tmpSumBldgNetArea doubleValue];
        double sumNewReportedGrossArea=[self.tmpSumBldgGrossArea doubleValue];
        double sumNewBldgGrossArea=[self.tmpSumReportedGrossArea doubleValue];
        double sumNewCapicity=[self.tmpSumCapicity intValue];        
        double sumNewOccupancy=[self.tmpSumOccupancy intValue];        
        double sumNewOccupancyNumber=[self.tmpSumOccupancyNumber intValue];        
        double sumNewEstimateCost=[self.tmpSumEstimateCost doubleValue];
        
        
        
        self.numBldgTextField2.text=[ProjectViewAppDelegate intToIntStr: numNew];
        self.numBldgsNetCalculatedTextField2.text=[ProjectViewAppDelegate doubleToAreaStr:sumNewBldgNetArea];
        self.grossCalculatedTextField2.text=[ProjectViewAppDelegate doubleToAreaStr:sumNewBldgGrossArea];
        self.grossReportedTextField2.text=[ProjectViewAppDelegate doubleToAreaStr:sumNewReportedGrossArea]; 
        self.capacityTextField2.text=[ProjectViewAppDelegate intToIntStr: sumNewCapicity];
        self.occupancyTextField2.text=[ProjectViewAppDelegate intToIntStr: sumNewOccupancy];                        
        self.occupancyNumberTextField2.text=[ProjectViewAppDelegate intToIntStr: sumNewOccupancyNumber];
        self.totalEstimatedBuildingCostTextField2.text=[ProjectViewAppDelegate doubleToCurrenyStr:sumNewEstimateCost];
        
        
        
        
        self.numBldgTextField3.text=[ProjectViewAppDelegate intToIntStr: numExisting+numRenovation+numNew];
        
        
        self.numBldgsNetCalculatedTextField3.text=[ProjectViewAppDelegate doubleToAreaStr:( sumExistingBldgNetArea+ sumRenovationBldgNetArea+sumNewBldgNetArea) ];        
        self.grossCalculatedTextField3.text=[ProjectViewAppDelegate doubleToAreaStr:(sumExistingBldgGrossArea+sumRenovationBldgGrossArea+ sumNewBldgGrossArea) ];
        self.grossReportedTextField3.text=[ProjectViewAppDelegate doubleToAreaStr:(sumExistingReportedGrossArea+sumRenovationReportedGrossArea+sumNewReportedGrossArea) ]; 
        self.capacityTextField3.text=[ProjectViewAppDelegate intToIntStr:sumExistingCapicity+sumRenovationCapicity+sumNewCapicity];
        self.occupancyTextField3.text=[ProjectViewAppDelegate intToIntStr:sumExistingOccupancy+sumRenovationOccupancy+sumNewOccupancy];                        
        self.occupancyNumberTextField3.text=[ProjectViewAppDelegate intToIntStr: sumExistingOccupancyNumber+sumRenovationOccupancyNumber+sumNewOccupancyNumber];
        
        self.totalEstimatedBuildingCostTextField3.text= [ProjectViewAppDelegate doubleToCurrenyStr:(sumExistingEstimateCost+sumRenovationEstimateCost+sumNewEstimateCost) ];
        
        
        
    }
    sqlite3_close(database);
    
    if (self.viewModelVC!=nil&&[self.viewModelVC aSelectedProductRep] !=nil){
        if ([[self.viewModelVC aSelectedProductRep] count]>1){
            self.liveSiteReportButton.hidden=true;
        }else{
            self.liveSiteReportButton.hidden=false;
        }
    }else{
        self.liveSiteReportButton.hidden=false;        
    }
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end


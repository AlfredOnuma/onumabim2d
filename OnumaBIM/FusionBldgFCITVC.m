//
//  FusionBldgFCITVC.m
//  ProjectView
//
//  Created by onuma on 14/10/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "FusionBldgFCITVC.h"

#import "FusionBldgFCIColor.h"
#import "ProjectViewAppDelegate.h"
#import "BIMPlanColorCatNVC.h"
#import "Site.h"
#import "DisplayModelUI.h"
#import "ViewModelVC.h"
#import "ViewModelToolbar.h"
#import "Bldg.h"


@interface FusionBldgFCITVC ()

@end

@implementation FusionBldgFCITVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}








- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    //#warning Incomplete method implementation.
    //    // Return the number of rows in the section.
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    Site* site=[appDele currentSite];
    //    uint numRow=0;
    //    if (site.aDepartment){
    //        numRow=[site.aDepartment count];
    //    }
    //    if (numRow<1) {numRow=1;}
    //    return numRow;
    return 5;
}



-(void)addToSelection:(NSIndexPath *) indexPath{
    uint row=[indexPath row];
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    Site* site=[appDele currentSite];
    //    Department* department=[site.aDepartment objectAtIndex:row];
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    
    
    for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
            if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                Floor* floor=(Floor*)[viewProductRep product];
                Bldg* bldg=(Bldg*) [[floor linkedRel] relating];

                switch (row){
                    case 0: { //==0
                        if (bldg.fusionBldgFCI==0){
                            if (!viewProductRep.selected){
                                [viewModelVC addAProductRepToSelection:viewProductRep];
                            }
                        }
                    }
                        break;
                    case 1:{ //<=10
                        if (bldg.fusionBldgFCI>0 && bldg.fusionBldgFCI<=10){
                            if (!viewProductRep.selected){
                                [viewModelVC addAProductRepToSelection:viewProductRep];
                            }
                        }
                    }
                        break;
                    case 2:{ //<=50
                        
                        if (bldg.fusionBldgFCI>10 && bldg.fusionBldgFCI<=50){
                            if (!viewProductRep.selected){
                                [viewModelVC addAProductRepToSelection:viewProductRep];
                            }
                        }
                    }
                        break;
                    case 3:{ //<=90
                        if (bldg.fusionBldgFCI>50 && bldg.fusionBldgFCI<=90){
                            if (!viewProductRep.selected){
                                [viewModelVC addAProductRepToSelection:viewProductRep];
                            }
                        }
                    }
                        break;
                    case 4:{ //>90
                        if (bldg.fusionBldgFCI>90){
                            if (!viewProductRep.selected){
                                [viewModelVC addAProductRepToSelection:viewProductRep];
                            }
                        }
                    }
                        break;
                    default:{
                        
                    }
                        break;
                }
                

                
            }
        }
    }
    
    
}


-(void)removeFromSelection:(NSIndexPath *) indexPath{
    
    uint row=[indexPath row];
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    
    
    for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
            if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                Floor* floor=(Floor*)[viewProductRep product];
                Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                
                switch (row){
                    case 0: { //==0
                        if (bldg.fusionBldgFCI==0){
                            if (viewProductRep.selected){
                                [viewModelVC removeAProductRepFromSelection:viewProductRep];
                            }
                        }
                    }
                        break;
                    case 1:{ //<=10
                        if (bldg.fusionBldgFCI>0 && bldg.fusionBldgFCI<=10){
                            if (viewProductRep.selected){
                                [viewModelVC removeAProductRepFromSelection:viewProductRep];
                            }
                        }
                    }
                        break;
                    case 2:{ //<=50
                        
                        if (bldg.fusionBldgFCI>10 && bldg.fusionBldgFCI<=50){
                            if (viewProductRep.selected){
                                [viewModelVC removeAProductRepFromSelection:viewProductRep];
                            }
                        }
                    }
                        break;
                    case 3:{ //<=90
                        if (bldg.fusionBldgFCI>50 && bldg.fusionBldgFCI<=90){
                            if (viewProductRep.selected){
                                [viewModelVC removeAProductRepFromSelection:viewProductRep];
                            }
                        }
                    }
                        break;
                    case 4:{ //>90
                        if (bldg.fusionBldgFCI>90){
                            if (viewProductRep.selected){
                                [viewModelVC removeAProductRepFromSelection:viewProductRep];
                            }
                        }
                    }
                        break;
                    default:{
                        
                    }
                        break;
                }
                
                
                

            }
        }
    }
    
    
    
}
@end

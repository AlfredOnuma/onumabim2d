//
//  MapTypeListTVC.h
//  ProjectView
//
//  Created by Alfred Man on 3/20/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MapTypeListDelegate 
@end
@class ViewModelVC;
@interface MapTypeListTVC : UITableViewController{
    id <MapTypeListDelegate> delegate;
}

@property (assign) id <MapTypeListDelegate> delegate;

- (id) init: (ViewModelVC*) viewModelVC;
@end

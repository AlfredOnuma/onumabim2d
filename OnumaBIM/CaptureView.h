//
//  CaptureView.h
//  ProjectView
//
//  Created by Alfred Man on 3/14/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>


@interface CaptureView : UIView {
@private
    UIImage *_imageCapture;
    CGRect _captureFrame;
}

@property(nonatomic, retain) UIImage *imageCapture;

@property(nonatomic, assign) CGRect captureFrame;
// Init
- (id)initWithView:(UIView *)view captureFrame:(CGRect)captureFrame;

@end
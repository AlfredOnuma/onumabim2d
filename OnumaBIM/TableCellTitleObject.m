//
//  TableCellTitleObject.m
//  ProjectView
//
//  Created by Alfred Man on 1/3/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import "TableCellTitleObject.h"


@implementation TableCellTitleObject

@synthesize ID;
@synthesize name;
@synthesize iconName;
@synthesize shared;


-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    TableCellTitleObject *copiedTableCellTitleObject = [[TableCellTitleObject allocWithZone:zone] init:ID name:name shared:shared  iconName:iconName];
//    if (dbPath){
//        copiedProjectSite.dbPath=dbPath;
//    }
    //    another.obj = [obj copyWithZone: zone];
    
    return copiedTableCellTitleObject;
}




- (id) init:(NSUInteger)_ID name:(NSString*)_name shared:(NSUInteger)_shared iconName:(NSString*)_iconName{
    self=[super init];
    if (self){
        ID=_ID;
        [name release];
        [_name retain];
        name=_name;
        shared=_shared;
        [iconName release];
        [_iconName retain];
        iconName=_iconName;
    }
    return self;
}
- (void)dealloc{
    [name release];
    [iconName release];    
    [super dealloc];
}



@end



//
//  ViewNeedWebForLiveDB.h
//  ProjectView
//
//  Created by Alfred Man on 10/18/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewNeedWebForLiveDB : UIViewController {
    IBOutlet UITextView* textDescription;
    IBOutlet UIButton* retryInternetButton;
}

@property (nonatomic, retain) IBOutlet UITextView* textDescription;
@property (nonatomic, retain) IBOutlet UIButton* retryInternetButton;
- (IBAction) doRetryInternetButton;
@end

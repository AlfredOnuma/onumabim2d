//
//  BIMMailGroup.m
//  ProjectView
//
//  Created by Alfred Man on 4/8/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "BIMMailGroup.h"

@implementation BIMMailGroup
@synthesize aBIMMailRecipient;
@synthesize name;
@synthesize checked;
-(void)dealloc{
    [name release];
    [aBIMMailRecipient release];
    [super dealloc];
}

-initWithGroupName:(NSString*) _name aBIMMailRecipient:(NSMutableArray*)_aBIMMailRecipient{
    self=[super init];
    if (self){
        self.name=_name;
        self.aBIMMailRecipient=_aBIMMailRecipient;
        checked=false;
    }
    return self;
}

@end

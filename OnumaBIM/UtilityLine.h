//
//  UtilityLine.h
//  ProjectView
//
//  Created by onuma on 09/05/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "Element.h"
#import <Foundation/Foundation.h>
#import <sqlite3.h>
@interface UtilityLine : Element
-(UtilityLine*) initWithUtilityLineSQLStmt: (OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform;
@end

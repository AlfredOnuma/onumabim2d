//
//  DisplayModelUI.m
//  ProjectView
//
//  Created by Alfred Man on 11/10/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "ViewModelVC.h"
#import "DisplayModelUI.h"
#import "ViewProductRep.h"

#import "ProjectViewAppDelegate.h"
//#import "TestUI4.h"
//#import "ProductViewButtonRing.h"
#import "Floor.h"
#import "Site.h"
#import "Bldg.h"
#import "Space.h"
#import "Slab.h"
#import "Furn.h"
#import "NavUIScroll.h"
#import "BitmapRep.h"
#import "SubViewSpatialStructSlab.h"
#import "OPSNavUI.h"
#import "ViewProductLabel.h"
#import "DisplayInfo.h"
#import "SpatialStructure.h"

#import "OPSProduct.h"
#import "ViewProductSprite.h"
#import "SpritePoly.h"
#import "SpriteBitmap.h"
//#import "SpriteLabel.h"
#import "ExtrudedAreaSolid.h"
#import "Site.h"
#import "CaptureView.h"
#import "OPSProjectSite.h"
#import "SitePolygon.h"
//#import <QuartzCore/QuartzCore.h>
@implementation DisplayModelUI
@synthesize userSelectedMapType=_userSelectedMapType;

@synthesize delegate;
//@synthesize sitePolygonLayer=_sitePolygonLayer;
//@synthesize elemViewLayer;
//@synthesize spatialStructViewLayer;
//@synthesize spatialStructSlabViewLayer;
//@synthesize ghostLayer=_ghostLayer;
//@synthesize ghostFurnLayer=_ghostFurnLayer;

@synthesize siteMapView;

@synthesize mapTransformScale;
//@synthesize capturedMap;
@synthesize userLocationImageView;
@synthesize mapFrame;
@synthesize userLocation=_userLocation;


@synthesize sitePolygonLayer=_sitePolygonLayer;
@synthesize productViewLabelLayer;
@synthesize elemViewLayer;
@synthesize spatialStructViewLayer;
@synthesize spatialStructSlabViewLayer;
@synthesize ghostLayer=_ghostLayer;
@synthesize ghostFurnLayer=_ghostFurnLayer;
@synthesize locationManager=_locationManager;
//@synthesize  aGhostStruct=_aGhostStruct;
//@synthesize  aGhostFurn=_aGhostFurn;
//@synthesize  aElemView=_aElemView;
//@synthesize  aSitePolygon=_aSitePolygon;
//@synthesize  aSpatialStruct=_aSpatialStruct;
//@synthesize  aSpatialStructSlab=_aSpatialStructSlab;

//@synthesize drawLayer=_drawLayer;


@synthesize modelCenterScreenTransform=_modelCenterScreenTransform;
/*

-(void) addGhostStruct:(ViewProductRep*) viewProductRep{
    if (self.aGhostStruct==nil){
        NSMutableArray* array=[[NSMutableArray alloc]init];
        self.aGhostStruct=array;
        [array release];
    }
    [self.aGhostStruct addObject:viewProductRep];
}


-(void) addGhostFurn:(ViewProductRep*) viewProductRep{
    if (self.aGhostFurn==nil){
        NSMutableArray* array=[[NSMutableArray alloc]init];
        self.aGhostFurn=array;
        [array release];
    }
    [self.aGhostFurn addObject:viewProductRep];
}

-(void) addElemView:(ViewProductRep*) viewProductRep{
    if (self.aElemView==nil){
        NSMutableArray* array=[[NSMutableArray alloc]init];
        self.aElemView=array;
        [array release];
    }
    [self.aElemView addObject:viewProductRep];
}


-(void) addSitePolygon:(ViewProductRep*) viewProductRep{
    if (self.aSitePolygon==nil){
        NSMutableArray* array=[[NSMutableArray alloc]init];
        self.aSitePolygon=array;
        [array release];
    }
    [self.aSitePolygon addObject:viewProductRep];
}

-(void) addSpatialStruct:(ViewProductRep*) viewProductRep{
    if (self.aSpatialStruct==nil){
        NSMutableArray* array=[[NSMutableArray alloc]init];
        self.aSpatialStruct=array;
        [array release];
    }
    [self.aSpatialStruct addObject:viewProductRep];
}


-(void) addSpatialStructSlab:(ViewProductRep*) viewProductRep{
    if (self.aSpatialStructSlab==nil){
        NSMutableArray* array=[[NSMutableArray alloc]init];
        self.aSpatialStructSlab=array;
        [array release];
    }
    [self.aSpatialStructSlab addObject:viewProductRep];
}
*/

- (void)dealloc
{     
    
    [_locationManager release];_locationManager=nil;
    siteMapView.delegate=nil; // VERY IMPORT LINE HERE TO FIX THE MAPVIEW BUG when User Hit Back Button Too fast before load finish!!!
    [_userLocation release];_userLocation=nil;
    [siteMapView release];siteMapView=nil;
    [_sitePolygonLayer release];_sitePolygonLayer=nil;

    [productViewLabelLayer release];productViewLabelLayer=nil;
    [spatialStructViewLayer release];spatialStructViewLayer=nil;
    [spatialStructSlabViewLayer release];spatialStructSlabViewLayer=nil;


    for (UIView* view in _ghostLayer.subviews){
        if ([view isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* productRep=(ViewProductRep*) view; // [poly viewProductRep];
            [productRep release];
            productRep=nil;
        }
    }
    [_ghostLayer release];_ghostLayer=nil;
    [_ghostFurnLayer release];_ghostFurnLayer=nil;
    [elemViewLayer release];elemViewLayer=nil;
    
    
    [userLocationImageView release];userLocationImageView=nil;

    [super dealloc];
}



//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//    //Get all the touches.
//    
//    UITouch *touch = [[event allTouches] anyObject];      
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];           
//    ViewModelVC* modelVC=[appDele activeViewModelVC];  
//}

- (ViewModelVC*) parentViewModelVC{
    return [((NavUIScroll*) delegate) parentViewModelVC];
}
- (OPSProduct*) findRefProduct:(OPSProduct*) product{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([product isKindOfClass:[Slab class]]){
        OPSProduct* parentSpatialStruct=(OPSProduct*) [[product linkedRel] relating];
        if ([parentSpatialStruct isKindOfClass:[Floor class]]){
            parentSpatialStruct=(OPSProduct*) [[parentSpatialStruct linkedRel] relating];
            return parentSpatialStruct;
        }else{
            return product;
        }
    }else if ([product isKindOfClass:[Furn class]] && [appDele modelDisplayLevel]==1 ){
        OPSProduct* parentSpatialStruct=(OPSProduct*) [[product linkedRel] relating];
        if ([parentSpatialStruct isKindOfClass:[Space class]]){
//            parentSpatialStruct=(OPSProduct*) [[parentSpatialStruct linkedRel] relating];
            return parentSpatialStruct;
        }else{
            return product;
        }          
    }else{
        return product;
    }
}

- (BOOL) searchAndSelectElem:(NSSet *)touches withEvent:(UIEvent *)event{
    ViewModelVC* viewModelVC=[self parentViewModelVC];
    
//    CGPoint touchPointInSelfMirrorY=[[touches anyObject] locationInView:spatialStructViewLayer];      
    
    for (UIView* subview in [self.elemViewLayer subviews]){
        if (![subview isKindOfClass:([ViewProductRep class])]){continue;}
        ViewProductRep* viewProductRep=(ViewProductRep*) subview;     
        if (![viewProductRep.product isKindOfClass:[Furn class]]) {continue;}

        CGPoint touchPointInViewProductRep=[[touches anyObject] locationInView:viewProductRep];

        
        
        
//        ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];    
        double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor]; 
        BitmapRep* bitmapRep=[[[[viewProductRep product] representation] aRepresentationItem] objectAtIndex:0];
        CGRect bitmapRepBBox=[bitmapRep.bBox cgRect];
        CGRect bitmapRepBBoxFrame=CGRectMake(0, 0, bitmapRepBBox.size.width*modelScaleFactor,  bitmapRepBBox.size.height*modelScaleFactor);
        

        if (!CGRectContainsPoint(bitmapRepBBoxFrame,touchPointInViewProductRep)){continue;}            
        [viewModelVC selectAProductRep:viewProductRep];
        
        
//        [viewModelVC switchToolbarToProductViewToolbar];
        return true;;
      
    }
    return false;
    
}

- (BOOL) searchAndSelectSpatialStruct:(NSSet *)touches withEvent:(UIEvent *)event{
    ViewModelVC* viewModelVC=[self parentViewModelVC];
    
    CGPoint touchPointInSelfMirrorY=[[touches anyObject] locationInView:self.spatialStructViewLayer];
//        CGPoint touchPointInSelfMirrorY=[[touches anyObject] locationInView:self];
    for (UIView* subview in [self.spatialStructViewLayer subviews]){
//    for (UIView* subview in self.aSpatialStruct){
        if (![subview isKindOfClass:([ViewProductRep class])]){continue;}
        ViewProductRep* viewProductRep=(ViewProductRep*) subview;     
        //        if ([appDele modelDisplayLevel]==0 && [viewProductRep.product isKindOfClass:[Site class]]) {continue;}
        
        //        if ([appDele modelDisplayLevel]==1 && [viewProductRep.product isKindOfClass:[Floor class]]) {continue;}        
        //        if ([appDele modelDisplayLevel]==2 && [viewProductRep.product isKindOfClass:[Space class]]) {continue;}
        CGRect subViewFrame=viewProductRep.frame;
        if (!CGRectContainsPoint(subViewFrame,touchPointInSelfMirrorY)){continue;}
        CGPoint pt = [[touches anyObject] locationInView:viewProductRep];; 
        
        if ([viewProductRep containsPoint:pt]){
            //RefProduct is being selected instead of geoProduct, hence slab will not be selected, the spatialStruct containing slab will be selected instead.If its spatialStruct is a floor, the floor's parent spatialStruct ie the bldg will be selected instead
            [viewModelVC selectAProductRep:viewProductRep];
            //            [viewModelVC selectAProduct: ([viewProductRep product])];
            //            [viewProductRep setNeedsDisplay];
            //            [viewProductRep setNeedsDisplay];
            
//            numTouchedProduct++;
            
            
//            [viewModelVC switchToolbarToProductViewToolbar];
            return true;
            //            break;
        }        
    }
    
    if (self.ghostLayer){
//    if (self.aGhostStruct){
        for (UIView* subview in [self.ghostLayer subviews]){
//        for (UIView* subview in self.aGhostStruct){
            if (![subview isKindOfClass:([ViewProductRep class])]){continue;}
            ViewProductRep* viewProductRep=(ViewProductRep*) subview;
            CGRect subViewFrame=viewProductRep.frame;
            if (!CGRectContainsPoint(subViewFrame,touchPointInSelfMirrorY)){continue;}
            CGPoint pt = [[touches anyObject] locationInView:viewProductRep];;
            
            if ([viewProductRep containsPoint:pt]){

                [viewModelVC selectAProductRep:viewProductRep];
                return true;
            }
        }
    }
    return false;
}

- (BOOL) searchAndSelectSitePolygon:(NSSet *)touches withEvent:(UIEvent *)event{
    ViewModelVC* viewModelVC=[self parentViewModelVC];
    CGPoint touchPointInSelfMirrorY=[[touches anyObject] locationInView:self.sitePolygonLayer];
//    CGPoint touchPointInSelfMirrorY=[[touches anyObject] locationInView:self.drawLayer];
//    CGPoint touchPointInSelfMirrorY=[[touches anyObject] locationInView:self];
    for (UIView* subview in [self.sitePolygonLayer subviews]){
//    for (UIView* subview in self.aSitePolygon){
        if (![subview isKindOfClass:([ViewProductRep class])]){continue;}
        //        if ([appDele modelDisplayLevel]==1) {break;}
        ViewProductRep* viewProductRep=(ViewProductRep*) subview;
        
        
        CGRect subViewFrame=viewProductRep.frame;
        if (!CGRectContainsPoint(subViewFrame,touchPointInSelfMirrorY)){continue;}
        CGPoint pt = [[touches anyObject] locationInView:viewProductRep];;
        
        if ([viewProductRep containsPoint:pt]){
            //RefProduct is being selected instead of geoProduct, hence slab will not be selected, the spatialStruct containing slab will be selected instead.If its spatialStruct is a floor, the floor's parent spatialStruct ie the bldg will be selected instead
            [viewModelVC selectAProductRep:viewProductRep];
            
            return true;
        }
    } 
    return false;
    
    
    
}


- (BOOL) searchAndSelectSpatialStructSlab:(NSSet *)touches withEvent:(UIEvent *)event{
    ViewModelVC* viewModelVC=[self parentViewModelVC];
    CGPoint touchPointInSelfMirrorY=[[touches anyObject] locationInView:self.spatialStructSlabViewLayer];

    for (UIView* subview in [self.spatialStructSlabViewLayer subviews]){
//    for (UIView* subview in self.aSpatialStructSlab){
        if (![subview isKindOfClass:([ViewProductRep class])]){continue;}
//        if ([appDele modelDisplayLevel]==1) {break;}        
        ViewProductRep* viewProductRep=(ViewProductRep*) subview;           
        
        
        CGRect subViewFrame=viewProductRep.frame;
        if (!CGRectContainsPoint(subViewFrame,touchPointInSelfMirrorY)){continue;}
        CGPoint pt = [[touches anyObject] locationInView:viewProductRep];; 
        
        if ([viewProductRep containsPoint:pt]){
            //RefProduct is being selected instead of geoProduct, hence slab will not be selected, the spatialStruct containing slab will be selected instead.If its spatialStruct is a floor, the floor's parent spatialStruct ie the bldg will be selected instead
            [viewModelVC selectAProductRep:viewProductRep];
            
            

            
            
            //            [viewModelVC selectAProduct: ([viewProductRep product])];
            //            [viewProductRep setNeedsDisplay];
            //            [viewProductRep setNeedsDisplay];
            
//            numTouchedProduct++;            
//            
//            [viewModelVC switchToolbarToProductViewToolbar];
            return true;
            //            break;
        }          
    } 
    return false;
    
    
    
}



- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    //If Long Pressed, touchesEnded will not be reached. This only reach if user not pressing for long
    //Get all the touches.     
//    NSLog (@"Touches End");    
    NSSet *allTouches = [event allTouches];
    switch ([allTouches count])
    {
        case 1:
        {
            //Get the first touch.            
            UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
//            NSLog (@"scale : %f tap count: %d",[[[self parentViewModelVC] navUIScrollVC] zoomScale], [touch tapCount]);
            
            switch([touch tapCount])
            {
                    //
                    //                case 3:{
                    //                    DisplayModelUI* _displayModelUI=[self displayModelUI];
                    //                    [_displayModelUI captureMapView];
                    //                    break;
                    //                }
                case 2://Double tap.                    
                    [[[self parentViewModelVC] navUIScrollVC] zoomFit];
                    break;
                case 1://Single tap 
                    {
                        /* Multiple Selection By Gesture, Don't remove these lines till stable*********************
                        ViewModelVC* viewModelVC=[self parentViewModelVC];

                        if ([viewModelVC.aSelectedProductRep count]>=1 && viewModelVC.lastDeselectdViewProductRep!=nil){  
                            NSLog(@"MultiSelection Touch");
                            if (![viewModelVC selectOnlyLastDeselctedProductRepInMemory]) {return;}
                        }else{
                            NSLog(@"Not MultiSelection Touch");                            
                            if (![[self parentViewModelVC] selectOnlyLastProductRep]) {return;}
                        }
                         */
                        

                    }
                    break;           
                    
            }
        }
            break;
    }
    
    
}
/*
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    //Get all the touches.
    
    
//    NSLog (@"Touches End");
//    if (![[self parentViewModelVC] selectOnlyLastProductRep]) return;
    //    UITouch *touch = [[event allTouches] anyObject];      
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];           
    //    ViewModelVC* modelVC=[appDele activeViewModelVC];  
}
*/


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
//    NSLog (@"Touch");
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    ViewModelVC* viewModelVC=[self parentViewModelVC];
    
    if ([appDele modelDisplayLevel]==0){
        if ([self searchAndSelectSpatialStructSlab:touches withEvent:event]) {return;};        
        if ([self searchAndSelectSpatialStruct:touches withEvent:event]) {return;}
        if ([self searchAndSelectSitePolygon:touches withEvent:event]) {return;}
    }
    if ([appDele modelDisplayLevel]==1){                
        if ([self searchAndSelectSpatialStruct:touches withEvent:event]) {return;}   
        
        if ([self searchAndSelectSpatialStructSlab:touches withEvent:event]) {return;}          
    }  
    if ([appDele modelDisplayLevel]==2){
        if ([self searchAndSelectElem:touches withEvent:event]) {return;}        
        if ([self searchAndSelectSpatialStruct:touches withEvent:event]) {return;}        
    }
    if (![viewModelVC bMultiSelect]){
    
//            
//            NSSet *allTouches = [event allTouches];
//                  
//            UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
//                    
//            NSLog (@"tapcount: %d", [touch tapCount]);
//                if ([touch tapCount]==1){
//            
//            UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
            
            
            [viewModelVC clearSelectedProductRep];
//                 }
    }
//    [viewModelVC switchToolbarToNavScrollUIToolbar];

    return;
}

 
/*
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *hitView = [super hitTest:point withEvent:event];
    
    // If the hitView is THIS view, return the view that you want to receive the touch instead:
    if (hitView == self) {
        return nil;
    }
    // Else return the hitView (as it could be one of this view's buttons):
    return hitView;
}
*/
- (void)oneFingerTwoTaps
{
//    NSLog(@"Action: One finger, two taps");
}




// NSString* urlStr=[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%d,%d&zoom=14&size=2046x2046&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Ccolor:red%7Clabel:C%7C40.718217,-73.998284&sensor=false",site.latitude,site.longitude];

//NSString* urlStr=[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%f,%f &zoom=17&size=640x640&maptype=roadmap&sensor=false",site.latitude,site.longitude];



- (id)initWithFrame:(CGRect)frame rootSpatialStruct:(SpatialStructure*)rootSpatialStruct modelCenterScreenTransform:(CGAffineTransform) modelCenterScreenTransform navUIScroll:(NavUIScroll *)navUIScroll
{


    
    self = [super initWithFrame:frame];
    if (self) {      
//        [self setClipsToBounds:true];
        self.userSelectedMapType=MKMapTypeStandard;
        self.modelCenterScreenTransform=modelCenterScreenTransform;
        self.delegate=navUIScroll;
        

        
//        UIView* drawLayer=[[UIView alloc] initWithFrame:frame];
//        self.drawLayer=drawLayer;
//        [drawLayer release];
//        [self.drawLayer setUserInteractionEnabled:NO];
//        [self.drawLayer setTransform:CGAffineTransformMakeScale(1.0, -1.0)];
//        [self addSubview:self.drawLayer];
//
        
        
        UIView* _spatialStructSlabViewLayer=[[UIView alloc] initWithFrame:frame];                
        self.spatialStructSlabViewLayer=_spatialStructSlabViewLayer;
        [_spatialStructSlabViewLayer release];
        [spatialStructSlabViewLayer setUserInteractionEnabled:NO];
        [spatialStructSlabViewLayer setTransform:CGAffineTransformMakeScale(1.0, -1.0)];        

        
        UIView* _spatialStructureViewLayer=[[UIView alloc] initWithFrame:frame];
        self.spatialStructViewLayer=_spatialStructureViewLayer;
        [_spatialStructureViewLayer release];
        [spatialStructViewLayer setUserInteractionEnabled:NO];
        [spatialStructViewLayer setTransform:CGAffineTransformMakeScale(1.0, -1.0)];

        
        
        
        
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        
        
        
        
        if ([appDele modelDisplayLevel]==2){
            
            UIView* ghostLayer=[[UIView alloc] initWithFrame:frame];
            self.ghostLayer=ghostLayer;
            [ghostLayer release];
            [self.ghostLayer setUserInteractionEnabled:NO];
            [self.ghostLayer setTransform:CGAffineTransformMakeScale(1.0, -1.0)];
            [self addSubview:self.ghostLayer];
            
            
            UIView* ghostFurnLayer=[[UIView alloc] initWithFrame:frame];
            self.ghostFurnLayer=ghostFurnLayer;
            [ghostFurnLayer release];
            [self.ghostFurnLayer setUserInteractionEnabled:NO];
            [self.ghostFurnLayer setTransform:CGAffineTransformMakeScale(1.0, -1.0)];
            [self addSubview:self.ghostFurnLayer];
        }
        if ([appDele modelDisplayLevel]==0){                        

            
            
            [self addSubview:spatialStructViewLayer];
            
            UIView* sitePolygonLayer=[[UIView alloc] initWithFrame:frame];
            self.sitePolygonLayer=sitePolygonLayer;
            [sitePolygonLayer release];
            [self.sitePolygonLayer setUserInteractionEnabled:YES];
            [self.sitePolygonLayer setTransform:CGAffineTransformMakeScale(1.0, -1.0)];
            [self addSubview:self.sitePolygonLayer];
            
            
            [self addSubview:spatialStructSlabViewLayer];
        }else{                        
            [self addSubview:spatialStructSlabViewLayer];              
            [self addSubview:spatialStructViewLayer];                           
        }
        
        if ([appDele modelDisplayLevel]!=0){
            UIView* _elemViewLayer=[[UIView alloc] initWithFrame:frame];      
            self.elemViewLayer=_elemViewLayer;
            [_elemViewLayer release];
            [elemViewLayer setUserInteractionEnabled:NO];
            [elemViewLayer setTransform:CGAffineTransformMakeScale(1.0, -1.0)];
            [self addSubview:elemViewLayer];   
        }            
        
       
        
        UIView* _productViewLabelLayer=[[UIView alloc] initWithFrame:frame];
        self.productViewLabelLayer=_productViewLabelLayer;
        [_productViewLabelLayer release];
        [self addSubview:productViewLabelLayer];  
        
        [self initDislayFromRootSpatialStruct:rootSpatialStruct rootTransform:modelCenterScreenTransform];//] rootMatrix:[[viewModelVC model ] modelMatrix]];
        
        
        if ([appDele modelDisplayLevel]==2){

            OPSModel* model=[[self parentViewModelVC] model];
            for (Space* space in [model aGhostSpace]){
                [self initDislayFromRootSpatialStruct:space rootTransform:modelCenterScreenTransform];
                
            }
        }

        [self addAllViewProductLabel];


        
        
    }
    
    
    
//    NSLog (@"--Num StrucView:%d",[self.spatialStructViewLayer.subviews count]);
//    
//    NSLog (@"--Num StrucSlabView:%d",[self.spatialStructSlabViewLayer.subviews count]);    
    return self;
}

//- (IBAction)longPressDetected:(UIGestureRecognizer *)sender {
//    statusLabel.text = @"Long Press";
//}


/*
- (void) threeFingerPinchDetected: (UIPinchGestureRecognizer *)recognizer{    
    //    - (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //        [super touchesBegan:touches withEvent:event];
    NSLog(@"threeFingerPinch:%d",recognizer.numberOfTouches);
    
    uint numberofTouches=recognizer.numberOfTouches;
    
    if (numberofTouches!=3) {
//        [[[[self parentViewModelVC] opsNavUI] navUIScroll] pinchGestureRecognizer:recognizer];
        return;
    }
    if (recognizer.state==UIGestureRecognizerStateBegan){
        threeFingerPinchStartScale=recognizer.scale;
    }    
    if (recognizer.state==UIGestureRecognizerStateEnded){
        threeFingerPinchEndScale=recognizer.scale;   
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        if (threeFingerPinchStartScale>0){ 
            if ([appDele modelDisplayLevel]<2){
            [[[self parentViewModelVC] viewModelToolbar] browse:nil];
            }
        }else{
            if ([appDele modelDisplayLevel]>0){
                [[[self parentViewModelVC] navigationController] popViewControllerAnimated:true];

//                [[[self parentViewModelVC] viewModelToolbar] browse:nil];
            }                       
        }
    }   
}


*/
- (void) longPressDetected: (UIGestureRecognizer *)recognizer{

//    - (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//        [super touchesBegan:touches withEvent:event];
//        NSLog(@"LongPress");
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//        ViewModelVC* viewModelVC=[self parentViewModelVC];
        
//        [self removeGestureRecognizer:recognizer];
  /*      
        
        if ([appDele modelDisplayLevel]==0){
            if ([self searchAndSelectSpatialStructSlab:touches withEvent:event]) {return;};        
            if ([self searchAndSelectSpatialStruct:touches withEvent:event]) {return;}
        }
        
        if ([appDele modelDisplayLevel]==1){                
            if ([self searchAndSelectSpatialStruct:touches withEvent:event]) {return;}   
            
            if ([self searchAndSelectSpatialStructSlab:touches withEvent:event]) {return;}          
        }
        
        if ([appDele modelDisplayLevel]==2){
            if ([self searchAndSelectElem:touches withEvent:event]) {return;}        
            if ([self searchAndSelectSpatialStruct:touches withEvent:event]) {return;}        
        }
        
        [viewModelVC clearSelectedProductRep];
        //    [viewModelVC switchToolbarToNavScrollUIToolbar];
        
        return;
    }
   */
}
//- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
//{
////    [recognizer numberOfTouches];
//    
//}
//-(void) drawRect:(CGRect)rect{
//    UIView* t=[[UIView alloc] initWithFrame:CGRectMake(0,0,400,400)];
//    t.backgroundColor=[UIColor redColor];
//    [self addSubview:t];
//}
/*
- (void)drawRect:(CGRect)rect{

    self.backgroundColor=[UIColor clearColor];
    
    self.alpha=0.7;

    ProjectViewAppDelegate* appDele0=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];   
    
    
    CGContextRef context0 = UIGraphicsGetCurrentContext();             
    CGAffineTransform t0 = CGContextGetCTM(context0);
    t0 = CGAffineTransformInvert(t0);
    CGContextConcatCTM(context0,t0);        
    
    
    
    double canavsWid=self.frame.size.width/[[[appDele0 activeViewModelVC] navUIScrollVC] zoomScale] ;
    double canvasHit=self.frame.size.height/[[[appDele0 activeViewModelVC] navUIScrollVC] zoomScale] ;
    
    double testWidth=900/[[[appDele0 activeViewModelVC] navUIScrollVC] zoomScale] ;
    double testHeight=300/[[[appDele0 activeViewModelVC] navUIScrollVC] zoomScale] ;

    double testOffX=-testWidth*0.25;
    double testOffY=0.0;
    double dX=canavsWid/2.0-testWidth/2.0;
    double dY=canvasHit/2.0-testHeight/2.0;
    TestUI4* tuiA0=[[TestUI4 alloc] initWithFrame:CGRectMake(0,-testHeight, testWidth, testHeight)];
//    [tuiA0 setBounds:CGRectMake(testOffX, testOffY, testWidth, testHeight)];
    CGAffineTransform atA0=CGAffineTransformMakeTranslation(dX,canvasHit-dY);//frameWidth/2,frameHeight/2);    
    [tuiA0 setTransform:atA0];
    [tuiA0 setAutoresizingMask:false];
    [self addSubview:tuiA0];

    double rotZ=30;
    TestUI4* tuiA1=[[TestUI4 alloc] initWithFrame:CGRectMake(0,-testHeight, testWidth, testHeight)];
    [tuiA1 setBounds:CGRectMake(testOffX, testOffY, testWidth, testHeight)];    
    CGAffineTransform atA10=CGAffineTransformMakeTranslation(dX,canvasHit-dY);//frameWidth/2,frameHeight/2);    
    CGAffineTransform atA11=CGAffineTransformTranslate(atA10,-testWidth/2, +testHeight/2);    
    CGAffineTransform atA12=CGAffineTransformRotate(atA11, -rotZ*M_PI/180);
    CGAffineTransform atA13=CGAffineTransformTranslate(atA12,testWidth/2, -testHeight/2);        
    [tuiA1 setTransform:atA13];
    [self addSubview:tuiA1];
    
    
    
//    double scaleY=30;
    TestUI4* tuiA2=[[TestUI4 alloc] initWithFrame:CGRectMake(0,-testHeight, testWidth, testHeight)];
    [tuiA2 setBounds:CGRectMake(testOffX, testOffY, testWidth, testHeight)];    
    CGAffineTransform atA20=CGAffineTransformMakeTranslation(dX,canvasHit-dY);//frameWidth/2,frameHeight/2);    
    CGAffineTransform atA21=CGAffineTransformTranslate(atA20,-testWidth/2, +testHeight/2);    
    CGAffineTransform atA22=CGAffineTransformRotate(atA21, -rotZ*M_PI/180);
    CGAffineTransform atA23=CGAffineTransformScale(atA22,-1,1);    
    CGAffineTransform atA24=CGAffineTransformTranslate(atA23,testWidth/2, -testHeight/2);       
    [tuiA2 setTransform:atA24];
    [self addSubview:tuiA2];
    
    
//    double frameWidth=self.frame.size.width;
//    double frameHeight=self.frame.size.height;
//    
//    
//    CGMutablePathRef path = CGPathCreateMutable();  
//    
//    
//    double wid=3000;//frameWidth;
//    double hit=1000;//frameHeight;
//    double offsetX=0;//-frameWidth/2.0;//0;
//    double offsetY=0;//-hit;    
//    
//    //    [self setBounds:CGRectMake(offsetX, offsetY, wid, hit)];
//    CGPathMoveToPoint(path,NULL,offsetX+0,offsetY+0);  
//    
//    CGPathAddLineToPoint(path,NULL,offsetX+wid,offsetY+0);
//    
//    //    CGPathAddLineToPoint(path,NULL,offsetX+wid,offsetY+hit);
//    
//    CGPathAddLineToPoint(path,NULL,offsetX+wid,offsetY+hit);
//    
//    
//    
//    CGContextBeginPath(context0);
//    CGContextAddPath(context0,path);
//    
//    CGContextSetRGBStrokeColor(context0, 1.0, 1.0, 1.0, 1.0);
//    CGContextSetRGBFillColor(context0, 1.0, 0.0, 0.0, 0.6);    
//    
//    CGContextSetLineWidth(context0, 5.0);
//    
//    CGContextClosePath(context0);     
//    CGContextFillPath(context0);
    
    
    return
    ;
}
 */
    /*
    OPSModel* model=[[appDele0 activeViewModelVC] model];
    BBox* bBox=[[model root] bBox];
    double modelScaleForScreenFactor=[model modelScaleForScreenFactor];
    CPoint* LL= [[CPoint alloc] initWithX:0+ [bBox minX]*modelScaleForScreenFactor y:0+ [bBox minY]*modelScaleForScreenFactor];
    LL=[[ model modelMatrix] applyPoint:LL];    
//    CGPoint LL=CGPointMake(0,0);//( (bBox.minX ) *modelScaleForScreenFactor ,   (bBox.minY  ) *modelScaleForScreenFactor);
//    CGPoint UR=CGPointMake( (bBox.minX + [bBox getWidth]) *modelScaleForScreenFactor ,   (bBox.minY + [bBox getHeight]  ) *modelScaleForScreenFactor);
//    CGPoint UR=CGPointMake( ( [bBox getWidth]) *modelScaleForScreenFactor ,   ([bBox getHeight]  ) *modelScaleForScreenFactor);
    CPoint* UR= [[CPoint alloc] initWithX:[bBox getWidth]*modelScaleForScreenFactor + [bBox minX]*modelScaleForScreenFactor y:[bBox getHeight]*modelScaleForScreenFactor + [bBox minY]*modelScaleForScreenFactor];
    UR=[[ model modelMatrix] applyPoint:UR];
    
    NSLog(@"LL.x:%f , LL.y:%f  |  UR.x:%f , UR.y:%f", LL.x,LL.y,UR.x,UR.y);     
    

    CGMutablePathRef path = CGPathCreateMutable();    
    
    
    CGPathMoveToPoint(path,NULL,LL.x,LL.y);  
    
    CGPathAddLineToPoint(path,NULL,UR.x,LL.y);
    
    CGPathAddLineToPoint(path,NULL,(UR.x+LL.x)/2,UR.y);
//    CGPathAddLineToPoint(path,NULL,0,400);
    
    CGContextRef context0 = UIGraphicsGetCurrentContext();     
    CGContextBeginPath(context0);
    CGContextAddPath(context0,path);
    
    CGContextSetRGBStrokeColor(context0, 1.0, 1.0, 1.0, 1.0);
    CGContextSetRGBFillColor(context0, 1.0, 0.0, 0.0, 0.6);    
    
    CGContextSetLineWidth(context0, 5.0);
    CGContextFillPath(context0);
    CGContextClosePath(context0);
    
    return;
    
}
 */

- (CGAffineTransform) setupBitmapPlacementTransform:(CGAffineTransform) placementTransform productBBox:(CGRect) productBBox{
    ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];    
    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
    //    BBox* productBBox=bBox;
    
    //    CGAffineTransform atShapeAnchorToLR=CGAffineTransformTranslate(CGAffineTransformIdentity,-[productBBox getWidth]*modelScaleFactor/2, -[productBBox getHeight]*modelScaleFactor/2);        
    CGAffineTransform atShapeAnchorToLR=CGAffineTransformTranslate(CGAffineTransformIdentity,-productBBox.size.width*modelScaleFactor/2, -productBBox.size.height*modelScaleFactor/2);        
    
    CGAffineTransform atShapePlacement= CGAffineTransformConcat(placementTransform,atShapeAnchorToLR);    
    CGAffineTransform atShapeAnchorToLRInverse=CGAffineTransformTranslate(atShapePlacement,productBBox.size.width*modelScaleFactor/2, productBBox.size.height*modelScaleFactor/2);                   
        return CGAffineTransformScale(atShapeAnchorToLRInverse,1,1);        
//    return CGAffineTransformScale(atShapeAnchorToLRInverse,1,-1);
}




- (CGAffineTransform) setupShapePlacementTransform:(CGAffineTransform) placementTransform productBBox:(CGRect) productBBox bFlipX:(Boolean)bFlipX {  

    ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];    
    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];       
    CGAffineTransform atShapeAnchorToLR=CGAffineTransformTranslate(CGAffineTransformIdentity,-productBBox.size.width*modelScaleFactor/2, -productBBox.size.height*modelScaleFactor/2);        
    
    CGAffineTransform atShapePlacement= CGAffineTransformConcat(placementTransform,atShapeAnchorToLR);    
    CGAffineTransform atShapeAnchorToLRInverse=CGAffineTransformTranslate(atShapePlacement,productBBox.size.width*modelScaleFactor/2, productBBox.size.height*modelScaleFactor/2);           
    //Following 2 lines identical
    //    CGAffineTransform atShapeMinXMinYDisplacement=CGAffineTransformTranslate(atShapeAnchorToLRInverse,productBBox.bounds.origin.x,productBBox.bounds.origin.y);    
    CGAffineTransform atShapeMinXMinYDisplacement=CGAffineTransformTranslate(atShapeAnchorToLRInverse,productBBox.origin.x*modelScaleFactor,productBBox.origin.y*modelScaleFactor);         
    
    if (bFlipX){
        return CGAffineTransformScale(atShapeMinXMinYDisplacement,1,-1);
    }else{
        return atShapeMinXMinYDisplacement;
    }
}
/*
- (CGAffineTransform) setupShapePlacementTransform:(CGAffineTransform) placementTransform bBox:(BBox*) bBox bFlipX:(Boolean)bFlipX {  
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
    ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];    
    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
    BBox* productBBox=bBox;
    CGAffineTransform atShapeAnchorToLR=CGAffineTransformTranslate(CGAffineTransformIdentity,-[productBBox getWidth]*modelScaleFactor/2, -[productBBox getHeight]*modelScaleFactor/2);        
    CGAffineTransform atShapePlacement= CGAffineTransformConcat(placementTransform,atShapeAnchorToLR);    
    CGAffineTransform atShapeAnchorToLRInverse=CGAffineTransformTranslate(atShapePlacement,[productBBox getWidth]*modelScaleFactor/2, [productBBox getHeight]*modelScaleFactor/2);           
    //Following 2 lines identical
    //    CGAffineTransform atShapeMinXMinYDisplacement=CGAffineTransformTranslate(atShapeAnchorToLRInverse,productBBox.bounds.origin.x,productBBox.bounds.origin.y);    
    CGAffineTransform atShapeMinXMinYDisplacement=CGAffineTransformTranslate(atShapeAnchorToLRInverse,productBBox.minX*modelScaleFactor,productBBox.minY*modelScaleFactor);         

    if (bFlipX){
        return CGAffineTransformScale(atShapeMinXMinYDisplacement,1,-1);
    }else{
        return atShapeMinXMinYDisplacement;
    }
}
*/

- (void) addAllViewProductLabel{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    if ([appDele modelDisplayLevel]==0){
        for (ViewProductRep* viewProductRep in [self sitePolygonLayer].subviews){        
            [self addDisplayLabelToViewProductRep:viewProductRep displayInfo:nil rootSpatialStructTransform:CGAffineTransformIdentity];
            [[[productViewLabelLayer subviews]lastObject] setNeedsDisplay];
            
        }
    }
    for (ViewProductRep* viewProductRep in [self spatialStructViewLayer].subviews){
        [self addDisplayLabelToViewProductRep:viewProductRep displayInfo:nil rootSpatialStructTransform:CGAffineTransformIdentity];
        [[[productViewLabelLayer subviews]lastObject] setNeedsDisplay];
    }
    if ([appDele modelDisplayLevel]==0){
        for (ViewProductRep* viewProductRep in [self spatialStructSlabViewLayer].subviews){
//        for (ViewProductRep* viewProductRep in self.aSpatialStructSlab){
            [self addDisplayLabelToViewProductRep:viewProductRep displayInfo:nil rootSpatialStructTransform:CGAffineTransformIdentity];
            [[[productViewLabelLayer subviews]lastObject] setNeedsDisplay];
        }
    }
    if ([appDele modelDisplayLevel]==1){
        for (ViewProductRep* viewProductRep in [self spatialStructSlabViewLayer].subviews){
//        for (ViewProductRep* viewProductRep in self.aSpatialStructSlab){
            [self addDisplayLabelToViewProductRep:viewProductRep displayInfo:nil rootSpatialStructTransform:CGAffineTransformIdentity];
            [[[productViewLabelLayer subviews]lastObject] setNeedsDisplay];
        }
    }
    if ([appDele modelDisplayLevel]==2){
        for (ViewProductRep* viewProductRep in [self elemViewLayer].subviews){
//        for (ViewProductRep* viewProductRep in self.aElemView){
            [self addDisplayLabelToViewProductRep:viewProductRep displayInfo:nil rootSpatialStructTransform:CGAffineTransformIdentity];
            [[[productViewLabelLayer subviews]lastObject] setNeedsDisplay];
        }
    }
    [[self parentViewModelVC] refreshLabel];
     

}
- (void)initDislayFromRootSpatialStruct:(SpatialStructure*) rootSpatialStruct rootTransform: (CGAffineTransform) rootTransform {//rootViewProductRep:(ViewProductRep*) rootViewProductRep{
//    NSLog (@"DisplayModelUI initDisplayFromRootSpatialStruct");
    
    
    
    
    
    
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    ViewModelVC* viewModelVC=[self parentViewModelVC];
    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
    CGAffineTransform rootSpatialStructTransform=CGAffineTransformConcat([rootSpatialStruct.placement toScreenScaledCGAffineTransform] , rootTransform );
    
    
    
    OPSProduct* refProduct=[self findRefProduct:rootSpatialStruct];
    
    
    
    
    
    
    
    
    
    
    if ([rootSpatialStruct representation]!=Nil){
        
//        CGRect rootSpatialStructUIFrameBBox=CGRectMake(rootSpatialStruct.uiFrameBBox.origin.x, rootSpatialStruct.uiFrameBBox.origin.y, rootSpatialStruct.uiFrameBBox.size.width, rootSpatialStruct.uiFrameBBox.size.height);
//        CGRect rootSpatialStructUIFrame=CGRectMake(0.0,0.0, rootSpatialStructUIFrameBBox.size.width * modelScaleFactor, rootSpatialStructUIFrameBBox.size.height * modelScaleFactor);                                

            
        
        
        
        
        
        
        
        
        
        
        
        
        
//        
//        
//        ViewProductRep* viewProduct=[[ViewProductRep alloc] initWithFrame:rootSpatialStructUIFrame frameTransform:CGAffineTransformMakeTranslation(-rootSpatialStructUIFrameBBox.origin.x* modelScaleFactor, -rootSpatialStructUIFrameBBox.origin.y* modelScaleFactor) product:refProduct displayModelUI:self] ;         
//        [viewProduct setTransform: [self setupShapePlacementTransform:rootSpatialStructTransform productBBox:rootSpatialStructUIFrameBBox bFlipX:false ]];
//        
//        
   
        
        Representation* rep=[rootSpatialStruct representation];
        if (rep!=NULL) {                           
             NSMutableArray* aRepItem=[rep aRepresentationItem];
             if (aRepItem!=NULL && [aRepItem count]>0){ 

                for (int pRepItem=0;pRepItem<[aRepItem count];pRepItem++){
                    RepresentationItem* repItem=[aRepItem objectAtIndex:pRepItem];
                    if (repItem==NULL) continue;
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    if ([repItem isKindOfClass:[ExtrudedAreaSolid class]]){
                        
                        
                        
                        
                        
                        
                        
                        CGAffineTransform polyTransform=CGAffineTransformConcat([rootSpatialStruct.placement toScreenScaledCGAffineTransform] , rootTransform );
                        
                        CGRect polyUIFrameBBox=repItem.bBox.cgRect;
                        CGRect polyUIFrame=CGRectMake(0.0,0.0, polyUIFrameBBox.size.width * modelScaleFactor, polyUIFrameBBox.size.height * modelScaleFactor);
                        
                        ViewProductRep* polyViewProduct=[[ViewProductRep alloc] initWithFrame:polyUIFrame frameTransform:CGAffineTransformMakeTranslation(-polyUIFrameBBox.origin.x* modelScaleFactor, -polyUIFrameBBox.origin.y* modelScaleFactor) product:refProduct displayModelUI:self] ;
                        [polyViewProduct setTransform: [self setupShapePlacementTransform:polyTransform productBBox:polyUIFrameBBox bFlipX:false ]];
                        
                        
                        
                        SpritePoly* spritePoly=[[SpritePoly alloc] initWithTransform:CGAffineTransformIdentity viewProductRep:polyViewProduct representationItem:repItem displayModelUI:self];
                        [polyViewProduct addSprite:spritePoly];
                        
                        
                        //                                NSLog(@"add Sprite to %@: %@, which has %d sprite",[[sitePolyViewProduct.product class]description], [sitePolyViewProduct.product name],[sitePolyViewProduct.aSprite count]);
                        
                        [spritePoly release];
                        
                        
                        
                        
                        
                        
                        
                        polyViewProduct.delegate=self;
                        if ([refProduct isGhostObject]){
                            polyViewProduct.userInteractionEnabled=FALSE;
                            [[self ghostLayer] addSubview:polyViewProduct];

                            //            NSLog(@"drawLayer object: %d ~ add Ghost Product:%@: %@",[[[self drawLayer] subviews] count],[[viewProduct.product class]description], [viewProduct.product name]);
                            //            [self addSubview:viewProduct];
                            
                        }else{
                            polyViewProduct.userInteractionEnabled=FALSE;
                            [[self spatialStructViewLayer] addSubview:polyViewProduct];
//                            if ([polyViewProduct.product isKindOfClass:[Site class]]){
//                                [[self spatialStructViewLayer] sendSubviewToBack:polyViewProduct];
//                            }

                            //            NSLog(@"drawLayer object: %d ~ addProduct:%@: %@",[[[self drawLayer] subviews] count],[[viewProduct.product class]description], [viewProduct.product name]);
                            //            [self addSubview:viewProduct];
                        }
                        
                        [polyViewProduct release];
                        

                        
                        
                        
                    }
                    
                    
                }

                 
                 
                 
                 
                 
                 
                 
//                 
//
//                    SpritePoly* spritePoly=[[SpritePoly alloc] initWithTransform:CGAffineTransformIdentity viewProductRep:viewProduct representationItem:repItem displayModelUI:self];
//                    
//                    [viewProduct addSprite:spritePoly];                    
////                    NSLog(@"add Sprite to %@: %@, which has %d sprite",[[viewProduct.product class]description], [viewProduct.product name], [viewProduct.aSprite count]);
//                    [spritePoly release];
//                    

                    
                }
             }
        }

//        viewProduct.delegate=self;
//        if ([refProduct isGhostObject]){
//            viewProduct.userInteractionEnabled=FALSE;
////            [[self ghostLayer] addSubview:viewProduct];
//            [self addGhostStruct:viewProduct];
//            [[self drawLayer] addSubview:viewProduct];
////            NSLog(@"drawLayer object: %d ~ add Ghost Product:%@: %@",[[[self drawLayer] subviews] count],[[viewProduct.product class]description], [viewProduct.product name]);
////            [self addSubview:viewProduct];
//            
//        }else{
//            viewProduct.userInteractionEnabled=FALSE;
////            [[self spatialStructViewLayer] addSubview:viewProduct];
//            [self addSpatialStruct:viewProduct];
//            [[self drawLayer] addSubview:viewProduct];
////            NSLog(@"drawLayer object: %d ~ addProduct:%@: %@",[[[self drawLayer] subviews] count],[[viewProduct.product class]description], [viewProduct.product name]);
////            [self addSubview:viewProduct];
//        }
//
//        [viewProduct release];
//    }
    
    NSMutableArray* aRelatedStruct=[[rootSpatialStruct relAggregate] related];
    if (aRelatedStruct!=NULL){                        
        for (int pRelatedStruct=0;pRelatedStruct<[aRelatedStruct count];pRelatedStruct++){
            SpatialStructure* spatialStruct=[aRelatedStruct objectAtIndex:pRelatedStruct];                                    
            [self initDislayFromRootSpatialStruct:spatialStruct rootTransform:rootSpatialStructTransform];
        }
    }
    
    if ([rootSpatialStruct relContain]==nil) {return;}
    NSMutableArray* aRelatedElem=[[rootSpatialStruct relContain] related];
    if (aRelatedElem==Nil || [aRelatedElem count] <=0) { return;}
         
  
    
    CGRect rootSpatialStructUIFrameBBox=CGRectMake(rootSpatialStruct.uiFrameBBox.origin.x, rootSpatialStruct.uiFrameBBox.origin.y, rootSpatialStruct.uiFrameBBox.size.width, rootSpatialStruct.uiFrameBBox.size.height);
    CGRect rootSpatialStructUIFrame=CGRectMake(0.0,0.0, rootSpatialStructUIFrameBBox.size.width * modelScaleFactor, rootSpatialStructUIFrameBBox.size.height * modelScaleFactor);                                
    

    ViewProductRep* viewProduct=[[ViewProductRep alloc] initWithFrame:rootSpatialStructUIFrame frameTransform:CGAffineTransformMakeTranslation(-rootSpatialStructUIFrameBBox.origin.x* modelScaleFactor, -rootSpatialStructUIFrameBBox.origin.y* modelScaleFactor) product:refProduct  displayModelUI:self] ; 
    
              
    
    [viewProduct setTransform: [self setupShapePlacementTransform:rootSpatialStructTransform productBBox:rootSpatialStructUIFrameBBox bFlipX:false ]];                        
        
    
    
    for (int pRelatedElem=0;pRelatedElem<[aRelatedElem count];pRelatedElem++){
        OPSProduct* elem=[aRelatedElem objectAtIndex:pRelatedElem]; 
        
        Representation* rep=[elem representation];
        if (rep!=NULL) {   
            
            
            if ([appDele modelDisplayLevel]==2 && [elem isKindOfClass:[Furn class]]){
                
                            
                Representation* elemRep=[elem representation];

                if (elemRep!=NULL) {                           
                    NSMutableArray* aRepItem=[elemRep aRepresentationItem];
                    if (aRepItem!=NULL && [aRepItem count]>0){                                                 
                        for (int pRepItem=0;pRepItem<[aRepItem count];pRepItem++){                                                        
                            RepresentationItem* repItem=[aRepItem objectAtIndex:pRepItem];
                            if (repItem==NULL) continue;
                            if ([repItem isKindOfClass:[BitmapRep class]]){
                                
                                BitmapRep* bitmapRep=(BitmapRep*) repItem;
                                ;
                                CGRect bitmapRepRect=[bitmapRep.bBox cgRect];

                                
                                 CGRect elemUIFrame=CGRectMake(0,0, bitmapRepRect.size.width * modelScaleFactor, bitmapRepRect.size.height * modelScaleFactor);                                

                                
                                CGAffineTransform frameLocalTransform=CGAffineTransformMakeTranslation(bitmapRepRect.origin.x*modelScaleFactor, bitmapRepRect.origin.y*modelScaleFactor);                                
                                
                                
                                ViewProductRep* elemViewProduct=[[ViewProductRep alloc] initWithFrame:elemUIFrame frameTransform:CGAffineTransformInvert(frameLocalTransform) product:elem  displayModelUI:self] ; 
                                
                                
                                CGAffineTransform elemTransform=[elem.placement toScreenScaledCGAffineTransform];                
                                CGAffineTransform elemInsideRootTransform=CGAffineTransformConcat(elemTransform, rootSpatialStructTransform); 

                                [elemViewProduct setTransform: [self setupShapePlacementTransform:elemInsideRootTransform productBBox:bitmapRepRect bFlipX:false]]; 

                                
                                
                                elemViewProduct.delegate=self;
                                
                                
                                
                                
                                
                                SpriteBitmap* spriteBitmap=[[SpriteBitmap alloc] initWithTransform:CGAffineTransformIdentity viewProductRep:elemViewProduct representationItem:repItem displayModelUI:self];
                                
                                [elemViewProduct addSprite:spriteBitmap];
                                
//                                NSLog(@"add Sprite to %@: %@, which has %d sprite",[[elemViewProduct.product class]description], [elemViewProduct.product name], [elemViewProduct.aSprite count]);
                                
                    
                                
                                [spriteBitmap release];
                                
                                
                                if ([elem isGhostObject]){
                                    elemViewProduct.userInteractionEnabled=FALSE;
                                    [[self ghostFurnLayer] addSubview:elemViewProduct];

//                                    NSLog(@"drawLayer object: %d ~ add Ghost Bitmap Product:%@: %@",[[[self drawLayer] subviews] count],[[viewProduct.product class]description], [viewProduct.product name]);
//                                        [self addSubview:elemViewProduct];
                                    
                                }else{
                                    elemViewProduct.userInteractionEnabled=TRUE;
                                    [elemViewLayer addSubview:elemViewProduct];
                                    
                                    
//                                    NSLog(@"drawLayer object: %d ~ add Bitmap Product:%@: %@",[[[self drawLayer] subviews] count],[[viewProduct.product class]description], [viewProduct.product name]);
//                                    [self addSubview:elemViewProduct];
                                }
                                
                                
                                [elemViewProduct release];  
                            }
                            
                            
                            
                           
                        }
                    }
                }                
                
                

                 
                               
            }else if ([appDele modelDisplayLevel]==0 && [elem isKindOfClass:[SitePolygon class]]){

                Representation* elemRep=[elem representation];
                
                
                if (elemRep!=NULL) {
                    NSMutableArray* aRepItem=[elemRep aRepresentationItem];
                    if (aRepItem!=NULL && [aRepItem count]>0){
                        
                        for (int pRepItem=0;pRepItem<[aRepItem count];pRepItem++){
                            RepresentationItem* repItem=[aRepItem objectAtIndex:pRepItem];
                            if (repItem==NULL) continue;
                            if ([repItem isKindOfClass:[ExtrudedAreaSolid class]]){
                
                                
                                
                                
                                 
                                 
                                 
                                 CGAffineTransform sitePolyTransform=CGAffineTransformConcat([elem.placement toScreenScaledCGAffineTransform] , rootTransform );
                                CGRect sitePolyUIFrameBBox=repItem.bBox.cgRect;//=CGRectMake(repItem.bBox.origin.x, repItem.bBox.origin.y, repItem.bBox.size.width,repItem.bBox.size.height);
                                 CGRect sitePolyUIFrame=CGRectMake(0.0,0.0, sitePolyUIFrameBBox.size.width * modelScaleFactor, sitePolyUIFrameBBox.size.height * modelScaleFactor);
                                 
                                 ViewProductRep* sitePolyViewProduct=[[ViewProductRep alloc] initWithFrame:sitePolyUIFrame frameTransform:CGAffineTransformMakeTranslation(-sitePolyUIFrameBBox.origin.x* modelScaleFactor, -sitePolyUIFrameBBox.origin.y* modelScaleFactor) product:elem displayModelUI:self] ;                                
                                [sitePolyViewProduct setTransform: [self setupShapePlacementTransform:sitePolyTransform productBBox:sitePolyUIFrameBBox bFlipX:false ]];
                                
                 
                                
                                SpritePoly* spritePoly=[[SpritePoly alloc] initWithTransform:CGAffineTransformIdentity viewProductRep:sitePolyViewProduct representationItem:repItem displayModelUI:self];                                
                                [sitePolyViewProduct addSprite:spritePoly];
                                
                                
//                                NSLog(@"add Sprite to %@: %@, which has %d sprite",[[sitePolyViewProduct.product class]description], [sitePolyViewProduct.product name],[sitePolyViewProduct.aSprite count]);
                                
                                [spritePoly release];
                                
                                
                                
                                [[self sitePolygonLayer] addSubview:sitePolyViewProduct];

                                
                                
//                                NSLog(@"drawLayer object: %d ~ add SitePolygon Product:%@: %@",[[[self drawLayer] subviews] count],[[viewProduct.product class]description], [viewProduct.product name]);
                                [sitePolyViewProduct release];

        

                            }
                            
                            
                        }
                    }
                }
                
            }else{
            
                Representation* elemRep=[elem representation];
                
                
                
                if (elemRep!=NULL) {                           
                    NSMutableArray* aRepItem=[elemRep aRepresentationItem];
                    if (aRepItem!=NULL && [aRepItem count]>0){ 
                        
                        //                 BBox* boundBBox=[[BBox alloc] init];
                        for (int pRepItem=0;pRepItem<[aRepItem count];pRepItem++){
                            RepresentationItem* repItem=[aRepItem objectAtIndex:pRepItem];
                            if (repItem==NULL) continue;
                            if ([repItem isKindOfClass:[ExtrudedAreaSolid class]]){
                                SpritePoly* spritePoly=[[SpritePoly alloc] initWithTransform:[[elem placement] toScreenScaledCGAffineTransform] viewProductRep:viewProduct representationItem:repItem displayModelUI:self];
                                
                                [viewProduct addSprite:spritePoly];
                                
                                
                                
//                                NSLog(@"add Sprite to %@: %@, which has %d sprite",[[viewProduct.product class]description], [viewProduct.product name],[viewProduct.aSprite count]);
                                
                                
                                
                                [spritePoly release];
                            }else if ([repItem isKindOfClass:[BitmapRep class]]){
                              
                                SpriteBitmap* spriteBitmap=[[SpriteBitmap alloc] initWithTransform:[[elem placement] toScreenScaledCGAffineTransform] viewProductRep:viewProduct representationItem:repItem displayModelUI:self];
                                
                                [viewProduct addSprite:spriteBitmap];
                                
                                
//                                NSLog(@"add Sprite to %@: %@, which has %d sprite",[[viewProduct.product class]description], [viewProduct.product name],[viewProduct.aSprite count]);
                                
                                
                                
                                [spriteBitmap release];
                            }

                            
                        }
                    }
                }
                
                
                
                
            }
        }
    }
    
    
    
    viewProduct.delegate=self;


    
    if ([refProduct isGhostObject]){        
        //////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        
        
//        if ([refProduct isKindOfClass:[Furn class]]){
//            viewProduct.userInteractionEnabled=FALSE;
//            [[self ghostFurnLayer] addSubview:viewProduct];
//        }else{
//            viewProduct.userInteractionEnabled=TRUE;
//            [[self ghostLayer] addSubview:viewProduct];
//        }

    }else{
        viewProduct.userInteractionEnabled=TRUE;
        if ([refProduct isKindOfClass:[Floor class]]){
            [spatialStructSlabViewLayer addSubview:viewProduct];

            
            
//            NSLog(@"drawLayer object: %d ~ add spatial Struct Slab:%@: %@",[[[self drawLayer] subviews] count],[[viewProduct.product class]description], [viewProduct.product name]);
//            [self  addSubview:viewProduct];

            
        }else if (viewProduct.aSprite!=nil && [viewProduct.aSprite  count]>0){//if ([viewProduct.product isKindOfClass:[Furn class]]){
            [elemViewLayer addSubview:viewProduct];            

            
//            NSLog(@"drawLayer object: %d ~ add Elem:%@: %@",[[[self drawLayer] subviews] count],[[viewProduct.product class]description], [viewProduct.product name]);
//            [self addSubview:viewProduct];
        }
    }
    
    
   

//    NSLog(@"Number of object in drawLayer: %d",[[self.drawLayer subviews] count]);
    [viewProduct release];
    	
    
    
}

-(Boolean) shouldDisplayLabel:(DisplayInfo*) displayInfo{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    if ([displayInfo.product.aViewProductRep count]>1){
//        return false;
//    }
    if (appDele.modelDisplayLevel==1 &&[displayInfo.product isKindOfClass:[Bldg class]]){
        return false;
    }
    if (appDele.modelDisplayLevel==2 &&[displayInfo.product isKindOfClass:[Space class]]){
        return false;
    }
    return true;
}
-(void) addDisplayLabelToViewProductRep:(ViewProductRep*) viewProductRep displayInfo:(DisplayInfo*) displayInfo rootSpatialStructTransform:(CGAffineTransform) rootSpatialStructTransform{

    ///////////////////Start of Trial to add Label directly to sprite anytime/////////////////
    OPSProduct* refProduct=nil;
    if ([[viewProductRep product] isKindOfClass:[SpatialStructure class]]){
        refProduct=[[viewProductRep product] findParentSpatialStruct];
    }else{
        refProduct=[viewProductRep product];
    }
    displayInfo=[refProduct displayInfo];
    if (displayInfo ==nil){
        return;
    }
//    CGAffineTransform placementTransform=[[viewProductRep product].placement toScreenScaledCGAffineTransform];
//    if ([[viewProductRep product] linkedRel]!=nil){       
//        SpatialStructure* spatialStructure=(SpatialStructure*)[[[viewProductRep product] linkedRel] relating];
//        placementTransform=CGAffineTransformConcat(placementTransform,[spatialStructure.placement toScreenScaledCGAffineTransform]);
//    }
    
    CGAffineTransform placementTransform=[refProduct.placement toScreenScaledCGAffineTransform];
    if ([[viewProductRep product] linkedRel]!=nil){
        SpatialStructure* spatialStructure=(SpatialStructure*)[[refProduct linkedRel] relating];
        placementTransform=CGAffineTransformConcat(placementTransform,[spatialStructure.placement toScreenScaledCGAffineTransform]);
    }
    rootSpatialStructTransform=CGAffineTransformConcat(placementTransform , [self modelCenterScreenTransform] );
    
//    DisplayInfo* displayInfo=nil;
//    if ([[viewProductRep product] isKindOfClass:[SpatialStructure class]]){
//        displayInfo=[[[viewProductRep product] findParentSpatialStruct] displayInfo];
//    }else{
//        displayInfo=[[viewProductRep product]  displayInfo];
//    }
    ///////////////////End of Trial to add Label directly to sprite anytime/////////////////
        
    
    if (![self shouldDisplayLabel:displayInfo]){
        return;
    }
    ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];    
    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];    
  
    float fontSize=[viewModelVC currentProductLabelSize];//[viewModelVC currentProductLabelSize]/[viewModelVC navUIScrollVC].zoomScale;
//    CGSize productLabelConstraint = CGSizeMake(fontSize*50, fontSize*2);
    CGSize productLabelConstraint = CGSizeMake(fontSize*50, fontSize*2*3);
//    CGSize productLabelSize = [[displayInfo displayLabelText] sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] 
//                                                         constrainedToSize:productLabelConstraint
//                                                             lineBreakMode:NSLineBreakByCharWrapping];
    CGSize productLabelSize = [[displayInfo displayLabelText] sizeWithFont:[UIFont boldSystemFontOfSize:fontSize]
                                                         constrainedToSize:productLabelConstraint
                                                             lineBreakMode:NSLineBreakByWordWrapping];
    ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) displayInfo:displayInfo displayModelUI:self viewProductRep:viewProductRep];        
    
    
    
//    
//    
//    
//    [self setFont:[UIFont boldSystemFontOfSize:fontSize]];//
//    
    
    
    
    
        
    

    
    
    CGPoint origin= CGPointMake(displayInfo.displacement.x*modelScaleFactor, displayInfo.displacement.y*modelScaleFactor);
    CGPoint labelPos= CGPointApplyAffineTransform(origin, rootSpatialStructTransform);  
    CGPoint labelLayerPos=[self.spatialStructViewLayer convertPoint:labelPos toView:productViewLabelLayer];
    [_viewProductLabel setTransform:CGAffineTransformMakeTranslation(labelLayerPos.x, labelLayerPos.y)];
    
    
    
    NSLog(@"Label %@'s fontsize: %f Pos: (%f,%f) Size: (%f,%f) TransformedPos: (%f,%f)", _viewProductLabel.text, fontSize,labelPos.x, labelPos.y, productLabelSize.width,productLabelSize.height, labelLayerPos.x,labelLayerPos.y);
    
    
    [productViewLabelLayer addSubview:_viewProductLabel];
    [_viewProductLabel release];

    
}
/*
-(bool) shouldAttachDisplayInfo: (OPSProduct*) product{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
    if (appDele.modelDisplayLevel==0){
        if ([product isKindOfClass:[Site class]]) return false;
    }
    if (appDele.modelDisplayLevel==1){        
        if ([product isKindOfClass:[Furn class]]) return false;        
        if ([product isKindOfClass:[Bldg class]]) return false;        
    }
    if (appDele.modelDisplayLevel==2){
        if ([product isKindOfClass:[Space class]]) return false;
        
    }
    return true;
}

-(void) attachDisplayInfo: (OPSProduct*) product productTransform:(CGAffineTransform) productTransform{
    //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    if (![self shouldAttachDisplayInfo:product]) return ;
    ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];    
    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];    
    
    DisplayInfo* displayInfo=[product displayInfo];
    
    if (displayInfo){        
        float fontSize=[viewModelVC currentProductLabelSize]/[viewModelVC navUIScrollVC].zoomScale;    
        CGSize productLabelConstraint = CGSizeMake(fontSize*50, fontSize*2);    
        CGSize productLabelSize = [[displayInfo displayLabelText] sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] 
                                                             constrainedToSize:productLabelConstraint 
                                                                 lineBreakMode:UILineBreakModeCharacterWrap];   
        
        
        
        ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) displayInfo:displayInfo displayModelUI:self];        
        
        CGPoint origin= CGPointMake(displayInfo.displacement.x*modelScaleFactor, displayInfo.displacement.y*modelScaleFactor);    
        CGPoint labelPos= CGPointApplyAffineTransform(origin, productTransform);       
        CGPoint labelLayerPos=[spatialStructViewLayer convertPoint:labelPos toView:productViewLabelLayer]; 
        //            [_viewProductLabel setText:[rootSpatialStruct name]];
        //        [_viewProductLabel setTransform:rootTransform];
        [_viewProductLabel setTransform:CGAffineTransformMakeTranslation(labelLayerPos.x, labelLayerPos.y)];        
        //    self.viewProductLabel=_viewProductLabel;
        //    [_viewProductLabel release];
        [productViewLabelLayer addSubview:_viewProductLabel];
        //        [productViewLabelLayer addSubview:_viewProductLabel];
        [_viewProductLabel release];
        
//        [self bringSubviewToFront:[viewModelVC displayModelUI].productViewLabelLayer];
        //        }
        
    }
    
}
*/

/*

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/
/*
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
 */

#pragma mark - View lifecycle

-(void) setMapType:(uint) mapType{
    if (!self.siteMapView){
        [self drawMap:[[self parentViewModelVC] navUIScrollVC].zoomScale];
    }
    if (!self.siteMapView){
        return;
    }
    switch (mapType) {
        case 0:
            self.siteMapView.mapType=MKMapTypeStandard;            
            break;
        case 1:
            self.siteMapView.mapType=MKMapTypeSatellite;
            break;
        case 2:
            self.siteMapView.mapType=MKMapTypeHybrid;
            break;
        default:
            break;
    }
    [self setUserSelectedMapType:self.siteMapView.mapType];

//    [self.siteMapView setNeedsDisplay];
}
-(void) removeAllViewProductLabel{
    for (ViewProductLabel* productLabel in [self.productViewLabelLayer subviews]){
        [productLabel.productRep setViewProductLabel:nil];
        [productLabel removeFromSuperview];
        productLabel=nil;
    }
}
-(void) removeMap{
    [self.siteMapView removeFromSuperview];
    [siteMapView release];
    siteMapView=nil;
    [self.locationManager stopUpdatingLocation];
    [_locationManager release];
    _locationManager=nil;

}
- (void) drawMap:(double) scale{
    NSLog(@"Setup Map");

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    ViewModelVC* viewModelVC=[self parentViewModelVC];        
    Site* site=(Site*) [appDele currentSite];// [[viewModelVC model] root];
      
    
    CGRect selfBound=self.bounds;
    
    

    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
    
    double mapBumpedUpScale=2.0;
    mapFrame=CGRectMake(0, 0, 1024*mapBumpedUpScale,  (768-44-44-20)*mapBumpedUpScale );
    
    
    MKMapView* mapView = [[MKMapView alloc] initWithFrame:mapFrame];
 
    CLLocationCoordinate2D siteLocation =  { site.latitude, site.longitude};
    if ([appDele modelDisplayLevel]==1){
        siteLocation=CLLocationCoordinate2DMake([appDele mapLatitude], [appDele mapLongitude]);
    }
    CLLocationDistance dist1 = 1024/modelScaleFactor/scale *2 ; //x2 for now, a abrupt factor to make map big enough. (//[site.bBox getWidth];//[[viewModelVC model] modelScaleForScreenFactor];//636.9887048804;
    CLLocationDistance dist2 = (768-44-44-20)/modelScaleFactor/scale *2;//x2 for now, an abrupt factor to make map big enough.// [site.bBox getHeight];//[[viewModelVC model] modelScaleForScreenFactor];//1600;//.8380655203;
        
    
    //        CLLocationDistance dist = dist1;
    
    [mapView setRegion:MKCoordinateRegionMakeWithDistance(siteLocation, dist1, dist2) animated:YES];
            
//    MKMapPoint mpTopLeft = mapView.visibleMapRect.origin;        
    MKMapPoint mpTopRight = MKMapPointMake(
                                           mapView.visibleMapRect.origin.x + mapView.visibleMapRect.size.width, 
                                           mapView.visibleMapRect.origin.y);    
    MKMapPoint mpBottomRight = MKMapPointMake(
                                              mapView.visibleMapRect.origin.x + mapView.visibleMapRect.size.width, 
                                              mapView.visibleMapRect.origin.y + mapView.visibleMapRect.size.height);
    
//    CLLocationDistance hDist = MKMetersBetweenMapPoints(mpTopLeft, mpTopRight);
    CLLocationDistance vDist = MKMetersBetweenMapPoints(mpTopRight, mpBottomRight);
    mapView.delegate=self;
    //        double vmrArea = hDist * vDist;
    
    
//    double modelBoundHeight=[[[[site.representation aRepresentationItem] objectAtIndex:0] bBox] getHeight];
//    double modelBoundHeight=[site.bBox getHeight];
//    double modelBoundHeight=[[[viewModelVC model] root].bBox getHeight];//[site.bBox getHeight]
    double modelBoundHeight=(768-44-44-20)/modelScaleFactor/scale;
    [mapView setCenter: CGPointMake(selfBound.size.width/2,selfBound.size.height/2)];    
    mapTransformScale=(1/scale* (vDist/modelBoundHeight)) /mapBumpedUpScale;
    [mapView setTransform:CGAffineTransformMakeScale(mapTransformScale, mapTransformScale)]; 
    
    
    
//    MKMapPoint mpCenter = MKMapPointMake(
//                                              mapView.visibleMapRect.origin.x + mapView.visibleMapRect.size.width/2, 
//                                              mapView.visibleMapRect.origin.y + mapView.visibleMapRect.size.height/2);
    
//    mapView.showsUserLocation=YES;
//    [mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    mapView.showsUserLocation=NO;
    
    
    mapView.mapType=self.userSelectedMapType;// MKMapTypeStandard;
    
    self.siteMapView=mapView;
    [mapView release];
    

    [self addSubview:self.siteMapView];        
    [self startStandardUpdates];
    	
        
//    UIImage* image=[UIImage imageNamed:@"poewredby.png"];
//    UIImageView* imageView=[[UIImageView alloc] initWithImage:image];
//
//    CGPoint pt=[mapView convertCoordinate:userLocation.coordinate toPointToView:self];  
//    
//    
//    double scrollViewzoomScale=[[[self parentViewModelVC] opsNavUI] navUIScroll].zoomScale;;
//    double pinSize=50/scrollViewzoomScale;
//    //    [userLocationImageView setFrame:CGRectMake(pt.x, pt.y, pinSize*1.25, pinSize)];
//    [imageView setFrame:CGRectMake(pt.x-pinSize/2, pt.y-pinSize/2, pinSize, pinSize)];     
//    [self.siteMapView addSubview:imageView];
//    [imageView release];    
    
    
    [self sendSubviewToBack:self.siteMapView];
        
}
/*
// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    // If it's a relatively recent event, turn off updates to save power
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0) {
        // If the event is recent, do something with it.
        NSLog(@"latitude %+.6f, longitude %+.6f\n",
              location.coordinate.latitude,
              location.coordinate.longitude);
            
            if (!self.siteMapView){
                return;
            }

            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            if (![appDele isLiveDataSourceAvailable]){
                return;
            }
            
            NSLog(@"update User location");
            self.userLocation=userLocation;
            
            //    UIImage* image=[UIImage imageNamed:@"onumaPinPoint.png"];
            UIImage* image=[UIImage imageNamed:@"blue-dot.png"];
            UIImageView* imageView=[[UIImageView alloc] initWithImage:image];
            if (self.userLocationImageView!=nil){
                [self.userLocationImageView removeFromSuperview];
            }
            self.userLocationImageView=imageView;
            [imageView release];
            CGPoint pt=[mapView convertCoordinate:userLocation.coordinate toPointToView:self];
            
            
            double scrollViewzoomScale=[[[self parentViewModelVC] opsNavUI] navUIScroll].zoomScale;;
            double pinSize=25/scrollViewzoomScale;
            //    [userLocationImageView setFrame:CGRectMake(pt.x, pt.y, pinSize*1.25, pinSize)];
            [self.userLocationImageView setFrame:CGRectMake(pt.x-pinSize/2, pt.y-pinSize/2, pinSize, pinSize)];
            [self addSubview:self.userLocationImageView];
            [self bringSubviewToFront:self.userLocationImageView];
        
    }
}

- (void)startStandardUpdates
{
    // Create the location manager if this object does not
    // already have one.
    if (self.locationManager==nil){
        CLLocationManager* locationManager = [[CLLocationManager alloc] init];
        self.locationManager=locationManager;
        [locationManager release];
    }
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    self.locationManager.distanceFilter = 500;
    
    [self.locationManager startUpdatingLocation];
}
*/
-(void) hideAllLayer{
//    [self setHidden:true];

    if (self.productViewLabelLayer){
        [self.productViewLabelLayer setHidden:true];
    }
    
    if (self.sitePolygonLayer){
        [self.sitePolygonLayer setHidden:true];
    }
    if (self.siteMapView){
        [self.siteMapView setHidden:true];
    }
    if (self.elemViewLayer) {
        [self.elemViewLayer setHidden:true];
    }
    if (self.spatialStructViewLayer){
        [self.spatialStructViewLayer setHidden:true];
    }
    if (self.spatialStructSlabViewLayer) {
            [self.spatialStructSlabViewLayer setHidden:true];    
    }
    if ([[self parentViewModelVC] mapLogo]){
        [[self.parentViewModelVC mapLogo] setHidden:true];
    }

}


-(void) showAllLayer{
    

//    [self setHidden:false];
    if (self.productViewLabelLayer){
        [self.productViewLabelLayer setHidden:false];
    }
    
    
    
    if (self.sitePolygonLayer){
        [self.sitePolygonLayer setHidden:false];
    }
    if (self.siteMapView){
        [self.siteMapView setHidden:false];
    }
    if (self.elemViewLayer) {
        [self.elemViewLayer setHidden:false];
    }
    if (self.spatialStructViewLayer){
        [self.spatialStructViewLayer setHidden:false];
    }
    if (self.spatialStructSlabViewLayer) {
        [self.spatialStructSlabViewLayer setHidden:false];    
    }
    if ([[self parentViewModelVC] mapLogo]){
        [[self.parentViewModelVC mapLogo] setHidden:false];
    }
     
}
- (void) mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    if (!self.siteMapView){
        return;
    }
    if (![mapView isUserLocationVisible]){
        return;
    }
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    if (![appDele isLiveDataSourceAvailable]){
        return;
    }

    NSLog(@"update User location");        
    self.userLocation=userLocation;
    
//    UIImage* image=[UIImage imageNamed:@"onumaPinPoint.png"];    
    UIImage* image=[UIImage imageNamed:@"blue-dot.png"];
    UIImageView* imageView=[[UIImageView alloc] initWithImage:image];
    if (self.userLocationImageView!=nil){
        [self.userLocationImageView removeFromSuperview];
    }
    self.userLocationImageView=imageView;
    [imageView release];
    CGPoint pt=[mapView convertCoordinate:userLocation.coordinate toPointToView:self];  
    
    
    double scrollViewzoomScale=[[[self parentViewModelVC] opsNavUI] navUIScroll].zoomScale;;
    double pinSize=25/scrollViewzoomScale;
//    [userLocationImageView setFrame:CGRectMake(pt.x, pt.y, pinSize*1.25, pinSize)];
    [self.userLocationImageView setFrame:CGRectMake(pt.x-pinSize/2, pt.y-pinSize/2, pinSize, pinSize)];     
    [self addSubview:self.userLocationImageView];
    [self bringSubviewToFront:self.userLocationImageView];
//    

}
-(void) stopStandardUpdatingLocation{
    if (self.locationManager){
        [self.locationManager stopUpdatingLocation];
    }
}

- (void)startStandardUpdates
{
    // Create the location manager if this object does not
    // already have one.
//    if (self.locationManager == nil){
    CLLocationManager* locationManager = [[CLLocationManager alloc] init];
    self.locationManager=locationManager;
    [locationManager release];
    
//    }
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    // Set a movement threshold for new events.
    self.locationManager.distanceFilter = 10;
    [self.locationManager disallowDeferredLocationUpdates];
    [self.locationManager dismissHeadingCalibrationDisplay];
    [self.locationManager startUpdatingLocation];
}

//- (void)startSignificantChangeUpdates
//{
//    // Create the location manager if this object does not
//    // already have one.
//    if (nil == locationManager)
//        locationManager = [[CLLocationManager alloc] init];
//    
//    locationManager.delegate = self;
//    [locationManager startMonitoringSignificantLocationChanges];
//}


// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    // If it's a relatively recent event, turn off updates to save power
    NSDate* eventDate = newLocation.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0)
    {
        NSLog(@"latitude %+.6f, longitude %+.6f\n",
              newLocation.coordinate.latitude,
              newLocation.coordinate.longitude);
        
        
        MKUserLocation* newUserLocation=[[MKUserLocation alloc] init];
        newUserLocation.coordinate=newLocation.coordinate;
        
        self.userLocation=newUserLocation;
        [newUserLocation release];
        
        [self redrawOnumaPin];
        
        /*
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        if ([appDele modelDisplayLevel]!=0){return;}
        if (!self.siteMapView){ return;}
//        if (![siteMapView isUserLocationVisible]){
//            return;
//        }
        NSLog(@"RedrawOnumaPin");
        double scrollViewzoomScale=[[[self parentViewModelVC] opsNavUI] navUIScroll].zoomScale;;
        double pinSize=25/scrollViewzoomScale;
        //    double pinSize=50;
        //    CGRect userLocationCurrentFrame=userLocationImageView.frame;
        
        //    if (userLocationImageView!=nil){
        //        [userLocationImageView removeFromSuperview];
        //    }
        
        UIImage* image=[UIImage imageNamed:@"blue-dot.png"];
        UIImageView* imageView=[[UIImageView alloc] initWithImage:image];
        if (userLocationImageView!=nil){
            [userLocationImageView removeFromSuperview];
            [userLocationImageView release];
            userLocationImageView=nil;
        }
        
        self.userLocationImageView=imageView;
        [imageView release];
        //    [userLocationImageView setFrame:CGRectMake(userLocationCurrentFrame.origin.x, userLocationCurrentFrame.origin.y, pinSize*1.25, pinSize)];
        //    [userLocationImageView setFrame:CGRectMake(userLocationCurrentFrame.origin.x-pinSize/2, userLocationCurrentFrame.origin.y-pinSize/2, pinSize, pinSize)];
        
//        CGPoint pt=[self.siteMapView convertCoordinate:self.userLocation.coordinate toPointToView:self];

        CGPoint pt=[self.siteMapView convertCoordinate:newLocation.coordinate toPointToView:self];
        
        [userLocationImageView setFrame:CGRectMake(pt.x-pinSize/2, pt.y-pinSize/2, pinSize, pinSize)];
        
        
        //    test.backgroundColor=[UIColor redColor];
        
        [self addSubview:userLocationImageView];    
        [self bringSubviewToFront:userLocationImageView];
        
        
        */
    }
    // else skip the event and process the next one.
}


-(void) redrawOnumaPin{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([appDele modelDisplayLevel]!=0){return;}
    if (!self.siteMapView){ return;}
//    if (![siteMapView isUserLocationVisible]){
//        return;
//    }
    NSLog(@"RedrawOnumaPin");
    double scrollViewzoomScale=[[[self parentViewModelVC] opsNavUI] navUIScroll].zoomScale;;
    double pinSize=25/scrollViewzoomScale;
//    double pinSize=50;
//    CGRect userLocationCurrentFrame=userLocationImageView.frame;
    
//    if (userLocationImageView!=nil){
//        [userLocationImageView removeFromSuperview];
//    }
    
    UIImage* image=[UIImage imageNamed:@"blue-dot.png"];
    UIImageView* imageView=[[UIImageView alloc] initWithImage:image];
    if (userLocationImageView!=nil){
        [userLocationImageView removeFromSuperview];
        [userLocationImageView release];
        userLocationImageView=nil;
    }
    
    self.userLocationImageView=imageView;
    [imageView release];
            //    [userLocationImageView setFrame:CGRectMake(userLocationCurrentFrame.origin.x, userLocationCurrentFrame.origin.y, pinSize*1.25, pinSize)];
//    [userLocationImageView setFrame:CGRectMake(userLocationCurrentFrame.origin.x-pinSize/2, userLocationCurrentFrame.origin.y-pinSize/2, pinSize, pinSize)];
    
    
    
    CGPoint pt=[self.siteMapView convertCoordinate:self.userLocation.coordinate toPointToView:self];  
    
    [userLocationImageView setFrame:CGRectMake(pt.x-pinSize/2, pt.y-pinSize/2, pinSize, pinSize)];
    
    
    //    test.backgroundColor=[UIColor redColor];
    
    [self addSubview:userLocationImageView];    
    [self bringSubviewToFront:userLocationImageView];  

}


//This block force mapkit's location pin to be transparent. Comment the block out if want to show mapkit's default location pin
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    static NSString* AnnotationIdentifier = @"Annotation";
    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
    
    if (!pinView) {
        
        MKPinAnnotationView *customPinView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier] autorelease];   
        if (annotation == mapView.userLocation) customPinView.image = [UIImage imageNamed:@"transparent.png"];
//        else customPinView.image = [UIImage imageNamed:@"mySomeOtherImage.png"];
        customPinView.animatesDrop = NO;
        customPinView.canShowCallout = YES;
        return customPinView;
        
    } else {
        
        pinView.annotation = annotation;
    }
    
    return pinView;
}


//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
//    
//    static NSString* AnnotationIdentifier = @"Annotation";
//    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
//    
//    if (!pinView) {
//        
//        MKPinAnnotationView *customPinView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier] autorelease];   
//        if ([annotation isKindOfClass:[MKUserLocation class]]) { 
////            customPinView.image = [UIImage imageNamed:@"myCarImage.png"];
//            customPinView.alpha=0;
//        }
//        else {
////            customPinView.image = [UIImage imageNamed:@"mySomeOtherImage.png"];   
//        }
//
//        customPinView.animatesDrop = NO;
//        customPinView.canShowCallout = YES;
//        return customPinView;
//        
//    } else {
//        
//        pinView.annotation = annotation;
//    }
//    
//    return pinView;
//}
//- (void) mapViewDidFinishLoadingMap:(MKMapView *)mapView{    
//    if (self.siteMapView){
//        if (self.siteMapView.mapType==MKMapTypeStandard){
//            [self.siteMapView setMapType:MKMapTypeSatellite];
//            [self.siteMapView setNeedsDisplay];
//            [self.siteMapView setMapType:MKMapTypeHybrid];
//            [self.siteMapView setNeedsDisplay];            
//            [self.siteMapView setMapType:MKMapTypeStandard];
//        }
//        if (self.siteMapView.mapType==MKMapTypeSatellite){
//            [self.siteMapView setMapType:MKMapTypeStandard];
//            [self.siteMapView setNeedsDisplay];            
//            [self.siteMapView setMapType:MKMapTypeHybrid];
//            [self.siteMapView setNeedsDisplay];            
//            [self.siteMapView setMapType:MKMapTypeSatellite];
//        }
//        if (self.siteMapView.mapType==MKMapTypeHybrid){
//            [self.siteMapView setMapType:MKMapTypeStandard];
//            [self.siteMapView setNeedsDisplay];            
//            [self.siteMapView setMapType:MKMapTypeSatellite];
//            [self.siteMapView setNeedsDisplay];            
//            [self.siteMapView setMapType:MKMapTypeHybrid];
//        }
//    }
//    
//}

/*
- (void) mapViewDidFinishLoadingMap:(MKMapView *)mapView{    
    NSLog(@"Finished loading");    
    if (self.capturedMap==nil){
        [self captureMapView];
    }
 
}
 */
/*
-(void) captureMapView{
    if (capturedMap){
        return;
    }
    
    
    
    
    CaptureView* cloneMap=[[CaptureView alloc] initWithView:siteMapView captureFrame:mapFrame];
    cloneMap.alpha=0.8;
    self.capturedMap=cloneMap;
    [cloneMap release];
    
//    [self addSubview:capturedMap];     
//    CGRect selfBound=self.bounds;    
//    [capturedMap setCenter: CGPointMake(selfBound.size.width/2,selfBound.size.height/2)];
//    [capturedMap setTransform:CGAffineTransformMakeScale(mapTransformScale, mapTransformScale)];
//    capturedMap.frame=CGRectOffset(capturedMap.frame, 0, 0);    
//    [self bringSubviewToFront:capturedMap];
    
    
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
    OPSProjectSite* projectSite=appDele.activeProjectSite;  
    
            
    NSString* localSiteParentFolderPath=[projectSite.dbPath stringByDeletingLastPathComponent];  
    
    ViewModelVC* viewModelVC=[self parentViewModelVC];
    Site* site=(Site*) [[viewModelVC model] root];
    double zoomScale=[[[self parentViewModelVC] navUIScrollVC] zoomScale];
    
    
    NSString* localImgName=[NSString stringWithFormat:@"%f_SiteMap_%d_%d.jpeg",zoomScale,appDele.activeStudioID,site.ID];    
    NSString *fullPath = [localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", localImgName]]; 
    //add our image to the path    

    if ([UIImageJPEGRepresentation([capturedMap imageCapture], 1) writeToFile:fullPath atomically:YES]) {
        //NSLog(@"save ok");
    }else{
        //NSlog (@"Failed Saving Jpeg");
    }

   
}
 */
/*
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
*/


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

//
//  CVector.m
//  ProjectView
//
//  Created by onuma on 08/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "CVector.h"
#import <math.h>
@implementation CVector
@synthesize x=_x;
@synthesize y=_y;
@synthesize z=_z;
@synthesize b3=_b3;

-(id) init{
    self=[super init];
    if (self){
        self.x=0.0;
        self.y=0.0;
        self.b3=false;
    }
    return self;
}
-(id) initWithX:(double)x y:(double)y{
    self=[super init];
    if (self){
        self.x=x;
        self.y=y;
        self.b3=false;
        
    }
    return self;
}

-(id) initWithX:(double)x y:(double)y z:(double)z{
    self=[super init];
    if (self){
        self.x=x;
        self.y=y;
        self.z=z;
        self.b3=true;
        
    }
    return self;
}


-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    CVector *newVector = [[CVector alloc] init];
    //    newCopy.obj = [obj copyWithZone: zone];
    [newVector setX:self.x];
    [newVector setY:self.y];
    [newVector setZ:self.z];
    return newVector;
}


-(void) incrementX:(double) x {
    self.x +=x;
}


-(void) incrementY:(double) y {
    self.y +=y;
}


-(void) incrementZ:(double) z {
    self.z +=z;
}

-(void) incrementComponentsWithX:(double)x y:(double)y{
    self.x += x;
    self.y += y;
//    if ($b3) z += incZ;
}

-(void) addsVector:(CVector*)v{
    self.x += v.x;
    self.y += v.y;
    if (self.b3) self.z += v.z;
}


-(void) subtractVector:(CVector*)v{
    self.x -= v.x;
    self.y -= v.y;
    if (self.b3) self.z -= v.z;
    
}

-(void) divide:(double)s{
    self.x/=s;
    self.y/=s;
    if (self.b3) self.z/=s;
    
}
-(void) scalar:(double)s{
    self.x *= s;
    self.y *= s;
    if (self.b3) self.z *= s;
}

-(double) dotProductWithVector:(CVector*) v {
    double n=0;
    
    if (self.b3) n = (self.x*v.x)+(self.y*v.y)+(self.z*v.z);
    else n = (self.x*v.x)+(self.y*v.y);
    return n;
}
-(void) crossProduct:(CVector*)v resultCrossVector:(CVector*)cross {    
//-(CVector*) crossProduct:(CVector*)v {
//    CVector* cross=[[[CVector alloc] initWithX:0.0 y:0.0 z:0.0] autorelease];
    
    if (self.b3) {
        cross.x = (self.y*v.z)-(self.z*v.y);
        cross.y = (self.z*v.x)-(self.x*v.z);
        cross.z = (self.x*v.y)-(self.y*v.x);
    }
    else {
        // ref: http://mathquest.com/discuss/sci.math/a/m/415647/415659
        cross.x = 0; //(y*V.z)-(z*V.y) = ((y*0)-(0*V.y) = 0
        cross.y = 0; //(z*V.x)-(x*V.z) = (0*V.x)-(x*0) = 0
        cross.z = (self.x*v.y)-(self.y*v.x);
    }
    
//    return cross;
}
-(double) norm{
    double n;
    
    
    if (self.b3) 	{ n = sqrt(  ((self.x*self.x)+(self.y*self.y)+(self.z*self.z)) );}
    else 		{ n = sqrt((self.x*self.x)+(self.y*self.y));   	}
    
    return n;
}

-(void) unitVectorWithResultUnitVector:(CVector*)unit {
//-(CVector*) unitVector{
//    CVector* unit=[[self copy] autorelease];
    double norm=[self norm];
    unit.x=self.x/norm;
    unit.y=self.y/norm;
    
    if (self.b3) { unit.z = self.z/norm; }
//    return unit;
}





-(void) normalize {
    double norm=[self norm];
    self.x/=norm;
    self.y/=norm;
    
    if (self.b3){self.z/=norm;}
}

-(void) perp:(CVector*)resultPerpVector {
//    var v:Vector;
//    
//    if ($b3) v = new Vector(-y, x, z);
//    else v = new Vector(-y, x);    
    if (self.b3){
        resultPerpVector.x=-self.y;
        resultPerpVector.y=self.x;
        resultPerpVector.z=self.z;
    }else{
        resultPerpVector.x=-self.y;
        resultPerpVector.y=self.x;
    }

    
//    return v;
}


-(double) exteriorPerpProduct:(CVector*)v2{
    return (self.x*v2.y- self.y*v2.x);
}

-(void) perp{
    //    var v:Vector;
    //
    //    if ($b3) v = new Vector(-y, x, z);
    //    else v = new Vector(-y, x);
    double tmpX=self.x;
    double tmpY=self.y;
    
    
    if (self.b3){
        double tmpZ=self.z;
        
        self.x=-tmpY;
        self.y=tmpX;
        self.z=tmpZ;
    }else{
        self.x=-tmpY;
        self.y=-tmpX;
    }
    
    
    //    return v;
}


-(double) scalarComponent:(CVector*) v{
    
    //    return   this.dotProduct( V.unitVector() );
    CVector* vUnit=[[CVector alloc] init];
    [v unitVectorWithResultUnitVector:vUnit];
    double scalar=[self dotProductWithVector:vUnit];
    [vUnit release];vUnit=nil;
    
    return scalar;
    
//    return [self dotProductWithVector:[v unitVector]];
}


          
-(void) swapWithVector:(CVector*)v{
    double tempX;
    double tempY;
    double tempZ;
    
    
    tempX = self.x;
    tempY = self.y;
    
    
    self.x = v.x;
    self.y = v.y;
    v.x = tempX;
    v.y = tempY;
    if (self.b3) {
        tempZ = self.z;
        self.z = v.z;
        v.z = tempZ;
    }
}


-(double) angleVector:(CVector*)v {    
    //return this.dotProduct(V)/(this.norm()*V.norm());  //Commenteded by A. Man on  20 Nov 2008 :
    //these are added by A. Man on 20 Nov 2008!'''''''''''''''''''''''''''''''''
    //	    	var thisUnitVector:Vector=this.unitVector();
    //	    	var targetUnitVector:Vector=V.unitVector();
    //
    double angle;
    double dotProduct=[self dotProductWithVector:v];
    double normalProduct=[self norm]*[v norm];
    double acosFactor=round(dotProduct/normalProduct);
    
    acosFactor = (double) ((int) (acosFactor * 1000000.0)) / 1000000.0;

    
    
    
    
    angle=acos(acosFactor);
    
    CVector* newUnitVector=[[CVector alloc] init];
    
    [self unitVectorWithResultUnitVector:newUnitVector];
    
    [newUnitVector perp];
   
    
    if ([newUnitVector dotProductWithVector:v]<0){
        angle=M_PI*2-angle;
    }
//    
//    if (  (this.unitVector().perp()).dotProduct(V)<0){
//        angle=Math.PI*2-angle;
//    }
    
    [newUnitVector release];newUnitVector=nil;
    return angle;
    //Add finish by A. Man on 20 Nov 2008!-----------------------------------------
    
    
}


@end

//
//  StructForBIMPlanColor.m
//  ProjectView
//
//  Created by Alfred Man on 4/20/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "StructForBIMPlanColor.h"

@implementation StructForBIMPlanColor

@synthesize name=_name;
@synthesize color=_color;
@synthesize ID=_ID;
-(void) dealloc{
    [_name release];_name=nil;
    [_color release];_color=nil;
    [super dealloc];
}
+(UIColor*) colorWithHexString:(NSString *)stringToConvert{
    
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];  
    
    // String should be 6 or 8 characters  
    if ([cString length] < 6) return [UIColor clearColor];  //DEFAULT_VOID_COLOR;
    
    // strip 0X if it appears  
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];  
    
    if ([cString length] != 6) return [UIColor clearColor];//DEFAULT_VOID_COLOR;  
    
    // Separate into r, g, b substrings  
    NSRange range;  
    range.location = 0;  
    range.length = 2;  
    NSString *rString = [cString substringWithRange:range];  
    
    range.location = 2;  
    NSString *gString = [cString substringWithRange:range];  
    
    range.location = 4;  
    NSString *bString = [cString substringWithRange:range];  
    
    // Scan values  
    unsigned int r, g, b;  
    [[NSScanner scannerWithString:rString] scanHexInt:&r];  
    [[NSScanner scannerWithString:gString] scanHexInt:&g];  
    [[NSScanner scannerWithString:bString] scanHexInt:&b];  
    
    return [UIColor colorWithRed:((float) r / 255.0f)  
                           green:((float) g / 255.0f)  
                            blue:((float) b / 255.0f)  
                           alpha:1.0f];  
    
}
-(id) initWithID:(uint) ID name:(NSString*) name colorStr:(NSString*) colorStr{
    self=[super init];
    if (self){
        //        uint dec;
        [self setID:ID];
        [self setName:name];
        [self setColor:[StructForBIMPlanColor colorWithHexString:colorStr]];
        //        NSString *hexString = colorStr;
        //        NSScanner *scan = [NSScanner scannerWithString:hexString];
        //        if ([scan scanHexInt:&dec]){
        //            [self setColor:dec];
        ////            NSLog(@"Dec value, %d is sccessfully scanned.", dec);
        //        }
        //        else{
        ////            NSLog(@"No dec value is scanned.");    
        //        }
        //        
    }
    return self;
    
}

@end

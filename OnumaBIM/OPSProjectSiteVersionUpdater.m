//
//  OPSProjectSiteVersionUpdater.m
//  ProjectView
//
//  Created by Alfred Man on 8/15/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "OPSProjectSiteVersionUpdater.h"
#import "OPSStudio.h"
#import "ProjectViewAppDelegate.h"
//#import "ZipArchive.h"


#import "ZipFile.h"
#import "ZipException.h"
#import "FileInZipInfo.h"
#import "ZipWriteStream.h"
#import "ZipReadStream.h"
#import "LocalProjectListVC.h"



@implementation OPSProjectSiteVersionUpdater

@synthesize localProjectListVC=_localProjectListVC;
-(void) dealloc{
    [_localProjectListVC release];_localProjectListVC=nil;    
    [super dealloc];
}

-(id) initWithOPSProjectSite:(OPSProjectSite*) projectSite  localProjectListVC:(LocalProjectListVC*) localProjectListVC{
    self=[super init:projectSite.ID name:projectSite.name shared:projectSite.shared projectID:projectSite.projectID projectName:projectSite.projectName iconName:projectSite.iconName];
    
    if (self){
        self.dbPath=projectSite.dbPath;
        self.localProjectListVC=localProjectListVC;
    }
    return self;
}



- (void) updateDBFromServer:(NSArray*) paramArray{
    //- (void) copyModelFromServer{//:(OPSProjectSite*) projectSite{      
//    bool bNoGraphicData=false;
//    if (paramArray!=nil){
//        NSNumber* numNoGraphicData=[paramArray objectAtIndex:0];
//        bNoGraphicData=[numNoGraphicData boolValue];
//    }
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];  
    //    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=44&siteID=%d&filename=OPS", [project ID]];
    
    
    
    
    NSString *urlString = nil;
//    if (bNoGraphicData){        
//        urlString=[NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=%d&siteID=%d&beta=1&siteInfoOnly=1",[appDele activeLiveStudio].ID, [[appDele activeProjectSite] ID]];
//    }else{
    
        
//    urlString=[NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=%d&siteID=%d&beta=1&versionLagStr=%@",[appDele activeLiveStudio].ID, [[appDele activeProjectSite] ID],[appDele activeProjectSite].versionLagStr];        
    
    urlString=[NSString stringWithFormat: @"https://www.onuma.com/plan/OPS/export/saveSQLitePatch.php?currentVersion=%@&u=%@&p=%@", [appDele activeProjectSite].versionLagStr,[ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]],[ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]]];
    
    
//    }
    NSURL *url = [NSURL URLWithString: urlString];
    //    UIImage *image = [[UIImage imageWithData: [NSData dataWithContentsOfURL: url]] retain];
    
    
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:url
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:1200.0];
    // create the connection with the request
    // and start loading the data
    // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file    
    //    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];    
    self.theConnection=aConnection;
    [aConnection release];
    if (theConnection) {
        // Create the NSMutableData that will hold
        // the received data
        // receivedData is declared as a method instance elsewhere
        
        connectionData=[[NSMutableData data] retain];
    } else {    
        // Inform the user that the connection failed.        
		UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[connectFailMessage show];
		[connectFailMessage release];
    }            
    return;        
}


#pragma mark NSURLConnection methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{  
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    downloadLabel.text=[NSString stringWithFormat:@"Downloading File from Server for Site: %@",[appDele activeProjectSite].name]; 
    
//    NSMutableData* connectionData;
    [connectionData setLength:0];    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.            
    
    [connectionData appendData:data];
    NSLog(@"data length: %d",[connectionData length]);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    // receivedData is declared as a method instance elsewhere
    
    //[connection release
    [theConnection release];
    theConnection=nil;
    [connectionData release];
    connectionData=nil;
    
    
    // inform the user
    UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"Conection Failed"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
    [didFailWithErrorMessage show];
    [didFailWithErrorMessage release];
	
    //inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];
}

-(void) execudteSQLStmt: (NSString*) sqlStr{
     
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate]; 
        
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *databaseFolderPath = [paths objectAtIndex:0]; 
        
//        databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], [appDele activeLocalStudio].ID, [appDele activeLocalStudio].name, [appDele activeLocalStudio].iconName ]]; 	    
        
        sqlite3_stmt *attachedFileStmt; 
        const char* attachedFileSql=[[NSString stringWithFormat:@"%@",sqlStr] UTF8String];            
        sqlite3 *database = nil;    
        
        if (sqlite3_open ([[self dbPath] UTF8String], &database) == SQLITE_OK) {      
            if(sqlite3_prepare_v2(database, attachedFileSql, -1, &attachedFileStmt, NULL) == SQLITE_OK) {
                while( sqlite3_step(attachedFileStmt) == SQLITE_ROW) {                                 
                    
//                    NSString* attachedFileName= nil;
//                    if ((char *) sqlite3_column_text(attachedFileStmt, 0)!=nil){
//                        attachedFileName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(attachedFileStmt, 0)];               
//                    }else{
//                        attachedFileName=[NSString stringWithFormat:@""];
//                    }                  
//                    
//                    if (![attachedFileName hasSuffix:@".jpg"]){
//                        continue;
//                    }                                
//                    NSString* imagePath=[databaseFolderPath stringByAppendingPathComponent:attachedFileName];
//                    
//                    
//                    NSFileManager *fileManager = [NSFileManager defaultManager]; 
//                    [fileManager removeItemAtPath: imagePath error:NULL];  
//                    
                }
            }        
            sqlite3_close(database);
        }else{
            sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
        }                          
        
        
    
//    [super openProjectToDisplay:selectedProjectSite];
        
        //    [self.navigationController popViewControllerAnimated:TRUE];
 

    
    
    
    NSString* databaseFolderPath=[self.dbPath stringByDeletingLastPathComponent];
                                  
//                                  stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",[appDele defUserName],[appDele activeLiveStudio].ID,[appDele activeLiveStudio].name,[appDele activeLiveStudio].iconName]] ;


    
    //-----------------------------------Create PList File of project inside Studio---------------------------------------------------------------
    NSMutableDictionary *dictionary=nil;
    //    NSString *path = [[NSBundle mainBundle] bundlePath];     

    NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
    NSFileManager *NSFm= [NSFileManager defaultManager]; 
    if(![NSFm fileExistsAtPath:dictionaryPath isDirectory:NO]){        
        if(![NSFm createFileAtPath:dictionaryPath contents:[NSData dataWithContentsOfFile:@""] attributes:Nil ]  )        {
            NSLog(@"Error: Create projectInfoFile failed");
        }
        dictionary = [NSMutableDictionary dictionaryWithObject:[self toDictionaryObject] forKey:self.name];        
    }else{
        dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];           
        [dictionary setObject:[self toDictionaryObject] forKey:self.name];  
    }    
    // write xml representation of dictionary to a file
    [dictionary writeToFile:dictionaryPath atomically:YES];

}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{        
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    OPSProjectSite* projectSiteSelected=[appDele activeProjectSite];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; 
    
    
    NSString* databaseFolderPath=nil;
//    if (isOnlyLoadingSiteInfo){
//        databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",[appDele defUserName],[appDele activeLiveStudio].ID,[appDele activeLiveStudio].name,[appDele activeLiveStudio].iconName]] ;
//    }else{
        databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName],[appDele activeLiveStudio].ID,[appDele activeLiveStudio].name,[appDele activeLiveStudio].iconName]] ;
//    }
    NSFileManager *NSFm= [NSFileManager defaultManager]; 
    BOOL isDir=YES;    
    if(![NSFm fileExistsAtPath:databaseFolderPath isDirectory:&isDir])
        if(![NSFm createDirectoryAtPath:databaseFolderPath withIntermediateDirectories:isDir attributes:Nil error:Nil])
            NSLog(@"Error: Create folder failed");
    
    
    
    //    [projectSite setDbPath: [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%@_I_%@/%@.sqlite",userName,activeLiveStudio.name,activeLiveStudio.iconName, [projectSite name]]] ];     
   /* 
    NSString* dbPath=nil;
//    if (isOnlyLoadingSiteInfo){
//        dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/tmpSiteInfo.sqlite"]] ];
//    }else{
        dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.sqlite", [projectSiteSelected name]]] ];
//    }
    [projectSiteSelected setDbPath: dbPath];   
    [dbPath release];    
    */        
        
    
    
    NSString* tmpSQLStmtZipPath=nil;
    //    if (isOnlyLoadingSiteInfo){
    //        dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/tmpSiteInfo.sqlite"]] ];
    //    }else{
//    tmpSQLStmtZipPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.sqlite", [projectSiteSelected name]]] ];
    tmpSQLStmtZipPath=[[NSString alloc] initWithString:[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"tmpSQLStmtZip.zip"]]];
    
    //    }
//    [projectSiteSelected setDbPath: dbPath];   
//    [tmpSQLStmtZipPath release];        
    [connectionData writeToFile:tmpSQLStmtZipPath atomically:YES];    



//    //-------/Users/onuma/iOS/ProjectView/ZipArhieve----------------------------Create PList File of project inside Studio---------------------------------------------------------------
//    NSMutableDictionary *dictionary=nil;
//    //    NSString *path = [[NSBundle mainBundle] bundlePath];        
//    
//    NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
//    if(![NSFm fileExistsAtPath:dictionaryPath isDirectory:NO]){        
//        if(![NSFm createFileAtPath:dictionaryPath contents:[NSData dataWithContentsOfFile:@""] attributes:Nil ]  )        {
//            NSLog(@"Error: Create projectInfoFile failed");
//        }
//        dictionary = [NSMutableDictionary dictionaryWithObject:[projectSiteSelected toDictionaryObject] forKey:projectSiteSelected.name];
//        
//    }else{
//        dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];           
//        [dictionary setObject:[projectSiteSelected toDictionaryObject] forKey:projectSiteSelected.name];  
//    }    
//    // write xml representation of dictionary to a file
//    [dictionary writeToFile:dictionaryPath atomically:YES];
//    //--------------------------------------------------------------------------------------------------    

    
    
    ZipFile *unzipFile = [[ZipFile alloc] initWithFileName:tmpSQLStmtZipPath mode:ZipFileModeUnzip];
    
    [tmpSQLStmtZipPath release];
    NSArray *infos = [unzipFile listFileInZipInfos];        
    for (FileInZipInfo *info in infos) {
        NSLog(@"- %@ %@ %d (%d)", info.name, info.date, info.size, info.level);        
        // Locate the file in the zip
        [unzipFile locateFileInZip:info.name];        
        // Expand the file in memory
        if ([[info.name pathExtension] isEqualToString:@"sql"]){             
            ZipReadStream *read= [unzipFile readCurrentFileInZip];
            NSMutableData *data= [[NSMutableData alloc] initWithLength:8192];// autorelease];
            int bytesRead= [read readDataWithBuffer:data];
            [read finishedReading];
            NSLog(@"byte Read: %d",bytesRead);  
            
            if (bytesRead>0){
                NSString* testStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSLog(@"testStr:%@",testStr);                                
                
                
                [self execudteSQLStmt:testStr];
                
                
                [testStr release];
                
                
                
            }

            [data release];
        }
//        [data release];
    }
    
    
    
//    NSString* tmpSQLStmtUnZipPath=nil;
//    tmpSQLStmtUnZipPath=[[NSString alloc] initWithString:[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"tmpSQLStmtUnZipPath"]]];
//    
//    
//    
//    uint BUFFER_SIZE=256;
//    NSFileHandle *file= [NSFileHandle fileHandleForWritingAtPath:tmpSQLStmtUnZipPath];
//    NSMutableData *buffer= [[NSMutableData alloc] initWithLength:BUFFER_SIZE];
//    ZipReadStream *read= [unzipFile readCurrentFileInZip];
//    
//    // Read-then-write buffered loop
//    do {
//        
//        // Reset buffer length
//        [buffer setLength:BUFFER_SIZE];
//        
//        // Expand next chunk of bytes
//        int bytesRead= [read readDataWithBuffer:buffer];
//        if (bytesRead > 0) {
//            
//            // Write what we have read
//            [buffer setLength:bytesRead];
//            [file writeData:buffer];
//            
//        } else
//            break;
//        
//    } while (YES);
//    
//    // Clean up
//    [file closeFile];
//    [read finishedReading];
//    [buffer release];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    [unzipFile close];
    [unzipFile release];
    /*
    
    
	ZipArchive *zipArchive = [[ZipArchive alloc] init];
	if([zipArchive UnzipOpenFile:tmpSQLStmtZipPath]) {
		NSLog(@"Archive Open Success");
    } else  {
        NSLog(@"Failure To Open Archive");
    }
                        
                        
    
    //    [self removeDownloadDisableOverlay];    
    //    self.navigationItem.hidesBackButton = FALSE;
    [appDele removeLoadingViewOverlay];
    
    
//    if (isOnlyLoadingSiteInfo){
//        OPSProjectSite* projectSite=self.projectSiteSelected;
//        [appDele setActiveProjectSite:self.projectSiteSelected];
//        [appDele readUnitStructWithDBPath:[projectSite dbPath]];                
//        [appDele displayProjectListSiteInfoVC: self];
//    }else{
        [super openProjectToDisplay:projectSiteSelected];        
//    }        
              */
    [theConnection release];
    theConnection=nil;
    [connectionData release];
    connectionData=nil;
    
    
    [appDele removeLoadingViewOverlay];
    [self.localProjectListVC openProjectToDisplay:self];
     

    
    
    
    
    
    
}


@end

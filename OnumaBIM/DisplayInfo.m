//
//  DisplayInfo.m
//  ProjectView
//
//  Created by Alfred Man on 1/19/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "DisplayInfo.h"
#import "OPSProduct.h"
#import "ProjectViewAppDelegate.h"
#import "Space.h"
#import "SitePolygon.h"
#import "UtilityLine.h"
#import "Bldg.h"
@implementation DisplayInfo
//@synthesize aDisplayLabelText;
@synthesize product;
@synthesize displacement;


- (id) initWithProduct: (OPSProduct*) _product displacement:(CGPoint)_displacement{//aDisplayLabelText:(NSMutableArray*) _aDisplayLabelText{
    self=[super init];
    if (self){
        [product release];
        product=_product;        
        displacement=_displacement;
        
        //    [aDisplayLabelText release];
        //    [_aDisplayLabelText alloc];
        //    aDisplayLabelText=_aDisplayLabelText;        
    }
    
    return self;
}
-(NSString*) displayLabelText{
//                double area=[(RepresentationItem*) [[[rootSpatialStruct representation] aRepresentationItem] objectAtIndex:0] area];

    if ([product isKindOfClass:[SitePolygon class]]){
        SitePolygon* sitePolygon=(SitePolygon*)product;
        NSString* str=nil;        
        str=[NSString stringWithFormat:@"%d\n%@",sitePolygon.ID,[sitePolygon type]];
        
        return str;
    }
    
    if ([product isKindOfClass:[Bldg class]]){
        Bldg* bldg=(Bldg*)product;
        NSString* bldgLabelStr=nil;


            bldgLabelStr=[NSString stringWithFormat:@"%d\n%@",bldg.ID,[bldg name]];           
      
        return bldgLabelStr;
    }
    
    
    if ([product isKindOfClass:[Space class]]){
        Space* space=(Space*)product;
        NSString* spaceLabelStr=nil;
//        if ([space spaceNumber]!=nil && ![[space spaceNumber] isEqualToString:@""]){
//            spaceLabelStr=[NSString stringWithFormat:@"Space: %@ | %@ | %@",[space name],[space spaceNumber], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]]; 
//        }else{
//            spaceLabelStr=[NSString stringWithFormat:@"Space: %@ | %@",[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]];             
//        }
        if ([space spaceNumber]){  
            spaceLabelStr=[NSString stringWithFormat:@"%@\n%@\n%@",[space spaceNumber],[space name],[ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]];           
        }else{   
            spaceLabelStr=[NSString stringWithFormat:@"%@\n%@",[space name],[ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]];      
            
        }
//        spaceLabelStr=[NSString stringWithFormat:@"%@",[space name]];        
        return spaceLabelStr;
    }        
    
    return product.name;
    
}

-(void) dealloc{
    [super dealloc];
}
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
*/
#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/
/*
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
*/
@end

//
//  AttachmentInfoTableUploadFile.h
//  ProjectView
//
//  Created by Alfred Man on 11/6/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AttachmentInfoDetail;
@class AttachmentImageView;
@class OPSProjectSite;
@interface AttachmentInfoTableUploadFile : NSObject <NSURLConnectionDelegate>
{
    AttachmentInfoDetail* _attachmentInfoDetail;
    OPSProjectSite* _projectSite;
    uint _attachFileID;
    NSString* _filePath;
    NSString* _fileName;
    NSString* _fileTitle;
    NSString* _attachedTo;
    NSInteger _referenceID;
    NSString* _uploadDate;
    AttachmentImageView* _attachmentImageView;
    
    
    NSMutableData* _myData;
    NSMutableData* _connectionData;
    NSURLConnection* _theConnection;
}
@property (nonatomic, retain) NSMutableData* myData;
@property (nonatomic, assign) uint attachFileID;
@property (nonatomic, assign) OPSProjectSite* projectSite;
@property (nonatomic, assign) AttachmentImageView* attachmentImageView;
@property (nonatomic, retain) NSMutableData* connectionData;
@property (nonatomic, retain) NSURLConnection* theConnection;
@property (nonatomic, assign) AttachmentInfoDetail* attachmentInfoDetail;
@property (nonatomic, retain) NSString* filePath;
@property (nonatomic, retain) NSString* fileName;
@property (nonatomic, retain) NSString* fileTitle;
@property (nonatomic, retain) NSString* attachedTo;
@property (nonatomic, assign) NSInteger referenceID;
@property (nonatomic, retain) NSString* uploadDate;

//-(id)initWithAttachmentInfoDetail:(AttachmentInfoDetail*)attachmentInfoDetail filePath:(NSString*)filePath;



//-(NSData*) upload:(NSArray*) paramArray;
-(id)initWithAttachmentImageView:(AttachmentImageView*) attachmentImageView attachmentInfoDetail:(AttachmentInfoDetail*)attachmentInfoDetail filePath:(NSString*)filePath fileName:(NSString*) fileName fileTitle:(NSString*) fileTitle attachedTo:(NSString*)attachedTo referenceID:(NSInteger) referenceID uploadDate:(NSString*) uploadDate;

-(id)initWithProjectSite:(OPSProjectSite*)projectSite attachFileID:(uint)attachFileID filePath:(NSString*)filePath fileName:(NSString*) fileName fileTitle:(NSString*) fileTitle attachedTo:(NSString*)attachedTo referenceID:(NSInteger) referenceID uploadDate:(NSString*) uploadDate;

-(void) upload:(NSArray*) paramArray;
@end

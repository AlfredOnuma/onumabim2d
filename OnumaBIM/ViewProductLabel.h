//
//  ViewProductLabel.h
//  ProjectView
//
//  Created by Alfred Man on 12/2/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ViewProductLabelDelegate
//- (OPSModel*) getModel;
@end
@class ViewProductRep;
@class DisplayModelUI;
@class DisplayInfo;
@class ViewModelVC;
@interface ViewProductLabel : UILabel {
    DisplayInfo* displayInfo;    
    id<ViewProductLabelDelegate> delegate;
    ViewProductRep* productRep;
//    CGAffineTransform productTransform;

//
//    CGPoint displacement;
//    NSString* productName;
//    double area;
//    NSMutableArray* displayTextArray;
}

@property (assign) id <ViewProductLabelDelegate> delegate;
@property (nonatomic, assign) ViewProductRep* productRep;
@property (nonatomic, assign) DisplayInfo* displayInfo;
//@property (nonatomic, assign) CGAffineTransform productTransform;
//@property (nonatomic, assign) CGPoint displacement;
//@property (nonatomic, retain) NSString* productName;
//@property (nonatomic, assign) double area;
//@property (nonatomic, retain) NSMutableArray* displayTextArray;

//- (id) initWithFrame:(CGRect)frame name: (NSString*)newProductName area:(double) newArea  displayModelUI:(DisplayModelUI*) displayModelUI;

//- (id) initWithFrame:(CGRect)frame displayInfo: (DisplayInfo*)_displayInfo  displayModelUI:(DisplayModelUI*) displayModelUI productTransform:(CGAffineTransform)_productTransform;;
- (id) initWithFrame:(CGRect)frame displayInfo:(DisplayInfo *)_displayInfo displayModelUI:(DisplayModelUI *)_displayModelUI viewProductRep:(ViewProductRep*)_viewProductRep;
- (ViewModelVC*) parentViewModelVC;
- (void) hide;
- (void) display;
@end

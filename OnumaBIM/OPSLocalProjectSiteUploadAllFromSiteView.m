//
//  OPSLocalProjectSiteUploadAllFromSiteView.m
//  ProjectView
//
//  Created by Alfred Man on 8/1/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "OPSLocalProjectSiteUploadAllFromSiteView.h"

#import "ProjectViewAppDelegate.h"

#import "AttachmentInfoTable.h"

//#import "OPSLocalProjectSiteInAttachmentInfoDetail.h"
#import "ProjectViewAppDelegate.h"
#import "OPSStudio.h"
#import "AttachmentInfoDetail.h"
#import "ViewModelToolbar.h"
@implementation OPSLocalProjectSiteUploadAllFromSiteView


@synthesize attachmentInfoTable=_attachmentInfoTable;
//uint const OPSLocalProjectSiteUploadBeforeDownload_CONFIRM_UPLOADNOINTERNET=1;


-(void) trashAllAttachmentEntry{
    [super trashAllAttachmentEntry];
    
    [self.attachmentInfoTable.viewModelToolbar.popoverController dismissPopoverAnimated:YES];
    [self.attachmentInfoTable.viewModelToolbar refreshToolbar];
//    [[self.attachmentInfoTable tableView] reloadData];
}
-(id) initWithAttachmentInfoTable:(AttachmentInfoTable*)attachmentInfoTable{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[ [UIApplication sharedApplication] delegate];
    
    self=[super initWithLiveOPSProjectSite:[appDele activeProjectSite]];
    if (self){
        self.attachmentInfoTable=attachmentInfoTable;
    }
    return self;    
}

-(void) uploadProjectSite:(NSArray*) paramArray{
    
    
//    if ([paramArray count]>0){
//        //        if ([[paramArray objectAtIndex:2] isKindOfClass:[ViewPlanSiteAndBldgInfoVC class]]){
//        //            viewPlanSiteAndBldgInfoVC=[paramArray objectAtIndex:2];        
//        //        }else{    
//        self.viewPlanSiteAndBldgInfoVC=[paramArray objectAtIndex:0];
//        //        }
//    }else{
//        self.viewPlanSiteAndBldgInfoVC=nil;
//    }
    [super uploadProjectSite:paramArray];     
}

//-(void) launchUploadBeforeDownloadWarning:(NSArray*) paramArray{
//    
//    //    
//    //    
//    //    UIAlertView *alert = [[UIAlertView alloc] 
//    //                          initWithTitle: @"No Internet For Upload"
//    //                          message: @""
//    //                          delegate: self
//    //                          cancelButtonTitle: @"OK"
//    //                          otherButtonTitles: nil];        
//    //    alert.tag = OPSLocalProjectSiteUploadBeforeDownload_CONFIRM_UPLOADNOINTERNET; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//    //    [alert show];
//    //    [alert release];
//    //    
//    [self.viewPlanSiteAndBldgInfoVC tryDownloadSite:nil];    
//    //    [self.viewPlanSiteAndBldgInfoVC downloadSite:nil];
//}
//
//-(void) closeAttachmentInfoDetailAndRemoveEmptySQLTable{
//    
//    if (self.attachmentInfoDetailToClose){
//        [self.attachmentInfoDetailToClose trashAttachmentEntry];
//        
//        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
//        NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];
//        
//        if ([[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {                
//            NSFileManager *fileManager = [NSFileManager defaultManager];
//            [fileManager removeItemAtPath:targetPath error:NULL];  
//        }                        
//        
//    }    
//}

-(void) refreshAttachmentInfoTable:(NSArray*) paramArray{
    [[self.attachmentInfoTable tableView] reloadData];

}
//- (void)connectionDidFinishLoading:(NSURLConnection *)connection
//{    
//    
////    [super connectionDidFinishLoading:connection afterUploadSuccessInvoker:self afterUploadSuccessCompleteAction:@selector(launchUploadBeforeDownloadWarning:) actionParamArray:nil];
//    
//    
//    [super connectionDidFinishLoading:connection afterUploadSuccessInvoker:self afterUploadSuccessCompleteAction:@selector(refreshAttachmentInfoTable:) actionParamArray:nil];
//}


//
//- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
//{
//    [super connection:connection didFailWithError:error];
//    
//    
////    if (self.viewPlanSiteAndBldgInfoVC){
////        NSFileManager *fileManager = [NSFileManager defaultManager];
////        
////        
////        
////        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
////        NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];        
////        [fileManager removeItemAtPath:targetPath error:NULL];  
////        
////        
////        
////        UIAlertView *alert = [[UIAlertView alloc] 
////                              initWithTitle: @"No Internet For Upload"
////                              message: @""
////                              delegate: self
////                              cancelButtonTitle: @"OK"
////                              otherButtonTitles: nil];        
////        alert.tag = OPSLocalProjectSiteUploadBeforeDownload_CONFIRM_UPLOADNOINTERNET; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
////        [alert show];
////        [alert release];
////    }
//    
//}
//
//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    switch (alertView.tag) {   
//        case OPSLocalProjectSiteUploadBeforeDownload_CONFIRM_UPLOADNOINTERNET:
//        {
//            //            [self.attachmentInfoDetailToClose actionOK:nil];
//        }
//            break;            
//        default:
//            //            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");            
//            break;
//    }	
//}
//

@end

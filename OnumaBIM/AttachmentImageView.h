//
//  AttachmentImageView.h
//  ProjectView
//
//  Created by Alfred Man on 2/29/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AssetsLibrary/AssetsLibrary.h>

@interface AttachmentImageView : UIImageView{
//    NSURL* imgURL; 
    uint attachFileID;
    NSString* path;
//    UIImage* mediaImage;
}
@property (nonatomic, assign) uint attachFileID;
@property (nonatomic, retain) NSString* path;
//@property (nonatomic, retain) UIImage* mediaImage;
//-(id) initWithMediaInfo:(NSDictionary*) info;


-(id) initWithImage:(UIImage *)image path:(NSString*) _path attachFileID:(uint)_attachFileID;


@end


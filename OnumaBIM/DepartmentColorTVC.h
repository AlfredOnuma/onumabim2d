//
//  DepartmentColorTVC.h
//  ProjectView
//
//  Created by Alfred Man on 4/19/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//
//#import "DepartmentColorTVCell.h"
#import "BIMPlanColorTVC.h"
#import <UIKit/UIKit.h>

@interface DepartmentColorTVC : BIMPlanColorTVC{

}

-(void)addToSelection:(NSIndexPath *) indexPath;
-(void)removeFromSelection:(NSIndexPath *) indexPath;
    

@end

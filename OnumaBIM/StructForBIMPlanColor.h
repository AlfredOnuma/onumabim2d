//
//  StructForBIMPlanColor.h
//  ProjectView
//
//  Created by Alfred Man on 4/20/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StructForBIMPlanColor : NSObject{
    uint _ID;
    NSString* _name;
    UIColor* _color;
    
}
@property (nonatomic, assign) uint ID;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) UIColor* color;

-(id) initWithID:(uint) ID name:(NSString*) name colorStr:(NSString*) colorStr;
+(UIColor*) colorWithHexString:(NSString*) colorStr;
@end

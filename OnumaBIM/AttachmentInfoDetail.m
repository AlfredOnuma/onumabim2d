//
//  AttachmentInfoDetail.m
//  ProjectView
//
//  Created by Alfred Man on 2/27/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "AttachmentInfoDetail.h"
#import "ViewModelVC.h"
#import "ViewModelToolbar.h"
#import "AttachmentInfoNVC.h"
#import "AttachmentImageView.h"
#import "ProjectViewAppDelegate.h"

#import "OPSStudio.h"
#import "OPSProjectSite.h"
#import "Site.h"
#import "Bldg.h"
#import "Furn.h"
#import "Space.h"	
#import "Floor.h"
#import "OPSProduct.h"
//#import "OPSLocalProjectSiteInAttachmentInfoDetail.h"
#import "AttachmentInfoData.h"
//#import "regex.h"
//#import "AttachmentImagePicker.h"
#import "AROverlayViewController.h"

#import "AttachmentInfoTableUploadFile.h"
#import "AttachmentInfoTableUploadComment.h"

#import "ViewModelNavCntr.h"
@interface UIImagePickerController(Rotating)
- (BOOL)shouldAutorotate;
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;
@end

@implementation UIImagePickerController(Rotating)

- (BOOL)shouldAutorotate {
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}
@end


// This category (i.e. class extension) is a workaround to get the
// Image PickerController to appear in landscape mode.

//@interface UIImagePickerController(Nonrotating)
//- (BOOL)shouldAutorotate;
//@end
//
//@implementation UIImagePickerController(Nonrotating)
//
//- (BOOL)shouldAutorotate {
//    return NO;
//}
//@end
@implementation AttachmentInfoDetail


uint const ATTACHMENT_CONFIRM_DELETE = 0;
uint const ATTACHMENT_CONFIRM_NOTITLE=1;
uint const ATTACHMENT_CONFIRM_UPLOADNOINTERNET=2;
uint const ATTACHMENT_MaximumImageReached=3;
uint const IMAGE_CONFIRM_DELETE = 0;
@synthesize arOverlayViewController=_arOverlayViewController;
@synthesize projectSiteInAttachmentInfoDetail=_projectSiteInAttachmentInfoDetail;
@synthesize numFileToUpload=_numFileToUpload;
@synthesize numFileUploaded=_numFileUploaded;
@synthesize buttonBin;
@synthesize buttonCamera;
@synthesize buttonImgLibrary;
@synthesize labelImgStrs;
@synthesize imgLibraryPopOverController;
@synthesize imgScrollView;
@synthesize pImg;

@synthesize aImg;
@synthesize preLoadImgView;
@synthesize postLoadImgView;

@synthesize labelAttachDate;
@synthesize labelAttachUser;

@synthesize uiTextViewAttachComment;
@synthesize uiTextFieldAttachTitle;
@synthesize uiTextFieldAttachURL;


@synthesize attachmentDate;
@synthesize attachmentCreatedBy;
@synthesize attachmentTitle;
@synthesize attachmentComment;
@synthesize attachmentURL;

//@synthesize attachedProduct;
@synthesize attachmentProductType=_attachmentProductType;
@synthesize attachmentProductID=_attachmentProductID;
@synthesize attachmentInfo=_attachmentInfo;
@synthesize aDeleteImg;
@synthesize aInsertImg;
@synthesize aAttachmentInfoUploadFile=_aAttachmentInfoUploadFile;
@synthesize attachmentInfoTableUploadComment=_attachmentInfoTableUploadComment;

@synthesize connectionData=_connectionData;
@synthesize theConnection=_theConnection;

-(void) dealloc{
    
    [_arOverlayViewController release];_arOverlayViewController=nil;
    [_connectionData release];_connectionData=nil;
    [_theConnection release];_theConnection=nil;
    
    [_aAttachmentInfoUploadFile release];_aAttachmentInfoUploadFile=nil;
    [_attachmentInfoTableUploadComment release];_attachmentInfoTableUploadComment=nil;
    [_projectSiteInAttachmentInfoDetail release];_projectSiteInAttachmentInfoDetail=nil;
    [aInsertImg release];aInsertImg=nil;
    [aDeleteImg release];aDeleteImg=nil;
    [_attachmentProductType release];_attachmentProductType=nil;
    [attachmentTitle release];attachmentTitle=nil;    
    [attachmentComment release];attachmentComment=nil;
    [attachmentURL release];attachmentURL=nil;    
    
    [uiTextFieldAttachURL release];uiTextFieldAttachURL=nil;
    [uiTextFieldAttachTitle release];uiTextFieldAttachTitle=nil;
    [uiTextViewAttachComment release];uiTextViewAttachComment=nil;
    
    [labelAttachDate release];labelAttachDate=nil;
    [labelAttachUser release];labelAttachUser=nil;
    [attachmentDate release];attachmentDate=nil;
    [attachmentCreatedBy release];attachmentCreatedBy=nil;
    [preLoadImgView release];preLoadImgView=nil;
    [postLoadImgView release];postLoadImgView=nil;
    [aImg release];aImg=nil;
    [buttonBin release];buttonBin=nil;
    [buttonCamera release];buttonCamera=nil;
    [buttonImgLibrary release];buttonImgLibrary=nil;
    [labelImgStrs release];labelImgStrs=nil;
    [imgLibraryPopOverController release];imgLibraryPopOverController=nil;
//    [attachmentInfo release];attachmentInfo=nil;
    [super dealloc];
}


- (void) toDeleteImgArray:(AttachmentImageView*) imgView{
    if (aDeleteImg==nil){
        NSMutableArray* _aDeleteImg=[[NSMutableArray alloc] init];
        self.aDeleteImg=_aDeleteImg;
        [_aDeleteImg release];
    }
//    if (aInsertImg){
//
//        uint pInd=0;
//        for (AttachmentImageView* insertView in aInsertImg){
//            if (insertView==imgView){
//                [aInsertImg removeObjectAtIndex:pInd];
//                pInd++;
//                break;
//            }
//        }
//    }
    [aDeleteImg addObject:imgView];
}
/*


- (void)viewDidLoad {
    [super viewDidLoad];
	
	// add the last image (image4) into the first position
	[self addImageWithName:@"image4.jpg" atPosition:0];
	
	// add all of the images to the scroll view
	for (int i = 1; i < 5; i++) {
		[self addImageWithName:[NSString stringWithFormat:@"image%i.jpg",i] atPosition:i];
	}
	
	// add the first image (image1) into the last position
	[self addImageWithName:@"image1.jpg" atPosition:5];
	
	scrollView.contentSize = CGSizeMake(1920, 416);    
	[scrollView scrollRectToVisible:CGRectMake(320,0,320,416) animated:NO]; 
}

 - (void)addImageWithName:(NSString*)imageString atPosition:(int)position {
 // add image to scroll view
 UIImage *image = [UIImage imageNamed:imageString];
 UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
 imageView.frame = CGRectMake(position*320, 0, 320, 416);
 [scrollView addSubview:imageView];
 [imageView release];
 }


- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {    
	NSLog(@"%f",scrollView.contentOffset.x);
	// The key is repositioning without animation      
	if (scrollView.contentOffset.x == 0) {         
		// user is scrolling to the left from image 1 to image 4         
		// reposition offset to show image 4 that is on the right in the scroll view         
		[scrollView scrollRectToVisible:CGRectMake(1280,0,320,416) animated:NO];     
	}    
	else if (scrollView.contentOffset.x == 1600) {         
		// user is scrolling to the right from image 4 to image 1        
		// reposition offset to show image 1 that is on the left in the scroll view         
		[scrollView scrollRectToVisible:CGRectMake(320,0,320,416) animated:NO];         
	} 
}
*/
//
////- (void)addImageWithName:(NSString*)imageString atPosition:(int)position {
//-(void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(CGPoint *)targetContentOffset{
//    
//    
////    CGRect scrollViewFrame=imgScrollView.frame;  
////    float contentOffX=imgScrollView.contentOffset.x;
//////    CGFloat moduloResult = (float)((int)contentOffX % (int)scrollViewFrame.size.width);
//////    uint currentImgIndex=(int) moduloResult;
////    
////    uint currentImgIndex = floor(((int)contentOffX-scrollViewFrame.size.width/2) / (int)scrollViewFrame.size.width);    
////    uint numImg=aImg?[aImg count]:0;
////    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",currentImgIndex,numImg];
////    
//    
//    /*
//    bool bScrollRight=true;
//    if (    velocity.x >0.0f){
//        bScrollRight=false;
//        NSLog(@"Scroll Left, now:%f prev:%f",imgScrollView.contentOffset.x,imgScrollOffX);
//    }else{
//        NSLog(@"Scroll Right now:%f prev:%f",imgScrollView.contentOffset.x,imgScrollOffX);
//    }
//    
//    
//    //    CGRect scrollViewFrame=imgScrollView.frame;
//    float contentOffX=imgScrollView.contentOffset.x;
//    //    CGFloat moduloResult = (float)((int)contentOffX % (int)scrollViewFrame.size.width);
//    //    uint currentImgIndex=(int) moduloResult;
//    
//    
//    CGRect scrollViewFrame=imgScrollView.frame; 
//    uint numImg=aImg?[aImg count]:0;    
//    
//
////    int middleOffset=bScrollRight?+scrollViewFrame.size.width/2:-scrollViewFrame.size.width/2;
////    uint currentImgIndex = floor(((int)contentOffX+middleOffset) / (int)scrollViewFrame.size.width);    
//    
////    uint currentImgIndex = floor(((int)contentOffX) / (int)scrollViewFrame.size.width);      
//
////    uint currentImgIndex = round((contentOffX-scrollViewFrame.size.width/2) / scrollViewFrame.size.width);    
//    uint currentImgIndex = round(contentOffX / scrollViewFrame.size.width);        
//    
//    //    uint numImg=aImg?[aImg count]:0;
//    
//    if (currentImgIndex>numImg){
//        currentImgIndex=1;
//    }
//    if (currentImgIndex<1){
//        currentImgIndex=numImg;
//    }    
//    //    currentImgIndex++;
//    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",currentImgIndex,numImg];
//    
//    
//    */
//}
-(void) scrollViewWillBeginDragging:(UIScrollView *)scrollView{
//    imgScrollOffX=scrollView.contentOffset.x;    
}
//
//-(void) scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
//    imgScrollOffX=scrollView.contentOffset.x;
//}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {    
    
//    bool bScrollRight=true;
//    if (    imgScrollView.contentOffset.x> imgScrollOffX){
//        bScrollRight=false;
//        NSLog(@"Scroll Left, now:%f prev:%f",imgScrollView.contentOffset.x,imgScrollOffX);
//    }else{
//        NSLog(@"Scroll Right now:%f prev:%f",imgScrollView.contentOffset.x,imgScrollOffX);
//    }
//    
    
//	NSLog(@"%f",scrollView.contentOffset.x);
	// The key is repositioning without animation      
    int numImg=[aImg count];
    CGRect scrollViewFrame=imgScrollView.frame;
    
/*
	if (imgScrollView.contentOffset.x == 0) {         
		// user is scrolling to the left from image 1 to image 4         
		// reposition offset to show image 4 that is on the right in the scroll view 

		[imgScrollView scrollRectToVisible:CGRectMake(scrollViewFrame.size.width*numImg,0,scrollViewFrame.size.width,scrollViewFrame.size.height) animated:NO];     
	}    
	else if (imgScrollView.contentOffset.x == (numImg+1)*scrollViewFrame.size.width) {         
		// user is scrolling to the right from image 4 to image 1        
		// reposition offset to show image 1 that is on the left in the scroll view         
		[imgScrollView scrollRectToVisible:CGRectMake(scrollViewFrame.size.width,0,scrollViewFrame.size.width,scrollViewFrame.size.height) animated:NO];         
	} else{
        float contentOffX=imgScrollView.contentOffset.x;
        uint currentImgIndex = round(contentOffX / scrollViewFrame.size.width);        
        
        //    uint numImg=aImg?[aImg count]:0;
        
        if (currentImgIndex>numImg){
            currentImgIndex=1;
        }
        if (currentImgIndex<1){
            currentImgIndex=numImg;
        }    
		[imgScrollView scrollRectToVisible:CGRectMake(scrollViewFrame.size.width*currentImgIndex,0,scrollViewFrame.size.width,scrollViewFrame.size.height) animated:NO];           
    }
*/

////    CGRect scrollViewFrame=imgScrollView.frame;
//    float contentOffX=imgScrollView.contentOffset.x;
//    //    CGFloat moduloResult = (float)((int)contentOffX % (int)scrollViewFrame.size.width);
//    //    uint currentImgIndex=(int) moduloResult;
//    
//    
//    
//    uint currentImgIndex = floor(((int)contentOffX) / (int)scrollViewFrame.size.width);  
//    
//    
////    uint currentImgIndex = floor(((int)contentOffX-scrollViewFrame.size.width/2) / (int)scrollViewFrame.size.width);    
////    uint numImg=aImg?[aImg count]:0;
//    
//    if (currentImgIndex>numImg){currentImgIndex=1;}
//    if (currentImgIndex<1){currentImgIndex=numImg;}    
////    currentImgIndex++;
//    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",currentImgIndex,numImg];
//    
 
    
    
    
    float contentOffX=imgScrollView.contentOffset.x;
    uint currentImgIndex = round(contentOffX / scrollViewFrame.size.width);        
    
    //    uint numImg=aImg?[aImg count]:0;
    
    if (currentImgIndex>numImg){
        currentImgIndex=1;
    }
    if (currentImgIndex<1){
        currentImgIndex=numImg;
    }    
    //    currentImgIndex++;    
    
    [imgScrollView scrollRectToVisible:CGRectMake(scrollViewFrame.size.width*currentImgIndex,0,scrollViewFrame.size.width,scrollViewFrame.size.height) animated:NO];  
    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",currentImgIndex,numImg];
    pImg=currentImgIndex-1;
    
}

 

- (void) removeImageWithAttachmentImageView:(AttachmentImageView*) imgView sender:(id) sender{
    
    //    [picker dismissModalViewControllerAnimated:YES];
    //    [imgLibraryPopOverController dismissPopoverAnimated:YES];
    
    
    
    if (!aImg){
        return;
    }
    
    int numImg=[aImg count];
    if (numImg<=0){
        return;
    }
    
    uint pImgView=0;
    
    
    for (AttachmentImageView* thisImgView in self.aImg){
        if (thisImgView==imgView){
            break;
        }
        pImgView++;
    }
    
//    int currentIndex=pImgView;
    
//    AttachmentImageView* imgView=[aImg objectAtIndex:pImgView];
    
    if (pImgView==0){
        [preLoadImgView removeFromSuperview];
    }
    if (pImgView==numImg-1){
        [postLoadImgView removeFromSuperview];
    }
    [imgView removeFromSuperview];
    
//    [self toDeleteImgArray:imgView];
    
    //    NSFileManager *fileManager = [NSFileManager defaultManager];
    //    [fileManager removeItemAtPath: imgView.path error:NULL];
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
    //
    
    
    
    
    //    NSLog(@"image removed");
    
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;
    
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
        [self deleteAnImgViewFromStorageAndDB:imgView database:database filemanager:fileManager];
    }
    sqlite3_close(database);
    
    
    [aImg removeObjectAtIndex:pImgView];
    numImg=[aImg count];
    
    
    
    [self rebuildImgScrollView];
    if (numImg==0){
        self.pImg=0;        
        labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",0,0];
        return;
    }
    
    
    [self setupScrollView];
    if (numImg!=0){
        self.pImg=numImg-1;
    }else{
        self.pImg=0;
    }
//    if (currentIndex==numImg){
//        currentIndex=0;
//    }
//    self.pImg=currentIndex;
	[imgScrollView scrollRectToVisible:CGRectMake((pImg+1)*imgScrollView.frame.size.width,0,imgScrollView.frame.size.width,imgScrollView.frame.size.height) animated:NO];
    //    pImg=numImg;
    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",( self.pImg+1),numImg];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
    
    
    ;
    
}

- (IBAction)removeImage:(id) sender{
    
//    [picker dismissModalViewControllerAnimated:YES];
//    [imgLibraryPopOverController dismissPopoverAnimated:YES];
    if (!aImg){
        return;
    }   
    int currentIndex=pImg;
    
    int numImg=[aImg count];
    if (numImg<=0){
        return;
    }
    AttachmentImageView* imgView=[aImg objectAtIndex:pImg];
    
    if (pImg==0){
        [preLoadImgView removeFromSuperview];
    }
    if (pImg==numImg-1){
        [postLoadImgView removeFromSuperview];
    }
    [imgView removeFromSuperview];
    
    
    
    
    
    
//  [self toDeleteImgArray:imgView];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;
    
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
        [self deleteAnImgViewFromStorageAndDB:imgView database:database filemanager:fileManager];
        
    }
    sqlite3_close(database);
    
    
    

    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    [fileManager removeItemAtPath: imgView.path error:NULL];    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
//        

    
    
    
//    NSLog(@"image removed");
 
    
    [aImg removeObjectAtIndex:pImg];
    numImg=[aImg count];
    
    

    [self rebuildImgScrollView];
    if (numImg==0){
        pImg=0;

        labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",0,0];
        return;
    }
    
    
    [self setupScrollView];
    
    
    if (currentIndex==numImg){
        pImg=0;
    }
	[imgScrollView scrollRectToVisible:CGRectMake((pImg+1)*imgScrollView.frame.size.width,0,imgScrollView.frame.size.width,imgScrollView.frame.size.height) animated:NO];        
//    pImg=numImg;
    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",( pImg+1),numImg];

    
//    
//    if (currentIndex!=numImg){
//        AttachmentImageView* oldImgView=[aImg objectAtIndex:currentIndex];
//        NSString *oldPath = oldImgView.path;
//        NSString *newFilename = [self generateImgFileNameByIndex:pImg];
//        
//        NSString *newPath = [[oldPath stringByDeletingLastPathComponent] stringByAppendingPathComponent:newFilename];
//        
//        
//        [[NSFileManager defaultManager] moveItemAtPath:oldPath toPath:newPath error:nil];
//
//
//    }
    
    
////    [self rebuildImgScrollView];    
//    
//    
//    UIImage * image = [info objectForKey:UIImagePickerControllerOriginalImage];    
//    [aImg addObject:image];
//    
//    
//    
//    [self setupScrollViewImage];
//    
//    int numImg=[aImg count];
//    pImg=numImg;
//    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",numImg,numImg];
//    
//    
//    
    
    
    
}
-(void)rebuildImgScrollView{
    CGRect scrollViewFrame=imgScrollView.frame;
    UIColor* scrollBGColor=imgScrollView.backgroundColor;
    if (imgScrollView){
        [imgScrollView removeFromSuperview];
        [imgScrollView release]; imgScrollView=nil;
        UIScrollView* _imgScrollView=[[UIScrollView alloc] initWithFrame:scrollViewFrame];
        _imgScrollView.delegate=self;
        self.imgScrollView=_imgScrollView;
        imgScrollView.backgroundColor=scrollBGColor;
        
        [_imgScrollView release];
        [self.view addSubview:imgScrollView];
    }    
}

- (NSString*) generateImgFileNameByDate{        
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;    
//    uint activeStudioID=appDele.activeStudioID;
    
    //
    //    NSString* localSiteParentFolderPath=[projectSite.dbPath stringByDeletingLastPathComponent];  
    //    
    //    
    
    
//    NSDateFormatter *formatter;      
//    formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"YYYY-D-HH-m-SS-SSS"];
//    NSString* _dateStr=  [formatter stringFromDate:[NSDate date]];    
//    //    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];  

    NSTimeInterval ti=[[NSDate date] timeIntervalSince1970];
    NSString* _dateStr=[NSString stringWithFormat:@"%f", ti];

        
    
    
//    NSString* localImgName=[NSString stringWithFormat: @"^user%@^studio%d^site%d^type%@^id%d^%@.png",[appDele defUserName], activeStudioID,[projectSite ID],self.attachmentProductType,[attachedProduct ID],_dateStr];
    NSString* localImgName=[NSString stringWithFormat: @"^%d^%@^%@.jpg",[projectSite ID],[appDele defUserName],_dateStr];
    
//    [formatter release];
    return  localImgName;
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
//    UIAlertView *alert;
//    
//    // Unable to save the image  
//    if (error){
//        alert = [[UIAlertView alloc] initWithTitle:@"Error" 
//                                           message:@"Unable to save image to Photo Album." 
//                                          delegate:self cancelButtonTitle:@"Ok" 
//                                 otherButtonTitles:nil];
//    }else{ // All is well
//        alert = [[UIAlertView alloc] initWithTitle:@"Success" 
//                                           message:@"Image saved to Photo Album." 
//                                          delegate:self cancelButtonTitle:@"Ok" 
//                                 otherButtonTitles:nil];
//    }
//    [alert show];
//    [alert release];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    470*470    
    // You have the image. You can use this to present the image in the next view like you require in `#3`.

    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [imgLibraryPopOverController dismissPopoverAnimated:YES];
    
    UIImage * pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UIImage* image=[ProjectViewAppDelegate correctImageRotation:pickedImage];
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
    
    [self dealWithImageChosen:image];
    
}

-(void) dealWithImageChosen:(UIImage*) image{
    
    if (!aImg){
        NSMutableArray* _aImg=[[NSMutableArray alloc] init ];

        self.aImg=_aImg;
        [_aImg release];
    }
    [self rebuildImgScrollView];
   
    
//    image=[ProjectViewAppDelegate correctImageRotation:image];

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;    

    
    NSString* localSiteParentFolderPath=[projectSite.dbPath stringByDeletingLastPathComponent];
    NSString* localImgName=[self generateImgFileNameByDate];
    NSString *fullPath = [localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", localImgName]]; //add our image to the path
    
    NSLog (@"Image Size: %f x %f",image.size.width,image.size.height);
    
    
    uint maxDimen=1455;
    double imageHeightToWidthRatio=image.size.height/image.size.width;
    
    if (image.size.height>maxDimen || image.size.width>maxDimen){
        CGSize optSize=CGSizeMake( imageHeightToWidthRatio>1?maxDimen/imageHeightToWidthRatio:maxDimen,  imageHeightToWidthRatio>1?maxDimen:maxDimen*imageHeightToWidthRatio);
        NSLog (@"Saving to Smaller Size");
        if ([UIImageJPEGRepresentation([ProjectViewAppDelegate resizeImage:image width:optSize.width height:optSize.height],0.7) writeToFile:fullPath atomically:YES]) {
            //        NSLog(@"save ok");
        }
        else {
            //        NSLog(@"save failed");
        }
    }else{
        //Save the image directly with no compression and no resize
        if ([UIImageJPEGRepresentation(image, 0.7) writeToFile:fullPath atomically:YES]) {
            //        NSLog(@"save ok");
        }
        else {
            //        NSLog(@"save failed");
        }
    }
    
    
        
    
    NSString* slImagePath=[localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"sl_%@", localImgName]];
    uint slideWidth=470;
    uint slideHeight=470;
//    double imageHeightToWidthRatio=image.size.height/image.size.width;
    UIImage* slImage=nil;
//    if (image.size.height>slideHeight || image.size.width>slideWidth){
        CGSize optSize=CGSizeMake( imageHeightToWidthRatio>1?slideHeight/imageHeightToWidthRatio:slideWidth,  imageHeightToWidthRatio>1?slideHeight:slideWidth*imageHeightToWidthRatio);
        NSLog (@"Saving to Smaller Size");
        slImage=[ProjectViewAppDelegate resizeImage:image width:optSize.width height:optSize.height];
        if ([UIImageJPEGRepresentation(slImage,0.7) writeToFile:slImagePath atomically:YES]) {
            //        NSLog(@"save ok");
        }
        else {
            //        NSLog(@"save failed");
        }

    
    NSString* tbImagePath=[localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"tb_%@", localImgName]];

    if ([UIImageJPEGRepresentation([ProjectViewAppDelegate resizeImage:image width:44 height:44], 1.0) writeToFile:tbImagePath atomically:YES]) {
        //        NSLog(@"save ok");
    }
    else {
        //        NSLog(@"save failed");
    }    
    
//-------------------------------------------------------------------------------------    
    
    AttachmentImageView* attachImgView=[[AttachmentImageView alloc] initWithImage:slImage path:slImagePath attachFileID:0];
//    [aImg addObject:attachImgView];
    [aImg addObject:attachImgView];
    if (aInsertImg==nil){
        NSMutableArray* _aInsertImg=[[NSMutableArray alloc] init];
        self.aInsertImg=_aInsertImg;
        [_aInsertImg release];
    }
    [aInsertImg addObject:attachImgView];
    [attachImgView release];
    
    
    [self setupScrollView];     
    int numImg=[aImg count];
    
    
	[imgScrollView scrollRectToVisible:CGRectMake(numImg*imgScrollView.frame.size.width,0,imgScrollView.frame.size.width,imgScrollView.frame.size.height) animated:NO];        
    pImg=numImg-1;
    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",numImg,numImg];
        
    
    //470*470
    
}
//-(void) setupScrollViewImage{
//-(void) setupScrollViewWithImageFromMedia: (NSDictionary *)info {   



- (void)addImage0:(AttachmentImageView*) imageView atPosition:(int)position{
	// add image to scroll view
    CGRect imgRect=imgScrollView.frame;
    
    //	UIImage *image = [UIImage imageNamed:imageString];
    
    if (position==0 ){
        
        AttachmentImageView *_preLoadImgView = [[AttachmentImageView alloc] initWithImage:imageView.image];
        self.preLoadImgView=_preLoadImgView;
        [_preLoadImgView release];
        preLoadImgView.frame = CGRectMake(position*imgRect.size.width, 0, imgRect.size.width, imgRect.size.height);
        [imgScrollView addSubview:preLoadImgView];
    }else if (position==([aImg count]+1)){
        
        AttachmentImageView *_postLoadImgView = [[AttachmentImageView alloc] initWithImage:imageView.image];
        self.postLoadImgView=_postLoadImgView;
        [_postLoadImgView release];
        postLoadImgView.frame = CGRectMake(position*imgRect.size.width, 0, imgRect.size.width, imgRect.size.height);
        [imgScrollView addSubview:postLoadImgView];
    }else{
        //	AttachmentImageView *imageView = [[AttachmentImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(position*imgRect.size.width, 0, imgRect.size.width, imgRect.size.height);
        [imgScrollView addSubview:imageView];
    }
    //	[imageView release];
}


- (void)addImage:(AttachmentImageView*) imageView atPosition:(int)position{
	// add image to scroll view
    CGFloat imgRatio=imageView.image.size.height/imageView.image.size.width;
    CGSize imageSize;
    CGRect imgFrameRect=imgScrollView.frame;
    if (imgRatio<1.0){
        imageSize=CGSizeMake(imgFrameRect.size.width, imgFrameRect.size.width*imgRatio);
    }else{
        imageSize=CGSizeMake(imgFrameRect.size.height/imgRatio, imgFrameRect.size.height);
    }
    
    
    
    //	UIImage *image = [UIImage imageNamed:imageString];
        
    if (position==0 ){        
        AttachmentImageView *_preLoadImgView = [[AttachmentImageView alloc] initWithImage:imageView.image];                
        self.preLoadImgView=_preLoadImgView;                                
        [_preLoadImgView release];        
        preLoadImgView.frame = CGRectMake(position*imgFrameRect.size.width+(imgFrameRect.size.width-imageSize.width)/2, (imgFrameRect.size.height -imageSize.height)/2,imageSize.width, imageSize.height);
        [imgScrollView addSubview:preLoadImgView];        
    }else if (position==([aImg count]+1)){
        
        AttachmentImageView *_postLoadImgView = [[AttachmentImageView alloc] initWithImage:imageView.image];
        self.postLoadImgView=_postLoadImgView;
        [_postLoadImgView release];        
        postLoadImgView.frame = CGRectMake(position*imgFrameRect.size.width+(imgFrameRect.size.width-imageSize.width)/2, (imgFrameRect.size.height -imageSize.height)/2, imageSize.width, imageSize.height);    
        [imgScrollView addSubview:postLoadImgView];        
    }else{
//	AttachmentImageView *imageView = [[AttachmentImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(position*imgFrameRect.size.width+(imgFrameRect.size.width-imageSize.width)/2, (imgFrameRect.size.height -imageSize.height)/2, imageSize.width, imageSize.height);
        [imgScrollView addSubview:imageView];
    }
//	[imageView release];
}


-(void) setupScrollView{   
    int numImg=[aImg count];    
//    UIImage* lastImg=((AttachmentImageView*) [aImg lastObject] ).image;
//    UIImage* firstImg=((AttachmentImageView*)[aImg objectAtIndex:0]).image;
    AttachmentImageView* lastImgView=[aImg lastObject];
    AttachmentImageView* firstImgView=[aImg objectAtIndex:0];
//    NSLog(@"subview count: %d",[[imgScrollView subviews] count]);
	// add the last image (image4) into the first position
	[self addImage:lastImgView atPosition:0];

    
    
	// add all of the images to the scroll view
	for (int i = 0; i < numImg; i++) {
//		[self addImage:((AttachmentImageView*) [aImg objectAtIndex:i]).image atPosition:(i+1)];
        
		[self addImage:((AttachmentImageView*) [aImg objectAtIndex:i]) atPosition:(i+1)];        
	}
	
    
	// add the first image (image1) into the last position
	[self addImage:firstImgView atPosition:(numImg+1)];        
	imgScrollView.contentSize = CGSizeMake((numImg+2)*imgScrollView.frame.size.width, imgScrollView.frame.size.height);  
}

//There are other ways you can scroll the edited area in a scroll view above an obscuring keyboard. Instead of altering the bottom content inset of the scroll view, you can extend the height of the content view by the height of the keyboard and then scroll the edited text object into view. Although the UIScrollView class has a contentSize property that you can set for this purpose, you can also adjust the frame of the content view, as shown in Listing 4-3. This code also uses the setContentOffset:animated: method to scroll the edited field into view, in this case scrolling it just above the top of the keyboard.


//- (void)keyboardWasShown:(NSNotification*)aNotification {
//    NSDictionary* info = [aNotification userInfo];
//    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    CGRect bkgndRect = activeField.superview.frame;
//    bkgndRect.size.height += kbSize.height;
//    [activeField.superview setFrame:bkgndRect];
//    [scrollView setContentOffset:CGPointMake(0.0, activeField.frame.origin.y-kbSize.height) animated:YES];
//}


-(IBAction)displayAttachWebView:(id)sender{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];

    NSString* urlStr=uiTextFieldAttachURL.text;
    
    NSString *mystring =urlStr;// @"Hello World!";
    NSString *regex = @"http://.*";
    NSPredicate *regextest = [NSPredicate
                              predicateWithFormat:@"SELF MATCHES %@", regex];
    
    if ([regextest evaluateWithObject:mystring] == YES) {
        NSLog(@"Match!");
    } else {
        NSLog(@"No match!");
        urlStr=[NSString stringWithFormat:@"http://%@",urlStr];
    }
    
    /*
    
    
    int isFail;
    char *regexPattern = "[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}"; // For IP address
    regex_t regex;
    regmatch_t pmatch[5]; // track up to 5 maches. Actually only one is needed.
    const char *sourceCString;
    char errorMessage[512], foundCString[16];
    NSString *errorMessageString;
    NSString *foundString = nil;
    sourceCString = [urlStr UTF8String];//[sourceString UTF8String];
    // setup the regular expression
    @try{
        NSException *exception;
        if(( isFail = regcomp(&regex, regexPattern, REG_EXTENDED) ))
        {
            regerror(isFail, &regex, errorMessage, 512);
            errorMessageString = [NSString stringWithCString:errorMessage encoding:NSUTF8StringEncoding];            
            exception = [NSException exceptionWithName:@"RegexException"
                                                reason:errorMessageString
                                              userInfo:nil];
            @throw exception;
        }
        else
        {
            if(( isFail = regexec( &regex, sourceCString, 5, pmatch, 0 ) ))
            {
                regerror( isFail, &regex, errorMessage, 512 );
                errorMessageString = [NSString stringWithCString:errorMessage encoding:NSUTF8StringEncoding];
                exception = [NSException exceptionWithName:@"RegexException"
                                                    reason:errorMessageString
                                                  userInfo:nil];
                @throw exception;
            }
            else
            {
                snprintf( foundCString, pmatch[0].rm_eo - pmatch[0].rm_so + 1,
                         "%s", &sourceCString[pmatch[0].rm_so] );
                
                foundString = [NSString stringWithCString:foundCString encoding:NSUTF8StringEncoding];
            }
        }
    }
    @catch( NSException *caughtException ) {
        NSLog(@"%@ occurred due to %@", [caughtException name], [caughtException reason]);
    }
    @finally {
        regfree(&regex);       
    }  
    
*/
    [appDele displayAttachmentWebView:urlStr];
}


-(IBAction)addImageFromLibrary:(id)sender{
    
    if ([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO){
        return ;
    }
    
//    if ([self.aImg count]>9){
//        
//        
//        UIAlertView *alert = [[UIAlertView alloc] 
//                              initWithTitle: @"You can only attach 10 photos in one note. Please start a new note to add more."
//                              message: @""
//                              delegate: self
//                              cancelButtonTitle: @"OK"
//                              otherButtonTitles: nil];
//        alert.tag = ATTACHMENT_MaximumImageReached; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//        [alert show];
//        [alert release];
//        
//        return;        
//    }
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    //    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    
//    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;    
    
    imagePicker.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    imagePicker.allowsEditing=false;
    imagePicker.delegate = self;
//    [self.view addSubview:imagePicker.view];
    
    UIPopoverController* aPopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
    self.imgLibraryPopOverController=aPopover;
//    CGRect imgLibFrame=buttonImgLibrary.frame;
//    CGRect pickerFrame = CGRectMake(imgLibFrame.origin.x, imgLibFrame.origin.y, 400, 600);
//    [aPopover setPopoverContentSize:pickerFrame.size animated:NO];
    
    
//    [self presentModalViewController:imagePicker animated:YES];
//    [self presentModalViewController: imagePicker animated: YES];
    CGRect bounds=[buttonImgLibrary bounds];
    [self.imgLibraryPopOverController presentPopoverFromRect:bounds inView:buttonImgLibrary permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    [self.imgLibraryPopOverController presentPopoverFromRect:bounds inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
//    
//    [self presentModalViewController:imagePicker animated:YES];
//    [self.imgLibraryPopOverController presentPopoverFromRect:[buttonImgLibrary bounds]//pickerFrame//CGRectMake(0.0, 0.0, 800.0, 800.0)
//                                                      inView:buttonImgLibrary
//                                    permittedArrowDirections:UIPopoverArrowDirectionAny
//                                                    animated:YES];
    
//    [self.imgLibraryPopOverController presentPopoverFromRect:[buttonImgLibrary bounds]//pickerFrame//CGRectMake(0.0, 0.0, 800.0, 800.0) 
//                             inView:buttonImgLibrary
//           permittedArrowDirections:UIPopoverArrowDirectionAny
//                           animated:YES];
    
//    [self.imgLibraryPopOverController presentPopoverFromBarButtonItem:buttonImgLibrary
//                                   permittedArrowDirections:UIPopoverArrowDirectionUp
//                                                   animated:YES];  
    [aPopover release];
//    [self presentModalViewController:imagePicker animated:YES];
    [imagePicker release];
}







-(IBAction)addImageFromCamera:(id)sender{
//    if ([self.aImg count]>9){
//                UIAlertView *alert = [[UIAlertView alloc]
//                              initWithTitle: @"You can only attach 10 photos in one note. Please start a new note to add more."
//                              message: @""
//                              delegate: self
//                              cancelButtonTitle: @"OK"
//                              otherButtonTitles: nil];
//        alert.tag = ATTACHMENT_MaximumImageReached; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//        [alert show];
//        [alert release];
//        return;
//    }
    AROverlayViewController* arOverlayViewController=[[AROverlayViewController alloc] init ];
    arOverlayViewController.toolbar=nil;
    arOverlayViewController.prevVC=self;
    self.arOverlayViewController=arOverlayViewController;
    [arOverlayViewController release];


    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [[appDele viewModelNavCntr] pushViewController:self.arOverlayViewController animated:NO];
    [appDele viewModelNavCntr].navigationBar.translucent=YES;
    [appDele viewModelNavCntr].navigationBar.alpha=0.7;
}



-(IBAction)addImageFromCamera0:(id)sender{
//    if ([self.aImg count]>9){
//        
//        
//        
//        UIAlertView *alert = [[UIAlertView alloc]
//                              initWithTitle: @"You can only attach 10 photos in one note. Please start a new note to add more."
//                              message: @""
//                              delegate: self
//                              cancelButtonTitle: @"OK"
//                              otherButtonTitles: nil];
//        alert.tag = ATTACHMENT_MaximumImageReached; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//        [alert show];
//        [alert release];
//        
//        return;        
//    }
    
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    

    [self presentViewController:imagePicker animated:YES completion:nil];
    [imagePicker release];
}






-(void) executeDeleteImgViewFromStorageAndDB{
    if (aDeleteImg==nil||[aDeleteImg count]<=0) {return;}
    NSFileManager *fileManager = [NSFileManager defaultManager];

    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
    OPSProjectSite* projectSite=appDele.activeProjectSite;   
    
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  
        
        for (AttachmentImageView* imgView in aDeleteImg){
            [self deleteAnImgViewFromStorageAndDB:imgView database:database filemanager:fileManager];
        }
     
    }
    sqlite3_close(database);
}


-(void) deleteAnImgViewFromStorageAndDB:(AttachmentImageView*)imgView database:(sqlite3*)database filemanager:(NSFileManager*) fileManager{
    NSString* slImagePath=imgView.path;
    [fileManager removeItemAtPath: slImagePath error:NULL];
    
    
    NSString* fullImgDirectory=[imgView.path stringByDeletingLastPathComponent];
    NSString* fullImgPathLastComponent=[[imgView.path lastPathComponent] substringFromIndex:3];
    
    NSString* fullImagePath=[[NSString alloc] initWithFormat:@"%@/%@",fullImgDirectory,fullImgPathLastComponent];
    [fileManager removeItemAtPath: fullImagePath error:NULL];
    [fullImagePath release];
    
    
    NSString* tbImagePath=[[NSString alloc] initWithFormat:@"%@/tb_%@",fullImgDirectory,fullImgPathLastComponent];
    //            NSLog( @"%@",tbImagePath);
    [fileManager removeItemAtPath: tbImagePath error:NULL];
    [tbImagePath release];
    
    
    
    //
    //            NSString* viewImagePath=[[NSString alloc] initWithFormat:@"%@/view_%@",fullImgDirectory,fullImgPathLastComponent];
    ////            NSLog( @"%@",viewImagePath);
    //            [fileManager removeItemAtPath: viewImagePath error:NULL];
    //            [viewImagePath release];
    
    
    
    if (imgView.attachFileID!=0){
        sqlite3_stmt *deleteAttachFileStmt;
        const char* deleteAttachFileSql=[[NSString stringWithFormat:@"delete from AttachFile where ID=%d",
                                          imgView.attachFileID] UTF8String];
        
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, deleteAttachFileSql, -1, &deleteAttachFileStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(deleteAttachFileStmt) == SQLITE_ROW) {
                
            }
        }
        sqlite3_finalize(deleteAttachFileStmt);
        
    }

    
}

- (void) updateRecord{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;    
    
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  
        
//        uint attachCommentID=0;
        
        
        sqlite3_stmt *updateAttachCommentStmt;
        const char* updateAttachCommentSql=[[NSString stringWithFormat:@"UPDATE AttachComment SET"
                                             @" \"userName\"=\"%@\","
                                             @" \"commentTitle\"=\"%@\","
                                             @" \"comment\"=\"%@\","
                                             @" \"links\"=\"%@\","
                                             @" \"commentDate\"=\"%@\","
                                             @" \"attachedTo\"=\"%@\","
                                             @" \"referenceID\"=%d"
                                             @" WHERE ID=%d",
                                             attachmentCreatedBy,
                                             uiTextFieldAttachTitle.text,
                                             uiTextViewAttachComment.text,
                                             uiTextFieldAttachURL.text,
                                             attachmentDate,
                                             self.attachmentProductType,
                                             self.attachmentProductID,// attachedProduct.ID,
                                             self.attachmentInfo.attachCommentID] UTF8String];
        
        
//                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, updateAttachCommentSql, -1, &updateAttachCommentStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(updateAttachCommentStmt) == SQLITE_ROW) {     
                //                int t=0;
                //                t=1;
                
            }
        }
        sqlite3_finalize(updateAttachCommentStmt);
        
        
//        const char* getLastIDSQL=[[NSString stringWithFormat:@"SELECT last_insert_rowid()"] UTF8String];        
//        sqlite3_stmt *getLastIDStmt;
//        if(sqlite3_prepare_v2 (database, getLastIDSQL, -1, &getLastIDStmt, NULL) == SQLITE_OK) {
//            while(sqlite3_step(getLastIDStmt) == SQLITE_ROW) {     
//                attachCommentID=sqlite3_column_int(getLastIDStmt, 0);                 
//            }
//        }

        
//        for (AttachmentImageView* imageView in aImg){
            
        for (AttachmentImageView* imageView in aInsertImg){
            
            sqlite3_stmt *attachImgStmt;
            
            const char* insertAttachImgSql=[[NSString stringWithFormat:@"INSERT INTO AttachFile (\"attachCommentID\",\"fileName\",\"fileTitle\",\"attachedTo\",\"referenceID\",\"uploadDate\") VALUES (%d,\"%@\",\"%@\",\"%@\",%d,\"%@\")",
                                             self.attachmentInfo.attachCommentID,
                                             [[[imageView path] lastPathComponent] substringFromIndex:3],
                                             @"",
                                             self.attachmentProductType,
                                             self.attachmentProductID,// attachedProduct.ID,
                                             attachmentDate  ] UTF8String];            
//                    NSString* test=[NSString stringWithUTF8String:insertAttachImgSql];
            if(sqlite3_prepare_v2 (database, insertAttachImgSql, -1, &attachImgStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(attachImgStmt) == SQLITE_ROW) {     
                    //                    int t=0;
                    //                    t=1;
                    
                }
            }
            sqlite3_finalize(attachImgStmt);
            
            
        }            
        
        
        
        
    }  
    sqlite3_close(database);    
}



- (void) insertNewRecordWithDatabase: (sqlite3*)database{
    

        uint attachCommentID=0;
        
        
        sqlite3_stmt *attachCommentStmt;
        const char* insertAttachCommentSql=[[NSString stringWithFormat:@"INSERT INTO AttachComment (\"userName\",\"commentTitle\",\"comment\",\"links\",\"commentDate\", \"attachedTo\", \"referenceID\") VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",%d)",
                                             attachmentCreatedBy,
                                             uiTextFieldAttachTitle.text,
                                             uiTextViewAttachComment.text,
                                             uiTextFieldAttachURL.text,
                                             attachmentDate,
                                             self.attachmentProductType,
                                             self.attachmentProductID] UTF8String];
//                                             attachedProduct.ID] UTF8String];
                                             
        //        NSString* test=[NSString stringWithUTF8String:insertAttachCommentSql];
        if(sqlite3_prepare_v2 (database, insertAttachCommentSql, -1, &attachCommentStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(attachCommentStmt) == SQLITE_ROW) {     
                //                int t=0;
                //                t=1;
                
            }
        }
        sqlite3_finalize(attachCommentStmt);
        
        const char* getLastIDSQL=[[NSString stringWithFormat:@"SELECT last_insert_rowid()"] UTF8String];
        
        sqlite3_stmt *getLastIDStmt;
        if(sqlite3_prepare_v2 (database, getLastIDSQL, -1, &getLastIDStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(getLastIDStmt) == SQLITE_ROW) {     
                attachCommentID=sqlite3_column_int(getLastIDStmt, 0);                 
            }
        }
        sqlite3_finalize(getLastIDStmt);
        
        for (AttachmentImageView* imageView in aImg){
            
            
            sqlite3_stmt *attachImgStmt;
            
            const char* insertAttachImgSql=[[NSString stringWithFormat:@"INSERT INTO AttachFile (\"attachCommentID\",\"fileName\",\"fileTitle\",\"attachedTo\",\"referenceID\",\"uploadDate\") VALUES (%d,\"%@\",\"%@\",\"%@\",%d,\"%@\")",
                                             attachCommentID,
                                             [[[imageView path] lastPathComponent] substringFromIndex:3],
                                             @"",
                                             self.attachmentProductType,
                                             self.attachmentProductID,// attachedProduct.ID,
                                             attachmentDate  ] UTF8String];
            //        NSString* test=[NSString stringWithUTF8String:insertAttachCommentSql];
            if(sqlite3_prepare_v2 (database, insertAttachImgSql, -1, &attachImgStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(attachImgStmt) == SQLITE_ROW) {     
                    //                    int t=0;
                    //                    t=1;
                    
                }
            }
            sqlite3_finalize(attachImgStmt);
            
            
        }            
  

    
}



-(void) uploadFileFinishWithAttachmentImageView:(AttachmentImageView*) attachmentImageView{
    self.numFileUploaded++;
    [self removeImageWithAttachmentImageView:attachmentImageView sender: nil];
    NSLog(@"%d /%d", self.numFileUploaded,self.numFileToUpload);
    if (self.numFileUploaded==self.numFileToUpload){
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication]delegate];
        [appDele removeLoadingViewOverlay];
        
        
//        [self uploadCommentTitle:nil];
        AttachmentInfoTableUploadComment* attachmentInfoTableUploadComment=[[AttachmentInfoTableUploadComment alloc]initWithAttachmentInfoDetail:self commentTitle:self.uiTextFieldAttachTitle.text comment:self.uiTextViewAttachComment.text links:self.uiTextFieldAttachURL.text commentDate:attachmentDate attachedTo:self.attachmentProductType referenceID:self.attachmentProductID] ;//] attachedProduct.ID];
        self.attachmentInfoTableUploadComment=attachmentInfoTableUploadComment;
        [attachmentInfoTableUploadComment release];
        [self.attachmentInfoTableUploadComment upload:nil];

    }

}

- (void) actionUpload:(id) sender{

    
    
//    if (numImg==0){
//        pImg=0;
//        
//        labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",0,0];
//        return;
//    }
//    
////    self.pImg=1;
//    [self rebuildImgScrollView];
//    [self setupScrollView];
//    return;
    
    
    
    
    NSMutableArray* aAttachmentInfoUploadFile=[[NSMutableArray alloc] init];
    self.aAttachmentInfoUploadFile=aAttachmentInfoUploadFile;
    [aAttachmentInfoUploadFile release];
    self.numFileToUpload=[self.aImg count];
    self.numFileUploaded=0;
    if (aImg!=nil){
        
        
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
        [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Attached File of Note to Onuma Server"] invoker:nil completeAction:nil actionParamArray:nil];
        
        
        uint pImgView=0;
        for (AttachmentImageView* imageView in aImg){
            
            
            NSString* slImagePath=imageView.path;
            
            
            NSString* fullImgDirectory=[slImagePath stringByDeletingLastPathComponent];
            NSString* fullImgPathLastComponent=[[slImagePath lastPathComponent] substringFromIndex:3];
            
            NSString* fullImagePath=[[NSString alloc] initWithFormat:@"%@/%@",fullImgDirectory,fullImgPathLastComponent];
            
            
            AttachmentInfoTableUploadFile* uploadFile=[[AttachmentInfoTableUploadFile alloc] initWithAttachmentImageView:imageView attachmentInfoDetail:self filePath:fullImagePath fileName:fullImgPathLastComponent fileTitle:@"" attachedTo:self.attachmentProductType referenceID:self.attachmentProductID uploadDate:attachmentDate];
            
//            NSLog (@"FullImage Path: %@ image name: %@",fullImagePath,fullImgPathLastComponent);
            
            [fullImagePath release];
            
            [self.aAttachmentInfoUploadFile addObject:uploadFile];
            [uploadFile release];
            
//            
//            ProjectViewAppDelegate* appDele=(PfcrojectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//            [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Attachment File:%d/%d to Onuma Server",count,[aImg count]]  invoker:[self.aAttachmentInfoUploadFile lastObject] completeAction:@selector(upload:) actionParamArray:nil];
            [[self.aAttachmentInfoUploadFile lastObject] upload:nil];
//            count++;
            pImgView++;
        }
    }else{
        
        
        
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
        [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Comment of Note to Onuma Server"] invoker:nil completeAction:nil actionParamArray:nil];
        
        AttachmentInfoTableUploadComment* attachmentInfoTableUploadComment=[[AttachmentInfoTableUploadComment alloc]initWithAttachmentInfoDetail:self commentTitle:self.uiTextFieldAttachTitle.text comment:self.uiTextViewAttachComment.text links:self.uiTextFieldAttachURL.text commentDate:attachmentDate attachedTo:self.attachmentProductType referenceID:self.attachmentProductID];
        self.attachmentInfoTableUploadComment=attachmentInfoTableUploadComment;
        [attachmentInfoTableUploadComment release];
        [self.attachmentInfoTableUploadComment upload:nil];
    }
//        
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//        [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[[appDele activeProjectSite] name]]  invoker:projectSiteInAttachmentInfoDetail completeAction:@selector(uploadProjectSite:) actionParamArray:nil];
//        
     
        
        
//            
//            
//            
//            sqlite3_stmt *attachImgStmt;
//            
//            const char* insertAttachImgSql=[[NSString stringWithFormat:@"INSERT INTO AttachFile (\"attachCommentID\",\"fileName\",\"fileTitle\",\"attachedTo\",\"referenceID\",\"uploadDate\") VALUES (%d,\"%@\",\"%@\",\"%@\",%d,\"%@\")",
//                                             attachCommentID,
//                                             [[imageView path] lastPathComponent],
//                                             @"",
//                                             attachmentProductType,
//                                             attachedProduct.ID,
//                                             attachmentDate  ] UTF8String];
//            //        NSString* test=[NSString stringWithUTF8String:insertAttachCommentSql];
//            if(sqlite3_prepare_v2 (database, insertAttachImgSql, -1, &attachImgStmt, NULL) == SQLITE_OK) {
//                while(sqlite3_step(attachImgStmt) == SQLITE_ROW) {
//                    //                    int t=0;
//                    //                    t=1;
//                    
//                }
//            }
//            sqlite3_finalize(attachImgStmt);
//            
//            
//        }
//    }

    /*
    
    
    
    
    
    NSString *trimmedString = [uiTextFieldAttachTitle.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([trimmedString isEqualToString: @""]){
        [self alertNoTitle:sender];
        return;
    }

    
    
    
    
    
    
    
    
    
    
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {
        // database doesn't exist in your library path... copy it from the bundle
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"EmptySQLTable" ofType:@"sqlite"];
        NSError *error = nil;
        
        if (![[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:targetPath error:&error]) {
            NSLog(@"Error: %@", error);
        }
    }
    
    
    NSString* dbPath=targetPath;
    sqlite3 * database=nil;
    
    if (sqlite3_open ([dbPath UTF8String], &database) == SQLITE_OK) {
        [self insertNewRecordWithDatabase:database];
    }
    
    sqlite3_close(database);

    
    
    OPSLocalProjectSiteInAttachmentInfoDetail* projectSiteInAttachmentInfoDetail=[[OPSLocalProjectSiteInAttachmentInfoDetail alloc]initWithAttaachmentInfoDetailToClose:self];
    self.projectSiteInAttachmentInfoDetail=projectSiteInAttachmentInfoDetail;
    [projectSiteInAttachmentInfoDetail release];
    self.projectSiteInAttachmentInfoDetail.dbPath=targetPath;

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[[appDele activeProjectSite] name]]  invoker:projectSiteInAttachmentInfoDetail completeAction:@selector(uploadProjectSite:) actionParamArray:nil];
    
     
     */
        
        
    
    /*  Don't remove this comment block yet... Testing to use project site class to do all upload, still experimental
     
     ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
     
     if (![appDele isLiveDataSourceAvailable]){
     NSFileManager *fileManager = [NSFileManager defaultManager];
     [fileManager removeItemAtPath:targetPath error:NULL];
     
     
     
     UIAlertView *alert = [[UIAlertView alloc]
     initWithTitle: @"No Internet For Upload, Data Saved locally instead."
     message: @""
     delegate: self
     cancelButtonTitle: @"OK"
     otherButtonTitles: nil];
     alert.tag = ATTACHMENT_CONFIRM_UPLOADNOINTERNET; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
     [alert show];
     [alert release];
     
     
     //        [self actionOK:(sender)];
     
     return;
     }else{
     
     }
     
     OPSProjectSite* selectedProjectSite=[appDele activeProjectSite];
     
     [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[selectedProjectSite name]]  invoker:appDele completeAction:@selector(uploadSelectedProjectSite:) actionParamArray:[NSArray arrayWithObjects: selectedProjectSite,dbPath,self,nil]];
     
     //
     //
     //    [self.navigationController popViewControllerAnimated:YES];
     //    [self executeDeleteImgViewFromStorageAndDB];
     //
     //
     */
}
/*
- (void) actionUpload1:(id) sender{
    NSString *trimmedString = [uiTextFieldAttachTitle.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([trimmedString isEqualToString: @""]){
        [self alertNoTitle:sender];
        return;
    }
    
//    if (self.attachmentInfo){
//        [self updateRecord];
//    }else{
        
        
//        
//        
   
    
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {
        // database doesn't exist in your library path... copy it from the bundle
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"EmptySQLTable" ofType:@"sqlite"];
        NSError *error = nil;
        
        if (![[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:targetPath error:&error]) {
            NSLog(@"Error: %@", error);
        }
    }
        
        
    NSString* dbPath=targetPath;
        sqlite3 * database=nil;
  
//    OPSProjectSite* projectSite=appDele.activeProjectSite;      
//        if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {              
//            [self insertNewRecordWithDatabase:database];
//        }
        if (sqlite3_open ([dbPath UTF8String], &database) == SQLITE_OK) {              
            [self insertNewRecordWithDatabase:database];
        }
        
        sqlite3_close(database);
//    }
    

        
    OPSLocalProjectSiteInAttachmentInfoDetail* projectSiteInAttachmentInfoDetail=[[OPSLocalProjectSiteInAttachmentInfoDetail alloc]initWithAttaachmentInfoDetailToClose:self];
    self.projectSiteInAttachmentInfoDetail=projectSiteInAttachmentInfoDetail;
    [projectSiteInAttachmentInfoDetail release];
    self.projectSiteInAttachmentInfoDetail.dbPath=targetPath;        
//    [self.projectSiteInAttachmentInfoDetail uploadProjectSite:nil];
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];

    
    
//      Don't remove this comment block yet... Testing to use project site class to do all upload, still experimental
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];  
//    
//    if (![appDele isLiveDataSourceAvailable]){                
//        NSFileManager *fileManager = [NSFileManager defaultManager];
//        [fileManager removeItemAtPath:targetPath error:NULL];  
//
//        
//        
//        UIAlertView *alert = [[UIAlertView alloc] 
//                              initWithTitle: @"No Internet For Upload, Data Saved locally instead."
//                              message: @""
//                              delegate: self
//                              cancelButtonTitle: @"OK"
//                              otherButtonTitles: nil];
//        alert.tag = ATTACHMENT_CONFIRM_UPLOADNOINTERNET; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//        [alert show];
//        [alert release];
//        
//        
////        [self actionOK:(sender)];
//        
//        return;
//    }else{
//        
//    }
//    
//    OPSProjectSite* selectedProjectSite=[appDele activeProjectSite];
//    
//    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[selectedProjectSite name]]  invoker:appDele completeAction:@selector(uploadSelectedProjectSite:) actionParamArray:[NSArray arrayWithObjects: selectedProjectSite,dbPath,self,nil]]; 
//    
////    
////    
////    [self.navigationController popViewControllerAnimated:YES];
////    [self executeDeleteImgViewFromStorageAndDB];
//// 
////    
//
}
*/

- (void) actionOK:(id) sender{                
//     NSString *trimmedString = [uiTextFieldAttachTitle.text stringByTrimmingCharactersInSet:
//                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    if ([trimmedString isEqualToString: @""]){
//        [self alertNoTitle:sender];
//        return;
//    }
    [self.navigationController popViewControllerAnimated:YES];
    if (self.attachmentInfo){
        [self updateRecord];
    }else{        
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
        OPSProjectSite* projectSite=appDele.activeProjectSite;    
        
        sqlite3 * database=nil;
        if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {              
            [self insertNewRecordWithDatabase:database];
        }
        sqlite3_close(database);
    }
    [self executeDeleteImgViewFromStorageAndDB];    
}


-(void) removeInsertImgArray{
    if (aInsertImg==nil||[aInsertImg count]<=0) {return;}
    NSFileManager *fileManager = [NSFileManager defaultManager];
    for (AttachmentImageView* imgView in aInsertImg){
        
        NSString* slImagePath=imgView.path;
        [fileManager removeItemAtPath: slImagePath error:NULL];
        
        
        NSString* fullImgDirectory=[imgView.path stringByDeletingLastPathComponent];
        NSString* fullImgPathLastComponent=[[imgView.path lastPathComponent] substringFromIndex:3];
        
        NSString* fullImagePath=[[NSString alloc] initWithFormat:@"%@/%@",fullImgDirectory,fullImgPathLastComponent];
        [fileManager removeItemAtPath: fullImagePath error:NULL];
        [fullImagePath release];
        
        
        NSString* tbImagePath=[[NSString alloc] initWithFormat:@"%@/tb_%@",fullImgDirectory,fullImgPathLastComponent];
        //            NSLog( @"%@",tbImagePath);
        [fileManager removeItemAtPath: tbImagePath error:NULL];
        [tbImagePath release];
        
        
    
        
        
//        
//        NSString* viewImagePath=[[NSString alloc] initWithFormat:@"%@/view_%@",fullImgDirectory,fullImgPathLastComponent];
//        //            NSLog( @"%@",viewImagePath);
//        [fileManager removeItemAtPath: viewImagePath error:NULL];           
//        [viewImagePath release];
    }    
}
- (void) actionCancel:(id) sender{
    [self.navigationController popViewControllerAnimated:YES];
    [self removeInsertImgArray];
    

}

// Called when an alertview button is clicked


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case ATTACHMENT_CONFIRM_DELETE:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	
                    NSLog(@"Delete was cancelled by the user");
                }
                    break;
                case 1: // delete
                {
                    [self trashAttachmentEntry];
                    // do the delete
                }
                    break;
            }
            
        
        }     
            break;
        case ATTACHMENT_CONFIRM_NOTITLE:
        {
        }
            break;
        case ATTACHMENT_CONFIRM_UPLOADNOINTERNET:
        {
            [self actionOK:nil];
        }
            break;            
        default:
            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");            
    }	
}

-(void) trashAttachmentEntry{//:(id)sender{
    
    
    [self.navigationController popViewControllerAnimated:YES];    
    [self removeInsertImgArray];
    
    if (!self.attachmentInfo){
        return;
    }
    
    //    uint attachCommentID=attachmentInfo.attachCommentID;
    
    
    
    
        
    
    NSFileManager *fileManager = [NSFileManager defaultManager];    
    
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
    OPSProjectSite* projectSite=appDele.activeProjectSite;  
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  
        
        
        if (aImg){
            for (AttachmentImageView* imgView in aImg){
                
                
                NSString* slImagePath=imgView.path;
                [fileManager removeItemAtPath: slImagePath error:NULL];
                
                
                NSString* fullImgDirectory=[imgView.path stringByDeletingLastPathComponent];
                NSString* fullImgPathLastComponent=[[imgView.path lastPathComponent] substringFromIndex:3];
                
                NSString* fullImagePath=[[NSString alloc] initWithFormat:@"%@/%@",fullImgDirectory,fullImgPathLastComponent];
                [fileManager removeItemAtPath: fullImagePath error:NULL];
                [fullImagePath release];
                
                
                NSString* tbImagePath=[[NSString alloc] initWithFormat:@"%@/tb_%@",fullImgDirectory,fullImgPathLastComponent];
                //            NSLog( @"%@",tbImagePath);
                [fileManager removeItemAtPath: tbImagePath error:NULL];
                [tbImagePath release];
        
                
                
                
                
//                NSString* viewImagePath=[[NSString alloc] initWithFormat:@"%@/view_%@",fullImgDirectory,fullImgPathLastComponent];
//                //            NSLog( @"%@",viewImagePath);
//                [fileManager removeItemAtPath: viewImagePath error:NULL];           
//                [viewImagePath release];
//                
                
                if (imgView.attachFileID!=0){                                            
                    sqlite3_stmt *deleteAttachFileStmt;
                    const char* deleteAttachFileSql=[[NSString stringWithFormat:@"delete from AttachFile where ID=%d",
                                                      imgView.attachFileID] UTF8String];                      
                    //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
                    if(sqlite3_prepare_v2 (database, deleteAttachFileSql, -1, &deleteAttachFileStmt, NULL) == SQLITE_OK) {
                        while(sqlite3_step(deleteAttachFileStmt) == SQLITE_ROW) {     
                            
                        }
                    }
                    sqlite3_finalize(deleteAttachFileStmt);
                    
                }
                
                
            }
        }
        
        if (self.attachmentInfo){
            
            sqlite3_stmt *deleteAttachCommentStmt;
            const char* deleteAttachCommentSql=[[NSString stringWithFormat:@"delete from AttachComment where id=%d",
                                                 self.attachmentInfo.attachCommentID] UTF8String];
            //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
            if(sqlite3_prepare_v2 (database, deleteAttachCommentSql, -1, &deleteAttachCommentStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(deleteAttachCommentStmt) == SQLITE_ROW) {     
                    
                }
            }
            
            sqlite3_finalize(deleteAttachCommentStmt);
            
            
            
            
        }        
        
        
        
    }            
    sqlite3_close(database);
    
    
    

    
}

-(void) alertTrashAttachmentEntry:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle: @"Trash This Note"
                          message: @""
                          delegate: self
                          cancelButtonTitle: @"Cancel"
                          otherButtonTitles: @"Delete", nil];
    alert.tag = ATTACHMENT_CONFIRM_DELETE; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
    [alert show];
    [alert release];
            
}
-(void) alertNoTitle:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle: @"No title entered"
                          message: @""
                          delegate: self
                          cancelButtonTitle: @"ok"
                          otherButtonTitles: nil];
    alert.tag = ATTACHMENT_CONFIRM_NOTITLE; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
    [alert show];
    [alert release];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil attachmentInfo:(AttachmentInfoData*) attachmentInfo selectedProduct:(OPSProduct*)selectedProduct
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil attachedProduct:(OPSProduct*) _attachedProduct attachmentInfo:(AttachmentInfoData*) _attachmentInfo
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        self.attachedProduct=_attachedProduct;
        if (attachmentInfo){
            self.attachmentInfo=attachmentInfo;
            self.attachmentProductID=attachmentInfo.attachedProductID;
            NSString*  attachmentProductType=[[NSString alloc] initWithFormat:@"%@",attachmentInfo.attachedProductType];
            self.attachmentProductType=attachmentProductType;
            [attachmentProductType release];
        }else{            
            self.attachmentProductID=selectedProduct.ID;
            NSString*  attachmentProductType=[[NSString alloc] initWithFormat:@"%@",[selectedProduct productType]];
            self.attachmentProductType=attachmentProductType;
            [attachmentProductType release];
        }
//        
//       NSString*  _attachmentProductType=[[NSString alloc] initWithFormat:@"%@",s];
        
        
        
        
        NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:2];
        
        
//        
//        self.attachmentProductType=_attachmentProductType;
//        [_attachmentProductType release];
        
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
        NSString* productDisplayName=[appDele getProductNumberNameComboFromProductType:self.attachmentProductType productID:self.attachmentProductID];
        self.title=[NSString stringWithFormat: @"Notes & Photos for %@: %@", self.attachmentProductType,productDisplayName];
        
        UIBarButtonItem* bi = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(actionOK:)];
//              initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(  actionOK: )];
        
        bi.style = UIBarButtonItemStyleBordered;
        [buttons addObject:bi];
//        self.navigationItem.leftBarButtonItem=bi;
        [bi release];
        
        
        
        
        
        
        
        
        
        
        
        // create a standard "add" button
//        bi = [[UIBarButtonItem alloc]
//                               initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector( alertTrashAttachmentEntry:)];
//        bi.style = UIBarButtonItemStyleBordered;
//        [buttons addObject:bi];
//        [bi release];
//        
        self.navigationItem.leftBarButtonItems=buttons;  
        
        [buttons release];
        
        
        
     
        

        buttons = [[NSMutableArray alloc] initWithCapacity:3];
//        
//        
//        bi = [[UIBarButtonItem alloc]
//              initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector( actionUpload:   )];
//        bi.style = UIBarButtonItemStyleBordered;
//        //        [buttons addObject:bi];        
//        [buttons addObject:bi];//=bi;
//        [bi release];   
        
        
        
//        
        
        
        
        
//        UIImage *uploadImage = [UIImage imageNamed:@"uploadSiteHighlight.png"] ;                            
//        bi= [[UIBarButtonItem alloc] initWithImage:uploadImage style:UIBarButtonItemStylePlain target:self action:@selector(actionUpload:)];
//        bi.style = UIBarButtonItemStyleBordered;
//        [buttons addObject:bi];//=bi;
//        [bi release];  
        
        bi= [[UIBarButtonItem alloc] initWithTitle:@"Upload" style:UIBarButtonItemStylePlain target:self action:@selector(actionUpload:)];
        bi.style = UIBarButtonItemStyleBordered;
        [buttons addObject:bi];//=bi;
        [bi release];  
        
  
        

        bi= [[UIBarButtonItem alloc] initWithTitle:@"Trash" style:UIBarButtonItemStylePlain target:self action:@selector(alertTrashAttachmentEntry:)];
        bi.style = UIBarButtonItemStyleBordered;
        [buttons addObject:bi];//=bi;
        [bi release];  
        
        
        
//        bi = [[UIBarButtonItem alloc]
//              initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector( alertTrashAttachmentEntry:)];
//        bi.style = UIBarButtonItemStyleBordered;
//        [buttons addObject:bi];
//        [bi release];
//        
        
        
        
        
            
//   
//        bi = [[UIBarButtonItem alloc]
//              initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
//        bi.style = UIBarButtonItemStyleBordered;
//        //        [buttons addObject:bi];        
//        [buttons addObject:bi];//=bi;
//        [bi release]; 
//        
//        
        
        
//        UIImage *uploadImage2 = [UIImage imageNamed:@"uploadSite.png"] ;            
//        
//        
//        bi= [[UIBarButtonItem alloc] initWithImage:uploadImage2 style:UIBarButtonItemStylePlain target:self action:@selector(actionOK:)];
//        bi.style = UIBarButtonItemStyleBordered;
//        [buttons addObject:bi];//=bi;
//        [bi release];  
//        
        
//        
//        bi = [[UIBarButtonItem alloc]
//              initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector( actionOK:   )];
        
        
        
        
   
        
        

        bi= [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(actionCancel:)];
        bi.style = UIBarButtonItemStyleBordered;
        [buttons addObject:bi];//=bi;
        [bi release];  
        
        
        
        
//        UIImage *whiteCancelIcon = [UIImage imageNamed:@"whiteCancelIcon.png"] ;                            
//        bi= [[UIBarButtonItem alloc] initWithImage:whiteCancelIcon style:UIBarButtonItemStylePlain target:self action:@selector(actionCancel:)];
//        bi.style = UIBarButtonItemStyleBordered;
//        [buttons addObject:bi];//=bi;
//        [bi release];  
        
        
        
//        bi = [[UIBarButtonItem alloc]
//              initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector( actionOK:   )];        
//        bi.style = UIBarButtonItemStyleBordered;
//        [buttons addObject:bi];                    
//        [bi release];  
//        
        
        
        
        
        
        
        /*
        
        
        
        UIImage *uploadImage = [UIImage imageNamed:@"uploadSite.png"] ;                            
        UIBarButtonItem *uploadButton= [[UIBarButtonItem alloc] initWithImage:uploadImage style:UIBarButtonItemStylePlain target:self action:@selector(actionUpload:)];

        
//        UIBarButtonItem *searchButton         = [[UIBarButtonItem alloc]
//                                                 initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
//                                                 target:self
//                                                 action:@selector(actionUpload:)];
//        
        
        
        
        UIBarButtonItem *okButton          = [[UIBarButtonItem alloc] 
                                                initWithBarButtonSystemItem:UIBarButtonItemStyleDone
                                                target:self action:@selector(actionOK:)];
        
        self.navigationItem.rightBarButtonItems =
        [NSArray arrayWithObjects:okButton, uploadImage, nil];
        
        
        [uploadButton release];
        [okButton release];
        
        */
        
        self.navigationItem.rightBarButtonItems=buttons;           
//        if (self.attachmentInfo.bSent){
//            [self disableEdit];
//        }
        
        [buttons release];
    }
    return self;
}
- (void) disableEdit{    
    self.uiTextFieldAttachTitle.enabled=false;
    self.uiTextFieldAttachURL.enabled=false;
    self.uiTextViewAttachComment.editable=false;
    self.buttonImgLibrary.enabled=false;
    self.buttonCamera.enabled=false;
    self.buttonBin.enabled=false;
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

//
//-(BOOL) prefersStatusBarHidden{
//    return YES;
//}


- (void)viewDidLoad
{
    [super viewDidLoad];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];

    
    if (self.attachmentInfo){
        NSString* _userStr=[[NSString alloc] initWithString:self.attachmentInfo.attachUserName];
        self.attachmentCreatedBy=_userStr;//attachmentInfo.attachUserName;
        [_userStr release];
    }else{
        NSString* _userStr=[appDele defUserName];        
        self.attachmentCreatedBy=_userStr;
    }
    labelAttachUser.text=[NSString stringWithFormat:@"Created by: %@",attachmentCreatedBy ];        
    
    
    
    if (self.attachmentInfo){        
        NSString* _dateStr=[[NSString alloc] initWithString:self.attachmentInfo.commentDate];
        self.attachmentDate=_dateStr;//attachmentInfo.attachUserName;
        [_dateStr release];        
//        self.attachmentDate=attachmentInfo.commentDate;
    }else{
        NSDateFormatter *formatter;
        //        NSString        *dateString;        
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY/MM/dd h:mm:ss a"];
    //    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];        
        NSString* _dateStr=[[NSString alloc] initWithString:[formatter stringFromDate:[NSDate date]]];
        self.attachmentDate=_dateStr;    
        [_dateStr release];        
        [formatter release];  // maybe; you might want to keep the formatter   
    }
    
    
//    attachmentDate = [formatter stringFromDate:[NSDate date]];          
    
    labelAttachDate.text=[NSString stringWithFormat:@"Created on: %@",attachmentDate ];  

    if (self.attachmentInfo){
        NSString* _linkStr=[[NSString alloc] initWithString:self.attachmentInfo.link];
        self.attachmentURL=_linkStr;//attachmentInfo.attachUserName;
        [_linkStr release];          
//        attachmentURL=attachmentInfo.link;
    }else{
        attachmentURL=@"";//http://www.google.co.uk/imghp?hl=en&tab=wi";        
    }
    uiTextFieldAttachURL.text=[NSString stringWithFormat:@"%@",attachmentURL];
    
    

    if (self.attachmentInfo){
        
        NSString* _titleStr=[[NSString alloc] initWithString:self.attachmentInfo.commentTitle];
        self.attachmentTitle=_titleStr;//attachmentInfo.attachUserName;
        [_titleStr release];          
        //        attachmentURL=attachmen        
//        self.attachmentTitle=attachmentInfo.commentTitle;
    }else{
        
        
//        uint numAttachedComment=0;
        
        OPSProjectSite* projectSite=appDele.activeProjectSite;                 
        sqlite3 * database=nil;
        if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  
            const char* countAttachCommentSQL=[[NSString stringWithFormat:@"SELECT count(*) FROM AttachComment WHERE attachedTo='%@' AND referenceID=%d",self.attachmentProductType, self.attachmentProductID] UTF8String];
            sqlite3_stmt *countAttachCommentStmt;
            if(sqlite3_prepare_v2 (database, countAttachCommentSQL, -1, &countAttachCommentStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(countAttachCommentStmt) == SQLITE_ROW) {
                    
//                    numAttachedComment=sqlite3_column_int(countAttachCommentStmt, 0);  
                }
            }
            sqlite3_finalize(countAttachCommentStmt);
        } 
        sqlite3_close(database);
//        self.attachmentTitle=[NSString stringWithFormat: @"Title %d",(numAttachedComment+1)];// ,attachedProduct.name];
        self.attachmentTitle=[NSString stringWithFormat: @""];//,attachedProduct.name];        
//        [self.uiTextFieldAttachTitle becomeFirstResponder];
    }
    uiTextFieldAttachTitle.text=[NSString stringWithFormat:@"%@",attachmentTitle];
    
    if (self.attachmentInfo){
        NSString* _commentStr=[[NSString alloc] initWithString:self.attachmentInfo.comment];
        self.attachmentComment=_commentStr;//attachmentInfo.attachUserName;
        [_commentStr release];          
//        self.attachmentComment=attachmentInfo.comment;
    }else{        
        self.attachmentComment=@"";
    }
    uiTextViewAttachComment.text=[NSString stringWithFormat:@"%@",attachmentComment];

    uiTextViewAttachComment.delegate=self;
    
    
    //[textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
//    
//    
    
    

    
    if (self.attachmentInfo){
        
        OPSProjectSite* projectSite=appDele.activeProjectSite; 
        
        NSString* folderPath=[projectSite.dbPath stringByDeletingLastPathComponent];          
        sqlite3 * database=nil;
        if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  

//            const char* getLastIDSQL=[[NSString stringWithFormat:@"SELECT ID, spatialStructureID, siteID, attachCommentID, userID, fileName, fileTitle, attachedTo, referenceID, uploadDate FROM AttachFile WHERE attachCommentID=%d",attachmentInfo.attachCommentID] UTF8String];
            const char* attachFileSQL=[[NSString stringWithFormat:@"SELECT ID, fileName, fileTitle FROM AttachFile WHERE attachCommentID=%d",self.attachmentInfo.attachCommentID] UTF8String];
            
            
            sqlite3_stmt *attachFileStmt;
            if(sqlite3_prepare_v2 (database, attachFileSQL, -1, &attachFileStmt, NULL) == SQLITE_OK) {
                if (!aImg){
                    NSMutableArray* _aImg=[[NSMutableArray alloc] init ];
                    self.aImg=_aImg;
                    [_aImg release];
                }
                while(sqlite3_step(attachFileStmt) == SQLITE_ROW) {     
                    
                    
                    
                    
                    uint attachFileID=sqlite3_column_int(attachFileStmt, 0);   
                    NSString* fileName= nil;
                    if ((char *) sqlite3_column_text(attachFileStmt, 1)!=nil){
                        fileName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(attachFileStmt, 1)];               
                    }
                    //else{
                    //   fileName=[NSString stringWithFormat:@""];
                    //}

                    

                    
                                                            
                    if (fileName){
//                        NSString* attachmentInfoFirstAttachedImgPath=[folderPath stringByAppendingPathComponent:[NSString stringWithFormat: @"%@",fileName]];
                        NSString* slAttachmentInfoFirstAttachedImgPath=[folderPath stringByAppendingPathComponent:[NSString stringWithFormat: @"sl_%@",fileName]];
                        UIImage *theImage = [UIImage imageWithContentsOfFile:slAttachmentInfoFirstAttachedImgPath];
                        
                        
//                        NSString* fullImgDirectory=[imgView.path stringByDeletingLastPathComponent];
//                        NSString* fullImgPathLastComponent=[imgView.path lastPathComponent];
//                        NSString* slImagePath=[[NSString alloc] initWithFormat:@"%@/sl_%@",fullImgDirectory,fullImgPathLastComponent];
//                        
//                        
                        AttachmentImageView* attachImgView=[[AttachmentImageView alloc] initWithImage:theImage path:slAttachmentInfoFirstAttachedImgPath attachFileID:attachFileID];
                        [aImg addObject:attachImgView];
                        [attachImgView release];
                    }
                    
                    
                }
                sqlite3_finalize(attachFileStmt);
                if (aImg && [aImg count]>0){
                    [self setupScrollView];
                    pImg=[aImg count]-1;
                    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",[aImg count],[aImg count]];
                }
            }                        
        }
        sqlite3_close(database);
        
    }
    if (self.attachmentInfo.bSent){
        [self disableEdit];
    }
    
    
    int height = 44;//self.frame.size.height;
    int width = 700;
    
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor blackColor];
    navLabel.font = [UIFont boldSystemFontOfSize:16];
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text=self.title;
    self.navigationItem.titleView = navLabel;
    [navLabel release]; 
    
    
    [self.uiTextViewAttachComment becomeFirstResponder];
//    [labelAttachDate setFont:[UIFont systemFontOfSize:14]];    
    // Do any additional setup after loading the view from its nib.
}
- (NSUInteger) wordCount:(NSString*) string
{
    NSArray *words = [string componentsSeparatedByString:@" "];
    return [words count];
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (self.uiTextFieldAttachTitle.text==Nil|| [self.uiTextFieldAttachTitle.text length]<=0){
        if (textView.text!=nil ){
            NSArray *words = [textView.text componentsSeparatedByString:@" "];
            uint count= [words count];
            if (count>3){
                self.uiTextFieldAttachTitle.text=[NSString stringWithFormat:@"%@ %@ %@",[words objectAtIndex:0],[words objectAtIndex:1],[words objectAtIndex:2]];
                [self.uiTextFieldAttachTitle setNeedsDisplay];	
                
            }
        }
    }
    return TRUE;
//    if([[textView.text stringByAppendingString:text] isEqualToString:"a"])
//    {
//        //trigger 'a' operation
//    }
//    else if([[textView.text stringByAppendingString:text] isEqualToString:"do it"])
//    {
//        //trigger do it operation
//    }
//    //Same conditions go on
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



//[textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
/*

-(NSData*) uploadCommentTitle:(NSArray*) paramArray{
    NSMutableData *myData = nil;    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    //    NSData* sqliteData=[NSData dataWithContentsOfFile:[self dbPath]];
    
    
    NSString *aBoundary = [NSString stringWithFormat:@"vYRNMz5SSe4vXrnc"];
    myData = [NSMutableData data];// [NSMutableData dataWithCapacity:1];
    NSString *myBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", aBoundary];
    NSString *closingBoundary = [NSString stringWithFormat:@"\r\n--%@--\r\n", aBoundary];
    
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", appDele.defUserName] dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", appDele.defUserPW] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"sysID\"\r\n\r\n%d", appDele.activeStudioID] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"siteID\"\r\n\r\n%d", [appDele currentSite].ID] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"commentTitle\"\r\n\r\n%@", uiTextFieldAttachTitle.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"comment\"\r\n\r\n%@", uiTextViewAttachComment.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"links\"\r\n\r\n%@", uiTextFieldAttachURL.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"commentDate\"\r\n\r\n%@", attachmentDate] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"attachedTo\"\r\n\r\n%@", attachmentProductType] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"referenceID\"\r\n\r\n%d", attachedProduct.ID] dataUsingEncoding:NSUTF8StringEncoding]];
    
        
    
    
    
    
    [myData appendData:[closingBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/postTestAction.php"];
    NSURL *url = [NSURL URLWithString: urlString];
    
    
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:900.0];
    
    
    [theRequest setHTTPMethod:@"POST"];
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [myData length]];
    [theRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", aBoundary];
    
    [theRequest setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    [theRequest setHTTPBody:myData];
    
    //    NSString* t=[theRequest valueForHTTPHeaderField:@"Content-Type"];
    //    NSLog(@"%@",t);
    
    
    
    // create the connection with the request
    // and start loading the data
    // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
    //    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    self.theConnection=aConnection;
    [aConnection release];
    if (self.theConnection) {
        // Create the NSMutableData that will hold
        // the received data
        // receivedData is declared as a method instance elsewhere
        if (self.connectionData){
            [self.connectionData release];
        }
        self.connectionData=[[NSMutableData data] retain];
    } else {
        // Inform the user that the connection failed.
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in uploading attachment comment"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        [connectFailMessage release];
        
    }
    
    return  myData;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in uploading attachment file"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [connectFailMessage show];
    [connectFailMessage release];
    
    [self actionOK:nil];
}
*/
-(void) uploadAttachmentCommentFinish{
//-(void)connectionDidFinishLoading:(NSURLConnection *)connection{

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication]delegate];
//    [self removeInsertImgArray];
    
    
    
    
    
    
    
    if (!self.attachmentInfo){
        
        
        [appDele removeLoadingViewOverlay];
        [self.navigationController popViewControllerAnimated:YES];
        
        return;
    }    
    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    OPSProjectSite* projectSite=appDele.activeProjectSite;
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {                
        
        if (self.attachmentInfo){
            
            sqlite3_stmt *deleteAttachCommentStmt;
            const char* deleteAttachCommentSql=[[NSString stringWithFormat:@"delete from AttachComment where id=%d",
                                                 self.attachmentInfo.attachCommentID] UTF8String];
            //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
            if(sqlite3_prepare_v2 (database, deleteAttachCommentSql, -1, &deleteAttachCommentStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(deleteAttachCommentStmt) == SQLITE_ROW) {
                    
                }
            }
            
            sqlite3_finalize(deleteAttachCommentStmt);
            
            
            
            
        }
        
        
        
    }            
    sqlite3_close(database);
    
    
    
    [appDele removeLoadingViewOverlay];
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (BOOL)shouldAutorotate {
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}

@end

//
//  ViewPlanBldgInfoVC.m
//  ProjectView
//
//  Created by Alfred Man on 5/18/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewPlanBldgInfoVC.h"
//#import "ViewModelVC.m"
//#import "ProjectViewAppDelegate.h"
//#import "Bldg.h"
//#import "Site.h"
//#import "Floor.h"
//#import "Space.h"
//#import "Furn.h"

@interface ViewPlanBldgInfoVC ()

@end

@implementation ViewPlanBldgInfoVC

static sqlite3 *database = nil;
////@synthesize viewModelVC=_viewModelVC;
//@synthesize rootView=_rootView;
//@synthesize infoView=_infoView;
//@synthesize infoScrollView=_infoScrollView;    
//@synthesize liveReportButton=_liveReportButton;
@synthesize uiBldgName;
@synthesize uiBldgNumber;
@synthesize uiNetCalculated;
@synthesize uiGrossCalculated;
@synthesize uiGrossReported;
@synthesize uiNumFloor;
@synthesize uiCapacity;
@synthesize uiOccupancy;
@synthesize uiOccupancyNumber;
@synthesize uiTotalEstimatedBldgCost;
@synthesize uiConstructionStatus;

-(void) dealloc {
//    [_viewModelVC release];_viewModelVC=nil;
//    [_rootView release];_rootView=nil;
//    [_infoView release];_infoView=nil;
//    [_infoScrollView release];_infoScrollView=nil;    
//    [_liveReportButton release];_liveReportButton=nil;
    
    
    [uiBldgName release]; uiBldgName=nil;
    [uiBldgNumber release]; uiBldgNumber=nil;
    [uiNetCalculated release];uiNetCalculated=nil;
    [uiGrossCalculated release];uiGrossCalculated=nil;
    [uiNetCalculated release];uiNetCalculated=nil;
    [uiGrossReported release];uiGrossReported=nil;
    [uiNumFloor release];uiNumFloor=nil;
    [uiCapacity release];uiCapacity=nil;
    [uiOccupancy release];uiOccupancy=nil;
    [uiOccupancyNumber release];uiOccupancyNumber=nil;
    [uiTotalEstimatedBldgCost release];uiTotalEstimatedBldgCost=nil;
    [uiConstructionStatus release];uiConstructionStatus=nil;

    
    [super dealloc];
}

-(id) initWithViewModelVC:(ViewModelVC*) viewModelVC{
    self=[super initWithViewModelVC:viewModelVC];
    if (self){
        uint topBotInset=50;
        
//        uint leftRightInset=100;
//        uint numRow=11;
        uint rowSpacing=10;
        uint pRow=0;
       
        
        uint labelHeight=20;
        uint labelWidth=400;
        uint uiLabelWidth=300; //130
        uint spaceLabelUILabelWidth=10;
        uint contentWidth=labelWidth+uiLabelWidth+spaceLabelUILabelWidth;
//        uint contentHeight=labelHeight*numRow+rowSpacing*(numRow-1);
        

        
        CGRect rootFrame=viewModelVC.view.frame;
        rootFrame.origin=CGPointMake(0.0,0.0);
        
        
        UIView* tmpRootView=[[UIView alloc] initWithFrame:rootFrame];
        self.rootView=tmpRootView;
        [tmpRootView release];
        [self setView:self.rootView];
        
 
        
        
        UIView* tmpView=[[UIView alloc] initWithFrame:rootFrame];
        self.infoView=tmpView;        
        [tmpView release];     
        
        
        UIScrollView* scrollView=[[UIScrollView alloc] initWithFrame:self.rootView.frame];
        [scrollView setContentSize:self.infoView.frame.size];
        self.infoScrollView=scrollView;
        [scrollView release];
        
        [self.rootView addSubview:self.infoScrollView];
        
        [self.infoScrollView addSubview:self.infoView];
 
        
/*
        CGSize topButtonSize=CGSizeMake(35, 35);

        UIImage* buttonImage = [UIImage imageNamed:@"Report.png"];   
        UIImage* buttonImageHighlight = [UIImage imageNamed:@"ReportHighlight.png"];        

        
        UIButton* button=nil;//[UIButton buttonWithType:UIButtonTypeCustom];


        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
        button.frame = CGRectMake((self.rootView.bounds.size.width / 2)-(contentWidth/2), topBotInset+ (pRow*(rowSpacing+labelHeight)), topButtonSize.width, topButtonSize.height) ; 
        
 */       
        
//        CGSize topButtonSize=CGSizeMake(200, 35);  
//        UIButton* button=nil;
//        button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
//        [button setTitle:@"Tab Here for Live Report" forState:UIControlStateNormal];
//        button.frame = CGRectMake((self.rootView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width, topBotInset+ (pRow*(rowSpacing+labelHeight)), topButtonSize.width, topButtonSize.height) ; 
//        
//        
//        
//        
//        [button addTarget:self action:@selector(displayWebView:) forControlEvents:UIControlEventTouchUpInside];
//        [self.infoView addSubview:button];
//        
        
        
        
        
        uint vPos=0;
        uint hPos=0;
        CGSize topButtonSize=CGSizeMake(200, 35);  
        UIButton* button=nil;
        button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button setTitle:@"Tab Here for Live Report" forState:UIControlStateNormal];
        
        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width;
        
        
        vPos=topBotInset;        
        button.frame = CGRectMake(hPos, vPos, topButtonSize.width, topButtonSize.height) ;        
        [button addTarget:self action:@selector(displayWebView:) forControlEvents:UIControlEventTouchUpInside];
        [self.infoView addSubview:button];
        vPos+=topButtonSize.height;
        
        
//
//        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
//        //        [button release];
//        [leftButtons addObject:bi];
//        [bi release];  
//        
//        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);       
        
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
        
//        UILabel* labelBldgName= [ [UILabel alloc ] initWithFrame:CGRectMake((self.rootView.bounds.size.width / 2)-(contentWidth/2), topBotInset+topButtonSize.height+rowSpacing+(pRow*(rowSpacing+labelHeight)), labelWidth, labelHeight) ];
        UILabel* labelBldgName= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, labelWidth, labelHeight) ];        
        [self prepareLabel:labelBldgName];        
        labelBldgName.text = [NSString stringWithFormat: @"Building Name:"];
        [self.infoView addSubview:labelBldgName];
        [labelBldgName release];        

        
        
//        UILabel* uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake((self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth, topBotInset+topButtonSize.height+rowSpacing+ (pRow*(rowSpacing+labelHeight)), uiLabelWidth, labelHeight) ];
        
        
        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
        UILabel* uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ];        
        [self prepareUILabel:uiLabel];
//        uiLabel.text = [NSString stringWithFormat: @"Bldg Name"];                 
        self.uiBldgName=uiLabel;        
        [self.infoView addSubview:self.uiBldgName];        
        [uiLabel release];        
        vPos+=labelHeight;
        pRow++;
        
    
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);     
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
        
        UILabel* labelBldgNumber= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
        [self prepareLabel:labelBldgNumber];        
        labelBldgNumber.text = [NSString stringWithFormat: @"Building Number:"];
        [self.infoView addSubview:labelBldgNumber];
        [labelBldgNumber release];
        
        
        
        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ];
        [self prepareUILabel:uiLabel];
        //        uiLabel.text = [NSString stringWithFormat: @"Bldg Name"];                 
        self.uiBldgNumber=uiLabel;        
        [self.infoView addSubview:self.uiBldgNumber];        
        [uiLabel release];
        
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);     
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
        
        UILabel* labelNetCalculated= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, labelWidth, labelHeight) ];
        [self prepareLabel:labelNetCalculated];        
        labelNetCalculated.text = [NSString stringWithFormat: @"Net Calculated (=modelled spaces):"];
        [self.infoView addSubview:labelNetCalculated];
        [labelNetCalculated release];
        
        
        
//        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth; 
        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake((self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth, topBotInset+topButtonSize.height+rowSpacing+ (pRow*(rowSpacing+labelHeight)), uiLabelWidth, labelHeight) ];
        [self prepareUILabel:uiLabel];
        //        uiLabel.text = [NSString stringWithFormat: @"Bldg Name"];                 
        self.uiNetCalculated=uiLabel;        
        [self.infoView addSubview:self.uiNetCalculated];        
        [uiLabel release];
        
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);     
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
        UILabel* labelGrossCalculated = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, labelWidth, labelHeight) ];
        [self prepareLabel:labelGrossCalculated];        
        labelGrossCalculated.text = [NSString stringWithFormat: @"Gross Calculated (default calculated from slab area):"];
        [self.infoView addSubview:labelGrossCalculated];
        [labelGrossCalculated release];
        
        
        
        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth; 
        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ];
        [self prepareUILabel:uiLabel];
        //        uiLabel.text = [NSString stringWithFormat: @"Bldg Name"];                 
        self.uiGrossCalculated=uiLabel;        
        [self.infoView addSubview:self.uiGrossCalculated];        
        [uiLabel release];
        
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);     
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
        UILabel* labelGrossReported = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, labelWidth, labelHeight) ];
        [self prepareLabel:labelGrossReported];        
        labelGrossReported.text = [NSString stringWithFormat: @"Gross Reported:"];
        [self.infoView addSubview:labelGrossReported];
        [labelGrossReported release];
        
        
        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth; 
        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ];
        [self prepareUILabel:uiLabel];
        //        uiLabel.text = [NSString stringWithFormat: @"Bldg Name"];                 
        self.uiGrossReported=uiLabel;        
        [self.infoView addSubview:self.uiGrossReported];        
        [uiLabel release];
        
        vPos+=labelHeight;
        pRow++;
        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);     
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
        UILabel* labelNumFloor = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, labelWidth, labelHeight) ];
        [self prepareLabel:labelNumFloor];        
        labelNumFloor.text = [NSString stringWithFormat: @"Total Number of Floors Including Basement Floors:"];
        [self.infoView addSubview:labelNumFloor];
        [labelNumFloor release];
        
        
        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ];
        [self prepareUILabel:uiLabel];
        //        uiLabel.text = [NSString stringWithFormat: @"Bldg Name"];                 
        self.uiNumFloor=uiLabel;        
        [self.infoView addSubview:self.uiNumFloor];        
        [uiLabel release];
        
        vPos+=labelHeight;
        pRow++;
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);     
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
        UILabel* labeCapacity = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, labelWidth, labelHeight) ];
        [self prepareLabel:labeCapacity];        
        labeCapacity.text = [NSString stringWithFormat: @"Capacity (aggregated from Space Attributes):"];
        [self.infoView addSubview:labeCapacity];
        [labeCapacity release];
        
        
        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ];
        [self prepareUILabel:uiLabel];
        //        uiLabel.text = [NSString stringWithFormat: @"Bldg Name"];                 
        self.uiCapacity=uiLabel;        
        [self.infoView addSubview:self.uiCapacity];        
        [uiLabel release];      
        vPos+=labelHeight;  
        pRow++;
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);     
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
        UILabel* labelOccupancy = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, labelWidth, labelHeight) ];
        [self prepareLabel:labelOccupancy];        
        labelOccupancy.text = [NSString stringWithFormat: @"Occupancy (aggregated from Space Attributes):"];
        [self.infoView addSubview:labelOccupancy];
        [labelOccupancy release];
        
        
        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ];
        [self prepareUILabel:uiLabel];
        //        uiLabel.text = [NSString stringWithFormat: @"Bldg Name"];                 
        self.uiOccupancy=uiLabel;        
        [self.infoView addSubview:self.uiOccupancy];        
        [uiLabel release];        
        vPos+=labelHeight;
        pRow++;
        
                        
        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);     
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
        UILabel* labelOccupancyNumber = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, labelWidth, labelHeight) ];
        [self prepareLabel:labelOccupancyNumber];        
        labelOccupancyNumber.text = [NSString stringWithFormat: @"Occupancy Number (Reported per Building):"];
        [self.infoView addSubview:labelOccupancyNumber];
        [labelOccupancyNumber release];
        
        
        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ];
        [self prepareUILabel:uiLabel];
        //        uiLabel.text = [NSString stringWithFormat: @"Bldg Name"];                 
        self.uiOccupancyNumber=uiLabel;        
        [self.infoView addSubview:self.uiOccupancyNumber];        
        [uiLabel release];     
        vPos+=labelHeight;   
        pRow++;
                        
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);     
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
        UILabel* labelTotalEstimatedCost = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, labelWidth, labelHeight) ];
        [self prepareLabel:labelTotalEstimatedCost];        
        labelTotalEstimatedCost.text = [NSString stringWithFormat: @"Total Estimated Bldg Cost:"];
        [self.infoView addSubview:labelTotalEstimatedCost];
        [labelTotalEstimatedCost release];
        
        
        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ];
        [self prepareUILabel:uiLabel];
        //        uiLabel.text = [NSString stringWithFormat: @"Bldg Name"];                 
        self.uiTotalEstimatedBldgCost=uiLabel;        
        [self.infoView addSubview:self.uiTotalEstimatedBldgCost];        
        [uiLabel release];     
        vPos+=labelHeight;   
        pRow++;
                
        
        
        vPos+=rowSpacing;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);     
        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
        UILabel* labelConstructionStatus = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, labelWidth, labelHeight) ];
        [self prepareLabel:labelConstructionStatus];        
        labelConstructionStatus.text = [NSString stringWithFormat: @"Construction Status:"];
        [self.infoView addSubview:labelConstructionStatus];
        [labelConstructionStatus release];
        
        
        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ];
        [self prepareUILabel:uiLabel];
        //        uiLabel.text = [NSString stringWithFormat: @"Bldg Name"];                 
        self.uiConstructionStatus=uiLabel;        
        [self.infoView addSubview:self.uiConstructionStatus];        
        [uiLabel release];     
//        vPos+=labelHeight;  //Temporary commented caused of value stored never read warning   
        pRow++;
        
        [self readFromSQL];
        
        
        
        
        
        
        
        
        UILabel* labelCell=nil;
        
        
        
        vPos+=rowSpacing*3;
        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                
        //                hPos+=labelXSpacing;        
        //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
        [self prepareSelectedProductTitle:labelCell];        
        labelCell.text=@"Selected Element:";
        [self.infoView addSubview:labelCell];        
        [labelCell release];  
        //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
        
        vPos+=[self selectedProductTitleFontSize];
        pRow++;
        
        

        if ([self.viewModelVC aSelectedProductRep] && [[self.viewModelVC aSelectedProductRep] count]>0){
            for (uint pViewProductRep=0; pViewProductRep<[[self.viewModelVC aSelectedProductRep] count];pViewProductRep++){
                ViewProductRep* viewProductRep=[[self.viewModelVC aSelectedProductRep] objectAtIndex:pViewProductRep];
                OPSProduct* product=[viewProductRep product];
                //                    if ([product isKindOfClass:[Floor class]]){
                //                        Floor* floor=(Floor*) product;
                //                        Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                //                        product=bldg;
                //                    }
                //                    
                //                                
                
                
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                
                //                hPos+=labelXSpacing;        
                //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
                [self prepareSelectedProductLabel:labelCell];                            
                labelCell.text=[product productRepDisplayStr];
                [self.infoView addSubview:labelCell];        
                [labelCell release];  
                //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
                
                vPos+=labelHeight;
                pRow++;
                
            }
        }else{
            
            
            OPSProduct* product=(OPSProduct*) [[self.viewModelVC model] root];
            
            vPos+=rowSpacing;
            hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                
            //                hPos+=labelXSpacing;        
            //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
            labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
            [self prepareSelectedProductLabel:labelCell];        
            labelCell.text=[product productRepDisplayStr];
            //                labelCell.text=[NSString stringWithFormat:@"%d - %@",[product ID],[product name]];
            [self.infoView addSubview:labelCell];        
            [labelCell release];  
            //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning               
//            vPos+=labelHeight; //Commented out because of dead store but activate it if more rows to come after
//            pRow++;//Commented out because of dead store but activate it if more rows to come after
        }
        
        

    }
    return self;
}

-(void) readFromSQL{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
    OPSProjectSite* projectSite=[appDele activeProjectSite];    
    if ([projectSite dbPath]==Nil) {return;}    
    
    Bldg* bldg=[appDele currentBldg];
    sqlite3_stmt* infoStmt;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  
        
//        
//        NSString* siteInfoSQLStr=[NSString stringWithFormat:@""
//                                  @"SELECT siteInfo.siteArea"
//                                  @" FROM siteinfo"                                ];
//        const char* siteInfoSQLChar=[siteInfoSQLStr UTF8String];    
//        double siteArea=0.0;
//        if(sqlite3_prepare_v2(database, siteInfoSQLChar, -1, &siteInfoStmt, NULL) == SQLITE_OK) {
//            while(sqlite3_step(siteInfoStmt) == SQLITE_ROW) {                     
//                siteArea = sqlite3_column_double(siteInfoStmt,0);
//                
//            }
//        }        
//        
//        self.siteAreaTextField.text=[ProjectViewAppDelegate doubleToAreaStr:siteArea];
        
        int bldgID=[bldg bldgReferenceID]==0?[bldg ID]:[bldg bldgReferenceID];
        
        NSString* referenceReportPart1=[NSString stringWithFormat:@""
                                        
                               @" SELECT COUNT(DISTINCT Floor.ID),"                                        
                               @" SUM(USCG_SpaceInfo.occupMax),"                                                                                
                               @" SUM(USCG_SpaceInfo.occup)"

                               @" FROM Bldg "
                               @" LEFT JOIN BldgInfo ON Bldg.ID=BldgInfo.bldgID"
                               @" LEFT JOIN USCG_BldgInfo ON Bldg.ID=USCG_BldgInfo.bldgID"
                               @" LEFT JOIN BldgReport ON Bldg.ID=BldgReport.bldgID"
                               @" LEFT JOIN Floor ON Bldg.ID=Floor.bldgID"
                               @" LEFT JOIN Space ON Floor.ID=Space.floorID"
                               @" LEFT JOIN USCG_SpaceInfo ON Space.ID=USCG_SpaceInfo.spaceID"
                               @" WHERE bldg.id=%d"
                               @" GROUP BY Bldg.ID",bldgID];//[[appDele currentSite] ID]];
        
        
        
        NSString* referenceSqlStr=[NSString stringWithFormat:@"%@",referenceReportPart1];
        const char* referenceSqlChar=[referenceSqlStr UTF8String];
        if(sqlite3_prepare_v2(database, referenceSqlChar, -1, &infoStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(infoStmt) == SQLITE_ROW) {
                uint numFloor=0.0;
                NSString* numFloorStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:0];
                if (numFloorStr!=nil && (![numFloorStr isEqualToString:@""])){
                    numFloor=[numFloorStr intValue];
                }
                self.uiNumFloor.text=[ProjectViewAppDelegate intToIntStr:numFloor];
                
                uint capacity=0.0;
                NSString* capacityStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:1];
                if (capacityStr!=nil && (![capacityStr isEqualToString:@""])){
                    capacity=[capacityStr intValue];
                }
                self.uiCapacity.text=[ProjectViewAppDelegate intToIntStr:capacity];
                
                
                uint occupancy=0.0;
                NSString* occupancyStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:2];
                if (occupancyStr!=nil && (![occupancyStr isEqualToString:@""])){
                    occupancy=[occupancyStr intValue];
                }
                self.uiOccupancy.text=[ProjectViewAppDelegate intToIntStr:occupancy];
                
            }
        }
        
        
        
        NSString* reportPart1=[NSString stringWithFormat:@""
                               @"SELECT bldg.id,"
                               @" USCG_BldgInfo.rpfn,"
                               @" USCG_BldgInfo.useNsfFactor,"
                               @" USCG_BldgInfo.nsfFactor,"
                               @" bldgInfo.bldgNetArea,"
                               @" bldgInfo.bldgGrossArea,"
                               @" bldgInfo.reportedGrossArea,"
                               @" COUNT(DISTINCT Floor.ID),"
                               @" SUM(USCG_SpaceInfo.occupMax),"
                               @" SUM(USCG_SpaceInfo.occup),"
                               @" bldgInfo.occupancy,"
                               @" BldgReport.estimateCost," 
                               @" USCG_BldgInfo.existing"
                               @" FROM Bldg "
                               @" LEFT JOIN BldgInfo ON Bldg.ID=BldgInfo.bldgID"
                               @" LEFT JOIN USCG_BldgInfo ON Bldg.ID=USCG_BldgInfo.bldgID"
                               @" LEFT JOIN BldgReport ON Bldg.ID=BldgReport.bldgID"
                               @" LEFT JOIN Floor ON Bldg.ID=Floor.bldgID"
                               @" LEFT JOIN Space ON Floor.ID=Space.floorID"
                               @" LEFT JOIN USCG_SpaceInfo ON Space.ID=USCG_SpaceInfo.spaceID"
                               @" WHERE bldg.id=%d"
                               @" GROUP BY Bldg.ID",[bldg ID]];//[[appDele currentSite] ID]];
  
        
        
        NSString* sqlStr=[NSString stringWithFormat:@"%@",reportPart1];
        const char* sqlChar=[sqlStr UTF8String];  
        if(sqlite3_prepare_v2(database, sqlChar, -1, &infoStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(infoStmt) == SQLITE_ROW) {  
                
                
                //        self.uiBldgName=[  BldgInfo.bldgNetArea
                
                
                self.uiBldgName.text=[bldg name];        
                self.uiBldgNumber.text=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:1];  
                int useNsfFactor=sqlite3_column_int(infoStmt, 2);
                double nsfFactor=sqlite3_column_double(infoStmt, 3);
                double netArea=sqlite3_column_double(infoStmt,4);
                
                self.uiNetCalculated.text=[ProjectViewAppDelegate doubleToAreaStr:netArea];
                
                double grossArea=sqlite3_column_double(infoStmt, 5);                
                if (useNsfFactor==0){
                    grossArea*=nsfFactor;
                }else{
                    grossArea=netArea*nsfFactor;
                }
                
                
                self.uiGrossCalculated.text=[ProjectViewAppDelegate doubleToAreaStr:grossArea];
                
                double grossReported=sqlite3_column_double(infoStmt, 6);                
                self.uiGrossReported.text=[ProjectViewAppDelegate doubleToAreaStr:grossReported];
                
                
//                uint numFloor=0.0;
//                NSString* numFloorStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:7];                
//                if (numFloorStr!=nil && (![numFloorStr isEqualToString:@""])){
//                    numFloor=[numFloorStr intValue];
//                }    
//                self.uiNumFloor.text=[ProjectViewAppDelegate intToIntStr:numFloor];
//                
//                uint capacity=0.0;
//                NSString* capacityStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:8];                
//                if (capacityStr!=nil && (![capacityStr isEqualToString:@""])){
//                    capacity=[capacityStr intValue];
//                }    
//                self.uiCapacity.text=[ProjectViewAppDelegate intToIntStr:capacity];                                
//                
//                
//                uint occupancy=0.0;
//                NSString* occupancyStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:9];                
//                if (occupancyStr!=nil && (![occupancyStr isEqualToString:@""])){
//                    occupancy=[occupancyStr intValue];
//                }    
//                self.uiOccupancy.text=[ProjectViewAppDelegate intToIntStr:occupancy]; 
                
                
                
                uint occupancyNumber=sqlite3_column_double(infoStmt,10);
                self.uiOccupancyNumber.text=[ProjectViewAppDelegate intToIntStr:occupancyNumber];                
                
                
                double estimatedCost=sqlite3_column_double(infoStmt, 11);
                self.uiTotalEstimatedBldgCost.text=[ProjectViewAppDelegate doubleToCurrenyStr:estimatedCost];                
                
                                
                int constructionStatus=sqlite3_column_int(infoStmt,12);
                switch (constructionStatus) {
                    case -1:    {                    
                        self.uiConstructionStatus.text=@"Existing";
                        break;
                    }
                    case 1:                        {
                        self.uiConstructionStatus.text=@"Renovation";
                    }
                        break;
                    case 0:                        
                        self.uiConstructionStatus.text=@"New";
                        break;                                                                        
                    default:
                        break;
                }
                
                
//                siteArea = sqlite3_column_double(infoStmt,0);
                
            }
        }        
        
//        
//        Bldg* bldg=[appDele currentBldg];
//        
//        self.uiBldgName.text=[bldg name];        
//        self.uiBldgNumber.text=[ProjectViewAppDelegate readNameFromSQLStmt:sqlChar column:1];
//        
//        
//        
//        //[appDele readNameFromSQLStmt:,sqlChar,[bldg name]];
//        double bldgNetArea = sqlite3_column_double(infoStmt,0);
//        if (isImperial) {bldgNetArea=[ProjectViewAppDelegate sqMeterToSqFeet:bldgNetArea];}
//
        
    }
    sqlite3_close(database);

    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end

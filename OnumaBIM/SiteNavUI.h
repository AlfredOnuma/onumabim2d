//
//  SiteNavUI.h
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//#include "OPSMobileViewController.h"

#include "OPSModel.h"

#import "ViewSpace.h"
@class NavUI;
//@protocol ViewSpaceDelegate;

@protocol NAVUIDelegate 
//- (float)smileForFaceView:(FaceView *)requestor;  // -1.0 (frown) to 1.0 (smile)
- (OPSModel*) getModel;
- (UIScrollView*) getScrollView;
@end

@interface SiteNavUI : UIView <ViewSpaceDelegate>{    
    id <NAVUIDelegate> delegate; 
}




@property (assign) id <NAVUIDelegate> delegate;

@end

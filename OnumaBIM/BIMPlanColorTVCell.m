//
//  BIMPlanColorTVCell.m
//  ProjectView
//
//  Created by Alfred Man on 4/20/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "BIMPlanColorTVCell.h"
#import "BIMPlanColorTVC.h"
@implementation BIMPlanColorTVCell
@synthesize colorLabel=_colorLabel;
@synthesize colorUIView=_colorUIView;
@synthesize addToSelectionButton=_addToSelectionButton;
@synthesize minusFromSelectionButton=_minusFromSelectionButton;
@synthesize delegate=_delegate;
-(void) dealloc{
    [_colorLabel release];_colorLabel=nil;
    
    [_colorUIView release];_colorUIView=nil;
    
    [_addToSelectionButton release];_addToSelectionButton=nil;
    
    [_minusFromSelectionButton release];_minusFromSelectionButton=nil;
    [super dealloc];
}


-(IBAction) addToSelectionButtonTapped:(id)sender event:(id)event{        
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    BIMPlanColorTVC* bimPlanColorTVC=(BIMPlanColorTVC*) self.delegate;
    CGPoint currentTouchPosition = [touch locationInView:bimPlanColorTVC.tableView];
    NSIndexPath *indexPath = [bimPlanColorTVC.tableView indexPathForRowAtPoint: currentTouchPosition];   
    [bimPlanColorTVC addToSelection:indexPath];
    
}


-(IBAction) removeFromSelectionButtonTapped:(id)sender event:(id)event{        
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    BIMPlanColorTVC* bimPlanColorTVC=(BIMPlanColorTVC*) self.delegate;
    CGPoint currentTouchPosition = [touch locationInView:bimPlanColorTVC.tableView];
    NSIndexPath *indexPath = [bimPlanColorTVC.tableView indexPathForRowAtPoint:currentTouchPosition] ;
    [bimPlanColorTVC removeFromSelection:indexPath];
}
 
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

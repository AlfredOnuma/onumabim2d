//
//  FontSizeTableViewController.m
//  ProjectView
//
//  Created by Alfred Man on 11/20/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "FontSizeTableViewController.h"
#import "ViewModelVC.h"
@interface FontSizeTableViewController ()

@end

@implementation FontSizeTableViewController
@synthesize viewModelVC=_viewModelVC;




- (id)initWithViewModelVC:(ViewModelVC*) viewModelVC Style:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.viewModelVC=viewModelVC;
        // Custom initialization
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    if (section==0){
        return 1;
    }else if (section==1){
        return 1;
    }
    return 0;
}
-(void) increaseFontSize{
    [self.viewModelVC increaseFontSize];
}
-(void) decreaseFontSize{    
    [self.viewModelVC decreaseFontSize];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString* IncFontSizeCellIdentifier = @"IncFontSizeCell";
    static NSString* DecFontSizeCellIdentifier = @"DecFontSizeCell";

//    static NSString* LocalCellIdentifier = @"LocalAttachmentInfoCell";
    
    UITableViewCell *cell =nil;
    //    static NSString* CellIdentifier = @"AttachmentInfoCell";
    if ([indexPath section]==0){
        
        
        cell = [tableView dequeueReusableCellWithIdentifier:IncFontSizeCellIdentifier];
        
        if (cell == nil) {
            //                    NSLog(@"section0_alloc");
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:IncFontSizeCellIdentifier] autorelease];
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            
            
            
            
            
            
            //        CGSize topButtonSize=CGSizeMake(100, 35);
            UIButton* incFontSizeButton=nil;
            incFontSizeButton=[UIButton buttonWithType:UIButtonTypeCustom];
            [incFontSizeButton setImage:[UIImage imageNamed:@"AddIcon"] forState:UIControlStateNormal];
            [incFontSizeButton setImage:[UIImage imageNamed:@"AddIconHighlight"] forState:UIControlStateHighlighted];
//            [incFontSizeButton setTitle:@"Live Attachments" forState:UIControlStateNormal];
            //        button.frame = CGRectMake((self.rootView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width, topBotInset+ (pRow*(rowSpacing+labelHeight)), topButtonSize.width, topButtonSize.height) ;
            
            
            //            liveAttachmentButton.frame = CGRectMake(0.0, 0.0, 250, 45.0) ;
            incFontSizeButton.frame = CGRectMake(5.0, 5.0, 35, 35.0) ;
            
            //        liveAttachmentButton.frame = CGRectMake((self.tableView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width, topBotInset+ (pRow*(rowSpacing+labelHeight)), topButtonSize.width, topButtonSize.height) ;
            
            [incFontSizeButton addTarget:self action:@selector(increaseFontSize) forControlEvents:UIControlEventTouchUpInside];
            //        [self.infoView addSubview:button];
            
            
            
            
            [cell.contentView addSubview:incFontSizeButton];
            //            [liveAttachmentButton release];
            
        }
        
        //                            NSLog(@"section0_nonAlloc");
        
    }
    
    
    
    if ([indexPath section]==1){
        
        cell = [tableView dequeueReusableCellWithIdentifier:DecFontSizeCellIdentifier];
        
        if (cell == nil) {
            //                    NSLog(@"section0_alloc");
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DecFontSizeCellIdentifier] autorelease];
            
            //            mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(70.0, 0.0, 250, 45.0)] ;
            //            mainLabel.tag = MAINLABEL_TAG;
            //            mainLabel.font = [UIFont systemFontOfSize:30.0];
            //            //        mainLabel.textAlignment = UITextAlignmentRight;
            //            mainLabel.textAlignment = UITextAlignmentLeft;
            //            mainLabel.textColor = [UIColor blackColor];
            //            mainLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            
            
            
            
            
            
            //        CGSize topButtonSize=CGSizeMake(100, 35);
            UIButton* decFontSizeButton=nil;
            decFontSizeButton=[UIButton buttonWithType:UIButtonTypeCustom];
            [decFontSizeButton setImage:[UIImage imageNamed:@"RemoveIcon"] forState:UIControlStateNormal];
            [decFontSizeButton setImage:[UIImage imageNamed:@"RemoveIconHighlight"] forState:UIControlStateHighlighted];
            //            [incFontSizeButton setTitle:@"Live Attachments" forState:UIControlStateNormal];
            //        button.frame = CGRectMake((self.rootView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width, topBotInset+ (pRow*(rowSpacing+labelHeight)), topButtonSize.width, topButtonSize.height) ;
            
            
            //            liveAttachmentButton.frame = CGRectMake(0.0, 0.0, 250, 45.0) ;
            decFontSizeButton.frame = CGRectMake(5.0, 5.0, 35, 35.0) ;
            
            //        liveAttachmentButton.frame = CGRectMake((self.tableView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width, topBotInset+ (pRow*(rowSpacing+labelHeight)), topButtonSize.width, topButtonSize.height) ;
            
            [decFontSizeButton addTarget:self action:@selector(decreaseFontSize) forControlEvents:UIControlEventTouchUpInside];
            //        [self.infoView addSubview:button];
            
            
            
            
            [cell.contentView addSubview:decFontSizeButton];
            //            [liveAttachmentButton release];
            
        }
        
        //                            NSLog(@"section0_nonAlloc");
        
    }
    return cell;

    
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    
//    // Configure the cell...
//    
//    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end

//
//  EverNoteGlassNote.m
//  ProjectView
//
//  Created by onuma on 12/09/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "EverNoteGlassNote.h"

@implementation EverNoteGlassNote
@synthesize notebookGUID=_notebookGUID;
@synthesize noteGUID=_noteGUID;

-(id) initWithNoteBookGUID:(NSString*)bookGUID noteGUID:(NSString*)noteGUID{
    self=[super init];
    if (self){
        self.notebookGUID=bookGUID;
        self.noteGUID=noteGUID;
    }
    return self;
}
-(void) dealloc{
    [_noteGUID release];_noteGUID=nil;
    [_notebookGUID release];_notebookGUID=nil;
    [super dealloc];
}
@end

//
//  UnitStruct.m
//  ProjectView
//
//  Created by Alfred Man on 6/12/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "UnitStruct.h"
#import "ProjectViewAppDelegate.h"
#import "OPSProjectSite.h"
@implementation UnitStruct

@synthesize currency=_currency;
@synthesize currencyUnit=_currencyUnit;
@synthesize measureUnitLength=_measureUnitLength;
@synthesize measureUnitArea=_measureUnitArea;
@synthesize isImperial=_isImperial;
static sqlite3 *database=nil;

-(void) dealloc{
    [_currency release];_currency=nil;
    [_currencyUnit release];_currencyUnit=nil;
    [_measureUnitLength release];_measureUnitLength=nil;
    [_measureUnitArea release];_measureUnitArea=nil;
    [super dealloc];
}


//if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  

-(id) initWithDBPath:(NSString*) dbPath {
    self=[super init];
    if (self){        
        
        if (dbPath==Nil) {return self;}
        if (sqlite3_open ([dbPath UTF8String], &database) == SQLITE_OK) {                                      
            sqlite3_stmt *unitStmt;    
            const char* unitSql=[[NSString stringWithFormat:@"select siteInfo.currency, siteInfo.unit from site left join siteinfo on siteinfo.siteID=site.ID"] UTF8String];
            
            if(sqlite3_prepare_v2(database, unitSql, -1, &unitStmt, NULL) == SQLITE_OK) {
                while( sqlite3_step(unitStmt) == SQLITE_ROW) {                                                                   
                    NSString* tmpCurrency=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:unitStmt column:0]];
                    self.currency=tmpCurrency;
                    [tmpCurrency release];                            
                    self.isImperial= [[ProjectViewAppDelegate readNameFromSQLStmt:unitStmt column:1] isEqualToString:@"IMPERIAL"];                                                                
                }                                
                sqlite3_close(database); 
            }
            
            sqlite3_close(database);
        }
        if (self.isImperial){
            NSString* tmpMeasureUnitArea=[[NSString alloc] initWithString:@"sqft"];
            self.measureUnitArea=tmpMeasureUnitArea;
            [tmpMeasureUnitArea release];
            NSString* tmpMeasureUnitLength=[[NSString alloc] initWithString:@"ft"];
            self.measureUnitLength=tmpMeasureUnitLength;
            [tmpMeasureUnitLength release];
        }else{            
            NSString* tmpMeasureUnitArea=[[NSString alloc] initWithString:@"sqm"];
            self.measureUnitArea=tmpMeasureUnitArea;
            [tmpMeasureUnitArea release];
            NSString* tmpMeasureUnitLength=[[NSString alloc] initWithString:@"m"];
            self.measureUnitLength=tmpMeasureUnitLength;
            [tmpMeasureUnitLength release];
        }
        
        
        
        if ([self.currency isEqualToString:@"USD"]){
            NSString* tmpCurrencyUnit=[[NSString alloc] initWithString:@"$"];        
            self.currencyUnit=tmpCurrencyUnit;
            [tmpCurrencyUnit release];
        }else if ([self.currency isEqualToString:@"EUR"]){
            NSString* tmpCurrencyUnit=[[NSString alloc] initWithString:@"€"];        
            self.currencyUnit=tmpCurrencyUnit;
            [tmpCurrencyUnit release];
        }else if ([self.currency isEqualToString:@"GBP"]){
            NSString* tmpCurrencyUnit=[[NSString alloc] initWithString:@"£"];        
            self.currencyUnit=tmpCurrencyUnit;
            [tmpCurrencyUnit release];
        }else if ([self.currency isEqualToString:@"ZAR"]){
            NSString* tmpCurrencyUnit=[[NSString alloc] initWithString:@"R"];        
            self.currencyUnit=tmpCurrencyUnit;
            [tmpCurrencyUnit release];
        }else if ([self.currency isEqualToString:@"NOK"]){
            NSString* tmpCurrencyUnit=[[NSString alloc] initWithString:@"kr"];        
            self.currencyUnit=tmpCurrencyUnit;
            [tmpCurrencyUnit release];
        } 
        
        

        
        
                
    }
    return self;
}




-(id) initWithCurrency:(NSString*) currency isImperial:(bool) isImperial {//(NSString*) currencyUnit measureUnitLength:(NSString*) measureUnitLength measureUnitArea:(NSString*) measureUnitArea isImperial:(bool) isImperial{
    self=[super init];
    if (self){
        self.currency=currency;
        self.isImperial=isImperial;        
              
        
        
        

        
        
        if (isImperial){
            NSString* tmpMeasureUnitArea=[[NSString alloc] initWithString:@"sqft"];
            self.measureUnitArea=tmpMeasureUnitArea;
            [tmpMeasureUnitArea release];
            NSString* tmpMeasureUnitLength=[[NSString alloc] initWithString:@"ft"];
            self.measureUnitLength=tmpMeasureUnitLength;
            [tmpMeasureUnitLength release];
        }else{
            
            NSString* tmpMeasureUnitArea=[[NSString alloc] initWithString:@"sqm"];
            self.measureUnitArea=tmpMeasureUnitArea;
            [tmpMeasureUnitArea release];
            NSString* tmpMeasureUnitLength=[[NSString alloc] initWithString:@"m"];
            self.measureUnitLength=tmpMeasureUnitLength;
            [tmpMeasureUnitLength release];
        }
        
       
        
        if ([currency isEqualToString:@"USD"]){
            NSString* tmpCurrencyUnit=[[NSString alloc] initWithString:@"$"];        
            self.currencyUnit=tmpCurrencyUnit;
            [tmpCurrencyUnit release];
        }else if ([currency isEqualToString:@"EUR"]){
            NSString* tmpCurrencyUnit=[[NSString alloc] initWithString:@"€"];        
            self.currencyUnit=tmpCurrencyUnit;
            [tmpCurrencyUnit release];
        }else if ([currency isEqualToString:@"GBP"]){
            NSString* tmpCurrencyUnit=[[NSString alloc] initWithString:@"£"];        
            self.currencyUnit=tmpCurrencyUnit;
            [tmpCurrencyUnit release];
        }else if ([currency isEqualToString:@"ZAR"]){
            NSString* tmpCurrencyUnit=[[NSString alloc] initWithString:@"R"];        
            self.currencyUnit=tmpCurrencyUnit;
            [tmpCurrencyUnit release];
        }else if ([currency isEqualToString:@"NOK"]){
            NSString* tmpCurrencyUnit=[[NSString alloc] initWithString:@"kr"];        
            self.currencyUnit=tmpCurrencyUnit;
            [tmpCurrencyUnit release];
        } 
        
        
        
    }
    return self;
}


/*
 
 isImperial=[[appDele currentSite] isImperial];;    
 measureUnitLength=nil;//@"m";
 measureUnitArea=nil;//@"sqm";        
 if (isImperial){
 NSString* _measureUnitArea=[[NSString alloc] initWithString:@"sqft"];
 self.measureUnitArea=_measureUnitArea;
 [_measureUnitArea release];
 NSString* _measureUnitLength=[[NSString alloc] initWithString:@"ft"];
 self.measureUnitLength=_measureUnitLength;
 [_measureUnitLength release];
 }else{
 
 NSString* _measureUnitArea=[[NSString alloc] initWithString:@"sqm"];
 self.measureUnitArea=_measureUnitArea;
 [_measureUnitArea release];
 NSString* _measureUnitLength=[[NSString alloc] initWithString:@"m"];
 self.measureUnitLength=_measureUnitLength;
 [_measureUnitLength release];
 }
 
 
 
 //    currencyUnit=nil;    
 NSString* currency=[[appDele currentSite] currency];
 
 if ([currency isEqualToString:@"USD"]){
 NSString* _currencyUnit=[[NSString alloc] initWithString:@"$"];        
 self.currencyUnit=_currencyUnit;
 [_currencyUnit release];
 }else if ([currency isEqualToString:@"EUR"]){
 NSString* _currencyUnit=[[NSString alloc] initWithString:@"€"];        
 self.currencyUnit=_currencyUnit;
 [_currencyUnit release];
 }else if ([currency isEqualToString:@"GBP"]){
 NSString* _currencyUnit=[[NSString alloc] initWithString:@"£"];        
 self.currencyUnit=_currencyUnit;
 [_currencyUnit release];
 }else if ([currency isEqualToString:@"ZAR"]){
 NSString* _currencyUnit=[[NSString alloc] initWithString:@"R"];        
 self.currencyUnit=_currencyUnit;
 [_currencyUnit release];
 }else if ([currency isEqualToString:@"NOK"]){
 NSString* _currencyUnit=[[NSString alloc] initWithString:@"kr"];        
 self.currencyUnit=_currencyUnit;
 [_currencyUnit release];
 } 
 

 
 */
@end

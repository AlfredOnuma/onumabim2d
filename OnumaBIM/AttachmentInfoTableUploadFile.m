//
//  AttachmentInfoTableUploadFile.m
//  ProjectView
//
//  Created by Alfred Man on 11/6/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "AttachmentInfoTableUploadFile.h"
#import "AttachmentInfoDetail.h"
#import "ProjectViewAppDelegate.h"
#import "Site.h"
#import "OPSProjectSite.h"
#import "OPSStudio.h"
@implementation AttachmentInfoTableUploadFile
@synthesize projectSite=_projectSite;
@synthesize attachmentInfoDetail=_attachmentInfoDetail;
@synthesize filePath=_filePath;
@synthesize fileName=_fileName;
@synthesize fileTitle=_fileTitle;
@synthesize attachedTo=_attachedTo;
@synthesize referenceID=_referenceID;
@synthesize uploadDate=_uploadDate;
@synthesize attachmentImageView=_attachmentImageView;

@synthesize connectionData=_connectionData;
@synthesize theConnection=_theConnection;
@synthesize attachFileID=_attachFileID;
@synthesize myData=_myData;
-(void) dealloc{
    [_filePath release];_filePath=nil;
    [_myData release];_myData=nil;
    [_fileName release];_fileName=nil;
    [_fileTitle release];_fileTitle=nil;
    [_attachedTo release];_attachedTo=nil;
    [_uploadDate release];_uploadDate=nil;
    [_theConnection release];_theConnection=nil;
    [_connectionData release];_connectionData=nil;
    [super dealloc];
}



//-(NSData*) upload:(NSArray*) paramArray{

-(void) upload:(NSArray*) paramArray{
        
    
    
    
    if (![self.filePath hasSuffix:@".jpg"]){
//        return nil;
        return;
    }
    
    
    
    //    [self trashAllAttachmentEntry];
    NSMutableData *data = [[NSMutableData alloc] init];;
    self.myData=data;
    [data release];
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    NSData* sqliteData=[NSData dataWithContentsOfFile:[self dbPath]];
    
    
    NSString *aBoundary = [NSString stringWithFormat:@"vYRNMz5SSe4vXrnc"];
    
    NSString *myBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", aBoundary];
    NSString *closingBoundary = [NSString stringWithFormat:@"\r\n--%@--\r\n", aBoundary];
    
    
    
    
    [self.myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
//    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", [ProjectViewAppDelegate urlEncodeValue:appDele.defUserName]] dataUsingEncoding:NSUTF8StringEncoding]];
    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", appDele.defUserName] dataUsingEncoding:NSUTF8StringEncoding]];
    [self.myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
//    NSString* testAttachedTo=self.attachedTo;
//    NSString* testUploadDate=self.uploadDate;
//    NSString* testFileTitle=self.fileTitle;
//    NSString* testFileName=self.fileName;
//    NSInteger testID=[appDele activeProjectSite].ID;
//    uint testStudioID=appDele.activeStudioID;
//    NSString* testUserPW=appDele.defUserPW;
//    NSString* testUserName=appDele.defUserName;
    
//    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", [ProjectViewAppDelegate urlEncodeValue:appDele.defUserPW]] dataUsingEncoding:NSUTF8StringEncoding]];
    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", appDele.defUserPW] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [self.myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"sysID\"\r\n\r\n%d", appDele.activeStudioID] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [self.myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"siteID\"\r\n\r\n%d", [appDele activeProjectSite].ID] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    [self.myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"filename\"\r\n\r\n%@", self.fileName] dataUsingEncoding:NSUTF8StringEncoding]];            
    
    [self.myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fileTitle\"\r\n\r\n%@", self.fileTitle] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [self.myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"attachedTo\"\r\n\r\n%@", self.attachedTo] dataUsingEncoding:NSUTF8StringEncoding]];
    
//    NSLog(@"%@",self.attachedTo);
    [self.myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploadDate\"\r\n\r\n%@", self.uploadDate] dataUsingEncoding:NSUTF8StringEncoding]];
    

    
    [self.myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"referenceID\"\r\n\r\n%d", self.referenceID] dataUsingEncoding:NSUTF8StringEncoding]];
    


    
    
//    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
//    [myData appendData:[@"Content-Disposition: form-data; name=\"uploadFile\"; filename=\"OPS\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [myData appendData:[@"Content-Type: application/x-sqlite3\r\n\r\n"dataUsingEncoding:NSUTF8StringEncoding]];
//    [myData appendData:sqliteData];
//    [myData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]]; //Ken
//
//    
    
    
    
    
    
    
    
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *databaseFolderPath = [paths objectAtIndex:0];
//
//    OPSStudio* studio=[appDele activeStudio];
//    
//    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], studio.ID, studio.name, studio.iconName ]];
//    
//    sqlite3_stmt *attachedFileStmt;
//    const char* attachedFileSql=[[NSString stringWithFormat:@"select attachfile.fileName from attachfile where sent=0"] UTF8String];
//    sqlite3 *database = nil;
//    uint pAttachedFile=0;
//    
    
    
//        //    NSString* dbPathToUpload=nil;
//        //    if ([self isKindOfClass:[OPSLocalProjectSiteInAttachmentInfoDetail class]]){
//        //
//        //
//        //
//        //
//        //        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
//        //        NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];
//        //
//        //        if (![[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {
//        //            // database doesn't exist in your library path... copy it from the bundle
//        //            NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"EmptySQLTable" ofType:@"sqlite"];
//        //            NSError *error = nil;
//        //
//        //            if (![[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:targetPath error:&error]) {
//        //                NSLog(@"Error: %@", error);
//        //            }
//        //        }
//        //        dbPathToUpload=targetPath;
//        //    }else{
//        //
//        //        dbPathToUpload=dbPath;
//        //    }
//        //
//        //
    
//    NSString* dbPathToUpload=self.dbPath;
//    
//    
//    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
//    NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];
//    
//    if (![[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {
//        // database doesn't exist in your library path... copy it from the bundle
//        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"EmptySQLTable" ofType:@"sqlite"];
//        NSError *error = nil;
//        
//        if (![[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:targetPath error:&error]) {
//            NSLog(@"Error: %@", error);
//        }
//    }
//    if (sqlite3_open ([dbPathToUpload UTF8String], &database) == SQLITE_OK) {
//        if(sqlite3_prepare_v2(database, attachedFileSql, -1, &attachedFileStmt, NULL) == SQLITE_OK) {
//            while( sqlite3_step(attachedFileStmt) == SQLITE_ROW) {
//                
//                NSString* attachedFileName= nil;
//                if ((char *) sqlite3_column_text(attachedFileStmt, 0)!=nil){
//                    attachedFileName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(attachedFileStmt, 0)];
//                }else{
//                    attachedFileName=[NSString stringWithFormat:@""];
//                }
//                
//                if (![attachedFileName hasSuffix:@".jpg"]){
//                    continue;
//                }
//              
//                NSString* imagePath=[databaseFolderPath stringByAppendingPathComponent:attachedFileName];
//                NSData *imageData = [[NSData alloc] initWithContentsOfFile:imagePath];
                NSData *imageData = [[NSData alloc] initWithContentsOfFile:self.filePath];

//    NSLog (@"%@",self.filePath);
                //Assuming data is not nil we add this to the multipart form
                if (imageData) {
                    
                    
                    
 
                    
                    
                    [self.myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
                    //        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    [self.myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"AttachedFileName\"; filename=\"%@\"\r\n",self.fileName] dataUsingEncoding:NSUTF8StringEncoding]];
                    [self.myData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    [self.myData appendData:imageData];
                    [self.myData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
                    
//                    NSLog (@"%@ %d %@",self.attachedTo,self.referenceID, self.fileName);
                    
                }
//                pAttachedFile++;
                [imageData release];
                
                
//            }
//        }
//        sqlite3_close(database);
//    }else{
//        sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
//    }
//
//    sqlite3_close(database);
//    
//    
//    

    
    [self.myData appendData:[closingBoundary dataUsingEncoding:NSUTF8StringEncoding]];
//    NSLog (@"%@",[[NSString alloc] initWithData:self.myData  encoding:NSASCIIStringEncoding]);
    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/postTestAction2.php"];
    //    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=%d&siteID=%d&filename=OPS",[appDele activeLiveStudio].ID, [selectedProjectSite ID]];
    NSURL *url = [NSURL URLWithString: urlString];
    //    UIImage *image = [[UIImage imageWithData: [NSData dataWithContentsOfURL: url]] retain];
    
    
    
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:900.0];
    
    
    [theRequest setHTTPMethod:@"POST"];
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [self.myData length]];
    [theRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", aBoundary];
    
    [theRequest setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    [theRequest setHTTPBody:self.myData];

    //    NSString* t=[theRequest valueForHTTPHeaderField:@"Content-Type"];
    //    NSLog(@"%@",t);
    
    
    
    // create the connection with the request
    // and start loading the data
    // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
    //    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    self.theConnection=aConnection;
    [aConnection release];
    if (self.theConnection) {
        // Create the NSMutableData that will hold
        // the received data
        // receivedData is declared as a method instance elsewhere
//        if (self.connectionData){
//            [self.connectionData release];
//        }
//        self.connectionData=[[NSMutableData data] retain];

        NSMutableData* data=[[NSMutableData alloc] init];
        self.connectionData=data;
        [data release];

    } else {
        // Inform the user that the connection failed.
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in uploading attachment file"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        [connectFailMessage release];
        
    }
//    [self.myData release];
    return ;
//    return  myData;
}



#pragma mark NSURLConnection methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.connectionData setLength:0];
    
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
// Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [self.connectionData appendData:data];    
//    NSLog (@"%d",[[self connectionData] length]);
//    NSLog (@"dataLen:%d",[self.connectionData length]);    
//    NSLog (@"%@",[[NSString alloc] initWithData:self.connectionData  encoding:NSASCIIStringEncoding]);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    if (self.attachmentInfoDetail){
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in uploading attachment file"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        [connectFailMessage release];
    }else{
        self.projectSite.numFileUploadFail++;
        [self.projectSite checkuploadFileStatus];
        

        [self.projectSite updateLoadingOverlayLabelStatus];
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//        appDele.loadingOverlayLabel.text=[NSString stringWithFormat:@"Uploaded %d/%d photo(s) and %d/%d user entered text from the iPad to the Onuma BIM Server. \n\rOne moment while this uploads.",self.projectSite.numFileUploadSuccess,[[self.projectSite aAttachmentInfoTableUploadFile] count],self.projectSite.numCommentUploadSuccess,[[self.projectSite aAttachmentInfoTableUploadComment] count]];
//        
//        if (self.projectSite.numFileUploadFail>0){
//            appDele.loadingOverlayLabel.text=[appDele.loadingOverlayLabel.text stringByAppendingFormat:@"\n\r%d file(s) failed to be uploaded.",[self.projectSite numFileUploadFail]];
//        }
//        
//             
//        if (self.projectSite.numFileUploadFail>0){
//            appDele.loadingOverlayLabel.text=[appDele.loadingOverlayLabel.text stringByAppendingFormat:@"\n\r%d user entered text failed to be uploaded.",[self.projectSite numCommentUploadFail]];
//        }
//        
//  
//        
//        
//        
//        [appDele.loadingOverlayLabel setNeedsDisplay];
    }
    
//    [self.attachmentInfoDetail actionOK:nil];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    
//    self.projectSite.numFileUploadSuccess++;
    
    
    NSString* dataStr=[[NSString alloc] initWithData:self.connectionData encoding:NSASCIIStringEncoding] ;
    //    NSString* dataStr=[[NSString alloc] initWithData:connectionData encoding:NSUTF8StringEncoding] ;
    NSLog(@"RESPONSEDATA: %@",dataStr);
    [dataStr release];
    //
    //    [self alertUploadResult:nil title:dataStr];
    //
    //    //    if ([dataStr isEqualToString:@"Upload Success"]){
    //    //
    //    //    }
    //    //
    //    [dataStr release];
    //
    //
    
//    NSString* dataStr=[[NSString alloc] initWithData:self.connectionData encoding:NSASCIIStringEncoding] ;
//    //    NSString* dataStr=[[NSString alloc] initWithData:connectionData encoding:NSUTF8StringEncoding] ;
//    NSLog(@"RESPONSEDATA: %@",dataStr);
    

    [_myData release];_myData=nil;
    [_theConnection release],_theConnection=nil;
    [_connectionData release],_connectionData=nil;
    
    if (self.attachmentInfoDetail){
        [self.attachmentInfoDetail uploadFileFinishWithAttachmentImageView:self.attachmentImageView];
    }else {
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//        bool bNoMoreAttachFile=true;
        OPSProjectSite* projectSite=[appDele activeProjectSite];
        [[NSFileManager defaultManager] removeItemAtPath:self.filePath error:NULL];

        
        
        NSString* fullImgDirectory=[self.filePath stringByDeletingLastPathComponent];
        NSString* fullImgPathLastComponent=[self.filePath lastPathComponent];
        
        
        NSString* slideImagePath=[[NSString alloc] initWithFormat:@"%@/sl_%@",fullImgDirectory,fullImgPathLastComponent];
        //            NSLog( @"%@",tbImagePath);
        [[NSFileManager defaultManager] removeItemAtPath: slideImagePath error:NULL];
        [slideImagePath release];
        
        
        NSString* tbImagePath=[[NSString alloc] initWithFormat:@"%@/tb_%@",fullImgDirectory,fullImgPathLastComponent];
        //            NSLog( @"%@",tbImagePath);
        [[NSFileManager defaultManager] removeItemAtPath: tbImagePath error:NULL];
        [tbImagePath release];
        
        
        
        //
        //            NSString* viewImagePath=[[NSString alloc] initWithFormat:@"%@/view_%@",fullImgDirectory,fullImgPathLastComponent];
        ////            NSLog( @"%@",viewImagePath);
        //            [fileManager removeItemAtPath: viewImagePath error:NULL];
        //            [viewImagePath release];
        
        
        
        if (self.attachFileID!=0){
            
            
            
//            
//            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//            
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            NSString *databaseFolderPath = [paths objectAtIndex:0];
//
//            OPSStudio* studio=[appDele activeStudio];
//
//            databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], studio.ID, studio.name, studio.iconName ]];

//        //            sqlite3_stmt *attachedFileStmt;
//        //            const char* attachedFileSql=[[NSString stringWithFormat:@"select attachfile.fileName from attachfile where sent=0"] UTF8String];
            sqlite3 *database = nil;
            
            if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
                sqlite3_stmt *deleteAttachFileStmt;
                const char* deleteAttachFileSql=[[NSString stringWithFormat:@"delete from AttachFile where ID=%d",
                                                  self.attachFileID] UTF8String];
                
                
                //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
                if(sqlite3_prepare_v2 (database, deleteAttachFileSql, -1, &deleteAttachFileStmt, NULL) == SQLITE_OK) {
                    while(sqlite3_step(deleteAttachFileStmt) == SQLITE_ROW) {
                        
                    }
                }                
                sqlite3_finalize(deleteAttachFileStmt);
                
                

//                sqlite3_stmt *selectAttachFileStmt;
//                const char* selectAttachFileSql=[@"select * from AttachFile" UTF8String];
//                
//                
//                //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
//                if(sqlite3_prepare_v2 (database, selectAttachFileSql, -1, &selectAttachFileStmt, NULL) == SQLITE_OK) {
//                    while(sqlite3_step(selectAttachFileStmt) == SQLITE_ROW) {
//                        bNoMoreAttachFile=false;
//                    }
//                }
//                sqlite3_finalize(selectAttachFileStmt);
            }
            
            sqlite3_close(database);
            
        }
//        if (bNoMoreAttachFile){
//            [self.projectSite uploadProjectSiteComment];
//        }
        
        
        self.projectSite.numFileUploadSuccess++;
        
        [self.projectSite checkuploadFileStatus];
        
        
        [self.projectSite updateLoadingOverlayLabelStatus];
        
//        appDele.loadingOverlayLabel.text=[NSString stringWithFormat:@"Uploaded %d/%d photo(s) and %d/%d user entered text from the iPad to the Onuma BIM Server. \n\rOne moment while this uploads.",self.projectSite.numFileUploadSuccess,[[self.projectSite aAttachmentInfoTableUploadFile] count],self.projectSite.numCommentUploadSuccess,[[self.projectSite aAttachmentInfoTableUploadComment] count]];
//        
//        if (self.projectSite.numFileUploadFail>0){
//            appDele.loadingOverlayLabel.text=[appDele.loadingOverlayLabel.text stringByAppendingFormat:@"\n\r%d file(s) failed to be uploaded.",[self.projectSite numFileUploadFail]];
//        }
//        
//        
//        if (self.projectSite.numFileUploadFail>0){
//            appDele.loadingOverlayLabel.text=[appDele.loadingOverlayLabel.text stringByAppendingFormat:@"\n\r%d user entered text failed to be uploaded.",[self.projectSite numCommentUploadFail]];
//        }
//        
//        
//        [appDele.loadingOverlayLabel setNeedsDisplay];
    }
    
}



-(id)initWithProjectSite:(OPSProjectSite*)projectSite attachFileID:(uint)attachFileID filePath:(NSString*)filePath fileName:(NSString*) fileName fileTitle:(NSString*) fileTitle attachedTo:(NSString*)attachedTo referenceID:(NSInteger) referenceID uploadDate:(NSString*) uploadDate{
    self=[super init];
    if (self){
        self.projectSite=projectSite;
        self.attachFileID=attachFileID;
        self.filePath=filePath;
        self.fileName=fileName;
        self.fileTitle=fileTitle;
        self.attachedTo=attachedTo;
        self.referenceID=referenceID;
        self.uploadDate=uploadDate;
    }
    return self;
}


-(id)initWithAttachmentImageView:(AttachmentImageView*) attachmentImageView attachmentInfoDetail:(AttachmentInfoDetail*)attachmentInfoDetail filePath:(NSString*)filePath fileName:(NSString*) fileName fileTitle:(NSString*) fileTitle attachedTo:(NSString*)attachedTo referenceID:(NSInteger) referenceID uploadDate:(NSString*) uploadDate{
    self=[super init];
    if (self){
        self.attachmentImageView=attachmentImageView;
        self.attachmentInfoDetail=attachmentInfoDetail;
        self.filePath=filePath;
        self.fileName=fileName;
        self.fileTitle=fileTitle;
        self.attachedTo=attachedTo;
        self.referenceID=referenceID;
        self.uploadDate=uploadDate;        
    }
    return self;
}

@end

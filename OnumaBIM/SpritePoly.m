//
//  SpritePoly.m
//  ProjectView
//
//  Created by Alfred Man on 2/10/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//
#import <math.h>
#import "SpritePoly.h"
#import "ViewModelVC.h"
#import "OPSModel.h"
#import "ExtrudedAreaSolid.h"
#import "CurveProfile.h"
#import "RepresentationItem.h"
#import "Slab.h"
#import "Bldg.h"
#import "Floor.h"
#import "Site.h"
#import "Furn.h"
#import "Space.h"
#import "ProjectViewAppDelegate.h"
#import "AlignColor.h"
#import "Department.h"
#import "CustomColorStruct.h"
#import "CustomColor.h"
#import "OPSProduct.h"
#import "ConstructionStatusColor.h"
#import "SitePolygon.h"
#import "UtilityLine.h"

#import "FusionBldgStatusColor.h"
#import "FusionBldgFCIColor.h"
#import "FusionBldgStatusFlagColor.h"
#import "FusionPlannedSFAssignedSFColor.h"
#import "FusionSpaceStatusFlagColor.h"
#import "FusionRecordStatusColor.h"
#import "FusionTopColor.h"
#import "FusionPGMColor.h"
#import "FusionRoomUseColor.h"



#import "BIMPlanColorTVC.h"



#import "AlignColorTVC.h"
#import "BIMPlanColorCatTVC.h"
#import "DepartmentColorTVC.h"
#import "CustomColorTVC.h"
#import "ConstructionStatusTVC.h"

#import "FusionBldgStatusTVC.h"
#import "FusionBldgFCITVC.h"
#import "FusionBldgStatusFlagTVC.h"

#import "FusionPlannedSFAssignedSFTVC.h"
#import "FusionSpaceStatusFlagTVC.h"
#import "FusionRecordStatusTVC.h"
#import "FusionTopTVC.h"
#import "FusionPGMTVC.h"
#import "FusionRoomUseTVC.h"
@implementation SpritePoly

-(void) dealloc{
//    NSLog(@"SpritePoly  Dealloc");
    [super dealloc];
}

-(void) addArcToBezierPath:(UIBezierPath*) path arcProfile:(ArcProfile*) thisArc curveProfile:(CurveProfile*)curveProfile
{
    ViewModelVC* viewModelVC=[self parentViewModelVC];// [appDele activeViewModelVC];
    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
    
    
    double centerX=thisArc.centerX*modelScaleFactor;
    double centerY=thisArc.centerY*modelScaleFactor;
    uint endPtIndex=thisArc.endPointIndex;

    uint pPrevPt=thisArc.endPointIndex-1;

    if (endPtIndex==[curveProfile.aPoint count]){
        endPtIndex=0;
    }
    if (endPtIndex==0){
        pPrevPt=[curveProfile.aPoint count]-1;
    }

    CPoint* point=[(CPoint*) [curveProfile.aPoint objectAtIndex:endPtIndex] copy];
    point.x*=modelScaleFactor;
    point.y*=modelScaleFactor;
    
    CPoint* prevPoint=(CPoint*) [curveProfile.aPoint objectAtIndex:pPrevPt];
    
    double startAngle=atan2((prevPoint.y*modelScaleFactor-centerY),(prevPoint.x*modelScaleFactor-centerX));
    
    double endAngle=atan2((point.y-centerY),(point.x-centerX));
    
    double radius=sqrt(pow(prevPoint.x*modelScaleFactor-centerX,2)+
                       pow(prevPoint.y*modelScaleFactor-centerY,2));
    
    [self.bPath addArcWithCenter:CGPointMake(centerX,centerY) radius:radius startAngle:startAngle endAngle:endAngle clockwise:thisArc.bAntiClock];
    
    [point release];
//    NSLog(@"centerX:%f centerY:%f rad:%f startAngle:%f endAngle:%f clockwise:%d",centerX,centerY,radius,startAngle,endAngle,thisArc.bAntiClock);
    
}


- (id)initWithTransform:(CGAffineTransform) transform viewProductRep:(ViewProductRep*)viewProductRep representationItem:(RepresentationItem*) representationItem displayModelUI:( DisplayModelUI*) displayModelUI{
//    self=[super initWithTransform:_transform viewproductRep:_viewProductRep representationItem:_representationItem displayModelUI:displayModelUI];
   self=[super initWithTransform:transform viewProductRep:viewProductRep representationItem:representationItem displayModelUI:displayModelUI];

    if (self){
//        drawCount=0;
//        OPSProduct* prod=[[self.representationItem representation] product];
//        if ([prod isKindOfClass:[Slab class]]){
//            prod=prod.linkedRel.relating;
//        }
//        NSLog (@"%@",prod.name );
        
        //        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate* ) [[UIApplication sharedApplication] delegate];
        ViewModelVC* viewModelVC=[self parentViewModelVC];// [appDele activeViewModelVC];
        double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
//        if (newRefProduct){
//            [newRefProduct addARefViewProductRep:self];
//        }
//        if (newGeoProduct){
//            [newGeoProduct addAGeoViewProductRep:self];
//        }
        //        [newGeoProduct setViewProduct:self];
//        geoProduct=newGeoProduct;        
//        refProduct=newRefProduct;
        if (representationItem==NULL || ![representationItem isKindOfClass:[ExtrudedAreaSolid class]] ){                     
            return self;
        }
        ExtrudedAreaSolid* thisExt=(ExtrudedAreaSolid*) representationItem;   
//        if (_path!=nil){
//            CGPathRelease(_path);
//            _path=nil;
//        }
        
//        [UIBezierPath bezierPathWithRect:clippingRec
        

        UIBezierPath* bPath=[[UIBezierPath alloc] init];
        self.bPath=bPath;
        

        [bPath release];
//        self.path = CGPathCreateMutable();

        
        
        NSMutableArray* aCurveProfile=thisExt.aCurveProfile;
        if (aCurveProfile==NULL||[aCurveProfile count]<=0) {return self;}
        
        
        for (int pCurveProfile=0;pCurveProfile<[aCurveProfile count];pCurveProfile++){
            CurveProfile* curveProfile=[aCurveProfile objectAtIndex:pCurveProfile];
            if (curveProfile==NULL) continue;
        
            
            
//            uint pPrevPt=[curveProfile.aPoint count]-1;

            
            NSMutableArray* aArcProfile=curveProfile.aArcProfile;
//            //            uint pArcProfile=0;
            
            NSMutableArray* aPoint=curveProfile.aPoint;
            if (aPoint==NULL||[aPoint count ]<=2) continue;
            
            for (int pPoint=0;pPoint < [aPoint count]; pPoint++){                       
                CPoint* point=[(CPoint*) [aPoint objectAtIndex:(pPoint)]copy ];
                point.x*=modelScaleFactor;
                point.y*=modelScaleFactor;
                ArcProfile* thisArc=nil;
//                //                bool bIsArc=false;
//                //                [boundBBox updatePoint:point];
                
//                                        NSLog(@"x = %f, y = %f", point.x, point.y);
                
                
                if (pPoint==0){                                   
//                    //                            CGPathMoveToPoint(_path,NULL,point.x,point.y);                            
//    //                    CGPathMoveToPoint(path,NULL,outsetBuffer+ point.x,outsetBuffer+ point.y);
                    [self.bPath moveToPoint:[point toCGPoint]];
//    //                    CGPathMoveToPoint(self.path,NULL,point.x,point.y);
                }else{
                    if (aArcProfile!=nil){
//    //                    for (uint pArc=pArcProfile;pArc<aCurveProfile.count;pArc++){
                        for (uint pArc=0;pArc<aArcProfile.count;pArc++){
                            ArcProfile* testArcProfile=[aArcProfile objectAtIndex:pArc];
                            if (testArcProfile.endPointIndex==pPoint){
                                thisArc=testArcProfile;//[aArcProfile objectAtIndex:pArc];
                                break;
                            }else{

                            }
                        }
                        //                            CGPathAddLineToPoint(_path,NULL,point.x,point.y);
    //                    CGPathAddLineToPoint(path,NULL,outsetBuffer+ point.x,outsetBuffer+ point.y);
    //                    CGPathAddLineToPoint(self.path,NULL,point.x,point.y);
                        

                        
                        
                        
                        if (thisArc!=nil){
                            
                            [self addArcToBezierPath:self.bPath arcProfile:thisArc curveProfile:curveProfile];
//    //                            [self.bPath addLineToPoint:[point toCGPoint]];
                            
                            
                            
                            
                            
//    //                            double centerX=thisArc.centerX*modelScaleFactor;
//    //                            double centerY=thisArc.centerY*modelScaleFactor;
//    //                            
//    //                            CPoint* prevPoint=(CPoint*) [aPoint objectAtIndex:pPrevPt];
//    //                            
//    //                            double startAngle=atan2((prevPoint.y*modelScaleFactor-centerY),(prevPoint.x*modelScaleFactor-centerX));
//    //                            
//    //                            double endAngle=atan2((point.y-centerY),(point.x-centerX));
//    //                            
//    //                            double radius=sqrt(pow(prevPoint.x*modelScaleFactor-centerX,2)+
//    //                                               pow(prevPoint.y*modelScaleFactor-centerY,2));
//    //                            NSLog(@"centerX:%f centerY:%f rad:%f startAngle:%f endAngle:%f clockwise:%d",centerX,centerY,radius,startAngle,endAngle,thisArc.bAntiClock);
//    //                            [self.bPath addArcWithCenter:CGPointMake(centerX,centerY) radius:radius startAngle:startAngle endAngle:endAngle clockwise:thisArc.bAntiClock];
//

                        }else{
                            [self.bPath addLineToPoint:[point toCGPoint]];
                        }

                    }else{
                        [self.bPath addLineToPoint:[point toCGPoint]];
                    }
                }
            
                [point release];
                
//    //                pPrevPt=pPoint;
                
            }
            
            if (aArcProfile!=nil){
                ArcProfile* endArc=nil;
                for (uint pArc=0;pArc<aArcProfile.count;pArc++){
                    ArcProfile* testArcProfile=[aArcProfile objectAtIndex:pArc];
                    if (testArcProfile.endPointIndex==0||testArcProfile.endPointIndex==[curveProfile.aPoint count]){
                        endArc=testArcProfile;//[aArcProfile objectAtIndex:pArc];
                        
                        [self addArcToBezierPath:self.bPath arcProfile:endArc curveProfile:curveProfile];
                        break;
                    }else{

                    }
                }
            }
            
            
            
            
            CPoint* p0=[(CPoint*) [aPoint objectAtIndex:0]copy ];
            p0.x*=modelScaleFactor;
            p0.y*=modelScaleFactor;
  
            [self.bPath addLineToPoint:[p0 toCGPoint]];
            [p0 release];
            break;
        }
            
            
        
        //         //---------------------------Temporarily remove-------------------------------------------------------------------------
        //        [self setBounds:CGRectOffset(self.bounds,boundBBox.minX,boundBBox.minY)];
        //        [boundBBox release];
        //        self.opaque = NO;        
        //        self.backgroundColor = [UIColor clearColor];
        //         //-----------------------------------------------------------------------------------------------------------------
        
    
    
    }
    
    //
    //    }
    return self;
    
}

-(UIColor*) onumaColorStringToUIColor:(NSString*) colorStr alpha:(float)alpha{
    
    if ([colorStr isEqualToString:@"RED"]){
        return [UIColor colorWithRed:1.0 green:0.0 blue: 0.0 alpha:alpha];
    }else if ([colorStr isEqualToString:@"GREEN"]){
        return [UIColor colorWithRed:0.0 green:1.0 blue: 0.0 alpha:alpha];
    }else if ([colorStr isEqualToString:@"BLUE"]){
        return [UIColor colorWithRed:0.0 green:0.0 blue: 1.0 alpha:alpha];
    }else if ([colorStr isEqualToString:@"ORANGE"]){
        return [UIColor colorWithRed:1.0 green:0.5 blue: 0.0 alpha:alpha];
    }else if ([colorStr isEqualToString:@"YELLOW"]){
        return [UIColor colorWithRed:1.0 green:1.0 blue: 0.0 alpha:alpha];
    }else if ([colorStr isEqualToString:@"PURPLE"]){
        return [UIColor colorWithRed:0.5 green:0.0 blue: 1.0 alpha:alpha];
    }else{
        return [UIColor colorWithRed:1.0 green:1.0 blue: 1.0 alpha:alpha];
    }
}
- (void) drawBody: (CGContextRef) context
{
//    drawCount++;
//    
//    if (drawCount>=2){;
//        NSLog(@"DrawCount %d . %@: %@",drawCount,[[self.viewProductRep.product class] description],[self.viewProductRep.product name]);
//        uint t=1;
//        t=2;
//    }
//  
//    ViewModelVC* modelVC=[self parentViewModelVC];
//    OPSProduct* selectedProduct=modelVC.selectedProduct;
//    OPSProduct* repProduct=[[[self representationItem] representation] product];
//    CGContextSetAllowsAntialiasing(context, false);    
//    CGContextSetShouldAntialias(UIGraphicsGetCurrentContext(), NO);
    
    
//    CGContextSetLineJoin(context, CGLineJoin join)
    OPSProduct* repProduct=[[[self representationItem] representation] product];
	
//    ViewProductRep* selectedProductRep=modelVC.selectedProductRep; 
//    bool selected=(selectedProductRep==self.viewProductRep);
    

    bool selected=[self.viewProductRep selected];
               
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];

     float alpha=(selected)?1.0:0.8;
    float defaultLineThk=([appDele modelDisplayLevel]==0)?3.0:1.0;//0.75;
    
// These two lines are needed for line redraw upon zooming
//    double zoomScale=[modelVC navUIScrollVC].zoomScale;
//     float lineThk=(selected)?defaultLineThk*2.0/zoomScale:defaultLineThk*1.0/zoomScale;  //6.0/zoomScale:1.0/zoomScale;
    
    float lineThk=(selected)?defaultLineThk*6.0:defaultLineThk*1.0;
    
    
    UIColor* lineColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];

//     CGContextBeginPath(context);
//     CGContextAddPath(context,self.bPath.CGPath);
//     CGContextClosePath(context);
    
    if ([repProduct isKindOfClass:[Slab class]]){
        if ([repProduct isGhostObject]){
//            CGContextSetRGBFillColor(context, 0.48, 0.67, 0.75, alpha/2.0);
            [[UIColor colorWithRed:0.48 green:0.67 blue: 0.75 alpha:alpha/2.0] setFill];
        }else{
//            CGContextSetRGBFillColor(context, 0.48, 0.67, 0.75, alpha);
            [[UIColor colorWithRed:0.48 green:0.67 blue: 0.75 alpha:alpha] setFill];
        }
    }else if ( [repProduct isKindOfClass:[Site class]]){
        lineThk=lineThk/2.0;
        lineColor=[UIColor redColor];
        alpha=alpha*0.5;
        if ([repProduct isGhostObject]){
//            CGContextSetRGBFillColor(context, 0.59, 0.8, 0.8, alpha/2.0);
//            [[UIColor colorWithRed:0.59 green:0.8 blue: 0.8 alpha:alpha/2.0] setFill];
            [[UIColor colorWithRed:0.96 green:0.96 blue: 0.73 alpha:alpha/2.0] setFill];
        }else{
//            CGContextSetRGBFillColor(context, 0.59, 0.8, 0.8, alpha);
            [[UIColor colorWithRed:0.96 green:0.96 blue: 0.73 alpha:alpha] setFill];
        }
    }else if ( [repProduct isKindOfClass:[SitePolygon class]]){
        
        lineThk=lineThk/1.5;
        
        
        SitePolygon* sitePolygon=(SitePolygon*) repProduct;
        if ([[sitePolygon color]isEqualToString:@""]){
            if ([[sitePolygon type] isEqualToString:@"ConstructionPad"]){
                [[UIColor colorWithRed:0.73 green:0.4 blue: 0.4 alpha:alpha*0.8] setFill];
            }else if  ([[sitePolygon type] isEqualToString:@"OpenSpace"]){
                [[UIColor colorWithRed:0.4 green:0.73 blue: 0.6 alpha:alpha*0.8] setFill];
            }else if  ([[sitePolygon type] isEqualToString:@"Parcel"]){
                [[UIColor colorWithRed:1.0 green:1.0 blue: 1.0 alpha:alpha*0.8] setFill];
            }else{            
                [[UIColor colorWithRed:0.87 green:0.87 blue: 0.45 alpha:alpha*0.8] setFill];
            }
        }else{
            [[self onumaColorStringToUIColor:[sitePolygon color] alpha:alpha*0.8] setFill];
        }
    }else if ( [repProduct isKindOfClass:[Furn class]]){
        if ([repProduct isGhostObject]){
//            CGContextSetRGBFillColor(context, 1.0,0.0,0.0,0.35);
            [[UIColor colorWithRed:1.0 green:0.0 blue: 0.0 alpha:0.35] setFill];
        }else{
            CGContextSetRGBFillColor(context, 1.0,0.0,0.0,0.7);
            [[UIColor colorWithRed:1.0 green:0.0 blue: 0.0 alpha:0.7] setFill];
        }
    }else{
        if ([repProduct isGhostObject]){        
//            CGContextSetRGBFillColor(context, 244.0/256.0, 244.0/256.0, 244.0/256.0,alpha/2.0);            
            [[UIColor colorWithRed:244.0/256.0 green: 244.0/244.0 blue: 244.0/256.0 alpha:alpha/2.0] setFill];
        }else{
//            CGContextSetRGBFillColor(context, 217.0/256.0, 217.0/256.0, 217.0/256.0,alpha);
            [[UIColor colorWithRed:217.0/256.0 green: 217.0/256.0 blue: 217.0/256.0 alpha:alpha] setFill];
//            CGContextSetRGBFillColor(context, 227.0/256.0, 227.0/256.0, 227.0/256.0,alpha);
//            CGContextSetRGBFillColor(context, 234.0/256.0, 234.0/256.0, 234.0/256.0,alpha);
        }
    }
    
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
    
    if ([appDele modelDisplayLevel]==0){
        
        Site* site=[appDele currentSite];
        
//        BIMPlanColorTVC* currentBIMPlanColorTVC=[[site model] currentBIMPlanColorTVC];
        
        uint currentBIMPlanColorMethod=[[self parentViewModelVC] currentColorMethod];
        
//        if (currentBIMPlanColorTVC==nil){
//            
//        }else if ([currentBIMPlanColorTVC isKindOfClass:[ConstructionStatusTVC class]]){
//
//
        
        if (currentBIMPlanColorMethod==sitePlan_NONE){
        }else if (currentBIMPlanColorMethod==sitePlan_CONSTRUCTION_STATUS){
                NSString* productType=[repProduct productType];
                SpatialStructure* containgSpatialStruct=nil;
                if ([productType isEqualToString:@"Slab"]){
                    containgSpatialStruct=(SpatialStructure*) [[repProduct linkedRel] relating];
                    if ([containgSpatialStruct isKindOfClass:[Floor class]]){
                        Floor* floor=(Floor*) containgSpatialStruct;
                        Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                       
                        
                        uint constructionStatusColorIndex=bldg.existing+1;

                        
                        ConstructionStatusColor* constructionColor=[site.constructionStatusDictionary valueForKey:[NSString stringWithFormat:@"%d",constructionStatusColorIndex]];
                        
                        UIColor* color=constructionColor.color;
                        
                        
    //                    Department* department=[[site aDepartment] objectAtIndex:(deptIDInArray)]; 
                        
    //                    UIColor* deptColor=department.color;
                        //        self.defaultColor=_defColor;
                        CGFloat red;
                        CGFloat green;
                        CGFloat blue;
                        CGFloat alpha2;
                        [color getRed:&red green:&green blue:&blue alpha:&alpha2];
    //                    //        
    //                    //        [_defColor release]; _defColor=nil;
    //                    //        self.labelDisplacement=CGPointMake(0.0,0.0);
    //                    
                        
//                        CGContextSetRGBFillColor(context, red, green, blue, alpha);
                        [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                    }
                    
                }
//        }else if ([currentBIMPlanColorTVC isKindOfClass:[FusionBldgStatusTVC class]]){
        }else if (currentBIMPlanColorMethod==sitePlan_FUSION_BLDG_STATUS){
                
                NSString* productType=[repProduct productType];
                SpatialStructure* containgSpatialStruct=nil;
                if ([productType isEqualToString:@"Slab"]){
                    containgSpatialStruct=(SpatialStructure*) [[repProduct linkedRel] relating];
                    if ([containgSpatialStruct isKindOfClass:[Floor class]]){
                        Floor* floor=(Floor*) containgSpatialStruct;
                        Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                        
                        
//                        FusionBldgStatusColor* fusionBldgStatusColor=[site.fusionBldgStatusDictionary valueForKey:@"Active"];
                        FusionBldgStatusColor* fusionBldgStatusColor=nil;
                        if (bldg.fusionBldgStatus && ![bldg.fusionBldgStatus isEqualToString:@""]){
                            fusionBldgStatusColor =[site.fusionBldgStatusDictionary valueForKey:[NSString stringWithFormat:@"%@",bldg.fusionBldgStatus]];
                            NSLog (@"%d : %@ - %@ | %@ , %@",bldg.ID,bldg.name,bldg.fusionBldgStatus,fusionBldgStatusColor.name, fusionBldgStatusColor.color);
                        }else{
                            fusionBldgStatusColor=[site.fusionBldgStatusDictionary valueForKey:@"DEFAULT"];
                        }
                        
                        UIColor* color=fusionBldgStatusColor.color;
                        
                        
                        //                    Department* department=[[site aDepartment] objectAtIndex:(deptIDInArray)];
                        
                        //                    UIColor* deptColor=department.color;
                        //        self.defaultColor=_defColor;
                        CGFloat red;
                        CGFloat green;
                        CGFloat blue;
                        CGFloat alpha2;
                        [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                        //                    //
                        //                    //        [_defColor release]; _defColor=nil;
                        //                    //        self.labelDisplacement=CGPointMake(0.0,0.0);
                        //
                        
                        //                        CGContextSetRGBFillColor(context, red, green, blue, alpha);
                        [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                    }
                    
                }
            
//        }else if ([currentBIMPlanColorTVC isKindOfClass:[FusionBldgFCITVC class]]){
            
        }else if (currentBIMPlanColorMethod==sitePlan_FUSION_BLDG_FCI){

                NSString* productType=[repProduct productType];
                SpatialStructure* containgSpatialStruct=nil;
                if ([productType isEqualToString:@"Slab"]){
                    containgSpatialStruct=(SpatialStructure*) [[repProduct linkedRel] relating];
                    if ([containgSpatialStruct isKindOfClass:[Floor class]]){
                        Floor* floor=(Floor*) containgSpatialStruct;
                        Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                        uint fci=bldg.fusionBldgFCI;
                        
                        FusionBldgFCIColor* fusionBldgFCIColor=nil;
                        if (fci==0){
                            fusionBldgFCIColor=[site.fusionBldgFCIDictionary valueForKey:@"=0"];
                        }else if (fci>90){
                            fusionBldgFCIColor=[site.fusionBldgFCIDictionary valueForKey:@">90"];
                        }else if (fci>50){
                            fusionBldgFCIColor=[site.fusionBldgFCIDictionary valueForKey:@">50"];
                        }else if (fci>10){
                            fusionBldgFCIColor=[site.fusionBldgFCIDictionary valueForKey:@">10"];
                        }else {
                            fusionBldgFCIColor=[site.fusionBldgFCIDictionary valueForKey:@">0"];
                        }
                        UIColor* color=fusionBldgFCIColor.color;
                        
                        
                        //                    Department* department=[[site aDepartment] objectAtIndex:(deptIDInArray)];
                        
                        //                    UIColor* deptColor=department.color;
                        //        self.defaultColor=_defColor;
                        CGFloat red;
                        CGFloat green;
                        CGFloat blue;
                        CGFloat alpha2;
                        [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                        //                    //
                        //                    //        [_defColor release]; _defColor=nil;
                        //                    //        self.labelDisplacement=CGPointMake(0.0,0.0);
                        //
                        
                        //                        CGContextSetRGBFillColor(context, red, green, blue, alpha);
                        [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                        
//                        [fusionBldgFCIColor release];fusionBldgFCIColor=nil;
                    }
                    
                }
            
//        }else if ([currentBIMPlanColorTVC isKindOfClass:[FusionBldgStatusFlagTVC class]]){            
        }else if (currentBIMPlanColorMethod==sitePlan_FUSION_BLDG_STATUS_FLAG){

                
                NSString* productType=[repProduct productType];
                SpatialStructure* containgSpatialStruct=nil;
                if ([productType isEqualToString:@"Slab"]){
                    containgSpatialStruct=(SpatialStructure*) [[repProduct linkedRel] relating];
                    if ([containgSpatialStruct isKindOfClass:[Floor class]]){
                        Floor* floor=(Floor*) containgSpatialStruct;
                        Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                        FusionBldgStatusFlagColor* fusionBldgStatusFlagColor=nil;
                        if (bldg.bldgPathID==0){
                            fusionBldgStatusFlagColor=[site.fusionBldgStatusFlagDictionary valueForKey:@"Not in FUSION"];
                        }else if (bldg.fusionBldgPathID>0){
                            if (bldg.fusionBldgStatusFlag_newAdded==1){
                                fusionBldgStatusFlagColor=[site.fusionBldgStatusFlagDictionary valueForKey:@"Added from FUSION"];
                            }else{
                                fusionBldgStatusFlagColor=[site.fusionBldgStatusFlagDictionary valueForKey:@"Matches FUSION"];                                
                            }
                        }else{
                            fusionBldgStatusFlagColor=[site.fusionBldgStatusFlagDictionary valueForKey:@"Deleted in FUSION"];
                        }
                        
                        UIColor* color=fusionBldgStatusFlagColor.color;
                        
                        
                        //                    Department* department=[[site aDepartment] objectAtIndex:(deptIDInArray)];
                        
                        //                    UIColor* deptColor=department.color;
                        //        self.defaultColor=_defColor;
                        CGFloat red;
                        CGFloat green;
                        CGFloat blue;
                        CGFloat alpha2;
                        [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                        //                    //
                        //                    //        [_defColor release]; _defColor=nil;
                        //                    //        self.labelDisplacement=CGPointMake(0.0,0.0);
                        //
                        
                        //                        CGContextSetRGBFillColor(context, red, green, blue, alpha);
                        [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                        
                        //                        [fusionBldgFCIColor release];fusionBldgFCIColor=nil;
                    }
                    
                }
            
        }else {

                NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
                uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
                
                if (aCustomColorStruct!=nil && [aCustomColorStruct count]>0){
                  
                    
                    
                    CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:activeCustomColorStruct];
                    if (customColorStruct!=nil) {
                        
                        //                if (([repProduct isKindOfClass:[Space class]] && [customColorStruct.type isEqualToString:@"Space"]) ||
                        //                    ([repProduct isKindOfClass:[Bldg class]] && [customColorStruct.type isEqualToString:@"Bldg"])
                        //                    ){
                        //
                        NSString* productType=[repProduct productType];
                        
                        SpatialStructure* containgSpatialStruct=nil;
                        
                        
                        
                        if ([productType isEqualToString:@"Slab"]){
                            containgSpatialStruct=(SpatialStructure*) [[repProduct linkedRel] relating];
                            if ([containgSpatialStruct isKindOfClass:[Floor class]]){
                                Bldg* bldg=(Bldg*) [[containgSpatialStruct linkedRel] relating];
                                containgSpatialStruct=bldg;
                            }
                            //                        if (containgSpatialStruct.ID=17533){
                            //                            uint t=0;
                            //                        }
                            if ([containgSpatialStruct customColorIndexDictionary]!=NULL){
                                
                                if ([[containgSpatialStruct productType] isEqualToString:customColorStruct.type]){
                                    
                                    uint productCustomColorIndex=[containgSpatialStruct customColorIndexWithDictionaryIndex:customColorStruct.customSettingNum];
                                    if (productCustomColorIndex>0){
                                        productCustomColorIndex--; // Back to index starting from 0
                                        //                                    CustomColor* customColor=[customColorStruct.aCustomColor objectAtIndex:productCustomColorIndex];
                                        CustomColor* customColor=[customColorStruct getCustomColorWithCustomInt:productCustomColorIndex];
                                        UIColor* color=customColor.color;
                                        //        self.defaultColor=_defColor;+
                                        CGFloat red;
                                        CGFloat green;
                                        CGFloat blue;
                                        CGFloat alpha2;
                                        [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                                        //
                                        //        [_defColor release]; _defColor=nil;
                                        //        self.labelDisplacement=CGPointMake(0.0,0.0);
                                        
                                        
                                        //                                    CGContextSetRGBFillColor(context, red, green, blue, alpha);
                                        [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                                    }
                                    //                    CustomColor* customColor=cu
                                }
                            }
                        }else{
                            
                            if ([repProduct customColorIndexDictionary]!=NULL){
                                if ([[repProduct productType] isEqualToString:customColorStruct.type]){
                                    
                                    uint productCustomColorIndex=[repProduct customColorIndexWithDictionaryIndex:customColorStruct.customSettingNum];
                                    if (productCustomColorIndex>0){
                                        productCustomColorIndex--; // Back to index starting from 0
                                        //                                    CustomColor* customColor=[customColorStruct.aCustomColor objectAtIndex:productCustomColorIndex];
                                        CustomColor* customColor=[customColorStruct getCustomColorWithCustomInt:productCustomColorIndex];
                                        UIColor* color=customColor.color;
                                        //        self.defaultColor=_defColor;
                                        CGFloat red;
                                        CGFloat green;
                                        CGFloat blue;
                                        CGFloat alpha2;
                                        [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                                        //                                    CGContextSetRGBFillColor(context, red, green, blue, alpha);
                                        
                                        [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                                    }
                                    //                    CustomColor* customColor=cu
                                }
                            }
                        }
                        
                    }
                }
                
                
               

        }
        
    }else if ([appDele modelDisplayLevel]==1){
        Site* site=[appDele currentSite];
//        BIMPlanColorTVC* currentBIMPlanColorTVC=[[modelVC model] currentBIMPlanColorTVC];
        int currentBIMPlanColorMethod=[[self parentViewModelVC] currentColorMethod];
        if (currentBIMPlanColorMethod==floorPlan_NONE){
            
            
//        }else if ([currentBIMPlanColorTVC isKindOfClass:[DepartmentColorTVC class]]){
        }else if (currentBIMPlanColorMethod==floorPlan_DEPARTMENT){

           
                if ([repProduct isKindOfClass:[Space class]] && [site aDepartment]){  
//                    OPSProduct* product=[[self viewProductRep] product];

                    Space* space=(Space*) repProduct;
                    if (space.deptID>0){
                        uint deptIDInArray=space.deptID-1;
                        Department* department=[[site aDepartment] objectAtIndex:(deptIDInArray)]; 
                        
                        UIColor* deptColor=department.color;
                        //        self.defaultColor=_defColor;
                        CGFloat red;
                        CGFloat green;
                        CGFloat blue;
                        CGFloat alpha2;
                        [deptColor getRed:&red green:&green blue:&blue alpha:&alpha2];
    //                    CGContextSetRGBFillColor(context, red, green, blue, alpha);
                        
                        [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                    }
                }

//       }else if ([currentBIMPlanColorTVC isKindOfClass:[AlignColorTVC class]]){           
       }else if (currentBIMPlanColorMethod==floorPlan_VERTICAL_ALIGNMENT){
                if ([repProduct isKindOfClass:[Space class]] && [site alignColorDictionary]){  
                    Space* space=(Space*) repProduct;
                    if (space.alignID>0) {
                        //                    OPSProduct* product=[[self viewProductRep] product];
                        
                        AlignColor* alignColor=[site.alignColorDictionary valueForKey:[NSString stringWithFormat:@"%d",space.ID]];
                        
                        UIColor* color=alignColor.color;
                        //        self.defaultColor=_defColor;
                        CGFloat red;
                        CGFloat green;
                        CGFloat blue;
                        CGFloat alpha2;
                        [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                        //
                        //        [_defColor release]; _defColor=nil;
                        //        self.labelDisplacement=CGPointMake(0.0,0.0);
                        
                        
                        //                    CGContextSetRGBFillColor(context, red, green, blue, alpha);
                        
                        [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                    }

                    
                }                
                
//                AlignColor* alignColor=[[site.alignColorDictionary allValues] objectAtIndex:[indexPath row]];
//                //            for (AlignColor* alignColor in [site.alignColorDictionary allValues]){
//                cell.colorLabel.text=alignColor.name;
//                cell.colorUIView.backgroundColor=alignColor.color;
//                
//                //            }
                
//       }else if ([currentBIMPlanColorTVC isKindOfClass:[FusionPlannedSFAssignedSFTVC class]]){
           
       }else if (currentBIMPlanColorMethod==floorPlan_FUSION_PLANNEDSFASSIGNEDSF){
           //Color by Fusion Planned SF / Assigned SF
                
                if ([repProduct isKindOfClass:[Space class]] && [site fusionPlannedSFAssignedSFDictionary]){
                    Space* space=(Space*) repProduct;
                    FusionPlannedSFAssignedSFColor* fusionPlannedSFAssignedSFColor=nil;
                    if (space.fusion_SpaceInfoRoomPathID>0){
                        if (space.fusion_SpaceInfoAssignedSF==0){
                            fusionPlannedSFAssignedSFColor=[site.fusionPlannedSFAssignedSFDictionary valueForKey:@"Assigned SF = 0"];
                        }else{
                            double sqMsqFt=10.7639104 ;
                            double ratio=space.spaceArea*sqMsqFt/space.fusion_SpaceInfoAssignedSF;
                            double diff=fabs(ratio-1.0);
                            if (diff < 0.01){                                
                                fusionPlannedSFAssignedSFColor=[site.fusionPlannedSFAssignedSFDictionary valueForKey:@"Difference < 1%"];
                            }else if (diff < 0.1){
                                fusionPlannedSFAssignedSFColor=[site.fusionPlannedSFAssignedSFDictionary valueForKey:@"Between 1% and 9%"];
                            }else{
                                fusionPlannedSFAssignedSFColor=[site.fusionPlannedSFAssignedSFDictionary valueForKey:@"Difference >= 10%"];
                            }
                            
                        } 
                    }else{
                        
                        fusionPlannedSFAssignedSFColor=[site.fusionPlannedSFAssignedSFDictionary valueForKey:@"No Assigned SF"];
                    }

  
                    UIColor* color=fusionPlannedSFAssignedSFColor.color;
                    
                    
                    //                    Department* department=[[site aDepartment] objectAtIndex:(deptIDInArray)];
                    
                    //                    UIColor* deptColor=department.color;
                    //        self.defaultColor=_defColor;
                    CGFloat red;
                    CGFloat green;
                    CGFloat blue;
                    CGFloat alpha2;
                    [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                    //                    //
                    //                    //        [_defColor release]; _defColor=nil;
                    //                    //        self.labelDisplacement=CGPointMake(0.0,0.0);
                    //
                    
                    //                        CGContextSetRGBFillColor(context, red, green, blue, alpha);
                    [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                    
                    //                        [fusionBldgFCIColor release];fusionBldgFCIColor=nil;
                }
                
        
//       }else if ([currentBIMPlanColorTVC isKindOfClass:[FusionSpaceStatusFlagTVC class]]){
           
       }else if (currentBIMPlanColorMethod==floorPlan_FUSION_SPACE_STATUS_FLAG){
           if ([repProduct isKindOfClass:[Space class]] && [site fusionSpaceStatusFlagDictionary]){
                    Space* space=(Space*) repProduct;
                    FusionSpaceStatusFlagColor* fusionSpaceStatusFlagColor=nil;
                    
                    
                    
                    if (space.fusion_ignoreFusionSpaceNumber==1){
                        fusionSpaceStatusFlagColor=[site.fusionSpaceStatusFlagDictionary valueForKey:@"Ignore Fusion Space Number"];
                    }else if (space.roomPathID==0){
                        fusionSpaceStatusFlagColor=[site.fusionSpaceStatusFlagDictionary valueForKey:@"Not in FUSION"];
                    }else if (space.fusion_SpaceInfoRoomPathID>0){
                        if (space.newAdded==1){
                            fusionSpaceStatusFlagColor=[site.fusionSpaceStatusFlagDictionary valueForKey:@"Added from FUSION"];
                        }else{
                           fusionSpaceStatusFlagColor=[site.fusionSpaceStatusFlagDictionary valueForKey:@"Matches FUSION"];
                        }
                    }else{
                        fusionSpaceStatusFlagColor=[site.fusionSpaceStatusFlagDictionary valueForKey:@"Deleted in FUSION"];
                    }
                    
                    
                    UIColor* color=fusionSpaceStatusFlagColor.color;
                    
                    
                    //                    Department* department=[[site aDepartment] objectAtIndex:(deptIDInArray)];
                    
                    //                    UIColor* deptColor=department.color;
                    //        self.defaultColor=_defColor;
                    CGFloat red;
                    CGFloat green;
                    CGFloat blue;
                    CGFloat alpha2;
                    [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                    //                    //
                    //                    //        [_defColor release]; _defColor=nil;
                    //                    //        self.labelDisplacement=CGPointMake(0.0,0.0);
                    //
                    
                    //                        CGContextSetRGBFillColor(context, red, green, blue, alpha);
                    [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                    
                    //                        [fusionBldgFCIColor release];fusionBldgFCIColor=nil;
                }
                
                
//       }else if ([currentBIMPlanColorTVC isKindOfClass:[FusionRecordStatusTVC class]]){
           
       }else if (currentBIMPlanColorMethod==floorPlan_FUSION_RECORDSTATUS){
           
                if ([repProduct isKindOfClass:[Space class]] && [site fusionRecordStatusDictionary]){
                    Space* space=(Space*) repProduct;
                    FusionRecordStatusColor* fusionRecordStatusColor=nil;
                    
                    
                    
                    if (space.roomPathID>0){

                        if ([[space.fusion_roomStatus uppercaseString] isEqualToString:@"ASSIGNABLE"]){
                            fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"ASSIGNABLE"];
                        }else if ([[space.fusion_roomStatus uppercaseString] isEqualToString:@"UNCLASSIFIED"]){
                            fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"UNCLASSIFIED"];
                        }else if  ([[space.fusion_roomStatus uppercaseString] isEqualToString:@"DEACTIVATED"]){
                            fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"DEACTIVATED"];
                        }else if  ([[space.fusion_roomStatus uppercaseString] isEqualToString:@"NON-ASSIGNABLE"]){
                            fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"NON-ASSIGNABLE"];
                        }else {
                            fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"Not in FUSION"];
                        }
                    }else{
                        fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"Not in FUSION"];
                    }
                    
                    
                    UIColor* color=fusionRecordStatusColor.color;
                    
                    
                    //                    Department* department=[[site aDepartment] objectAtIndex:(deptIDInArray)];
                    
                    //                    UIColor* deptColor=department.color;
                    //        self.defaultColor=_defColor;
                    CGFloat red;
                    CGFloat green;
                    CGFloat blue;
                    CGFloat alpha2;
                    [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                    //                    //
                    //                    //        [_defColor release]; _defColor=nil;
                    //                    //        self.labelDisplacement=CGPointMake(0.0,0.0);
                    //
                    
                    //                        CGContextSetRGBFillColor(context, red, green, blue, alpha);
                    [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                    
                    //                        [fusionBldgFCIColor release];fusionBldgFCIColor=nil;
                }
                
//       }else if ([currentBIMPlanColorTVC isKindOfClass:[FusionTopTVC class]]){
           
       }else if (currentBIMPlanColorMethod==floorPlan_FUSION_TOP){
                
                if ([repProduct isKindOfClass:[Space class]] && [site fusionTopDictionary]){
                    Space* space=(Space*) repProduct;
                    
                    FusionTopColor* fusionTopColor=[site.fusionTopDictionary valueForKey:[NSString stringWithFormat:@"%d",space.fusion_topCssCode]];
                    if (fusionTopColor){
                        UIColor* color=fusionTopColor.color;
                        
                        
                        //                    Department* department=[[site aDepartment] objectAtIndex:(deptIDInArray)];
                        
                        //                    UIColor* deptColor=department.color;
                        //        self.defaultColor=_defColor;
                        CGFloat red;
                        CGFloat green;
                        CGFloat blue;
                        CGFloat alpha2;
                        [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                        //                    //
                        //                    //        [_defColor release]; _defColor=nil;
                        //                    //        self.labelDisplacement=CGPointMake(0.0,0.0);
                        //
                        
                        //                        CGContextSetRGBFillColor(context, red, green, blue, alpha);
                        [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                        
                        //                        [fusionBldgFCIColor release];fusionBldgFCIColor=nil;
                    }
                }
                
//       }else if ([currentBIMPlanColorTVC isKindOfClass:[FusionPGMTVC class]]){
           
       }else if (currentBIMPlanColorMethod==floorPlan_FUSION_PGM){
           if ([repProduct isKindOfClass:[Space class]] && [site fusionPGMDictionary]){
                    Space* space=(Space*) repProduct;
                    
                    FusionPGMColor* fusionPGMColor=[site.fusionPGMDictionary valueForKey:[NSString stringWithFormat:@"%d",space.fusion_pgmNum]];
                    if (fusionPGMColor){
                        UIColor* color=fusionPGMColor.color;
                        
                        
                        //                    Department* department=[[site aDepartment] objectAtIndex:(deptIDInArray)];
                        
                        //                    UIColor* deptColor=department.color;
                        //        self.defaultColor=_defColor;
                        CGFloat red;
                        CGFloat green;
                        CGFloat blue;
                        CGFloat alpha2;
                        [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                        //                    //
                        //                    //        [_defColor release]; _defColor=nil;
                        //                    //        self.labelDisplacement=CGPointMake(0.0,0.0);
                        //
                        
                        //                        CGContextSetRGBFillColor(context, red, green, blue, alpha);
                        [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                        
                        //                        [fusionBldgFCIColor release];fusionBldgFCIColor=nil;
                    }
                }
                
//       }else if ([currentBIMPlanColorTVC isKindOfClass:[FusionRoomUseTVC class]]){
           
       }else if (currentBIMPlanColorMethod==floorPlan_FUSION_ROOM_USE){
                
                if ([repProduct isKindOfClass:[Space class]] && [site fusionRoomUseDictionary]){
                    Space* space=(Space*) repProduct;
                    
                    FusionRoomUseColor* fusionRoomUseColor=[site.fusionRoomUseDictionary valueForKey:[NSString stringWithFormat:@"%d",space.fusion_roomUseCode]];
                    if (fusionRoomUseColor){
                        UIColor* color=fusionRoomUseColor.color;
                        
                        
                        //                    Department* department=[[site aDepartment] objectAtIndex:(deptIDInArray)];
                        
                        //                    UIColor* deptColor=department.color;
                        //        self.defaultColor=_defColor;
                        CGFloat red;
                        CGFloat green;
                        CGFloat blue;
                        CGFloat alpha2;
                        [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                        //                    //
                        //                    //        [_defColor release]; _defColor=nil;
                        //                    //        self.labelDisplacement=CGPointMake(0.0,0.0);
                        //
                        
                        //                        CGContextSetRGBFillColor(context, red, green, blue, alpha);
                        [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                        
                        //                        [fusionBldgFCIColor release];fusionBldgFCIColor=nil;
                    }
                }
                
                
       }else{
       
            NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
            uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
            
            if (aCustomColorStruct!=nil && [aCustomColorStruct count]>0){
                
                CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:activeCustomColorStruct];
                if (customColorStruct!=Nil) {
                    
                    //                if (([repProduct isKindOfClass:[Space class]] && [customColorStruct.type isEqualToString:@"Space"]) ||
                    //                    ([repProduct isKindOfClass:[Bldg class]] && [customColorStruct.type isEqualToString:@"Bldg"])
                    //                    ){
                    //
                    if ([repProduct customColorIndexDictionary]!=NULL){
                        if ([[repProduct productType] isEqualToString:customColorStruct.type]){
                            
                            uint productCustomColorIndex=[repProduct customColorIndexWithDictionaryIndex:customColorStruct.customSettingNum];
                            if (productCustomColorIndex>0){
                                productCustomColorIndex--; // Back to index starting from 0
                                //                            CustomColor* customColor=[customColorStruct.aCustomColor objectAtIndex:productCustomColorIndex];
                                //                            if (repProduct.ID==11402){
                                //                                int t=0;
                                //                                t=2;
                                //                            }
                                CustomColor* customColor=[customColorStruct getCustomColorWithCustomInt:productCustomColorIndex];
                                
                                UIColor* color=customColor.color;
                                //        self.defaultColor=_defColor;
                                CGFloat red;
                                CGFloat green;
                                CGFloat blue;
                                CGFloat alpha2;
                                [color getRed:&red green:&green blue:&blue alpha:&alpha2];
                                //
                                //        [_defColor release]; _defColor=nil;
                                //        self.labelDisplacement=CGPointMake(0.0,0.0);
                                
                                
                                //                            CGContextSetRGBFillColor(context, red, green, blue, alpha);
                                [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
                                
                            }
                            //                    CustomColor* customColor=cu
                            
                        }
                    }
                }
            }
                
    
                
                
                
        }
    }
     
//     CGContextFillPath(context);
    [self.bPath fill];
    
//    CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
//    CGContextSetLineWidth(context, lineThk);
    [lineColor setStroke];
    

    
    self.bPath.lineWidth=lineThk;
    [self.bPath stroke];
    
    
//     CGContextBeginPath(context);
//    CGContextAddPath(context,self.bPath.CGPath);
//     CGContextClosePath(context);
//    
//
//     CGContextStrokePath(context);     
}


@end

//
//  SpriteBitmap.m
//  ProjectView
//
//  Created by Alfred Man on 2/10/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//
#import "ProjectViewAppDelegate.h"
#import "SpriteBitmap.h"
#import "BitmapRep.h"
#import "ViewModelVC.h"
@implementation SpriteBitmap
@synthesize image;
//@synthesize imageView;
@synthesize imageFrame;


- (id)initWithTransform:(CGAffineTransform) transform viewProductRep:(ViewProductRep*)viewProductRep representationItem:(RepresentationItem*) representationItem displayModelUI:( DisplayModelUI*) displayModelUI{
    
    
    
    self=[super initWithTransform:transform viewProductRep:viewProductRep representationItem:representationItem displayModelUI:displayModelUI];
    if (self){
            
        //        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];
        double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
        
        
        
        if (representationItem==NULL || ![representationItem isKindOfClass:[BitmapRep class]] ){                     
            return self;
        }
        BitmapRep* bitmapRep=(BitmapRep*) representationItem;
  
//        NSString* imgName=[NSString stringWithFormat:@"%@.png",[bitmapRep name]];
        
//        UIImageView* _imageView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:imgName]];
//        self.imageView = _imageView;
//        [_imageView release];
        
//        NSString* myImagePath=[NSString stringWithFormat:@"/Users/onuma/iOS/furnBitmaps/%@",imgName];
        
//        NSString *myImagePath = [[[NSBundle mainBundle] resourcePath]  stringByAppendingString:imgName];
//        NSString *myImagePath = [[[NSBundle mainBundle] bundlePath]  stringByAppendingString:imgName];
        
        
        NSString* imgName=[NSString stringWithFormat:@"%@",[bitmapRep name]];
        
        NSString *myImagePath = [[NSBundle mainBundle] pathForResource:imgName ofType:@"png"];
        UIImage* tmpImage=[[UIImage alloc] initWithContentsOfFile:myImagePath];
        self.image=tmpImage;
        [tmpImage release];
        
//        image=[UIImage imageNamed:imgName] ;
        
        //        BBox* imgViewBBox=[[bitmapRep bBox] copy];
        //        [imgViewBBox multiply:modelScaleFactor];
        
        //                    [bitmapRep.bBox ];
        imageFrame=[[bitmapRep bBox] cgRect];
        imageFrame=CGRectApplyAffineTransform(imageFrame, CGAffineTransformMakeScale(modelScaleFactor,modelScaleFactor));

//        if (self.path){
//            CGPathRelease(_path);
//            _path=nil;
//        }
        UIBezierPath* bPath=[[UIBezierPath alloc] init];
        self.bPath=bPath;
        [bPath release]; bPath=nil;
        
        
//        self.path = CGPathCreateMutable();
        CGPoint bitmapPathOrigin=CGPointMake (imageFrame.origin.x, imageFrame.origin.y);
        CGFloat bitmapPathWidth=imageFrame.size.width;
        CGFloat bitmapPathHeight=imageFrame.size.height;                
        
        CGPoint p0=CGPointMake(bitmapPathOrigin.x,                  bitmapPathOrigin.y);
        CGPoint p1=CGPointMake(bitmapPathOrigin.x+bitmapPathWidth,  bitmapPathOrigin.y);                   
        CGPoint p2=CGPointMake(bitmapPathOrigin.x+bitmapPathWidth,  bitmapPathOrigin.y+bitmapPathHeight);
        CGPoint p3=CGPointMake(bitmapPathOrigin.x,                  bitmapPathOrigin.y+bitmapPathHeight);
        
//        CGPathMoveToPoint   (self.path,NULL,p0.x,p0.y);
//        CGPathAddLineToPoint(self.path,NULL,p1.x,p1.y);
//        CGPathAddLineToPoint(self.path,NULL,p2.x,p2.y);
//        CGPathAddLineToPoint(self.path,NULL,p3.x,p3.y);
        
        [self.bPath moveToPoint:p0];
        [self.bPath addLineToPoint:p1];
        [self.bPath addLineToPoint:p2];
        [self.bPath addLineToPoint:p3];


    }
    return self;
    
        
}


- (void) drawBody: (CGContextRef) context
{
    
    
//    ViewModelVC* modelVC=[self parentViewModelVC];    
//    ViewProductRep* selectedProductRep=modelVC.selectedProductRep;        
//    bool selected=(selectedProductRep==self.viewProductRep);    
    bool selected=[self.viewProductRep selected];

//    OPSProduct* repProduct=[[[self representationItem] representation] product];
//    bool selected=(selectedProduct==repProduct);
//    double zoomScale=[modelVC navUIScrollVC].zoomScale;
    
    
//if ( [self.geoProduct isKindOfClass:[Furn class]]){  


    CGContextSaveGState(context);
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
    if ([appDele modelDisplayLevel]==2){        
        if (selected){
            CGContextSetAlpha(context, 1.0);
        }else{
            CGContextSetAlpha(context, 0.7);
        }
    }
    
    
    CGImageRef imageRef = image.CGImage;
    CGContextDrawImage(context,imageFrame, imageRef);
    
    CGContextRestoreGState(context);
    if (selected) {[self drawSelectionFrame:context];}
    return;
//}
    
    /*
    float alpha=(selected)?1.0:0.3;
    float lineThk=(selected)?6.0/zoomScale:1.0/zoomScale;
    
    
    CGContextBeginPath(context);
    CGContextAddPath(context,path);            
    CGContextClosePath(context);
    
    if ([repProduct isKindOfClass:[Slab class]]){        
        CGContextSetRGBFillColor(context, 0.48, 0.67, 0.75, alpha);             
        
    }else if ( [repProduct isKindOfClass:[Site class]]){  
        CGContextSetRGBFillColor(context, 0.59, 0.8, 0.8, alpha);        
    }else if ( [repProduct isKindOfClass:[Furn class]]){                
        CGContextSetRGBFillColor(context, 1.0,0.0,0.0,0.7);   
    }else{                
        CGContextSetRGBFillColor(context, 234.0/256.0, 234.0/256.0, 234.0/256.0,alpha);   
    }
    
    
    CGContextFillPath(context);     
    CGContextBeginPath(context);
    CGContextAddPath(context,path); 
    
    
    
    
    CGContextClosePath(context);
    CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
    //    CGContextSetRGBFillColor(context, 1.0, 0.0, 0.0, 0.6);       
    CGContextSetLineWidth(context, lineThk);
    
    
    //    CGContextFillPath(context);
    CGContextStrokePath(context);     
    //    CGContextEndTransparencyLayer (context);// 6    
    */
}

- (void) drawSelectionFrame:(CGContextRef) context{
    
//    CGContextSetRGBFillColor(context, 0.48, 0.67, 0.75, 1); 
//    
//    CGContextBeginPath(context);
//    CGContextAddPath(context,path);            
//    CGContextClosePath(context);
//        
//    
//    CGContextFillPath(context);     
//    CGContextBeginPath(context);
//    CGContextAddPath(context,path); 
    
    
    CGContextSetRGBStrokeColor(context, 1, 0, 0, 1);
    CGContextSetLineWidth(context, 20.0);
    CGFloat dash[] = {0.0, 40.0};
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineDash(context, 0.0, dash, 2);
//    CGContextAddPath(context, self.path);
    CGContextAddPath(context, self.bPath.CGPath);
    CGContextClosePath(context);
//    CGContextMoveToPoint(context, 10.0, 30.0);        
//    CGContextAddLineToPoint(context, 310.0, 30.0);
    CGContextStrokePath(context);
    
    
    //    CGContextFillPath(context);
    
    //    CGContextEndTransparencyLayer (context);// 6    
    
    
//    CGMutablePathRef selectedFramePath=CGPathCreateMutable();        
//    CGPoint p0=CGPointMake(0, 0);
//    CGPoint p1=CGPointMake(0, 0);
//    CGPoint p2=CGPointMake(0, 0);
//    CGPoint p3=CGPointMake(0, 0);    
//    CGPathMoveToPoint(selectedFramePath,NULL,p0.x,p0.y);
//    CGPathAddLineToPoint(selectedFramePath,NULL,p1.x,p1.y); 
//    
//    
//    NSMutableArray* aCurveProfile=thisExt.aCurveProfile;
//    if (aCurveProfile==NULL||[aCurveProfile count]<=0) {return self;}
//    
//    for (int pCurveProfile=0;pCurveProfile<[aCurveProfile count];pCurveProfile++){
//        CurveProfile* curveProfile=[aCurveProfile objectAtIndex:pCurveProfile];
//        if (curveProfile==NULL) continue;
//        NSMutableArray* aPoint=curveProfile.aPoint;
//        if (aPoint==NULL||[aPoint count ]<=2) continue;
//        
//        for (int pPoint=0;pPoint < [aPoint count]; pPoint++){                       
//            CPoint* point=[(CPoint*) [aPoint objectAtIndex:(pPoint)]copy ];
//            point.x*=modelScaleFactor;
//            point.y*=modelScaleFactor;
//            
//            
//            //                [boundBBox updatePoint:point];
//            
//            //                                        NSLog(@"x = %f, y = %f", point.x, point.y);
//            
//            
//            if (pPoint==0){                                   
//                //                            CGPathMoveToPoint(_path,NULL,point.x,point.y);                            
//                //                    CGPathMoveToPoint(path,NULL,outsetBuffer+ point.x,outsetBuffer+ point.y);  
//                
//                CGPathMoveToPoint(path,NULL,point.x,point.y);                      
//            }else{                            
//                
//                //                            CGPathAddLineToPoint(_path,NULL,point.x,point.y);
//                //                    CGPathAddLineToPoint(path,NULL,outsetBuffer+ point.x,outsetBuffer+ point.y);
//                CGPathAddLineToPoint(path,NULL,point.x,point.y);                    
//            }       
//            
//            [point release];
//
//        }     
//        
        
    
}


- (void)dealloc
{

//    [imageView release];
    
//    NSLog(@"SpriteBitmap  Dealloc");
//    NSLog(@"Sprite Bitmap Dealloc");
    [image release];image=nil;
    [super dealloc];
}

@end

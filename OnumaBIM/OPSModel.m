//
//  OPSModel.m
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//
#import "OPSProjectSite.h"
#import "OPSModel.h"
#import "ExtrudedAreaSolid.h"
#import "Space.h"
#import "RelAggregate.h"
#import "Floor.h"
#import "Site.h"
#import "Bldg.h"
#import "Slab.h"
#import "Space.h"

#import "Placement.h"
#import "CPoint.h"
#import "ProjectViewAppDelegate.h"
#import "Furn.h"
#import "BitmapRep.h"
#import "SpatialStructure.h"
#import "CustomColorStruct.h"
#import "CustomHeaderStruct.h"
#import "Department.h"
#import "SitePolygon.h"
#import "UtilityLine.h"
//#import "DisplayInfo.h"

static sqlite3 *database = nil;

//static float multiFactor=5.0;
@implementation OPSModel
@synthesize bHasFusionFloorData=_bHasFusionFloorData;
@synthesize bHasFusionSiteData=_bHasFusionSiteData;

@synthesize aGhostFloorSlab;
@synthesize aGhostSpace;

@synthesize root;
@synthesize modelScaleForScreenFactor;
@synthesize canvasScaleForScreenFactor;

@synthesize fitZoom;
@synthesize minimumZoom;
@synthesize maximumZoom;


@synthesize colorMethod;

@synthesize isImperial;
@synthesize measureUnitArea;
@synthesize measureUnitLength;
@synthesize currencyUnit;

//@synthesize currentBIMPlanColorTVC=_currentBIMPlanColorTVC;

- (void)dealloc
{
    [aGhostFloorSlab release];aGhostFloorSlab=nil;
    [aGhostSpace release];aGhostSpace=nil;
    
    [measureUnitArea release];measureUnitArea=nil;
    [measureUnitLength release];measureUnitLength=nil;
    [currencyUnit release];currencyUnit=nil;
    
    
    
    //    NSLog(@"RetainCount1:%d",[root retainCount]);
    //    [root dealloc];
    //    NSLog (@"**************Model Dealloc");
    //    NSLog(@"RetainCount2:%d",[root retainCount]);
    [root release];
    
    //    NSLog(@"RetainCount3:%d",[root retainCount]);
    root=nil;
    //    NSLog(@"RetainCount4:%d",[root retainCount]);    
    //    [self.dbPath release];
    //    [self.project release];
    
    
    
    
    
    //@synthesize dbPath;
    
    [super dealloc];
}


- (void) displayToView: (UIView*)view{

}
-(void) setupModelFromProjectSite: (OPSProjectSite*) projectSite  rootSpatialStruct:(SpatialStructure*) rootSpatialStruct {    

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];  

    if ([projectSite dbPath]==Nil) {return;}
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  
        if ([appDele modelDisplayLevel]==0){
//            multiFactor=10;
//                    multiFactor=1.0;
            [self setupSiteModelFromProjectSite: projectSite];
        }
        if ([appDele modelDisplayLevel]==1){
//            multiFactor=50;         
//                    multiFactor=1.0;
            Bldg* bldg=(Bldg*) rootSpatialStruct;
            [self setupBldgModelFromBldgID: bldg.ID floorIndex:bldg.selectedFloorIndex];
        } 
        
        if ([appDele modelDisplayLevel]==2){            
//            multiFactor=100;                        
//                    multiFactor=1.0;
            Space* space=(Space*)rootSpatialStruct;
            uint rootSpaceID=space.ID;//(space.spaceReferenceID==0)?space.ID:space.spaceReferenceID;
            [self setupSpaceModelFromSpaceID:rootSpaceID];
        }   
    }else{
        sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
    }  
    sqlite3_close(database);
}
/*
-(void) setupSiteNavModelFromProject: (Project*) project {
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];        
    Site* site=[[appDele model] root];
    
    RelAggregate* relAgg=[site relAggregate];    
    Bldg* bldg=[[relAgg related] objectAtIndex:(indexPath.row)];
    cell.textLabel.text=bldg.name;            
} 
*/




-(void) setupSpaceWithFurnInside:(NSInteger) spaceID isGhostSpace:(bool) isGhostSpace{// actionParamArray:(NSArray*) paramArray{
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    Space* space=NULL;
    sqlite3_stmt *spaceStmt;
    
    NSString* customColorStr=@"";
    for (uint pCustomColorStr=1;pCustomColorStr<=30;pCustomColorStr++){
        if (pCustomColorStr==30){
            customColorStr=[NSString stringWithFormat:@"%@ spaceinfo.customInt%d",customColorStr,pCustomColorStr];
        }else{
            customColorStr=[NSString stringWithFormat:@"%@ spaceinfo.customInt%d,",customColorStr,pCustomColorStr];
        }
    }
    
    
    const char* spaceSql=[[NSString stringWithFormat:@"select space.polylineID, space.spatialStructureID,  spatialStructure.placementID ,spatialStructure.name, spatialStructure.GUID, placement.x , placement.y ,placement.z, placement.angle, space.ID, spaceinfo.centerPointOffsetX, spaceinfo.centerPointOffsetY, spaceinfo.referenceID, referenceSpace.polylineID as referencePolylineID, referenceSpace.spatialStructureID as referenceSpatialStructureID, space.polylineStr, space.arcStr,referenceSpace.polylineStr as referencePolylineStr, spaceinfo.deptID, spaceinfo.alignID, spaceinfo.alignColor,%@, space.spaceNumber, spaceinfo.spaceArea from space LEFT JOIN spatialstructure on space.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID LEFT JOIN spaceinfo ON spaceinfo.spaceID=space.ID left join spaceinfo as referenceSpaceInfo on spaceinfo.referenceID=referenceSpaceInfo.spaceID left join space as referenceSpace on spaceinfo.referenceID=referenceSpace.ID where space.ID=%d",customColorStr, spaceID] UTF8String];
    
    
    
    
    
    
    if(sqlite3_prepare_v2(database, spaceSql, -1, &spaceStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(spaceStmt) == SQLITE_ROW) {
//            
//            CGAffineTransform transform=isGhostSpace?appDele. rootSpaceInSpacePagePlacementTransformInverse:CGAffineTransformIdentity;

            
            
            space=[[Space alloc] initWithSpaceSQLStmt:self database:database sqlStmt:spaceStmt parentTransform:CGAffineTransformIdentity];
            
            
//            space=[[Space alloc] initWithSpaceSQLStmt:self database:database sqlStmt:spaceStmt parentTransform:transform];// CGAffineTransformIdentity];
            CGAffineTransform spaceTransform=[space.placement toCGAffineTransform];
           
            
            
            uint spaceSpatialStructIDForUse=space.spaceReferenceID==0?space.spatialStructureID:space.spaceReferenceSpatialStructID;
            
            
            if (!isGhostSpace){
            
                
                //******************Equipment******************************
                
                
                
                
                Furn* furn=NULL;
                //2
                const char* furnSQL=[[NSString stringWithFormat:@"select Equipment.ID as equipmentID, Element.GUID, Element.name, placement.x, placement.y, placement.z, placement.angle, FurnData.imageName,FurnData.insertPoint,FurnData.fixedSize, Equipment.mirrorY, Equipment.dimensionX,  Equipment.dimensionY, Equipment.dimensionZ, Equipment.componentName, element.centerPointOffsetX,element.centerPointOffsetY FROM Equipment LEFT JOIN Element ON Equipment.ElementID = Element.ID LEFT JOIN Placement ON Element.placementID = Placement.ID LEFT JOIN FurnData ON Equipment.furnName=FurnData.furnName where spatialStructureID=%d",spaceSpatialStructIDForUse] UTF8String];
                
                
                
                
                
                sqlite3_stmt *furnStmt;
                if(sqlite3_prepare_v2(database, furnSQL, -1, &furnStmt, NULL) == SQLITE_OK) {
                    while(sqlite3_step(furnStmt) == SQLITE_ROW) {
                        
                        furn=[[Furn alloc]initWithFurnSQLStmt_mustSetParentTransformLater:self database:database sqlStmt:furnStmt isGhost:(isGhostSpace)];
                        
                        [furn setParentTransformAndSetupRepresentation:spaceTransform];
                        //                    furn=[[Furn alloc]initWithFurnSQLStmt:self database:database sqlStmt:furnStmt parentTransform:spaceTransform];
                        if (isGhostSpace){
                            furn.isGhostObject=true;
                        }
                        [space addElement:furn];
                        [furn release];
                        
                    }
                    
                }
                
                sqlite3_finalize(furnStmt);
                //!-----------------Equipment------------------------------
            }
            
            
            
            if (isGhostSpace){
                space.isGhostObject=true;
                if ([self aGhostSpace]==nil){
                    NSMutableArray* tmpAGhostSpace=[[NSMutableArray alloc]init];
                    self.aGhostSpace=tmpAGhostSpace;
                    tmpAGhostSpace=nil;
                }
                [[self aGhostSpace] addObject:space];
            }else{
                self.root=space;
                appDele.rootSpaceInSpacePagePlacementTransformInverse=CGAffineTransformInvert(spaceTransform);
                [appDele setCurrentSpace:(Space*) self.root];
            }
            [space release];
            
        }
    }
    
    sqlite3_finalize(spaceStmt);
}

-(void) setupSpaceModelFromSpaceID: (NSInteger) spaceID{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele setActiveModel:self];
    //Must setup the root space first before setting up ghost space
    [self setupSpaceWithFurnInside:spaceID isGhostSpace:false];
    

    for (NSNumber* pSpaceID in [appDele aGhostSpaceID]){
        NSInteger spaceID=[pSpaceID integerValue];
        [self setupSpaceWithFurnInside:spaceID isGhostSpace:true];
    }
}


-(void) setupBldgModelFromBldgID:(NSInteger) bldgID  floorIndex:(NSInteger) floorIndex{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele setActiveModel:self];
    OPSProjectSite* projectSite=[appDele activeProjectSite];
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
        if ([appDele modelDisplayLevel]==1){
    
    
        self.bHasFusionFloorData=false;
        const char* hasFusionDataSQL=[[NSString stringWithFormat:@"select * from FUSION_SpaceInfo"] UTF8String];
        sqlite3_stmt *hasFusionDataStmt;
        if(sqlite3_prepare_v2(database, hasFusionDataSQL, -1, &hasFusionDataStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(hasFusionDataStmt) == SQLITE_ROW) {
                self.bHasFusionFloorData=true;
                break;
            }
            
            
        
        }
        sqlite3_finalize(hasFusionDataStmt);
        
    }else{
        sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
    }
    sqlite3_close(database);
}

    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
        if ([appDele modelDisplayLevel]==1){
        
            Bldg* bldg=NULL;
            sqlite3_stmt *bldgStmt;       

            
            
                

        //    const char* bldgSql=[[NSString stringWithFormat:@"select bldg.ID, bldg.spatialStructureID,  spatialStructure.placementID ,spatialStructure.name, spatialStructure.GUID, placement.x , placement.y ,placement.z, placement.angle, bldginfo.firstFloor   from bldg LEFT JOIN spatialstructure on bldg.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID left join bldginfo on bldg.ID=bldginfo.bldgID   where bldg.ID=%d", bldgID] UTF8String];
                
            NSString* customColorStr=@"";
            for (uint pCustomColorStr=1;pCustomColorStr<=30;pCustomColorStr++){
                if (pCustomColorStr==30){
                    customColorStr=[NSString stringWithFormat:@"%@ bldginfo.customInt%d",customColorStr,pCustomColorStr];            
                }else{
                    customColorStr=[NSString stringWithFormat:@"%@ bldginfo.customInt%d,",customColorStr,pCustomColorStr];
                }
            }
            
            const char* bldgSql=[[NSString stringWithFormat:@"select bldg.ID, bldg.spatialStructureID,  spatialStructure.placementID ,spatialStructure.name, spatialStructure.GUID, placement.x , placement.y ,placement.z, placement.angle, bldginfo.firstFloor,bldginfo.centerPointOffsetX, bldginfo.centerPointOffsetY, bldginfo.referenceID, uscg_bldginfo.existing, %@ from bldg LEFT JOIN spatialstructure on bldg.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID left join bldginfo on bldg.ID=bldginfo.bldgID left join uscg_bldginfo on uscg_bldginfo.bldgID=bldg.ID  where bldg.ID=%d",customColorStr, bldgID] UTF8String];
                                  
        
            if(sqlite3_prepare_v2(database, bldgSql, -1, &bldgStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(bldgStmt) == SQLITE_ROW) {       
                    
                    bldg=[[Bldg alloc] initWithBldgSQLStmt:self database:database sqlStmt:bldgStmt parentTransform:CGAffineTransformIdentity];


                    Placement* bldgPlacement=[[Placement alloc] init:0 guid:nil name:nil pt:CGPointMake(0.0, 0.0) angle:0.0  isMirrorY:false];
                    [bldg setPlacement:bldgPlacement];
                    [bldgPlacement release];
                    
                    CGAffineTransform bldgTransform= [bldg.placement toCGAffineTransform];
        //            NSLog ("Bldg Root Retain 1:%d",[self.root retainCount]);
                    
        //            NSLog ("Bldg Root Retain 2:%d",[bldg retainCount]);            
                    
                    
                    [bldg setSelectedFloorIndex:floorIndex];
                    self.root=bldg;
                    [bldg release];
                            
                    Floor* floor=NULL;        
                    int pFloor=0;
                    //2
                    uint bldgIDForFloor=bldg.bldgReferenceID==0?bldg.ID:bldg.bldgReferenceID;
                    
                    const char* floorSql=[[NSString stringWithFormat:@"select floor.ID, floor.spatialStructureID, spatialstructure.name, spatialstructure.GUID , placement.x , placement.y ,placement.z, placement.angle  from floor LEFT JOIN spatialstructure on floor.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID left join floorinfo on floorinfo.floorID=floor.ID where bldgID=%d order by placement.z, floorinfo.floorHeight!=0", bldgIDForFloor] UTF8String];
                    
                    sqlite3_stmt *floorStmt;
                    if(sqlite3_prepare_v2(database, floorSql, -1, &floorStmt, NULL) == SQLITE_OK) {
                        while(sqlite3_step(floorStmt) == SQLITE_ROW) {       

                            
                            floor=[[Floor alloc] initWithFloorSQLStmt:self database:database sqlStmt:floorStmt parentTransform:bldgTransform bldgFirstLevelIndex:bldg.firstFloorIndex currentIndex:pFloor];

                            
        //                    
                            
                            
        //                    [floor.placement release];
        //                    Placement* floorPlacement=[[Placement alloc] init:nil guid:nil name:nil pt:CGPointMake(0.0, 0.0) angle:0.0 ];
        //                    floor.placement=floorPlacement;                    
                            
                             
                            CGAffineTransform floorTransform=CGAffineTransformConcat([floor.placement toCGAffineTransform], bldgTransform);
                            
                            if (pFloor!=((Bldg*) self.root).selectedFloorIndex){                        
                                [((Bldg*) self.root) addSpatialStructure:floor]; 
                                [floor release];
                                pFloor++;
                                continue;
                            }
                                           
                            
                            
                            //                               [bldg addSpatialStructure:floor];
                            
                            
                            
                            
                            
                            //                        const char* slabSql=[[NSString stringWithFormat:@"select slab.ID, element.polylineID from slab LEFT JOIN element on slab.elementID=element.ID  where element.spatialStructureID=%d", floorSpatialStructureID] UTF8String];
                            
                            
                            
                            /*
                            const char* furnOnFloorSQL=[[NSString stringWithFormat:@"select Equipment.ID as equipmentID, Element.GUID, Element.name, placement.x, placement.y, placement.z, placement.angle, FurnData.imageName,FurnData.insertPoint,FurnData.fixedSize, Equipment.mirrorY, Equipment.dimensionX,  Equipment.dimensionY, Equipment.dimensionZ FROM Equipment LEFT JOIN Element ON Equipment.ElementID = Element.ID LEFT JOIN Placement ON Element.placementID = Placement.ID LEFT JOIN FurnData ON Equipment.furnName=FurnData.furnName LEFT JOIN Space ON Element.spatialStructureID=Space.spatialStructureID where floorID=%d",floor.ID] UTF8String];
                            
                            */
                            
                             const char* furnOnFloorSQL=[[NSString stringWithFormat:@"select Equipment.ID as equipmentID, Element.GUID, Element.name, placement.x, placement.y, placement.z, placement.angle,FurnData.imageName,FurnData.insertPoint,FurnData.fixedSize, Equipment.mirrorY, Equipment.dimensionX,  Equipment.dimensionY, Equipment.dimensionZ, Equipment.componentName, element.centerPointOffsetX,element.centerPointOffsetY FROM Space LEFT JOIN SpaceInfo ON Space.ID=SpaceInfo.spaceID LEFT JOIN Space AS Space2 ON SpaceInfo.referenceID=Space2.ID JOIN Element ON (((SpaceInfo.referenceID==0) AND (Element.spatialStructureID=Space.spatialStructureID)) OR (Element.spatialStructureID=Space2.spatialStructureID)) LEFT JOIN Equipment ON Equipment.ElementID = Element.ID LEFT JOIN Placement ON Element.placementID = Placement.ID LEFT JOIN FurnData ON Equipment.furnName=FurnData.furnName WHERE Equipment.ID IS NOT NULL AND Space.floorID=%d",floor.ID] UTF8String];
                             
                            
                            
                            sqlite3_stmt *furnOnFloorStmt;

                            
         
                            NSMutableDictionary *furnOnFloorDict = [[NSMutableDictionary alloc] init];

                            if(sqlite3_prepare_v2(database, furnOnFloorSQL, -1, &furnOnFloorStmt, NULL) == SQLITE_OK) {
                                while(sqlite3_step(furnOnFloorStmt) == SQLITE_ROW) {                                                         
                                    Furn* furn=[[Furn alloc] initWithFurnSQLStmt_mustSetParentTransformLater:self database:database sqlStmt:furnOnFloorStmt isGhost:false];
        //                            NSLog (@"FurnID: %d",furn.ID);
                                    [furnOnFloorDict setObject:furn forKey:[NSString stringWithFormat:@"%d", furn.ID]];
                                    [furn release];                        
                                }
                                
                            }    
                            
                            sqlite3_finalize(furnOnFloorStmt); 
        //                    NSDictionary *furnOnFloorDict = [[NSDictionary alloc] initWithObjects:value_aFurn forKeys:key_aFurnID];                    
                            
           
                            sqlite3_stmt *spaceStmt;       

                                                
        //                    const char* spaceSql=[[NSString stringWithFormat:@"select space.polylineID, space.spatialStructureID,  spatialStructure.placementID ,spatialStructure.name, spatialStructure.GUID, placement.x , placement.y ,placement.z, placement.angle,  space.ID, spaceinfo.centerPointOffsetX, spaceinfo.centerPointOffsetY, spaceinfo.referenceID, referenceSpace.polylineID as referencePolylineID, referenceSpace.spatialStructureID as referenceSpatialStructureID, space.polylineStr,referenceSpace.polylineStr as referencePolylineStr from space LEFT JOIN spatialstructure on space.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID LEFT JOIN spaceinfo ON spaceinfo.spaceID=space.ID left join spaceinfo as referenceSpaceInfo on spaceinfo.referenceID=referenceSpaceInfo.spaceID left join space as referenceSpace on spaceinfo.referenceID=referenceSpace.ID where space.floorID=%d", floor.ID] UTF8String];                     
                            NSString* customColorStr=@"";
                            for (uint pCustomColorStr=1;pCustomColorStr<=30;pCustomColorStr++){
                                if (pCustomColorStr==30){
                                    customColorStr=[NSString stringWithFormat:@"%@ spaceinfo.customInt%d",customColorStr,pCustomColorStr];            
                                }else{
                                    customColorStr=[NSString stringWithFormat:@"%@ spaceinfo.customInt%d,",customColorStr,pCustomColorStr];
                                }
                            }

                            NSString* spaceSqlStr=[NSString stringWithFormat:@"select space.polylineID, space.spatialStructureID,  spatialStructure.placementID ,spatialStructure.name, spatialStructure.GUID, placement.x , placement.y ,placement.z, placement.angle,  space.ID, spaceinfo.centerPointOffsetX, spaceinfo.centerPointOffsetY, spaceinfo.referenceID, referenceSpace.polylineID as referencePolylineID, referenceSpace.spatialStructureID as referenceSpatialStructureID, space.polylineStr, space.arcStr,referenceSpace.polylineStr as referencePolylineStr, spatialStructure.equipmentIDStr, referenceSpatialStructure.equipmentIDStr, spaceinfo.deptID, spaceinfo.alignID, spaceinfo.alignColor,%@, space.spaceNumber, spaceinfo.spaceArea, FUSION_SpaceInfo.roomPathID,FUSION_SpaceInfo.assignedSF,SpaceInfo.ignoreFusionSpaceNumber,SpaceInfo.roomPathID,SpaceInfo.newAdded,FUSION_SpaceInfo.roomStatus,FUSION_SpaceInfo.topCssCode,TopColor.description, TopColor.color,FUSION_SpaceInfo.pgmNum,PGMColor.description, PGMColor.color, FUSION_SpaceInfo.roomUseCode, RoomUseColor.description, RoomUseColor.color from space LEFT JOIN spatialstructure on space.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID LEFT JOIN spaceinfo ON spaceinfo.spaceID=space.ID left join spaceinfo as referenceSpaceInfo on spaceinfo.referenceID=referenceSpaceInfo.spaceID left join space as referenceSpace on spaceinfo.referenceID=referenceSpace.ID LEFT JOIN spatialstructure as referenceSpatialStructure on referenceSpace.spatialStructureID=referenceSpatialStructure.ID LEFT JOIN FUSION_SpaceInfo on space.ID=FUSION_SpaceInfo.spaceID LEFT JOIN TopColor on TopColor.value=FUSION_SpaceInfo.topCssCode LEFT JOIN PGMColor on PGMColor.value=FUSION_SpaceInfo.pgmNum LEFT JOIN RoomUseColor on RoomUseColor.value=FUSION_SpaceInfo.roomUseCode where space.floorID=%d", customColorStr,floor.ID];
                            NSLog(@"%@",spaceSqlStr);
                            const char* spaceSql=[spaceSqlStr  UTF8String];
                            
                            
                            
                            
                            
                            /*
                            const char* spaceSql=[[NSString stringWithFormat:@"select Equipment.ID as equipmentID, Element.GUID," @"Element.name, placement.x, placement.y, placement.z, placement.angle,"
                            @"FurnData.imageName,FurnData.insertPoint,FurnData.fixedSize, Equipment.mirrorY, Equipment.dimensionX,  Equipment.dimensionY, Equipment.dimensionZ"
                            @"FROM Space LEFT JOIN SpaceInfo ON Space.ID=SpaceInfo.spaceID"
                            @"LEFT JOIN Space AS Space2 ON SpaceInfo.referenceID=Space2.ID"
                            @"JOIN Element ON"
                            @"(((SpaceInfo.referenceID==0) AND (Element.spatialStructureID=Space.spatialStructureID)) OR (Element.spatialStructureID=Space2.spatialStructureID))"
                            @"LEFT JOIN Equipment ON Equipment.ElementID = Element.ID"
                            @"LEFT JOIN Placement ON Element.placementID = Placement.ID"
                            @"LEFT JOIN FurnData ON Equipment.furnName=FurnData.furnName"
                                                   @"WHERE Equipment.ID IS NOT NULL AND Space.floorID=%d",floor.ID] UTF8String]; 
                            */
                            
                            /*
                            const char* spaceSql=[[NSString stringWithFormat:@"select Equipment.ID as equipmentID, Element.GUID, Element.name, placement.x, placement.y, placement.z, placement.angle,FurnData.imageName,FurnData.insertPoint,FurnData.fixedSize, Equipment.mirrorY, Equipment.dimensionX,  Equipment.dimensionY, Equipment.dimensionZ FROM Space LEFT JOIN SpaceInfo ON Space.ID=SpaceInfo.spaceID LEFT JOIN Space AS Space2 ON SpaceInfo.referenceID=Space2.ID JOIN Element ON (((SpaceInfo.referenceID==0) AND (Element.spatialStructureID=Space.spatialStructureID)) OR (Element.spatialStructureID=Space2.spatialStructureID)) LEFT JOIN Equipment ON Equipment.ElementID = Element.ID LEFT JOIN Placement ON Element.placementID = Placement.ID LEFT JOIN FurnData ON Equipment.furnName=FurnData.furnName WHERE Equipment.ID IS NOT NULL AND Space.floorID=%d",floor.ID] UTF8String];
                            */
                            
                            
                            if(sqlite3_prepare_v2(database, spaceSql, -1, &spaceStmt, NULL) == SQLITE_OK) {
                                while(sqlite3_step(spaceStmt) == SQLITE_ROW) {       
                                      
                                    Space* space=[[Space alloc] initWithSpaceSQLStmt:self database:database sqlStmt:spaceStmt parentTransform:floorTransform furnDict:furnOnFloorDict];

                                    

        //                            uint spaceSpatialStructIDForUse=space.spaceReferenceID==0?space.spatialStructureID:space.spaceReferenceSpatialStructID;
        //                            CGAffineTransform spaceTransform=CGAffineTransformConcat([space.placement toCGAffineTransform], floorTransform);
                                    //                                        [slab updateBBox];        
                            
                                   
                                    //******************Equipment******************************
                                    

        /*
                                    
                                    Furn* furn=NULL;
                                    //3
        //                                        const char* furnSQL=[[NSString stringWithFormat:@"select Equipment.*, Element.*, Polyline.*, Placement.*,Equipment.ID AS equipmentID, FurnData.imageName FROM Equipment LEFT JOIN Element ON Equipment.ElementID = Element.ID LEFT JOIN Polyline ON Element.polylineID=Polyline.ID LEFT JOIN Placement ON Element.placementID = Placement.ID LEFT JOIN FurnData ON Equipment.furnName=FurnData.furnName where spatialStructureID=%d",spaceSpatialStructID] UTF8String];
                                    
                                    
                                    
                                    const char* furnSQL=[[NSString stringWithFormat:@"select Equipment.ID as equipmentID, Element.GUID, Element.name, placement.x, placement.y, placement.z, placement.angle, FurnData.imageName,FurnData.insertPoint,FurnData.fixedSize, Equipment.mirrorY, Equipment.dimensionX,  Equipment.dimensionY, Equipment.dimensionZ  FROM Equipment LEFT JOIN Element ON Equipment.ElementID = Element.ID LEFT JOIN Placement ON Element.placementID = Placement.ID LEFT JOIN FurnData ON Equipment.furnName=FurnData.furnName where spatialStructureID=%d",spaceSpatialStructIDForUse] UTF8String];            
                                    
        //                            const char* furnSQL=[[NSString stringWithFormat:@"select Equipment.ID as equipmentID, Element.GUID, Element.name, placement.x, placement.y, placement.z, placement.angle, FurnData.imageName,FurnData.insertPoint,FurnData.fixedSize, Equipment.mirrorY, Equipment.dimensionX,  Equipment.dimensionY, Equipment.dimensionZ, Equipment.centerPointOffsetX, Equipment.centerPointOffsetY  FROM Equipment LEFT JOIN Element ON Equipment.ElementID = Element.ID LEFT JOIN Placement ON Element.placementID = Placement.ID LEFT JOIN FurnData ON Equipment.furnName=FurnData.furnName where spatialStructureID=%d",space.spatialStructureID] UTF8String];            
                                    
                                    
                                    NSLog(@"SPACENAME: %@",space.name);

                                    
                                    sqlite3_stmt *furnStmt;
                                    if(sqlite3_prepare_v2(database, furnSQL, -1, &furnStmt, NULL) == SQLITE_OK) {
                                        while(sqlite3_step(furnStmt) == SQLITE_ROW) {       

                                            furn=[[Furn alloc]initWithFurnSQLStmt:self database:database sqlStmt:furnStmt parentTransform:spaceTransform];
                                            //                                        [slab updateBBox];        
                                            
                                            
                                            //                                        NSLog (@"slab  placement : %f, %f", slab.placement.pt.x,slab.placement.pt.y);                                        
                                            //                                        NSLog (@"slab bbox minX%f minY%f maxX%f maxY%f width%f height%f",[slab.bBox minX],[slab.bBox minY],[slab.bBox maxX],[slab.bBox maxY],[slab.bBox getWidth],[slab.bBox getHeight]);
                                            
                                            
                                            [space addElement:furn];
                                            [furn release];
                                            
                                            //                                        NSLog (@"floor bbox minX%f minY%f maxX%f maxY%f width%f height%f",[floor.bBox minX],[floor.bBox minY],[floor.bBox maxX],[floor.bBox maxY],[floor.bBox getWidth],[floor.bBox getHeight]);
                                            
                                            
                                            
        s
                                            
                                            
                                        }
                                        
                                    } 
         */
                                    //!-----------------Equipment------------------------------
                                    
                                    
                                    
                                    
                                    [floor addSpatialStructure:space];
                                    
        //                            NSLog (@"Finished Loading Space ID: %d Name: %@",space.ID, space.name  );
                                    [space release];
                                    

                                    
                                }
                            }
                            
                            sqlite3_finalize(spaceStmt);
                            
                            
                            
        //                    NSLog (@"Finished Loading Floor Spacing");
                            
                            
                            
                            Slab* slab=NULL;
                            
        //                    
        //                    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        //                    Site* site=[appDele currentSite];
                            const char* slabSql=[[NSString stringWithFormat:@"select slab.ID, element.polylineID, element.GUID, slab.slabThk, placement.x , placement.y ,placement.z ,placement.angle, element.polylineStr,element.arcStr from slab LEFT JOIN element on slab.elementID=element.ID LEFT JOIN polyline on element.polylineID=polyline.ID LEFT JOIN placement on element.placementID=placement.ID where element.spatialStructureID=%d", floor.spatialStructureID ] UTF8String];
                            sqlite3_stmt *slabStmt;
                            if(sqlite3_prepare_v2(database, slabSql, -1, &slabStmt, NULL) == SQLITE_OK) {
                                while(sqlite3_step(slabStmt) == SQLITE_ROW) {                                   
                                    slab=[[Slab alloc] initWithSlabSQLStmt:self database:database sqlStmt:slabStmt parentTransform:floorTransform];                                                       
                                    [floor addElement:slab];
                                    [slab release];
                                    
                                    //                                        NSLog (@"floor bbox minX%f minY%f maxX%f maxY%f width%f height%f",[floor.bBox minX],[floor.bBox minY],[floor.bBox maxX],[floor.bBox maxY],[floor.bBox getWidth],[floor.bBox getHeight]);
                                    
                                    
                                    
                                    
                                    
                                    
                                }
                                
                            }  
                            
                            sqlite3_finalize(slabStmt);
                            [((Bldg*) self.root) addSpatialStructure:floor];                                        
                            [furnOnFloorDict release];
        //                    NSLog (@"Finished Loading Floor: %@",floor.name);
                            [floor release];
                            //                                NSLog (@"bldg  placement : %f, %f", bldg.placement.pt.x,bldg.placement.pt.y);
                            //                                NSLog (@"bldg bbox minX%f minY%f maxX%f maxY%f width%f height%f",[bldg.bBox minX],[bldg.bBox minY],[bldg.bBox maxX],[bldg.bBox maxY],[bldg.bBox getWidth],[bldg.bBox getHeight]);
                            
                            
                            pFloor++;
        //                    break;        // ***Important: Should break to avoid loading all floors data into one bldg in one model                                                                                                  
                            //                        NSLog (@"site bbox minX%f minY%f maxX%f maxY%f width%f height%f",[site.bBox minX],[site.bBox minY],[site.bBox maxX],[site.bBox maxY],[site.bBox getWidth],[site.bBox getHeight]);
                            
                        }
                        
                    }
                    
                    
                    sqlite3_finalize(floorStmt);
                    
                }
            }
            sqlite3_finalize(bldgStmt);
            
        }else{
            sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
        }
        sqlite3_close(database);
    }
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele setCurrentBldg:(Bldg*) self.root];
    
}
-(void) setupSiteModelFromProjectSite: (OPSProjectSite*) projectSite{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele setActiveModel:self];
//    int siteID=[project ID];
    Site* site=NULL;    
    
    
    
    
    
    self.bHasFusionSiteData=false;
    const char* hasFusionDataSQL=[[NSString stringWithFormat:@"select * from FUSION_BldgInfo"] UTF8String];
    sqlite3_stmt *hasFusionDataStmt;
    if(sqlite3_prepare_v2(database, hasFusionDataSQL, -1, &hasFusionDataStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(hasFusionDataStmt) == SQLITE_ROW) {
            self.bHasFusionSiteData=true;
            break;
        }
        
        
    }
    sqlite3_finalize(hasFusionDataStmt);

    
    sqlite3_stmt *siteStmt;
    //Skip the site.ID=%d because the siteID will not be saved to the localDatabase , instead it is assumed there must always be one and onlye one site stored in the database
    //        const char* siteSql=[[NSString stringWithFormat:@"select site.polylineID, site.spatialStructureID,  spatialStructure.placementID ,spatialStructure.name, spatialStructure.GUID, placement.x , placement.y ,placement.z, placement.angle   from site LEFT JOIN spatialstructure on site.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID  where site.ID=%d", siteID] UTF8String];
    
//    const char* siteSql=[[NSString stringWithFormat:@"select site.polylineID, site.spatialStructureID,  spatialStructure.placementID ,spatialStructure.name, spatialStructure.GUID, siteInfo.hasWorldCoord, site.latitude, site.longitude, placement.x , placement.y ,placement.z, placement.angle,  site.ID, site.polylineStr from site LEFT JOIN spatialstructure on site.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID"] UTF8String];
//  
    
//    const char* siteSql=[[NSString stringWithFormat:@"select site.polylineID, site.spatialStructureID,  spatialStructure.placementID ,spatialStructure.name, spatialStructure.GUID, siteInfo.hasWorldCoord, site.latitude, site.longitude, placement.x , placement.y ,placement.z, placement.angle,  site.ID, site.polylineStr,site.shareView,site.shareEdit from site LEFT JOIN spatialstructure on site.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID left join siteinfo on siteID=site.ID"] UTF8String];
 
        const char* siteSql=[[NSString stringWithFormat:@"select site.polylineID, site.spatialStructureID,  spatialStructure.placementID ,spatialStructure.name, spatialStructure.GUID, siteInfo.hasWorldCoord, site.latitude, site.longitude, placement.x , placement.y ,placement.z, placement.angle,  site.ID, site.polylineStr, site.arcStr,site.shareView,site.shareEdit, deptname.deptName1,deptname.color1, deptname.deptName2,deptname.color2, deptname.deptName3,deptname.color3, deptname.deptName4,deptname.color4, deptname.deptName5,deptname.color5, deptname.deptName6,deptname.color6, deptname.deptName7,deptname.color7, deptname.deptName8,deptname.color8, deptname.deptName9,deptname.color9, deptname.deptName10,deptname.color10,siteInfo.currency, siteInfo.unit,spatialstructure.dateModified from site LEFT JOIN spatialstructure on site.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID left join siteinfo on siteinfo.siteID=site.ID left join deptname on deptname.siteID=site.ID"] UTF8String];
//    
//    const char* siteSql=[[NSString stringWithFormat:@"select site.polylineID, site.spatialStructureID,  spatialStructure.placementID ,spatialStructure.name, spatialStructure.GUID, siteInfo.hasWorldCoord, site.latitude, site.longitude, placement.x , placement.y ,placement.z, placement.angle,  site.ID, site.polylineStr,site.shareView,site.shareEdit, deptname.deptName1 from site LEFT JOIN spatialstructure on site.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID left join siteinfo on siteID=site.ID left join deptname on siteID=site.ID"] UTF8String];
    
    
    
    if(sqlite3_prepare_v2(database, siteSql, -1, &siteStmt, NULL) == SQLITE_OK) {
        while( sqlite3_step(siteStmt) == SQLITE_ROW) {       

            site=[[Site alloc] initWithSiteSQLStmt:[projectSite ID] model:self database:database sqlStmt:siteStmt parentTransform:CGAffineTransformIdentity];
            
            CGAffineTransform siteTransform=[site.placement toCGAffineTransform];                        
            self.root=site;//[site retain];
            [site release];
            
            
            
            
            
            
//            const char* bldgSql=[[NSString stringWithFormat:@"select bldg.ID, bldg.spatialStructureID,  spatialstructure.placementID ,spatialstructure.name, spatialstructure.GUID, placement.x , placement.y ,placement.z ,placement.angle, bldginfo.firstFloor  from bldg LEFT JOIN spatialstructure on bldg.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID left join bldginfo on bldg.ID=bldginfo.bldgID where siteID=%d", ((Site*) self.root).ID ] UTF8String];  
            
            
            
            
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
            
            

            
            
            //                        const char* slabSql=[[NSString stringWithFormat:@"select slab.ID, element.polylineID from slab LEFT JOIN element on slab.elementID=element.ID  where element.spatialStructureID=%d", floorSpatialStructureID] UTF8String];
            
            
            SitePolygon* sitePolygon=NULL;            
            const char* sitePolygonSql=[[NSString stringWithFormat:@"select sitepolygon.ID, sitepolygon.elementID, element.polylineID, element.name, placement.x , placement.y ,placement.z ,placement.angle, element.polylineStr,element.arcStr, sitepolygon.type, sitepolygon.custom1,element.centerPointOffsetX, element.centerPointOffsetY from sitepolygon LEFT JOIN element on sitepolygon.elementID=element.ID LEFT JOIN polyline on element.polylineID=polyline.ID LEFT JOIN placement on element.placementID=placement.ID"] UTF8String];
            sqlite3_stmt *sitePolygonStmt;
            if(sqlite3_prepare_v2(database, sitePolygonSql, -1, &sitePolygonStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(sitePolygonStmt) == SQLITE_ROW) {
                    sitePolygon=[[SitePolygon alloc] initWithSitePolygonSQLStmt:self database:database sqlStmt:sitePolygonStmt parentTransform:siteTransform];
                    [site addElement:sitePolygon];
                    [sitePolygon release];
                    
                    //                                        NSLog (@"floor bbox minX%f minY%f maxX%f maxY%f width%f height%f",[floor.bBox minX],[floor.bBox minY],[floor.bBox maxX],[floor.bBox maxY],[floor.bBox getWidth],[floor.bBox getHeight]);
                    
                    
//                            bldg=[[Bldg alloc] initWithBldgSQLStmt:self database:database sqlStmt:bldgStmt parentTransform:siteTransform];
//                            
//                            
//                            CGAffineTransform bldgPlacement=[bldg.placement toCGAffineTransform];
//                            CGAffineTransform bldgTransform=CGAffineTransformConcat(bldgPlacement, siteTransform);
                    
                    
                }
                
                
            }
            
            
            
//                    sqlite3_finalize(sitePolygonStmt);
//                    [bldg addSpatialStructure:floor];
//                    [floor release];
    
            
            //                                NSLog (@"bldg  placement : %f, %f", bldg.placement.pt.x,bldg.placement.pt.y);
            //                                NSLog (@"bldg bbox minX%f minY%f maxX%f maxY%f width%f height%f",[bldg.bBox minX],[bldg.bBox minY],[bldg.bBox maxX],[bldg.bBox maxY],[bldg.bBox getWidth],[bldg.bBox getHeight]);
            
            
            
            sqlite3_finalize(sitePolygonStmt);
//            
//            [((Site*) self.root) addSpatialStructure:bldg];
//            [bldg release];
            

            
            
            
            //--------------------------------
                        
  
            
            NSString* customColorStr=@"";
            for (uint pCustomColorStr=1;pCustomColorStr<=30;pCustomColorStr++){
                if (pCustomColorStr==30){
                    customColorStr=[NSString stringWithFormat:@"%@ bldginfo.customInt%d",customColorStr,pCustomColorStr];
                }else{
                    customColorStr=[NSString stringWithFormat:@"%@ bldginfo.customInt%d,",customColorStr,pCustomColorStr];
                }
            }
            
            NSString* bldgStr=[NSString stringWithFormat:@"select bldg.ID, bldg.spatialStructureID,  spatialstructure.placementID ,spatialstructure.name, spatialstructure.GUID, placement.x , placement.y ,placement.z ,placement.angle, bldginfo.firstFloor,bldginfo.centerPointOffsetX, bldginfo.centerPointOffsetY,bldginfo.referenceID, uscg_bldginfo.existing, FUSION_BldgInfo.bldgStatus, FUSION_BldgInfo.fci,BldgInfo.bldgPathID,FUSION_BldgInfo.bldgPathID,BldgInfo.newAdded, %@ from bldg LEFT JOIN spatialstructure on bldg.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID left join bldginfo on bldg.ID=bldginfo.bldgID left join uscg_bldginfo on USCG_BldgInfo.bldgID=bldg.ID left join FUSION_BldgInfo on bldg.ID=FUSION_BldgInfo.bldgID where siteID=%d",customColorStr, ((Site*) self.root).ID ] ;
            
            NSLog(@"%@",bldgStr);
            
            const char* bldgSql=[bldgStr  UTF8String];
            Bldg* bldg=NULL;
            sqlite3_stmt *bldgStmt;
            if(sqlite3_prepare_v2(database, bldgSql, -1, &bldgStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(bldgStmt) == SQLITE_ROW) {       
                                        
                    

                    bldg=[[Bldg alloc] initWithBldgSQLStmt:self database:database sqlStmt:bldgStmt parentTransform:siteTransform];
                    
                    
                    CGAffineTransform bldgPlacement=[bldg.placement toCGAffineTransform];
                    CGAffineTransform bldgTransform=CGAffineTransformConcat(bldgPlacement, siteTransform);
//                    [bldg setSelectedFloorIndex:0];

                    
                    
                    //  ~                      [site addSpatialStructure:bldg];
                    int pFloor=0;
                    Floor* floor=NULL;
                    
                    uint bldgIDForFloor=bldg.bldgReferenceID==0?bldg.ID:bldg.bldgReferenceID;
                    const char* floorSql=[[NSString stringWithFormat:@"select floor.ID, floor.spatialStructureID, spatialstructure.name, spatialstructure.GUID , placement.x , placement.y ,placement.z, placement.angle  from floor LEFT JOIN spatialstructure on floor.spatialStructureID=spatialstructure.ID LEFT JOIN placement on spatialstructure.placementID=placement.ID left join floorinfo on floorinfo.floorID=floor.ID where bldgID=%d order by placement.z, floorinfo.floorHeight!=0", bldgIDForFloor] UTF8String];
                    
                    sqlite3_stmt *floorStmt;
                    if(sqlite3_prepare_v2(database, floorSql, -1, &floorStmt, NULL) == SQLITE_OK) {
                        while(sqlite3_step(floorStmt) == SQLITE_ROW) {       
                            

                            floor=[[Floor alloc] initWithFloorSQLStmt:self database:database sqlStmt:floorStmt parentTransform:bldgTransform bldgFirstLevelIndex:bldg.firstFloorIndex currentIndex:pFloor];
                            if (pFloor!=bldg.selectedFloorIndex){
                                [bldg addSpatialStructure:floor];
                                [floor release];
                                pFloor++;
                            continue;}
                            CGAffineTransform floorTransform=CGAffineTransformConcat([floor.placement toCGAffineTransform], bldgTransform) ;
                            //                               [bldg addSpatialStructure:floor];
                            
                            
                            
                            
                            
                            //                        const char* slabSql=[[NSString stringWithFormat:@"select slab.ID, element.polylineID from slab LEFT JOIN element on slab.elementID=element.ID  where element.spatialStructureID=%d", floorSpatialStructureID] UTF8String];
                            
                            
                            Slab* slab=NULL;
                            const char* slabSql=[[NSString stringWithFormat:@"select slab.ID, element.polylineID, element.GUID, slab.slabThk, placement.x , placement.y ,placement.z ,placement.angle, element.polylineStr,element.arcStr  from slab LEFT JOIN element on slab.elementID=element.ID LEFT JOIN polyline on element.polylineID=polyline.ID LEFT JOIN placement on element.placementID=placement.ID where element.spatialStructureID=%d",floor.spatialStructureID] UTF8String];
                            sqlite3_stmt *slabStmt;
                            if(sqlite3_prepare_v2(database, slabSql, -1, &slabStmt, NULL) == SQLITE_OK) {
                                while(sqlite3_step(slabStmt) == SQLITE_ROW) {  
                                    slab=[[Slab alloc] initWithSlabSQLStmt:self database:database sqlStmt:slabStmt parentTransform:floorTransform];                                    
                                    [floor addElement:slab];
                                    [slab release];
                                    
                                    //                                        NSLog (@"floor bbox minX%f minY%f maxX%f maxY%f width%f height%f",[floor.bBox minX],[floor.bBox minY],[floor.bBox maxX],[floor.bBox maxY],[floor.bBox getWidth],[floor.bBox getHeight]);
                                    
                                    
                                    
                                    
                                }
                                
                                
                            }      
                        
                            sqlite3_finalize(slabStmt);
                            [bldg addSpatialStructure:floor];
                            [floor release];
                            
                            
                            //                                NSLog (@"bldg  placement : %f, %f", bldg.placement.pt.x,bldg.placement.pt.y);
                            //                                NSLog (@"bldg bbox minX%f minY%f maxX%f maxY%f width%f height%f",[bldg.bBox minX],[bldg.bBox minY],[bldg.bBox maxX],[bldg.bBox maxY],[bldg.bBox getWidth],[bldg.bBox getHeight]);
                            
                            
                            pFloor++;
//                            continue;
//                            break; //**Important to break so not to import all floor into one bldg in one model
                        }
                        
                        
                    }
            
                    sqlite3_finalize(floorStmt);
                    
                    [((Site*) self.root) addSpatialStructure:bldg];
                    [bldg release];
                    
                    
                    //                        NSLog (@"site bbox minX%f minY%f maxX%f maxY%f width%f height%f",[site.bBox minX],[site.bBox minY],[site.bBox maxX],[site.bBox maxY],[site.bBox getWidth],[site.bBox getHeight]);
                    
                }
                
            }
    
            sqlite3_finalize(bldgStmt);
        }
        

    }
    sqlite3_finalize(siteStmt);

    [appDele setCurrentSite:(Site*) self.root];
    
    
    const char* otherDeptSql=[[NSString stringWithFormat:@"SELECT * from DeptName_Other WHERE siteID=\"%d\" ORDER BY ID",[appDele currentSite].ID] UTF8String];
    
    sqlite3_stmt *otherDeptStmt; 
    if(sqlite3_prepare_v2(database, otherDeptSql, -1, &otherDeptStmt, NULL) == SQLITE_OK) {
        while( sqlite3_step(otherDeptStmt) == SQLITE_ROW) {
            uint otherDeptID=sqlite3_column_int(otherDeptStmt,0) ;
            
            Department* dept=[[Department alloc] initWithID:otherDeptID name:[ProjectViewAppDelegate readNameFromSQLStmt:otherDeptStmt column:2] colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:otherDeptStmt column:3]];

//            colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:otherDeptStmt column:35]];
            
//            CustomColorStruct* customColorStruct=[[CustomColorStruct alloc] initWithDatabase:database sqlStmt:customSpaceColorStmt];
            [appDele.currentSite.aDepartment addObject:dept];  
            [dept release];
        }
    }

    sqlite3_finalize(otherDeptStmt);
    const char* customSpaceColorSql=[[NSString stringWithFormat:@"select * from customsetting where type=\"Space\" AND customsetting.siteID=%d",[appDele currentSite].ID] UTF8String];

    sqlite3_stmt *customSpaceColorStmt; 
    if(sqlite3_prepare_v2(database, customSpaceColorSql, -1, &customSpaceColorStmt, NULL) == SQLITE_OK) {
        while( sqlite3_step(customSpaceColorStmt) == SQLITE_ROW) {
            CustomColorStruct* customColorStruct=[[CustomColorStruct alloc] initWithDatabase:database sqlStmt:customSpaceColorStmt];
            [appDele.currentSite addSpaceCustomColorStruct:customColorStruct];  
            [customColorStruct release];
        }
    }
    sqlite3_finalize(customSpaceColorStmt);
    
    const char* customBldgColorSql=[[NSString stringWithFormat:@"select * from customsetting where type=\"Bldg\" AND customsetting.siteID=%d",[appDele currentSite].ID] UTF8String];
    
    sqlite3_stmt *customBldgColorStmt; 
    if(sqlite3_prepare_v2(database, customBldgColorSql, -1, &customBldgColorStmt, NULL) == SQLITE_OK) {
        while( sqlite3_step(customBldgColorStmt) == SQLITE_ROW) {
            CustomColorStruct* customColorStruct=[[CustomColorStruct alloc] initWithDatabase:database sqlStmt:customBldgColorStmt];
            [appDele.currentSite addBldgCustomColorStruct:customColorStruct];       
            [customColorStruct release];
        }
    }    
    sqlite3_finalize(customBldgColorStmt);
    
    const char* customHeaderSql=[[NSString stringWithFormat:@"select * from customheader where customheader.siteID=%d",[appDele currentSite].ID] UTF8String];
    
    sqlite3_stmt *customHeaderStmt; 
    if(sqlite3_prepare_v2(database, customHeaderSql, -1, &customHeaderStmt, NULL) == SQLITE_OK) {
        while( sqlite3_step(customHeaderStmt) == SQLITE_ROW) {
            
            CustomHeaderStruct* customHeaderStruct=[[CustomHeaderStruct alloc] initWithDatabase:database sqlStmt:customHeaderStmt];
            appDele.currentSite.customHeaderStruct=customHeaderStruct;      
            [customHeaderStruct release];
            
            
            
        }
    }    

    sqlite3_finalize(customHeaderStmt);
    
    
    
    
    
    
    isImperial=[[appDele currentSite] isImperial];;    
    measureUnitLength=nil;//@"m";
    measureUnitArea=nil;//@"sqm";        
    if (isImperial){
        NSString* _measureUnitArea=[[NSString alloc] initWithString:@"sqft"];
        self.measureUnitArea=_measureUnitArea;
        [_measureUnitArea release];
        NSString* _measureUnitLength=[[NSString alloc] initWithString:@"ft"];
        self.measureUnitLength=_measureUnitLength;
        [_measureUnitLength release];
    }else{
        
        NSString* _measureUnitArea=[[NSString alloc] initWithString:@"sqm"];
        self.measureUnitArea=_measureUnitArea;
        [_measureUnitArea release];
        NSString* _measureUnitLength=[[NSString alloc] initWithString:@"m"];
        self.measureUnitLength=_measureUnitLength;
        [_measureUnitLength release];
    }
    
    
    
//    currencyUnit=nil;    
    NSString* currency=[[appDele currentSite] currency];
    
    if ([currency isEqualToString:@"USD"]){
        NSString* _currencyUnit=[[NSString alloc] initWithString:@"$"];        
        self.currencyUnit=_currencyUnit;
        [_currencyUnit release];
    }else if ([currency isEqualToString:@"EUR"]){
        NSString* _currencyUnit=[[NSString alloc] initWithString:@"€"];        
        self.currencyUnit=_currencyUnit;
        [_currencyUnit release];
    }else if ([currency isEqualToString:@"GBP"]){
        NSString* _currencyUnit=[[NSString alloc] initWithString:@"£"];        
        self.currencyUnit=_currencyUnit;
        [_currencyUnit release];
    }else if ([currency isEqualToString:@"ZAR"]){
        NSString* _currencyUnit=[[NSString alloc] initWithString:@"R"];        
        self.currencyUnit=_currencyUnit;
        [_currencyUnit release];
    }else if ([currency isEqualToString:@"NOK"]){
        NSString* _currencyUnit=[[NSString alloc] initWithString:@"kr"];        
        self.currencyUnit=_currencyUnit;
        [_currencyUnit release];
    } 
    
}


- (void) closeDBModel{
	if(database) sqlite3_close(database);
}

- (void) addAProductToModel: (OPSProduct*) product
{
    
}


- (id) init{
    self=[super init];
    if (self){
        self.modelScaleForScreenFactor=1.0;
        self.colorMethod=0;
//        [ root release];
//        root = [ [Floor alloc] init];
//        [root autorelease];
    }
    
    
    
    
    
    
    
    return self;
    
}



+ (void) finalizeStatements {
	
	if(database) sqlite3_close(database);
}

- (id) initWithPrimaryKey:(NSInteger) pk {
	
	self=[super init];
    if (self){
        self.modelScaleForScreenFactor=1.0;
    }
	return self;
}

@end

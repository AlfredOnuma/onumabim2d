//
//  DataConnect.h
//  DataConnect
//
//  Created by Alfred Man on 10/17/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

//#import <Foundation/Foundation.h>

#import <sqlite3.h>

@class ProjectViewAppDelegate;
@class OPSModel;
@class Project;
@class OPSStudio;
@class LiveProjectListVC;

@interface DataConnect : NSObject {
    NSString* userName;
    NSString* userPW;    
    NSMutableArray *projectArray;
    NSMutableArray *studioArray;
    
//    OPSStudio* activeStudio;
//    uint activeStudioID;
    NSFileHandle* handle;
    NSMutableData* receivedData;   
    NSNumber* filesize;
    float receivedLength;    
    
    NSString* dbPath;    
}
//@property (nonatomic, assign) uint activeStudioID;
//@property (nonatomic, copy) OPSStudio* activeStudio;
@property (nonatomic, retain) NSMutableArray* studioArray;
//@property (nonatomic, retain) NSMutableArray* projectSiteArray;
@property (nonatomic, retain) NSMutableArray* projectArray;
@property (nonatomic, retain) NSString* userName;
@property (nonatomic, retain) NSString* userPW;


@property (nonatomic, retain) NSFileHandle* handle;
@property (nonatomic, retain) NSMutableData* receivedData;
@property (nonatomic, retain) NSNumber* fileSize;
@property (nonatomic, assign) float receivedLength;

@property (nonatomic, retain) NSString* dbPath;

//Static methods.

//class method
//- (void) copyModelFromServer:(Project*) project liveProjectListVC:(LiveProjectListVC*) liveProjectListVC;
- (NSString* ) getProjectNameByIndex: (int) index;
//-(int) checkLiveUserNameAndPW;
- (id) init:(NSString*) newUserName userPW:(NSString*) newUserPW;
//- (int) setupProjectFromServer;
//- (uint) readSiteListFromServer;
- (BOOL) readSiteListFromLocal;

//- (uint) readStudioListFromServer;
//- (void) setupProjectFromDB:(NSString *) dbPath;
- (void) closeDBModel;


@end

//
//  DataConnect.m
//  DataConnect
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//
#import "DataConnect.h"
#import "Project.h"
#import "CXMLDocument.h"

#import "ProjectViewAppDelegate.h"
#import "OPSModel.h"
//#import "Project.h"
#import "OPSProjectSite.h"
#import "OPSStudio.h"
#import "TableCellTitleProjectCat.h"
#import "LiveProjectListVC.h"
#import "TableCellTitleProject.h"
static sqlite3 *database = nil;

@implementation DataConnect
@synthesize userName;
@synthesize userPW;
@synthesize dbPath;
@synthesize projectArray;
@synthesize studioArray;
//@synthesize activeStudio;
//@synthesize activeStudioID;


//- (void) setupModelFromServer:(NSInteger) siteID{  

//
//- (void) copyModelFromServer:(OPSProjectSite*) projectSite liveProjectListVC:(LiveProjectListVC*) liveProjectListVC{      
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];  
////    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=44&siteID=%d&filename=OPS", [project ID]];
//    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=%d&siteID=%d",[appDele activeStudio].ID, [projectSite ID]];
//    NSURL *url = [NSURL URLWithString: urlString];
//    //    UIImage *image = [[UIImage imageWithData: [NSData dataWithContentsOfURL: url]] retain];
//    
//    
//    
//    //    NSData *fetchedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"",projectID]];
//    
//    NSData *fetchedData = [NSData dataWithContentsOfURL:url];    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0]; 
//        
//
//    NSString* databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",userName,[appDele activeStudio].ID,[appDele activeStudio].name,[appDele activeStudio].iconName]] ;
//    NSFileManager *NSFm= [NSFileManager defaultManager]; 
//    BOOL isDir=YES;    
//    if(![NSFm fileExistsAtPath:databaseFolderPath isDirectory:&isDir])
//        if(![NSFm createDirectoryAtPath:databaseFolderPath withIntermediateDirectories:isDir attributes:Nil error:Nil])
//            NSLog(@"Error: Create folder failed");
//    
////    [projectSite setDbPath: [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%@_I_%@/%@.sqlite",userName,activeStudio.name,activeStudio.iconName, [projectSite name]]] ];     
//    
//    [projectSite setDbPath: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.sqlite", [projectSite name]]] ];     
//    
//    [fetchedData writeToFile:projectSite.dbPath atomically:YES];    
//
//
//    //-----------------------------------Create PList File of project inside Studio---------------------------------------------------------------
//    NSMutableDictionary *dictionary=nil;
//    //    NSString *path = [[NSBundle mainBundle] bundlePath];        
//    
//    NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
//    if(![NSFm fileExistsAtPath:dictionaryPath isDirectory:NO]){        
//        if(![NSFm createFileAtPath:dictionaryPath contents:[NSData dataWithContentsOfFile:@""] attributes:Nil ]  )        {
//            NSLog(@"Error: Create projectInfoFile failed");
//        }
//        dictionary = [NSMutableDictionary dictionaryWithObject:[projectSite toDictionaryObject] forKey:projectSite.name];
//        
//    }else{
//        dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];           
//        [dictionary setObject:[projectSite toDictionaryObject] forKey:projectSite.name];  
//    }
//    // dump the contents of the dictionary to the console
//    //    for (id key in dictionary) {
//    //        NSLog(@"memory: key=%@, value=%@", key, [dictionary objectForKey:key]);
//    //    }  
//    
//    // write xml representation of dictionary to a file
//    [dictionary writeToFile:dictionaryPath atomically:YES];
//    //--------------------------------------------------------------------------------------------------    
//    
//    
//    [liveProjectListVC removeDownloadDisableOverlay];
//    
//    return;
//    
//    
//}

/*
 ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
 [request setDelegate:self];
 [request setDownloadDestinationPath:@"{path to file in documents folder}"]];
 [request startAsynchronous];
 
 ...
 
 - (void)requestFinished:(ASIHTTPRequest *)request 
 {
 // Downloaded file is ready for use
 }
 
 - (void)requestFailed:(ASIHTTPRequest *)request
 {
 // Download failed. This is why.
 NSError *error = [request error];
 }
 */

-(void) downloadRequest:(NSURL *)requestURL

{    
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:requestURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];    
    if (theConnection) {        
        
        NSFileManager *manager = [NSFileManager defaultManager];        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);        
        NSString *path = [paths objectAtIndex:0];
        
        //Some code to get the correct path and name of file        
        if( [manager fileExistsAtPath:path] )
            
        {
            
            NSError *error = [[NSError alloc] init];
            
            [manager removeItemAtPath:path error:&error];
            
            [error release];
            
        }
        
        [manager createFileAtPath:path contents:nil attributes:nil];
        NSFileHandle* _handle=[[NSFileHandle fileHandleForWritingAtPath:path] retain]; 
        handle = _handle;
        [_handle release];
        
        NSMutableData* _receivedData=[[NSMutableData alloc] initWithLength:0];
        receivedData =_receivedData;
        [_receivedData release];
        
        [theConnection start];
        
    }
    [theConnection release];
    
}



//#pragma mark NSURLConnection functions____

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response

{
    
    [receivedData setLength:0]; 
    
    filesize = [[NSNumber numberWithLong: [response expectedContentLength] ] retain];
    
    receivedLength = 0;
    
}



- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data

{
    
    [receivedData appendData:data];
    
    /*
     
    receivedLength = receivedLength + [data length];
    
    
    float progress = 0;
    
    progress = ((float)receivedLength / [filesize floatValue]) ;
    
     progressView.progress = progress;
     
     
     if( receivedData.length > TWO_AND_A_HALF_MB && handle!=nil )
     
     {          
     [handle writeData:receivedData];
     
     [receivedData release];
     
     receivedData =[[NSMutableData alloc] initWithLength:0];
     }
     */
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection

{     
    [handle writeData:receivedData];        
    //some code to update UI
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge

{     
    //some code to correctly authenticate
}


/*
- (void) copyModelFromServer: (Project*) project{    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
    appDele.model= [[OPSModel alloc]init];        
    [appDele.model copyModelFromServer:project];
}

- (void) setupModelFromDB: (NSIndexPath*) indexPath{
    int row=[indexPath row];
    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];    
    appDele.model= [[OPSModel alloc]init];        

    appDele.model= [[OPSModel alloc]init];
        
    [appDele.model setupModelFromDB:[self getDBPath] projectID:row];   
}
*/

/*
- (void) copyDatabaseIfNeeded {
	
	//Using NSFileManager we can perform many file system operations.
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSString *dbPath = [self getDBPath];
	BOOL success = [fileManager fileExistsAtPath:dbPath]; 	
    
	if(!success) {		
		NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"OPS.sqlite"];
		success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
		
		if (!success) 
			NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
	}else{
        
    }
}
*/
/*
- (NSString *) getDBPath {	
	//Search for standard documents using NSSearchPathForDirectoriesInDomains
	//First Param = Searching the documents directory
	//Second Param = Searching the Users directory and not the System
	//Expand any tildes and identify home directories.
    
//	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
//	NSString *documentsDir = [paths objectAtIndex:0];
//	return [documentsDir stringByAppendingPathComponent:@"OPS.sqlite"];
    
    return self.dbPath;
}

*/


- (id) init:(NSString*) newUserName userPW:(NSString*) newUserPW{    
    self=[super init];
    if (self){
//        [newUserName retain];
        [userName release];
        userName=[newUserName copy];
        
//        [newUserPW retain];
        [userPW release];
        userPW=[newUserPW copy];
    }
    return self;
}
//kenchoihm 
//5021983

//
//
//- (uint) readStudioListFromServer{     
//    uint errorCode=0;
////    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    //    int errorCode=[appDele checkStudioWithUserName:<#(NSString *)#> userPW:<#(NSString *)#>];
//    //    if (errorCode!=0) {return errorCode;}
//    
//    NSMutableArray* _studioArray=[[NSMutableArray alloc]init];
//    self.studioArray=_studioArray;
//    [_studioArray release];
//
//    NSString *urlString = [NSString stringWithFormat: @"http://www.onuma.com/plan/webservices/baseXML.php?u=%@&p=%@&studios=1", userName, userPW];            
//    NSURL *url = [NSURL URLWithString:urlString ];
//    NSError* error;
//    CXMLDocument *parser = [[[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:&error] autorelease];
//    //    - (id)initWithContentsOfURL:(NSURL *)aURL options:(NSUInteger)mask error:(NSError **)errorPtr
//    
//    
//    //    NSMutableArray *res = [[NSMutableArray alloc] init];                     
//    NSArray *rows = [parser nodesForXPath:@"//ROW" error:nil];
//    
//    for (CXMLElement *row in rows) {        
//        
//        CXMLElement* studioIDNode=[row nodesForXPath:@"COL[1]/DATA" error:nil];       
//        NSString* studioIDStr=[[studioIDNode objectAtIndex:0] stringValue];
//        NSInteger studioID=[studioIDStr integerValue];
//        
//        CXMLElement* studioNameNode=[row nodesForXPath:@"COL[2]/DATA" error:nil];       
//        NSString *studioName=[[studioNameNode objectAtIndex:0] stringValue];
//        
//        CXMLElement* studioIconNameNode=[row nodesForXPath:@"COL[3]/DATA" error:nil];       
//        NSString *studioIconName=[[studioIconNameNode objectAtIndex:0] stringValue];
//        
//        NSInteger dotStart = [studioIconName rangeOfString:@"."].location;
////        if(dotStart == NSNotFound)
////            return nil;
////        return [theString substringToIndex:hyphenStart];
//        studioIconName=[studioIconName substringToIndex:dotStart];
//        
//        //        NSLog(@"%@", name);
//        OPSStudio* opsStudio= [[OPSStudio alloc] init:studioID name:studioName  iconName:studioIconName];
//        [self.studioArray addObject:opsStudio];
//        [opsStudio release];
//                         
//    }
//    
//    
//    //  and we print our results
//    //  NSLog(@"%@", res);
//    //    [res release];
//    
//    return errorCode;
//    
//}
//
//
//+(NSArray*)arrayOfFoldersInFolder:(NSString*) folder {
//	NSFileManager *fm = [NSFileManager defaultManager];
//	NSArray* files = [fm directoryContentsAtPath:folder];
//	NSMutableArray *directoryList = [NSMutableArray arrayWithCapacity:10];
//    
//	for(NSString *file in files) {
//		NSString *path = [folder stringByAppendingPathComponent:file];
//		BOOL isDir = NO;
//		[fm fileExistsAtPath:path isDirectory:(&isDir)];
//		if(isDir) {
//			[directoryList addObject:file];
//		}
//	}
//    
//	return directoryList;
//}
//
//- (uint) readSiteListFromServer{        
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
////    int errorCode=[appDele checkStudioWithUserName:<#(NSString *)#> userPW:<#(NSString *)#>];
////    if (errorCode!=0) {return errorCode;}
//    
//    uint errCode=0;
//    NSMutableArray* _projectArray=[[NSMutableArray alloc] init];
//    self.projectArray=_projectArray;
//    [_projectArray release];
//    
//    NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"MY PROJECTS:",self.userName];
//    TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:0 iconName:nil];
//    [tbCellTitleMyProjectStr release];
//    [self.projectArray addObject:tbCellTitleMyProject];
//    [tbCellTitleMyProject release];
//    
////    self.projectArray=[[NSMutableArray alloc] init];            
//    NSString *urlString = [NSString stringWithFormat: @"http://www.onuma.com/plan/webservices/baseXML.php?sysID=%d&u=%@&p=%@&sites", [appDele activeStudio].ID, userName, userPW];            
//    NSURL *url = [NSURL URLWithString:urlString ];
//    NSError* error;
//    CXMLDocument *parser = [[[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:&error] autorelease];
////    - (id)initWithContentsOfURL:(NSURL *)aURL options:(NSUInteger)mask error:(NSError **)errorPtr
//    
//    
////    TableCellTitleProject* tbCellTitleProject=nil;
////    NSMutableArray *res = [[NSMutableArray alloc] init];                     
//    NSArray *rows = [parser nodesForXPath:@"//ROW" error:nil];
//
//    for (CXMLElement *row in rows) {        
//        
//
//        
//        CXMLElement* siteIDNode=[row nodesForXPath:@"COL[1]/DATA" error:nil];       
//        NSString* siteIDStr=[[siteIDNode objectAtIndex:0] stringValue];
//        NSUInteger siteID=[siteIDStr integerValue];
//                        
//        CXMLElement* nameDataNode=[row nodesForXPath:@"COL[2]/DATA" error:nil];       
//        NSString *siteName=[[nameDataNode objectAtIndex:0] stringValue];
//        
//        
//        CXMLElement* siteSharedNode=[row nodesForXPath:@"COL[3]/DATA" error:nil];       
//        NSString* siteSharedStr=[[siteSharedNode objectAtIndex:0] stringValue];
//        NSUInteger siteShared=[siteSharedStr integerValue];
//        
//        
//        
//        CXMLElement* siteProjectIDNode=[row nodesForXPath:@"COL[11]/DATA" error:nil];       
//        NSString* siteProjectIDStr=[[siteProjectIDNode objectAtIndex:0] stringValue];
//        NSUInteger siteProjectID=[siteProjectIDStr integerValue];
//        
//        
//        
//        CXMLElement* siteProjectNameNode=[row nodesForXPath:@"COL[12]/DATA" error:nil];      
//        NSString *siteProjectName=[[NSString alloc ] initWithString:[[siteProjectNameNode objectAtIndex:0] stringValue]];
//        
//        
//        
//        CXMLElement* siteThumnailNameNode=[row nodesForXPath:@"COL[13]/DATA" error:nil];   
//        NSString *siteThumnailName=[[NSString alloc ] initWithString:[[siteThumnailNameNode objectAtIndex:0] stringValue]];
//        
//        siteName=[[NSString alloc] initWithFormat: @"S%d_%d - %@",[appDele activeStudio].ID,siteID,siteName];
//        
//        OPSProjectSite* projectSite=[[OPSProjectSite alloc] init:siteID name:siteName shared:siteShared projectID:siteProjectID projectName:siteProjectName iconName:siteThumnailName];
//        [siteName release];
//        [siteProjectName release];
//        [siteThumnailName release];
//        
////        NSLog(@"%@", name);
////        Project* project= [[Project alloc] init:projectID guid:NULL name:name ];
//        
//        if (siteShared==1 && ((TableCellTitleObject*) [self.projectArray lastObject]).shared!=1 ){
//            NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"READ ONLY PROJECTS:",self.userName];
//            TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:1 iconName:nil];
//            [tbCellTitleMyProjectStr release];
//            [self.projectArray addObject:tbCellTitleMyProject];
//            [tbCellTitleMyProject release];
//        }
//        if (siteShared==2 && ((TableCellTitleObject*) [self.projectArray lastObject]).shared!=2 ){
//            NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"READ / WRITE PROJECTS:",self.userName];
//            TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:2 iconName:nil];
//            [tbCellTitleMyProjectStr release];
//            [self.projectArray addObject:tbCellTitleMyProject];
//            [tbCellTitleMyProject release];
//        }
//
//        
//        
//        if ([[[self projectArray] lastObject] isKindOfClass:[TableCellTitleProjectCat class]] || 
//            (([[self.projectArray lastObject] isKindOfClass:[OPSProjectSite class]]) && 
//             ((OPSProjectSite*) [self.projectArray lastObject]).projectID!=siteProjectID)
//        ){            
////        if ([[self projectArray] count]==1 || 
////            (([[self.projectArray lastObject] isKindOfClass:[OPSProjectSite class]]) && 
////             ((OPSProjectSite*) [self.projectArray lastObject]).projectID!=siteProjectID)
////        ){            
//            NSString* tbCellTitleProjectNameStr=[[NSString alloc] initWithFormat: @"Project: %@",siteProjectName];
////            [tbCellTitleProject release];
//            TableCellTitleProject* tbCellTitleProject=[[TableCellTitleProject alloc] init:siteProjectID name:tbCellTitleProjectNameStr shared:siteShared iconName:nil];
//            [tbCellTitleProjectNameStr release];
//            [self.projectArray addObject:tbCellTitleProject];             
//            [tbCellTitleProject release];
//        }
//        
//
//        [self.projectArray addObject:projectSite];
//        [projectSite release];
////        [project release];                     
//    }
////    [tbCellTitleProject release];
//
//    
////  and we print our results
////  NSLog(@"%@", res);
////    [res release];
//    
//    return errCode;
//    
//}


- (BOOL) readSiteListFromLocal{        
//    int errorCode=0;
    
    self.projectArray=[[NSMutableArray alloc] init];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; 
    
    documentsDirectory=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",userName]]; 	    
    
    
    
    
//    NSMutableArray* studioFolder=[DataConnect arrayOfFoldersInFolder:documentsDirectory];
    
    
    //    NSString *bundleRoot = [[NSBundle mainBundle] resourcePath];        
    NSFileManager *manager = [NSFileManager defaultManager];    
    //    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:bundleRoot];
    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:documentsDirectory];    
    
    NSString *filename;
    
    int pProjectID=0;
    while ((filename = [direnum nextObject] )) {
        
//        NSString *regexStr = @"href=\"\\/functions\\?=(.+?)\\\\x26amp";
        /*
        NSString *regexStr = @"\\w*.sqlite";
        NSError *error;
        NSRegularExpression *testRegex = [NSRegularExpression regularExpressionWithPattern:regexStr options:NSRegularExpressionCaseInsensitive error:&error];


        if( testRegex == nil ) NSLog( @"Error making regex: %@", error );
        NSTextCheckingResult *result = [testRegex firstMatchInString:filename options:NSRegularExpressionCaseInsensitive range:NSMakeRange(0, [filename length])];
//        NSRange range = [result rangeAtIndex:1];        
        */


        
        
        
        if ([filename hasSuffix:@".sqlite"]) {   //change the suffix to what you are looking for
             
            NSMutableString *filenameOnly=[NSMutableString stringWithString:filename];
            [filenameOnly replaceCharactersInRange: [filenameOnly rangeOfString: @".sqlite"] withString: @""];

//            [filenameOnly replaceCharactersInRange: [filenameOnly rangeOfString: [NSString stringWithFormat:@"%@_-_",userName] ] withString: @""];
            
            // Do work here            
            uint projectSiteShared=0;
            uint projectSiteProjectID=0;
            NSString* projectSiteProjectName=nil;
            NSString* projectSiteThumnailName=nil;
            
            OPSProjectSite* projectSite= [[OPSProjectSite alloc] init:pProjectID name:filenameOnly shared:projectSiteShared projectID:projectSiteProjectID projectName:projectSiteProjectName iconName:projectSiteThumnailName];
            [projectSite setDbPath: [documentsDirectory stringByAppendingPathComponent:filename ] ];
            [projectArray addObject:projectSite];
            [projectSite release];     
            
        }        
    }    
    if (projectArray==Nil || [projectArray count]<=0 ) {
        return false;        
    }else{
        return true;
    }

    
}



/*

- (void) setupProjectFromDB: (NSString *)dbPath{
    //    NSInteger floorID=0;
    
    self.projectArray=[[NSMutableArray alloc] init];
    
	
    //  root=[[Floor alloc] init:0 guid:nil name:nil];
    
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {  
        
        
        const char* projectSql=[[NSString stringWithFormat:@"select ID, name from project "] UTF8String];                
		sqlite3_stmt *projectSelectstmt;
		if(sqlite3_prepare_v2(database, projectSql, -1, &projectSelectstmt, NULL) == SQLITE_OK) {
			while(sqlite3_step(projectSelectstmt) == SQLITE_ROW) {                                                       
                
                NSInteger projectID= sqlite3_column_int(projectSelectstmt, 0); 
                NSString* name= [NSString stringWithUTF8String:(char *) sqlite3_column_text(projectSelectstmt, 1)];                                
                [name autorelease];        
                
                Project* project= [[Project alloc] init:projectID guid:NULL name:name ];
                [projectArray addObject:project];
                [project release];
            }
        }
        
        
    }else{
        sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
    }
}
*/
- (NSString* ) getProjectNameByIndex: (int) index{
    Project* project=[self.projectArray objectAtIndex:index];    
    return project.name;
}

- (void) closeDBModel{
    if(database) sqlite3_close(database);
}


+ (void) finalizeStatements {
    if(database) sqlite3_close(database);
}

/*
 - (id) initWithPrimaryKey:(NSInteger) pk {
 
 [super init];
 //	coffeeID = pk;
 
 //	isDetailViewHydrated = NO;
 
 return self;
 }
 */                            

- (void)dealloc
{              
    
    [dbPath release];
    [userPW release];
    [userName release];
    [projectArray release]; 
    [studioArray release];
    [handle release];
    [receivedData release];
    [filesize release];
//    [activeStudio release];
    
    [super dealloc];
}

@end

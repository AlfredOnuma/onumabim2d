//
//  CMatrix.h
//  OPSMobile
//
//  Created by Alfred Man on 7/26/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CPoint.h"
@interface CMatrix : NSObject <NSCopying>{
    // [ a b p ]
    // [ c d q ]
    // [ e f r ]
    CGFloat a;
    CGFloat b;
    CGFloat c;    
    CGFloat d;
    CGFloat e;
    CGFloat f;    
    CGFloat u;
    CGFloat v;
    CGFloat w;   
}
@property (assign) CGFloat a;
@property (assign) CGFloat b;
@property (assign) CGFloat c;
@property (assign) CGFloat d;
@property (assign) CGFloat e;
@property (assign) CGFloat f;
@property (assign) CGFloat u;
@property (assign) CGFloat v;
@property (assign) CGFloat w;


-(id) init:(CGFloat) newA b:(CGFloat) newB c:(CGFloat) newC d:(CGFloat)  newD e:(CGFloat) newE f:(CGFloat) newF;


-(id) init:(CGFloat) newA b:(CGFloat) newB c:(CGFloat) newC d:(CGFloat)  newD e:(CGFloat) newE f:(CGFloat) newF u:(CGFloat)  newU v:(CGFloat) newV w:(CGFloat) newW ;
-(CMatrix*) concatMatrixCopy: (CMatrix*) matrix;
-(CMatrix*) concatMatrix: (CMatrix*) matrix;
-(CPoint*) applyPoint: (CPoint*) point;
-(CGFloat) getDeterminant;
-(CMatrix*) getInverse;
-(id)copyWithZone:(NSZone *)zone;
-(CGAffineTransform) toCGAffineTransform;
@end

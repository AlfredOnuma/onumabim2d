//
//  CMatrix.m
//  OPSMobile
//
//  Created by Alfred Man on 7/26/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "CMatrix.h"


@implementation CMatrix

@synthesize a;
@synthesize b;
@synthesize c;
@synthesize d;
@synthesize e;
@synthesize f;


@synthesize u;
@synthesize v;
@synthesize w;


-(CGAffineTransform) toCGAffineTransform{
    CGAffineTransform cgTrans=CGAffineTransformMake(self.a,self.b,self.c,self.d,self.e,self.f);
    return cgTrans;
}

-(id) init:(CGFloat) newA b:(CGFloat) newB c:(CGFloat) newC d:(CGFloat)  newD e:(CGFloat) newE f:(CGFloat) newF
{    
    if ([super init]){
        [self setA:(newA)];        
        [self setB:(newB)];     
        [self setC:(newC)];     
        [self setD:(newD)];     
        [self setE:(newE)];     
        [self setF:(newF)];
        
        [self setU:0];
        [self setV:0];
        [self setW:1];
        return self;
    }else{
        return nil;
    }        
}


-(id) init:(CGFloat) newA b:(CGFloat) newB c:(CGFloat) newC d:(CGFloat)  newD e:(CGFloat) newE f:(CGFloat) newF u:(CGFloat)  newU v:(CGFloat) newV w:(CGFloat) newW
{    
    if ([super init]){
        [self setA:(newA)];        
        [self setB:(newB)];     
        [self setC:(newC)];     
        [self setD:(newD)];     
        [self setE:(newE)];     
        [self setF:(newF)];    
        [self setU:(newU)];     
        [self setV:(newV)];     
        [self setW:(newW)];
        
        
        return self;
    }else{
        return nil;
    }        
}


-(CMatrix*) concatMatrix: (CMatrix*) matrix{
    CGFloat newA=a*matrix.a + c*matrix.b + e*matrix.u;
    CGFloat newC=a*matrix.c + c*matrix.d + e*matrix.v;
    CGFloat newE=a*matrix.e + c*matrix.f + e*matrix.w;
    
    CGFloat newB=b*matrix.a + d*matrix.b + f*matrix.u;
    CGFloat newD=b*matrix.c + d*matrix.d + f*matrix.v;
    CGFloat newF=b*matrix.e + d*matrix.f + f*matrix.w;
    
    
    CGFloat newU=u*matrix.a + v*matrix.b + w*matrix.u;
    CGFloat newV=u*matrix.c + v*matrix.d + w*matrix.v;
    CGFloat newW=u*matrix.e + v*matrix.f + w*matrix.w;    

    CMatrix* newMatrix=[[CMatrix alloc] init:newA b:newB c:newC d:newD e:newE f:newF u:newU v:newV w:newW];
    [newMatrix retain];
    return newMatrix;  
    /*
    [self setA:(a*matrix.a + c*matrix.b + e*0)];
    [self setC:(a*matrix.c + c*matrix.d + e*0)];
    [self setE:(a*matrix.e + c*matrix.f + e*1)];
    
    [self setB:(b*matrix.a + d*matrix.b + f*0)];
    [self setD:(b*matrix.c + d*matrix.d + f*0)];
    [self setF:(b*matrix.e + d*matrix.f + f*1)];
 
    return self;
         */
}
//              [ a b u ]
//[ x y 1 ]  *  [ c d v ]
//              [ e f w ]
-(CPoint*) applyPoint: (CPoint*) point{
//    CGFloat newX=a*point.x+c*point.y+e*1;
//    CGFloat newY=b*point.x+d*point.y+f*1;
    
    CGFloat newX=a*point.x+b*point.y+e*1;
    CGFloat newY=c*point.x+d*point.y+f*1;    
    [point setX:newX];
    [point setY:newY];
    return point;
}
-(CGFloat) getDeterminant{
//    return (a*d - b*c);
    return (a * (w*d - f*v) - c * (w*b - f*u) + e * (v*b - d*u));
}
-(CMatrix*) getInverse{
    /*
    CGFloat det=[self getDeterminant];
    CGFloat newA=1/det * d;
    CGFloat newB=1/det - b;
    CGFloat newC=1/det - c;
    CGFloat newD=1/det * a;
    CGFloat newE=1/det * (c*f - d*e);
    CGFloat newF=1/det * (e*b - a*f);    
    
    CMatrix* newMatrix=[[CMatrix alloc] init:newA b:newB c:newC d:newD e:newE f:newF];    
    */
    
    CGFloat det=[self getDeterminant];
    CGFloat newA=1/det * (w*d - f*v);
    CGFloat newB=1/det * (w*b - f*u)*-1;
    CGFloat newU=1/det * (v*b - d*u);
    
    CGFloat newC=1/det * (w*c - e*v)*-1;
    CGFloat newD=1/det * (w*a - e*u);
    CGFloat newV=1/det * (v*a - c*u)*-1;    
    
    CGFloat newE=1/det * (f*c - e*d);
    CGFloat newF=1/det * (f*a - e*b)*-1;                    
    CGFloat newW=1/det * (d*a - c*b);    
    
    
    CMatrix* newMatrix=[[CMatrix alloc] init:newA b:newB c:newC d:newD e:newE f:newF u:newU v:newV w:newW];    
    
    return newMatrix;     
}




// In the implementation
-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    CMatrix *newCopy = [[CMatrix alloc] init];
//    newCopy.obj = [obj copyWithZone: zone];    
    [newCopy setA:self.a];
    [newCopy setB:self.b];
    [newCopy setC:self.c];
    [newCopy setD:self.d];
    [newCopy setE:self.e];
    [newCopy setF:self.f];
    
    [newCopy setU:self.u];
    [newCopy setV:self.v];
    [newCopy setW:self.w];
    return newCopy;
}
@end

//
//  CPoint.m
//  OPSMobile
//
//  Created by Alfred Man on 7/13/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "CPoint.h"


@implementation CPoint
@synthesize x;
@synthesize y;

-(void) multiply: (double) multiplier{
    self.x*=multiplier;
    self.y*=multiplier;
}
-(id) init{
    self=[super init];
    if (self){
        
    }
    return self;
}
-(CGPoint) toCGPoint{
    return CGPointMake(self.x,self.y);
}
- (id) initWithCGPoint: (CGPoint) newCGPoint{
    self=[super init];
    if (self){
        self.x=newCGPoint.x;
        self.y=newCGPoint.y;
    }
    return self;
}
- (id) initWithX: (CGFloat) newX y:(CGFloat) newY{
    self=[super init];
    if (self){
        self.x=newX;
        self.y=newY;
    }    
    return self;
}
-(id) setX: (CGFloat)newX setY:(CGFloat)newY{
    self=[super init];
    if (self){
        self.x=newX;
        self.y=newY;
    }
    return self;
}

// In the implementation
-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    CPoint *newPoint = [[CPoint alloc] init];
    //    newCopy.obj = [obj copyWithZone: zone];    
    [newPoint setX:self.x];
    [newPoint setY:self.y];
    return newPoint;
}
@end

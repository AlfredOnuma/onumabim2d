//
//  CPoint.h
//  OPSMobile
//
//  Created by Alfred Man on 7/13/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CPoint : NSObject <NSCopying> {        
    CGFloat x;
    CGFloat y;    
}

@property (assign) CGFloat x;
@property (assign) CGFloat y;

-(id) setX: (CGFloat)newX setY:(CGFloat)newY;

- (id) initWithCGPoint: (CGPoint) newCGPoint;
- (id) initWithX: (CGFloat) newX y:(CGFloat) newY;
- (CGPoint) toCGPoint;
-(id)copyWithZone:(NSZone *)zone;
-(void) multiply: (double) multiplier;

@end

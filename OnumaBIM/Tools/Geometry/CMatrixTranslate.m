//
//  CMatrixTranslate.m
//  OPSMobile
//
//  Created by Alfred Man on 7/26/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "CMatrixTranslate.h"


@implementation CMatrixTranslate
@synthesize x;
@synthesize y;



-(id) init:(CGFloat)dX y:(CGFloat) dY
{
    
    if ([super init:1 b:0 c:0 d:1 e:dX f:dY]){
        [self setX:dX];  
        [self setY:dY];  
        return self;
    }else{
        return nil;
    }
        
}

-(CMatrix*) getInverse{
    CMatrix* newMatrix=[[CMatrixTranslate alloc] init: (-1*self.x) y: (-1*self.y)];
    return newMatrix;
}
@end

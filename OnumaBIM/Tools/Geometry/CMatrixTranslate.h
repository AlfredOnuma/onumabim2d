//
//  CMatrixTranslate.h
//  OPSMobile
//
//  Created by Alfred Man on 7/26/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMatrix.h"

@interface CMatrixTranslate : CMatrix {
    CGFloat x;
    CGFloat y;

}
@property (assign) CGFloat x;
@property (assign) CGFloat y;



-(id) init:(CGFloat)dX y:(CGFloat) dY;

@end

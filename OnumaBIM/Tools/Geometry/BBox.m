//
//  BBox.m
//  OPSMobile
//
//  Created by Alfred Man on 7/22/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "BBox.h"
#import "CPoint.h"

@implementation BBox

@synthesize bEmpty;
@synthesize minX;
@synthesize maxX;
@synthesize minY;
@synthesize maxY;

//-(void) rotate:(double) angle{
//    CPoint* p0=[[CPoint alloc] initWithX:self.minX y:self.minY];
//    CPoint* p1=[[CPoint alloc] initWithX:self.maxX y:self.minY];
//    CPoint* p2=[[CPoint alloc] initWithX:self.maxX y:self.maxY];
//    CPoint* p3=[[CPoint alloc] initWithX:self.minX y:self.maxY];
//
//    
//}
-(void) expand:(CGFloat) factor{
    self.minX-=[self getWidth]*(factor-1) *0.5;
    self.minY-=[self getHeight]*(factor-1)*0.5;
    self.maxX+=[self getWidth]*(factor-1)*0.5;
    self.maxY+=[self getHeight]*(factor-1)*0.5;    
        
}
-(void) multiply:(CGFloat)multiplier{
    self.minX*=multiplier;
    self.minY*=multiplier;
    self.maxX*=multiplier;
    self.maxY*=multiplier;
}
-(id) initWithCGRect: (CGRect) cgRect{
    self=[super init];

    if (self){    
//        CPoint        * p0=[[CPoint alloc ] initWithX:cgRect.origin.x y:cgRect.origin.y];        
//        [self updatePoint: p0 ];
//        [p0 release];
//        
//        CPoint* p1=[[CPoint alloc ] initWithX:cgRect.origin.x+cgRect.size.width y:cgRect.origin.y];        
//        [self updatePoint: p1 ];
//        [p1 release];
//        
//
//        CPoint* p2=[[CPoint alloc ] initWithX:cgRect.origin.x+cgRect.size.width y:cgRect.origin.y+cgRect.size.height];        
//        [self updatePoint: p2 ];
//        [p2 release];
//        
//        
//        CPoint* p3=[[CPoint alloc ] initWithX:cgRect.origin.x y:cgRect.origin.y+cgRect.size.height];        
//        [self updatePoint: p3 ];
//        [p3 release];
//        return self;
//        
        self.bEmpty=false;
        self.minX=cgRect.origin.x;    
        self.minY=cgRect.origin.y;
        self.maxX=self.minX+cgRect.size.width;
        self.maxY=self.minY+cgRect.size.height;     
    }
    return self;
}
/*
// In the implementation
-(CPoint*) lowerLeftPoint{
    CPoint* llPoint=[[CPoint alloc] setX:self.minX setY:self.minY];
    return llPoint;
}

-(CPoint*) lowerRightPoint{
    CPoint* llPoint=[[CPoint alloc] setX:self.maxX setY:self.minY];
    return llPoint;
}
-(CPoint*) upperRightPoint{
    CPoint* llPoint=[[CPoint alloc] setX:self.maxX setY:self.maxY];
    return llPoint;
}
-(CPoint*) upperLeftPoint{
    CPoint* llPoint=[[CPoint alloc] setX:self.minX setY:self.maxY];
    return llPoint;
}
 */
-(CGPoint) center{
    return CGPointMake((self.minX+self.maxX)/2,(self.minY+self.maxY)/2);
}
-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    BBox *newBBox = [[BBox alloc] init];
    //    newCopy.obj = [obj copyWithZone: zone];    
    [newBBox setBEmpty:self.bEmpty];
    [newBBox setMinX:self.minX];
    [newBBox setMinY:self.minY];
    [newBBox setMaxX:self.maxX];
    [newBBox setMaxY:self.maxY];

    
    return newBBox;
}
/*
-(void) applyMatrix:(CMatrix *)matrix{


   
    if (matrix.a==1 && matrix.b==0 && matrix.c==0 && matrix.d==1){
        CPoint* ptLL=[[CPoint alloc] setX:self.minX setY:minY];    
        CPoint* ptUR=[[CPoint alloc] setX:self.maxX setY:maxY];
        
        [matrix applyPoint: (ptLL)];
        [matrix applyPoint: (ptUR)];

        
        self.minX=ptLL.x;
        self.minY=ptLL.y;
        self.maxX=ptUR.x;
        self.maxY=ptUR.y; 
        
        [ptLL release];
        [ptUR release];
        
        
    }else{ //if It isnt a straight translation, eg rotated, negative scale... need to recaculate the min and max point
        CPoint* ptLL=[[CPoint alloc] setX:self.minX setY:minY];    
        CPoint* ptUR=[[CPoint alloc] setX:self.maxX setY:maxY];                
        CPoint* ptLR=[[CPoint alloc] setX:self.maxX setY:minY];    
        CPoint* ptUL=[[CPoint alloc] setX:self.minX setY:maxY];
        
        [matrix applyPoint: (ptLL)];
        [matrix applyPoint: (ptUR)];
        [matrix applyPoint: (ptLR)];
        [matrix applyPoint: (ptUL)];      
        
        self.minX=ptLL.x;
        self.minY=ptLL.y;
        self.maxX=ptUR.x;
        self.maxY=ptUR.y; 
        
        NSArray *aPoint=[[NSArray alloc] initWithObjects:ptLL,ptUR,ptLR,ptUL,nil];
        for (CPoint* pt in aPoint){
            if (pt.x<minX){
                minX=pt.x;
            }
            if (pt.x>maxX){
                maxX=pt.x;
            }        
            if (pt.y<minY){
                minY=pt.y;
            }
            if (pt.y>maxY){
                maxY=pt.y;
            }              
        }
        [aPoint release];
        
        [ptLR release];
        [ptUL release];
        
        [ptLL release];
        [ptUR release];
    }
    
    
    
}
*/
- (id) init{
    self=[super init];
    if (self){
        self.bEmpty=true;
    }
    return self;
}

-(id) mergeBBox: (BBox*) bBox{
    if (bBox==nil ){return self;}
    if (self.bEmpty==true){        
        [self setMinX:bBox.minX];
        [self setMinY:bBox.minY];
        [self setMaxX:bBox.maxX];
        [self setMaxY:bBox.maxY];
        bEmpty=false;
        return self;
    }
    if (bBox.minX<self.minX){
        [self setMinX:(bBox.minX)];
    }
    if (bBox.minY<self.minY){
        [self setMinY:(bBox.minY)];
    }
    if (bBox.maxX>self.maxX){
        [self setMaxX:(bBox.maxX)];
    }
    if (bBox.maxY>self.maxY){
        [self setMaxY:(bBox.maxY)];
    }
    return self;
}
-(void) updatePoint: (CPoint*) pt{
    if (self.bEmpty==true){
        self.minX=pt.x;
        self.minY=pt.y;
        self.maxX=pt.x;
        self.maxY=pt.y;        
        [self setBEmpty:false];
    }
    if (pt.x<=self.minX){
        [self setMinX:(pt.x)];
    }
    if (pt.y<=self.minY){
        [self setMinY:(pt.y)];
    }
    if (pt.x>=self.maxX){
        [self setMaxX:(pt.x)];
    }
    if (pt.y>=self.maxY){
        [self setMaxY:(pt.y)];
    }
    return;    
}
-(CGFloat) getWidth{
    return (self.maxX-self.minX);
}
-(CGFloat) getHeight{
    return (self.maxY-self.minY);
}
-(CGRect) cgRect{
    return CGRectMake(minX, minY, maxX-minX, maxY-minY);
}

-(void) displace: (CPoint*) pt{
    self.minX+=pt.x;
    self.minY+=pt.y;
    self.maxX+=pt.x;
    self.maxY+=pt.y;    
}
-(void)dealloc{
    [super dealloc];
}
@end

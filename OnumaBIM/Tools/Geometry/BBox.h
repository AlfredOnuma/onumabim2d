//
//  BBox.h
//  OPSMobile
//
//  Created by Alfred Man on 7/22/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPoint.h"
#import "CMatrix.h"
#import "ArcProfile.h"
@interface BBox : NSObject <NSCopying>{    
    CGFloat minX;
    CGFloat maxX;
    CGFloat minY;
    CGFloat maxY;
    bool bEmpty;
}

@property (assign) bool bEmpty;
@property (assign) CGFloat minX;
@property (assign) CGFloat maxX;
@property (assign) CGFloat minY;
@property (assign) CGFloat maxY;


-(void) expand:(CGFloat) factor;
-(void) multiply:(CGFloat) multiplier;
-(id) initWithCGRect: (CGRect) cgRect;
//-(id) setX: (CGFloat)newX setY:(CGFloat)newY;
/*
-(CPoint*) upperRightPoint;
-(CPoint*) upperLeftPoint;
-(CPoint*) lowerLeftPoint;
-(CPoint*) lowerRightPoint;
-(void) applyMatrix:(CMatrix *)matrix;
*/
-(id) mergeBBox: (BBox*) bBox;
-(void) updatePoint: (CPoint*) pt;
-(CGFloat) getWidth;
-(CGFloat) getHeight;
-(void) displace: (CPoint*) pt;
//-(void) rotate:(double) angle;
-(CGRect) cgRect;

-(CGPoint) center;
@end

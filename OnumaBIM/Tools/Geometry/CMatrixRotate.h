//
//  CMatrixRotate.h
//  OPSMobile
//
//  Created by Alfred Man on 7/26/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMatrix.h"

@interface CMatrixRotate : CMatrix {    
    CGFloat ang;    
}
@property (nonatomic, assign) CGFloat ang;


-(id) init:(CGFloat)newAng;
-(CMatrix*) getInverse;

@end

//
//  CMatrixRotate.m
//  OPSMobile
//
//  Created by Alfred Man on 7/26/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "CMatrixRotate.h"


@implementation CMatrixRotate

@synthesize ang;

-(id) init:(CGFloat)newAng //in Degree
{
    [self setAng:newAng];
    double angRad=newAng*M_PI/180;
    if ([super init:cos(angRad) b:-sin(angRad) c:sin(angRad) d:cos(angRad) e:0 f:0]){        
//Above is normal matrix for rotation but since we did a mirrorY right at start we make the matrix rotate as below:    
//    if ([super init:cos(newAng) b:sin(newAng) c:-sin(newAng) d:cos(newAng) e:0 f:0]){

        return self;
    }else{
        return nil;
    }
    
}

-(CMatrix*) getInverse{
    CMatrix* newMatrix=[[CMatrixRotate alloc] init: (-1*self.ang)];
    return newMatrix;
}

@end

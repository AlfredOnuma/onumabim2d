//
//  CMatrixScale.m
//  OPSMobile
//
//  Created by Alfred Man on 7/26/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "CMatrixScale.h"


@implementation CMatrixScale
@synthesize x;
@synthesize y;




-(id) init:(CGFloat)dX y:(CGFloat) dY
{
    
    if ([super init:dX b:0 c:0 d:dY e:0 f:0]){
        [self setX:dX];  
        [self setY:dY];  
        return self;
    }else{
        return nil;
    }
    
}

-(CMatrix*) getInverse{
    CMatrix* newMatrix=[[CMatrixScale alloc] init: (1/self.x) y: (1/self.y)];
    return newMatrix;
}
@end

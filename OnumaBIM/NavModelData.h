//
//  NavModelData.h
//  ProjectView
//
//  Created by Alfred Man on 11/18/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@class OPSToken;

@interface NavModelData : NSObject {
    OPSToken* opsToken;
    NSString* name;
}

@property (nonatomic, retain) OPSToken* opsToken;
@property (nonatomic, retain) NSString* name;

@end

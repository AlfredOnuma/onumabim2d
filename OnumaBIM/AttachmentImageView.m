//
//  AttachmentImageView.m
//  ProjectView
//
//  Created by Alfred Man on 2/29/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "AttachmentImageView.h"

@implementation AttachmentImageView
//@synthesize mediaImage;
@synthesize path;
@synthesize attachFileID;
-(void) dealloc{
//    [mediaImage release];mediaImage=nil;
    [path release],path=nil;
    [super dealloc];
}
-(id) initWithImage:(UIImage *)image path:(NSString*) _path attachFileID:(uint)_attachFileID{
    self=[super initWithImage:image];
    if (self){
        self.attachFileID=_attachFileID;
        self.path=_path;
    }
    return self;
}
/*
-(UIImage*)findLargeImage:(NSURL*) mediaurl
{
    //    NSString *mediaurl = [self.node valueForKey:kVMMediaURL];
    UIImage* largeImage=nil;
    //
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        ALAssetRepresentation *rep = [myasset defaultRepresentation];
        CGImageRef iref = [rep fullResolutionImage];
        if (iref) {
            UIImage* _image=[UIImage imageWithCGImage:iref];
            self.mediaImage=_image;
            [_image release];
//            self.image=[UIImage imageWithCGImage:iref];
            //            largeimage = [UIImage imageWithCGImage:iref];
            //            [largeimage retain];
            
        }
    };
    
    //
    ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
    {
        NSLog(@"booya, cant get image - %@",[myerror localizedDescription]);
    };
    
    if(mediaurl)// && [mediaurl length] )//&& ![[mediaurl pathExtension] isEqualToString:AUDIO_EXTENSION])
    {
        //        [largeimage release];
        NSURL *asseturl = mediaurl;//[NSURL URLWithString:mediaurl];
        ALAssetsLibrary* assetslibrary = [[[ALAssetsLibrary alloc] init] autorelease];
        [assetslibrary assetForURL:asseturl 
                       resultBlock:resultblock
                      failureBlock:failureblock];
    }
    return largeImage;
}
-(id) initWithMediaInfo:(NSDictionary*) info{
    self=[super init];
    if (self){
        
        NSURL* imgLocalUrl = (NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL];
        
        
        [self findLargeImage:imgLocalUrl];
    }
    return self;
}
*/
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

//- (void)didReceiveMemoryWarning
//{
//    // Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//    
//    // Release any cached data, images, etc that aren't in use.
//}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

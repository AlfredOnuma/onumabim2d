//
//  OPSStudio.m
//  ProjectView
//
//  Created by Alfred Man on 12/30/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "OPSStudio.h"


@implementation OPSStudio
@synthesize ID;
@synthesize name;
@synthesize iconName;
- (id) init:(uint) _ID name:(NSString*) _name iconName:(NSString*) _iconName{
    self=[super init];
    if (self){
        ID=_ID;
        [name release];
        [_name retain];
        name=_name;
        
        if (_iconName==nil){
            NSString* tmpIconName=[[NSString alloc] initWithString: @"onumalogo_50"];
            [iconName release];
            [tmpIconName retain];
            iconName=tmpIconName;
            [tmpIconName release];
            
        }else{
            [iconName release];
            [_iconName retain];
            iconName=_iconName;
        }
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
//    NSString* newName=[[NSString alloc ]initWithString:name];
//    NSString* newIconName=[[NSString alloc]initWithString:iconName];
//    NSString* newName=[NSString stringWithString:name];
//    NSString* newIconName=[NSString stringWithString:newIconName];
//    OPSStudio *copiedStudio = [[OPSStudio allocWithZone:zone] init:ID name:newName iconName:newIconName];
//    OPSStudio *copiedStudio = [[OPSStudio allocWithZone:zone] init:ID name:name iconName:iconName];
    
    OPSStudio *copiedStudio = [[OPSStudio allocWithZone:zone] init:ID name:name iconName:iconName];
//    OPSStudio *copiedStudio = [[OPSStudio allocWithZone:zone] init];
//    [copiedStudio setID:ID];
//    [copiedStudio setName:name];
//    [copiedStudio setIconName:iconName] ;
//    [newName release];
//    [newIconName release];
//    another.obj = [obj copyWithZone: zone];
    return copiedStudio;
}
- (void)dealloc{
//    NSLog(@"OPSSTUDIO DEALLOC ***************************** ");//name count:%d",[name retainCount]);
    [name release];
    [iconName release];    
    
//    NSLog(@"iconName count:%d",[iconName retainCount]);
    [super dealloc];
}
@end

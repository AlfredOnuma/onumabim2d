//
//  BIMReportNVC.m
//  ProjectView
//
//  Created by Alfred Man on 4/14/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "BIMReportNVC.h"
#import "BIMPlanColorCatTVC.h"
#import "ProjectViewAppDelegate.h"
@implementation BIMReportNVC

//@synthesize popoverController;
-(void) dealloc{
//    [popoverController release];popoverController=nil;
    [super dealloc];
}
//- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
//{
//    CGSize size=CGSizeMake(150, 135);
//    [popoverController setPopoverContentSize:size];
////    [popoverController setPopoverContentSize:viewController.contentSizeForViewInPopover];
//}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated
{      
//    self setContentSizeForViewInPopover:<#(CGSize)#>
//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate]; 
    if ([[[self viewControllers] lastObject] isKindOfClass:[BIMPlanColorCatTVC class]]){     
        
        

        self.contentSizeForViewInPopover=CGSizeMake(150, 135);
//        CGRect defaultFrame=CGRectMake( self.view.frame.origin.x,self.view.frame.origin.y,150,135);        
//        self.tableview.frame=defaultFrame;        
        return [super popViewControllerAnimated:animated];
    }
    /*
    if ([[[self viewControllers] lastObject] isKindOfClass:[BIMMailVC class]]){
        [UIView transitionWithView:self.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlDown) animations:^{
            [super popViewControllerAnimated:NO];            
            
        } completion:^(BOOL finished) {
            //            ViewModelVC* viewModelVC=[[self viewControllers] lastObject]; 
            //            ViewModelToolbar* toolbar=[viewModelVC viewModelToolbar];            
            //            [toolbar attachmentInfoTable:[toolbar attachButton]];            
        }                        
         ];
        
        return self;        
    }
    if ([[[self viewControllers] lastObject] isKindOfClass:[AttachmentInfoDetail class]]){
        
        //        ViewModelVC* viewModelVC=[[self viewControllers] objectAtIndex:([[ self viewControllers] count]-2)]; 
        
        [UIView transitionWithView:self.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlDown) animations:^{
            //            [viewModelVC.viewModelToolbar.popoverController dismissPopoverAnimated:NO];
            [super popViewControllerAnimated:NO];
            
            //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
        } completion:^(BOOL finished) {
            ViewModelVC* viewModelVC=[[self viewControllers] lastObject]; 
            ViewModelToolbar* toolbar=[viewModelVC viewModelToolbar];
            
            [toolbar attachmentInfoTable:[toolbar attachButton]];            
        }                        
         ];
        

        return self;
      
    }
     */
    return  [super popViewControllerAnimated:animated];
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

//
//  StudioListVC.m
//  ProjectView
//
//  Created by Alfred Man on 12/30/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "StudioListVC.h"
//#import "LiveProjectListTableVC.h"

#import "LiveProjectListVC.h"
#import "ProjectViewAppDelegate.h"

#import "OPSStudio.h"
//#import "DataConnect.h"
#import "ViewNeedWebForLiveDB.h"
#import "ProjectViewTabBarCtr.h"
#import "AttachmentWebViewController.h"

@implementation StudioListVC

@synthesize disableViewOverlay;
@synthesize theSearchBar;
@synthesize tableData;
@synthesize theTableView;
@synthesize studioArray;


// Initialize tableData and disabledViewOverlay 
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    self.tableData =[[NSMutableArray alloc]init];
//    self.disableViewOverlay = [[UIView alloc]
//                               initWithFrame:CGRectMake(0.0f,44.0f,320.0f,416.0f)];
//    self.disableViewOverlay.backgroundColor=[UIColor blackColor];
//    self.disableViewOverlay.alpha = 0;
//}

// Since this view is only for searching give the UISearchBar 
// focus right away





-(void) addHelpViewController: (id) sender{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if ([appDele isLiveDataSourceAvailable]){
        NSString* urlAddress=nil;

        
        urlAddress=@"https://www.onuma.com/products/OnumaHelp.php#ViewLocalStudios";
        
        
        
        
        uint width= self.view.bounds.size.width;
        uint height=self.view.bounds.size.height+44+20; //748
        
        
        AttachmentWebViewController* webViewController=[[AttachmentWebViewController alloc] initWithFrame:CGRectMake(0, 0, width, height) initURL:urlAddress hasNavControlBar:false];
        
        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
            [self.navigationController pushViewController:webViewController animated:NO];
            [webViewController release];
        } completion:^(BOOL finished) {
            
        }
         ];
        
        
    }else{
        
        NSString* urlAddress=nil;
        
        
        urlAddress=[[NSBundle mainBundle]pathForResource:@"ONUMA - Help" ofType:@"html" inDirectory:nil];
        UIWebView *web=[[UIWebView alloc]initWithFrame:self.view.frame];
        NSURL* url=[NSURL fileURLWithPath:urlAddress];

//        NSString *absoluteURLwithQueryString = [urlAddress stringByAppendingString: @"#ViewLocalStudios"];
//        NSURL *finalURL = [NSURL URLWithString: absoluteURLwithQueryString];

        
        [web loadRequest:[NSURLRequest requestWithURL:  url   ]];
        
        UIViewController* webViewController=[[UIViewController alloc] init];
        [webViewController setView:web];
        [web release];web=nil;
        

        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
            [self.navigationController pushViewController:webViewController animated:NO];
            [webViewController release];
        } completion:^(BOOL finished) {

        }
        ];

        

    }
    
}


- (void) setCustomNavigationBarButtons{
    
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:3];
    UIImage *buttonImage=nil;
    UIImage *buttonImageHighlight=nil;
    UIButton *button=nil;
    UIBarButtonItem *bi=nil;
    
    buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2.png" ofType:nil]];
    buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2Highlight.png" ofType:nil]];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(addHelpViewController:) forControlEvents:UIControlEventTouchUpInside];
    bi = [[UIBarButtonItem alloc] initWithCustomView:button];
    [buttons addObject:bi];
    [bi release];
    
    
    
    
    self.navigationItem.rightBarButtonItems=buttons;
    [buttons release];
    
    
    
    
}

//- (void) viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    [self setCustomNavigationBarButtons];
//}

-(void) viewDidLoad{
    [super viewDidLoad];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}
- (void)viewDidAppear:(BOOL)animated {
//    [self.theSearchBar becomeFirstResponder];
    [super viewDidAppear:animated];
    
    
//#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

    
}

-(IBAction) liveStudioBackButtonResond: (id) sender{  
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele displayLoginViewFromProjectVC];
    
}
#pragma mark -
#pragma mark UISearchBarDelegate Methods

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    // We don't want to do anything until the user clicks 
    // the 'Search' button.
    // If you wanted to display results as the user types 
    // you would do that here.

    if([searchBar.text isEqualToString: @""]){
        
//        searchBar.text=@"";
        [self searchBar:searchBar activate:NO];
        [tableData release];
        tableData=[self.studioArray copy];
        [self.theTableView reloadData];
        
        [searchBar resignFirstResponder];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // searchBarTextDidBeginEditing is called whenever 
    // focus is given to the UISearchBar
    // call our activate method so that we can do some 
    // additional things when the UISearchBar shows.
    [self searchBar:searchBar activate:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    // searchBarTextDidEndEditing is fired whenever the 
    // UISearchBar loses focus
    // We don't need to do anything here.

    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    // Clear the search text
    // Deactivate the UISearchBar
    searchBar.text=@"";
    [self searchBar:searchBar activate:NO];
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    self.tableData=[[[appDele dataConnecter] studioArray] copy];
    [tableData release];
    tableData=[self.studioArray copy];
    [self.theTableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // Do the search and show the results in tableview
    // Deactivate the UISearchBar
	
    // You'll probably want to do this on another thread
    // SomeService is just a dummy class representing some 
    // api that you are using to do the search
    
//
//    if([searchText isEqualToString:@""]searchText==nil){
//        [myTableView reloadData];
//        return;
//    }
    NSMutableArray* results=[[NSMutableArray alloc] init ];
    NSInteger counter = 0;
    for(OPSStudio *studio in tableData)
    {
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
        NSRange r = [[studio.name lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
        if(r.location != NSNotFound)
            [results addObject:studio];
        counter++;
        [pool release];
    }
    
//    NSArray *results = [SomeService doSearch:searchBar.text];
	
    [self searchBar:searchBar activate:NO];
//	[tableData release];
//    
//    tableData=[[NSMutableArray alloc] init ];
    NSMutableArray* _tableData=[[NSMutableArray alloc] init ];
    //	[tableData release];    
    self.tableData=_tableData;
    [_tableData release];    
//    [tableData removeAllObjects];
    [self.tableData addObjectsFromArray:results];
    [self.theTableView reloadData];
    [results release];
}

// We call this when we want to activate/deactivate the UISearchBar
// Depending on active (YES/NO) we disable/enable selection and 
// scrolling on the UITableView
// Show/Hide the UISearchBar Cancel button
// Fade the screen In/Out with the disableViewOverlay and 
// simple Animations

//- (BOOL)resignFirstResponder{
//
//    return [super resignFirstResponder];
//}

//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    [textField resignFirstResponder];        
//    [disableViewOverlay removeFromSuperview];
//}


- (void)searchBar:(UISearchBar *)searchBar activate:(BOOL) active{	
    self.theTableView.allowsSelection = !active;
    self.theTableView.scrollEnabled = !active;
    if (!active) {
        [disableViewOverlay removeFromSuperview];
        [searchBar resignFirstResponder];
    } else {
        self.disableViewOverlay.alpha = 0;
        [self.view addSubview:self.disableViewOverlay];
		
        [UIView beginAnimations:@"FadeIn" context:nil];
        [UIView setAnimationDuration:0.5];
        self.disableViewOverlay.alpha = 0.6;
        [UIView commitAnimations];
		
        // probably not needed if you have a details view since you 
        // will go there on selection
        NSIndexPath *selected = [self.theTableView 
                                 indexPathForSelectedRow];
        if (selected) {
            [self.theTableView deselectRowAtIndexPath:selected 
                                             animated:NO];
        }
    }
    [searchBar setShowsCancelButton:active animated:YES];
}


#pragma mark -
#pragma mark UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {    
    return [tableData count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    uint MAINLABEL_TAG=1;
//    uint SECONDLABEL_TAG=2;
    uint PHOTO_TAG=3;
    static NSString *CellIdentifier = @"LiveStudioListCell";
    
    UILabel *mainLabel;//, *secondLabel;
    UIImageView *photo;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(243.0, 0.0, 979, 45.0)] ;
        mainLabel.tag = MAINLABEL_TAG;
        mainLabel.font = [UIFont systemFontOfSize:30.0];
        //        mainLabel.textAlignment = UITextAlignmentRight;
        mainLabel.textAlignment = NSTextAlignmentLeft;        
        mainLabel.textColor = [UIColor blackColor];
        mainLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        cell.selectionStyle=UITableViewCellSelectionStyleBlue;
        [cell.contentView addSubview:mainLabel];
        [mainLabel release];
    
        
        photo = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 233.0, 45.0)] ;
        photo.tag = PHOTO_TAG;
        photo.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        [cell.contentView addSubview:photo];
        [photo release];
    } else {
        mainLabel = (UILabel *)[cell.contentView viewWithTag:MAINLABEL_TAG];
        photo = (UIImageView *)[cell.contentView viewWithTag:PHOTO_TAG];
    }
    
    //    NSDictionary *aDict = [self.list objectAtIndex:indexPath.row];        
    
    OPSStudio *studio = [self.tableData objectAtIndex:indexPath.row];
//    mainLabel.text = [NSString stringWithFormat:@"S%d - %@",[studio ID],studio.name];
    mainLabel.text = [NSString stringWithFormat:@"%@",studio.name];
    
    

    NSString *imagePath =[[NSBundle mainBundle] pathForResource:[studio iconName] ofType:@"png"];//[[NSBundle mainBundle] pathForResource:[aDict objectForKey:@"imageKey"] ofType:@"png"];
//    NSLog(@"iconName: %@",(studio.iconName));
    UIImage *theImage = [UIImage imageWithContentsOfFile:imagePath];
    photo.image = theImage;
    
    return cell;
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"SearchResult";
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:MyIdentifier];
	
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] 
                 initWithStyle:UITableViewCellStyleDefault 
                 reuseIdentifier:MyIdentifier] autorelease];
    }
	
    OPSStudio *studio = [self.tableData objectAtIndex:indexPath.row];
    cell.textLabel.text = studio.name;
    return cell;
}
*/
/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    // Navigation logic may go here. Create and push another view controller.                
    
    int row=[indexPath row];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];     
    
    OPSStudio* selectedStudio=[tableData objectAtIndex:row];
    [appDele setActiveStudio:selectedStudio];
    

    LiveProjectListVC* liveProjectTableListVC=[[LiveProjectListVC alloc] init];
    NSString* liveProjectTitleStr=[[NSString alloc] initWithFormat: @"{%@}'s live projects in Studio: %@",[appDele defUserName] , selectedStudio.name ];
    liveProjectTableListVC.title=liveProjectTitleStr;
    
    
    [self.navigationController pushViewController:liveProjectTableListVC animated:NO];      

    [liveProjectTableListVC release];        
    
}
*/

#pragma mark -
#pragma mark Memory Management Methods

//-(BOOL) prefersStatusBarHidden{
//    return YES;
//}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];	
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    
//    NSLog(@"Dealloc 0_StudioArray RetainCount: %d",[studioArray retainCount]);
    [studioArray release], studioArray=nil;
//    NSLog(@"Dealloc 1_StudioArray RetainCount: %d",[studioArray retainCount]);
//    NSLog(@"1_StudioArray Count: %d",[studioArray counts]);
//    studioArray=nil;
//    NSLog(@"Dealloc 2_StudioArray RetainCount: %d",[studioArray retainCount]);
    
    [theTableView release], theTableView = nil;
    [theSearchBar release], theSearchBar = nil;
    
//    NSLog(@"Dealloc 0_tableData RetainCount: %d",[tableData retainCount]);
    [tableData release];
//    NSLog(@"Dealloc 1_tableData RetainCount: %d",[tableData retainCount]);
    tableData=nil;
//    NSLog(@"2_tableData RetainCount: %d",[tableData retainCount]);
    [disableViewOverlay release], disableViewOverlay=nil;
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


/*

- (BOOL) readSiteListFromLocal{        
    //    int errorCode=0;
    
    self.projectArray=[[NSMutableArray alloc] init];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; 
    
    documentsDirectory=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",userName]]; 	    
    
    
    
    
    //    NSMutableArray* studioFolder=[DataConnect arrayOfFoldersInFolder:documentsDirectory];
    
    
    //    NSString *bundleRoot = [[NSBundle mainBundle] resourcePath];        
    NSFileManager *manager = [NSFileManager defaultManager];    
    //    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:bundleRoot];
    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:documentsDirectory];    
    
    NSString *filename;
    
    int pProjectID=0;
    while ((filename = [direnum nextObject] )) {
        

        
        
        
        
        if ([filename hasSuffix:@".sqlite"]) {   //change the suffix to what you are looking for
            
            NSMutableString *filenameOnly=[NSMutableString stringWithString:filename];
            [filenameOnly replaceCharactersInRange: [filenameOnly rangeOfString: @".sqlite"] withString: @""];
            
            //            [filenameOnly replaceCharactersInRange: [filenameOnly rangeOfString: [NSString stringWithFormat:@"%@_-_",userName] ] withString: @""];
            
            // Do work here            
            uint projectSiteShared=0;
            uint projectSiteProjectID=0;
            NSString* projectSiteProjectName=nil;
            NSString* projectSiteThumnailName=nil;
            
            OPSProjectSite* projectSite= [[OPSProjectSite alloc] init:pProjectID name:filenameOnly shared:projectSiteShared projectID:projectSiteProjectID projectName:projectSiteProjectName iconName:projectSiteThumnailName];
            [projectSite setDbPath: [documentsDirectory stringByAppendingPathComponent:filename ] ];
            [projectArray addObject:projectSite];
            [projectSite release];     
            
        }        
    }    
    if (projectArray==Nil || [projectArray count]<=0 ) {
        return false;        
    }else{
        return true;
    }
    
    
}
 */


 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
     [super loadView];
     
     
     [self setCustomNavigationBarButtons];
//     ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
     

     
     
//     if (appDele.projectViewTabBarCtr.selectedIndex==0&&  ![appDele isLiveDataSourceAvailable]){           
//         if ([self tableData]==Nil){
//             
////         }
////         if ([[appeDel dataConnecter] readSiteListFromLocal]){            
////             //        int errorCode=[[appDel dataConnecter] readProjectListFromServer];
////             //        if (errorCode!=0) { return; }
////         }else{        
//             ViewNoLocalDB* viewNoLocalDB=[[ViewNoLocalDB alloc] initWithNibName:@"ViewNoLocalDB" bundle:[NSBundle mainBundle]];                
//             [[self navigationController] pushViewController:viewNoLocalDB animated:YES];
//             [viewNoLocalDB release];                
//             //        [self.view  addSubview:viewNeedWebForLiveDB.view];        
//             //        [viewNeedWebForLiveDB release];       #Learnt: If add this line, any button event will crash the app. 
//         }       
////         [self.tableView reloadData];         
//     }

         
     
     //     <string>UIInterfaceOrientationPortrait</string>
     //     <string>UIInterfaceOrientationPortraitUpsideDown</string>
     //     <string>UIInterfaceOrientationLandscapeLeft</string>
     //     <string>UIInterfaceOrientationLandscapeRight</string>
     
    CGRect frame=CGRectMake(0, 0, 768, 1024);         
     if ([UIApplication sharedApplication].statusBarOrientation==UIInterfaceOrientationLandscapeLeft ||
         [UIApplication sharedApplication].statusBarOrientation==UIInterfaceOrientationLandscapeRight ){
         frame=CGRectMake(0, 0, 1024, 768);
     }
     
     
     
     
//         CGRect frame = [[UIScreen mainScreen] bounds];
     // Use the max dimension for width and height
//         if (frame.size.width > frame.size.height)
//             frame.size.height = frame.size.width;
//         else
//             frame.size.width = frame.size.height;
     
     frame.origin.y = 44.0f; // Offset the UISearchBar
//     UIView* _disableViewOverlay=[[UIView alloc] initWithFrame:frame];
//     self.disableViewOverlay = _disableViewOverlay;
//     [_disableViewOverlay release];

     
     UISearchBar* _searchBar=[[UISearchBar alloc]initWithFrame:CGRectMake(0,0,frame.size.width,44.0f)];
     
     self.theSearchBar=_searchBar;
     [_searchBar release];
     theSearchBar.delegate = self;
     [self.view addSubview:theSearchBar];
     
     UITableView* _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 44.0f, frame.size.width, frame.size.height-44.0f-44.0f-49.0f-20.0f)];          
     self.theTableView =_tableView;
     [_tableView release];
     theTableView.delegate = self;
     theTableView.dataSource = self;
     [self.view addSubview:theTableView];
     
     
//     if (appDele.projectViewTabBarCtr.selectedIndex==0){
//         [self readStudioListFromLocal];         
//         self.tableData=[studioArray copy];         
//     }else if (appDele.projectViewTabBarCtr.selectedIndex==1){
//         [[appDele dataConnecter] readStudioListFromServer];         
//         self.tableData=[[[appDele dataConnecter] studioArray] copy];
//     }
     
     
//     NSMutableArray* _tableData=[[NSMutableArray alloc]init];
//     self.tableData =_tableData;
//     [_tableData release];
     UIView* _disableViewOverlay=[[UIView alloc]
                                  initWithFrame:CGRectMake(0.0f,44.0f,frame.size.width,frame.size.height-44.0f)];
     self.disableViewOverlay = _disableViewOverlay;
     [_disableViewOverlay release];
     self.disableViewOverlay.backgroundColor=[UIColor blackColor];
     self.disableViewOverlay.alpha = 0;
     
     

     
     
//         NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:3];         
//         UIBarButtonItem * liveStudioBackButton = [[UIBarButtonItem alloc]
//                                 initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];         
//    UIBarButtonItem *liveStudioBackButton  = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(liveStudioBackButtonResond:)];
//         liveStudioBackButton.style = UIBarButtonItemStyleBordered;                
//         self.navigationItem.leftBarButtonItem = liveStudioBackButton;
//         [liveStudioBackButton release];
//             

     
     UIImage *buttonImage=nil;
     UIImage *buttonImageHighlight=nil;
     UIButton *button=nil;
     UIBarButtonItem *bi=nil;
     
     NSMutableArray* leftButtons = [[NSMutableArray alloc] initWithCapacity:1];
     

     buttonImage = [UIImage imageNamed:@"Logout.png"];   
     buttonImageHighlight = [UIImage imageNamed:@"LogoutHighlight.png"];        
     button = [UIButton buttonWithType:UIButtonTypeCustom];
     [button setImage:buttonImage forState:UIControlStateNormal];
     [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
     button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
     [button addTarget:self action:@selector(liveStudioBackButtonResond:) forControlEvents:UIControlEventTouchUpInside];
     bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
     //        [button release];
     [leftButtons addObject:bi];
     [bi release];  
     
     self.navigationItem.leftBarButtonItems = leftButtons;
     [leftButtons release];
         
     
     //initialize the two arrays; dataSource will be initialized and populated by appDelegate
//     searchedData = [[NSMutableArray alloc]init];
//     tableData = [[NSMutableArray alloc]init];
//     [tableData addObjectsFromArray:dataSource];//on launch it should display all the records      
 }


/*
 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad
 {
 [super viewDidLoad];
 }
 */


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));    
    // Return YES for supported orientations

}

@end




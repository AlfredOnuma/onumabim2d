//
//  BIMMailRecipient.h
//  ProjectView
//
//  Created by Alfred Man on 4/7/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//


@interface BIMMailRecipient : NSObject{
    uint ID;    
    NSString* name;
    NSString* role;
    NSString* email;
    bool checked;
}
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* role;
@property (nonatomic, assign) uint ID;
@property (nonatomic, retain) NSString* email;
@property (nonatomic, assign) bool checked;
-initWithID:(uint) _ID name:(NSString*) _name role:(NSString*)_role email:(NSString*)_email checked:(bool)_checked; 
@end

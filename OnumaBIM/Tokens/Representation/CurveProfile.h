//
//  CurveProfile.h
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RepresentationItem.h"
#import "CPoint.h"
#import "BBox.h"
#import "ArcProfile.h"


@interface CurveProfile : RepresentationItem {
    NSMutableArray *_aPoint;
    bool _isAntiClock;
    NSMutableArray* _aArcProfile;

}
@property (nonatomic, retain) NSMutableArray *aPoint;
@property (nonatomic, retain) NSMutableArray *aArcProfile; 
@property (nonatomic, assign) bool isAntiClock;

//@property (nonatomic, assign) double area;
- (id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName pointArray:(NSMutableArray*) newAPoints;

- (id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName;
//- (void) addPoint:(CPoint*) pt;
- (void) addPointX:(CGFloat) x y:(CGFloat) y;
- (void) addArcProfileWithEndPointIndex:(uint)endPointIndex centerX:(double)centerX centerY:(double)centerY bIsAntiClock:(bool)bIsAntiClock arcBoundingPt0:(CPoint*)boundingPt0 arcBoundingPt1:(CPoint*)boundingPt1;
- (void) calculateAreaAndCheckAntiClock;


-(id)copyWithZone:(NSZone *)zone;
//-(void) reCalculateBBox;
//-(void) applyMatrix:(CMatrix*) matrix;
@end

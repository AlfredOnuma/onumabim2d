//
//  CurveProfile.m
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "CurveProfile.h"
#import "CVector.h"

@implementation CurveProfile

@synthesize aPoint=_aPoint;

@synthesize aArcProfile=_aArcProfile;
//@synthesize area;
@synthesize isAntiClock;
- (id) init{
    self= [super init];                
    if (self){
        /*
        [ aPoint release];
        aPoint = [ [NSMutableArray alloc] init];
//        [aPoint autorelease];        
         */
//        aPoint=nil;
        isAntiClock=false;

    }else{

    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone
{
    NSMutableArray *aNewPoint =nil;
    NSMutableArray *aNewArcProfile =nil;

    
    CurveProfile* newCurveProfile=[[CurveProfile alloc] init:self.ID  guid:self.GUID name:self.name] ;
    if (self.aPoint!=nil){                
        aNewPoint=[NSMutableArray array];
        for (CPoint* pt in self.aPoint){
            CPoint* newPt=[[CPoint alloc] initWithX:pt.x y:pt.y];            
            [aNewPoint addObject:newPt];
            [newPt release];
        }        
        [newCurveProfile setAPoint:aNewPoint];

    }
    if (self.aArcProfile!=nil){
        aNewArcProfile=[NSMutableArray array];
        for (ArcProfile* arcProfile in self.aArcProfile){
            ArcProfile* newArcProfile=[[ArcProfile alloc] init:arcProfile.ID guid:arcProfile.GUID name:arcProfile.name endPointIndex:arcProfile.endPointIndex centerX:arcProfile.centerX centerY:arcProfile.centerY bAntiClock:arcProfile.bAntiClock];
            [aNewArcProfile addObject:newArcProfile];
            [newArcProfile release];
        }
        [newCurveProfile setAArcProfile:aNewArcProfile];
        
    }
    BBox* selfBBox=[self.bBox copy];
    newCurveProfile.bBox=selfBBox;
    [selfBBox release];
    newCurveProfile.area=self.area;
    newCurveProfile.isAntiClock=self.isAntiClock;    
    return newCurveProfile;
}
- (void) calculateAreaAndCheckAntiClock{
    self.area=0;
//    CPoint* thisPt=[self.aPoint objectAtIndex:0];
    CPoint* prevPt=[self.aPoint lastObject];
    
    for (CPoint* thisPt in self.aPoint){
        self.area+=prevPt.x*thisPt.y-thisPt.x*prevPt.y;
        prevPt=thisPt;
    }
    self.area/=2;
    self.isAntiClock=(self.area>0);
    self.area=fabs(self.area);
}

//- (void) addPoint:(CPoint*) pt{

- (void) addArcProfileWithEndPointIndex:(uint)endPointIndex centerX:(double)centerX centerY:(double)centerY bIsAntiClock:(bool)bIsAntiClock arcBoundingPt0:(CPoint*)boundingPt0 arcBoundingPt1:(CPoint*)boundingPt1{
    if (self.aArcProfile==nil){
        NSMutableArray* aArcProfile=[[NSMutableArray alloc]init];
        self.aArcProfile=aArcProfile;
        [aArcProfile release];
    }
    ArcProfile* arcProfile=[[ArcProfile alloc] init:0 guid:@"" name:@"" endPointIndex:endPointIndex centerX:centerX centerY:centerY bAntiClock:bIsAntiClock];
    [self.aArcProfile addObject:arcProfile];
    
    if (self.bBox==nil){
        BBox* _bBox=[[BBox alloc] init];
        self.bBox=_bBox;
        [_bBox release];
        //        [self.bBox retain];
    }
    //IMPORTANT: MUST UPDATE
    uint prevPointIndex=[self.aPoint count]-1;
    if (endPointIndex==[self.aPoint count]){
        endPointIndex=0;
    }
    if (endPointIndex>0){
        prevPointIndex=endPointIndex-1;
    }
    
    CPoint* prevPoint=[[self aPoint] objectAtIndex:prevPointIndex];
    CPoint* endPoint=[[self aPoint] objectAtIndex:endPointIndex];
    CVector* centerVect=[[CVector alloc]initWithX:centerX y:centerY];
    
    CVector* arcV0V1=[[CVector alloc]initWithX:(endPoint.x-prevPoint.x) y:(endPoint.y-prevPoint.y)];
    

    CVector* arcV0V1Perp=[[CVector alloc]init];
    [arcV0V1 perp:arcV0V1Perp];
    
    CVector* centerV1=[[CVector alloc] initWithX:(endPoint.x-centerX) y:(endPoint.y-centerY)];
    
    double rad=[centerV1 norm];
    
    
    CVector* arcV0v1PerpUnit=[[CVector alloc]init];
    [arcV0V1Perp unitVectorWithResultUnitVector:arcV0v1PerpUnit];
    
    if (bIsAntiClock){
        [arcV0v1PerpUnit scalar:-1];
    }

    
    
    
    CVector* vertexMid=[arcV0v1PerpUnit copy];
    [vertexMid scalar:rad];
    
//    [vertexMid addsVector:centerVect];
    
    
    if ([arcProfile bAntiClock] != [self isAntiClock]){
        //arc will go 'inward'        
//        [vertexMid addsVector:centerVect];
    }else{
        //arc will go 'outward'
        
        CVector* vertex1=[arcV0v1PerpUnit copy];
        [vertex1 scalar:rad];
        CVector* vertex0=[arcV0v1PerpUnit copy];
        [vertex0 scalar:-1*rad];
        
        
        [vertex0 addsVector:centerVect];
        [vertex0 addsVector:vertexMid];
        
        [vertex1 addsVector:centerVect];
        [vertex1 addsVector:vertexMid];
        
        
        if (self.bBox==nil){
            BBox* _bBox=[[BBox alloc] init];
            self.bBox=_bBox;
            [_bBox release];
            //        [self.bBox retain];
        }
        CPoint* pt1=[[CPoint alloc] initWithX:vertex1.x y:vertex1.y];
        CPoint* pt0=[[CPoint alloc] initWithX:vertex0.x y:vertex0.y];
        
        boundingPt0.x=pt0.x;
        boundingPt0.y=pt0.y;
        
        boundingPt1.x=pt1.x;
        boundingPt1.y=pt1.y;
        
        [self.bBox updatePoint:(pt1)];
        [self.bBox updatePoint:(pt0)];
        
        
        [pt1 release];pt1=nil;
        [pt0 release];pt0=nil;
        
        
        [vertex0 release];vertex0=nil;
        [vertex1 release];vertex1=nil;

        
    }
    


    
//        [centerVect unitVectorWithResultUnitVector:vertexL];
//        [centerVect unitVectorWithResultUnitVector:vertexR];
    
//        [vertexL scalar:￼]
    


    
    
    
    [vertexMid release];vertexMid=nil;
    [centerVect release];centerVect=nil;
    [arcV0v1PerpUnit release];arcV0v1PerpUnit=nil;
    [centerV1 release];centerV1=nil;
    [arcV0V1 release];arcV0V1=nil;
    [arcV0V1Perp release];arcV0V1Perp=nil;
    

//    CPoint* outerPoint=
//    [self.bBox updatePoint:(outerPoint)];
    [arcProfile release];
    
}
- (void) addPointX:(CGFloat)x y:(CGFloat) y{
    if (self.aPoint==nil){    
        NSMutableArray* aPoint=[[NSMutableArray alloc] init];
        self.aPoint=aPoint;
        [aPoint release];
//        self.aPoint=[NSMutableArray array];
    }    
    CPoint* pt=[[CPoint alloc] initWithX:x y:y];
    [self.aPoint addObject:pt];

    if (self.bBox==nil){    
        BBox* _bBox=[[BBox alloc] init]; 
        self.bBox=_bBox;
        [_bBox release];
//        [self.bBox retain];
    }
    [self.bBox updatePoint:(pt)];  
    [pt release];

}

- (id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName pointArray:(NSMutableArray*) newAPoints{

    
    self = [super init:newID guid:newGUID name:newName];
    if (self){
//        [aPoint release];
//        [newAPoints retain];
//        aPoint=newAPoints;
        
        [self setAPoint:newAPoints];  
     
    }
    return self;
    
}


- (id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName{    
    self= [super init:newID guid:newGUID name:newName];
    if (self){
//        [self setAPoint:newAPoints];  

    }else{

    }
    return self;
}

//-(void) reCalculateBBox{
//    [self.bBox release];
//    self.bBox=nil;
//    self.bBox=[[BBox alloc] init];
//    for (CPoint* pt in self.aPoint){
//        [self.bBox updatePoint:pt];        
//    }
//}
//
//-(void) applyMatrix:(CMatrix*) matrix{    
//    for (CPoint* pt in self.aPoint){
//        [matrix applyPoint:pt];
//    }
//
//}


- (void)dealloc
{
//    [aPoint release];
//    for (CPoint* pt in aPoint){
//        [pt release];
//    }
    [_aPoint release]; _aPoint=nil;
    [_aArcProfile release]; _aArcProfile=nil;
    [super dealloc];
}

@end

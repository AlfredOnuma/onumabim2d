//
//  RepresentationItem.h
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OPSToken.h"
@class BBox;
@class Representation;
@interface RepresentationItem : OPSToken {
    
    BBox* bBox;
    CGFloat area;
    CGRect bBoxInRootContext;
    CGRect bBoxInLocalFrame;
    
    Representation* representation;
    
@private
    bool bBoxInRootContextDefined;
    bool bBoxInLocalFrameDefined;
}
@property (nonatomic, assign) bool bBoxInRootContextDefined;
@property (nonatomic, assign) bool bBoxInLocalFrameDefined;
@property (nonatomic, assign) CGRect bBoxInRootContext;
@property (nonatomic, assign) CGRect bBoxInLocalFrame;
@property (nonatomic, assign) Representation* representation;
@property (nonatomic, retain) BBox* bBox;
@property CGFloat area;
//-(BBox*) findBBoxOfAppliedMatrix;


//-(CGRect) bBoxInRootContex;
-(void) setBBoxInLocalFrame:(CGRect)_bBoxInLocalFrame;
-(void) setBBoxInRootContext:(CGRect)_bBoxInRootContext;
-(void) updateBBoxInRootContext:(CGPoint) point;
-(void) updateBBoxinLocalFrame:(CGPoint) point;
@end

//
//  Representation.m
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//


#import "OPSToken.h"
#import "BBox.h"
#import "RepresentationItem.h"
#import "Placement.h"


#import "Representation.h"
#import "OPSProduct.h"

#import "ExtrudedAreaSolid.h"
#import "CurveProfile.h"
#import "CPoint.h"

@implementation Representation
@synthesize aRepresentationItem;
@synthesize bBox;
@synthesize area;
@synthesize product;
@synthesize currentRepItemIndex;
//@synthesize optimiseOffset;

-(RepresentationItem*) currentRepItem{
    if (self.aRepresentationItem!=nil && [self.aRepresentationItem count]>0){
        return [self.aRepresentationItem  objectAtIndex: self.currentRepItemIndex];
    }else{
        return nil;
    }
}

- (id) init{
    self=[super init];
    if (self){
        self.currentRepItemIndex=0;
//        aRepresentationItem = nil;// [ [NSMutableArray alloc] init];
//        [aRepresentationItem autorelease];        
    }
    return self;
}


-(id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName representationItemArray:(NSMutableArray*) _aRepresentationItem
{    
    self=[super init:newID guid:newGUID name:newName];
    if (self){        
        [self setARepresentationItem:(_aRepresentationItem)];
        self.currentRepItemIndex=0;
//        self.optimiseOffset=Nil;
    }else{

    }        
    return self;
}


-(void) addARepresentationItem:(RepresentationItem*) repItem{
    [repItem setRepresentation:self];
    if (aRepresentationItem==nil){        
//        self.aRepresentationItem=[NSMutableArray array];
        [aRepresentationItem release];
        NSMutableArray* _aRepresentationItem=[ [NSMutableArray alloc] init];
        self.aRepresentationItem = _aRepresentationItem;
        [_aRepresentationItem release];
    }
    [aRepresentationItem addObject:repItem];
        
    if (self.bBox==nil){        
        BBox* _bBox=[[BBox alloc] init] ; 
        self.bBox=_bBox;
        [_bBox release];
    }
    if (repItem.bBox!=nil){
        [self.bBox mergeBBox:(repItem.bBox)];  
    }
    area=area+repItem.area;
//    //Not efficient , come back later
//    [self optimise];
    return;
}

- (void)dealloc
{
    [aRepresentationItem release];
    [bBox release];
//    [self.optimiseOffset release];
    [super dealloc];
}


@end

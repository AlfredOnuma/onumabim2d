//
//  Placement.m
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "Placement.h"
#import "ProjectViewAppDelegate.h"
#import "ViewModelVC.h"
#import "OPSModel.h"

@implementation Placement
@synthesize pt;
@synthesize angle; //in degree
@synthesize product;
@synthesize mirrorY;


-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    Placement* newPlacement=[[Placement alloc] init:self.ID guid:self.GUID name:self.name pt:(CGPointMake(pt.x, pt.y)) angle:angle isMirrorY:mirrorY] ;
    return newPlacement;
}



- (CGAffineTransform) toScreenScaledCGAffineTransform{ 
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
//    ViewModelVC* viewModelVC=[appDele activeViewModelVC];  
    OPSModel* model=[[self product] model];
    double modelScaleFactor=[model modelScaleForScreenFactor];    
    CGAffineTransform placementTranslate=CGAffineTransformMakeTranslation(pt.x*modelScaleFactor,pt.y*modelScaleFactor);
    CGAffineTransform placementRotate=CGAffineTransformRotate(placementTranslate, angle*M_PI/180.0);
    CGAffineTransform placementScale=CGAffineTransformScale (placementRotate,mirrorY?-1.0:1.0,1.0);   
    return placementScale;    
}

- (CGAffineTransform) toCGAffineTransform{ 
    CGAffineTransform placementTranslate=CGAffineTransformMakeTranslation(pt.x,pt.y);
    CGAffineTransform placementRotate=CGAffineTransformRotate(placementTranslate, angle*M_PI/180.0);
    CGAffineTransform placementScale=CGAffineTransformScale (placementRotate,mirrorY?-1.0:1.0,1.0);   
    return placementScale;
}

- (id) init:(NSInteger) newID guid:(NSString*) newGUID name:(NSString*) newName pt: (CGPoint)newPt angle:(CGFloat) newAngle isMirrorY:(bool) isMirrorY{
    //    if ([super init]){  
    self =[super init:newID guid:newGUID name:newName];
    if (self){
        [self setPt:(newPt)];
        [self setAngle:(newAngle)];
        [self setMirrorY:isMirrorY];
    }
    return self;
}
/*
-(void)setPt:(CPoint *)newPt
{
    [pt release];
//    pt=[[CPoint alloc] init];
    pt=[newPt retain];
}
-(CPoint*) pt{
    return pt;
}
 */

- (void) dealloc {	
//	[pt release];
	[super dealloc];
}
@end

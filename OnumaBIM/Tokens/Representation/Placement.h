//
//  Placement.h
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "OPSToken.h"
#import "CPoint.h"


@class OPSProduct;

@interface Placement : OPSToken {
    CGPoint pt;
    CGFloat angle;
    bool mirrorY;    
    OPSProduct* product;
}

@property (nonatomic, assign) CGPoint pt;
@property (nonatomic, assign) CGFloat angle;
@property (nonatomic, assign) bool mirrorY;
@property (nonatomic, assign) OPSProduct* product;
- (CGAffineTransform) toScreenScaledCGAffineTransform;
- (CGAffineTransform) toCGAffineTransform;
- (id) init:(NSInteger) newID guid:(NSString*) newGUID name:(NSString*) newName pt: (CGPoint)newPt angle:(CGFloat) newAngle isMirrorY:(bool) isMirrorY;

/*
//Static methods.
+ (void) getInitialDataToDisplay:(NSString *)dbPath;
+ (void) finalizeStatements;

//Instance methods.
- (id) initWithPrimaryKey:(NSInteger)pk;

*/

@end

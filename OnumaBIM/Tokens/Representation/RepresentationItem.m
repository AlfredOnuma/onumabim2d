//
//  RepresentationItem.m
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "RepresentationItem.h"

#import "BBox.h"
@implementation RepresentationItem

@synthesize bBox;
@synthesize area;
@synthesize bBoxInRootContext;
@synthesize bBoxInLocalFrame;
@synthesize bBoxInRootContextDefined;
@synthesize bBoxInLocalFrameDefined;
@synthesize representation;

- (void) setBBoxInLocalFrame:(CGRect)_bBoxInLocalFrame{
    bBoxInLocalFrame=_bBoxInLocalFrame;    
    //    [super setBBoxInRootContext:_bBoxInRootContext];
    bBoxInLocalFrameDefined=true;
    return;
}

- (void) setBBoxInRootContext:(CGRect)_bBoxInRootContext{
    bBoxInRootContext=_bBoxInRootContext;    
//    [super setBBoxInRootContext:_bBoxInRootContext];
    bBoxInRootContextDefined=true;
    return;
}
//-(CGRect) bBoxInRootContext{
//    return bBoxInRootContext;
//}
- (id) init{
    self=[super init];
    if (self){
        area=0.0;
        bBoxInRootContextDefined=false;
    }
    return self;
}

- (id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName{
    self = [super init:newID guid:newGUID name:newName];
    if (self){        
        area=0.0;
        bBoxInRootContextDefined=false;                
    }
    return self;
}

-(void) updateBBoxInRootContext:(CGPoint) point{    
    if (self.bBoxInRootContextDefined){
        self.bBoxInRootContext=CGRectUnion(CGRectMake(point.x, point.y, 0.0, 0.0), self.bBoxInRootContext);
     
    }else{
        self.bBoxInRootContext=CGRectMake( point.x, point.y, 0.0,0.0);        
        [self setBBoxInRootContextDefined:true];   
    }
}

-(void) updateBBoxinLocalFrame:(CGPoint) point{    
    if (self.bBoxInLocalFrameDefined){
        self.bBoxInLocalFrame=CGRectUnion(CGRectMake(point.x, point.y, 0.0, 0.0), self.bBoxInLocalFrame);
        
    }else{
        self.bBoxInLocalFrame=CGRectMake( point.x, point.y, 0.0,0.0);        
        [self setBBoxInLocalFrameDefined:true];   
    }
}

- (void)dealloc
{
    //    [aRepresentationItem release];
    [bBox release];
    [super dealloc];
}


@end

//
//  ExtrudedAreaSolid.h
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <sqlite3.h>
#import <Foundation/Foundation.h>

#import "CurveProfile.h"
@class RepresentationItem;
@interface ExtrudedAreaSolid : RepresentationItem {    
    NSMutableArray *aCurveProfile;
    
}
@property (nonatomic, retain) NSMutableArray *aCurveProfile;


-(id) initWithPolyStr:(NSString*) polyStr arcStr:(NSString*) arcStr parentTransform:(CGAffineTransform) parentTransform  localFrameTransform:(CGAffineTransform) localFrameTransform;
-(id) initWithPolyStr:(NSString*) polyStr parentTransform:(CGAffineTransform) parentTransform  localFrameTransform:(CGAffineTransform) localFrameTransform;
//-(id) initWithExtrudedDatabase: (sqlite3*) database polylineID:(NSInteger) polylineID parentTransform:(CGAffineTransform) parentTransform;

-(id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName curveProfileArray:(NSMutableArray*) aCurveProfile;

//-(BBox*) findBBoxOfAppliedMatrix:(CMatrix*) matrix ;

- (void) addCurveProfile:(CurveProfile*) _curveProfile;

@end

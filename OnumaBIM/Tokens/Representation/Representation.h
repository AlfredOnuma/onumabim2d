//
//  Representation.h
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OPSToken.h"
@class BBox;
@class RepresentationItem;
@class Placement;
@class CPoint;

@class OPSProduct;
@class RepresentationItem;
@interface Representation : OPSToken {
    NSMutableArray *aRepresentationItem;
    BBox* bBox;    
    CGFloat area;    
    OPSProduct* product;
    uint currentRepItemIndex;

}
@property (nonatomic, assign) uint currentRepItemIndex;
@property (nonatomic, retain) BBox* bBox;
@property (nonatomic, assign) CGFloat area;
@property (nonatomic, assign) OPSProduct* product;

@property (nonatomic, retain) NSMutableArray *aRepresentationItem;

-(id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName representationItemArray:(NSMutableArray*) _aRepresentationItem;
-(RepresentationItem* ) currentRepItem;
-(void) addARepresentationItem:(RepresentationItem*) repItem;
//-(void) optimise: (Placement*) placement;
@end

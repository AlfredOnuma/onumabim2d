	//
//  ExtrudedAreaSolid.m
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "ExtrudedAreaSolid.h"
//#import "CMatrix.h"

#import "BBox.h"
@implementation ExtrudedAreaSolid
@synthesize aCurveProfile;

//-(BBox*) findBBoxOfAppliedMatrix:(CMatrix*) matrix {
//    if (aCurveProfile==nil || [aCurveProfile count]<=0){return nil;}
//    CurveProfile* tmpCurveProfile= [[aCurveProfile objectAtIndex:0] copy];
//    [tmpCurveProfile applyMatrix:matrix];
//    BBox* returnBBox=[[tmpCurveProfile bBox] copy];    
//    [tmpCurveProfile release];
//    return returnBBox;    
//}

- (id) init{    
    self = [super init];
    if (self){        
        area=0.0;
        aCurveProfile=nil;
    }    
    return self;
}

-(id) initWithPolyStr:(NSString*) polyStr arcStr:(NSString*)arcStr parentTransform:(CGAffineTransform) parentTransform localFrameTransform:(CGAffineTransform) localFrameTransform{
    self=[super init:0 guid:nil name:nil];
    if (self){
        //Getting Polyline Point Info
        
        CurveProfile* curveProfile=[[CurveProfile alloc] init:0  guid:nil name:nil];
        


        
        NSArray *components = [polyStr componentsSeparatedByString:@";"];
        for (NSString* component in components){
            NSArray *atom=[component componentsSeparatedByString:@","];
            if ([atom count]==2){
                double ptX= [((NSString*)[atom objectAtIndex:0]) doubleValue];//*multiFactor;
                double ptY=[((NSString*)[atom objectAtIndex:1]) doubleValue];//*multiFactor;
                //                CPoint* pt=[[CPoint alloc] setX:ptX setY:ptY];
                
                //              NSLog (@"Shape Point X:%f Y:%f",pt.x,pt.y);
                [curveProfile addPointX:ptX y:ptY];
                //                [curveProfile addPoint:(pt)];
                //                [pt release];
                
                //!''''''''Add this If Cause To Save Transforming each Point if no Rotation is in parent transformation''''''''''''''''''''''''''''
                if (parentTransform.b!=0 || parentTransform.c!=0){
                    //!------------------------------------------------------------------------------
                    CGPoint cgPt=CGPointMake(ptX, ptY);
                    CGPoint transformedToRootPt=CGPointApplyAffineTransform(cgPt, parentTransform);
                    CGPoint transformedToLocalFramePt=CGPointApplyAffineTransform(cgPt, localFrameTransform);
                    
                    
                    //            NSLog (@"Context Shape Point X:%f Y:%f",transformedPt.x,transformedPt.y);
                    
                    [self updateBBoxInRootContext:transformedToRootPt];
                    [self updateBBoxinLocalFrame:transformedToLocalFramePt];
                    //!''''''''Close this If Cause To Save Transforming each Point if no Rotation is in parent transformation''''''''''''''''''''''''''''
                }
                
            }
        }
        
        //has to calculate the curveprofile anticlock or clock before adding arc
        [curveProfile calculateAreaAndCheckAntiClock];
        
        
        if (arcStr!=nil){
            NSArray *arcComponents = [arcStr componentsSeparatedByString:@";"];
            for (NSString* arcComponent in arcComponents){
                NSArray *atom=[arcComponent componentsSeparatedByString:@","];
                if ([atom count]==4){
                    uint endPointIndex= [((NSString*)[atom objectAtIndex:0]) intValue];//*multiFactor;
                    double centerX=[((NSString*)[atom objectAtIndex:1]) doubleValue];//*multiFactor;
                    double centerY=[((NSString*)[atom objectAtIndex:2]) doubleValue];//*multiFactor;
                    bool bIsAntiClock= [((NSString*)[atom objectAtIndex:3]) intValue]==1;//*multiFactor;
                    //                CPoint* pt=[[CPoint alloc] setX:ptX setY:ptY];
                    
                    //              NSLog (@"Shape Point X:%f Y:%f",pt.x,pt.y);
                    CPoint* arcBoundingPt0=[[CPoint alloc] initWithX:0.0 y:0.0];
                    CPoint* arcBoundingPt1=[[CPoint alloc] initWithX:0.0 y:0.0];
                    [curveProfile addArcProfileWithEndPointIndex:endPointIndex centerX:centerX centerY:centerY bIsAntiClock:bIsAntiClock arcBoundingPt0:arcBoundingPt0 arcBoundingPt1:arcBoundingPt1];
                    
                    
                    //                [curveProfile addPoint:(pt)];
                    //                [pt release];
                    
                    //!''''''''Add this If Cause To Save Transforming each Point if no Rotation is in parent transformation''''''''''''''''''''''''''''
                    if (bIsAntiClock==curveProfile.isAntiClock){
                        if (parentTransform.b!=0 || parentTransform.c!=0){
                            //!------------------------------------------------------------------------------

                            CGPoint pt1=CGPointMake(arcBoundingPt1.x, arcBoundingPt1.y);
                            CGPoint transformedToRootPt1=CGPointApplyAffineTransform(pt1, parentTransform);
                            CGPoint transformedToLocalFramePt1=CGPointApplyAffineTransform(pt1, localFrameTransform);
                            
                            //            NSLog (@"Context Shape Point X:%f Y:%f",transformedPt.x,transformedPt.y);
                            
                            [self updateBBoxInRootContext:transformedToRootPt1];
                            [self updateBBoxinLocalFrame:transformedToLocalFramePt1];
                            
                            
                            CGPoint pt0=CGPointMake(arcBoundingPt0.x, arcBoundingPt0.y);
                            CGPoint transformedToRootPt0=CGPointApplyAffineTransform(pt0, parentTransform);
                            CGPoint transformedToLocalFramePt0=CGPointApplyAffineTransform(pt0, localFrameTransform);
                            
                            //            NSLog (@"Context Shape Point X:%f Y:%f",transformedPt.x,transformedPt.y);
                            
                            [self updateBBoxInRootContext:transformedToRootPt0];
                            [self updateBBoxinLocalFrame:transformedToLocalFramePt0];
                            //!''''''''Close this If Cause To Save Transforming each Point if no Rotation is in parent transformation''''''''''''''''''''''''''''
            //                }
                            
                        }
                    }
                    [arcBoundingPt0 release];arcBoundingPt0=nil;
                    [arcBoundingPt1 release];arcBoundingPt1=nil;
                }
            }
        }
        
        
                
        //!''''''''Now, not every point got updated if no Rotation is in parent matrix, instead, update the entire curveprofile bbox with the matrix
        if (parentTransform.b==0 && parentTransform.c==0){
            [self setBBoxInRootContext: CGRectApplyAffineTransform([curveProfile.bBox cgRect] , parentTransform)];
            
            [self setBBoxInLocalFrame: CGRectApplyAffineTransform([curveProfile.bBox cgRect] , localFrameTransform)];
        }
        //!------------------------------------------------------------------------------
        [self addCurveProfile:(curveProfile)];
        [curveProfile release];
    }
    return self;
}


-(id) initWithPolyStr:(NSString*) polyStr parentTransform:(CGAffineTransform) parentTransform localFrameTransform:(CGAffineTransform) localFrameTransform{
    self=[super init:0 guid:nil name:nil];
    if (self){        
        //Getting Polyline Point Info          
        
        CurveProfile* curveProfile=[[CurveProfile alloc] init:0  guid:nil name:nil]; 
        NSArray *components = [polyStr componentsSeparatedByString:@";"];
        for (NSString* component in components){
            NSArray *atom=[component componentsSeparatedByString:@","];
            if ([atom count]==2){
                double ptX= [((NSString*)[atom objectAtIndex:0]) doubleValue];//*multiFactor;
                double ptY=[((NSString*)[atom objectAtIndex:1]) doubleValue];//*multiFactor;                          
                //                CPoint* pt=[[CPoint alloc] setX:ptX setY:ptY];        
                
                //              NSLog (@"Shape Point X:%f Y:%f",pt.x,pt.y);
                [curveProfile addPointX:ptX y:ptY];
                //                [curveProfile addPoint:(pt)];             
                //                [pt release];
                
                //!''''''''Add this If Cause To Save Transforming each Point if no Rotation is in parent transformation''''''''''''''''''''''''''''
                if (parentTransform.b!=0 || parentTransform.c!=0){                       
                    //!------------------------------------------------------------------------------
                    CGPoint cgPt=CGPointMake(ptX, ptY);
                    CGPoint transformedToRootPt=CGPointApplyAffineTransform(cgPt, parentTransform);
                    CGPoint transformedToLocalFramePt=CGPointApplyAffineTransform(cgPt, localFrameTransform);                    
                    //            NSLog (@"Context Shape Point X:%f Y:%f",transformedPt.x,transformedPt.y);                    
                    [self updateBBoxInRootContext:transformedToRootPt];
                    [self updateBBoxinLocalFrame:transformedToLocalFramePt];
                    //!''''''''Close this If Cause To Save Transforming each Point if no Rotation is in parent transformation''''''''''''''''''''''''''''                    
                }
                
            }
        }  
        //!''''''''Now, not every point got updated if no Rotation is in parent matrix, instead, update the entire curveprofile bbox with the matrix
        if (parentTransform.b==0 && parentTransform.c==0){            
            [self setBBoxInRootContext: CGRectApplyAffineTransform([curveProfile.bBox cgRect] , parentTransform)];
        
            [self setBBoxInLocalFrame: CGRectApplyAffineTransform([curveProfile.bBox cgRect] , localFrameTransform)];        
        }        
        //!------------------------------------------------------------------------------
        [self addCurveProfile:(curveProfile)];
        [curveProfile release];
    }
    return self;    
}

/*
-(id) initWithExtrudedDatabase: (sqlite3*) database polylineID:(NSInteger) polylineID parentTransform:(CGAffineTransform) parentTransform{
    
    self=[super init:polylineID guid:nil name: nil];
    if (self){
    
        //Getting Polyline Point Info                
        const char* polylinePointSQL=[[NSString stringWithFormat:@"select x,y,z FROM polylinepoint WHERE polylineType='Outer'  AND polylineID=%d ORDER BY sequence", polylineID] UTF8String];

        sqlite3_stmt *polylinePointStmt;         
        //                NSMutableArray* aPoint=[NSMutableArray array];
        //                CGPathRef* polyPath=
        
        //                                    NSMutableArray* aPoint=[NSMutableArray array];  
        CurveProfile* curveProfile=[[CurveProfile alloc] init:0  guid:nil name:nil]; 
        
        
        if(sqlite3_prepare_v2(database, polylinePointSQL, -1, &polylinePointStmt, NULL) == SQLITE_OK) {                    
//            uint pPt=0;
            while(sqlite3_step(polylinePointStmt) == SQLITE_ROW) {                                                                 
                CGFloat ptX= sqlite3_column_double(polylinePointStmt, 0);// *multiFactor;
                CGFloat ptY= sqlite3_column_double(polylinePointStmt, 1);// *multiFactor;                          
//                CPoint* pt=[[CPoint alloc] setX:ptX setY:ptY];        
                
                //              NSLog (@"Shape Point X:%f Y:%f",pt.x,pt.y);
                [curveProfile addPointX:ptX y:ptY];
//                [curveProfile addPoint:(pt)];             
//                [pt release];
                
                //!''''''''Add this If Cause To Save Transforming each Point if no Rotation is in parent transformation''''''''''''''''''''''''''''
                if (parentTransform.b!=0 || parentTransform.c!=0){                       
                //!------------------------------------------------------------------------------
                    CGPoint cgPt=CGPointMake(ptX, ptY);
                    CGPoint transformedPt=CGPointApplyAffineTransform(cgPt, parentTransform);
                    
                    
                    //            NSLog (@"Context Shape Point X:%f Y:%f",transformedPt.x,transformedPt.y);

                    [self updateBBoxInRootContext:transformedPt];
                //!''''''''Close this If Cause To Save Transforming each Point if no Rotation is in parent transformation''''''''''''''''''''''''''''                    
                }
                
                //!------------------------------------------------------------------------------
                
            }
        }       
        //!''''''''Now, not every point got updated if no Rotation is in parent matrix, instead, update the entire curveprofile bbox with the matrix
        if (parentTransform.b==0 && parentTransform.c==0){            
            [self setBBoxInRootContext: CGRectApplyAffineTransform([curveProfile.bBox cgRect] , parentTransform)];
        }        
        //!------------------------------------------------------------------------------
        [self addCurveProfile:(curveProfile)];
        [curveProfile release];
    }
    return self;
}


*/

-(id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName curveProfileArray:(NSMutableArray*) _aCurveProfile
{
    self=[super init:newID guid:newGUID name:newName];
    if (self){
        [_aCurveProfile retain];
        [aCurveProfile release];
        aCurveProfile=_aCurveProfile;
//        [self setACurveProfile:(_aCurveProfile)];

    }else{

    }    
    return self;
}


- (void) addCurveProfile:(CurveProfile*) curveProfile{
    
    if (self.area==0.0){
        self.area+=curveProfile.area;
    }else{
        self.area-=curveProfile.area;
    }
    if (self.aCurveProfile==nil){        

        NSMutableArray* _aCurveProfile=[ [NSMutableArray alloc] init];
        self.aCurveProfile=_aCurveProfile;
        [_aCurveProfile release];
        
//        self.aCurveProfile=[NSMutableArray array];
    }
    if ([self.aCurveProfile count]==0){
        area=curveProfile.area;
    }else{
        area=area-curveProfile.area;
    }
    [self.aCurveProfile addObject:curveProfile];

    if (self.bBox==nil){        
        BBox* _bBox=[[BBox alloc]  init]; 
        self.bBox=_bBox;
        [_bBox release];
    }
    
    [self.bBox mergeBBox:(curveProfile.bBox)];    
}
- (void)dealloc
{
    [aCurveProfile release];
    [super dealloc];
}



@end

//
//  OPSObject.m
//  OPSMobile
//
//  Created by Alfred Man on 7/12/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "OPSObject.h"



@implementation OPSObject
@synthesize linkedRel;
@synthesize isGhostObject;

- (id) init:(NSInteger) newID guid:(NSString*) newGUID name:(NSString*) newName{
    self=[super init:newID guid:newGUID name:newName];
    if (self){
        self.isGhostObject=false;
    }    
    return self;
}
@end

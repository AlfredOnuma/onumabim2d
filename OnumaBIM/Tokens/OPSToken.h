//
//  OPSToken.h
//  OPSMobile
//
//  Created by Alfred Man on 7/12/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OPSToken : NSObject {
    
    NSInteger _ID;
    NSString  *_GUID;
    NSString  *_name;
}
@property (nonatomic) NSInteger ID;
@property (nonatomic, retain)  NSString *GUID;
@property (nonatomic, retain)  NSString *name;
- (id) init:(NSInteger) newID guid:(NSString*) newGUID name:(NSString*) newName;
@end

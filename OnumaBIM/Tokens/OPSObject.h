//
//  OPSObject.h
//  OPSMobile
//
//  Created by Alfred Man on 7/12/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OPSToken.h"
@class Relationship;
@interface OPSObject : OPSToken {
    bool isGhostObject;
    Relationship* linkedRel;
}
@property (nonatomic, assign) bool isGhostObject;
@property (nonatomic, assign) Relationship* linkedRel;
- (id) init:(NSInteger) newID guid:(NSString*) newGUID name:(NSString*) newName;
@end

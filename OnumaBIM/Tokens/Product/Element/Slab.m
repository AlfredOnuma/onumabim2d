//
//  Slab.m
//  OPSMobile
//
//  Created by Alfred Man on 9/5/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "Slab.h"

#import "ExtrudedAreaSolid.h"
#import "Representation.h"

@implementation Slab



-(Slab*) initWithSlabSQLStmt: (OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform{
    self.model=model;
    NSInteger slabID = sqlite3_column_int(sqlStmt, 0);
//    NSInteger slabPolylineID = sqlite3_column_int(sqlStmt, 1);     
    
    NSString* slabGuid= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 2)!=nil){
        slabGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 2)];               
    }else{
        slabGuid=[NSString stringWithFormat:@""];
    }                       
    
    self=[super init:slabID guid:slabGuid name:NULL];
    if (self){
        
        //                                        [slabGuid autorelease];                                              
//        CGFloat slabThk=sqlite3_column_double(sqlStmt, 3);
        CGFloat placementX = sqlite3_column_double(sqlStmt, 4);//*multiFactor;                           
        CGFloat placementY = sqlite3_column_double(sqlStmt, 5);//*multiFactor;                          
//        CGFloat placementZ = sqlite3_column_double(sqlStmt, 6);//*multiFactor;                          
        CGFloat placementAngle = sqlite3_column_double(sqlStmt, 7);         
        
        char* pPolyStr=(char *) sqlite3_column_text(sqlStmt, 8);
        char* pArcStr=(char *) sqlite3_column_text(sqlStmt, 9);

//        NSString* polyStr= [NSString stringWithUTF8String:(char *) sqlite3_column_text16(sqlStmt, 8)];              
        CGPoint placementPt=CGPointMake(placementX, placementY);     
        Placement* slabPlacement=[[Placement alloc] init:0 guid:nil name:nil pt:placementPt angle:placementAngle isMirrorY:false]; 
        
        [self setPlacement:slabPlacement]; 
        [slabPlacement release];
        
        //    NSLog(@"Slab -------------------------------");  
        //    NSLog(@"Slab Placement x:%f y:%f angle:%f",placementX,placementY,placementAngle);
        
        
        CGAffineTransform slabTransform=CGAffineTransformConcat([slabPlacement toCGAffineTransform], parentTransform);
        
        if (pPolyStr!=0){            
            NSString* polyStr= [NSString stringWithUTF8String:pPolyStr];
            
            NSString* arcStr=nil;
            if (pArcStr!=0){
                arcStr=[NSString stringWithUTF8String:pArcStr];
            }
            ExtrudedAreaSolid* extrudedAreaSolid=[[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr arcStr:arcStr parentTransform:slabTransform localFrameTransform:[placement toCGAffineTransform]];
//            ExtrudedAreaSolid* extrudedAreaSolid=[[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr parentTransform:slabTransform localFrameTransform:[placement toCGAffineTransform]];

//        ExtrudedAreaSolid* extrudedAreaSolid= [[ExtrudedAreaSolid alloc] initWithExtrudedDatabase:database polylineID:slabPolylineID parentTransform:slabTransform];
        
            Representation* rep=[[Representation alloc] init:(0) guid:(nil) name:(nil)];
            [rep addARepresentationItem:(extrudedAreaSolid)];                      
            [extrudedAreaSolid release];         
            [self setRepresentation:rep];  
            [rep release];            
        }



    }
    return self;
}


@end

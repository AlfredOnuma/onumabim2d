//
//  Element.h
//  OPSMobile
//
//  Created by Alfred Man on 9/5/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OPSProduct.h"

@interface Element : OPSProduct {
    
}

-(id) init: (OPSModel*) model ID:(NSInteger) ID guid:(NSString*) GUID name:(NSString*) name;
@end

//
//  Slab.h
//  OPSMobile
//
//  Created by Alfred Man on 9/5/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Element.h"
@interface Slab : Element {
    
}

-(Slab*) initWithSlabSQLStmt: (OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform;
@end

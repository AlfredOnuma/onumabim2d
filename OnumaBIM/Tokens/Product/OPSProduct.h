//
//  OPSProduct.h
//  OPSMobile
//
//  Created by Alfred Man on 7/12/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OPSObject.h"
#import "Placement.h"
#import "Representation.h"
#import "BBox.h"


#import "RelContain.h"
@class Element;
@class ViewProductRep;
@class ViewProductLabel;
@class SpatialStructure;
@class OPSModel;
@class DisplayInfo;
@class space;
@interface OPSProduct : OPSObject { 
    OPSModel* _model;
    RelContain *relContain;
    Placement *placement;
    BBox* bBox;
    bool definedUIFrameBBox;
    CGRect uiFrameBBox;
    Representation *representation;
    bool selected;
//    ViewProductRep* viewProduct;
    
    
    NSMutableArray* aViewProductRep;
//    ViewProductRep* viewProductRep;
    
//    NSMutableArray* aRefViewProductRep;
//    NSMutableArray* aGeoViewProductRep;
    
    
    
    
    ViewProductLabel* viewProductLabel;
    CGPoint labelDisplacement;
    DisplayInfo* displayInfo;
    NSMutableDictionary* customColorIndexDictionary;
//    UIColor* defaultColor;
}
//@property (nonatomic, retain) UIColor* defaultColor;

@property (nonatomic, retain) NSMutableDictionary* customColorIndexDictionary;
@property (nonatomic, retain) NSMutableArray* aViewProductRep;
//@property (nonatomic, retain) ViewProductRep* viewProductRep;
@property (nonatomic, assign) bool definedUIFrameBBox; 
@property (nonatomic, assign) CGRect uiFrameBBox;
@property (nonatomic, assign) OPSModel* model;
@property (nonatomic, retain) RelContain *relContain;
@property (nonatomic, retain) BBox* bBox;
@property (nonatomic, retain) Placement *placement;
@property (nonatomic, retain) Representation *representation;
@property (nonatomic, assign) bool selected;
//@property (nonatomic, assign) ViewProductRep* viewProduct;

//@property (nonatomic, retain) NSMutableArray* aRefViewProductRep;
//@property (nonatomic, retain) NSMutableArray* aGeoViewProductRep;




@property (nonatomic, assign) ViewProductLabel* viewProductLabel;
//@property (nonatomic, assign) CGPoint labelDisplacement;
@property (nonatomic, retain) DisplayInfo* displayInfo;

//-(void) redrawAllViewProductRep;
- (void) addElement: (Element*) newElement;
- (void) setRepresentation:(Representation *)newRep;
- (void) setPlacement:(Placement *)newPlacement;

//-(void) addAGeoViewProductRep:(ViewProductRep*) viewProductRep;
//-(void) addARefViewProductRep:(ViewProductRep*) viewProductRep;

- (id) init:(OPSModel*) model ID:(NSInteger) newID guid:(NSString*) newGUID name:(NSString*) newName;
-(void) addViewProductRep:(ViewProductRep*)viewProductRep;
-(ViewProductRep*) firstViewProductRep;

//
//-(id) initWithSQLStmt:(OPSModel*) _model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform;
//- (SpatialStructure*) findParentBldg;
- (SpatialStructure*) findParentSpatialStruct;



-(NSString*) productType;
//- (BBox*) setupBBoxFromRepandPlacement;
//- (BBox*) updateBBox;

-(void) setCustomColorToDictionaryWithIndex:(uint) index value:(uint) value;
-(uint) customColorIndexWithDictionaryIndex:(uint) dictionaryIndex;

-(NSString*) productRepDisplayStr;
@end

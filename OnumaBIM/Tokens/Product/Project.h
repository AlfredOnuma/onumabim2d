//
//  Project.h
//  OPSMobile
//
//  Created by Alfred Man on 8/16/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import "OPSProduct.h"

@interface Project : OPSProduct {    
    NSString* dbPath; 
}

@property (nonatomic, retain) NSString* dbPath;
@end

//
//  OPSProduct.m
//  OPSMobile
//
//  Created by Alfred Man on 7/12/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "OPSProduct.h"
#import "SpatialStructure.h"
#import "Element.h"
#import "Slab.h"

/*
#import "Bldg.h" 
#import "Space.h"
#import "Furn.h"
#import "ViewModelVC.h"
#import "DisplayModelUI.h"
*/







#import "Site.h"
#import "Bldg.h"
#import "Space.h"
#import "Furn.h"
#import "ViewProductRep.h"
#import "Floor.h"
#import "RepresentationItem.h"
#import "Representation.h"
#import "SitePolygon.h"
#import "OPSProjectSite.h"
#import "ProjectViewAppDelegate.h"
@implementation OPSProduct
//@synthesize viewProductRep;
@synthesize aViewProductRep;
@synthesize placement;
@synthesize representation;
@synthesize bBox;
@synthesize definedUIFrameBBox;
@synthesize model=_model;
@synthesize uiFrameBBox;
@synthesize relContain;
@synthesize selected;
//@synthesize defaultColor;
//@synthesize viewProduct;
//@synthesize aRefViewProductRep;
//@synthesize aGeoViewProductRep;
@synthesize viewProductLabel;
//@synthesize labelDisplacement;
@synthesize displayInfo;

@synthesize customColorIndexDictionary;

- (void)dealloc
{
    //    [viewProductRep release];
//    [defaultColor release];defaultColor=nil;
    [customColorIndexDictionary release];customColorIndexDictionary=nil;
    [aViewProductRep release];aViewProductRep=nil;
    //    [aRefViewProductRep release];
    //    [aGeoViewProductRep release];
    [relContain release];relContain=nil;
    [placement release];placement=nil;
    [representation release];representation=nil;
    [bBox release];bBox=nil;
    [displayInfo release];displayInfo=nil;
    //    [viewProductLabel release];
    //    [viewProduct release]; //Don't Release caused its only referencing the view class. It will be destroyed by the Viewing Frame 
    [super dealloc];
}


-(NSString*) productRepDisplayStr{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
//    ViewModelVC*  viewModelVC=[viewModelToolbar parentViewModelVC];
//    //        OPSProduct* selectedProduct=[viewModelVC selectedProduct];
//    //    OPSProduct* selectedProduct=[[viewModelVC selectedProductRep] product];
//    OPSProduct* product=[productRep product];    
//    
    OPSProduct* refProductFromSelectedProduct=nil;
//    
//    
    NSString* displayStr=nil;
    switch ([appDele modelDisplayLevel]){
        case 0:
        {
            if ([self isKindOfClass:[SitePolygon class]]){
                SitePolygon* sitePoly=(SitePolygon*) self;
                displayStr=[NSString stringWithFormat:@"%@: %@",[sitePoly type],[sitePoly name]];
            }else if ([self isKindOfClass:[Site class]]){
                refProductFromSelectedProduct=(Site*) [[self model] root];
                displayStr=[NSString stringWithFormat:@"Site: (S%d_%d) %@",[appDele activeStudioID],[appDele activeProjectSite].ID,[refProductFromSelectedProduct name]];            
            }else if ([self isKindOfClass:[Floor class]]){
                refProductFromSelectedProduct=(Bldg*) [[self linkedRel]relating];
                displayStr=[NSString stringWithFormat:@"Bldg: %d - %@",[refProductFromSelectedProduct ID],[refProductFromSelectedProduct name]];
            }else{
                refProductFromSelectedProduct=self;                
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];
            }
        }
            break;
        case 1:
        {
            
            if ([self isKindOfClass:[Floor class]]){
                Bldg* bldg=(Bldg*) [[self model] root];
                Floor* floor=(Floor*) [[[bldg relAggregate] related] objectAtIndex:[bldg selectedFloorIndex]];
                refProductFromSelectedProduct=floor;
                displayStr=[NSString stringWithFormat:@"Floor: %@",[refProductFromSelectedProduct name]];
            }
            else if ([self isKindOfClass:[Space class]]){
                //                refProductFromSelectedProduct=(Space*) product;
                //                displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]];
                
                Space* space=(Space*) self;
                
                if ([space spaceNumber]!=nil && ![[space spaceNumber] isEqualToString:@""]){                    
                    displayStr=[NSString stringWithFormat:@"%@ - %@ (%@)",[space spaceNumber],[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]]; 
                }else{                    
                    displayStr=[NSString stringWithFormat:@"%@ (%@)",[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]]; 
                }
                
                //                    refProductFromSelectedProduct=(Floor*) [[selectedProduct linkedRel]relating];                    
                //                    displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]];                    
            }else{
                refProductFromSelectedProduct=self;                    
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];                    
            }
            
        }
            break;
        case 2:
        {
            if ([self isKindOfClass:[Space class]]){            
                //                refProductFromSelectedProduct=(Space*) [[viewModelVC model] root];
                //                displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]];
                //                
                Space* space=(Space*) self;//[[self model] root];
                
                if ([space spaceNumber]!=nil && ![[space spaceNumber] isEqualToString:@""]){                    
                    displayStr=[NSString stringWithFormat:@"%@ - %@ (%@)",[space spaceNumber],[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]]; 
                }else{                    
                    displayStr=[NSString stringWithFormat:@"%@ (%@)",[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]]; 
                }
                
                
            }else if ([self isKindOfClass:[Furn class]]){
                //                refProductFromSelectedProduct=(Furn*) product;
                Furn * furn=(Furn*) self;
                
                
                if ([furn componentName]){
                    if (![[furn componentName] isEqualToString:@""]){
                        NSString *trimmedString = [[furn componentName] stringByTrimmingCharactersInSet:
                                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        if (![trimmedString isEqualToString:@""]){                        
                            displayStr=[NSString stringWithFormat:@"%@ - %@",[furn componentName],[furn name]];   
                        }
                    }
                }
                if (displayStr==nil){
                    displayStr=[NSString stringWithFormat:@"%@",[furn name]];
                }
            }else{
                refProductFromSelectedProduct=self;
                
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];
            }
        }
            break;
        
        default:
            break;
    }
    return displayStr;
}



//-(id) initWithSQLStmt:(OPSModel*) _model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform{
//    self=[super init];
//    if (self){}
//    return self;
//}

//-(void) redrawAllViewProductRep{
////    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//    
//
////    if (aGeoViewProductRep ) {
////        for (ViewProductRep* viewProductRep in aGeoViewProductRep){
////            if ([[viewProductRep geoProduct] isKindOfClass:[Furn class]]){
////                continue;
////            }
////            [viewProductRep setNeedsDisplay];
////        }
////    }
//    
//    if (aRefViewProductRep) {
//        for (ViewProductRep* viewProductRep in aRefViewProductRep){
//            if ([[viewProductRep product] isKindOfClass:[Furn class]]){
//                continue;
//            }
//            [viewProductRep setNeedsDisplay];
//        }
//    }
//    
//}
-(NSString*) productType{
    NSString* typeStr=nil;
    if ([self isKindOfClass:[SitePolygon class]]){
        typeStr=@"SitePolygon";
    }
    if ([self isKindOfClass:[Site class]]){
        typeStr=@"Site";                    
    }else if ([self isKindOfClass:[Bldg class]]){
        typeStr=@"Bldg";                      
    }else if ([self isKindOfClass:[Floor class]]){
        typeStr=@"Floor";                   
    }else if ([self isKindOfClass:[Space class]]){
        typeStr=@"Space";                   
    }else if ([self isKindOfClass:[Furn class]]){
        typeStr=@"Equipment";                    
    }else if ([self isKindOfClass:[Slab class]]){
        typeStr=@"Slab";
    }
    return typeStr;
}

- (SpatialStructure*) findParentSpatialStruct{
    if (![self isKindOfClass:[Floor class]]&&
        [self isKindOfClass:[SpatialStructure class]]){ return (SpatialStructure*) self;}

    OPSProduct* testProduct=self;
    if ([testProduct linkedRel] ==nil){ return nil;}
    if ([[testProduct linkedRel] relating]==nil){return nil;}
    OPSObject* linkedRelatingObj=[[testProduct linkedRel] relating];    
    
    while ([linkedRelatingObj isKindOfClass:[Floor class]]
           ||![linkedRelatingObj isKindOfClass:[SpatialStructure class]]) {
        testProduct=(OPSProduct*) linkedRelatingObj;        
        if ([testProduct linkedRel] ==nil){ return nil;}
        if ([[testProduct linkedRel] relating]==nil){return nil;}
        linkedRelatingObj=[[testProduct linkedRel] relating];    
    }
    return (SpatialStructure*) linkedRelatingObj;
    

}

//- (SpatialStructure*) findParentBldg{
//    if ([self isKindOfClass:[Bldg class]]){ return (SpatialStructure*) self;}
//    
//    OPSProduct* testProduct=self;
//    if ([testProduct linkedRel] ==nil){ return nil;}
//    if ([[testProduct linkedRel] relating]==nil){return nil;}
//    OPSObject* linkedRelatingObj=[[testProduct linkedRel] relating];    
//    while (![linkedRelatingObj isKindOfClass:[Bldg class]]) {
//        testProduct=(OPSProduct*) linkedRelatingObj;        
//        if ([testProduct linkedRel] ==nil){ return nil;}
//        if ([[testProduct linkedRel] relating]==nil){return nil;}
//        OPSProduct* linkedRelatingObj=[[testProduct linkedRel] relating];    
//    }
//    return linkedRelatingObj;
//    
//    
//}

//-(void) addAGeoViewProductRep:(ViewProductRep*) viewProductRep{
//    if (aGeoViewProductRep==nil){
//        aGeoViewProductRep=[[NSMutableArray alloc] init];     
//    }
//   [aGeoViewProductRep addObject:viewProductRep];    
//}
//
//-(void) addARefViewProductRep:(ViewProductRep*) viewProductRep{
//    if (aRefViewProductRep==nil){
//        aRefViewProductRep=[[NSMutableArray alloc] init];     
//    }
//    [aRefViewProductRep addObject:viewProductRep];    
//}

-(void) addViewProductRep:(ViewProductRep*)viewProductRep{
    if (aViewProductRep==nil){
        aViewProductRep=[[NSMutableArray alloc] init];     
    }
   [aViewProductRep addObject:viewProductRep];    
}
-(ViewProductRep*) firstViewProductRep{

    
    if ([self isKindOfClass:[Bldg class]]){
        Bldg* bldg=(Bldg*) self;
        Floor* firstFloor=(Floor*)[[ [bldg relAggregate] related] objectAtIndex:[bldg firstFloorIndex]];
        
        return [firstFloor firstViewProductRep];
//        for (OPSProduct* product in [[bldg relAggregate] related]){
//            if ([product isKindOfClass:[Floor class]]){)
//                Floor* floor=(Floor*) product;
//                if ([floor aViewProductRep]){
//                    return [floor firstViewProductRep];
//                }
//            }
//        }
    }
    if (aViewProductRep){
        return [aViewProductRep objectAtIndex:0];
    }else{
        return nil;
    }
    return nil;
}

-(void) setCustomColorToDictionaryWithIndex:(uint) index value:(uint) value{
//    if (self.ID==3229){
//        uint t=0;
//    }
    if(customColorIndexDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.customColorIndexDictionary=dictionary;
        [dictionary release];    
    }
    if ([self.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",index]]) {        
        NSLog(@"Custom Color with Index: %d and value: %u already registered",index,value);
    }else{
        NSNumber* customColorValue=[[NSNumber alloc] initWithInteger:value];
        [self.customColorIndexDictionary setValue:customColorValue forKey:[NSString stringWithFormat:@"%d",index]];
        [customColorValue release];        
    }
    
}
-(uint) customColorIndexWithDictionaryIndex:(uint) dictionaryIndex{
    uint customColorUInt=0;
    if (self.customColorIndexDictionary){
        NSNumber* customColorIndex=[self.customColorIndexDictionary valueForKey:[NSString stringWithFormat:@"%d",dictionaryIndex]];
        customColorUInt=(uint) 
        [customColorIndex integerValue];
    }
    return customColorUInt;
}
- (void) addElement: (Element*) newElement{
    if (self.relContain==nil){
        RelContain* _relContain=[[RelContain alloc] initWithRelatingObj: self];                  
        self.relContain= _relContain;
        [_relContain release];
    }    
    [self.relContain addRelatedObj:newElement]; 
    
    if (!definedUIFrameBBox){
        uiFrameBBox=[[[newElement representation] currentRepItem] bBoxInLocalFrame];
        definedUIFrameBBox=true;
    }else{
        uiFrameBBox=CGRectUnion(uiFrameBBox, [[[newElement representation] currentRepItem] bBoxInLocalFrame]);
    }
//    if (self.representation==nil){
//        if ([newElement representation]!=nil){
//            Representation* rep=[[Representation alloc] init];
//            self.representation=rep;
//            self.representation.bBox=[newElement.representation.bBox copy];
//            [rep release];            
//        }
//
//    }else{
//        [self.representation.bBox mergeBBox:newElement.bBox];
//    }
    if (self.bBox==nil){      
//        BBox* _bBox=[[BBox alloc] initWithCGRect:[newElement.bBox cgRect]];
//        self.bBox=_bBox;
//        [_bBox release];
        BBox* newElemBBox=[newElement.bBox copy];
        self.bBox=newElemBBox;
        [newElemBBox release];
        
    }else{
        [self.bBox mergeBBox:newElement.bBox];
    }
    
}
/*
- (BOOL) shouldDisplayProductLabel{    
    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
    if ([appDele modelDisplayLevel]==0){        
        if ([self isKindOfClass:[Bldg class]]){
            return true;
        }
    }
    if ([appDele modelDisplayLevel]==1){        
        if ([self isKindOfClass:[Space class]]){
            return true;
        }
    }
    if ([appDele modelDisplayLevel]==2){
        if ([self isKindOfClass:[Furn class]]){
            return true;
        }
    }
    return false;
}
 

- (void) displayProductLabeltoDisplayModelUI: (DisplayModelUI*) displayModelUI productTransform:(CGAffineTransform) productTransform{
    
    if (![self shouldDisplayProductLabel]){
        return;
    }
    ViewModelVC* viewModelVC=[displayModelUI parentViewModelVC];
    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];    
    //---------------------------------------------------
    
    
//    
//    
//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//    if ([appDele modelDisplayLevel]==0 && [self isKindOfClass:[Bldg class]]){
        float fontSize=[viewModelVC currentProductLabelSize]/[viewModelVC navUIScrollVC].zoomScale;    
        CGSize productLabelConstraint = CGSizeMake(fontSize*50, fontSize*2);    
        CGSize productLabelSize = [[self name] sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] 
                                                       constrainedToSize:productLabelConstraint 
                                                           lineBreakMode:UILineBreakModeCharacterWrap];   
        
        
        
        //        CGPoint origin=CGPointMake(0, 0);    
        //        origin=CGPointApplyAffineTransform(origin, rootSpatialStructTransform);    
        //        CGPoint labelPos= [self convertPoint:origin toView:productViewLabelLayer];  
        double area=0.0;
        //            double area=[(RepresentationItem*) [[[self.geoProduct representation] aRepresentationItem] objectAtIndex:0] area];
        //        CGPoint labelPos=CGPointMake(0, 0);
        //        ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(labelPos.x-productLabelSize.width/2, labelPos.y-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) name:[rootSpatialStruct name] area:area displayModelUI:self] ;    
        
        
        
        CGPoint origin= CGPointMake(self.labelDisplacement.x*modelScaleFactor, self.labelDisplacement.y*modelScaleFactor);    
        CGPoint labelPos= CGPointApplyAffineTransform(origin, productTransform);       
        CGPoint labelLayerPos=[spatialStructViewLayer convertPoint:labelPos toView:productViewLabelLayer]; 
        
        ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) name:[rootSpatialStruct name] area:area displayModelUI:self] ;
        
        [_viewProductLabel setText:[rootSpatialStruct name]];
        //        [_viewProductLabel setTransform:rootTransform];
        [_viewProductLabel setTransform:CGAffineTransformMakeTranslation(labelLayerPos.x, labelLayerPos.y)];        
        //    self.viewProductLabel=_viewProductLabel;
        //    [_viewProductLabel release];
        [productViewLabelLayer addSubview:_viewProductLabel];
        //        [productViewLabelLayer addSubview:_viewProductLabel];
        [_viewProductLabel release];
        
        [self bringSubviewToFront:[viewModelVC displayModelUI].productViewLabelLayer];
//    }
    
    //---------------------------------------------------    
    
}
 */


- (id) init{ //:(Placement*)placement representation:(Representation*)rep{
    self=[super init];
    if (self){
        self.selected=false;         
        definedUIFrameBBox=false;
//        BBox* _bBox=[[BBox alloc] init];
//        self.bBox=_bBox;
//        [_bBox release];        
//        self.labelDisplacement=CGPointMake(0.0,0.0);
        
        
        

        
    }
    return self;
    
}
- (id) init:(OPSModel*) model ID:(NSInteger) newID guid:(NSString*) newGUID name:(NSString*) newName{

    self=[super init:newID guid:newGUID name:newName];
    if (self){ 
        self.model=model;
        self.selected=false;  
        definedUIFrameBBox=false;
        
//        
//        UIColor* _defColor=[[UIColor alloc] initWithRed:234.0/256.0 green:234.0/256.0 blue:234.0/256.0 alpha:1.0];
//        self.defaultColor=_defColor;
//        CGFloat red;
//        CGFloat green;
//        CGFloat blue;
//        CGFloat alpha;
//        [defaultColor getRed:&red green:&green blue:&blue alpha:&alpha];
//        
//        [_defColor release]; _defColor=nil;
//        self.labelDisplacement=CGPointMake(0.0,0.0);
    }    
    return self;
}


- (void) setRepresentation:(Representation *)newRep{
    [newRep retain];
    [representation release];
    representation= newRep ;
    [representation setProduct:self];
    [bBox release];   
    BBox* _bBox=[[BBox alloc ] initWithCGRect: [newRep currentRepItem].bBoxInRootContext];
    self.bBox=_bBox;
    [_bBox release];
    
    
    
    if (!definedUIFrameBBox){
        uiFrameBBox=[[newRep currentRepItem] bBoxInLocalFrame];
        definedUIFrameBBox=true;
    }else{
        uiFrameBBox=CGRectUnion(uiFrameBBox, [[newRep currentRepItem] bBoxInLocalFrame]);
    }
}

- (void) setPlacement:(Placement *)newPlacement{
    [newPlacement retain];
    [placement release];
    placement=newPlacement ;
    [placement setProduct:(self)];

}

@end

//
//  Project.m
//  OPSMobile
//
//  Created by Alfred Man on 8/16/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "Project.h"

@implementation Project
@synthesize dbPath;
- (id) init{
    self=[super init];
    if (self){
    }
    return self;
}
- (void)dealloc{
    [dbPath release];
    [super dealloc];
}
@end

//
//  Space.m
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "Space.h"
#import "ExtrudedAreaSolid.h"
#import "Representation.h"
#import "DisplayInfo.h"
#import "Furn.h"
#import "AlignColor.h"
#import "ProjectViewAppDelegate.h"
#import "Site.h"
#import "SpatialStructure.h"
#import "Floor.h"
#import "Relationship.h"
#import "OPSObject.h"
#import "Slab.h"
#import "OPSModel.h"
@implementation Space
@synthesize spaceReferenceID;
@synthesize spaceReferencePolyID;
@synthesize spaceReferenceSpatialStructID;
@synthesize deptID;
@synthesize alignID;
@synthesize alignColorStr;
@synthesize spaceNumber;
@synthesize spaceArea;
@synthesize fusion_SpaceInfoRoomPathID=_fusion_SpaceInfoRoomPathID;
@synthesize fusion_SpaceInfoAssignedSF=_fusion_SpaceInfoAssignedSF;

@synthesize fusion_ignoreFusionSpaceNumber=_fusion_ignoreFusionSpaceNumber;
@synthesize roomPathID=_roomPathID;
@synthesize newAdded=_newAdded;
@synthesize fusion_roomStatus=_fusion_roomStatus;
@synthesize fusion_topCssCode=_fusion_topCssCode;
@synthesize fusion_pgmNum=_fusion_pgmNum;
@synthesize fusion_roomUseCode=_fusion_roomUseCode;

@synthesize fusion_topColorDescrption=_fusion_topColorDescription;
@synthesize fusion_topColorColor=_fusion_topColorColor;

@synthesize fusion_pgmColorDescription=_fusion_pgmColorDescription;
@synthesize fusion_pgmColorColor=_fusion_pgmColorColor;

@synthesize fusion_roomUseColorDescription=_fusion_roomUseColorDescription;
@synthesize fusion_roomUseColorColor=_fusion_roomUseColorColor;

//SpaceInfo.ignoreFusionSpaceNumber,SpaceInfo.newAdded,FUSION_SpaceInfo.roomStatus,FUSION_SpaceInfo.topCssCode,FUSION_SpaceInfo.pgmNum,FUSION_SpaceInfo.roomUseCode

//@synthesize spaceIDBeforeRefernce=_spaceIDBeforeRefernce;

-(void) dealloc{
    [alignColorStr release];alignColorStr=nil;
    [_fusion_roomStatus release];_fusion_roomStatus=nil;
    [spaceNumber release];spaceNumber=nil;
    
    [_fusion_topColorDescription release];_fusion_topColorDescription=nil;
    [_fusion_topColorColor release];_fusion_topColorColor=nil;
    
    [_fusion_pgmColorDescription release];_fusion_pgmColorDescription=nil;
    [_fusion_pgmColorColor release];_fusion_pgmColorColor=nil;
    
    [_fusion_roomUseColorDescription release];_fusion_roomUseColorDescription=nil;
    [_fusion_roomUseColorColor release];_fusion_roomUseColorColor=nil;
    [super dealloc];
}




- (void) registerGhostFloorSlabAndNearbySpaceToAppDele{
    if ([self linkedRel]==nil ){
        return;
    }
    BBox* screenBox=[[self bBox] copy];
    
    
    CGRect appWindowFrame= CGRectMake(0,0,1024,768) ;
    CGFloat statusBarHeight=20;
    uint toolbarHeight=44;//85;//44;
    //    uint navigationToolBarHeight=44;
    CGRect opsNavUIFrame=CGRectMake(0,0,appWindowFrame.size.width,appWindowFrame.size.height-statusBarHeight-toolbarHeight);
    
    
    
    double frameWidth=opsNavUIFrame.size.width;
    double frameHeight=opsNavUIFrame.size.height-toolbarHeight;//(-44*2);
    double frameWidthToHeightRatio=frameWidth/frameHeight;
    //        CGRect frameRect=CGRectMake(0,0,frameWidth,frameHeight);
//    
//    double modelScreenFitWidth=frameWidth/[[self model] fitZoom];
//    double modelScreenFitHeight=frameHeight/[[self model] fitZoom];
//    CGRect modelScreenFitRect=CGRectMake(0, 0, modelScreenFitWidth, modelScreenFitHeight);
//
    double screenBoxHeight=[screenBox getHeight];
    double screenBoxWidth=[screenBox getWidth];
    if (screenBoxHeight>screenBoxWidth){
        double targetWidth=[screenBox getHeight]*frameWidthToHeightRatio;
        screenBox.minX=screenBox.minX-(targetWidth-screenBoxWidth)/2;
        screenBox.maxX=screenBox.maxX+(targetWidth-screenBoxWidth)/2;
    }
    CGFloat screenExpandRatio=[[self model] fitZoom]/[[self model] minimumZoom] * [[self model] maximumZoom];
    [screenBox expand:screenExpandRatio];
    CGRect screenBBoxRect=[screenBox cgRect];
    [screenBox release];
    screenBox=nil;
    
    OPSObject* parentObject=[[self linkedRel] relating];
    if (![parentObject isKindOfClass:[Floor class]]){
        return;
    }
    
    Floor* parentFloor=(Floor*)parentObject;    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[UIApplication sharedApplication].delegate;
//    
//    for (OPSProduct* product in [parentFloor relContain].related){
//        if ([product isKindOfClass:[Slab class]]) {
//            Slab* slab=(Slab*) product;
//            NSNumber* slabID=[[NSNumber alloc] initWithInteger:slab.ID];
//            [appDele.aGhostFloorSlabID addObject:slabID];
//            [slabID release];
//        }
//    }
    for (OPSObject* object in [[parentFloor relAggregate] related]){
        if (![object isKindOfClass:[Space class]] || object==self){
            continue;
        }
        Space* space=(Space*)object;
        CGRect spaceBBoxRect=[[space bBox] cgRect];
        if (!CGRectIntersectsRect(spaceBBoxRect , screenBBoxRect)){
            continue;
        }

        
        NSNumber* spaceID=[[NSNumber alloc]initWithInteger:space.ID];
        [appDele.aGhostSpaceID addObject:spaceID];
        [spaceID release];
    }


}






-(Space*) initWithSpaceSQLStmt:(OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform furnDict:(NSMutableDictionary*) furnDict{          
    //       NSInteger siteID = sqlite3_column_int(siteStmt, 0);
    //                NSInteger siteSpatialStructureID = sqlite3_column_int(siteStmt, 1);     
//    NSInteger tokenPolylineID = sqlite3_column_int(sqlStmt, 0); 
    NSInteger tokenSpatialStructID = sqlite3_column_int(sqlStmt, 1);
    //    NSInteger tokenPlacementID = sqlite3_column_int(sqlStmt, 2);    
        
    NSString* tokenName= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 3)!=nil){
        tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 3)];               
    }else{
        tokenName=[NSString stringWithFormat:@""];
    }     
    
    NSString* tokenGuid= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 4)!=nil){
        tokenGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 4)];               
    }else{
        tokenGuid=[NSString stringWithFormat:@""];
    }                              
    
    int tokenID = sqlite3_column_int(sqlStmt, 9);   
    self=[super init:model ID:tokenID spatialStructureID:tokenSpatialStructID guid:tokenGuid name:tokenName];
    if (self){  
        
//        self.spaceIDBeforeRefernce=0;
        
        CGFloat tokenPlacementX = sqlite3_column_double(sqlStmt, 5);//*multiFactor;                           
        CGFloat tokenPlacementY = sqlite3_column_double(sqlStmt, 6);//*multiFactor;                          
        //    CGFloat tokenPlacementZ = sqlite3_column_double(sqlStmt, 7);//*multiFactor;                          
        CGFloat tokenPlacementAngle = sqlite3_column_double(sqlStmt, 8);                 
        
        CGPoint tokenPlacementPt=CGPointMake(tokenPlacementX, tokenPlacementY);
    
        Placement* tokenPlacement=[[Placement alloc] init:0 guid:nil name:nil pt:tokenPlacementPt angle:tokenPlacementAngle isMirrorY:false];                                
        
   
//            NSLog(@"Space %@ ----------------------------", tokenName);          
        //    NSLog(@"Space Placement x:%f y:%f angle:%f",tokenPlacementX,tokenPlacementY,tokenPlacementAngle);
        
        
        
        
        [self setPlacement:tokenPlacement];
        [tokenPlacement release];
        
        
        CGFloat spaceLabelDisplacementX = sqlite3_column_double(sqlStmt, 10);         
        CGFloat spaceLabelDisplacementY = sqlite3_column_double(sqlStmt, 11); 
        

        
        spaceReferenceID = sqlite3_column_double(sqlStmt, 12);
        spaceReferencePolyID = sqlite3_column_double(sqlStmt, 13); 
        spaceReferenceSpatialStructID = sqlite3_column_double(sqlStmt, 14); 
        
        
//        tokenGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 4)];
        char* pSpacePolyStr = (char *) sqlite3_column_text(sqlStmt, 15);
        
        char* pArcStr = (char *) sqlite3_column_text(sqlStmt, 16);
        char* pSpaceReferencePolyStr = (char *) sqlite3_column_text(sqlStmt, 17);
        
        

        
        
        CGAffineTransform cascadedTransform=CGAffineTransformConcat([tokenPlacement toCGAffineTransform], parentTransform);
        
        
        
        
        char* pSpacePolyStrForUse=spaceReferenceID==0?pSpacePolyStr:pSpaceReferencePolyStr;
        
        
        if (pSpacePolyStrForUse!=0){
            
            NSString* polyStr= [NSString stringWithUTF8String:pSpacePolyStrForUse];
            
            
            
            NSString* arcStr=nil;
            if (pArcStr!=0){
                arcStr=[NSString stringWithUTF8String:pArcStr];
            }

            
//            ExtrudedAreaSolid* extrudedAreaSolid= [[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr parentTransform:cascadedTransform localFrameTransform:CGAffineTransformIdentity];
            ExtrudedAreaSolid* extrudedAreaSolid= [[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr arcStr:arcStr parentTransform:cascadedTransform localFrameTransform:CGAffineTransformIdentity];
            
            Representation* tokenRep=[[Representation alloc] init:(0) guid:(nil) name:(nil)];
            [tokenRep addARepresentationItem:(extrudedAreaSolid)];                      
            [extrudedAreaSolid release];
            
            [self setRepresentation:tokenRep];  
            
            [tokenRep release];            
            //                                        [rep optimise:slab.placement];                
        
        }
 
//        [self setLabelDisplacement:CGPointMake(spaceLabelDisplacementX, spaceLabelDisplacementY)];
        
        
        DisplayInfo* _displayInfo=[[DisplayInfo alloc] initWithProduct:self displacement:CGPointMake(spaceLabelDisplacementX, spaceLabelDisplacementY)];
//        NSLog(@"Space displayLabel id: %d name:%@ dx: %f df: %f",self.ID, self.name, spaceLabelDisplacementX,spaceLabelDisplacementY);
        self.displayInfo=_displayInfo;
        [_displayInfo release];
        
        
        
        char* pSpatialStructEquipIDStr = (char *) sqlite3_column_text(sqlStmt, 18);
        char* pSpatialStructReferenceEquipIDStr = (char *) sqlite3_column_text(sqlStmt, 19);
        
        
        char* pSpatialStructEquipIDStrForUse=spaceReferenceID==0?pSpatialStructEquipIDStr:pSpatialStructReferenceEquipIDStr;
        
        
        self.deptID = sqlite3_column_int(sqlStmt, 20);
        self.alignID = sqlite3_column_int(sqlStmt, 21);
        NSString* tmpAlignColorStr=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:22]];
        self.alignColorStr=tmpAlignColorStr;
        [tmpAlignColorStr release];
        if (alignID!=0){
            AlignColor* alignColor=[[AlignColor alloc] initWithID:self.alignID name:self.name colorStr:self.alignColorStr];
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            Site* site=[appDele currentSite];
            [site addAlignColorWithSpace:self];
            [alignColor release];
        }
        
        
        
        
        uint pCol=22;
        
        for (uint pCustomColor=1;pCustomColor<=30;pCustomColor++){
            uint customColorValue=sqlite3_column_int(sqlStmt, pCol+pCustomColor);
            if (customColorValue!=0){
                [self setCustomColorToDictionaryWithIndex:pCustomColor value:customColorValue];            
            }
        }
        
        pCol+=(31);
        
        NSString* tmpSpaceNumber=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol]];
        self.spaceNumber=tmpSpaceNumber;
        [tmpSpaceNumber release];
        pCol++;
        
        self.spaceArea=sqlite3_column_double(sqlStmt, pCol);
        pCol++;
        
        
        self.fusion_SpaceInfoRoomPathID=sqlite3_column_int(sqlStmt, pCol);
        pCol++;
        
        self.fusion_SpaceInfoAssignedSF=sqlite3_column_double(sqlStmt, pCol);
        pCol++;
        
        self.fusion_ignoreFusionSpaceNumber=sqlite3_column_int(sqlStmt, pCol);
        pCol++;
        
        self.roomPathID=sqlite3_column_int(sqlStmt, pCol);
        pCol++;
        
        self.newAdded=sqlite3_column_int(sqlStmt, pCol);
        pCol++;
   
        NSString* tmpRoomStatusStr=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol]];
        self.fusion_roomStatus=tmpRoomStatusStr;
        [tmpRoomStatusStr release];
        pCol++;
        
        self.fusion_topCssCode=sqlite3_column_int(sqlStmt, pCol);
        pCol++;
        
        
        NSString* tmpTopColorDescription=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol]];
        self.fusion_topColorDescrption=tmpTopColorDescription;
        [tmpTopColorDescription release];
        pCol++;
        
        
        NSString* tmpTopColorColor=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol]];
        self.fusion_topColorColor=tmpTopColorColor;
        [tmpTopColorColor release];
        pCol++;
        
        if (self.fusion_topCssCode>0 && self.fusion_topColorDescrption && self.fusion_topColorColor){
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            Site* site=[appDele currentSite];
            [site addFusionTopColor:self.fusion_topCssCode name:self.fusion_topColorDescrption color:self.fusion_topColorColor bOverWrite:false];
        }
        
        self.fusion_pgmNum=sqlite3_column_int(sqlStmt, pCol);
        pCol++;
        
        
        NSString* tmpPGMDescription=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol]];
        self.fusion_pgmColorDescription=tmpPGMDescription;
        [tmpPGMDescription release];
        pCol++;
        
        NSString* tmpPGMColor=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol]];
        self.fusion_pgmColorColor=tmpPGMColor;
        [tmpPGMColor release];
        pCol++;
        
        
        if (self.fusion_pgmNum>0 && self.fusion_pgmColorDescription && self.fusion_pgmColorColor){
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            Site* site=[appDele currentSite];
            [site addFusionPGMColor:self.fusion_pgmNum name:self.fusion_pgmColorDescription color:self.fusion_pgmColorColor bOverWrite:false];
        }
        
        self.fusion_roomUseCode=sqlite3_column_int(sqlStmt, pCol);
        pCol++;        
        
        NSString* tmpRoomUseColorDescription=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol]];
        self.fusion_roomUseColorDescription=tmpRoomUseColorDescription;
        [tmpRoomUseColorDescription release];
        pCol++;
        
        NSString* tmpRoomUseColorColor=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol]];
        self.fusion_roomUseColorColor=tmpRoomUseColorColor;
        [tmpRoomUseColorColor release];
        pCol++;
        
        
        if (self.fusion_roomUseCode>0 && self.fusion_roomUseColorDescription && self.fusion_roomUseColorColor){
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            Site* site=[appDele currentSite];
            [site addFusionRoomUseColor:self.fusion_roomUseCode name:self.fusion_roomUseColorDescription color:self.fusion_roomUseColorColor bOverWrite:false];
        }
        if (furnDict!=nil && [furnDict count]>0){
            if (pSpatialStructEquipIDStrForUse!=0){            
    //            uint spaceSpatialStructIDForUse=self.spaceReferenceID==0?self.spatialStructureID:self.spaceReferenceSpatialStructID;
                
                
                NSString* spatialStructEquipIDStr= [[NSString stringWithUTF8String:pSpatialStructEquipIDStrForUse] stringByTrimmingCharactersInSet:
                [NSCharacterSet whitespaceAndNewlineCharacterSet]];  
    //            NSLog(@"-%@-",spatialStructEquipIDStr);
                if (![spatialStructEquipIDStr isEqualToString: @""]){
                    CGAffineTransform spaceTransform=CGAffineTransformConcat([self.placement toCGAffineTransform], parentTransform);
                    

                    
                    NSArray *aSpaceFurnID=[spatialStructEquipIDStr componentsSeparatedByString:@","];
                    
                    for (NSString* furnID in aSpaceFurnID){
                        
                        
                        if (spaceReferenceID==0){
                            Furn* furn=[furnDict objectForKey:furnID];
                            [furn setParentTransformAndSetupRepresentation:spaceTransform];
                            
                            [self addElement:furn];
                        }else{
                            Furn* furn=[[furnDict objectForKey:furnID] copy];
                            [furn setParentTransformAndSetupRepresentation:spaceTransform];
                            
                            [self addElement:furn];
                            [furn release];
                        }
                        

    //                    [furnDict removeObjectForKey:furnID];
                    }
                }
            }
        }
        
    }
    return self;
}


-(Space*) initWithSpaceSQLStmt:(OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform{
    //       NSInteger siteID = sqlite3_column_int(siteStmt, 0);
    //                NSInteger siteSpatialStructureID = sqlite3_column_int(siteStmt, 1);     
//    NSInteger tokenPolylineID = sqlite3_column_int(sqlStmt, 0);
    
//    self.spaceIDBeforeRefernce=0;
    
    NSInteger tokenSpatialStructID = sqlite3_column_int(sqlStmt, 1);
    //    NSInteger tokenPlacementID = sqlite3_column_int(sqlStmt, 2);    
    
    NSString* tokenName= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 3)!=nil){
        tokenName=[[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 3)];
//        tokenName=[NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 3)];
    }else{
        tokenName=[[NSString alloc] initWithFormat:@""];
//        tokenName=[NSString stringWithFormat:@""];
    }
    
    NSString* tokenGuid= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 4)!=nil){
        tokenGuid= [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 4)];
//        tokenGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 4)];               
    }else{
        tokenGuid=[[NSString alloc]initWithFormat:@""];
//        tokenGuid=[NSString stringWithFormat:@""];
    }
    
    int tokenID = sqlite3_column_int(sqlStmt, 9);   
    self=[super init:model ID:tokenID spatialStructureID:tokenSpatialStructID guid:tokenGuid name:tokenName];
//    self=[super init:_model ID:tokenID spatialStructureID:tokenSpatialStructID guid:tokenGuid name:tokenName isMirrorY:false];
    if (self){  
        
        CGFloat tokenPlacementX = sqlite3_column_double(sqlStmt, 5);//*multiFactor;                           
        CGFloat tokenPlacementY = sqlite3_column_double(sqlStmt, 6);//*multiFactor;                          
        //    CGFloat tokenPlacementZ = sqlite3_column_double(sqlStmt, 7);//*multiFactor;                          
        CGFloat tokenPlacementAngle = sqlite3_column_double(sqlStmt, 8);                 
        
        CGPoint tokenPlacementPt=CGPointMake(tokenPlacementX, tokenPlacementY);
        
        Placement* tokenPlacement=[[Placement alloc] init:0 guid:nil name:nil pt:tokenPlacementPt angle:tokenPlacementAngle isMirrorY:false];                                
        
        
        //    NSLog(@"Space %@ ----------------------------", tokenName);          
        //    NSLog(@"Space Placement x:%f y:%f angle:%f",tokenPlacementX,tokenPlacementY,tokenPlacementAngle);
        
        
        
        
        [self setPlacement:tokenPlacement];
        [tokenPlacement release];
        
        
        CGFloat spaceLabelDisplacementX = sqlite3_column_double(sqlStmt, 10);         
        CGFloat spaceLabelDisplacementY = sqlite3_column_double(sqlStmt, 11); 
        
        
        
        spaceReferenceID = sqlite3_column_double(sqlStmt, 12); 
        spaceReferencePolyID = sqlite3_column_double(sqlStmt, 13); 
        spaceReferenceSpatialStructID = sqlite3_column_double(sqlStmt, 14); 
        
        
        //        tokenGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 4)];
        char* pSpacePolyStr = (char *) sqlite3_column_text(sqlStmt, 15);
        
        char* pArcStr = (char *) sqlite3_column_text(sqlStmt, 16);
        char* pSpaceReferencePolyStr = (char *) sqlite3_column_text(sqlStmt, 17);
        

        
        
        
        self.deptID = sqlite3_column_int(sqlStmt, 18);
        self.alignID = sqlite3_column_int(sqlStmt, 19);
        
        NSString* tmpAlignColorStr=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:20]];
        self.alignColorStr=tmpAlignColorStr;
        [tmpAlignColorStr release];
        
        
        if (alignID!=0){
            AlignColor* alignColor=[[AlignColor alloc] initWithID:self.alignID name:self.name colorStr:self.alignColorStr];
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            Site* site=[appDele currentSite];
            [site addAlignColorWithSpace:self];
            [alignColor release];
        }
        
        NSString* tmpSpaceNumber=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:(21+30)]];
        self.spaceNumber=tmpSpaceNumber;
        [tmpSpaceNumber release];
        self.spaceArea=sqlite3_column_double(sqlStmt, (22+30));
        
        CGAffineTransform cascadedTransform=CGAffineTransformConcat([tokenPlacement toCGAffineTransform], parentTransform);
        
        
        
        
        char* pSpacePolyStrForUse=spaceReferenceID==0?pSpacePolyStr:pSpaceReferencePolyStr;
        
        
        if (pSpacePolyStrForUse!=0){
            
            NSString* polyStr= [NSString stringWithUTF8String:pSpacePolyStrForUse];    
//            ExtrudedAreaSolid* extrudedAreaSolid= [[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr parentTransform:cascadedTransform localFrameTransform:[placement toCGAffineTransform]];
            
            
            
            
            
            
            NSString* arcStr=nil;
            if (pArcStr!=0){
                arcStr=[NSString stringWithUTF8String:pArcStr];
            }
                        
            
            
//            ExtrudedAreaSolid* extrudedAreaSolid= [[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr parentTransform:cascadedTransform localFrameTransform:CGAffineTransformIdentity];
            ExtrudedAreaSolid* extrudedAreaSolid= [[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr arcStr:arcStr parentTransform:cascadedTransform localFrameTransform:CGAffineTransformIdentity];
            
            Representation* tokenRep=[[Representation alloc] init:(0) guid:(nil) name:(nil)];
            [tokenRep addARepresentationItem:(extrudedAreaSolid)];                      
            [extrudedAreaSolid release];
            
            [self setRepresentation:tokenRep];  
            
            [tokenRep release];            
            //                                        [rep optimise:slab.placement];                
            
        }
        
        //        [self setLabelDisplacement:CGPointMake(spaceLabelDisplacementX, spaceLabelDisplacementY)];
        
        
        DisplayInfo* _displayInfo=[[DisplayInfo alloc] initWithProduct:self displacement:CGPointMake(spaceLabelDisplacementX, spaceLabelDisplacementY)];        
        self.displayInfo=_displayInfo;
        [_displayInfo release];
        
        
    }
    [tokenGuid release];
    tokenGuid=nil;
    [tokenName release];
    tokenName=nil;
    return self;
}

@end

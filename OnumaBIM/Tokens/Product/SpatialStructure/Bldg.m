//
//  Bldg.m
//  OPSMobile
//
//  Created by Alfred Man on 9/1/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//
//#import "Site.h"
#import "Bldg.h"
#import "ProjectViewAppDelegate.h"
#import "ExtrudedAreaSolid.h"
#import "Representation.h"
#import "DisplayInfo.h"
#import "Floor.h"
#import "RelAggregate.h"
@implementation Bldg
@synthesize selectedFloorIndex;
@synthesize firstFloorIndex;
@synthesize bldgReferenceID;
@synthesize existing;
@synthesize bldgPathID=_bldgPathID;
@synthesize fusionBldgPathID=_fusionBldgPathID;
@synthesize fusionBldgStatus=_fusionBldgStatus;
@synthesize fusionBldgFCI=_fusionBldgFCI;
@synthesize fusionBldgStatusFlag_newAdded=_fusionBldgStatusFlag_newAdded;
-(Floor*) selectedFloor{
//    RelAggregate* relAggregate=[self relAggregate];
    NSMutableArray* relatedAggregate=[relAggregate related];    
    return (Floor*)[relatedAggregate objectAtIndex:self.selectedFloorIndex];
}
-(Bldg*) initWithBldgSQLStmt: (OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform{    
    self.model=model;
            
    NSInteger bldgID = sqlite3_column_int(sqlStmt, 0);
    NSInteger bldgSpatialStructureID = sqlite3_column_int(sqlStmt, 1);     
//    NSInteger bldgPlacementID = sqlite3_column_int(sqlStmt, 2);
//    NSString* bldgName=nil;
//    if ((char *) sqlite3_column_text(sqlStmt, 3)!=nil){
//        bldgName = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 3)];
//    }else{                        
//        bldgName = [NSString stringWithFormat:@""];                        
//    }    
//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
    
    NSString* bldgName=[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:3];
    


    //                        [bldgName autorelease];
    
//    NSString* bldgGuid=nil;
//    if ((char *) sqlite3_column_text(sqlStmt, 4)!=nil){
//        bldgGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 4)];                        
//    }else{
//        bldgGuid= [NSString stringWithFormat:@""];                        
//    }
    
    
    NSString* bldgGuid=[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:4];
    
    
    //                        [bldgGuid autorelease];
    
    //    NSLog("RetainTest:%d",[bldg retainCount]);
    
    
    
    CGFloat bldgPlacementX = sqlite3_column_double(sqlStmt, 5);//*multiFactor;                           
    CGFloat bldgPlacementY = sqlite3_column_double(sqlStmt, 6);//*multiFactor;                          
    //    CGFloat bldgPlacementZ = sqlite3_column_double(sqlStmt, 7);//*multiFactor;                          
    CGFloat bldgPlacementAngle = sqlite3_column_double(sqlStmt, 8);     
    
    CGFloat bldgFirstFloorIndex = sqlite3_column_double(sqlStmt, 9); 
    
    
    CGFloat bldgLabelDisplacementX = sqlite3_column_double(sqlStmt, 10);         
    CGFloat bldgLabelDisplacementY = sqlite3_column_double(sqlStmt, 11); 
    
    
    bldgReferenceID = sqlite3_column_double(sqlStmt, 12); 
    
    
//    NSString* tmpBldgNumber=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:13]];
//    self.bldgNumber=tmpBldgNumber;
//    [tmpBldgNumber release];
//    
//    
    
    firstFloorIndex=(int) bldgFirstFloorIndex;
    selectedFloorIndex=firstFloorIndex;

    
    
    
    
    

    
    
    self=[super  init :_model ID:bldgID spatialStructureID:bldgSpatialStructureID guid:bldgGuid name:bldgName];

    
    
    
    
    if (self){
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//        Site* site=[appDele currentSite];
        
        self.existing = sqlite3_column_int(sqlStmt, 13);
        NSString* tmpStr=[[NSString alloc] initWithString: [ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:14]];
        self.fusionBldgStatus=tmpStr;
        [tmpStr release];
        
//        if (self.fusionBldgStatus && ![self.fusionBldgStatus isEqualToString:@""]){
//            if (site.fusionBldgStatusDictionary==nil){
//                [site setupFusionBldgStatusColor];
//            }
//        }
        
        self.fusionBldgFCI = sqlite3_column_int(sqlStmt, 15);
        self.bldgPathID = sqlite3_column_int(sqlStmt, 16);
        self.fusionBldgPathID = sqlite3_column_int(sqlStmt, 17);
        self.fusionBldgStatusFlag_newAdded = sqlite3_column_int(sqlStmt, 18);
        uint pCol=18;
        
        for (uint pCustomColor=1;pCustomColor<=30;pCustomColor++){
            uint customColorValue=sqlite3_column_int(sqlStmt, pCol+pCustomColor);
            if (customColorValue!=0){
                [self setCustomColorToDictionaryWithIndex:pCustomColor value:customColorValue];            
            }
        }

        
        CGPoint bldgPlacementPt=CGPointMake(bldgPlacementX, bldgPlacementY); 
        Placement* bldgPlacement=[[Placement alloc] init:0 guid:nil name:nil pt:bldgPlacementPt angle:bldgPlacementAngle isMirrorY:false];                                
        
    //    Bldg* bldg=[[Bldg alloc] init:self ID:bldgID spatialStructureID:bldgSpatialStructureID guid:bldgGuid name:bldgName];    
        [self setPlacement:bldgPlacement];
        [bldgPlacement release];                
//        [self setLabelDisplacement:CGPointMake(bldgLabelDisplacementX, bldgLabelDisplacementY)];
        
        
        DisplayInfo* _displayInfo=[[DisplayInfo alloc] initWithProduct:self displacement:CGPointMake(bldgLabelDisplacementX, bldgLabelDisplacementY)];        
        self.displayInfo=_displayInfo;
        [_displayInfo release];
        
        
    }
    return self;
}


- (void)dealloc
{
    [_fusionBldgStatus release];_fusionBldgStatus=nil;
    [super dealloc];
}

@end

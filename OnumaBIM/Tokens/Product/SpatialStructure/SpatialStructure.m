//
//  SpatialStructure.m
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "SpatialStructure.h"
#import "RelAggregate.h"

@implementation SpatialStructure
@synthesize relAggregate;

@synthesize spatialStructureID;


- (void)dealloc
{
    [relAggregate release];
    [super dealloc];
}
//
//-getAllRelatedProduct:(NSMutableArray*) aRelatedProduct{
//
//    if (self.relAggregate!=nil && [self.relAggregate related]){
//        if (aRelatedProduct==nil){
//            aRelatedProduct=[[NSMutableArray alloc] init];
//        }
//        uint pRelated=0;
//
//        OPSProduct* product=[[[self relAggregate] related] objectAtIndex:pRelated];
//        aRelatedProduct addObject:
//    }
//}


- (id) init{
    self=[super init];
    if (self){
        spatialStructureID=0;
//        relAggregate= nil;// [[RelAggregate alloc] initWithRelatingObj: self];             
    }
    return self;
}

- (void) addSpatialStructure: (SpatialStructure*) newSpatialStruct{
    if (relAggregate==nil){ 
        RelAggregate* _relAggregate=[[RelAggregate alloc] initWithRelatingObj: self];              
        self.relAggregate= _relAggregate;
        [_relAggregate release];
    }
    [relAggregate addRelatedObj:newSpatialStruct];    
    
    
//    if (self.representation==nil){
//        Representation* rep=[[Representation alloc] init];
//        self.representation=rep;
//        self.representation.bBox=[newSpatialStruct.bBox copy];
//        [rep release];
//    }else{
//        [self.representation.bBox mergeBBox:newSpatialStruct.bBox];
//    }    
    if (self.bBox==nil){   
        BBox* newSpatialStructBBox=[newSpatialStruct.bBox copy];
        self.bBox=newSpatialStructBBox;
        [newSpatialStructBBox release];
    }else{
        [self.bBox mergeBBox:newSpatialStruct.bBox];
    }
}

- (id) init:(OPSModel*) model ID:(NSInteger) newID guid:(NSString*) newGUID name:(NSString*) newName{
    self=[super init:newID guid:newGUID name:newName];
    if (self){  
        self.model=model;
        spatialStructureID=0;
//        relAggregate= nil;//[[RelAggregate alloc] initWithRelatingObj: self];
    }
    return self;
}


- (id) init:(OPSModel*) model ID:(NSInteger) newID spatialStructureID:(NSInteger) _spatialStructureID guid:(NSString*) newGUID name:(NSString*) newName{

    self=[super init:newID guid:newGUID name:newName];
    if (self){
        self.model=model;
        spatialStructureID=_spatialStructureID;
//        relAggregate= nil;//[[RelAggregate alloc] initWithRelatingObj: self];                
    }
    return self;
}



@end

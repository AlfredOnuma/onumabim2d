//
//  Floor.h
//  OPSMobile
//
//  Created by Alfred Man on 7/12/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpatialStructure.h"
#import <sqlite3.h>
@interface Floor : SpatialStructure {
    
}

-(Floor*) initWithFloorSQLStmt: (OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform bldgFirstLevelIndex: (uint) bldgFirstLevelIndex currentIndex:(uint) currentIndex;

@end

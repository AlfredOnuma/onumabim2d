//
//  Floor.m
//  OPSMobile
//
//  Created by Alfred Man on 7/12/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "Floor.h"

#import "ExtrudedAreaSolid.h"
#import "Representation.h"
@implementation Floor

- (id) init{
    self=[super init];
    if (self){
    }
    return self;
    
}


-(Floor*) initWithFloorSQLStmt: (OPSModel*) model database: (sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform bldgFirstLevelIndex: (uint) bldgFirstLevelIndex currentIndex:(uint) currentIndex{   

    NSInteger tokenID = sqlite3_column_int(sqlStmt, 0);
    NSInteger tokenSpatialStructID = sqlite3_column_int(sqlStmt, 1);    


    NSString* tokenName= nil;
    char* pTokenName=(char *) sqlite3_column_text(sqlStmt, 2);
    if (pTokenName!=nil && pTokenName){
        tokenName= [NSString stringWithUTF8String:pTokenName];               
    }
    if (tokenName==nil || [tokenName isEqualToString:@""]){
        
        int floorIndex=currentIndex-bldgFirstLevelIndex;
        if (floorIndex<0){
            tokenName=[NSString stringWithFormat:@"Floor B%d",(-1*floorIndex)];
        }else{
            tokenName=[NSString stringWithFormat:@"Floor %d",(floorIndex+1)];
        }
    }
    NSString* tokenGuid= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 3)!=nil){
        tokenGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 3)];               
    }else{
        tokenGuid=[NSString stringWithFormat:@""];
    }          
    
    
        

    self=[super init:model ID:tokenID spatialStructureID:tokenSpatialStructID guid:tokenGuid name:tokenName];
    if (self){  
        
        //                                [floorName autorelease];                                       
        //                                [floorGuid autorelease];      


        //    NSInteger floorID0=floor.ID;
        //    NSInteger floorSpatialStructID=[(SpatialStructure*) [floor superclass] ID];

        //    NSInteger floorSpatialStructID=((SpatialStructure*) [floor super]).ID;


        ////////Moved to this part instead of after continue to keep a simple consistent block for subcalling
        //Drawback is the placement info of the floor not belonged to current floor view  will be read as well in the floorview. We need to read the name of other floors anyway but now read the placement as well.
        CGFloat tokenPlacementX = sqlite3_column_double(sqlStmt, 4);//*multiFactor;                           
        CGFloat tokenPlacementY = sqlite3_column_double(sqlStmt, 5);//*multiFactor;                          
//        CGFloat tokenPlacementZ = sqlite3_column_double(sqlStmt, 6);//*multiFactor;                          
        CGFloat tokenPlacementAngle = sqlite3_column_double(sqlStmt, 7); 


        CGPoint placementPt=CGPointMake(tokenPlacementX, tokenPlacementY); 
        Placement* tokenPlacement=[[Placement alloc] init:0 guid:nil name:nil pt:placementPt angle:tokenPlacementAngle isMirrorY:false];  
        [self setPlacement:tokenPlacement];  
        [tokenPlacement release];
    }
    return self;
}

@end

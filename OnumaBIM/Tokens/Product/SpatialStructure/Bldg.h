//
//  Bldg.h
//  OPSMobile
//
//  Created by Alfred Man on 9/1/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpatialStructure.h"
@class Floor;
@interface Bldg : SpatialStructure {
    int selectedFloorIndex;
    int firstFloorIndex;
    uint bldgReferenceID;
    
    NSString* _fusionBldgStatus;
    uint _fusionBldgFCI;
    int _bldgPathID;
    int _fusionBldgPathID;
    uint fusionBldgStatusFlag_newAdded;
    int existing;
//    NSString* bldgNumber;
}
//@property (nonatomic, retain) NSString* bldgNumber;
@property (nonatomic, retain) NSString* fusionBldgStatus;
@property (nonatomic, assign) uint fusionBldgFCI;
@property (nonatomic, assign) int bldgPathID;
@property (nonatomic, assign) int fusionBldgPathID;
@property (nonatomic, assign) uint fusionBldgStatusFlag_newAdded;


@property (nonatomic, assign) int existing;
@property (nonatomic, assign) int selectedFloorIndex;
@property (nonatomic, assign) int firstFloorIndex;
@property (nonatomic, assign) uint bldgReferenceID;

-(Bldg*) initWithBldgSQLStmt: (OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform;
-(Floor*) selectedFloor;
@end

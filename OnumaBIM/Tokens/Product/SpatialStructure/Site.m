//
//  Site.m
//  OPSMobile
//
//  Created by Alfred Man on 9/1/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "Site.h"
#import "ExtrudedAreaSolid.h"
#import "Representation.h"
#import "ProjectViewAppDelegate.h"
#import "Department.h"
#import "AlignColor.h"
#import "ConstructionStatusColor.h"
#import "Space.h"
#import "CustomColorStruct.h"
#import "CustomHeaderStruct.h"


#import "FusionBldgStatusColor.h"
#import "FusionBldgFCIColor.h"
#import "FusionBldgStatusFlagColor.h"
#import "FusionPlannedSFAssignedSFColor.h"
#import "FusionSpaceStatusFlagColor.h"
#import "FusionRecordStatusColor.h"
#import "FusionTopColor.h"
#import "FusionPGMColor.h"
#import "FusionRoomUseColor.h"

@implementation Site
@synthesize constructionStatusDictionary;
@synthesize dateModified;
@synthesize currency;
@synthesize isImperial;
@synthesize latitude;	
@synthesize longitude;
@synthesize hasWorldCoord;
@synthesize shareView;
@synthesize shareEdit;
@synthesize aDepartment;
@synthesize alignColorDictionary;
@synthesize aBldgCustomColorStruct;
@synthesize activeSpaceCustomColorStruct;
@synthesize aSpaceCustomColorStruct;
@synthesize activeBldgCustomColorStruct;
@synthesize customHeaderStruct;




@synthesize fusionBldgStatusDictionary=_fusionBldgStatusDictionary;
@synthesize fusionBldgFCIDictionary=_fusionBldgFCIDictionary;
@synthesize fusionBldgStatusFlagDictionary=_fusionBldgStatusFlagDictionary;
@synthesize fusionPlannedSFAssignedSFDictionary=_fusionPlannedSFAssignedSFDictionary;
@synthesize fusionSpaceStatusFlagDictionary=_fusionSpaceStatusFlagDictionary;
@synthesize fusionRecordStatusDictionary=_fusionRecordStatusDictionary;
@synthesize fusionTopDictionary=_fusionTopDictionary;
@synthesize fusionPGMDictionary=_fusionPGMDictionary;
@synthesize fusionRoomUseDictionary=_fusionRoomUseDictionary;


//NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
//NSLog(@"%@", date);

+(uint) numNonCustomColor{
    return 3;
}
-(void) dealloc{
    [aBldgCustomColorStruct release];aBldgCustomColorStruct=nil;
    [aSpaceCustomColorStruct release];aSpaceCustomColorStruct=nil;
    [aDepartment release];aDepartment=nil;
    [alignColorDictionary release];alignColorDictionary=nil;
    [constructionStatusDictionary release];constructionStatusDictionary=nil;
    [customHeaderStruct release];customHeaderStruct=nil;
    [currency release];currency=nil;
    [super dealloc];
}

-(void) setupConstructionStatusColor{
    
    if(constructionStatusDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.constructionStatusDictionary=dictionary;
        [dictionary release];    
    }
//    if ([self.constructionStatusDictionary objectForKey:[NSString stringWithFormat:@"%d",0]]) {
////        NSLog(@"Align Color with space ID %d and space name: %@ already registered",space.ID,space.name);
//    }else{
    ConstructionStatusColor* constructionStatusColor=[[ConstructionStatusColor alloc] initWithID:0 name:@"Existing"colorStr:@"0066ff"];
    [self.constructionStatusDictionary setValue:constructionStatusColor forKey:[NSString stringWithFormat:@"%d",constructionStatusColor.ID]];
    [constructionStatusColor release];
    
    
    constructionStatusColor=[[ConstructionStatusColor alloc] initWithID:1 name:@"New"colorStr:@"ff9966"];
    [self.constructionStatusDictionary setValue:constructionStatusColor forKey:[NSString stringWithFormat:@"%d",constructionStatusColor.ID]];
    [constructionStatusColor release];
    
    
    constructionStatusColor=[[ConstructionStatusColor alloc] initWithID:2 name:@"Renovation"colorStr:@"99ff66"];
    [self.constructionStatusDictionary setValue:constructionStatusColor forKey:[NSString stringWithFormat:@"%d",constructionStatusColor.ID]];
    [constructionStatusColor release];
    
//    }
    
}

-(void) setupFusionBldgStatusColor{
    
    if(self.fusionBldgStatusDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.fusionBldgStatusDictionary=dictionary;
        [dictionary release];
    }
    //    if ([self.constructionStatusDictionary objectForKey:[NSString stringWithFormat:@"%d",0]]) {
    ////        NSLog(@"Align Color with space ID %d and space name: %@ already registered",space.ID,space.name);
    //    }else{
    FusionBldgStatusColor* fusionBldgStatusColor=[[FusionBldgStatusColor alloc] initWithID:0 name:@"ACTIVE" colorStr:@"CC66FF"];
    [self.fusionBldgStatusDictionary setValue:fusionBldgStatusColor forKey:@"Active"];
    [fusionBldgStatusColor release];
    

    
    fusionBldgStatusColor=[[FusionBldgStatusColor alloc] initWithID:1 name:@"UNCLASSIFIED" colorStr:@"00FF00"];
    [self.fusionBldgStatusDictionary setValue:fusionBldgStatusColor forKey:@"UNCLASSIFIED"];
    [fusionBldgStatusColor release];
    
    fusionBldgStatusColor=[[FusionBldgStatusColor alloc] initWithID:2 name:@"OFFLINE" colorStr:@"FFFF00"];
    [self.fusionBldgStatusDictionary setValue:fusionBldgStatusColor forKey:@"OFFLINE"];
    [fusionBldgStatusColor release];
    
    fusionBldgStatusColor=[[FusionBldgStatusColor alloc] initWithID:3 name:@"OFFLINE TEMPORARILY" colorStr:@"FFFF00"];
    [self.fusionBldgStatusDictionary setValue:fusionBldgStatusColor forKey:@"OFFLINE TEMPORARILY"];
    [fusionBldgStatusColor release];
    
    fusionBldgStatusColor=[[FusionBldgStatusColor alloc] initWithID:4 name:@"DEACTIVATED" colorStr:@"FF0000"];
    [self.fusionBldgStatusDictionary setValue:fusionBldgStatusColor forKey:@"DEACTIVATED"];
    [fusionBldgStatusColor release];
        
    fusionBldgStatusColor=[[FusionBldgStatusColor alloc] initWithID:5 name:@"NON-ASSIGNABLE" colorStr:@"00FFFF"];
    [self.fusionBldgStatusDictionary setValue:fusionBldgStatusColor forKey:@"NON-ASSIGNABLE"];
    [fusionBldgStatusColor release];
    
    fusionBldgStatusColor=[[FusionBldgStatusColor alloc] initWithID:6 name:@"DEFAULT" colorStr:@"FFFFFF"];
    [self.fusionBldgStatusDictionary setValue:fusionBldgStatusColor forKey:@"DEFAULT"];
    [fusionBldgStatusColor release];
    
    
}



-(void) setupFusionBldgFCIColor{
    
    if(self.fusionBldgFCIDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.fusionBldgFCIDictionary=dictionary;
        [dictionary release];
    }
    //    if ([self.constructionStatusDictionary objectForKey:[NSString stringWithFormat:@"%d",0]]) {
    ////        NSLog(@"Align Color with space ID %d and space name: %@ already registered",space.ID,space.name);
    //    }else{
    FusionBldgFCIColor* fusionBldgFCIColor=[[FusionBldgFCIColor alloc] initWithID:0 name:@"=0" colorStr:@"FFFFFF"];
    [self.fusionBldgFCIDictionary setValue:fusionBldgFCIColor forKey:@"=0"];
    [fusionBldgFCIColor release];
    
    
    fusionBldgFCIColor=[[FusionBldgFCIColor alloc] initWithID:1 name:@"<=10" colorStr:@"0000FF"];
    [self.fusionBldgFCIDictionary setValue:fusionBldgFCIColor forKey:@">0"];
    [fusionBldgFCIColor release];
    
            
    
    fusionBldgFCIColor=[[FusionBldgFCIColor alloc] initWithID:2 name:@"<=50" colorStr:@"00FF00"];
    [self.fusionBldgFCIDictionary setValue:fusionBldgFCIColor forKey:@">10"];
    [fusionBldgFCIColor release];
    
    fusionBldgFCIColor=[[FusionBldgFCIColor alloc] initWithID:3 name:@"<=90" colorStr:@"FFFF00"];
    [self.fusionBldgFCIDictionary setValue:fusionBldgFCIColor forKey:@">50"];
    [fusionBldgFCIColor release];
    
    
    fusionBldgFCIColor=[[FusionBldgFCIColor alloc] initWithID:4 name:@">90" colorStr:@"FF0000"];
    [self.fusionBldgFCIDictionary setValue:fusionBldgFCIColor forKey:@">90"];
    [fusionBldgFCIColor release];
    
    
}

-(void) setupFusionBldgStatusFlagColor{
    
    if(self.fusionBldgStatusFlagDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.fusionBldgStatusFlagDictionary=dictionary;
        [dictionary release];
    }

    //    if ([self.constructionStatusDictionary objectForKey:[NSString stringWithFormat:@"%d",0]]) {
    ////        NSLog(@"Align Color with space ID %d and space name: %@ already registered",space.ID,space.name);
    //    }else{
    FusionBldgStatusFlagColor* fusionBldgStatusFlagColor=[[FusionBldgStatusFlagColor alloc] initWithID:0 name:@"Not in FUSION " colorStr:@"FFFFFF"];
    [self.fusionBldgStatusFlagDictionary setValue:fusionBldgStatusFlagColor forKey:@"Not in FUSION"];
    [fusionBldgStatusFlagColor release];
    
    fusionBldgStatusFlagColor=[[FusionBldgStatusFlagColor alloc] initWithID:1 name:@"Added from FUSION" colorStr:@"FF0000"];
    [self.fusionBldgStatusFlagDictionary setValue:fusionBldgStatusFlagColor forKey:@"Added from FUSION"];
    [fusionBldgStatusFlagColor release];
    
    fusionBldgStatusFlagColor=[[FusionBldgStatusFlagColor alloc] initWithID:2 name:@"Matches FUSION" colorStr:@"00FF00"];
    [self.fusionBldgStatusFlagDictionary setValue:fusionBldgStatusFlagColor forKey:@"Matches FUSION"];
    [fusionBldgStatusFlagColor release];
    
    fusionBldgStatusFlagColor=[[FusionBldgStatusFlagColor alloc] initWithID:3 name:@"Deleted in FUSION" colorStr:@"FF9900"];
    [self.fusionBldgStatusFlagDictionary setValue:fusionBldgStatusFlagColor forKey:@"Deleted in FUSION"];
    [fusionBldgStatusFlagColor release];
    
}



-(void) setupFusionPlannedSFAssignedSF{
    
    if(self.fusionPlannedSFAssignedSFDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.fusionPlannedSFAssignedSFDictionary=dictionary;
        [dictionary release];
    }
    //    if ([self.constructionStatusDictionary objectForKey:[NSString stringWithFormat:@"%d",0]]) {
    ////        NSLog(@"Align Color with space ID %d and space name: %@ already registered",space.ID,space.name);
    //    }else{
    
  
    FusionPlannedSFAssignedSFColor* fusionPlannedSFAssignedSFColor=[[FusionPlannedSFAssignedSFColor alloc] initWithID:0 name:@"No Assigned SF" colorStr:@"FFFFFF"];
    [self.fusionPlannedSFAssignedSFDictionary setValue:fusionPlannedSFAssignedSFColor forKey:@"No Assigned SF"];
    [fusionPlannedSFAssignedSFColor release];
    
    
    fusionPlannedSFAssignedSFColor=[[FusionPlannedSFAssignedSFColor alloc] initWithID:1 name:@"Assigned SF = 0" colorStr:@"999999"];
    [self.fusionPlannedSFAssignedSFDictionary setValue:fusionPlannedSFAssignedSFColor forKey:@"Assigned SF = 0"];
    [fusionPlannedSFAssignedSFColor release];
    
    
    
    fusionPlannedSFAssignedSFColor=[[FusionPlannedSFAssignedSFColor alloc] initWithID:2 name:@"Difference < 1%" colorStr:@"00FF00"];
    [self.fusionPlannedSFAssignedSFDictionary setValue:fusionPlannedSFAssignedSFColor forKey:@"Difference < 1%"];
    [fusionPlannedSFAssignedSFColor release];
    
    fusionPlannedSFAssignedSFColor=[[FusionPlannedSFAssignedSFColor alloc] initWithID:3 name:@"Between 1% and 9%" colorStr:@"FF9900"];
    [self.fusionPlannedSFAssignedSFDictionary setValue:fusionPlannedSFAssignedSFColor forKey:@"Between 1% and 9%"];
    [fusionPlannedSFAssignedSFColor release];
    
    
    fusionPlannedSFAssignedSFColor=[[FusionPlannedSFAssignedSFColor alloc] initWithID:4 name:@"Difference >= 10%" colorStr:@"FF0000"];
    [self.fusionPlannedSFAssignedSFDictionary setValue:fusionPlannedSFAssignedSFColor forKey:@"Difference >= 10%"];
    [fusionPlannedSFAssignedSFColor release];
    
    
}


-(void) setupFusionSpaceStatusFlagColor{
    
    if(self.fusionSpaceStatusFlagDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.fusionSpaceStatusFlagDictionary=dictionary;
        [dictionary release];
    }
    
    //    if ([self.constructionStatusDictionary objectForKey:[NSString stringWithFormat:@"%d",0]]) {
    ////        NSLog(@"Align Color with space ID %d and space name: %@ already registered",space.ID,space.name);
    //    }else{
    FusionSpaceStatusFlagColor* fusionSpaceStatusFlagColor=[[FusionSpaceStatusFlagColor alloc] initWithID:0 name:@"Matches FUSION" colorStr:@"00FF00"];
    [self.fusionSpaceStatusFlagDictionary setValue:fusionSpaceStatusFlagColor forKey:@"Matches FUSION"];
    [fusionSpaceStatusFlagColor release];
    
    fusionSpaceStatusFlagColor=[[FusionSpaceStatusFlagColor alloc] initWithID:1 name:@"Deleted in FUSION" colorStr:@"FF9900"];
    [self.fusionSpaceStatusFlagDictionary setValue:fusionSpaceStatusFlagColor forKey:@"Deleted in FUSION"];
    [fusionSpaceStatusFlagColor release];
    
    fusionSpaceStatusFlagColor=[[FusionSpaceStatusFlagColor alloc] initWithID:2 name:@"Added from FUSION" colorStr:@"FF0000"];
    [self.fusionSpaceStatusFlagDictionary setValue:fusionSpaceStatusFlagColor forKey:@"Added from FUSION"];
    [fusionSpaceStatusFlagColor release];
    
    fusionSpaceStatusFlagColor=[[FusionSpaceStatusFlagColor alloc] initWithID:3 name:@"Ignore Fusion Space Number" colorStr:@"0000FF"];
    [self.fusionSpaceStatusFlagDictionary setValue:fusionSpaceStatusFlagColor forKey:@"Ignore Fusion Space Number"];
    [fusionSpaceStatusFlagColor release];
    
    fusionSpaceStatusFlagColor=[[FusionSpaceStatusFlagColor alloc] initWithID:4 name:@"Not in FUSION" colorStr:@"FFFFFF"];
    [self.fusionSpaceStatusFlagDictionary setValue:fusionSpaceStatusFlagColor forKey:@"Not in FUSION"];
    [fusionSpaceStatusFlagColor release];


}



-(void) setupFusionSpaceRecordStatusColor{
    
    if(self.fusionRecordStatusDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.fusionRecordStatusDictionary=dictionary;
        [dictionary release];
    }
    
    //    if ([self.constructionStatusDictionary objectForKey:[NSString stringWithFormat:@"%d",0]]) {
    ////        NSLog(@"Align Color with space ID %d and space name: %@ already registered",space.ID,space.name);
    //    }else{
    FusionRecordStatusColor* fusionRecordStatusColor=[[FusionRecordStatusColor alloc] initWithID:0 name:@"ASSIGNABLE" colorStr:@"CC66FF"];
    [self.fusionRecordStatusDictionary setValue:fusionRecordStatusColor forKey:@"ASSIGNABLE"];
    [fusionRecordStatusColor release];
    
    fusionRecordStatusColor=[[FusionRecordStatusColor alloc] initWithID:1 name:@"UNCLASSIFIED" colorStr:@"00FF00"];
    [self.fusionRecordStatusDictionary setValue:fusionRecordStatusColor forKey:@"UNCLASSIFIED"];
    [fusionRecordStatusColor release];
    
    fusionRecordStatusColor=[[FusionRecordStatusColor alloc] initWithID:2 name:@"DEACTIVATED" colorStr:@"FF0000"];
    [self.fusionRecordStatusDictionary setValue:fusionRecordStatusColor forKey:@"DEACTIVATED"];
    [fusionRecordStatusColor release];
    
    fusionRecordStatusColor=[[FusionRecordStatusColor alloc] initWithID:3 name:@"NON-ASSIGNABLE" colorStr:@"00FFFF"];
    [self.fusionRecordStatusDictionary setValue:fusionRecordStatusColor forKey:@"NON-ASSIGNABLE"];
    [fusionRecordStatusColor release];
    
    fusionRecordStatusColor=[[FusionRecordStatusColor alloc] initWithID:4 name:@"Not in FUSION" colorStr:@"FFFFFF"];
    [self.fusionRecordStatusDictionary setValue:fusionRecordStatusColor forKey:@"Not in FUSION"];
    [fusionRecordStatusColor release];
    
}

-(void) addFusionTopColor: (uint) keyID name:(NSString*) name color:(NSString*) color bOverWrite:(bool)bOverWrite{
    
    if(self.fusionTopDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.fusionTopDictionary=dictionary;
        [dictionary release];
    }
    if ([self.fusionTopDictionary objectForKey: [NSString stringWithFormat:@"%d",keyID]]){
        if (bOverWrite){
            [self.fusionTopDictionary removeObjectForKey:[NSString stringWithFormat:@"%d",keyID]];
        }else{
              return;
        }
    }
    FusionTopColor* fusionTopColor=[[FusionTopColor alloc] initWithID:keyID name:name colorStr:color];
    [self.fusionTopDictionary setValue:fusionTopColor forKey:[NSString stringWithFormat:@"%d",keyID]];
    [fusionTopColor release];
    fusionTopColor=nil;
    return;
}

-(void) addFusionPGMColor: (uint) keyID name:(NSString*) name color:(NSString*) color bOverWrite:(bool)bOverWrite{
    
    if(self.fusionPGMDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.fusionPGMDictionary=dictionary;
        [dictionary release];
    }
    if ([self.fusionPGMDictionary objectForKey:[NSString stringWithFormat:@"%d",keyID]]){
        if (bOverWrite){
            [self.fusionPGMDictionary removeObjectForKey:[NSString stringWithFormat:@"%d",keyID]];
        }else{
            return;
        }
    }
    FusionPGMColor* fusionPGMColor=[[FusionPGMColor alloc] initWithID:keyID name:name colorStr:color];
    [self.fusionPGMDictionary setValue:fusionPGMColor forKey:[NSString stringWithFormat:@"%d",keyID]];
    [fusionPGMColor release];
    fusionPGMColor=nil;
    return;
}

-(void) addFusionRoomUseColor: (uint) keyID name:(NSString*) name color:(NSString*) color bOverWrite:(bool)bOverWrite{
    
    if(self.fusionRoomUseDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.fusionRoomUseDictionary=dictionary;
        [dictionary release];
    }
    if ([self.fusionRoomUseDictionary objectForKey:[NSString stringWithFormat:@"%d",keyID]]){
        if (bOverWrite){
            [self.fusionRoomUseDictionary removeObjectForKey:[NSString stringWithFormat:@"%d",keyID]];
        }else{
            return;
        }
    }
    FusionRoomUseColor* fusionRoomUseColor=[[FusionRoomUseColor alloc] initWithID:keyID name:name colorStr:color];
    [self.fusionRoomUseDictionary setValue:fusionRoomUseColor forKey:[NSString stringWithFormat:@"%d",keyID]];
    [fusionRoomUseColor release];
    fusionRoomUseColor=nil;
    return;
}
//
//-(void) setupFusionTopColor{
//}
//
//-(void) setupFusionPGMColor{
//}
//
//-(void) setupFusionRoomUseColor{
//}
//


-(void) addAlignColorWithSpace:(Space*) space {

    if(alignColorDictionary==nil){
        NSMutableDictionary* dictionary=[[NSMutableDictionary alloc] init];
        self.alignColorDictionary=dictionary;
        [dictionary release];    
    }
    if ([self.alignColorDictionary objectForKey:[NSString stringWithFormat:@"%d",space.ID]]) {
        NSLog(@"Align Color with space ID %d and space name: %@ already registered",space.ID,space.name);
    }else{
        AlignColor* alignColor=[[AlignColor alloc] initWithID:space.alignID name:space.name colorStr:space.alignColorStr];
        [self.alignColorDictionary setValue:alignColor forKey:[NSString stringWithFormat:@"%d",space.ID]];
//        [self.alignColorDictionary setObject:alignColor forKey:(id) space];
        [alignColor release];
    }

}

-(void) addBldgCustomColorStruct:(CustomColorStruct*) customColorStruct {    
    if(aBldgCustomColorStruct==nil){
        NSMutableArray* aCustomColor=[[NSMutableArray alloc] init];
        self.aBldgCustomColorStruct=aCustomColor;
        [aCustomColor release];    
    }
    [self.aBldgCustomColorStruct addObject:customColorStruct];    
}
-(void) addSpaceCustomColorStruct:(CustomColorStruct*) customColorStruct {    
    if(aSpaceCustomColorStruct==nil){
        NSMutableArray* aCustomColor=[[NSMutableArray alloc] init];
        self.aSpaceCustomColorStruct=aCustomColor;
        [aCustomColor release];    
    }
    [self.aSpaceCustomColorStruct addObject:customColorStruct];    
}
-(Site*) initWithSiteSQLStmt:(uint)_siteID model:(OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform{    
//    self.model=_model;          
//    int _siteID=0; //Need rework on this siteID later

//    NSInteger sitePolylineID = sqlite3_column_int(sqlStmt, 0);    
    
    NSString* siteName= [ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:3];;
//    if ((char *) sqlite3_column_text(sqlStmt, 3)!=nil){
//        siteName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 3)];               
//    }else{
//        siteName=[NSString stringWithFormat:@""];
//    }                              
//    
    
    //                [siteName autorelease];
    
    
    NSString* siteGuid= [ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:4];
//    if ((char *) sqlite3_column_text(sqlStmt, 4)!=nil){
//        siteGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 4)];               
//    }else{
//        siteGuid=[NSString stringWithFormat:@""];
//    }     
//    
    uint siteSpatialStructID=0; //Need rework on this later
    //                [siteGuid autorelease];
    self=[super init:model ID:_siteID spatialStructureID:siteSpatialStructID guid:siteGuid name:siteName];

    if (self){

        self.hasWorldCoord= (sqlite3_column_int(sqlStmt,5) ==0)?false:true;
        self.latitude= sqlite3_column_double(sqlStmt, 6);
        self.longitude= sqlite3_column_double(sqlStmt, 7);
        
        
        CGFloat sitePlacementX = sqlite3_column_double(sqlStmt, 8);//*multiFactor;                           
        CGFloat sitePlacementY = sqlite3_column_double(sqlStmt, 9);//*multiFactor;                          
        //    CGFloat sitePlacementZ = sqlite3_column_double(sqlStmt, 7);//*multiFactor;                          
        CGFloat sitePlacementAngle = sqlite3_column_double(sqlStmt, 11); 
        
        [self setID : sqlite3_column_int(sqlStmt, 12)]; 
        
        char* pPolyStr=(char *) sqlite3_column_text(sqlStmt, 13);
        
        char* pArcStr=(char *) sqlite3_column_text(sqlStmt, 14);
        
        self.shareView= (sqlite3_column_int(sqlStmt,15) );
        self.shareEdit= (sqlite3_column_int(sqlStmt,16) );

        

        
        Department* dept1=[[Department alloc] initWithID:0 name:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:17] colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:18]];
        Department* dept2=[[Department alloc] initWithID:1 name:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:19] colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:20]];
        Department* dept3=[[Department alloc] initWithID:2 name:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:21] colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:22]];
        Department* dept4=[[Department alloc] initWithID:3 name:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:23] colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:24]];
        Department* dept5=[[Department alloc] initWithID:4 name:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:25] colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:26]];
        Department* dept6=[[Department alloc] initWithID:5 name:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:27] colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:28]];
        Department* dept7=[[Department alloc] initWithID:6 name:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:29] colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:30]];
        Department* dept8=[[Department alloc] initWithID:7 name:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:31] colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:32]];
        Department* dept9=[[Department alloc] initWithID:8 name:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:33] colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:34]];
        Department* dept10=[[Department alloc] initWithID:9 name:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:35] colorStr:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:36]];
        NSMutableArray* aDept=[[NSMutableArray alloc] initWithObjects:dept1,dept2,dept3,dept4,dept5,dept6,dept7,dept8,dept9,dept10,nil];
        [dept1 release];dept1=nil;
        [dept2 release];dept2=nil;
        [dept3 release];dept3=nil;
        [dept4 release];dept4=nil;
        [dept5 release];dept5=nil;
        [dept6 release];dept6=nil;
        [dept7 release];dept7=nil;
        [dept8 release];dept8=nil;
        [dept9 release];dept9=nil;
        [dept10 release];dept10=nil;
        
        self.aDepartment=aDept;
        [aDept release];
        
        
        
//        NSString* _currency=[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:36];
        NSString* tmpCurrency=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:37]];
        self.currency=tmpCurrency;
        [tmpCurrency release];
        
        
//        [_currency release];
        
        self.isImperial= [[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:38] isEqualToString:@"IMPERIAL"];


        
        self.dateModified= sqlite3_column_int(sqlStmt,39);
        
//        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//        [appDele setCurrentSiteSharedView:shareView];
//        [appDele setCurrentSiteSharedEdit:shareEdit];
        
        CGPoint sitePlacementPt=CGPointMake(sitePlacementX, sitePlacementY); 
        Placement* sitePlacement=[[Placement alloc] init:0 guid:nil name:nil pt:sitePlacementPt angle:sitePlacementAngle isMirrorY:false];                                
        [self setPlacement:sitePlacement];
        [sitePlacement release];
        
        
        CGAffineTransform siteTransform=CGAffineTransformConcat([self.placement toCGAffineTransform], parentTransform);                    
        
        
        
        if (pPolyStr!=0){
            NSString* polyStr= [NSString stringWithUTF8String:pPolyStr];         
//            ExtrudedAreaSolid* extrudedAreaSolid=[[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr  parentTransform:siteTransform localFrameTransform:[placement toCGAffineTransform]];
            
            NSString* arcStr=nil;
            if (pArcStr!=0){
                arcStr=[NSString stringWithUTF8String:pArcStr];
            }
//            ExtrudedAreaSolid* extrudedAreaSolid=[[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr  parentTransform:siteTransform localFrameTransform:CGAffineTransformIdentity];
            ExtrudedAreaSolid* extrudedAreaSolid=[[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr arcStr:arcStr parentTransform:siteTransform localFrameTransform:CGAffineTransformIdentity];
            
    //        ExtrudedAreaSolid* extrudedAreaSolid= [[ExtrudedAreaSolid alloc] initWithExtrudedDatabase:database polylineID:sitePolylineID parentTransform:siteTransform];
            
            
            Representation* siteRep=[[Representation alloc] init:(0) guid:(nil) name:(nil)];
            [siteRep addARepresentationItem:(extrudedAreaSolid)];                      
            [extrudedAreaSolid release];
            self.representation=siteRep;
            [siteRep release];
            
            
            [self setupConstructionStatusColor];
            
            

            [self setupFusionBldgStatusColor];
            [self setupFusionBldgFCIColor];
            [self setupFusionBldgStatusFlagColor];
            [self setupFusionPlannedSFAssignedSF];
            [self setupFusionSpaceStatusFlagColor];
            [self setupFusionSpaceRecordStatusColor];
//            [self setupFusionTopColor];
//            [self setupFusionPGMColor];
//            [self setupFusionRoomUseColor];
        }
//        [self setRepresentation:siteRep];
    }

    return self;
}




@end

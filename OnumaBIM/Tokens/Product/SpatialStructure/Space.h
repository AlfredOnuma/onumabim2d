//
//  Space.h
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpatialStructure.h"
#import <sqlite3.h>
@interface Space : SpatialStructure {
//    uint _spaceIDBeforeRefernce;
    uint spaceReferenceID;
    uint spaceReferencePolyID;
    uint spaceReferenceSpatialStructID;
    uint deptID;
    uint alignID;
    NSString* alignColorStr;
    NSString* spaceNumber;
    double spaceArea;
    uint _fusion_SpaceInfoRoomPathID;
    double _fusion_SpaceInfoAssignedSF;
    uint _fusion_ignoreFusionSpaceNumber;
    uint _roomPathID;
    uint _newAdded;
    NSString* _fusion_roomStatus;
    uint _fusion_topCssCode;
    uint _fusion_pgmNum;
    uint _fusion_roomUseCode;
    
    NSString* _fusion_topColorDescription;
    NSString* _fusion_topColorColor;
    
    NSString* _fusion_pgmColorDescription;
    NSString* _fusion_pgmColorColor;
    
    NSString* _fusion_roomUseColorDescription;
    NSString* _fusion_roomUseColorColor;
    
    //SpaceInfo.ignoreFusionSpaceNumber,SpaceInfo.newAdded,FUSION_SpaceInfo.roomStatus,FUSION_SpaceInfo.topCssCode,FUSION_SpaceInfo.pgmNum,FUSION_SpaceInfo.roomUseCode
}
//@property (nonatomic, assign) uint spaceIDBeforeRefernce;
@property (nonatomic, retain) NSString* fusion_topColorDescrption;
@property (nonatomic, retain) NSString* fusion_topColorColor;


@property (nonatomic, retain) NSString* fusion_pgmColorDescription;
@property (nonatomic, retain) NSString* fusion_pgmColorColor;

@property (nonatomic, retain) NSString* fusion_roomUseColorDescription;
@property (nonatomic, retain) NSString* fusion_roomUseColorColor;

@property (nonatomic, assign) uint fusion_SpaceInfoRoomPathID;
@property (nonatomic, assign) double fusion_SpaceInfoAssignedSF;
@property (nonatomic, assign) uint fusion_ignoreFusionSpaceNumber;
@property (nonatomic, assign) uint roomPathID;
@property (nonatomic, assign) uint newAdded;
@property (nonatomic, retain) NSString* fusion_roomStatus;
@property (nonatomic, assign) uint fusion_topCssCode;
@property (nonatomic, assign) uint fusion_pgmNum;
@property (nonatomic, assign) uint fusion_roomUseCode;

@property (nonatomic, assign) uint spaceReferenceID;
@property (nonatomic, assign) uint spaceReferencePolyID;
@property (nonatomic, assign) uint spaceReferenceSpatialStructID;
@property (nonatomic, assign) uint deptID;
@property (nonatomic, assign) uint alignID;
@property (nonatomic, retain) NSString* alignColorStr;
@property (nonatomic, retain) NSString* spaceNumber;
@property (nonatomic, assign) double spaceArea;
-(Space*) initWithSpaceSQLStmt:(OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform furnDict:(NSMutableDictionary*) furnDict;


-(Space*) initWithSpaceSQLStmt:(OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform;

- (void) registerGhostFloorSlabAndNearbySpaceToAppDele;
@end

//
//  Site.h
//  OPSMobile
//
//  Created by Alfred Man on 9/1/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpatialStructure.h"
//#import "Site.h"
//#import "CustomHeaderStruct.h"
@class Space;
@class CustomColorStruct;
@class CustomHeaderStruct;
@interface Site : SpatialStructure {
    NSString* currency;
    bool isImperial;
    double longitude;
    double latitude;
    bool hasWorldCoord;
    uint shareView;    
    uint shareEdit;       
    int dateModified;
    NSMutableArray* aDepartment;    
    NSMutableDictionary* alignColorDictionary;
    NSMutableDictionary* constructionStatusDictionary;
    NSMutableArray* aBldgCustomColorStruct;
    uint activeSpaceColorStruct;
    NSMutableArray* aSpaceCustomColorStruct;    
    uint activeCustomColorStruct;
    CustomHeaderStruct* customHeaderStruct;
    
    NSMutableDictionary* _fusionBldgStatusDictionary;
    NSMutableDictionary* _fusionBldgFCIDictionary;
    NSMutableDictionary* _fusionBldgStatusFlagDictionary;
    NSMutableDictionary* _fusionPlannedSFAssignedSFDictionary;
    NSMutableDictionary* _fusionSpaceStatusFlagDictionary;
    NSMutableDictionary* _fusionRecordStatusDictionary;
    NSMutableDictionary* _fusionTopDictionary;
    NSMutableDictionary* _fusionPGMDictionary;
    NSMutableDictionary* _fusionRoomUseDictionary;
    
    
}

@property (nonatomic, retain) NSMutableDictionary* fusionBldgStatusDictionary;
@property (nonatomic, retain) NSMutableDictionary* fusionBldgFCIDictionary;
@property (nonatomic, retain) NSMutableDictionary* fusionBldgStatusFlagDictionary;
@property (nonatomic, retain) NSMutableDictionary* fusionPlannedSFAssignedSFDictionary;
@property (nonatomic, retain) NSMutableDictionary* fusionSpaceStatusFlagDictionary;
@property (nonatomic, retain) NSMutableDictionary* fusionRecordStatusDictionary;
@property (nonatomic, retain) NSMutableDictionary* fusionTopDictionary;
@property (nonatomic, retain) NSMutableDictionary* fusionPGMDictionary;
@property (nonatomic, retain) NSMutableDictionary* fusionRoomUseDictionary;;


@property (nonatomic, assign) int dateModified;
@property (nonatomic, retain) NSString* currency;
@property (nonatomic, assign) bool isImperial;
@property (nonatomic, retain) NSMutableArray* aBldgCustomColorStruct;
@property (nonatomic, retain) NSMutableArray* aSpaceCustomColorStruct;
@property (nonatomic, assign) uint activeBldgCustomColorStruct;
@property (nonatomic, assign) uint activeSpaceCustomColorStruct;
@property (nonatomic, retain) NSMutableDictionary* alignColorDictionary;
@property (nonatomic, retain) NSMutableDictionary* constructionStatusDictionary;
@property (nonatomic, assign) uint shareView;
@property (nonatomic, assign) uint shareEdit;
@property (nonatomic, assign) bool hasWorldCoord;
@property (nonatomic, assign) double longitude;
@property (nonatomic, assign) double latitude;
@property (nonatomic, retain) NSMutableArray* aDepartment;
@property (nonatomic, retain) CustomHeaderStruct* customHeaderStruct;

-(Site*) initWithSiteSQLStmt:(uint)_siteID model:(OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform;

-(void) addAlignColorWithSpace:(Space*) space ;

-(void) addBldgCustomColorStruct:(CustomColorStruct*) customColorStruct;
-(void) addSpaceCustomColorStruct:(CustomColorStruct*) customColorStruct;

-(void) addFusionTopColor: (uint) keyID name:(NSString*) name color:(NSString*) color bOverWrite:(bool)bOverWrite;

-(void) addFusionPGMColor: (uint) keyID name:(NSString*) name color:(NSString*) color bOverWrite:(bool)bOverWrite;

-(void) addFusionRoomUseColor: (uint) keyID name:(NSString*) name color:(NSString*) color bOverWrite:(bool)bOverWrite;

//-(void) addFusionBldgStatusColor:(CustomColorStruct*)

-(void) setupFusionBldgStatusColor;
-(void) setupFusionBldgFCIColor;
-(void) setupFusionBldgStatusFlagColor;
-(void) setupFusionPlannedSFAssignedSF;
-(void) setupFusionSpaceStatusFlagColor;
-(void) setupFusionSpaceRecordStatusColor;

+(uint) numNonCustomColor;
@end

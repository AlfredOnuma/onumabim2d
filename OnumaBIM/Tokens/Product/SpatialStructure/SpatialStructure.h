//
//  SpatialStructure.h
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OPSProduct.h"
#import "RelAggregate.h"
@interface SpatialStructure : OPSProduct {    
    RelAggregate *relAggregate;    
    NSInteger spatialStructureID;    
}

@property (nonatomic, assign) NSInteger spatialStructureID;
@property (nonatomic, retain) RelAggregate *relAggregate;

- (id) init:(OPSModel*) model ID:(NSInteger) newID guid:(NSString*) newGUID name:(NSString*) newName;
- (void) addSpatialStructure: (SpatialStructure*) newSpatialStruct;

- (id) init:(OPSModel*) model ID:(NSInteger) newID spatialStructureID:(NSInteger) _spatialStructureID guid:(NSString*) newGUID name:(NSString*) newName;
//@property (nonatomic, readwrite) BOOL isDirty;
//@property (nonatomic, readwrite) BOOL 

@end

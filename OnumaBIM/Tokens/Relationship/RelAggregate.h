//
//  RelAggregate.h
//  OPSMobile
//
//  Created by Alfred Man on 7/12/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Relationship.h"
//#import "SpatialStructure.h"
@class SpatialStructure;
@interface RelAggregate : Relationship {
    
}

//- (id) init: (SpatialStructure*) relatingObj;
@end

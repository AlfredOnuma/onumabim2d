//
//  Relationship.h
//  OPSMobile
//
//  Created by Alfred Man on 7/12/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OPSObject.h"
#import "OPSToken.h"

@interface Relationship : OPSToken {
    OPSObject *relating;
    NSMutableArray*related;
    
}

@property (nonatomic, assign) OPSObject *relating;
@property (nonatomic, retain) NSMutableArray *related;

- (void) addRelatedObj: (OPSObject* ) relatedObj;
- (id) initWithRelatingObj: (OPSObject*) relatingObj;
@end

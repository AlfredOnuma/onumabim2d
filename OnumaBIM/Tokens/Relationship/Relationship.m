//
//  Relationship.m
//  OPSMobile
//
//  Created by Alfred Man on 7/12/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "Relationship.h"


@implementation Relationship
@synthesize related;
@synthesize relating;


- (void) addRelatedObj: (OPSObject* ) relatedObj{
    [relatedObj setLinkedRel:self];    
    if (self.related==nil){    
        NSMutableArray* _related=[[ NSMutableArray alloc] init];
        self.related = _related;
        [_related release];
//        self.related=[ NSMutableArray array];
    }
    [self.related addObject:relatedObj];
}
- (id) initWithRelatingObj: (OPSObject*) relatingObj{
    self=[super init];
    if (self){
        relating = relatingObj; //[ [OPSObject alloc] init];        
//        related = nil;
    }
    return self;
}

- (void)dealloc
{
    [related release];
    [super dealloc];
}

@end

//
//  OPSToken.m
//  OPSMobile
//
//  Created by Alfred Man on 7/12/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "OPSToken.h"


@implementation OPSToken
@synthesize ID=_ID;
@synthesize GUID=_GUID;
@synthesize name=_name;



- (id) init{
    self =[super init];
    if (self){
        
    }
    return self;
}


- (id) init:(NSInteger) newID guid:(NSString*) newGUID name:(NSString*) newName{
    self=[super init];
    
    if (self){
        _ID=newID;
        [_GUID release];
        _GUID=[newGUID retain];
        [_name release];
        _name=[newName retain];
        
    }
    return self;
}
- (void)dealloc
{
    [_GUID release];
    _GUID=nil;
    [_name release];
    _name=nil;
    [super dealloc];
}

@end

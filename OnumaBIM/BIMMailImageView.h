//
//  BIMMailImageView.h
//  ProjectView
//
//  Created by Alfred Man on 4/13/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIMMailImageView : UIImageView{    
//    uint attachFileID;
    NSString* path;    
}

@property (nonatomic, retain) NSString* path;
-(id) initWithImage:(UIImage *)image path:(NSString*) _path;

@end

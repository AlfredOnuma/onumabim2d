//
//  ViewModelToolBarTitleScroll.h
//  ProjectView
//
//  Created by Alfred Man on 4/17/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

@class ViewModelToolbar;
@class ViewProductRep;
@interface ViewModelToolBarTitleScroll : UIScrollView <UIScrollViewDelegate> {
    UIView* titleBarView;
    IBOutlet UIToolbar* productTitleArrayToolbar;
    NSMutableArray* aProductViewRep;
    ViewModelToolbar* viewModelToolbar;
}

@property (nonatomic, assign) ViewModelToolbar* viewModelToolbar;
@property (nonatomic, retain) UIView* titleBarView;
@property (nonatomic, retain) IBOutlet UIToolbar* productTitleArrayToolbar;
@property (nonatomic, retain) NSMutableArray* aProductViewRep;


-(NSString*) findProductRepDisplayStr: (ViewProductRep*) productRep;
-(void) updateASelectedProductRep:(NSMutableArray *) aProductRep;
-(id) initWithFrame:(CGRect)frame viewModelToolbar:(ViewModelToolbar*) _viewModelToolbar;// aProdcutRep:(NSMutableArray*)aProductRep;
@end

//
//  TableCellTitleProject.h
//  ProjectView
//
//  Created by Alfred Man on 1/3/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TableCellTitleObject.h"

@interface TableCellTitleProject : TableCellTitleObject {
    
}

-(id) init:(uint)_ID name:(NSString *)_name shared:(uint)_shared iconName:(NSString *)_iconName;
@end

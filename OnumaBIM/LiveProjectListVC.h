//
//  LiveProjectListVC.h
//  ProjectView
//
//  Created by Alfred Man on 1/5/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProjectListVC.h"
@class OPSProjectSite;
@interface LiveProjectListVC : ProjectListVC <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, NSURLConnectionDelegate>{
    NSMutableData* connectionData;
    NSURLConnection* theConnection;
    OPSProjectSite* projectSiteSelected;
    long completeFileSize;
    bool isOnlyLoadingSiteInfo;
    NSFileHandle* fileBeingDownloaded;
    
    OPSProjectSite* _projectSiteBeingUploaded;
}

@property (nonatomic, retain) OPSProjectSite* projectSiteBeingUploaded;
@property (nonatomic, retain) NSFileHandle* fileBeingDownloaded;
@property (nonatomic, assign) bool isOnlyLoadingSiteInfo;
@property (nonatomic,retain) NSURLConnection* theConnection;
@property (nonatomic,retain) NSMutableData* connectionData;
@property (nonatomic,retain) OPSProjectSite* projectSiteSelected;
@property (nonatomic,assign) long completeFileSize;

- (void)downloadSiteFromSelectedRow:(int) row bNoGraphicData:(bool) bNoGraphicData;
- (void) copyModelFromServer:(NSArray*) paramArray;//:(OPSProjectSite*) projectSite;
- (uint) readSiteListFromServer;
-(void)updateProgressBar;

-(void) tryDownloadSite;
@end

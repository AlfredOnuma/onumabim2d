//
//  OPSModel.h
//  OPSMobile
//
//  Created by Alfred Man on 7/11/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <sqlite3.h>
#import "Floor.h"
#import "OPSProduct.h"

#ifndef modelHead
#define modelHead
@protocol OPSModelDelegate

@end

@class OPSProjectSite;
@class Site;
@class Bldg;
@class Space;
@class Floor;
@class Slab;
@class Furn;
@class ExtrudedAreaSolid;
@class BIMPlanColorTVC;
@interface OPSModel : NSObject {
    bool _bHasFusionSiteData;
    bool _bHasFusionFloorData;
    double fitZoom;    
    double minimumZoom;
    double maximumZoom;
    NSString* currencyUnit;
    NSString* measureUnitLength;
    NSString* measureUnitArea;
    bool isImperial;
    
    OPSProduct *root;

    double modelScaleForScreenFactor;
    double canvasScaleForScreenFactor;
    
    uint colorMethod;
    
    
    NSMutableArray* aGhostSpace;
    NSMutableArray* aGhostFloorSlab;
//    BIMPlanColorTVC* _currentBIMPlanColorTVC;
    
//    Space* tmpSpace;
}

//@property (nonatomic, retain) Space* tmpSpace;
@property (nonatomic, assign) bool bHasFusionSiteData;
@property (nonatomic, assign) bool bHasFusionFloorData;

//@property (nonatomic, assign) BIMPlanColorTVC* currentBIMPlanColorTVC;
@property (nonatomic, retain) NSMutableArray* aGhostSpace;
@property (nonatomic, retain) NSMutableArray* aGhostFloorSlab;



@property (nonatomic, retain) NSString* currencyUnit;
@property (nonatomic, retain) NSString* measureUnitLength;
@property (nonatomic, retain) NSString* measureUnitArea;
@property (nonatomic, assign) bool isImperial;


@property (nonatomic, assign) uint colorMethod;
@property (nonatomic, assign) double fitZoom;
@property (nonatomic, assign) double minimumZoom;
@property (nonatomic, assign) double maximumZoom;




@property (nonatomic, assign) double modelScaleForScreenFactor;
@property (nonatomic, assign) double canvasScaleForScreenFactor;
@property (nonatomic, retain) OPSProduct *root;


//Static methods.
//class method

-(void) setupSpaceWithFurnInside:(NSInteger) spaceID isGhostSpace:(bool) isGhostSpace;
-(void) setupSiteModelFromProjectSite: (OPSProjectSite*) projectSite;
-(void) setupModelFromProjectSite: (OPSProjectSite*) projectSite  rootSpatialStruct:(SpatialStructure*) rootSpatialStruct;

-(void) setupSpaceModelFromSpaceID: (NSInteger) spaceID;
-(void) setupBldgModelFromBldgID:(NSInteger) bldgID  floorIndex:(NSInteger) auxID;
- (void) displayToView: (UIView*)view;
- (void) closeDBModel;
- (void) addAProductToModel:(OPSProduct*) product;


//-(Site*) readASite:(sqlite3_stmt*) siteStmt  parentTransform:(CGAffineTransform) parentTransform;
//-(Bldg*) readABldg:(sqlite3_stmt*) bldgStmt parentTransform:(CGAffineTransform) parentTransform;
//-(Floor*) readAFloor: (sqlite3_stmt*) floorStmt parentTransform:(CGAffineTransform) parentTransform;
//-(Furn*) readAFurn: (sqlite3_stmt*) furnStmt parentTransform:(CGAffineTransform) parentTransform;


@end

#endif
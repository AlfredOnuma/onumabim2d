//
//  ViewProductSprite.h
//  OPSMobile
//
//  Created by Alfred Man on 9/8/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//


#import <UIKit/UIKit.h>

//#include "OPSMobileViewController.h"

//#include "DisplayModelUI.h"
#import <Foundation/Foundation.h>
#include "OPSModel.h"
#include "OPSProduct.h"
//#include "ViewProductLabel.h"

@protocol ViewProductSpriteDelegate
//- (OPSModel*) getModel;
@end
@class ViewProductRep;
@class ViewProductLabel;
@class RepresentationItem;
@class DisplayModelUI;
@class ViewModelVC;
@interface ViewProductSprite: NSObject {    
    id <ViewProductSpriteDelegate> _delegate;
//    OPSProduct* geoProduct;
//    OPSProduct* refProduct;    
    RepresentationItem* _representationItem;
//    CGMutablePathRef _path;
//    NSMutableArray* _aPoint;
//    NSObject* _objNSMutablePath;
    UIBezierPath* _bPath;
//    CGFloat x; //x location
//    CGFloat y; //y location

//    CGFloat rotation; //rotation
//    CGFloat scale;
    CGAffineTransform _transform;
    
    CGFloat _r; //red tint
    CGFloat _g; //green tint
    CGFloat _b; //blue tint
//    CGFloat alpha; //alpha value
//    CGFloat angle; //angle
    
    CGFloat _width;
    CGFloat _height;
    
    CGRect _box; //bounding box
    BOOL _render; //True when it is being rendered
    BOOL _offScreen; //True when off the screen
    
    
    ViewProductLabel* _viewProductLabel;
    
    ViewProductRep* _viewProductRep;
//    UIImage* image;
    
    
//    UIImageView* imageView;
    //    NSMutableArray* deleteMePath;
    //    NSString* displayID;
    
}

//@property (nonatomic, assign) CGFloat x;
//@property (nonatomic, assign) CGFloat y;
//@property (nonatomic, assign) CGFloat rotation;
//@property (nonatomic, assign) CGFloat scale;
@property (nonatomic, assign) CGAffineTransform transform;
@property (nonatomic, assign) RepresentationItem* representationItem;
@property (nonatomic, assign) ViewProductRep* viewProductRep;



@property (nonatomic, assign) CGFloat r;
@property (nonatomic, assign) CGFloat g;
@property (nonatomic, assign) CGFloat b;
//@property (nonatomic, assign) CGFloat alpha;
//@property (nonatomic, assign) CGFloat angle;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGRect box;
@property (nonatomic, assign) BOOL render;
@property (nonatomic, assign) BOOL offScreen;

//@property (nonatomic, retain) UIImage* image;



//@property (nonatomic, retain) NSMutableArray* deleteMePath;

//@property (nonatomic, retain) NSString* displayID;


//@property (nonatomic, retain) NSMutableArray* viewProductLabelStrArray;
//@property (nonatomic, retain) UIImageView* imageView;

@property (nonatomic, assign) id <ViewProductSpriteDelegate> delegate;
//@property (nonatomic, assign) OPSProduct* geoProduct;
//@property (nonatomic, assign) OPSProduct* refProduct;
//@property (nonatomic, assign) CGMutablePathRef path;
//@property (nonatomic, retain) NSMutableArray* aPoint;
//@property (nonatomic, retain) NSObject* objNSMutablePath;
@property (nonatomic, retain) UIBezierPath* bPath;
@property (nonatomic, retain) ViewProductLabel* viewProductLabel;

- (BOOL)containsPoint:(CGPoint)point ;
- (void) displayProductInfo: (OPSProduct*) product;
//- (void) displayProductLabel;
//- (void) clearProductLabel;

//- (void)initExtrudedRepBaseWithGeoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*)newRefProduct;

//- (void)initBitmapRepBaseWithGeoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*)newRefProduct  ;

- (ViewModelVC*) parentViewModelVC;
- (id)initWithTransform:(CGAffineTransform) transform viewProductRep:(ViewProductRep*)viewProductRep representationItem:(RepresentationItem*) representationItem displayModelUI:( DisplayModelUI*) displayModelUI;
//- (id)initWithTransform:(CGAffineTransform) _transform  geoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*) newRefProduct displayModelUI:( DisplayModelUI*) displayModelUI;
//- (id)initWithFrame:(CGRect)frame geoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*) newRefProduct displayModelUI:( DisplayModelUI*) displayModelUI;
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
//
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

- (void) draw: (CGContextRef) context;

- (void) drawBody: (CGContextRef) context;
@end

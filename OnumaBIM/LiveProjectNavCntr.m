//
//  LiveProjectNavCntr.m
//  ProjectView
//
//  Created by Alfred Man on 11/22/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "LiveProjectNavCntr.h"
#import "LiveProjectListVC.h"
#import "ProjectViewAppDelegate.h"
#import "ViewPlanSiteAndBldgInfoVC.h"
#import "OPSProjectSite.h"
@implementation LiveProjectNavCntr



- (UIViewController *)popViewControllerAnimated:(BOOL)animated
{      
//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate]; 
//    if ([[[self viewControllers] lastObject] isKindOfClass:[LiveProjectListVC class]]){
//        LiveProjectListVC* liveProjectListVC=(LiveProjectListVC*) [[self viewControllers] lastObject];
//        if (liveProjectListVC.downloadDisableViewOverlay.alpha>0){
//            return self;
//        }
//    } 
    
    UIViewController* vc=[[self viewControllers] lastObject];
    if ([vc isKindOfClass:[ViewPlanSiteAndBldgInfoVC class]]){        
        
        UIViewController* preVC=[[self viewControllers] objectAtIndex:([[self viewControllers] count]-2 )];
        if ([preVC isKindOfClass:[LiveProjectListVC class]]){                
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication]delegate];
            OPSProjectSite* projectSite=[appDele activeProjectSite];
//        ViewPlanSiteInfoVC* siteInfoVC=(ViewPlanSiteInfoVC*) vc;


            NSFileManager* fileManager=[NSFileManager defaultManager]; 
            if (projectSite && projectSite.dbPath){
                [fileManager removeItemAtPath: projectSite.dbPath error:NULL]; 
            }
            [preVC loadView];
            
            
            
            NSMutableArray* leftButtons = [[NSMutableArray alloc] initWithCapacity:1];
            
            UIBarButtonItem *bi = [[UIBarButtonItem alloc] initWithTitle:@"Live Studios" style:UIBarButtonItemStylePlain target:self action:@selector(popViewControllerAnimated:)];
//            UIBarButtonItem *bi = [[UIBarButtonItem alloc] initWithTitle:@"Live Studios" style:UIBarButtonItemStylePlain target:self action:@selector(popViewControllerAnimated:)];
            [leftButtons addObject:bi];
            [bi release];
            bi=nil;

            
            preVC.navigationItem.leftBarButtonItems = leftButtons;
            [leftButtons release];
            leftButtons=nil;
            
//            preVC.navigationItem.backBarButtonItem = backBarButtonItem;
//            preVC.navigationItem.leftBarButtonItems=
//            [backBarButtonItem release];
//            
//
//            prev.navigationItem.leftBarButtonItems=
//            prev.navigationItem.leftBarButtonItems=leftButtons
            
//            UIImage *buttonImage=nil;
//            UIImage *buttonImageHighlight=nil;
//            UIButton *button=nil;
//            UIBarButtonItem *bi=nil;
//            
//            NSMutableArray* leftButtons = [[NSMutableArray alloc] initWithCapacity:1];
//            
//            
//            buttonImage = [UIImage imageNamed:@"Logout.png"];
//            buttonImageHighlight = [UIImage imageNamed:@"LogoutHighlight.png"];
//            button = [UIButton buttonWithType:UIButtonTypeCustom];
//            [button setImage:buttonImage forState:UIControlStateNormal];
//            [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
//            button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//            [button addTarget:self action:@selector(liveStudioBackButtonResond:) forControlEvents:UIControlEventTouchUpInside];
//            bi = [[UIBarButtonItem alloc] initWithCustomView:button];
//            //        [button release];
//            [leftButtons addObject:bi];
//            [bi release];  
//            
//            preVC.navigationItem.leftBarButtonItems = leftButtons;
//            [leftButtons release];
        }
        
    }
     
    
    return  [super popViewControllerAnimated:animated];
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
}

@end

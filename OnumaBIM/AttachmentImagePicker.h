//
//  AttachmentImagePicker.h
//  ProjectView
//
//  Created by Alfred Man on 2/28/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol AttachmentImagePickerDelegate
//- (OPSModel*) getModel;

//@end

@interface AttachmentImagePicker : UIImagePickerController
{
//    id <AttachmentImagePickerDelegate> delegate;    
}
//@property (nonatomic,assign) id <AttachmentImagePickerDelegate> delegate;
@end

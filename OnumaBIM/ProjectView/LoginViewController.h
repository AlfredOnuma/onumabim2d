//
//  LoginViewController.h
//  ProjectView
//
//  Created by Alfred Man on 10/14/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoginViewController : UIViewController {        
	IBOutlet UITextField *usernameField;
	IBOutlet UITextField *passwordField;
	IBOutlet UIButton *forgotPasswordButton;
    IBOutlet UIButton *signinButton;
    IBOutlet UIButton *signupButton;
    IBOutlet UIButton *infoButton;
	IBOutlet UIActivityIndicatorView *loginIndicator;
    UIPopoverController* _popOverController;
    IBOutlet UILabel* _versionStr;
}


@property (nonatomic, retain) UITextField *usernameField;
@property (nonatomic, retain) UITextField *passwordField;
@property (nonatomic, retain) UIButton *forgotPasswordButton;
@property (nonatomic, retain) UIButton *signinButton;
@property (nonatomic, retain) UIButton *signupButton;
@property (nonatomic, retain) UIButton *infoButton;
@property (nonatomic, retain) UIActivityIndicatorView *loginIndicator;
@property (nonatomic, retain) UIPopoverController* popOverController;
@property (nonatomic, retain) UILabel* versionStr;
- (IBAction) focusPassword: (id) sender;
- (IBAction) login: (id) sender;

-(IBAction) addLoginInfoViewController:(id) sender;

-(IBAction) addHelpViewController: (id) sender;
-(IBAction)addRegisterFreeAccountViewController:(id) sender;

-(IBAction) addForgotPWViewController:(id) sender;
@end

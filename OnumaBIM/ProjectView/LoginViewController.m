	//
//  LoginViewController.m
//  ProjectView
//
//  Created by Alfred Man on 10/14/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "LoginViewController.h"
#import "ProjectViewTabBarCtr.h"
#import "ProjectViewAppDelegate.h"
#import "LoginInfoViewController.h"
#import "AttachmentWebViewController.h"
//#import "SVWebViewController.h"
@implementation LoginViewController
@synthesize usernameField;
@synthesize passwordField;
@synthesize forgotPasswordButton;
@synthesize signupButton;
@synthesize loginIndicator;
@synthesize signinButton;
@synthesize infoButton;
@synthesize popOverController;
@synthesize versionStr=_versionStr;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setCustomNavigationBarButtons];
        
    }
    return self;
}


-(void) addHelpViewController: (id) sender{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if ([appDele isLiveDataSourceAvailable]){
        NSString* urlAddress=nil;
        
        urlAddress=@"https://www.onuma.com/products/OnumaHelp.html";
        
        
        
        
        
        
        uint width= self.view.bounds.size.width;
        uint height=self.view.bounds.size.height+44+20; //748
        
        
        AttachmentWebViewController* webViewController=[[AttachmentWebViewController alloc] initWithFrame:CGRectMake(0, 0, width, height) initURL:urlAddress hasNavControlBar:false];
        
        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
            [self.navigationController pushViewController:webViewController animated:NO];
            [webViewController release];
        } completion:^(BOOL finished) {
            
        }
         ];
        
        
    }else{
        
        NSString* urlAddress=nil;
        
        
        urlAddress=[[NSBundle mainBundle]pathForResource:@"ONUMA - Help" ofType:@"html" inDirectory:nil];
        UIWebView *web=[[UIWebView alloc]initWithFrame:self.view.frame];
        NSURL* url=[NSURL fileURLWithPath:urlAddress];
        
        //        NSString *absoluteURLwithQueryString = [urlAddress stringByAppendingString: @"#ViewLocalStudios"];
        //        NSURL *finalURL = [NSURL URLWithString: absoluteURLwithQueryString];
        
        
        [web loadRequest:[NSURLRequest requestWithURL:  url   ]];
        
        UIViewController* webViewController=[[UIViewController alloc] init];
        [webViewController setView:web];
        [web release];web=nil;
        
        
        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
            [self.navigationController pushViewController:webViewController animated:NO];
            [webViewController release];
        } completion:^(BOOL finished) {
            
        }
         ];
        
        
    }
    
}


- (void) setCustomNavigationBarButtons{
    
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:3];
    UIImage *buttonImage=nil;
    UIImage *buttonImageHighlight=nil;
    UIButton *button=nil;
    UIBarButtonItem *bi=nil;
    
    buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2.png" ofType:nil]];
    buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2Highlight.png" ofType:nil]];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(addHelpViewController:) forControlEvents:UIControlEventTouchUpInside];
    bi = [[UIBarButtonItem alloc] initWithCustomView:button];
    [buttons addObject:bi];
    [bi release];
    
    
    
    
    self.navigationItem.rightBarButtonItems=buttons;
    [buttons release];
    
    
    
    
}





-(void) addForgotPWViewController:(id) sender{
    

    
    [usernameField resignFirstResponder];
    [passwordField resignFirstResponder];

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication]delegate];
    if ([appDele isLiveDataSourceAvailable]){
    
        NSString* urlAddress=@"https://www.onuma.com/products/OnumaPassword.php";            
        AttachmentWebViewController* webViewController=[[AttachmentWebViewController alloc] initWithFrame:CGRectMake(0, 0, 1024, 748) initURL:urlAddress hasNavControlBar:false];
        
        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
            [self.navigationController pushViewController:webViewController animated:NO];
            [webViewController release];
        } completion:^(BOOL finished) {
            
        }
         ];
    }else{

        [appDele resetUserDefaults];
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Need Internet Access to Retrieve Password"
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"OK"
                              otherButtonTitles: nil];
        
        //        alert.tag = ATTACHMENT_CONFIRM_DELETE; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];
        
        [loginIndicator stopAnimating];
        loginIndicator.hidden = TRUE;
        
        
    }
    
}





-(void) addRegisterFreeAccountViewController:(id) sender{


    
    [usernameField resignFirstResponder];
    [passwordField resignFirstResponder];

    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if ([appDele isLiveDataSourceAvailable]){
    
        NSString* urlAddress=@"https://www.onuma.com/products/OnumaRegistration.php";

        
        AttachmentWebViewController* webViewController=[[AttachmentWebViewController alloc] initWithFrame:CGRectMake(0, 0, 1024, 748) initURL:urlAddress hasNavControlBar:false];

        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
            [self.navigationController pushViewController:webViewController animated:NO];
            [webViewController release];
        } completion:^(BOOL finished) {
            
        }
         ];
    }else{
        
        [appDele resetUserDefaults];
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Need Internet Access to Regsiter New User Account"
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"OK"
                              otherButtonTitles: nil];
        
        //        alert.tag = ATTACHMENT_CONFIRM_DELETE; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];
        
        [loginIndicator stopAnimating];
        loginIndicator.hidden = TRUE;
        
    }
}

/*

-(void) addHelpViewController: (id) sender{

    
    [usernameField resignFirstResponder];
    [passwordField resignFirstResponder];
 
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    if ([appDele isLiveDataSourceAvailable]){
        NSString* urlAddress=@"https://www.onuma.com/products/OnumaHelp.php#ViewSiteLocally";
        
        AttachmentWebViewController* webViewController=[[AttachmentWebViewController alloc] initWithFrame:CGRectMake(0, 0, 1024, 748) initURL:urlAddress hasNavControlBar:false];
        
        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
            [self.navigationController pushViewController:webViewController animated:NO];
            [webViewController release];
        } completion:^(BOOL finished) {
            
        }
         ];
        
        
    }else{
//        LoginInfoViewController * _loginInfoViewController = [[LoginInfoViewController alloc] initWithNibName:@"LoginInfoViewController" bundle:[NSBundle mainBundle]];
//        
//        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
//            [self.navigationController pushViewController:_loginInfoViewController animated:NO];
//            [_loginInfoViewController release];
//            
//        } completion:^(BOOL finished) {
//            
//        }
//        ];
        
    }

}

*/


-(void) addLoginInfoViewController: (id) sender{
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    [appDele addLoginInfoViewController];
//    
//    
    
    [usernameField resignFirstResponder];
    [passwordField resignFirstResponder];
    
    LoginInfoViewController * _loginInfoViewController = [[LoginInfoViewController alloc] initWithNibName:@"LoginInfoViewController" bundle:[NSBundle mainBundle]];
    
//
    
    
    
    
    [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
        [self.navigationController pushViewController:_loginInfoViewController animated:NO];
        [_loginInfoViewController release];
        
    } completion:^(BOOL finished) {
        
    }
     ];
    
//    
//    
//    [self.navigationController pushViewController:_loginInfoViewController animated:YES];
//    [_loginInfoViewController release];
    
    /*
    _loginInfoViewController.contentSizeForViewInPopover=CGSizeMake(1024, 374);
    
    UIPopoverController *popover = 
    [[UIPopoverController alloc] 
     initWithContentViewController:_loginInfoViewController]; 
    [_loginInfoViewController release];
    popover.delegate = (id) self;
    self.popOverController=popover;
    [popover release];
   
    
    [self.popOverController presentPopoverFromRect:[self.infoButton bounds] inView:self.infoButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
*/
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    if (textField.tag==2){
        [self login:nil];
        return NO;
    }
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}
- (void)dealloc
{    
    [popOverController release];
    popOverController=nil;
    [infoButton release];
    infoButton=nil;
    [signinButton release];
    signinButton=nil;
	[usernameField release];
    usernameField=nil;
	[passwordField release];
    passwordField=nil;
	[forgotPasswordButton release];
    forgotPasswordButton=nil;
    [signupButton release]; 
    signupButton=nil;
	[loginIndicator release];
    loginIndicator=nil;
    [_versionStr release];_versionStr=nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDele retrieveFromUserDefaults];
    
    if (appDele.defUserName!=Nil && appDele.defUserPW!=Nil){
        self.usernameField.text=appDele.defUserName;
        self.passwordField.text=appDele.defUserPW;
    }        
    
    [loginIndicator stopAnimating];
    loginIndicator.hidden = TRUE;

    
    self.versionStr.text=[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
//    [[button layer] setCornerRadius:12.0f];
//    [[button layer] setMasksToBounds:YES];
//    [[button layer] setBackgroundColor:[[UIColor redColor] CGColor]];
    
//    [self.signinButton layer] 
    
//    .backgroundColor=[UIColor colorWithRed:(89.0/256.0) green:(101.0/256.0) blue:(138.0/256.0) alpha:1.0];
//    self.signupButton.backgroundColor=[UIColor colorWithRed:(89.0/256.0) green:(101.0/256.0) blue:(138.0/256.0) alpha:1.0];
//    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);    
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}



// Tell the system what we support
- (NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskAllButUpsideDown;
    return UIInterfaceOrientationLandscapeRight; //UIInterfaceOrientationMaskLandscape;//UIInterfaceOrientationLandscapeLeft|UIInterfaceOrientationMaskLandscape;
}

// Tell the system It should autorotate
- (BOOL) shouldAutorotate {
    return YES;

}
//
// Tell the system which initial orientation we want to have
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}



*/




- (IBAction) focusPassword: (id) sender{
    [self.usernameField resignFirstResponder];
    [self.passwordField becomeFirstResponder];
}

- (IBAction) login: (id) sender
{

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
	
	loginIndicator.hidden = FALSE;
	[loginIndicator startAnimating];
	

//    uint errCode=0;
    uint errCode=([appDele checkStudioWithUserName:usernameField.text userPW:passwordField.text]);
    
    if (errCode==0){
    
        [loginIndicator stopAnimating];
        loginIndicator.hidden = TRUE;
        
        [appDele saveToUserDefaults:usernameField.text userPW:passwordField.text];        
        [appDele displayProjectViewFromLoginVC];
 
                                                                   
    }else{
        [appDele resetUserDefaults];
        
        
        if ([appDele isLiveDataSourceAvailable]){
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"Invalid Username/Password"
                                  message: @""
                                  delegate: self
                                  cancelButtonTitle: @"Retry"
                                  otherButtonTitles: nil];
            
    //        alert.tag = ATTACHMENT_CONFIRM_DELETE; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
            [alert show];
            [alert release];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"Username/Password is different from saved User details. Please get live connection if password is changed"
                                  message: @""
                                  delegate: self
                                  cancelButtonTitle: @"Retry"
                                  otherButtonTitles: nil];
            

            [alert show];
            [alert release];
        }
        
        [loginIndicator stopAnimating];
        loginIndicator.hidden = TRUE;
        
        
    }
    
//	loginButton.enabled = FALSE;
}


@end

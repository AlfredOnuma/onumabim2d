//
//  FirstViewController.h
//  ProjectView
//
//  Created by Alfred Man on 10/14/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
//@class LiveProjectListTableVC;

#import "LiveProjectListTableVC.h"
@interface OPSLiveController : UIViewController {

//    IBOutlet UIView* retryInternetConView;
    IBOutlet LiveProjectListTableVC* liveProjectTableView;
}
//@property (nonatomic, retain) IBOutlet UIView* retryInternetConView;
@property (nonatomic, retain) IBOutlet LiveProjectListTableVC* liveProjectTableView;
-(void) loadLiveProjectIntoTable;
@end

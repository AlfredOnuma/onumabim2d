//
//  ProjectViewAppDelegate.h
//  ProjectView
//
//  Created by Alfred Man on 10/14/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
//#import <CLLocation.h>
//@class RootViewController;
//@class DetailViewController;
//@class MGSplitViewController;
@class BIMMailVC;



@class LoginViewController;
//@class DataConnect;
@class OPSModel;
//@class Project;
@class ViewModelTabBarCtr;
@class ProjectViewTabBarCtr;
//@class NavPlanNavCntr;
//@class ViewReportNavCntr;
//@class DisplayPlanNavCntr;
//@class OPSNavUI;

@class UnitStruct;
@class NavUIScroll;
@class ViewModelVC;
@class NavModelVC;
@class SpatialStructure;
@class ViewModelNavCntr;
@class OPSProjectSite;
@class OPSStudio;
@class LoadingView;
@class Site;
@class Bldg;
@class Floor;
@class Space;
@class Slab;
@class AttachmentInfoData;
@class ViewModelToolbar;
@class LoginNavCntr;
@class AttachmentInfoDetail;
@class EverNoteGlassNote;



#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface ProjectViewAppDelegate : NSObject <UIApplicationDelegate, NSURLConnectionDelegate, UIAlertViewDelegate>{//,UIWebViewDelegate>{//, UITabBarControllerDelegate> {
    NSMutableArray* _aEverNoteLinkedNote;
    OPSModel* _activeModel;
//    uint _numFixedColorCatOnSiteLevel;
//    uint _numFixedColorCatOnSFloorLevel;
//    uint _numFixedColorCatOnSpaceLevel;
    uint modelDisplayLevel;   //0=>site || 1=>floor || 2=>Space  || 3=>Furn
    NSArray* _aVersionHistory;
    LoginNavCntr* loginViewNavCntr;
	LoginViewController *loginViewController;
//    DataConnect* dataConnecter;
    NSUserDefaults* userDef;
    NSString* defUserName;
    NSString* defUserPW;    
    IBOutlet ProjectViewTabBarCtr *projectViewTabBarCtr; 
    IBOutlet ViewModelNavCntr* viewModelNavCntr;
    
//    uint studioID;    
    bool _isEnteringNoteDetailFromSyncTable;
    OPSStudio* activeLocalStudio;
    OPSStudio* activeLiveStudio;  
    OPSStudio* activeStudio;
//    uint activeSiteID;
    uint activeStudioID;
    OPSProjectSite* activeProjectSite;
    Site* currentSite;
    Bldg* currentBldg;
    Space* currentSpace;
//    Floor* currentFloor;z
    
    UIView* bottomPopupOverlay;

    UIView* loadingOverlay;
    UITextView* loadingOverlayLabel;    
    UIActivityIndicatorView  *loadingOverlayActIndi;
    UIProgressView* loadingOverlayProgressBar;
    
    UIView* loadingOverlayBackground;
    UIView* bottomPopupOverlayBackground;
//    UIActivityIndicatorView  *bottomPopupOverlayActIndi;
    
    uint currentSiteSharedView;
    uint currentSiteSharedEdit;
    LoadingView* loadingView;
    
//    NSMutableData* connectionData;    
//    NSURLConnection* theConnection;    
//    AttachmentInfoDetail* attachmentInfoDetailToClose;

    UnitStruct* _currentUnitStruct;
//    CLLocationCoordinate2D mc;
//    CGPoint* mapCenter;
    double mapLongitude;
    double mapLatitude;
    
    NSMutableArray* aGhostSpaceID;
    NSMutableArray* aGhostFloorSlabID;
        
    CGAffineTransform rootSpaceInSpacePagePlacementTransformInverse;
    
    uint _browseLevel;
    NSMutableArray* _aBrowseIDStr;
//    CLLocationCoordinate2D mapCenter;
//    UISplitViewController *splitViewController;
//    UITableViewController *rootViewController;
//    BIMMailVC *detailViewController;
}

//@property (nonatomic, assign) uint numFixedColorCatOnSiteLevel;
//@property (nonatomic, assign) uint numFixedColorCatOnSFloorLevel;
//@property (nonatomic, assign) uint numFixedColorCatOnSpaceLevel;
@property (nonatomic, assign) OPSModel* activeModel;
@property (nonatomic, assign) uint browseLevel;
@property (nonatomic, retain) NSMutableArray* aBrowseIDStr;

@property (nonatomic, retain) NSMutableArray* aEverNoteLinkedNote;
@property (nonatomic, assign) bool isEnteringNoteDetailFromSyncTable;
@property (nonatomic, assign) CGAffineTransform rootSpaceInSpacePagePlacementTransformInverse;
@property (nonatomic, retain) NSMutableArray* aGhostSpaceID;
@property (nonatomic, retain) NSMutableArray* aGhostFloorSlabID;

@property (nonatomic, retain) NSArray* aVersionHistory;

//@property (nonatomic, retain) IBOutlet UISplitViewController *splitViewController;
//@property (nonatomic, retain) IBOutlet UITableViewController *rootViewController;
//@property (nonatomic, retain) IBOutlet BIMMailVC* detailViewController;
//@property (nonatomic, retain) IBOutlet DetailViewController *detailViewController;
@property (nonatomic, assign) double mapLongitude;
@property (nonatomic, assign) double mapLatitude;

//@property (nonatomic, assign) uint activeSiteID;
@property (nonatomic, retain) UnitStruct* currentUnitStruct;
//@property (nonatomic, assign) AttachmentInfoDetail* attachmentInfoDetailToClose;
//@property (nonatomic, retain) NSMutableData* connectionData;    
//@property (nonatomic, retain) NSURLConnection* theConnection;

@property (nonatomic, assign) Site* currentSite;
@property (nonatomic, assign) Bldg* currentBldg;
@property (nonatomic, assign) Space* currentSpace;
//@property (nonatomic, assign) Floor* currentFloor;
@property (nonatomic, assign) uint currentSiteSharedView;
@property (nonatomic, assign) uint currentSiteSharedEdit;
@property (nonatomic, assign) uint activeStudioID;
@property (nonatomic, assign) uint modelDisplayLevel;
//@property (nonatomic, assign) uint studioID;
@property (nonatomic, assign) OPSStudio* activeStudio;
@property (nonatomic, copy) OPSStudio* activeLocalStudio;
@property (nonatomic, copy) OPSStudio* activeLiveStudio;
@property (nonatomic, retain) ViewModelNavCntr* viewModelNavCntr;
@property (nonatomic, copy) OPSProjectSite* activeProjectSite;
@property (nonatomic, retain) NSString* defUserName;
@property (nonatomic, retain) NSString* defUserPW;
@property (nonatomic, retain) NSUserDefaults* userDef;
//@property (nonatomic, retain) DataConnect* dataConnecter;
@property (nonatomic, retain)  UIWindow *window;
@property (nonatomic, retain)  ProjectViewTabBarCtr* projectViewTabBarCtr;
@property (nonatomic, retain) LoginNavCntr* loginViewNavCntr;
@property (nonatomic, retain) LoginViewController *loginViewController;

@property (nonatomic, retain) UIView* bottomPopupOverlayBackground;
@property (nonatomic, retain) UIView* bottomPopupOverlay;
//@property (nonatomic, retain) UIActivityIndicatorView  *bottomPopupOverlayActIndi;

@property (nonatomic, retain) UIActivityIndicatorView  *loadingOverlayActIndi;
@property (nonatomic, retain) UIProgressView* loadingOverlayProgressBar;
@property (nonatomic, retain) UIView* loadingOverlayBackground;
//@property (nonatomic, retain) UIViewController* loadingOverlayVC;
@property (nonatomic, retain) UIView* loadingOverlay;
@property (nonatomic, retain) UITextView* loadingOverlayLabel;

@property (nonatomic, retain) LoadingView* loadingView;

-(void) removeBottomPopupOverlay;
-(void) setupProjectView;

//- (ViewModelVC*) activeViewModelVC;
-(BOOL) browseAndSelect;
-(void) resetEverNoteLinkedNote;
-(void) addEverNoteLinkedNote:(EverNoteGlassNote*)glassNote;
-(void) removeEverNoteLinkedNote:(EverNoteGlassNote*)glassNote;
-(void) createUserDirectory;
- (void) incrementDisplayLevel;
- (void) ProceedViewModelLevel: (NSInteger) rootID;
- (void) displayLoginViewFromProjectVC;
-(void) displayLoginViewFromViewModelVC;
- (void) displayProjectViewFromViewModelVC;
- (void) displayProjectViewFromLoginVC;
-(void) displayViewModelNavCntr:(SpatialStructure*) rootSpatialStruct;// (OPSModel*) model;

//- (void) displayViewModelNavCntr1: (SpatialStructure*) rootSpatialStruct bPopPreviousPage:(bool)bPopPreviousPage;
//- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController;
- (BOOL) containLocalDatabase;
-(BOOL) isLiveDataSourceAvailable;
//- (int) setupData:(NSString*) userName userPW:(NSString*) userPW;

-(void)resetUserDefaults;
-(void)saveToUserDefaults:(NSString*) userName userPW:(NSString*) userPW;
-(void)retrieveFromUserDefaults;

- (uint) checkStudioWithUserName:(NSString*)userName userPW:(NSString*) userPW;


- (void) displayViewPlanSiteInfoVC;
- (void) displayAttachmentWebView:(NSString*) urlAddress;
- (void) displayAttachmentInfoDetail:(AttachmentInfoData*) attachmentInfo;
- (void) displayProjectUploadWebView:(NSString*) urlAddress;     

- (void) displayInfoWebView:(NSString*) urlAddress;
//- (void) displayBottomPopupOverlay:(ViewModelToolbar*) toolbar  buttonID:(id) buttonID;
-(void) removeLoadingViewOverlay;

- (void) displayLoadingViewOverlay:(NSString*) loadingOverlayLabelText invoker:(id)invoker completeAction:(SEL) completeAction actionParamArray:(NSArray*) paramArray;
-(void) displayEverNoteNoteBrowser;
- (void) displayBIMMailVC;
-(void) completeSwitchingViewModelVC: (NSArray*) paramArray;




//-(NSData*) uploadSelectedProjectSite:(NSArray*) paramArray;

+ (CGAffineTransform) transformCameraUI;
+ (UIImage*) correctImageRotation:(UIImage* )src ;
//+ (void) correctImageRotation:(UIImage* )src outputImage:(UIImage*)outputImage;
//+(UIImage*)imageByRotatingImage:(UIImage*)initImage fromImageOrientation:(UIImageOrientation)orientation;
+ (NSString *)urlEncodeValue:(NSString *)str;

+(NSString*) readNameFromSQLStmt:(sqlite3_stmt*)sqlStmt column:(uint) column;

//+(NSString*) readDateFromSQLStmt:(sqlite3_stmt*)sqlStmt column:(uint) column;

+(double) meterToFeet:(double)meter;
+(double) sqMeterToSqFeet:(double)meter;


+(double) feetToMeter:(double)feet;
+(double) sqFeetToSqMeter:(double)sqFeet;
+(NSString*) toSeperatedNumberStr:(double)number numberOfFraction:(uint)numberOfFraction;


+ (NSString*) doubleToAreaStr:(double) d;
+ (NSString*) doubleToLengthStr:(double) d;
+ (NSString*) doubleToCurrenyStr:(double) d;

+ (NSString*) intToIntStr:(int) i;

+(bool) useThread;

+(UIImage *)resizeImage:(UIImage *)image width:(CGFloat)resizedWidth height:(CGFloat)resizedHeight;
//-(void) addGhostSpace:(Space*) space;
//-(void) addGhostFloorSlab:(Slab*) slab;
-(void) clearGhostElement;
-(void) resetGhostElementOnSpacePage;
- (void) displayProjectListSiteInfoVC: (UIViewController*) vc;

- (void) pushInfoWebViewToNavCntr:(NSString*) urlAddress navCntr:(UINavigationController*) navCntr;

-(void) readUnitStructWithDBPath:(NSString*) dbPath;

-(uint) readAppCurrentVersion;
-(uint) convertToVersionIntFromVersionStr:(NSString*)versionStr;



- (void) addLoginInfoViewController;



-(NSString*) getProductNumberNameComboFromProductType:(NSString*) productType productID:(uint) productID;
- (uint) reCheckUserNameAndPW;
@end

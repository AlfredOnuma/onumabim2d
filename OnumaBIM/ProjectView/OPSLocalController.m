//
//  SecondViewController.m
//  ProjectView
//
//  Created by Alfred Man on 10/14/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "OPSLocalController.h"
#import "ProjectViewAppDelegate.h"
//#import "ViewNeedLiveAccessForDL.h"
@implementation OPSLocalController


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

/*
- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
    ProjectViewAppDelegate* appDel=[[UIApplication sharedApplication] delegate];
    if ( ![appDel containLocalDatabase]){
        if ([appDel isLiveDataSourceAvailable]){
            [[appDel tabBarController] setSelectedIndex:1];
        }else{
            [self.view removeFromSuperview];
            
            
            ViewNeedLiveAccessForDL * _viewNeedLiveAccessForDL = [[ViewNeedLiveAccessForDL alloc] initWithNibName:@"ViewNeedLiveAccessForDL" bundle:Nil];
//            self.loginViewController = _loginViewController;
//            [_loginViewController release];        
//            [self.window addSubview:(self.loginViewController.view)];                         
            
            [appDel.window addSubview:_viewNeedLiveAccessForDL.view];
            [_viewNeedLiveAccessForDL release];            

        }
    }

}

*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload
{
    [super viewDidUnload];

    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc
{
    [super dealloc];
}

@end

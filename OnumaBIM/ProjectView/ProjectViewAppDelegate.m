//
//  ProjectViewAppDelegate.m
//  ProjectView
//
//  Created by Alfred Man on 10/14/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//
#import "EverNoteNoteBrowserVC.h"
#import "LoginNavCntr.h"
#import "CXMLDocument.h"
#import "ViewPlanSiteInfoVC.h"
#import "ViewPlanSiteAndBldgInfoVC.h"
//#import "ViewPlanSiteInfoNVC.h"

#import "ViewPlanBldgInfoVC.h"
#import "ViewPlanSpaceInfoVC.h"
#import "ViewPlanFurnInfoVC.h"

#import "ProjectViewAppDelegate.h"
#import "LoginInfoViewController.h"
#import "LoginViewController.h"
//#import "DataConnect.h"
#import "ViewModelNavCntr.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"
#import "OPSModel.h"
#import "AttachmentInfoData.h"
//#import "ViewModelNavTableVC.h"
//#import "ViewModelNavVC.h"
//#import "ViewModelTabBarCtr.h"
#import "ProjectViewTabBarCtr.h"

//#import "DisplayPlanNavCntr.h"
#import "ViewModelVC.h"
#import "NavPlanNavCntr.h"
//#import "ViewReportTableVC.h"
//#import "ViewReportNavCntr.h"
#import "BIMMailVC.h"
#import "../NavModelVC.h"

#import "Bldg.h"
#import "RelAggregate.h"
#import "Floor.h"
#import "Space.h"
#import "SitePolygon.h"

#import "LocalProjectNavCntr.h"
//#import "LocalProjectListTableVC.h"
#import "LiveProjectNavCntr.h"
//#import "LiveProjectListTableVC.h"
//#import "OPSNavUI.h"
#import "OPSProjectSite.h"
//#import "NavUIScroll.h"
#import "OPSStudio.h"
#import "AttachmentInfoDetail.h"
#import "LoadingView.h"
#import "Site.h"
#import "ViewModelToolbar.h"
#import "AttachmentWebView.h"
#import "LocalProjectListVC.h"
#import "LocalStudioListVC.h"
#import "AttachmentInfoDetail.h"
#import "UnitStruct.h"
#import "ViewPlanSiteAndBldgInfoVC.h"

#import "AttachmentWebViewController.h"
#import "OPSSiteVersion.h"

#import "EvernoteSession.h"
#import "ENConstants.h"

#import "EverNoteGlassNote.h"
//#import "SVWebViewController.h"
@implementation ProjectViewAppDelegate


//@synthesize numFixedColorCatOnSiteLevel=_numFixedColorCatOnSiteLevel;
//@synthesize numFixedColorCatOnSFloorLevel=_numFixedColorCatOnSFloorLevel;
//@synthesize numFixedColorCatOnSpaceLevel=_numFixedColorCatOnSpaceLevel;
//@synthesize aBrowseIDStr=_aBrowseIDStr;
@synthesize browseLevel=_browseLevel;
@synthesize aGhostFloorSlabID;
@synthesize aGhostSpaceID;

@synthesize currentUnitStruct=_currentUnitStruct;
@synthesize viewModelNavCntr;
//@synthesize dataConnecter;
@synthesize userDef;
@synthesize window=_window;
@synthesize projectViewTabBarCtr;

//@synthesize projectViewTabBarController=_tabBarController;
@synthesize loginViewNavCntr;
@synthesize loginViewController;

@synthesize defUserName;
@synthesize defUserPW;
//@synthesize model;
//@synthesize modelViewTabBarController;
@synthesize activeProjectSite;
@synthesize modelDisplayLevel;  //0=>site || 1=>floor || 2=>Space  || 3=>Furn

@synthesize activeStudio;
@synthesize activeLiveStudio;
@synthesize activeLocalStudio;

@synthesize isEnteringNoteDetailFromSyncTable=_isEnteringNoteDetailFromSyncTable;

@synthesize loadingOverlay;
@synthesize loadingOverlayActIndi;
@synthesize loadingOverlayLabel;
@synthesize loadingOverlayProgressBar;
@synthesize loadingOverlayBackground;;
@synthesize loadingView;
@synthesize activeStudioID;

@synthesize activeModel=_activeModel;
//@synthesize rootViewController;
//@synthesize detailViewController;
//@synthesize splitViewController;

@synthesize bottomPopupOverlay;
@synthesize bottomPopupOverlayBackground;

@synthesize currentSiteSharedEdit;
@synthesize currentSiteSharedView;

@synthesize currentSite;
@synthesize currentBldg;
@synthesize currentSpace;

@synthesize mapLatitude;
@synthesize mapLongitude;

@synthesize aVersionHistory=_aVersionHistory;
@synthesize aEverNoteLinkedNote=_aEverNoteLinkedNote;

@synthesize rootSpaceInSpacePagePlacementTransformInverse;
//@synthesize theConnection;
//@synthesize connectionData;
//@synthesize attachmentInfoDetailToClose;
uint const UIAlertViewTag_NoInternetConfirm=0;
//uint const APPDELEGATE_CONFIRM_UPLOADNOINTERNET=1;
uint const AppDele_USER_PW_INVALID=1;

-(BOOL) selectBldgByID:(int)bldgID{
    

    if ([self viewModelNavCntr]==nil) {
        return FALSE;
    }
    if ([[self viewModelNavCntr] viewControllers]==nil){
        return FALSE;
    }
    if (! [[[[self viewModelNavCntr] viewControllers] lastObject] isKindOfClass:[ViewModelVC class]]){
        return FALSE;
    }
    ViewModelVC* viewModelVC=[[[self viewModelNavCntr] viewControllers] lastObject];
    [viewModelVC clearSelectedProductRep];
    
    Site* site=(Site*)[[viewModelVC model]root];
    for (OPSProduct* product in [[site relAggregate] related]){
        if ([product isKindOfClass:[Bldg class]]){
            Bldg* bldg=(Bldg*) product;
            if ([bldg ID]==bldgID){
                ViewProductRep* rep=[bldg firstViewProductRep];
                [viewModelVC selectAProductRep:rep] ;
                return TRUE;
            }
        }
    }

    return FALSE;
}




-(BOOL) selectAndBrowseBldgWithID:(int)bldgID invoker:(id) invoker completeAction:(SEL) completeAction actionParamArray:(NSArray*) paramArray{
    bool selectSucuess=[self selectBldgByID:bldgID];
    if (selectSucuess){
        
        if ([self viewModelNavCntr]==nil) {
            return FALSE;
        }
        if ([[self viewModelNavCntr] viewControllers]==nil){
            return FALSE;
        }
        if (! [[[[self viewModelNavCntr] viewControllers] lastObject] isKindOfClass:[ViewModelVC class]]){
            return FALSE;
        }
        ViewModelVC* viewModelVC=[[[self viewModelNavCntr] viewControllers] lastObject];
        [[viewModelVC viewModelToolbar] browse:nil];
    }
    return selectSucuess;
}


//- (void) displayCamera:(NSString*) loadingOverlayLabelText invoker:(id)invoker completeAction:(SEL) completeAction actionParamArray:(NSArray*) paramArray{


//
//
//[UIView animateWithDuration:0.001
//                      delay:0.0
//                    options:UIViewAnimationOptionBeginFromCurrentState
//                 animations:^{
//                     //animation block
//                     bottomPopupOverlayBackground.alpha = 0.6;
//                     //                         loadingView.view.alpha=0.6;
//                 }
//                 completion:^(BOOL finished){//this block starts only when
//                     //                         [@selector( overlayFinishFunction )];
//                     [toolbar displayAttachInfoTable:buttonID];
//                     //                         [toolbar performSelector:@selector(overlayFinishFunction:)
//                     //                                      withObject:aNeighbor];
//                 }];


//-(BOOL) selectByLevel:(int)levelID selectElemIDString:(NSArray*)aIDs{
-(BOOL) browseAndSelect{
    
    
    if ([self browseLevel]==0){
        return false;
    }
    if ([self viewModelNavCntr]==nil) {
        return FALSE;
    }
    if ([[self viewModelNavCntr] viewControllers]==nil){
        return FALSE;
    }
    if (! [[[[self viewModelNavCntr] viewControllers] lastObject] isKindOfClass:[ViewModelVC class]]){
        return FALSE;
    }
    //        ViewModelVC* viewModelVC=[[[self viewModelNavCntr] viewControllers] lastObject];
    
    
    ViewModelVC* viewModelVC=[[[self viewModelNavCntr] viewControllers] lastObject];
    
    
    //level 1 is site, level 2 is bldg, level 3 is floor, level 4 is space and level 5 is furn
    
    if ([self modelDisplayLevel]==0){
        //                    int elemID=(int) [idStr intValue];
        if (self.browseLevel==1){//site
            [viewModelVC clearSelectedProductRep];
            Site* site=(Site*)[[viewModelVC model]root];
            ViewProductRep* rep=[site firstViewProductRep];
            [viewModelVC selectAProductRep:rep] ;
            self.browseLevel=0;
            return true;
        }
        if (self.browseLevel==2){//bldg
            int elemID=(int) [(NSString*)[self.aBrowseIDStr objectAtIndex:0] intValue];
            [viewModelVC clearSelectedProductRep];
            
            Site* site=(Site*)[[viewModelVC model]root];
            for (OPSProduct* product in [[site relAggregate] related]){
                if ([product isKindOfClass:[Bldg class]]){
                    Bldg* bldg=(Bldg*) product;
                    if ([bldg ID]==elemID){
                        ViewProductRep* rep=[bldg firstViewProductRep];
                        [viewModelVC selectAProductRep:rep] ;
                        self.browseLevel=0;
                        return true;
                    }
                }
            }
            self.browseLevel=0;
            return false;
        }
        
        if (self.browseLevel==3 || self.browseLevel==4 || self.browseLevel==5){ //floor or space or furn
            
            int bldgID=(int) [(NSString*)self.aBrowseIDStr[0] intValue];
            int floorIndex=(int) [(NSString*)self.aBrowseIDStr[1] intValue];

            //                        NSLog(@"bldgID:%d floorID:%d spaceID: %d furnID:%d",bldgID,floorID,spaceID,furnID);
            [viewModelVC clearSelectedProductRep];
            
            Site* site=(Site*)[[viewModelVC model]root];
            for (OPSProduct* product in [[site relAggregate] related]){
                if ([product isKindOfClass:[Bldg class]]){
                    Bldg* bldg=(Bldg*) product;
                    if ([bldg ID]==bldgID){                       
                        ViewProductRep* rep=[bldg firstViewProductRep];
                        [viewModelVC selectAProductRep:rep] ;
                        [bldg setSelectedFloorIndex:floorIndex];
                        
                        [[viewModelVC viewModelToolbar] browse:nil];
                        return false;

                    }
                }
            }
            //if no bldg is found, reset browselevel and return
            self.browseLevel=0;
            return false;
            
            
            //                        Site* site=(Site*)[[viewModelVC model]root];
            //                        for (OPSProduct* product in [[site relContain] related]){
            //                            if ([product isKindOfClass:[SitePolygon class]]){
            //                                SitePolygon* sitePolygon=(SitePolygon*) product;
            //                                if ([sitePolygon ID]==elemID){
            //                                    ViewProductRep* rep=[sitePolygon firstViewProductRep];
            //                                    [viewModelVC selectAProductRep:rep] ;
            //                                }
            //                            }
            //                        }
        }
        if (self.browseLevel==6){ //SitePolygon
            int elemID=(int) [(NSString*)[self.aBrowseIDStr objectAtIndex:0] intValue];
            [viewModelVC clearSelectedProductRep];
            Site* site=(Site*)[[viewModelVC model]root];
            for (OPSProduct* product in [[site relContain] related]){
                if ([product isKindOfClass:[SitePolygon class]]){
                    SitePolygon* sitePolygon=(SitePolygon*) product;
                    if ([sitePolygon ID]==elemID){
                        ViewProductRep* rep=[sitePolygon firstViewProductRep];
                        [viewModelVC selectAProductRep:rep] ;
                        self.browseLevel=0;
                        return true;
                    }
                }
            }
            self.browseLevel=0;
            return false;
            
        }        
        
        
        
        

    }else if (modelDisplayLevel==1){
        if (self.browseLevel==1){//site
            [[viewModelVC navigationController] popViewControllerAnimated:true];
            return false;
        }else if (self.browseLevel==2){//bldg
            [[viewModelVC navigationController] popViewControllerAnimated:true];
            return false;
        }else if (self.browseLevel==3){//Floor || self.browseLevel==4 || self.browseLevel==5){ //floor or space or furn
            //                int bldgID=(int) [(NSString*)self.aBrowseIDStr[0] intValue];
            //                int floorIndex=(int) [(NSString*)self.aBrowseIDStr[1] intValue];
            //            int spaceID=(int) [(NSString*)self.aBrowseIDStr[2] intValue];
            //            int furnID=(int) [(NSString*)self.aBrowseIDStr[3] intValue];
            
            //                        NSLog(@"bldgID:%d floorID:%d spaceID: %d furnID:%d",bldgID,floorID,spaceID,furnID);
            [viewModelVC clearSelectedProductRep];
            
            
            
            uint bldgID=(int) [(NSString*)self.aBrowseIDStr[0] intValue];
            uint floorIndex=(int) [(NSString*)self.aBrowseIDStr[1] intValue];
            
            
            Bldg* bldg=(Bldg*)[[viewModelVC model]root];
            
            if (bldg.ID!=bldgID){
                [[viewModelVC navigationController] popViewControllerAnimated:true];
                return false;
            }            
            if ([bldg selectedFloorIndex]!=floorIndex){
                [[viewModelVC navigationController] popViewControllerAnimated:true];
                return false;
            }
            
            ViewProductRep* rep=[bldg firstViewProductRep];
            [viewModelVC selectAProductRep:rep];
            self.browseLevel=0;
            return true;
            
        }else if (self.browseLevel==4){ //Space
            //                int bldgID=(int) [(NSString*)self.aBrowseIDStr[0] intValue];
            //                int floorIndex=(int) [(NSString*)self.aBrowseIDStr[1] intValue];
            //            int spaceID=(int) [(NSString*)self.aBrowseIDStr[2] intValue];
            //            int furnID=(int) [(NSString*)self.aBrowseIDStr[3] intValue];
            
            //                        NSLog(@"bldgID:%d floorID:%d spaceID: %d furnID:%d",bldgID,floorID,spaceID,furnID);
            [viewModelVC clearSelectedProductRep];
            
            
            
            uint bldgID=(int) [(NSString*)self.aBrowseIDStr[0] intValue];
            uint floorIndex=(int) [(NSString*)self.aBrowseIDStr[1] intValue];
            
            
            Bldg* bldg=(Bldg*)[[viewModelVC model]root];
            
            if (bldg.ID!=bldgID){
                [[viewModelVC navigationController] popViewControllerAnimated:true];
                return false;
            }
            if ([bldg selectedFloorIndex]!=floorIndex){
                [[viewModelVC navigationController] popViewControllerAnimated:true];
                return false;
            }

            
            
//            ViewProductRep* rep=[bldg firstViewProductRep];
//            [viewModelVC selectAProductRep:rep];
            Floor* floor=(Floor*) [bldg selectedFloor];
            
            uint spaceID=(int) [(NSString*)self.aBrowseIDStr[2] intValue];
            for (OPSProduct* product in [[floor relAggregate] related]){
                if ([product isKindOfClass:[Space class]]){
                    Space* space=(Space*) product;
                    if ([space ID]==spaceID){
                        ViewProductRep* rep=[space firstViewProductRep];
                        [viewModelVC selectAProductRep:rep] ;
                        self.browseLevel=0;
                        return true;
                    }
                }
            }
            
            self.browseLevel=0;
            
            
            return true;
            
        }else if (self.browseLevel==5){ //Furn
            
            
            uint bldgID=(int) [(NSString*)self.aBrowseIDStr[0] intValue];
            uint floorIndex=(int) [(NSString*)self.aBrowseIDStr[1] intValue];
            
            
            [viewModelVC clearSelectedProductRep];
            
            Bldg* bldg=(Bldg*)[[viewModelVC model]root];
            //            ViewProductRep* rep=[bldg firstViewProductRep];
            //            [viewModelVC selectAProductRep:rep];
            Floor* floor=(Floor*) [bldg selectedFloor];
            
            
            if (bldg.ID!=bldgID){
                [[viewModelVC navigationController] popViewControllerAnimated:true];
                return false;
            }
            if ([bldg selectedFloorIndex]!=floorIndex){
                [[viewModelVC navigationController] popViewControllerAnimated:true];
                return false;
            }
            
            uint spaceID=(int) [(NSString*)self.aBrowseIDStr[2] intValue];
            for (OPSProduct* product in [[floor relAggregate] related]){
                if ([product isKindOfClass:[Space class]]){
                    Space* space=(Space*) product;
                    if ([space ID]==spaceID){
                        ViewProductRep* rep=[space firstViewProductRep];
                        [viewModelVC selectAProductRep:rep] ;
                        
                        [[viewModelVC viewModelToolbar] browse:nil];
                        return false;
                        break;

                    }
                }
            }
            //If no space is found on this floor plan, pop 
            [[viewModelVC navigationController] popViewControllerAnimated:true];
            return false;
        }
        
        if (self.browseLevel==6){ //SitePolygon
            [[viewModelVC navigationController] popViewControllerAnimated:true];
            return false;            
        }
    }else if (modelDisplayLevel==2){
        if (self.browseLevel==1){//site
            [[viewModelVC navigationController] popViewControllerAnimated:true];
            return false;
        }else if (self.browseLevel==2){//bldg
            [[viewModelVC navigationController] popViewControllerAnimated:true];
            return false;
        }else if (self.browseLevel==3){//Floor || self.browseLevel==4 || self.browseLevel==5){ //floor or space or furn
            [[viewModelVC navigationController] popViewControllerAnimated:true];
            return false;            
        }else if (self.browseLevel==4){ //Space
            //                int bldgID=(int) [(NSString*)self.aBrowseIDStr[0] intValue];
            //                int floorIndex=(int) [(NSString*)self.aBrowseIDStr[1] intValue];
            //            int spaceID=(int) [(NSString*)self.aBrowseIDStr[2] intValue];
            //            int furnID=(int) [(NSString*)self.aBrowseIDStr[3] intValue];
            
            //                        NSLog(@"bldgID:%d floorID:%d spaceID: %d furnID:%d",bldgID,floorID,spaceID,furnID);

            
            uint spaceID=(int) [(NSString*)self.aBrowseIDStr[2] intValue];
            Space* space=(Space*)[[viewModelVC model]root];
            
            
            
            if (space.ID==spaceID){
                [viewModelVC clearSelectedProductRep];
                

                
                Space* space=(Space*)[[viewModelVC model]root];
                ViewProductRep* rep=[space firstViewProductRep];
                [viewModelVC selectAProductRep:rep] ;
                self.browseLevel=0;
                return true;
            }else{
                [[viewModelVC navigationController] popViewControllerAnimated:true];
                return false;
            }
            
            
            
        }else if (self.browseLevel==5){ //Furn
            [viewModelVC clearSelectedProductRep];
            
            uint spaceID=(int) [(NSString*)self.aBrowseIDStr[2] intValue];
            
            Space* space=(Space*)[[viewModelVC model]root];
            if (space.ID!=spaceID){
                [[viewModelVC navigationController] popViewControllerAnimated:true];
                return false;
                
            }
            //            ViewProductRep* rep=[bldg firstViewProductRep];
            //            [viewModelVC selectAProductRep:rep];
                        
            uint furnID=(int) [(NSString*)self.aBrowseIDStr[3] intValue];
            for (OPSProduct* product in [[space relContain] related]){
                if ([product isKindOfClass:[Furn class]]){
                    Furn* furn=(Furn*) product;
                    if ([furn ID]==furnID){
                        ViewProductRep* rep=[furn firstViewProductRep];
                        [viewModelVC selectAProductRep:rep] ;

                        break;
                    }
                }
            }
            self.browseLevel=0;
            return TRUE;
        }
        
        if (self.browseLevel==6){ //SitePolygon            
            [[viewModelVC navigationController] popViewControllerAnimated:true];
            return false;
        }
    }

    return false;
}



-(BOOL) application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    BOOL canHandle=NO;
    if ([[NSString stringWithFormat:@"en-%@",[[EvernoteSession sharedSession] consumerKey]] isEqualToString:[url scheme]] == YES){
        canHandle=[[EvernoteSession sharedSession] canHandleOpenURL:url];
    }
//    else if ([[url absoluteString] hasPrefix:@"OnumaBim"]){
    else if ([[url scheme] isEqualToString:@"onumabim"]){

        
        if ([self viewModelNavCntr]==nil) {
            return FALSE;
        }
        if ([[self viewModelNavCntr] viewControllers]==nil){
            return FALSE;
        }
        if (! [[[[self viewModelNavCntr] viewControllers] lastObject] isKindOfClass:[ViewModelVC class]]){
            return FALSE;
        }
//        ViewModelVC* viewModelVC=[[[self viewModelNavCntr] viewControllers] lastObject];
        

//        ViewModelVC* viewModelVC=[[[self viewModelNavCntr] viewControllers] lastObject];
        NSDictionary* parameters= [self parseQUeryString:[url query]];
        NSString* studioIDStr=[parameters valueForKey:@"studioID"];
        NSString* siteIDStr=[parameters valueForKey:@"siteID"];
        NSString* levelIDStr=[parameters valueForKey:@"level"];
        NSString* idStr=[parameters valueForKey:@"ID"];

        
        
        

        
        
        if (studioIDStr!=nil&&siteIDStr!=nil){
            int studioID=(int) [studioIDStr intValue];
            int siteID=(int) [siteIDStr intValue];
//            int elemID=(int) [idStr intValue];    
            
            
            if (studioID==[[self activeStudio] ID] && siteID==[[self activeProjectSite] ID] ){
                
//                int levelID=(int) [levelIDStr intValue];                
                self.browseLevel=(uint) [levelIDStr intValue];
                NSMutableArray* aIDs=[[NSMutableArray alloc] initWithArray: [idStr componentsSeparatedByString:@":"]];
                self.aBrowseIDStr=aIDs;
                [aIDs release];
                
                
                [self browseAndSelect];
                
            }
            
        }
        canHandle=TRUE;
        

    }
    return canHandle;
}

- (NSDictionary*) parseQUeryString:(NSString*) query{
    NSMutableDictionary *dict=[[[NSMutableDictionary alloc]init] autorelease];
    NSArray* pairs=[query componentsSeparatedByString:@"&"];
    for (NSString *pair in pairs){
        NSArray* elements=[pair componentsSeparatedByString:@"="];
        NSString* key=[[elements objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF16BigEndianStringEncoding];
        NSString* value=[[elements objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF16BigEndianStringEncoding];
        [dict setObject:value forKey:key];
        
    }
    return dict;
    
}
-(void) resetEverNoteLinkedNote{
    [_aEverNoteLinkedNote release];_aEverNoteLinkedNote=nil;
}
-(void) addEverNoteLinkedNote:(EverNoteGlassNote*)glassNote{
    if (self.aEverNoteLinkedNote==nil){
        NSMutableArray* aEverNoteLinkedNote=[[NSMutableArray alloc] init];
        self.aEverNoteLinkedNote=aEverNoteLinkedNote;
        [aEverNoteLinkedNote release];
    }
    for (EverNoteGlassNote* thisGlassNote in self.aEverNoteLinkedNote){
        if ([thisGlassNote.noteGUID isEqualToString:glassNote.noteGUID] && [thisGlassNote.notebookGUID isEqualToString:glassNote.notebookGUID]){
            return;
        }
    }
    [self.aEverNoteLinkedNote addObject:glassNote];
}
-(void) removeEverNoteLinkedNote:(EverNoteGlassNote*)glassNote{
    if (self.aEverNoteLinkedNote==nil){
        return;
    }

    for (EverNoteGlassNote* thisGlassNote in self.aEverNoteLinkedNote){
        if (thisGlassNote==glassNote){
            [self.aEverNoteLinkedNote removeObject:glassNote];
        }
    }
}
+ (CGAffineTransform) transformCameraUI{

    CGAffineTransform cameraTransform=CGAffineTransformIdentity;
    UIInterfaceOrientation orient=[UIApplication sharedApplication].statusBarOrientation;

//    UIDeviceOrientation deviceOrient = [[UIDevice currentDevice] orientation];

    //        UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
        case UIInterfaceOrientationLandscapeLeft:

            //EXIF = 3
        {
            
            cameraTransform = CGAffineTransformMakeRotation(180*M_PI/180);;
            /*
            NSLog(@"UIInterfaceOrientationLandscapeLeft");
            
            
            switch (deviceOrient){
                case UIDeviceOrientationUnknown:
                {
                    NSLog(@"UIDeviceOrientationUnknown");
                    cameraTransform = CGAffineTransformIdentity;
                }
                    break;
                case UIDeviceOrientationPortrait:
                {
                    NSLog(@"UIDeviceOrientationPortrait");
                    //                    cameraTransform = CGAffineTransformMakeRotation(-90*M_PI/180);;
                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
                }
                    break;
                case UIDeviceOrientationPortraitUpsideDown:
                {
                    NSLog(@"UIDeviceOrientationPortraitUpsideDown");
                    cameraTransform = CGAffineTransformMakeRotation(-90*M_PI/180);
                    //                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
                }
                    break;
                case UIDeviceOrientationLandscapeLeft:
                {
                    NSLog(@"UIDeviceOrientationLandscapeLeft");
                    cameraTransform = CGAffineTransformIdentity;
                }
                    break;
                case UIDeviceOrientationLandscapeRight:
                {
                    NSLog(@"UIDeviceOrientationLandscapeRight");
                    //                    cameraTransform = CGAffineTransformIdentity;
                }
                    break;
                case UIDeviceOrientationFaceUp:
                {
                    NSLog(@"UIDeviceOrientationFaceUp");
                    cameraTransform = CGAffineTransformIdentity;
                }
                    break;
                case UIDeviceOrientationFaceDown:
                {
                    NSLog(@"UIDeviceOrientationFaceDown");
                    cameraTransform = CGAffineTransformIdentity;
                }
                    break;
                default:
                    cameraTransform = CGAffineTransformIdentity;
                    break;
            }
            
         */   
        }
            break;
/*
        case UIInterfaceOrientationLandscapeRight: //EXIF = 4
        {
            NSLog(@"UIInterfaceOrientationLandscapeRight");

            switch (deviceOrient) {
                case UIDeviceOrientationUnknown:
                {
                    NSLog(@"UIDeviceOrientationUnknown");
                    cameraTransform = CGAffineTransformIdentity;
                }
                    break;
                case UIDeviceOrientationPortrait:
                {
                    NSLog(@"UIDeviceOrientationPortrait");
                    cameraTransform = CGAffineTransformMakeRotation(-90*M_PI/180);;
                    //                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
                }
                    break;
                case UIDeviceOrientationPortraitUpsideDown:
                {
                    NSLog(@"UIDeviceOrientationPortraitUpsideDown");
                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
                }
                    break;
                case UIDeviceOrientationLandscapeLeft:
                {
                    NSLog(@"UIDeviceOrientationLandscapeLeft");
                    //                    cameraTransform = CGAffineTransformIdentity;
                }
                    break;
                case UIDeviceOrientationLandscapeRight:
                {
                    NSLog(@"UIDeviceOrientationLandscapeRight");
                    cameraTransform = CGAffineTransformIdentity;
                }
                    break;
                case UIDeviceOrientationFaceUp:
                {
                    NSLog(@"UIDeviceOrientationFaceUp");
                    cameraTransform = CGAffineTransformIdentity;
                }
                    break;
                case UIDeviceOrientationFaceDown:
                {
                    NSLog(@"UIDeviceOrientationFaceDown");
                    cameraTransform = CGAffineTransformIdentity;
                }
                    break;
                default:
                    cameraTransform = CGAffineTransformIdentity;
                    break;
             
            }
            
            
            //            cameraTransform = CGAffineTransformIdentity;
        }
            break;
            */
        default:
            cameraTransform = CGAffineTransformIdentity;
            break;
    }

    return cameraTransform;
}

/*
+ (void) correctImageRotation:(UIImage* )src outputImage:(UIImage*)outputImage{
    
    UIImageOrientation orientation = src.imageOrientation;
    
    UIGraphicsBeginImageContext(src.size);
    
//    [src drawAtPoint:CGPointMake(0, 0)];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, M_PI);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, M_PI);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, 0);
    }
    UIImage* image=UIGraphicsGetImageFromCurrentImageContext();
    outputImage = [[image copy] autorelease];
    [image release];
    
    CGContextRestoreGState(context);
    //    CGContextRelease(context);

}
 */

+ (UIImage*) correctImageRotation:(UIImage* )src {
    
    UIImageOrientation orientation = src.imageOrientation;
    
    UIGraphicsBeginImageContext(src.size);
    
    [src drawAtPoint:CGPointMake(0, 0)];

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, M_PI);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, M_PI);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, 0);
    }
    UIImage* image=UIGraphicsGetImageFromCurrentImageContext();
    CGContextRestoreGState(context);
//    CGContextRelease(context);
    return image;
}




//+ (UIImage*) correctImageRotation:(UIImage* )src {
//    
//    UIImageOrientation orientation = src.imageOrientation;
//    UIGraphicsBeginImageContext(src.size);
//    [src drawAtPoint:CGPointMake(0, 0)];
//    
//    CGContextRef bitmap = UIGraphicsGetCurrentContext();
//    CGContextSaveGState(bitmap);
//    if (orientation == UIImageOrientationRight) {
//        CGContextRotateCTM (bitmap, M_PI);
//    } else if (orientation == UIImageOrientationLeft) {
//        CGContextRotateCTM (bitmap, M_PI);
//    } else if (orientation == UIImageOrientationDown) {
//        // NOTHING
//    } else if (orientation == UIImageOrientationUp) {
//        CGContextRotateCTM (bitmap, 0);
//    }
//    UIImage* image=UIGraphicsGetImageFromCurrentImageContext();
//    CGContextRestoreGState(bitmap);
//    
//    
//
//    CGContextDrawImage(bitmap, CGRectMake(-image.size.width / 2, -image.size.height / 2, image.size.width, image.size.height), [image CGImage]);
//
//    correctedImg=[ [UIImage alloc] initWithCGImage: UIGraphicsGetImageFromCurrentImageContext().CGImage];
//    UIGraphicsEndImageContext();
//}
/*
+ (UIImage*) correctImageRotation:(UIImage* )src {
    UIImageOrientation orientation = src.imageOrientation;
    UIGraphicsBeginImageContext(src.size);
    [src drawAtPoint:CGPointMake(0, 0)];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, M_PI);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, M_PI);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, 0);
    }
    UIImage* image=UIGraphicsGetImageFromCurrentImageContext();
    CGContextRestoreGState(context);
    
//    CGContextRelease(context);
    return image;
}
 */



/*

+(UIImage*)imageByRotatingImage:(UIImage*)initImage fromImageOrientation:(UIImageOrientation)orientation
{
    CGImageRef imgRef = initImage.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = orientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            return initImage;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
//            transform= CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    // Create the bitmap context
    CGContextRef    context = NULL;
    void *          bitmapData;
    int             bitmapByteCount;
    int             bitmapBytesPerRow;
    
    // Declare the number of bytes per row. Each pixel in the bitmap in this
    // example is represented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    bitmapBytesPerRow   = (bounds.size.width * 4);
    bitmapByteCount     = (bitmapBytesPerRow * bounds.size.height);
    bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        return nil;
    }
    
    // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
    // per component. Regardless of what the source image format is
    // (CMYK, Grayscale, and so on) it will be converted over to the format
    // specified here by CGBitmapContextCreate.
    CGColorSpaceRef colorspace = CGImageGetColorSpace(imgRef);
    context = CGBitmapContextCreate (bitmapData,bounds.size.width,bounds.size.height,8,bitmapBytesPerRow,
                                     colorspace,kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colorspace);
    
    if (context == NULL)
        // error creating context
        return nil;
    
    CGContextScaleCTM(context, -1.0, -1.0);
    CGContextTranslateCTM(context, -bounds.size.width, -bounds.size.height);
    
    CGContextConcatCTM(context, transform);
    
    // Draw the image to the bitmap context. Once we draw, the memory
    // allocated for the context for rendering will then contain the
    // raw image data in the specified color space.
    CGContextDrawImage(context, CGRectMake(0,0,width, height), imgRef);
    
    CGImageRef imgRef2 = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    free(bitmapData);
    UIImage * image = [UIImage imageWithCGImage:imgRef2 scale:initImage.scale orientation:UIImageOrientationUp];
    CGImageRelease(imgRef2);
    return image;
}

*/
-(NSString*) getProductNumberNameComboFromProductType:(NSString*) productType productID:(uint) productID{
    
    NSString* productNumber=nil;
    NSString* productName=nil;
    sqlite3 * database=nil;
    
    if (sqlite3_open ([[[self activeProjectSite] dbPath] UTF8String], &database) == SQLITE_OK) {
        
        if ([productType isEqualToString:@"Space"]){
            const char* numberNameQuerySQL=[[NSString stringWithFormat:   @"SELECT name,spaceNumber from space LEFT JOIN spatialStructure WHERE spatialStructure.ID=space.spatialStructureID AND space.ID=%d",productID] UTF8String];
            sqlite3_stmt *numberNameQueryStmt;
            if(sqlite3_prepare_v2 (database, numberNameQuerySQL, -1, &numberNameQueryStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(numberNameQueryStmt) == SQLITE_ROW) {
                    productName=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:0];
                    productNumber=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:1];
                    
                }
            }            
            sqlite3_finalize(numberNameQueryStmt);
        }
        
        if ([productType isEqualToString:@"Bldg"]){
            const char* numberNameQuerySQL=[[NSString stringWithFormat:   @"SELECT name,bldg.ID from bldg LEFT JOIN spatialStructure WHERE spatialStructure.ID=bldg.spatialStructureID AND bldg.ID=%d",productID] UTF8String];
            sqlite3_stmt *numberNameQueryStmt;
            if(sqlite3_prepare_v2 (database, numberNameQuerySQL, -1, &numberNameQueryStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(numberNameQueryStmt) == SQLITE_ROW) {
                    productName=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:0];
                    productNumber=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:1];
                    
                }
            }
            sqlite3_finalize(numberNameQueryStmt);
        }
        
        if ([productType isEqualToString:@"Site"]){
            const char* numberNameQuerySQL=[[NSString stringWithFormat:   @"SELECT name,site.ID from site LEFT JOIN spatialStructure WHERE spatialStructure.ID=site.spatialStructureID AND site.ID=%d",productID] UTF8String];
            sqlite3_stmt *numberNameQueryStmt;
            if(sqlite3_prepare_v2 (database, numberNameQuerySQL, -1, &numberNameQueryStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(numberNameQueryStmt) == SQLITE_ROW) {
                    productName=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:0];
                    productNumber=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:1];
                    
                }
            }
            sqlite3_finalize(numberNameQueryStmt);
        }
        
        if ([productType isEqualToString:@"Equipment"]){
            const char* numberNameQuerySQL=[[NSString stringWithFormat:   @"SELECT element.name,componentName from equipment LEFT JOIN element WHERE element.ID=equipment.elementID AND equipment.ID=%d",productID] UTF8String];
            sqlite3_stmt *numberNameQueryStmt;
            if(sqlite3_prepare_v2 (database, numberNameQuerySQL, -1, &numberNameQueryStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(numberNameQueryStmt) == SQLITE_ROW) {
                    productName=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:0];
                    productNumber=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:1];
                    
                }
            }
            sqlite3_finalize(numberNameQueryStmt);
        }
        if ([productType isEqualToString:@"SitePolygon"]){
            const char* numberNameQuerySQL=[[NSString stringWithFormat:   @"SELECT element.name,sitepolygon.ID from sitepolygon LEFT JOIN element WHERE element.ID=sitepolygon.elementID AND sitepolygon.ID=%d",productID] UTF8String];
            sqlite3_stmt *numberNameQueryStmt;
            if(sqlite3_prepare_v2 (database, numberNameQuerySQL, -1, &numberNameQueryStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(numberNameQueryStmt) == SQLITE_ROW) {
                    productName=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:0];
                    productNumber=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:1];                    
                }
            }
            sqlite3_finalize(numberNameQueryStmt);
        }
        
        if ([productType isEqualToString:@"Slab"]){
            const char* numberNameQuerySQL=[[NSString stringWithFormat:   @"SELECT element.name,slab.ID from slab LEFT JOIN element ON element.ID=slab.elementID WHERE slab.ID=%d",productID] UTF8String];
            sqlite3_stmt *numberNameQueryStmt;
            if(sqlite3_prepare_v2 (database, numberNameQuerySQL, -1, &numberNameQueryStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(numberNameQueryStmt) == SQLITE_ROW) {
                    productName=@"Slab";//[ProjectViewAppDelegate readNameFromSQLStmt:siteNumberNameQueryStmt column:0];
                    productNumber=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:1];
                }
            }
            sqlite3_finalize(numberNameQueryStmt);
        }
        
        if ([productType isEqualToString:@"Floor"]){
            
            uint bldgID=0;
            int firstFloor=0;
            NSString* bldgName=nil;
            const char* numberNameQuerySQL=[[NSString stringWithFormat:   @"SELECT spatialStructure.name,floor.ID,floor.bldgID,firstFloor from bldg LEFT JOIN floor on bldg.ID=floor.bldgID LEFT JOIN spatialStructure on spatialStructure.ID=bldg.spatialStructureID LEFT JOIN bldginfo on bldginfo.bldgID=bldg.ID WHERE floor.ID=%d",productID] UTF8String];
            sqlite3_stmt *numberNameQueryStmt;
            if(sqlite3_prepare_v2 (database, numberNameQuerySQL, -1, &numberNameQueryStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(numberNameQueryStmt) == SQLITE_ROW) {
                    bldgName=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:0];
//                    productName=[NSString stringWithFormat:@"%@'s Floor",bldgName];
                    productNumber=[ProjectViewAppDelegate readNameFromSQLStmt:numberNameQueryStmt column:2];
                    bldgID = sqlite3_column_int(numberNameQueryStmt, 2);
                    firstFloor=sqlite3_column_int(numberNameQueryStmt, 3);
                }
            }
            sqlite3_finalize(numberNameQueryStmt);
            
            
            
            const char* querySQL=[[NSString stringWithFormat:   @"SELECT spatialstructure.name, floor.ID from  floor LEFT JOIN spatialStructure on floor.spatialStructureID=spatialStructure.ID left join placement on placement.ID=spatialStructure.placementID  where  floor.bldgID=%d ORDER BY placement.Z",bldgID] UTF8String];
            
            
            
            sqlite3_stmt *queryStmt;
            int pFloor=1;
            NSString* floorName=nil;
            if(sqlite3_prepare_v2 (database, querySQL, -1, &queryStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(queryStmt) == SQLITE_ROW) {
                    
                    floorName=[ProjectViewAppDelegate readNameFromSQLStmt:queryStmt column:0];
                    uint floorID=sqlite3_column_int(queryStmt, 1);
                    if (floorID==productID){
                        break;
                    }
                    pFloor++;
                }
            }
            
            if (floorName!=nil && ![floorName isEqualToString:@""]){
                productNumber=nil;
                productName=[NSString stringWithFormat:@"%@",floorName];
            }else{
                int floorIndex=pFloor-firstFloor;
                if (floorIndex<1){ //Assume there is no 'ground floor' ie floorIndex cant be 0. Then if its below first floor, all basement is -1,-2,-3,-4 etc
                    floorIndex--;
                }
                productName=[NSString stringWithFormat:@"Floor %d of bldg %@",floorIndex,bldgName];
            }
            
            sqlite3_finalize(queryStmt);
            NSLog(@"DB ProductName: %@",productName);
        }

        
        
//        sqlite3_close(database);
    }else{
        sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
    }
    sqlite3_close(database);
    
    NSString* returnCombo=@"";
    if (productNumber!=nil && ![productNumber isEqualToString:@""]){
        returnCombo=[returnCombo stringByAppendingFormat:@"%@-",productNumber];
    }
    if (productName!=nil&&![productName isEqualToString:@""]){
        returnCombo=[returnCombo stringByAppendingFormat:@"%@",productName];
    }
    return returnCombo;
}
+(UIImage *)resizeImage:(UIImage *)image width:(CGFloat)resizedWidth height:(CGFloat)resizedHeight
{
    CGImageRef imageRef = [image CGImage];
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef bitmap = CGBitmapContextCreate(NULL, resizedWidth, resizedHeight, 8, 4 * ((uint)resizedWidth), colorSpace, kCGImageAlphaPremultipliedFirst);
    CGContextDrawImage(bitmap, CGRectMake(0, 0, resizedWidth, resizedHeight), imageRef);
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage *result = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);
    CGColorSpaceRelease(colorSpace);
    CGImageRelease(ref);
    
    return result;
}

- (void)dealloc
{
    [_aBrowseIDStr release];_aBrowseIDStr=nil;
    [_aEverNoteLinkedNote release];_aEverNoteLinkedNote=nil;
    [aGhostFloorSlabID release];aGhostFloorSlabID=nil;
    [aGhostSpaceID release];aGhostSpaceID=nil;
    
        	
    [_aVersionHistory release];_aVersionHistory=nil;
    [_currentUnitStruct release];_currentUnitStruct=nil;
    [loadingOverlay release],loadingOverlay=nil;
    [loadingOverlayLabel release], loadingOverlayLabel=nil;
    [loadingOverlayActIndi release], loadingOverlayActIndi=nil;
    [loadingOverlayProgressBar release], loadingOverlayProgressBar=nil;
    [loadingOverlayBackground release], loadingOverlayBackground=nil;
    
    [userDef release];
    [defUserName release];
    [defUserPW release];
    [_window release];
    [projectViewTabBarCtr release];
    [loginViewNavCntr release];
	[loginViewController release];   
    [viewModelNavCntr release];
    [activeProjectSite release];
    [activeLocalStudio release];
    [activeLiveStudio release];
    
    //    [activeSite release];activeSite=nil;
    [loadingView release];
    [bottomPopupOverlay release];
    [bottomPopupOverlayBackground release];
    
    //    [bottomPopupOverlayActIndi release];
//    [theConnection release];theConnection=nil;
//    [connectionData release];connectionData=nil;
    [super dealloc];
}

-(void) clearGhostElement{
    [aGhostFloorSlabID release];
    aGhostFloorSlabID=nil;
    [aGhostSpaceID release];
    aGhostSpaceID=nil;
    
}
-(void) resetGhostElementOnSpacePage{

//    NSMutableArray* newAGhostFloorSlabID=[[NSMutableArray alloc]init];
//    self.aGhostFloorSlabID=newAGhostFloorSlabID;
//    [newAGhostFloorSlabID release];

    NSMutableArray* newAGhostSpaceID=[[NSMutableArray alloc]init];
    self.aGhostSpaceID=newAGhostSpaceID;
    [newAGhostSpaceID release];
}

-(void) readUnitStructWithDBPath:(NSString*) dbPath{
    UnitStruct* tmpUnitStruct=[[UnitStruct alloc] initWithDBPath:dbPath];
    self.currentUnitStruct=tmpUnitStruct;
    [tmpUnitStruct release];
}

//@synthesize currentFloor;

//@synthesize bottomPopupOverlayActIndi;
//@synthesize activeSiteID;

+(bool) useThread{
    return true;
}
+(double) meterToFeet:(double)meter{
    return meter*3.2808399f;
}
+(double) sqMeterToSqFeet:(double)sqMeter{
    return sqMeter*10.7639105f;
}

+(double) feetToMeter:(double)feet{
    return feet/3.2808399f;
}
+(double) sqFeetToSqMeter:(double)sqFeet{
    return sqFeet/10.7639105f;
}



+(NSString*) toSeperatedNumberStr:(double)number numberOfFraction:(uint)numberOfFraction{
    NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init]; [frmtr setGroupingSize:3];
    [frmtr setGroupingSeparator:@","];
    [frmtr setUsesGroupingSeparator:YES];
    [frmtr setMaximumFractionDigits:numberOfFraction];
    NSString *commaString = [frmtr stringFromNumber:[NSNumber numberWithDouble:number]]; 
    [frmtr release];
    return commaString;
}

+(NSString*) readNameFromSQLStmt:(sqlite3_stmt*)sqlStmt column:(uint) column{
    NSString* returnStr;
    char* c=(char *) sqlite3_column_text(sqlStmt, column);
    if (c!=nil){
//    if ((char *) sqlite3_column_text(sqlStmt, column)!=nil){
//        char* t=(char *) sqlite3_column_text(sqlStmt, column);
        returnStr= [NSString stringWithUTF8String:c];
//        if (returnStr==nil){
//            returnStr=[[[NSString alloc] initWithBytes:c length:256 encoding:NSUTF8StringEncoding] autorelease];
//            
//        }
        
//        NSString* tmpStr=[[NSString alloc]initWithBytes:t length: encoding:NSUTF8StringEncoding];
//        returnStr=tmpStr;
//        [tmpStr release];
//        returnStr= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, column)];
    }else{
        returnStr=[NSString stringWithFormat:@""];
    }    
    return returnStr;
}
//+(NSString*) readDateFromSQLStmt:(sqlite3_stmt*)sqlStmt column:(uint) column{
//    NSString* returnStr;
//    if ((char *) sqlite3_column_text(sqlStmt, column)!=nil){
//        returnStr= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, column)];               
//    }else{
//        returnStr=[NSString stringWithFormat:@""];
//    }    
//    return returnStr;
//}

+ (NSString*) doubleToAreaStr:(double) d{        
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    NSString* measureUnitArea=nil;
    if ([appDele viewModelNavCntr]==nil){
        measureUnitArea=[appDele currentUnitStruct].measureUnitArea;
    }else{
        measureUnitArea=[[[appDele currentSite] model] measureUnitArea];
    }

    if ([appDele viewModelNavCntr]==nil){
        if ([appDele currentUnitStruct].isImperial){            
            d=[ProjectViewAppDelegate sqMeterToSqFeet:d];
        }
    }
    else{
        if ([[[appDele currentSite] model] isImperial]){
            d=[ProjectViewAppDelegate sqMeterToSqFeet:d];
        }
    }
    return [NSString stringWithFormat:@"%@ %@",[ProjectViewAppDelegate toSeperatedNumberStr:d numberOfFraction:0],measureUnitArea];
    
}
+ (NSString*) doubleToLengthStr:(double) d{    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    NSString* measureUnitLength=[[[appDele currentSite] model] measureUnitLength];
//    
    NSString* measureUnitLength=nil;
    if ([appDele viewModelNavCntr]==nil){
        measureUnitLength=[appDele currentUnitStruct].measureUnitLength;
    }else{
        measureUnitLength=[[[appDele currentSite] model] measureUnitLength];
    }


    
    if ([appDele viewModelNavCntr]==nil){
        if ([appDele currentUnitStruct].isImperial){            
            d=[ProjectViewAppDelegate meterToFeet:d];
        }
    }
    else{
        if ([[[appDele currentSite] model] isImperial]){
            d=[ProjectViewAppDelegate meterToFeet:d];
        }
    }
    
    
    
    return [NSString stringWithFormat:@"%@ %@",[ProjectViewAppDelegate toSeperatedNumberStr:d numberOfFraction:0],measureUnitLength];
    
}



+ (NSString*) doubleToCurrenyStr:(double) d{    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
    NSString* currencyUnit=nil;
    if ([appDele viewModelNavCntr]==nil){
        currencyUnit=[appDele currentUnitStruct].currencyUnit;
    }else{
        currencyUnit=[[[appDele currentSite] model] currencyUnit];
    }
    
        
    
    
    
    
    
    return [NSString stringWithFormat:@"%@%@",currencyUnit,[ProjectViewAppDelegate toSeperatedNumberStr:d numberOfFraction:0]];
    
}

+ (NSString*) intToIntStr:(int) i{    
    
//        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];        
    return [NSString stringWithFormat:@"%@",[ProjectViewAppDelegate toSeperatedNumberStr:i numberOfFraction:0]];
    
}


/*

-(void) trashAllAttachmentEntryFromSelectedProjectSite:(OPSProjectSite*) projectSite{//:(id)sender{
    
//    [self.navigationController popViewControllerAnimated:YES];    
//    [self removeInsertImgArray];
    
//    if (!attachmentInfo){
//        return;
//    }
    
    //    uint attachCommentID=attachmentInfo.attachCommentID;
    
    
    
    
    
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];    
    
    
    
       
//    OPSProjectSite* projectSite=appDele.activeProjectSite;  
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {          
        
        
        
        
        sqlite3_stmt *selectAttachFileStmt;
        const char* selectAttachFileSql=[[NSString stringWithFormat:@"select fileName from AttachFile"
                                             ] UTF8String];                      
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, selectAttachFileSql, -1, &selectAttachFileStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(selectAttachFileStmt) == SQLITE_ROW) {     
                NSString* fileName=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:selectAttachFileStmt column:0]];
                
                
                
                
                OPSStudio* studio=[self activeStudio];

                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *databaseFolderPath = [paths objectAtIndex:0]; 
                databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[self defUserName], studio.ID, studio.name, studio.iconName ]]; 	    
                

                NSString* fileToBeDeleted=[databaseFolderPath stringByAppendingPathComponent:fileName];
                
                [fileManager removeItemAtPath: fileToBeDeleted error:NULL];
                [fileName release];
                
            }
        }
        
        
        
        sqlite3_stmt *deleteAttachFileStmt=nil;
        const char* deleteAttachFileSql=[[NSString stringWithFormat:@"delete from AttachFile"
                                             ] UTF8String];                      
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, deleteAttachFileSql, -1, &deleteAttachFileStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(deleteAttachFileStmt) == SQLITE_ROW) {     
                
            }
        }
        
        
                        

  
        
//        if (attachmentInfo){
            
            sqlite3_stmt *deleteAttachCommentStmt;
            const char* deleteAttachCommentSql=[[NSString stringWithFormat:@"delete from AttachComment"
                                                 ] UTF8String];                      
            //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
            if(sqlite3_prepare_v2 (database, deleteAttachCommentSql, -1, &deleteAttachCommentStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(deleteAttachCommentStmt) == SQLITE_ROW) {     
                    
                }
            }
            
            
            
            
            
//        }        
        
        
        
    }            
    sqlite3_close(database);
    
    
    
    
    
}

*/
/*
-(NSData*) uploadSelectedProjectSite:(NSArray*) paramArray{
    
    OPSProjectSite* selectedProjectSite=[paramArray objectAtIndex:0];
    NSString* dbPath=[paramArray objectAtIndex:1];
//    ViewPlanSiteAndBldgInfoVC* viewPlanSiteAndBldgInfoVC=nil;
    if ([paramArray count]>2){
//        if ([[paramArray objectAtIndex:2] isKindOfClass:[ViewPlanSiteAndBldgInfoVC class]]){
//            viewPlanSiteAndBldgInfoVC=[paramArray objectAtIndex:2];        
//        }else{    
            self.attachmentInfoDetailToClose=[paramArray objectAtIndex:2];
//        }
    }else{
        self.attachmentInfoDetailToClose=nil;
    }
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];  
    
    NSData* sqliteData=[NSData dataWithContentsOfFile:[selectedProjectSite dbPath]];
    
    
    
    
    
    
    NSString *aBoundary = [NSString stringWithFormat:@"vYRNMz5SSe4vXrnc"];        
    NSMutableData *myData = [NSMutableData data];// [NSMutableData dataWithCapacity:1];
    NSString *myBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", aBoundary];
    NSString *closingBoundary = [NSString stringWithFormat:@"\r\n--%@--\r\n", aBoundary];    
    
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", appDele.defUserName] dataUsingEncoding:NSUTF8StringEncoding]];        
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", appDele.defUserPW] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"sysID\"\r\n\r\n%d", appDele.activeStudioID] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"siteID\"\r\n\r\n%d", selectedProjectSite.ID] dataUsingEncoding:NSUTF8StringEncoding]];    
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];    
	[myData appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"uploadFile\"; filename=\"OPS\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];    
	[myData appendData:[[NSString stringWithString:@"Content-Type: application/x-sqlite3\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:sqliteData];
    [myData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]]; //Ken
    //    
    //    
    //    
    
    
    
    
    
    
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *databaseFolderPath = [paths objectAtIndex:0]; 
    
    OPSStudio* studio=[appDele activeStudio];

    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], studio.ID, studio.name, studio.iconName ]]; 	    
    
    sqlite3_stmt *attachedFileStmt; 
    const char* attachedFileSql=[[NSString stringWithFormat:@"select attachfile.fileName from attachfile where sent=0"] UTF8String];            
    sqlite3 *database = nil;    
    uint pAttachedFile=0;    
    if (sqlite3_open ([dbPath UTF8String], &database) == SQLITE_OK) {      
        if(sqlite3_prepare_v2(database, attachedFileSql, -1, &attachedFileStmt, NULL) == SQLITE_OK) {
            while( sqlite3_step(attachedFileStmt) == SQLITE_ROW) {                                 
                
                NSString* attachedFileName= nil;
                if ((char *) sqlite3_column_text(attachedFileStmt, 0)!=nil){
                    attachedFileName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(attachedFileStmt, 0)];               
                }else{
                    attachedFileName=[NSString stringWithFormat:@""];
                }                  
                
                if (![attachedFileName hasSuffix:@".jpg"]){
                    continue;
                }
                
                
                //            NSString *FileParamConstant = @"image";
                
                // add image data and compress to send if needed.
                //            CGFloat compression         = 0.9f;
                //            CGFloat maxCompression = 0.1f;
                //            int maxFileSize             = 250*1024;
                
                NSString* imagePath=[databaseFolderPath stringByAppendingPathComponent:attachedFileName];
                
                NSData *imageData = [[NSData alloc] initWithContentsOfFile:imagePath];    
                //            while ([imageData length] > maxFileSize && compression > maxCompression)
                //            {
                //                compression -= 0.1;
                //                imageData = UIImageJPEGRepresentation([UIImage imageNamed:imagePath], compression);
                //            }
                
                //Assuming data is not nil we add this to the multipart form
                if (imageData) {
                    
                    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];        
                    //        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];                
                    
                    
                    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"AttachedFileName%04d\"; filename=\"%@\"\r\n",pAttachedFile,attachedFileName] dataUsingEncoding:NSUTF8StringEncoding]];    
                    [myData appendData:[[NSString stringWithString:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];                                
                    [myData appendData:imageData];
                    [myData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                }            
                pAttachedFile++;
                [imageData release];
                
                
            }
        }        
        sqlite3_close(database);
    }else{
        sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
    }  
    
    
    
    
    
    
    
    
    
    
    
    
    
    [myData appendData:[closingBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/postTestAction.php"];
    //    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=%d&siteID=%d&filename=OPS",[appDele activeLiveStudio].ID, [selectedProjectSite ID]];
    NSURL *url = [NSURL URLWithString: urlString];
    //    UIImage *image = [[UIImage imageWithData: [NSData dataWithContentsOfURL: url]] retain];
    
    
    
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:900.0];
    
    
    [theRequest setHTTPMethod:@"POST"];
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [myData length]];  
    [theRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];  
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", aBoundary];
    
    [theRequest setValue:contentType forHTTPHeaderField:@"Content-Type"];        
    
    
    [theRequest setHTTPBody:myData];
    
    //    NSString* t=[theRequest valueForHTTPHeaderField:@"Content-Type"];
    //    NSLog(@"%@",t);
    
    
    
    // create the connection with the request
    // and start loading the data
    // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file    
    //    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];    
    self.theConnection=aConnection;
    [aConnection release];
    if (theConnection) {
        // Create the NSMutableData that will hold
        // the received data
        // receivedData is declared as a method instance elsewhere
        if (connectionData){
            [connectionData release];
        }
        connectionData=[[NSMutableData data] retain];
    } else {    
        // Inform the user that the connection failed.        
		UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[connectFailMessage show];
		[connectFailMessage release];
        
    }   
    return  myData;
}

*/
/*
#pragma mark NSURLConnection methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
  [connectionData setLength:0];
    
    

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.            
    
    [connectionData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    // receivedData is declared as a method instance elsewhere
    
//    [connection release];
    [theConnection release];theConnection=nil;
    [connectionData release];connectionData=nil;
    

    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];    
    
    
    
    
    if (self.attachmentInfoDetailToClose){
        NSFileManager *fileManager = [NSFileManager defaultManager];

        
        
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
        NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];        
        [fileManager removeItemAtPath:targetPath error:NULL];  
        
        
        
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle: @"No Internet For Upload, Data Saved locally instead."
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"OK"
                              otherButtonTitles: nil];

        alert.tag = APPDELEGATE_CONFIRM_UPLOADNOINTERNET; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];
        
    }else{
        // inform the user
        UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"didFailWithError"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
        [didFailWithErrorMessage show];
        [didFailWithErrorMessage release];
        
        //inform the user
        NSLog(@"Connection failed! Error - %@ %@",
              [error localizedDescription],
              [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
        
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {   
        case APPDELEGATE_CONFIRM_UPLOADNOINTERNET:
        {
            [self.attachmentInfoDetailToClose actionOK:nil];
        }
            break;            
        default:
//            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");            
            break;
    }	
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{

    
    NSString* dataStr=[[NSString alloc] initWithData:connectionData encoding:NSASCIIStringEncoding] ;
    //    NSString* dataStr=[[NSString alloc] initWithData:connectionData encoding:NSUTF8StringEncoding] ;
    NSLog(@"RESPONSEDATA: %@",dataStr);
    
    [self alertUploadResult:nil title:dataStr];
    if ([dataStr isEqualToString:@"Upload Success"]){
                
        sqlite3_stmt *updateAttachCommentStmt; 
        const char* updateAttachCommentSQL=[[NSString stringWithFormat:@"update AttachComment set sent=1"] UTF8String];   
        
        sqlite3_stmt *updateAttachFileStmt; 
        const char* updateAttachFileSQL=[[NSString stringWithFormat:@"update AttachFile set sent=1"] UTF8String];    
        
        //        update AttachFile set sent=1
        sqlite3 *database = nil;    
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
        OPSProjectSite* selectedProjectSite=appDele.activeProjectSite;
        if (sqlite3_open ([[selectedProjectSite dbPath] UTF8String], &database) == SQLITE_OK) {      
            if(sqlite3_prepare_v2(database, updateAttachCommentSQL, -1, &updateAttachCommentStmt, NULL) == SQLITE_OK) {
                while( sqlite3_step(updateAttachCommentStmt) == SQLITE_ROW) {                     
                }
            }                    
            if(sqlite3_prepare_v2(database, updateAttachFileSQL, -1, &updateAttachFileStmt, NULL) == SQLITE_OK) {
                while( sqlite3_step(updateAttachFileStmt) == SQLITE_ROW) {                     
                }
            }                   
            sqlite3_close(database);
        }else{
            sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
        }  
        
        if (self.attachmentInfoDetailToClose){
            [self.attachmentInfoDetailToClose trashAttachmentEntry];
            
            
            
            NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
            NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                [fileManager removeItemAtPath:targetPath error:NULL];  
            }
            

            
            
        }else{
            [self trashAllAttachmentEntryFromSelectedProjectSite:[self activeProjectSite]];
        }
        
    }
    [dataStr release];
    
    
    
    [theConnection release],theConnection=nil;
    [connectionData release],connectionData=nil;
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];
    
    if (self.attachmentInfoDetailToClose){                
        [self.attachmentInfoDetailToClose.navigationController popViewControllerAnimated:YES];
        [self.attachmentInfoDetailToClose executeDeleteImgViewFromStorageAndDB];        
    }
    
}

-(void) alertUploadResult:(id)sender title:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle: title
                          message: @""
                          delegate: self
                          cancelButtonTitle: nil
                          otherButtonTitles: @"ok", nil];
    alert.tag = UIAlertViewTag_NoInternetConfirm; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
    [alert show];
    [alert release];
    
    
}
*/



- (void) setActiveLiveStudio:(OPSStudio *)_activeLiveStudio
{
    OPSStudio* newStudio = [_activeLiveStudio copy];  // make copy
    [activeLiveStudio release];  // release old value, if any
    activeLiveStudio = newStudio;   // assign new value
    activeStudioID=activeLiveStudio.ID;
    self.activeStudio=self.activeLiveStudio;

}

- (void) setActiveLocalStudio:(OPSStudio *)_activeLocalStudio
{
    OPSStudio* newStudio = [_activeLocalStudio copy];  // make copy
    [activeLocalStudio release];  // release old value, if any
    activeLocalStudio = newStudio;   // assign new value
    activeStudioID=activeLocalStudio.ID;
    self.activeStudio=self.activeLocalStudio;
}



- (uint) reCheckUserNameAndPW{
    uint errorCode=0;
    
    NSString *urlString = [NSString stringWithFormat: @"http://www.onuma.com/plan/webservices/baseXML.php?u=%@&p=%@&studios", [ProjectViewAppDelegate urlEncodeValue:[self defUserName]], [ProjectViewAppDelegate urlEncodeValue:[self defUserPW]]];
    //    NSString *urlString = [NSString stringWithFormat: @"http://www.onuma.com/plan/webservices/baseXML.php?sysID=%d&u=%@&p=%@&sites", 44, userName, userPW];
    
    NSURL *url = [NSURL URLWithString:urlString ];
    CXMLDocument *parser = [[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:nil] ;
    
    NSArray *aError = [parser nodesForXPath:@"//ERRORCODE" error:nil];
    for(CXMLElement* errorElem in aError){
        //        NSLog (@"%@",errorElem);
        NSString* errorCodeStr=[errorElem stringValue];
        errorCode=[errorCodeStr integerValue];
        
        //        [errorElem autorelease];
        break;
    }
    [parser release];
    
    if (aError==nil || errorCode!=0){
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Password invalid with current user name. "
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"re-Login"
                              otherButtonTitles: nil];
        
        alert.tag = AppDele_USER_PW_INVALID;
        [alert show];
        [alert release];
        
        
//        if ([[self.window rootViewController] isKindOfClass:[ProjectViewTabBarCtr class]]){
//            
//            UIAlertView *alert = [[UIAlertView alloc]
//                                  initWithTitle: @"Password invalid with current user name. "
//                                  message: @""
//                                  delegate: self
//                                  cancelButtonTitle: @"re-Login"
//                                  otherButtonTitles: nil];
//            
//            alert.tag = AppDele_USER_PW_INVALID; 
//            [alert show];
//            [alert release];
//        
//            
//            [self displayLoginViewFromProjectVC];
//            
//        }else if ([[self.window rootViewController] isKindOfClass:[ViewModelNavCntr class]]){
//            
//            UIAlertView *alert = [[UIAlertView alloc]
//                                  initWithTitle: @"Password invalid with current user name. "
//                                  message: @""
//                                  delegate: self
//                                  cancelButtonTitle: @"re-Login"
//                                  otherButtonTitles: nil];
//            
//            alert.tag = AppDele_USER_PW_INVALID;
//            [alert show];
//            [alert release];
//        
//            
//            
//            [self displayLoginViewFromViewModelVC];
//        }
    }
    //    [aError autorelease];
    if (aError==nil){errorCode=1;}
    return errorCode;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case AppDele_USER_PW_INVALID:
        {
            switch (buttonIndex) {
//                case 0: // cancel
//                {
//                    NSLog(@"Delete was cancelled by the user");
//                }
//                    break;
                case 0: // delete
                {
                    
                    if ([[self.window rootViewController] isKindOfClass:[ProjectViewTabBarCtr class]]){
                        
                        [self displayLoginViewFromProjectVC];
                    }else if ([[self.window rootViewController] isKindOfClass:[ViewModelNavCntr class]]){
                        
                        
                        [self displayLoginViewFromViewModelVC];
                    }                    
                }
                    break;
            }
            
            
        }
            break;
        default:
            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");
    }
}

//
//- (NSString *) URLEncodedString_ch {
//    NSMutableString * output = [NSMutableString string];
//    const unsigned char * source = (const unsigned char *)[self UTF8String];
//    int sourceLen = strlen((const char *)source);
//    for (int i = 0; i < sourceLen; ++i) {
//        const unsigned char thisChar = source[i];
//        if (thisChar == ' '){
//            [output appendString:@"+"];
//        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
//                   (thisChar >= 'a' && thisChar <= 'z') ||
//                   (thisChar >= 'A' && thisChar <= 'Z') ||
//                   (thisChar >= '0' && thisChar <= '9')) {
//            [output appendFormat:@"%c", thisChar];
//        } else {
//            [output appendFormat:@"%%%02X", thisChar];
//        }
//    }
//    return output;
//}


//+ (NSString *)urlEncodeValue:(NSString *)str
//{
//    NSString *result = (NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8);
//    return [result autorelease];
//}
+ (NSString *)urlEncodeValue:(NSString *)str
{
    NSString *result = (NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8);
    return [result autorelease];
}
// tmp pw:
//a:/?#[]@!$&’()*+,;=1
//a:/?#[]@!$$()*+,;=1


//+ (NSString *)urlEncodeValue:(NSString *)str
//{
//    NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8));
//    return result;
//}
//                      
- (uint) checkStudioWithUserName:(NSString*)userName userPW:(NSString*) userPW{
    uint errorCode=0;

    if ([self isLiveDataSourceAvailable]){
    
        NSString *urlString = [[NSString alloc ] initWithFormat: @"http://www.onuma.com/plan/webservices/baseXML.php?u=%@&p=%@&studios",  [ProjectViewAppDelegate urlEncodeValue:userName],[ProjectViewAppDelegate urlEncodeValue:userPW] ];
        NSURL *url = [[NSURL alloc ]initWithString: urlString ];
        [urlString release];urlString=nil;
        CXMLDocument *parser = [[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:nil] ;
        [url release];url=nil;
        if (parser==nil){
            return 1;
        }
        
        //    NSArray *aError = [NSArray array];    
        //    aError = [parser nodesForXPath:@"//ERRORCODE" error:nil];
        
        NSArray *aError = [parser nodesForXPath:@"//ERRORCODE" error:nil];
        for(CXMLElement* errorElem in aError){
            //        NSLog (@"%@",errorElem);
            NSString* errorCodeStr=[errorElem stringValue];
            errorCode=[errorCodeStr integerValue];
            
            //        [errorElem autorelease];
            break;
        }
        [parser release];
    }else{
        NSArray *keys = [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys];
        NSArray *values = [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allValues];
        for (int i = 0; i < keys.count; i++) {
            NSLog(@"%@: %@", [keys objectAtIndex:i], [values objectAtIndex:i]);
        }
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        if (standardUserDefaults) {
                    
            NSString* storedUserName=(NSString*) [standardUserDefaults valueForKey:[NSString stringWithFormat:@"UserName_%@",userName ]];
            NSString* storedUserPW=(NSString*) [standardUserDefaults valueForKey:[NSString stringWithFormat:@"UserPW_%@",userName]];
            if ([userName isEqualToString: storedUserName ] && [userPW isEqualToString: storedUserPW]){
                errorCode=0;
            }else{
                errorCode=1;
            }
            
        }else{
            errorCode=1;
        }
    }
    
    
    if (errorCode){
        
    }
    //    [aError autorelease];    
    return errorCode;        
}



-(void) incrementDisplayLevel{

    self.modelDisplayLevel++;
    NSLog(@"model displaylevel : %d",self.modelDisplayLevel);
}

 
- (void) ProceedViewModelLevel: (NSInteger) rootID{
//    self.model=[[OPSModel alloc] init];
//    [[self model] setupModelFromProject:project displayLevel:0];
//    [[[self projectViewTabBarController] view] removeFromSuperview];    
//    [project release];    
//    NSString* title= [NSString stringWithFormat:@"%@",[[self.model root] name] ];        
//    [self.model release];
//    [self setModelDisplayLevel:([self modelDisplayLevel]+1)];    
//    [self displayModelViewingTB];
    return;

}

-(void) displayLoginViewFromViewModelVC{    
    //Display Login Screen if no default user registered before
    LoginViewController * _loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:[NSBundle mainBundle]];
    self.loginViewController = _loginViewController;
    [_loginViewController release];
    
    LoginNavCntr* loginNavCntr=(LoginNavCntr*)[[LoginNavCntr alloc] initWithRootViewController:self.loginViewController];
    self.loginViewNavCntr=loginNavCntr;
    [loginNavCntr release];
//    
//    [self.window setRootViewController:self.loginViewController];
    
    [self.window setRootViewController:self.loginViewNavCntr];
    
    
    //    [self.window addSubview:(self.loginViewController.view)];
    
    
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [[self projectViewTabBarCtr] view].alpha=0l;
                     }
  
                     completion:^ (BOOL finished) {
                         if (finished) {
                             [[[self viewModelNavCntr] view] removeFromSuperview];
                             [viewModelNavCntr release];
                             viewModelNavCntr=nil;
                             //                             [self.window addSubview:(self.loginViewController.view)];
//                             [self.window setRootViewController:self.loginViewController];

                             [self.window setRootViewController:self.loginViewNavCntr];
                             // cleanup code
                         }}];
   
    
    

}
- (void) displayLoginViewFromProjectVC{
    
    
    
    //Display Login Screen if no default user registered before        
        
    [self resetUserDefaults];
    
    LoginViewController * _loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:[NSBundle mainBundle]];
    self.loginViewController = _loginViewController;
    [_loginViewController release];
    
    LoginNavCntr* loginNavCntr=[[LoginNavCntr alloc] initWithRootViewController:self.loginViewController];
    self.loginViewNavCntr=loginNavCntr;
    [loginNavCntr release];
    //
    //    [self.window setRootViewController:self.loginViewController];
    
    [self.window setRootViewController:self.loginViewNavCntr];
    
    
    
    [UIView animateWithDuration:0.5 
                          delay:0.0 
                        options:UIViewAnimationOptionBeginFromCurrentState 
                     animations:^{
                         [[self projectViewTabBarCtr] view].alpha=0l;     
                     }
     /*
      [UIView transitionFromView:[[self viewModelNavCntr] view]
      toView:[projectViewTabBarCtr view] 
      duration:0.5f 
      
      options:UIViewAnimationOptionTransitionFlipFromLeft
      */
                     completion:^ (BOOL finished) {
                         if (finished) {
                             [[[self projectViewTabBarCtr] view] removeFromSuperview];
                             [projectViewTabBarCtr release];
                             projectViewTabBarCtr=nil;                                                                                        
//                             [self.window addSubview:(self.loginViewController.view)]; 
//                             [self.window setRootViewController:self.loginViewController];
                             
                             [self.window setRootViewController:self.loginViewNavCntr];
                             
                             // cleanup code
                         }}];
    /*
     [UIView animateWithDuration:2.0 
     delay:0.0 
     options:UIViewAnimationOptionTransitionCurlUp 
     animations:^{
     //animation block                         
     
     
     
     }
     completion:^(BOOL finished){//this block starts only when 
     //                         ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
     
     [[[self viewModelNavCntr] view] removeFromSuperview];
     [viewModelNavCntr release];
     viewModelNavCntr=nil; 
     
     [self setupProjectView];                        
     //the animation in the upper block ends
     //so you know when exactly the animation ends
     }];
     
     
     */
    
    
}

-(void) loadLocalProjectTableListVC:(NSArray*) paramArray{
//    NSIndexPath* indexPath=[paramArray objectAtIndex:0];
//    int row=[indexPath row];
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];     
    UINavigationController* localStudioNVC=[self.projectViewTabBarCtr.viewControllers objectAtIndex:0];
    
//    LocalStudioListVC* localStudioListVC=[localStudioNVC.viewControllers objectAtIndex:0];  
    
    OPSStudio* selectedStudio=[self activeLocalStudio];
    
    
//    [appDele setActiveLocalStudio:selectedStudio];    
    
    LocalProjectListVC* localProjectTableListVC=[[LocalProjectListVC alloc] init];
    NSString* localProjectTitleStr=[[NSString alloc] initWithFormat: @"local projects in Studio: %@", selectedStudio.name ];
    localProjectTableListVC.title=localProjectTitleStr;    
    [localProjectTitleStr release];
    localProjectTitleStr=nil;
    
    
//    int height = 44;//self.frame.size.height;
//    int width = 800;
//    
//    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
//    navLabel.backgroundColor = [UIColor clearColor];
//    navLabel.textColor = [UIColor blackColor];
//    navLabel.font = [UIFont boldSystemFontOfSize:16];
//    navLabel.textAlignment = UITextAlignmentCenter;
//    navLabel.text=localProjectTableListVC.title;
//    localStudioNVC.navigationItem.titleView = navLabel;
//    [navLabel release];
    
    
    
    [localStudioNVC pushViewController:localProjectTableListVC animated:NO];      
    
    [localProjectTableListVC release];      
}

- (void) displayProjectViewFromLoginVC{
//    ProjectViewTabBarCtr* _projectViewTabBarCtr=[[ProjectViewTabBarCtr alloc] init];
//    self.projectViewTabBarCtr = _projectViewTabBarCtr;
//    [_projectViewTabBarCtr release];
//    projectViewTabBarCtr.delegate= projectViewTabBarCtr;

    
    //**************
//    [[ loginViewController view] removeFromSuperview];
//    [loginViewController release];
//    loginViewController=nil;
//    
//    
    
    [self.loginViewNavCntr removeFromParentViewController];
//    [self.loginViewNavCntr release];
//    self.loginViewNavCntr=nil;
//    
//    
//
//    self.defUserName=@"kenchoihm";
//    self.defUserPW=@"5021983";
    [self setupProjectView];  
    

    /*
    
    
    
    [UIView animateWithDuration:0.5 
                          delay:0.0 
                        options:UIViewAnimationOptionBeginFromCurrentState 
                     animations:^{
                         [[self loginViewController] view].alpha=0l;     
                     }

                     completion:^ (BOOL finished) {
                         if (finished) {
                             [[[self loginViewController] view] removeFromSuperview];
                             [loginViewController release];
                             loginViewController=nil; 
                             
                             
                             
                             [self setupProjectView];     
                             // cleanup code
                         }}];
   
    */
    
}


- (void) displayProjectViewFromViewModelVC{
    [UIView animateWithDuration:0.5 
                          delay:0.0 
                        options:UIViewAnimationOptionBeginFromCurrentState 
                     animations:^{
                         [[self viewModelNavCntr] view].alpha=0l;     
                     }
     
                    completion:^ (BOOL finished) {
                        if (finished) {
                            [[[self viewModelNavCntr] view] removeFromSuperview];
                            [viewModelNavCntr release];
                            viewModelNavCntr=nil;                                                                                     
                            
                            
                            
//                            [self setupProjectView];                                                                                       
//                            [self.window addSubview: [self.projectViewTabBarCtr view]];
                            [self.window setRootViewController:self.projectViewTabBarCtr];
                            

                            UIViewController* vc0=[[self.projectViewTabBarCtr viewControllers] objectAtIndex:0];
//                            UIViewController* vc1=[[self.projectViewTabBarCtr viewControllers] objectAtIndex:1];
                            
                            if ([vc0 isKindOfClass:[LocalProjectNavCntr class]]){
                                LocalProjectNavCntr* localProjectListNvc=(LocalProjectNavCntr*) vc0;
                                ;
                                NSArray* aVC=[localProjectListNvc viewControllers];
                                
                                LocalProjectListVC* localProjectListVC=(LocalProjectListVC*) [aVC objectAtIndex:0];                                
                                LocalProjectListVC* localProjectListVC1=(LocalProjectListVC*) [aVC objectAtIndex:1];
                                [[localProjectListVC theTableView] reloadData];
                                [[localProjectListVC1 theTableView] reloadData];
                                NSLog (@"Reload Table");
                            }
                            
                        }}];
 
}


- (void) displayProjectUploadWebView:(NSString*) urlAddress{        
    
    CGRect webFrame = self.viewModelNavCntr.view.frame;// CGRectMake(0.0, 0.0, 320.0, 460.0);
    AttachmentWebView *webView = [[AttachmentWebView alloc] initWithFrame:webFrame];
    
    [webView setBackgroundColor:[UIColor clearColor]];
    
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [webView loadRequest:requestObj];
    
    UIViewController* webViewController=[[UIViewController alloc] init];
    [webViewController setView:webView];
    [webView release];
    [[[[self projectViewTabBarCtr] viewControllers] objectAtIndex:0] pushViewController:webViewController animated:YES];
//    [[[self projectViewTabBarCtr] view] addSubview:[webViewController view]];
    
//    [self.viewModelNavCntr pushViewController:webViewController animated:NO]; 
    [webViewController release];
    
    

}






- (void) pushInfoWebViewToNavCntr:(NSString*) urlAddress navCntr:(UINavigationController*) navCntr{        

    CGRect webFrame = self.viewModelNavCntr.view.frame;// CGRectMake(0.0, 0.0, 320.0, 460.0);
    UIWebView *webView = [[UIWebView alloc] initWithFrame:webFrame];
    [webView setBackgroundColor:[UIColor clearColor]];


    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];

    UIViewController* webViewController=[[UIViewController alloc] init];
    [webViewController setView:webView];
    [webView release];

    [navCntr pushViewController:webViewController animated:YES]; 
    [webViewController release];

    
}


- (void) displayInfoWebView:(NSString*) urlAddress{        

    CGRect webFrame = self.viewModelNavCntr.view.frame;// CGRectMake(0.0, 0.0, 320.0, 460.0);
    UIWebView *webView = [[UIWebView alloc] initWithFrame:webFrame];
    [webView setBackgroundColor:[UIColor clearColor]];


    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];

    UIViewController* webViewController=[[UIViewController alloc] init];
    [webViewController setView:webView];
    [webView release];

    [self.viewModelNavCntr pushViewController:webViewController animated:YES]; 
    [webViewController release];


    
    
    
    
    
    
//    NSURL *URL = [NSURL URLWithString:urlAddress];
//	SVWebViewController *webViewController = [[SVWebViewController alloc] initWithURL:URL];
//	[self.viewModelNavCntr pushViewController:webViewController animated:YES];
//    [webViewController release];
    

}

- (void) displayAttachmentWebView:(NSString*) urlAddress{        
    
    
    
    uint width= self.viewModelNavCntr.view.bounds.size.width;
    uint height=self.viewModelNavCntr.view.bounds.size.height+20; //748
    
    CGRect webFrame = CGRectMake(0,0,width,height);//, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>) self.viewModelNavCntr.view.frame;// CGRectMake(0.0, 0.0, 320.0, 460.0);
    
    AttachmentWebViewController* attachmentWebViewController=[[AttachmentWebViewController alloc] initWithFrame:webFrame initURL:urlAddress hasNavControlBar:true];
    [self.viewModelNavCntr pushViewController:attachmentWebViewController animated:YES]; 
    [attachmentWebViewController release];
    
    
//    AttachmentWebView *webView = [[AttachmentWebView alloc] initWithFrame:webFrame];
//    [webView setBackgroundColor:[UIColor clearColor]];
//    
//
//    NSURL *url = [NSURL URLWithString:urlAddress];
//    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//    [webView loadRequest:requestObj];
//    
//    
//    
//    UIViewController* webViewController=[[UIViewController alloc] init];
//    [webViewController setView:webView];
//    [webView release];
//    
//    [self.viewModelNavCntr pushViewController:webViewController animated:YES]; 
//    [webViewController release];
//    
    
    
    
//    NSURL *URL = [NSURL URLWithString:urlAddress];
//	SVWebViewController *webViewController = [[SVWebViewController alloc] initWithURL:URL];
//	[self.viewModelNavCntr pushViewController:webViewController animated:YES];
//    [webViewController release];
    
    
    
    
    
    
    
    
    /*
    AttachmentInfoDetail* attachmentInfoDetail=[[AttachmentInfoDetail alloc] initWithNibName:@"AttachmentInfoDetail" bundle:nil];
    
    //    ViewModelVC* testVC=[[viewModelNavCntr viewControllers] objectAtIndex:([[viewModelNavCntr viewControllers] count]-2)];
    
    ViewModelVC* viewModelVC=[[viewModelNavCntr viewControllers] lastObject];    
    
    [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
        [viewModelVC.viewModelToolbar.popoverController dismissPopoverAnimated:NO];
        [self.viewModelNavCntr pushViewController:attachmentInfoDetail animated:NO];
        
        //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }                        
     ];
    
    [attachmentInfoDetail release];  
        
    */
}

/*

- (void) displayBIMMailVC{
    
    
    ViewModelVC* viewModelVC=[[viewModelNavCntr viewControllers] lastObject];
    //    OPSProduct* selectedProduct=[viewModelVC selectedProductForReference];// [[viewModelVC selectedProductRep] product];
    
    // Override point for customization after app launch.
    self.splitViewController =[[UISplitViewController alloc]init];
	self.rootViewController=[[UITableViewController alloc]init];
    
    
    BIMMailVC* bimMailVC=[[BIMMailVC alloc] initWithNibName:@"BIMMailVC" bundle:nil];    
//	self.detailViewController=[[FirstDetailViewController alloc]init];    
    self.detailViewController=bimMailVC;
    [bimMailVC release];    
    
	UINavigationController *rootNav=[[UINavigationController alloc]initWithRootViewController:rootViewController];
    UINavigationController *detailNav=[[UINavigationController alloc]initWithRootViewController:detailViewController];
    
	self.splitViewController.viewControllers=[NSArray arrayWithObjects:rootNav,detailNav,nil];
	self.splitViewController.delegate=self.detailViewController;

    
//    [self.viewModelNavCntr pushViewController:splitViewController animated:NO];
    
    [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
//        [viewModelVC.viewModelToolbar.popoverController dismissPopoverAnimated:NO];
        [self.viewModelNavCntr pushViewController:splitViewController animated:NO];
        
        //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }                        
     ];
    
    
    
    
    
    [bimMailVC release];  
    
    
}
*/


- (void) displayBIMMailVC{
    
    //    if (![self isLiveDataSourceAvailable]){
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet needed to send BIMMail" message:nil delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
    //
    //        [alert show];
    //
    //        [alert release];
    //
    //        return;
    //    }
    
    if (![self isLiveDataSourceAvailable]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sending BIM Mail requires an Internet connection." message:nil delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];

        [alert show];

        [alert release];

        return;
    }
    
    ViewModelVC* viewModelVC=[[viewModelNavCntr viewControllers] lastObject];
    //    OPSProduct* selectedProduct=[viewModelVC selectedProductForReference];// [[viewModelVC selectedProductRep] product];
    BIMMailVC* bimMailVC=[[BIMMailVC alloc] initWithNibName:@"BIMMailVC" bundle:nil viewModelVC:viewModelVC];
    
    
    
    
    
    [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
        [viewModelVC.viewModelToolbar.popoverController dismissPopoverAnimated:NO];
        [self.viewModelNavCntr pushViewController:bimMailVC animated:NO];
        
        //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }                        
     ];
    
    
    
    
    
    [bimMailVC release];  
    
    
}




- (uint) readAppCurrentVersion{    
    NSString*	versionStr = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];    
    return [self convertToVersionIntFromVersionStr:versionStr];
}

-(uint) convertToVersionIntFromVersionStr:(NSString*)versionStr {

    if (!versionStr || [versionStr isEqualToString:@""]){
        return 0;
    }
//    NSString*	versionStr = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];    
    //    NSLog(@"Version: %@",versionStr);
    //    NSString* versionStr=@"1.1.2";        
    NSRange textRangeDot0;
    textRangeDot0 =[versionStr rangeOfString:@"."];    
    NSUInteger dotLocation0=textRangeDot0.location;    
    NSString* remainStr=[versionStr substringFromIndex:(dotLocation0+1)];    
    NSString* versionStr0=[versionStr substringToIndex:dotLocation0];
    //    NSLog(@"%@",versionStr0);
    
    
    
    NSRange textRangeDot1;
    textRangeDot1 =[remainStr rangeOfString:@"."];    
    NSUInteger dotLocation1=textRangeDot1.location;    
    NSString* versionStr1=[remainStr substringToIndex:dotLocation1];
    //    NSLog(@"%@",versionStr1);
    
    
    NSString* versionStr2=[remainStr substringFromIndex:(dotLocation1+1)];
    //    NSLog(@"%@",versionStr2);
    
    uint versionInt=[versionStr0 intValue]*1000000+[versionStr1 intValue]*1000+[versionStr2 intValue];       
    //    NSLog(@"%d",versionInt);
    return versionInt;
}


- (void) displayProjectListSiteInfoVC: (UIViewController*) vc{
    
//    ViewModelVC* viewModelVC=[[viewModelNavCntr viewControllers] lastObject];    
    UINavigationController* navCntr=vc.navigationController;
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
    ViewPlanInfoVC* infoVC=nil;    
    

        infoVC=[[ViewPlanSiteAndBldgInfoVC alloc] initWithViewController:vc];
    
    
    //    if ([appDele modelDisplayLevel]==0){    
//        infoVC=[[ViewPlanSiteInfoVC alloc] initWithNibName:@"ViewPlanSiteInfoVC" bundle:nil viewController:vc];    
//        infoVC.title=[NSString stringWithFormat:@"(S%d_%d) %@",[appDele activeStudioID],[appDele activeProjectSite].ID,[appDele activeProjectSite].name];
    
    NSString* titleStr=[[NSString alloc] initWithFormat:@"%@",[appDele activeProjectSite].name];
    infoVC.title=titleStr;
    [titleStr release];
    titleStr=nil;
    
//    infoVC.title=[NSString stringWithFormat:@"%@",[appDele activeProjectSite].name];
    
    
    
    
//    }else if ([appDele modelDisplayLevel]==1){
//        if ([viewModelVC didSelectOneLevelCascadedProduct]){
//            infoVC=[[ViewPlanSpaceInfoVC alloc] initWithViewModelVC:viewModelVC];
//        }else{
//            infoVC=[[ViewPlanBldgInfoVC alloc] initWithViewModelVC:viewModelVC];        
//        }        
//    }else if ([appDele modelDisplayLevel]==2){// && [viewModelVC didSelectOneLevelCascadedProduct]){
//        if ([viewModelVC didSelectOneLevelCascadedProduct]){            
//            infoVC=[[ViewPlanFurnInfoVC alloc] initWithViewModelVC:viewModelVC];
//        }else{
//            infoVC=[[ViewPlanSpaceInfoVC alloc] initWithViewModelVC:viewModelVC];        
//        }        
//    }    
    
    uint width=800;
    uint height=44;
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor blackColor];
    navLabel.font = [UIFont boldSystemFontOfSize:16];
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text=infoVC.title;
    infoVC.navigationItem.titleView=navLabel;
    [navLabel release];
    

    [navCntr pushViewController:infoVC animated:YES];
    [infoVC release];
    
//    [UIView transitionWithView:navCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
////        [viewModelVC.viewModelToolbar.popoverController dismissPopoverAnimated:NO];
//        [navCntr pushViewController:infoVC animated:NO];        
//    } completion:^(BOOL finished) {            
//    }                        
//     ];
    
        
    
    
}





- (void) displayViewPlanSiteInfoVC{
    
//    if (![self isLiveDataSourceAvailable]){
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet needed to send BIMMail" message:nil delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
//
//        [alert show];
//
//        [alert release];
//
//        return;
//    }
    
    ViewModelVC* viewModelVC=[[viewModelNavCntr viewControllers] lastObject];
//    OPSProduct* selectedProduct=[viewModelVC selectedProductForReference];// [[viewModelVC selectedProductRep] product];        
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    ViewPlanInfoVC* infoVC=nil;    
    
    if ([appDele modelDisplayLevel]==0){
        infoVC=[[ViewPlanSiteAndBldgInfoVC alloc] initWithViewController:viewModelVC];
//        infoVC=[[ViewPlanSiteInfoVC alloc] initWithNibName:@"ViewPlanSiteInfoVC" bundle:nil viewController:viewModelVC];
        if ([viewModelVC lastSelectedProductRep]==nil ){
            
//            NSString* titleStr=[[NSString alloc] initWithFormat:@"(S%d_%d) %@",[appDele activeStudioID],[appDele activeProjectSite].ID,[appDele activeProjectSite].name];            
            NSString* titleStr=[[NSString alloc] initWithFormat:@"%@",[appDele activeProjectSite].name];
            infoVC.title=titleStr;
            [titleStr release];
            titleStr=nil;
            
//            infoVC.title=[NSString stringWithFormat:@"(S%d_%d) %@",[appDele activeStudioID],[appDele activeProjectSite].ID,[appDele activeProjectSite].name];
        }else{
            if ([viewModelVC aSelectedProductRep].count>1){
                
                NSString* titleStr=[[NSString alloc] initWithFormat:@"Multiple Buildings Selected"];
                infoVC.title=titleStr;
                [titleStr release];
                titleStr=nil;
//                infoVC.title=[NSString stringWithFormat:@"(S%d_%d) %@ : Multiple Buildings Selected",[appDele activeStudioID],[appDele activeProjectSite].ID,[appDele activeProjectSite].name];
            }else{
                if ([[[viewModelVC lastSelectedProductRep] product] isKindOfClass:[Site class]]){
                    
//                  NSString* titleStr=[[NSString alloc] initWithFormat:@"(S%d_%d) %@",[appDele activeStudioID],[appDele activeProjectSite].ID,[appDele activeProjectSite].name];
                    NSString* titleStr=[[NSString alloc] initWithFormat:@"%@",[appDele activeProjectSite].name];
                    infoVC.title=titleStr;
                    [titleStr release];
                    titleStr=nil;
                }else{
                    Floor* floor=(Floor*) [viewModelVC lastSelectedProductRep].product;            
                    Bldg* bldg=(Bldg*) [floor.linkedRel relating];
                    
                    
    //                NSString* titleStr=[[NSString alloc] initWithFormat:@"(S%d_%d) %@ : %d - %@",[appDele activeStudioID],[appDele activeProjectSite].ID,[appDele activeProjectSite].name, bldg.ID, bldg.name];
                    
                    NSString* titleStr=[[NSString alloc] initWithFormat:@"%@ : %d - %@",[appDele activeProjectSite].name, bldg.ID, bldg.name];
                    infoVC.title=titleStr;
                    [titleStr release];
                    titleStr=nil;
                }
                
                
                
            }
        }
        
    }else if ([appDele modelDisplayLevel]==1){// && [viewModelVC didSelectOneLevelCascadedProduct]){
//        infoVC=[[ViewPlanSiteInfoVC alloc] initWithNibName:@"ViewPlanBldgInfoVC" bundle:nil viewModelVC:viewModelVC];
        if ([viewModelVC didSelectOneLevelCascadedProduct]){
            
            
            
            if ([viewModelVC aSelectedProductRep].count>1){
                
                infoVC=[[ViewPlanSpaceInfoVC alloc] initWithViewModelVC:viewModelVC];
                NSString* titleStr=[[NSString alloc] initWithFormat:@"Multiple Spaces Selected"];
                infoVC.title=titleStr;
                [titleStr release];
                titleStr=nil;
                //                infoVC.title=[NSString stringWithFormat:@"(S%d_%d) %@ : Multiple Buildings Selected",[appDele activeStudioID],[appDele activeProjectSite].ID,[appDele activeProjectSite].name];
            }else{
                
                infoVC=[[ViewPlanSpaceInfoVC alloc] initWithViewModelVC:viewModelVC];
                Space* space=(Space*) [viewModelVC lastSelectedProductRep].product;
                NSString* titleStr=[[NSString alloc] initWithFormat:@"%@ - %@ (%@)",space.spaceNumber,space.name,[ ProjectViewAppDelegate doubleToAreaStr: space.spaceArea] ];
                infoVC.title=titleStr;
                [titleStr release];
                titleStr=nil;
            }
//            infoVC.title=[NSString stringWithFormat:@"%@ - %@ (%@)",space.spaceNumber,space.name,[ ProjectViewAppDelegate doubleToAreaStr: space.spaceArea] ];
        }else{
            infoVC=[[ViewPlanBldgInfoVC alloc] initWithViewModelVC:viewModelVC];
            
            Bldg* bldg=(Bldg*) [viewModelVC model].root;
            
            NSString* titleStr= [[NSString alloc] initWithFormat:@"%d - %@",bldg.ID,bldg.name ];
            infoVC.title=titleStr;
            [titleStr release];
            titleStr=nil;
//            infoVC.title=[NSString stringWithFormat:@"%d - %@",bldg.ID,bldg.name ];
        }

    }else if ([appDele modelDisplayLevel]==2){// && [viewModelVC didSelectOneLevelCascadedProduct]){
        if ([viewModelVC didSelectOneLevelCascadedProduct]){            
            infoVC=[[ViewPlanFurnInfoVC alloc] initWithViewModelVC:viewModelVC];

            
            
            if ([viewModelVC aSelectedProductRep].count>1){
                
                NSString* titleStr=[[NSString alloc] initWithFormat:@"Multiple Equipments Selected"];
                infoVC.title=titleStr;
                [titleStr release];
                titleStr=nil;
                //                infoVC.title=[NSString stringWithFormat:@"(S%d_%d) %@ : Multiple Buildings Selected",[appDele activeStudioID],[appDele activeProjectSite].ID,[appDele activeProjectSite].name];
            }else{
                Furn* furn=(Furn*) [viewModelVC lastSelectedProductRep].product;
                if (furn.componentName && ![furn.componentName isEqualToString:@""]){
                    NSString* titleStr=[[NSString alloc] initWithFormat:@"%@ - %@",furn.componentName,furn.name ];
                    infoVC.title=titleStr;
                    [titleStr release];
                    titleStr=nil;
    //                infoVC.title=[NSString stringWithFormat:@"%@ - %@",furn.componentName,furn.name ];
                }else{
                    NSString* titleStr=[[NSString alloc]initWithFormat:@"%@",furn.name ];
                    infoVC.title=titleStr;
                    [titleStr release];
                    titleStr=nil;
    //                infoVC.title=[NSString stringWithFormat:@"%@",furn.name ];
                }
            }
        }else{
            if ([viewModelVC aSelectedProductRep]==nil || [[viewModelVC aSelectedProductRep] count]==0){
                [viewModelVC selectTopLevelProductRep];
            }
            infoVC=[[ViewPlanSpaceInfoVC alloc] initWithViewModelVC:viewModelVC];
//            Space* space=(Space*) [viewModelVC model].root;
            
            Space* space=(Space*)[viewModelVC lastSelectedProductRep].product;
            NSString* titleStr=[[NSString alloc]initWithFormat:@"%@ - %@ (%@)",space.spaceNumber,space.name,[ ProjectViewAppDelegate doubleToAreaStr: space.spaceArea]];
            infoVC.title=titleStr;
            [titleStr release];
            titleStr=nil;
//            infoVC.title=[NSString stringWithFormat:@"%@ - %@ (%@)",space.spaceNumber,space.name,[ ProjectViewAppDelegate doubleToAreaStr: space.spaceArea] ];      
        }
        
//        infoVC=[[ViewPlanSiteInfoVC alloc] initWithNibName:@"ViewPlanSpaceInfoVC" bundle:nil viewModelVC:viewModelVC];
    }
//    
//    ViewPlanSiteInfoVC* siteInfoVC=[[ViewPlanSiteInfoVC alloc] initWithNibName:@"ViewPlanSiteInfoVC" bundle:nil viewModelVC:viewModelVC];
    
//    siteInfoVC.contentSizeForViewInPopover=CGSizeMake(633,383 );
//    ViewPlanSiteInfoNVC* siteInfoNVC=[[ViewPlanSiteInfoNVC alloc] initWithRootViewController:siteInfoVC viewModelVC:viewModelVC];
        
//     siteInfoVC.title=@"Site Info";   

    uint width=800;
    uint height=44;
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor blackColor];
    navLabel.font = [UIFont boldSystemFontOfSize:16];
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text=infoVC.title;
    infoVC.navigationItem.titleView=navLabel;
    [navLabel release];
//    self.navigationItem.titleView = navLabel;
    
    
    
    [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
        [viewModelVC.viewModelToolbar.popoverController dismissPopoverAnimated:NO];
        [self.viewModelNavCntr pushViewController:infoVC animated:NO];
        
        //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }                        
     ];
    
    
    
    
    
//    [siteInfoNVC release];  
    [infoVC release];
   
    
}


- (void) displayEverNoteNoteBrowser{
    
    
    ViewModelVC* viewModelVC=[[viewModelNavCntr viewControllers] lastObject];
    
    
    EverNoteNoteBrowserVC * nbvc=[[EverNoteNoteBrowserVC alloc] initWithNibName:@"EverNoteNoteBrowserVC" bundle:nil];    
    [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
        [viewModelVC.viewModelToolbar.popoverController dismissPopoverAnimated:NO];
        [self.viewModelNavCntr pushViewController:nbvc animated:NO];
        
        //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }
     ];
    
    
    
    
    
    //    [self.viewModelNavCntr pushViewController:attachmentInfoDetail animated:YES];
    [nbvc release];
    
    //            [viewModelNavCntr hideLoadingOverlay];
    
    
}

- (void) displayAttachmentInfoDetail:(AttachmentInfoData*) attachmentInfo{
    
    
    ViewModelVC* viewModelVC=[[viewModelNavCntr viewControllers] lastObject];
    OPSProduct* selectedProduct=[viewModelVC selectedProductForReference];// [[viewModelVC selectedProductRep] product];
    
    
//    if ([selectedProduct isKindOfClass:[Bldg class]]){
//        Bldg* bldg=(Bldg*)selectedProduct;
//        if (bldg.relAggregate==nil|| [bldg.relAggregate.related count]<=0 ){
//            return;
//        }
//        Floor* floor=(Floor*) [bldg.relAggregate.related objectAtIndex:0];
//        selectedProduct=floor;
//    }
//    
    
    AttachmentInfoDetail* attachmentInfoDetail=[[AttachmentInfoDetail alloc] initWithNibName:@"AttachmentInfoDetail" bundle:nil attachmentInfo:attachmentInfo selectedProduct:selectedProduct];// attachedProduct:selectedProduct attachmentInfo:attachmentInfo];
    
//    AttachmentInfoDetail* attachmentInfoDetail=[[AttachmentInfoDetail alloc] initWithNibName:@"AttachmentInfoDetail" bundle:nil attachedProduct:selectedProduct attachmentInfo:attachmentInfo];
    
    
//    ViewModelVC* testVC=[[viewModelNavCntr viewControllers] objectAtIndex:([[viewModelNavCntr viewControllers] count]-2)];
        
    
//    ViewModelToolbar* toolbar=[testVC viewModelToolbar] ;
//    
//
//    NSArray *passthroughViews = [[toolbar popoverController] passthroughViews];
//    
//    NSMutableArray *views = [passthroughViews mutableCopy];
//    [views addObject:[attachmentInfoDetail view]];
//    [[toolbar popoverController] setPassthroughViews:views];
//    [views release];
    
    
//    [[self view] becomeFirstResponder];
    // Custom initialization


//    [UIView setAnimationTransition:flipUp ? UIViewAnimationTransitionCurlUp : UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
    
    
    
    
    [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
        [viewModelVC.viewModelToolbar.popoverController dismissPopoverAnimated:NO];
        [self.viewModelNavCntr pushViewController:attachmentInfoDetail animated:NO];

//        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
        } completion:^(BOOL finished) {
        
        }                        
    ];
    
                         
                         

    
//    [self.viewModelNavCntr pushViewController:attachmentInfoDetail animated:YES];
    [attachmentInfoDetail release];  
    
            //            [viewModelNavCntr hideLoadingOverlay];

    
}
- (void) removeBottomPopupOverlay{    
    [bottomPopupOverlay removeFromSuperview];
}
/*
- (void) displayBottomPopupOverlay:(ViewModelToolbar*) toolbar  buttonID:(id) buttonID{// overlayFinishFunction:(SEL) overlayFinishFunction{
   
    UIView* _bottomPopupOverlay=[[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.bottomPopupOverlay = _bottomPopupOverlay;
    [_bottomPopupOverlay release];
    
    
    

    self.bottomPopupOverlay.backgroundColor=[UIColor clearColor];
    self.bottomPopupOverlay.alpha = 1.0;
    
    
    UIView* _bottomPopupOverlayBackground=[[UIView alloc]
                                       initWithFrame:loadingOverlay.frame];
    self.bottomPopupOverlayBackground=_bottomPopupOverlayBackground;
    [_bottomPopupOverlayBackground release];
    
    bottomPopupOverlayBackgroun©d.center=loadingOverlay.center;
    bottomPopupOverlayBackground.backgroundColor=[UIColor blackColor];
    bottomPopupOverlayBackground.alpha=0.0;
    [bottomPopupOverlay addSubview:bottomPopupOverlayBackground];
    bottomPopupOverlayBackground.autoresizesSubviews=true;
    bottomPopupOverlayBackground.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;    
    
    // create textview
    
    
//    UIActivityIndicatorView* _bottomPopupOverlayActIndi=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, 80.0f)];
//    self.bottomPopupOverlayActIndi=_bottomPopupOverlayActIndi;
//    [_bottomPopupOverlayActIndi release];
//    [bottomPopupOverlayActIndi setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
//    bottomPopupOverlayActIndi.center=loadingOverlay.center;
//    
//    bottomPopupOverlayActIndi.frame=CGRectOffset(loadingOverlayActIndi.frame, 0, -160.0f);    
//    [bottomPopupOverlay addSubview:bottomPopupOverlayActIndi];
//    bottomPopupOverlayActIndi.autoresizesSubviews=true;
//    bottomPopupOverlayActIndi.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;    
//    
//    
//    [bottomPopupOverlayActIndi startAnimating];
//    

    
    [self.window addSubview:bottomPopupOverlay];
    //     [self.window bringSubviewToFront:loadingOverlay];
    
    
    UIDeviceOrientation orientation=[[UIApplication sharedApplication] statusBarOrientation];

    
    
    if (orientation==UIInterfaceOrientationLandscapeRight){
        [bottomPopupOverlay setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
        bottomPopupOverlay.bounds=CGRectMake(0, 0, 1024, 768);
    }
    if (orientation==UIInterfaceOrientationLandscapeLeft){
        [bottomPopupOverlay setTransform:CGAffineTransformMakeRotation(M_PI_2)];
        bottomPopupOverlay.bounds=CGRectMake(0, 0, 1024, 768);        
    } 
    
    if (orientation==UIInterfaceOrientationPortraitUpsideDown){
        [bottomPopupOverlay setTransform:CGAffineTransformMakeRotation(M_PI*2)];
    }     
    

    
    [self.window addSubview:bottomPopupOverlay];
    bottomPopupOverlayBackground.alpha=0.0;
    [UIView animateWithDuration:0.001 
                          delay:0.0 
                        options:UIViewAnimationOptionBeginFromCurrentState 
                     animations:^{
                         //animation block                         
                         bottomPopupOverlayBackground.alpha = 0.6;
                         //                         loadingView.view.alpha=0.6;
                     }
                     completion:^(BOOL finished){//this block starts only when 
//                         [@selector( overlayFinishFunction )];
                         [toolbar displayAttachInfoTable:buttonID];
//                         [toolbar performSelector:@selector(overlayFinishFunction:)
//                                      withObject:aNeighbor]; 
                     }];
    
    return;    
    
  
    
}
*/
-(void) removeLoadingViewOverlay{
    if (loadingOverlay){
        [loadingOverlay removeFromSuperview];
    }
}





- (void) displayCamera:(NSString*) loadingOverlayLabelText invoker:(id)invoker completeAction:(SEL) completeAction actionParamArray:(NSArray*) paramArray{
    
    CGRect frame=[[UIScreen mainScreen] bounds];
    UIView* _loadingOverlay=[[UIView alloc] initWithFrame:frame];
    
    self.loadingOverlay = _loadingOverlay;
    [_loadingOverlay release];
    
    self.loadingOverlay.backgroundColor=[UIColor clearColor];
    self.loadingOverlay.alpha = 1.0;
    
    
    UIView* _loadingOverlayBackground=[[UIView alloc]
                                       initWithFrame:loadingOverlay.frame];
    self.loadingOverlayBackground=_loadingOverlayBackground;
    [_loadingOverlayBackground release];
    
    loadingOverlayBackground.center=loadingOverlay.center;
    loadingOverlayBackground.backgroundColor=[UIColor blackColor];
    loadingOverlayBackground.alpha=0.0;
    [loadingOverlay addSubview:loadingOverlayBackground];
    loadingOverlayBackground.autoresizesSubviews=true;
    loadingOverlayBackground.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    // create textview
    
    UITextView* _loadingOverlayLabel=[[UITextView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 800.0f, 200.0f)];
    self.loadingOverlayLabel=_loadingOverlayLabel;
    
    [_loadingOverlayLabel release];
    //    UILabel* _loadingOverlayLabel=[[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 800.0f, 60.0f)];
    //    self.loadingOverlayLabel=_loadingOverlayLabel;
    //    [_loadingOverlayLabel release];
    loadingOverlayLabel.editable=false;
    loadingOverlayLabel.center=loadingOverlay.center;
    loadingOverlayLabel.textAlignment=NSTextAlignmentCenter;
    loadingOverlayLabel.textColor = [UIColor whiteColor];
    //        downloadLabel.text=@"asdfffffffffffffffffffffffff";
    loadingOverlayLabel.backgroundColor = [UIColor clearColor];
    loadingOverlayLabel.font = [UIFont systemFontOfSize:18];
    
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    loadingOverlayLabel.text=loadingOverlayLabelText;//: %@",[site name]];
    
    
    loadingOverlayLabel.frame=CGRectOffset(loadingOverlayLabel.frame, 0, -140.0f);
    [loadingOverlay addSubview:loadingOverlayLabel];
    
    loadingOverlayLabel.autoresizesSubviews=true;
    loadingOverlayLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    //
    
    
    UIActivityIndicatorView* _loadingOverlayActIndi=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, 80.0f)];
    self.loadingOverlayActIndi=_loadingOverlayActIndi;
    [_loadingOverlayActIndi release];
    [loadingOverlayActIndi setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    loadingOverlayActIndi.center=loadingOverlay.center;
    
    loadingOverlayActIndi.frame=CGRectOffset(loadingOverlayActIndi.frame, 0, -160.0f);
    [loadingOverlay addSubview:loadingOverlayActIndi];
    loadingOverlayActIndi.autoresizesSubviews=true;
    loadingOverlayActIndi.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    
    [loadingOverlayActIndi startAnimating];
    
    //        loadingOverlayLabel.text=overlayLabelText;//[NSString stringWithFormat:@"Generating File from Server for Site: %@",projectSiteSelected.name]
    
    //     loadingOverlay.alpha = 1.0;
    
    //    [self.window addSubview:loadingOverlay];
    
    //    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
    UIDeviceOrientation orientation=[[UIApplication sharedApplication] statusBarOrientation];
    if (orientation==UIDeviceOrientationLandscapeLeft){
        [loadingOverlay setTransform:CGAffineTransformMakeRotation(M_PI_2)];
        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);
    }else if (orientation==UIDeviceOrientationLandscapeRight){
        [loadingOverlay setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);
    }else {
        [loadingOverlay setTransform:CGAffineTransformMakeRotation(M_PI_2)];
        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);
    }
            
    
    [self.window addSubview:loadingOverlay];
    loadingOverlayBackground.alpha=0.0;
    [UIView animateWithDuration:0.001
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         //animation block
                         loadingOverlayBackground.alpha = 0.6;
                         //                         loadingView.view.alpha=0.6;
                     }
                     completion:^(BOOL finished){//this block starts only when
                         [invoker performSelector:completeAction withObject:paramArray];
                     }];
    
    
    
    
    return;    
    
}









- (void) displayLoadingViewOverlay:(NSString*) loadingOverlayLabelText invoker:(id)invoker completeAction:(SEL) completeAction actionParamArray:(NSArray*) paramArray{        
    NSLog(@"DipslyaingLoadingViewOverlay");
    CGRect frame=[[UIScreen mainScreen] bounds];
    UIView* _loadingOverlay=[[UIView alloc] initWithFrame:frame];

    self.loadingOverlay = _loadingOverlay;
    [_loadingOverlay release];
    
    self.loadingOverlay.backgroundColor=[UIColor clearColor];
    self.loadingOverlay.alpha = 1.0;
    
    
    UIView* _loadingOverlayBackground=[[UIView alloc]
                                       initWithFrame:loadingOverlay.frame];
    self.loadingOverlayBackground=_loadingOverlayBackground;
    [_loadingOverlayBackground release];
    
    loadingOverlayBackground.center=loadingOverlay.center;
    loadingOverlayBackground.backgroundColor=[UIColor blackColor];
    loadingOverlayBackground.alpha=0.0;
    [loadingOverlay addSubview:loadingOverlayBackground];
    loadingOverlayBackground.autoresizesSubviews=true;
    loadingOverlayBackground.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;    
    
    // create textview
    
    UITextView* _loadingOverlayLabel=[[UITextView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 800.0f, 200.0f)];
    self.loadingOverlayLabel=_loadingOverlayLabel;
    
    [_loadingOverlayLabel release];
//    UILabel* _loadingOverlayLabel=[[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 800.0f, 60.0f)];
//    self.loadingOverlayLabel=_loadingOverlayLabel;
//    [_loadingOverlayLabel release];
    loadingOverlayLabel.editable=false;
    loadingOverlayLabel.center=loadingOverlay.center;
    loadingOverlayLabel.textAlignment=NSTextAlignmentCenter;
    loadingOverlayLabel.textColor = [UIColor whiteColor];
    //        downloadLabel.text=@"asdfffffffffffffffffffffffff";
    loadingOverlayLabel.backgroundColor = [UIColor clearColor];
    loadingOverlayLabel.font = [UIFont systemFontOfSize:18];
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];

    loadingOverlayLabel.text=loadingOverlayLabelText;//: %@",[site name]]; 
    
    
    loadingOverlayLabel.frame=CGRectOffset(loadingOverlayLabel.frame, 0, -140.0f);
    [loadingOverlay addSubview:loadingOverlayLabel];
    
    loadingOverlayLabel.autoresizesSubviews=true;
    loadingOverlayLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;    
    //     
    
    
    UIActivityIndicatorView* _loadingOverlayActIndi=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, 80.0f)];
    self.loadingOverlayActIndi=_loadingOverlayActIndi;
    [_loadingOverlayActIndi release];
    [loadingOverlayActIndi setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    loadingOverlayActIndi.center=loadingOverlay.center;
    
    loadingOverlayActIndi.frame=CGRectOffset(loadingOverlayActIndi.frame, 0, -160.0f);    
    [loadingOverlay addSubview:loadingOverlayActIndi];
    loadingOverlayActIndi.autoresizesSubviews=true;
    loadingOverlayActIndi.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;    
    
    
    [loadingOverlayActIndi startAnimating];
    
    //        loadingOverlayLabel.text=overlayLabelText;//[NSString stringWithFormat:@"Generating File from Server for Site: %@",projectSiteSelected.name]        
    
    //     loadingOverlay.alpha = 1.0;
    
//    [self.window addSubview:loadingOverlay];
    
//    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
    UIDeviceOrientation orientation=[[UIApplication sharedApplication] statusBarOrientation];
    if (orientation==UIDeviceOrientationLandscapeLeft){
        [loadingOverlay setTransform:CGAffineTransformMakeRotation(M_PI_2)];
        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);          
    }else if (orientation==UIDeviceOrientationLandscapeRight){
        [loadingOverlay setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);        
    }else {        
        [loadingOverlay setTransform:CGAffineTransformMakeRotation(M_PI_2)];
        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);     
    }

    
    
//    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeRight){

//    }
    /*
    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeRight){
        [loadingOverlay setTransform:CGAffineTransformMakeRotation(M_PI_2)];
        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);
    }
    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeLeft){
        [loadingOverlay setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);        
    } 
    
    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationPortraitUpsideDown){
        [loadingOverlay setTransform:CGAffineTransformMakeRotation(M_PI*2)];
    }     

    */
    
    [self.window addSubview:loadingOverlay];
    loadingOverlayBackground.alpha=0.0;    
    [UIView animateWithDuration:0.001 
                          delay:0.0 
                        options:UIViewAnimationOptionBeginFromCurrentState 
                     animations:^{
                         //animation block                         
                         loadingOverlayBackground.alpha = 0.6;
                         //                         loadingView.view.alpha=0.6;
                     }
                     completion:^(BOOL finished){//this block starts only when                           
                         [invoker performSelector:completeAction withObject:paramArray];
                     }];
    
    
    
    
    return;    
    
}




-(void) completeSwitchingViewModelVC: (NSArray*) paramArray{//(SpatialStructure*) rootSpatialStruct{
    NSLog(@"Complete SwitchViewModelVC");
    SpatialStructure* rootSpatialStruct=[paramArray objectAtIndex:0];
    
//    bool bPopPreviousPage=false;
//    if ([paramArray count]>1){
//        NSNumber* popPreviousPage=[paramArray objectAtIndex:1];  
//        if ([popPreviousPage integerValue]==1){
//            bPopPreviousPage=true;
//        }
//    }
    NSUInteger animationOption=UIViewAnimationOptionTransitionCurlUp;
    if (![NavFloorVC isPoopingFloor]){// [appDele modelDisplayLevel]>0){
//        NSArray* aVC=[self viewControllers];
//        
//        if ([aVC count]>1){
//            ViewModelVC* previousVC=[aVC objectAtIndex:([aVC count]-2)];
//            OPSNavUI* previousNavUI=[previousVC opsNavUI];
//            NavUIScroll* previousScroll= [previousNavUI navUIScroll];
//            DisplayModelUI* previousDisplayModelUI=[previousScroll displayModelUI];
//            [previousDisplayModelUI showAllLayer];
//            //                animationOption=UIViewAnimationOptionTransitionCrossDissolve;
//        }   
    }else{            
        animationOption=UIViewAnimationOptionTransitionCrossDissolve;
    }

    
    
    switch ([self modelDisplayLevel]){
        case 0:
        {
            //                                    Site* site=(Site*) rootSpatialStruct;
            //                                    self.activeSiteID=site.ID;
            

            
            
            
//            CGPoint pt=[mapView convertCoordinate:userLocation.coordinate toPointToView:self];  
            
//            double modelScaleFactor=[[oldViewModelVC model] modelScaleForScreenFactor];            
//            double mapBumpedUpScale=2.0;
//            mapFrame=CGRectMake(0, 0, 1024*mapBumpedUpScale,  (768-44-44-20)*mapBumpedUpScale );
            

            
            
            
            
            
            
            ViewModelVC* _viewModelVC=[[ViewModelVC alloc] initWithRootSpatialStruct: rootSpatialStruct];
            //            viewModelVC.title=[NSString stringWithFormat:@"Site: %@",[[[viewModelVC model]root] name] ];
            [viewModelNavCntr release];
            ViewModelNavCntr* _viewModelNavCntr=[[ViewModelNavCntr alloc] initWithRootViewController:_viewModelVC];             
            self.viewModelNavCntr = _viewModelNavCntr;
            [_viewModelNavCntr release];
            
            
            [_viewModelVC release];
            
//            [self browseAndSelect];
            
            break;
        }
        case 1:
        {
            
            
           ViewModelVC* oldViewModelVC=[[viewModelNavCntr viewControllers] lastObject];
            [[oldViewModelVC displayModelUI] removeMap];
//            [[oldViewModelVC displayModelUI].siteMapView removeFromSuperview];
//            [[oldViewModelVC displayModelUI].siteMapView release];

            
//            
//            ViewModelVC* oldViewModelVC=[[viewModelNavCntr viewControllers] lastObject];
//            CLLocationCoordinate2D center= [[[oldViewModelVC displayModelUI] siteMapView] convertPoint:[(rootSpatialStruct.bBox) center] toCoordinateFromView:[oldViewModelVC displayModelUI].spatialStructViewLayer];
//            self.mapLatitude=center.latitude;
//            self.mapLongitude=center.longitude;
//            
            
            
            ViewModelVC* viewModelVC=[[ViewModelVC alloc] initWithRootSpatialStruct: rootSpatialStruct];            
            
            [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(animationOption) animations:^{
                
                //                ViewModelVC* oldViewModelVC=[[viewModelNavCntr viewControllers] lastObject];    
                //                   [oldViewModelVC zoomFitSelectedProduct];                 
                [self.viewModelNavCntr pushViewController:viewModelVC animated:NO];
                
                //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
            } completion:^(BOOL finished) {
                NSArray* aButtons=viewModelVC.navigationItem.rightBarButtonItems;
                for (int pButton=0;pButton<[aButtons count];pButton++){
                    UIBarButtonItem *bi=[aButtons objectAtIndex:pButton];
                    UIView *buttonView=bi.customView;
                    buttonView.hidden=false;
                    
                    
//                    [self browseAndSelect];

                }
                
            }
             ];
            
            
            [viewModelVC release];  
            
            //            [viewModelNavCntr hideLoadingOverlay];
        }
            break;
        case 2:
        {
            
//            
//            ViewModelVC* oldViewModelVC=[[viewModelNavCntr viewControllers] lastObject];
//            [[oldViewModelVC displayModelUI].productViewLabelLayer setHidden:true];
//            
            
            ViewModelVC* viewModelVC=[[ViewModelVC alloc] initWithRootSpatialStruct: rootSpatialStruct];            
       
            [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(animationOption) animations:^{             
                
                [self.viewModelNavCntr pushViewController:viewModelVC animated:NO];
 
            } completion:^(BOOL finished) {
                
                
                    
                NSArray* aButtons=viewModelVC.navigationItem.rightBarButtonItems;
                for (int pButton=0;pButton<[aButtons count];pButton++){
                    UIBarButtonItem *bi=[aButtons objectAtIndex:pButton];
                    UIView *buttonView=bi.customView;
                    buttonView.hidden=false;
                }

//                [self browseAndSelect];

            }                        
             ];
            //            
            
            
            [viewModelVC release];  
            
            break;
        }
        default:
            break;
    }    
    
    
    
    
    if ([self modelDisplayLevel]==0) {           
        
        
        [[[self projectViewTabBarCtr] view] removeFromSuperview];
        
        
        
//        projectViewTabBarCtr.viewControllers=0;        
//        [projectViewTabBarCtr release];
//        projectViewTabBarCtr=0;
//        [self.window addSubview: [self.viewModelNavCntr view]];
        [self.window setRootViewController:self.viewModelNavCntr];
    }
    
    [self removeLoadingViewOverlay];
    
    [NavFloorVC setIsPoppingFloor:false];
}

/*
 
-(void) completeSwitchingViewModelVC: (NSArray*) paramArray{//(SpatialStructure*) rootSpatialStruct{
    SpatialStructure* rootSpatialStruct=[paramArray objectAtIndex:0];

//    bool bPopPreviousPage=false;
//    if ([paramArray count]>1){
//        NSNumber* popPreviousPage=[paramArray objectAtIndex:1];  
//        if ([popPreviousPage integerValue]==1){
//            bPopPreviousPage=true;
//        }
//    }
    switch ([self modelDisplayLevel]){
        case 0:
        {
            //                                    Site* site=(Site*) rootSpatialStruct;
            //                                    self.activeSiteID=site.ID;
            ViewModelVC* viewModelVC=[[ViewModelVC alloc] initWithRootSpatialStruct: rootSpatialStruct];
            //            viewModelVC.title=[NSString stringWithFormat:@"Site: %@",[[[viewModelVC model]root] name] ];
            [viewModelNavCntr release];
            ViewModelNavCntr* _viewModelNavCntr=[[ViewModelNavCntr alloc] initWithRootViewController:viewModelVC];             
            self.viewModelNavCntr = _viewModelNavCntr;
            [_viewModelNavCntr release];
            
            
            [viewModelVC release];  
            
            break;
        }
        case 1:
        {
            
            //            Bldg* bldg= (Bldg*) rootSpatialStruct;//[[viewModelVC model] root];                         
            //            RelAggregate* relAgg=[bldg relAggregate];    
            //            Floor* floor=[[relAgg related] objectAtIndex:bldg.selectedFloorIndex];   
            //            [viewModelNavCntr showLoadingOverlay:[NSString stringWithFormat:@"Loading Floor: %@",[floor name]]];
//            ViewModelVC* oldViewModelVC=[[viewModelNavCntr viewControllers] lastObject];    
//            [oldViewModelVC zoomFitSelectedProduct];  
            
            ViewModelVC* viewModelVC=[[ViewModelVC alloc] initWithRootSpatialStruct: rootSpatialStruct];            
            
            //            viewModelVC.title=[NSString stringWithFormat:@"Bldg: %@ | Floor: %@",[bldg name] , [floor name]];             
            
//            [self.viewModelNavCntr pushViewController:viewModelVC animated:YES];
            
            
            

//            [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
//                [self.viewModelNavCntr pushViewController:viewModelVC animated:NO];                
//                //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
//            } completion:^(BOOL finished) {
//                
//            }                        
//             ];
            

            
//            [UIView transitionFromView:self.viewModelNavCntr.view toView:[viewModelVC view] duration:0.5 options:UIViewAnimationTransitionCurlUp completion:^(BOOL finished) {                
//                [self.viewModelNavCntr pushViewController:viewModelVC animated:NO]; 
//            }                        
//             ];
//          
            
            [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
                
//                ViewModelVC* oldViewModelVC=[[viewModelNavCntr viewControllers] lastObject];    
//                [oldViewModelVC zoomFitSelectedProduct];                 
                [self.viewModelNavCntr pushViewController:viewModelVC animated:NO];
                
                //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
            } completion:^(BOOL finished) {
//                if (bPopPreviousPage){
//                    ViewModelVC* oldViewModelVC=[[self.viewModelNavCntr viewControllers] objectAtIndex:[[self.viewModelNavCntr viewControllers] count]-2];                     
//                }
            }                        
             ];
            
            
            [viewModelVC release];  
            
            //            [viewModelNavCntr hideLoadingOverlay];
        }
            break;
        case 2:
        {
//            ViewModelVC* oldViewModelVC=[[viewModelNavCntr viewControllers] lastObject];    
//            [oldViewModelVC zoomFitSelectedProduct];  
            ViewModelVC* viewModelVC=[[ViewModelVC alloc] initWithRootSpatialStruct: rootSpatialStruct];            
            //            Space* space = (Space*) [[viewModelVC model] root];               
            //            viewModelVC.title=[NSString stringWithFormat:@"Space: %@ ",[space name] ];    
//            [self.viewModelNavCntr pushViewController:viewModelVC animated:YES];
            
            
            

            
//            [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
//                [self.viewModelNavCntr pushViewController:viewModelVC animated:NO];                
//                //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
//            } completion:^(BOOL finished) {
//                
//            }                        
//             ];


            
//            [UIView transitionFromView:self.viewModelNavCntr.view.window toView:[viewModelVC view] duration:0.5 options:UIViewAnimationTransitionCurlUp completion:^(BOOL finished) {                
//                [self.viewModelNavCntr pushViewController:viewModelVC animated:NO]; 
//            }                        
//             ];
            
            [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{             
                
                [self.viewModelNavCntr pushViewController:viewModelVC animated:NO];
//                ViewModelVC* oldViewModelVC=[[viewModelNavCntr viewControllers] lastObject];    
//                [oldViewModelVC zoomFitSelectedProduct];               
                //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
            } completion:^(BOOL finished) {
                 
            }                        
             ];
//            
            
            
            [viewModelVC release];  
            
            break;
        }
        default:
            break;
    }    
    
    
    
    
    if ([self modelDisplayLevel]==0) {           
        [[[self projectViewTabBarCtr] view] removeFromSuperview];
        projectViewTabBarCtr.viewControllers=0;        
        [projectViewTabBarCtr release];
        projectViewTabBarCtr=0;
        [self.window addSubview: [self.viewModelNavCntr view]];
  
    }    
    
    [self removeLoadingViewOverlay];
    
}
*/

//-(void) removeCurrentStudio{
//
//    [[[[[self projectViewTabBarCtr] viewControllers] objectAtIndex:0] navigationController] popViewControllerAnimated:YES];
//    
//}
/*

- (void) displayViewModelNavCntr1: (SpatialStructure*) rootSpatialStruct bPopPreviousPage:(bool)bPopPreviousPage{// (OPSModel*) model{         
    NSString* loadingOverLayLabelText=nil;
    switch ([self modelDisplayLevel]){
        case 0:{
            //                Site* site=(Site*) rootSpatialStruct;            
            //            loadingOverlayLabel.text=[NSString stringWithFormat:@"Loading Site"];//: %@",[site name]]; 
            loadingOverLayLabelText=[NSString stringWithFormat:@"Loading Site"];//: %@",[site name]];             
            break;
        }
        case 1:{
            Bldg* bldg= (Bldg*) rootSpatialStruct;
            RelAggregate* relAgg=[bldg relAggregate];    
            Floor* floor=[[relAgg related] objectAtIndex:bldg.selectedFloorIndex];               
            loadingOverLayLabelText=[NSString stringWithFormat:@"Loading Floor: %@ | Floor: %@",[bldg name] , [floor name]];             
            break;
        }
        case 2:{            
            Space* space=(Space*) rootSpatialStruct;            
            //            loadingOverlayLabel.text=[NSString stringWithFormat:@"Loading Space: %@",[space name]]; 
            loadingOverLayLabelText=[NSString stringWithFormat:@"Loading Space: %@",[space name]];             
            break;
        }
            default:
            break;
    }    
    [self displayLoadingViewOverlay:loadingOverLayLabelText invoker:self completeAction:@selector(completeSwitchingViewModelVC:) actionParamArray: rootSpatialStruct?[NSArray arrayWithObject:rootSpatialStruct]:nil];            
    
}

*/

- (void) displayViewModelNavCntr: (SpatialStructure*) rootSpatialStruct{// (OPSModel*) model{ 
    
    NSLog(@"DisplayNviewModelNavCntr");
    NSString* loadingOverLayLabelText=nil;
    switch ([self modelDisplayLevel]){
        case 0:{
            //                Site* site=(Site*) rootSpatialStruct;
            
//            loadingOverlayLabel.text=[NSString stringWithFormat:@"Loading Site"];//: %@",[site name]];
            loadingOverLayLabelText=[[NSString alloc]initWithFormat:@"Loading Site"];//: %@",[site name]];
//            loadingOverLayLabelText=[NSString stringWithFormat:@"Loading Site"];//: %@",[site name]];
            break;
        }
        case 1:{
            Bldg* bldg= (Bldg*) rootSpatialStruct;
            RelAggregate* relAgg=[bldg relAggregate];    
            Floor* floor=[[relAgg related] objectAtIndex:bldg.selectedFloorIndex];               
//            loadingOverlayLabel.text=[NSString stringWithFormat:@"Loading Floor: %@ | Floor: %@",[bldg name] , [floor name]]; 
//            if ([rootSpatialStruct isKindOfClass:[Bldg class]]){
//                int i=0;
//                i=1;
//            }
            loadingOverLayLabelText=[[NSString alloc] initWithFormat:@"Loading Floor: %@ | Floor: %@",[bldg name] , [floor name]];

//            loadingOverLayLabelText=[NSString stringWithFormat:@"Loading Floor: %@ | Floor: %@",[bldg name] , [floor name]];
            break;
        }
        case 2:{
            
            Space* space=(Space*) rootSpatialStruct;            
//            loadingOverlayLabel.text=[NSString stringWithFormat:@"Loading Space: %@",[space name]];
            loadingOverLayLabelText=[[NSString alloc ] initWithFormat:@"Loading Space: %@",[space name]];
//            loadingOverLayLabelText=[NSString stringWithFormat:@"Loading Space: %@",[space name]];
            break;
        }
        default:{
               loadingOverLayLabelText=[[NSString alloc ] initWithFormat:@""];
            break;
        }

    }
    [self displayLoadingViewOverlay:loadingOverLayLabelText invoker:self completeAction:@selector(completeSwitchingViewModelVC:) actionParamArray: rootSpatialStruct?[NSArray arrayWithObject:rootSpatialStruct]:nil];
    
    [loadingOverLayLabelText release];
    loadingOverLayLabelText=nil;
}


  /*  
- (void) displayViewModelNavCntr0   : (SpatialStructure*) rootSpatialStruct{// (OPSModel*) model{ 


    

//     UIView* _loadingOverlay=[[UIView alloc] initWithFrame:self.window.frame];        
//    _loadingOverlay.autoresizesSubviews = YES;
//    _loadingOverlay.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;  
    CGRect frame=[[UIScreen mainScreen] bounds];
    UIView* _loadingOverlay=[[UIView alloc] initWithFrame:frame];
//    
//    CGRect screenFrame=CGRectMake(0, 0, 0, 0);
//    
//    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeRight||[[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeLeft){
//        screenFrame=CGRectMake(0, 0, 1024,768);
//    } else{
//        screenFrame=CGRectMake(0, 0, 768,1024);        
//    }     
//    
//    UIView* _loadingOverlay=[[UIView alloc] initWithFrame:screenFrame]; 
    
     self.loadingOverlay = _loadingOverlay;
     [_loadingOverlay release];
    
     self.loadingOverlay.backgroundColor=[UIColor clearColor];
     self.loadingOverlay.alpha = 1.0;
     
    
    UIView* _loadingOverlayBackground=[[UIView alloc]
                         initWithFrame:loadingOverlay.frame];
    self.loadingOverlayBackground=_loadingOverlayBackground;
    [_loadingOverlayBackground release];
    
    loadingOverlayBackground.center=loadingOverlay.center;
    loadingOverlayBackground.backgroundColor=[UIColor blackColor];
    loadingOverlayBackground.alpha=0.0;
    [loadingOverlay addSubview:loadingOverlayBackground];
    loadingOverlayBackground.autoresizesSubviews=true;
    loadingOverlayBackground.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;    
    
    // create textview
     
    UILabel* _loadingOverlayLabel=[[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 800.0f, 60.0f)];
    self.loadingOverlayLabel=_loadingOverlayLabel;
    [_loadingOverlayLabel release];
    
     loadingOverlayLabel.center=loadingOverlay.center;
     loadingOverlayLabel.textAlignment=UITextAlignmentCenter;
     loadingOverlayLabel.textColor = [UIColor whiteColor];
     //        downloadLabel.text=@"asdfffffffffffffffffffffffff";
     loadingOverlayLabel.backgroundColor = [UIColor clearColor];
     loadingOverlayLabel.font = [UIFont systemFontOfSize:18];
     
     ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
     
     switch ([appDele modelDisplayLevel]){
     case 0:{
     //                Site* site=(Site*) rootSpatialStruct;
     
     loadingOverlayLabel.text=[NSString stringWithFormat:@"Loading Site"];//: %@",[site name]]; 
     break;
     }
     case 1:{
     Bldg* bldg= (Bldg*) rootSpatialStruct;
     RelAggregate* relAgg=[bldg relAggregate];    
     Floor* floor=[[relAgg related] objectAtIndex:bldg.selectedFloorIndex];               
     loadingOverlayLabel.text=[NSString stringWithFormat:@"Loading Floor: %@ | Floor: %@",[bldg name] , [floor name]]; 
     break;
     }
     case 2:{
     
     Space* space=(Space*) rootSpatialStruct;            
     loadingOverlayLabel.text=[NSString stringWithFormat:@"Loading Space: %@",[space name]]; 
     break;
     }
     }
     
     
     loadingOverlayLabel.frame=CGRectOffset(loadingOverlayLabel.frame, 0, -240.0f);
     [loadingOverlay addSubview:loadingOverlayLabel];
    
    loadingOverlayLabel.autoresizesSubviews=true;
    loadingOverlayLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;    
//     
     
     
     UIActivityIndicatorView* _loadingOverlayActIndi=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, 80.0f)];
     self.loadingOverlayActIndi=_loadingOverlayActIndi;
    [_loadingOverlayActIndi release];
     [loadingOverlayActIndi setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
     loadingOverlayActIndi.center=loadingOverlay.center;
    
    loadingOverlayActIndi.frame=CGRectOffset(loadingOverlayActIndi.frame, 0, -160.0f);    
     [loadingOverlay addSubview:loadingOverlayActIndi];
    loadingOverlayActIndi.autoresizesSubviews=true;
    loadingOverlayActIndi.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;    
     ƒ
     
     [loadingOverlayActIndi startAnimating];
     
     //        loadingOverlayLabel.text=overlayLabelText;//[NSString stringWithFormat:@"Generating File from Server for Site: %@",projectSiteSelected.name]        
     
//     loadingOverlay.alpha = 1.0;
     
//     [self.window addSubview:loadingOverlay];
//     [self.window bringSubviewToFront:loadingOverlay];
//    UIDeviceOrientation* o=[[UIDevice currentDevice] orientation];

// 
//    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeRight){
//        [loadingOverlay setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
//        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);
//    }
//    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeLeft){
//        [loadingOverlay setTransform:CGAffineTransformMakeRotation(0)];
//        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);        
//    } 
    
    
    
    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeRight){
        [loadingOverlay setTransform:CGAffineTransformMakeRotation(M_PI_2)];
        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);
    }
    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeLeft){
        [loadingOverlay setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
        loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);        
    } 
    
//    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationPortraitUpsideDown){
//        [loadingOverlay setTransform:CGAffineTransformMakeRotation(M_PI*2)];
//    }     
    


//    LoadingView* _loadingView=[[LoadingView alloc] initWithNibName:@"LoadingView" bundle:nil];
//    self.loadingView=_loadingView;
//    [_loadingView release];
//    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeRight){
//        [loadingView.view setTransform:CGAffineTransformMakeRotation(M_PI_2)];
//    }
//    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeLeft){
//        [loadingView.view setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
//    }        

    
    [self.window addSubview:loadingOverlay];
    loadingOverlayBackground.alpha=0.0;
    [UIView animateWithDuration:0.001 
                          delay:0.0 
                        options:UIViewAnimationOptionBeginFromCurrentState 
                     animations:^{
                         //animation block                         
                         loadingOverlayBackground.alpha = 0.6;
//                         loadingView.view.alpha=0.6;
                     }
                     completion:^(BOOL finished){//this block starts only when 
    
    
    
    
                            switch ([self modelDisplayLevel]){
                                case 0:
                                {
//                                    Site* site=(Site*) rootSpatialStruct;
//                                    self.activeSiteID=site.ID;
                                    ViewModelVC* viewModelVC=[[ViewModelVC alloc] initWithRootSpatialStruct: rootSpatialStruct];
                        //            viewModelVC.title=[NSString stringWithFormat:@"Site: %@",[[[viewModelVC model]root] name] ];
                                    [viewModelNavCntr release];
                                    ViewModelNavCntr* _viewModelNavCntr=[[ViewModelNavCntr alloc] initWithRootViewController:viewModelVC];             
                                    self.viewModelNavCntr = _viewModelNavCntr;
                                    [_viewModelNavCntr release];
                                    
                                    
                                    [viewModelVC release];  

                                    break;
                                }
                                case 1:
                                {
                                    
                        //            Bldg* bldg= (Bldg*) rootSpatialStruct;//[[viewModelVC model] root];                         
                        //            RelAggregate* relAgg=[bldg relAggregate];    
                        //            Floor* floor=[[relAgg related] objectAtIndex:bldg.selectedFloorIndex];   
                        //            [viewModelNavCntr showLoadingOverlay:[NSString stringWithFormat:@"Loading Floor: %@",[floor name]]];

                                    ViewModelVC* viewModelVC=[[ViewModelVC alloc] initWithRootSpatialStruct: rootSpatialStruct];            
                                  
                        //            viewModelVC.title=[NSString stringWithFormat:@"Bldg: %@ | Floor: %@",[bldg name] , [floor name]];             
                                    
                                    [self.viewModelNavCntr pushViewController:viewModelVC animated:YES];
                                    [viewModelVC release];  
                                    
                        //            [viewModelNavCntr hideLoadingOverlay];
                                }
                                    break;
                                case 2:
                                {
                                    ViewModelVC* viewModelVC=[[ViewModelVC alloc] initWithRootSpatialStruct: rootSpatialStruct];            
                        //            Space* space = (Space*) [[viewModelVC model] root];               
                        //            viewModelVC.title=[NSString stringWithFormat:@"Space: %@ ",[space name] ];    
                                    [self.viewModelNavCntr pushViewController:viewModelVC animated:YES];
                                    [viewModelVC release];  
                               
                                    break;
                                }
                                default:
                                    break;
                            }    
                                
                              

                            
                            if ([self modelDisplayLevel]==0) {           
                                [[[self projectViewTabBarCtr] view] removeFromSuperview];
                                projectViewTabBarCtr.viewControllers=0;        
                                [projectViewTabBarCtr release];
                                projectViewTabBarCtr=0;
                        //        [self.window release];
                        //        [_window release];
                                
                        //        UIView* t=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 500, 400)];
                        //        t.backgroundColor=[UIColor redColor];
                        //        [self.window addSubview:t];
                        //            [self.window addSubview:[viewModelVC view]];
                                [self.window addSubview: [self.viewModelNavCntr view]];
                        //        [viewModelNavCntr release];
                        //        [self.window makeKeyAndVisible];
                        //        [[self.viewModelNavCntr view] setNeedsDisplay];                                                                                              

                        //        [self presentModalViewController: self.viewModelNavCntr    animated: NO];
                        //        [viewModelNavCntr release];        
                            }                             
                         [loadingOverlay removeFromSuperview];
//                         [loadingView.view removeFromSuperview];
                         
//                         [t release];
                     }];
    
    return;    
    
}

*/


- (BOOL) containLocalDatabase{
    //    NSString *bundleRoot = [[NSBundle mainBundle] bundlePath];   
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; 

//    NSString *bundleRoot = [[NSBundle mainBundle] resourcePath];    

    NSFileManager *manager = [NSFileManager defaultManager];
    
//    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:bundleRoot];
    
    
    NSString *userDir = [documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/%@",[self defUserName]]   ];
//    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:documentsDirectory];    
    NSDirectoryEnumerator *userDirEnum = [manager enumeratorAtPath:userDir];
    NSString* studioName;
    while ((studioName = [userDirEnum nextObject])){
        
        
        NSString *studioDir = [userDir stringByAppendingString:[NSString stringWithFormat:@"/%@",studioName]   ];
        NSDirectoryEnumerator *studioDirEnum = [manager enumeratorAtPath:studioDir];
        
        NSString *filename;
        while ((filename = [studioDirEnum nextObject] )) {
            if ([filename hasSuffix:@".sqlite"]) {   //change the suffix to what you are looking for
                return true;
                // Do work here
            }
        }            	
    }
    
    
//    NSString *filename;
//    while ((filename = [direnum nextObject] )) {                
//        if ([filename hasSuffix:@".sqlite"]) {   //change the suffix to what you are looking for
//            return true;
//            // Do work here            
//        }        
//    }    
//    
    
    return false;
}

- (BOOL)isLiveDataSourceAvailable
{
    
    Reachability* reachability = [Reachability reachabilityWithHostName:@"www.onuma.com"];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) { NSLog(@"onuma.com not reachable"); return FALSE;}
    else if (remoteHostStatus == ReachableViaWWAN) { NSLog(@"onuma.com reachable via wwan"); return TRUE;}
    else if (remoteHostStatus == ReachableViaWiFi) { NSLog(@"onuma.com reachable via wifi"); return TRUE;}
    return FALSE;
}


-(void)resetUserDefaults
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
	if (standardUserDefaults) {
		[standardUserDefaults setObject:nil forKey:@"UserName"];
		[standardUserDefaults setObject:nil forKey:@"UserPW"];
		[standardUserDefaults synchronize];
	}
    
    //    [defUserName release];ßßßßß
    [defUserName release];
    defUserName=nil;
    [defUserPW release];
    defUserPW=nil;
    
}


-(void)saveToUserDefaults:(NSString*) userName userPW:(NSString*) userPW
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];    
	if (standardUserDefaults) {
		[standardUserDefaults setObject:userName forKey:@"UserName"];
		[standardUserDefaults setObject:userPW forKey:@"UserPW"];
        
		[standardUserDefaults setObject:userName forKey:[NSString stringWithFormat:@"UserName_%@",userName ]];
		[standardUserDefaults setObject:userPW forKey:[NSString stringWithFormat:@"UserPW_%@",userName]];
        
        
        
		[standardUserDefaults synchronize];
	}  
    
//    [defUserName release];
    NSString* tmpUserName=[[NSString alloc]initWithString:userName];
    self.defUserName=tmpUserName;//[userName copy];
    [tmpUserName release];
    tmpUserName=nil;
//    [defUserPW release];
    NSString* tmpDefUserPW=[[NSString alloc]initWithString:userPW];
    self.defUserPW=tmpDefUserPW;//[userPW copy];
    [tmpDefUserPW release];
    tmpDefUserPW=nil;
    [self createUserDirectory];
    
}
-(void) createUserDirectory{
NSString *path;
NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"SomeDirectoryName"];
path = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.defUserName];    
NSError *error;
if (![[NSFileManager defaultManager] fileExistsAtPath:path])	//Does directory already exist?
{
    if (![[NSFileManager defaultManager] createDirectoryAtPath:path
                                   withIntermediateDirectories:NO
                                                    attributes:nil
                                                         error:&error])
    {
        NSLog(@"Create directory error: %@", error);
    }
}
}

-(void)retrieveFromUserDefaults
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	
	if (standardUserDefaults) {
        NSString* retrievedUserName=(NSString*) [standardUserDefaults objectForKey:@"UserName"];
        if (retrievedUserName){
            NSString* _userName=[[NSString alloc] initWithString: retrievedUserName];
            self.defUserName =_userName;
            [_userName release];
            _userName=nil;
            
            
            NSString* retrievedUserPW=(NSString*) [standardUserDefaults objectForKey:@"UserPW"];
            if (retrievedUserPW){
                NSString* _userPW=[[NSString alloc] initWithString: retrievedUserPW];
                self.defUserPW =  _userPW;
                [_userPW release];
                _userPW=nil;
            }
            
        }
        

	}
}



/*
 -(void)setArrayAndPushNextController
 {
 MyFirstViewController *myFirstViewController = [[MyFirstViewController alloc] init];
 MySecondViewController *mySecondViewController = [[MySecondViewController alloc] init];
 
 myFirstViewController.array = self.array;
 
 
 NSArray *array = [[NSArray alloc] initWithObjects:myFirstViewController, mySecondViewController, nil];
 UITabBarController *tab = [[UITabBarController alloc] init];
 tab.viewControllers = array;
 
 [array release];
 
 UITabBarItem *item1 = [[UITabBarItem alloc] initWithTitle:@"first title" image:nil tag:1];
 UITabBarItem *item2 = [[UITabBarItem alloc] initWithTitle:@"second title" image:nil tag:2];
 
 myFirstViewController.tabBarItem = item1;
 mySecondViewController.tabBarItem = item2;
 
 [self stopAnimatingSpinner];
 
 [self.navigationController pushViewController:tab animated:YES];
 
 [tab release];
 [item1 release];
 [item2 release];
 */
/*
if ([[WSFUserDefaults sharedInstance] savedTabBarLocation] > 0) {
    
    if ([[WSFUserDefaults sharedInstance] savedTabBarLocation] > 3) {
        UIViewController *selectViewController = [tabBarController.viewControllers objectAtIndex:[[WSFUserDefaults sharedInstance] savedTabBarLocation]];
        [tabBarController setSelectedViewController:tabBarController.moreNavigationController];
        [tabBarController.moreNavigationController popToRootViewControllerAnimated:NO];//make sure we're at the top level More
        [tabBarController.moreNavigationController pushViewController:selectViewController animated:NO];
    }
    else {
        [tabBarController setSelectedIndex:[[WSFUserDefaults sharedInstance] savedTabBarLocation]];
    }
}
*/
//
//- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController 
//{
//    
//
//    ProjectViewAppDelegate* appDel=[[UIApplication sharedApplication] delegate];
//
//    if (projectViewTabBarCtr.selectedIndex == 0) {
//
//        //Local Project Viewer
//        
//        /*
//        if ( ![appDel containLocalDatabase]){
//            //No Local Project
//            if ([appDel isLiveDataSourceAvailable]){
//                //Has InternetConnection
//                [[appDel tabBarController] setSelectedIndex:1];
//            }else{
//                [self.view removeFromSuperview];
//                
//                
//                ViewNeedLiveAccessForDL * _viewNeedLiveAccessForDL = [[ViewNeedLiveAccessForDL alloc] initWithNibName:@"ViewNeedLiveAccessForDL" bundle:Nil];
//                //            self.loginViewController = _loginViewController;
//                //            [_loginViewController release];        
//                //            [self.window addSubview:(self.loginViewController.view)];                         
//                
//                [appDel.window addSubview:_viewNeedLiveAccessForDL.view];
//                [_viewNeedLiveAccessForDL release];
//                
//                
//            }
//        }*/
//    }    
//    
//    
//    
//    
//    
//    [viewController.view setNeedsDisplay];        
//    
//    /*
//    if (tabBarController.selectedIndex == 0) {
//        MyViewController* my = (MyViewController *)viewController;
//        [my myMethod];
//    }
//     */
//}

//-(int) setupDataConnecter:(NSString*) userName userPW: (NSString*) userPW{   
//    int errCode=0;
//    dataConnecter=[[DataConnect alloc ] init:userName userPW:userPW]; 
//    if (self.dataConnecter!=nil){
//        errCode=0;
//    }else{        
//        errCode=1;
//        [self.dataConnecter release];
//    }        
//}

- (void) setupProjectView{
    self.isEnteringNoteDetailFromSyncTable=false;
    ProjectViewTabBarCtr* _projectViewTabBarCtr=[[ProjectViewTabBarCtr alloc] init];
    self.projectViewTabBarCtr = _projectViewTabBarCtr;
    [_projectViewTabBarCtr release];
    projectViewTabBarCtr.delegate= projectViewTabBarCtr;

    [self.window setRootViewController:self.projectViewTabBarCtr];
//    [self.window addSubview: [self.projectViewTabBarCtr view]];
}


-(void) registerVersionHistory{    
    OPSSiteVersion* v1001001=[[OPSSiteVersion alloc] initWithVersion:1001001 bStructureUpdated:false];
    OPSSiteVersion* v1001002=[[OPSSiteVersion alloc] initWithVersion:1001002 bStructureUpdated:false]; 
    OPSSiteVersion* v1001003=[[OPSSiteVersion alloc] initWithVersion:1001003 bStructureUpdated:false]; 
    OPSSiteVersion* v1001004=[[OPSSiteVersion alloc] initWithVersion:1001004 bStructureUpdated:false];
    
    OPSSiteVersion* v1001008=[[OPSSiteVersion alloc] initWithVersion:1001008 bStructureUpdated:true];
    OPSSiteVersion* v1001031=[[OPSSiteVersion alloc] initWithVersion:1001031 bStructureUpdated:false];
    OPSSiteVersion* v1001040=[[OPSSiteVersion alloc] initWithVersion:1001040 bStructureUpdated:false];
    
    
    OPSSiteVersion* v1002001=[[OPSSiteVersion alloc] initWithVersion:1002001 bStructureUpdated:false];
    OPSSiteVersion* v1002015=[[OPSSiteVersion alloc] initWithVersion:1002015 bStructureUpdated:false];
    OPSSiteVersion* v1002016=[[OPSSiteVersion alloc] initWithVersion:1002016 bStructureUpdated:false];
    OPSSiteVersion* v1002017=[[OPSSiteVersion alloc] initWithVersion:1002017 bStructureUpdated:false];
    
    
    OPSSiteVersion* v1002020=[[OPSSiteVersion alloc] initWithVersion:1002020 bStructureUpdated:false];
    OPSSiteVersion* v1002021=[[OPSSiteVersion alloc] initWithVersion:1002021 bStructureUpdated:false];
    OPSSiteVersion* v1002022=[[OPSSiteVersion alloc] initWithVersion:1002022 bStructureUpdated:false];
    OPSSiteVersion* v1002023=[[OPSSiteVersion alloc] initWithVersion:1002023 bStructureUpdated:false];
    OPSSiteVersion* v1002024=[[OPSSiteVersion alloc] initWithVersion:1001024 bStructureUpdated:false];
    
    OPSSiteVersion* v1003001=[[OPSSiteVersion alloc] initWithVersion:1003001 bStructureUpdated:false];;
    OPSSiteVersion* v1003002=[[OPSSiteVersion alloc] initWithVersion:1003002 bStructureUpdated:true];
    OPSSiteVersion* v1003003=[[OPSSiteVersion alloc] initWithVersion:1003003 bStructureUpdated:false];
    OPSSiteVersion* v1003004=[[OPSSiteVersion alloc] initWithVersion:1003004 bStructureUpdated:false];
    OPSSiteVersion* v1003005=[[OPSSiteVersion alloc] initWithVersion:1003005 bStructureUpdated:false];
    
    
    OPSSiteVersion* v1003006=[[OPSSiteVersion alloc] initWithVersion:1003006 bStructureUpdated:false];
    OPSSiteVersion* v1003007=[[OPSSiteVersion alloc] initWithVersion:1003007 bStructureUpdated:false];
    OPSSiteVersion* v1003008=[[OPSSiteVersion alloc] initWithVersion:1003008 bStructureUpdated:true];
    OPSSiteVersion* v1003009=[[OPSSiteVersion alloc] initWithVersion:1003009 bStructureUpdated:false];
    OPSSiteVersion* v1003010=[[OPSSiteVersion alloc] initWithVersion:10030010 bStructureUpdated:false];
    
    
    


    
    
 
    NSArray* aVersion=[[NSArray alloc]initWithObjects:v1001001,v1001002,v1001003,v1001004,v1001008,v1001031,v1001040,v1002001,v1002015,v1002016,v1002017,v1002020,v1002021, v1002022, v1002023, v1002024, v1003001,v1003002,  v1003003,v1003004,v1003005,v1003006,v1003007,v1003008,v1003009,v1003010,nil];

    [v1001001 release];
    [v1001002 release];
    [v1001003 release];
    [v1001004 release];
    [v1001008 release];
    [v1001031 release];
    [v1001040 release];
    
    [v1002001 release];  
    [v1002015 release];
    [v1002016 release];
    [v1002017 release];
    [v1002020 release];
    [v1002021 release];
    [v1002022 release];
    [v1002023 release];
    [v1002024 release];
    [v1003001 release];
    [v1003002 release];
    [v1003003 release];
    [v1003004 release];
    [v1003005 release];
    [v1003006 release];
    [v1003007 release];
    [v1003008 release];
    [v1003009 release];
    [v1003010 release];
    
    //    NSArray* aVersion=[[NSArray alloc]initWithObjects:[NSNumber numberWithUnsignedInt:1001001],[NSNumber numberWithUnsignedInt:1001002],[NSNumber numberWithUnsignedInt:1002001],[NSNumber numberWithUnsignedInt:1002002],nil];
    
    //    NSArray* aVersion=[NSArray arrayWithObjects:[NSNumber numberWithUnsignedInt:1001001],[NSNumber numberWithUnsignedInt:1001002],nil];
    self.aVersionHistory=aVersion;
    [aVersion release];    
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{     
//    studioID=141;//13;//141;//Beta 44 //MedicalPlanner 8 //Bimstorm Team 141
//    self.defUserPW=@"5021983";
//    self.defUserName=@"kenchoihm";
//    self.numFixedColorCatOnSiteLevel=5;
//    self.numFixedColorCatOnSFloorLevel=9;
    
    [self registerVersionHistory];
    
    [self retrieveFromUserDefaults];
    if (defUserName==Nil || defUserPW==Nil) {
        //Display Login Screen if no default user registered before
        LoginViewController * _loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:[NSBundle mainBundle]];
        self.loginViewController = _loginViewController;
        [_loginViewController release];
        
        LoginNavCntr* loginNavCntr=[[LoginNavCntr alloc] initWithRootViewController:self.loginViewController];
        self.loginViewNavCntr=loginNavCntr;
        [loginNavCntr release];
        loginNavCntr=nil;
//        [self.window addSubview:(self.loginViewController.view)];
//        [self.window setRootViewController:self.loginViewController];
        [self.window setRootViewController:self.loginViewNavCntr];
    }else{    
        
        [self setupProjectView];        
//        [self.window setRootViewController:self.projectViewTabBarCtr];
    }
    
    [self.window makeKeyAndVisible];
    application.statusBarOrientation = UIInterfaceOrientationLandscapeRight;
    
    
    NSString* EVERNOTE_HOST=BootstrapServerBaseURLStringSandbox;
    NSString* CONSUMER_KEY=@"onuma";
    NSString* CONSUMER_SECRET=@"d3cf2d90144511a7";
    [EvernoteSession setSharedSessionHost:EVERNOTE_HOST consumerKey:CONSUMER_KEY consumerSecret:CONSUMER_SECRET];


    return YES;
}
//-(void) addLoginInfoViewController{
//    
//    LoginViewController * _loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:[NSBundle mainBundle]];
//    self.loginViewController = _loginViewController;
//    [_loginViewController release];         
//    [self.window addSubview:(self.loginViewController.view)];         
//}



- (void) addLoginInfoViewController{            
    //Display Login Screen if no default user registered before        
    LoginInfoViewController * _loginInfoViewController = [[LoginInfoViewController alloc] initWithNibName:@"LoginInfoViewController" bundle:[NSBundle mainBundle]];
//    self.loginViewController = _loginViewController;
//    [_loginViewController release];         
//    [self.window addSubview:(self.loginViewController.view)];  
//    self.loginViewController = _loginViewController;
//    [_loginViewController release];         
//    [self.window addSubview:(self.loginViewController.view)];  
    [self.window addSubview:_loginInfoViewController.view];
    [_loginInfoViewController release];
    
    
    
    
//    
//    [UIView transitionWithView:self.viewModelNavCntr.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
//        [viewModelVC.viewModelToolbar.popoverController dismissPopoverAnimated:NO];
//        [self.viewModelNavCntr pushViewController:bimMailVC animated:NO];
//        
//        //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
//    } completion:^(BOOL finished) {
//        
//    }                        
//     ];
    

    
//    
//    [UIView animateWithDuration:0.5 
//                          delay:0.0 
////                        options:UIViewAnimationOptionBeginFromCurrentState 
//                        options:UIViewAnimationTransitionCurlUp      
//                     animations:^{
////                         [[self projectViewTabBarCtr] view].alpha=0l;     
//                     }
//
//                     completion:^ (BOOL finished) {
////                         if (finished) {
////                             [[[self projectViewTabBarCtr] view] removeFromSuperview];
////                             [projectViewTabBarCtr release];
////                             projectViewTabBarCtr=nil; 
////                             
////                             
////                             
////                             [self.window addSubview:(self.loginViewController.view)]; 
////                             
////                             // cleanup code
////                         }
//                     }];

}


-(void) applicationDidBecomeActive:(UIApplication *)application{
    [[EvernoteSession sharedSession] handleDidBecomeActive];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

//- (void)applicationDidBecomeActive:(UIApplication *)application
//{
//    /*
//     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//     */
//}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}


/*
 
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/
/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

@end

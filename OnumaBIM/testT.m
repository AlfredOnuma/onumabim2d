//
//  testT.m
//  ProjectView
//
//  Created by onuma on 31/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "testT.h"

@interface testT ()

@end

@implementation testT

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

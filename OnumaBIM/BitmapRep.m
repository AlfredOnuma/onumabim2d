//
//  BitmapRep.m
//  ProjectView
//
//  Created by Alfred Man on 12/6/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "BitmapRep.h"

#import "ProjectViewAppDelegate.h"
#import "ViewModelVC.h"
#import "OPSModel.h"
@implementation BitmapRep
@synthesize dimensionX;
@synthesize dimensionY;
@synthesize dimensionZ;
@synthesize mirrorY;
@synthesize insertPoint;
@synthesize fixedSize;
@synthesize insertOffsetX;
@synthesize insertOffsetY;


-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    BitmapRep *newBitmapRep = [[BitmapRep alloc] init:self.ID guid:self.GUID name:self.name insertPoint:insertPoint fixedSize:fixedSize mirrorY:mirrorY dimensionX:dimensionX dimensionY:dimensionY dimensionZ:dimensionZ];
 
    return newBitmapRep;
}

- (id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName insertPoint:(uint ) newInsertPoint fixedSize: (bool) newFixedSize mirrorY: (bool) newMirrorY dimensionX: (double) newDimensionX dimensionY: (double) newDimensionY dimensionZ: (double) newDimensionZ{
    
    
    
    self = [super init:(newID) guid:newGUID name:newName];
    if (self){        
        //        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
        //        ViewModelVC* viewModelVC=[appDele activeViewModelVC];
        //        double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
        self.insertPoint=newInsertPoint;
        self.fixedSize=newFixedSize;
        self.mirrorY=newMirrorY;
        self.dimensionX=newDimensionX;
        self.dimensionY=newDimensionY;
        self.dimensionZ=newDimensionZ;
        
        
        
        if (self.bBox!=nil){        
            [bBox release];
            bBox=nil;
        }
        CGRect bBoxRect=CGRectMake(0, 0, dimensionX, dimensionY);
        
        switch (self.insertPoint){
            case 1:
                
                bBoxRect=CGRectOffset(bBoxRect, 0, -self.dimensionY);
                break;
            case 2:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX/2, -self.dimensionY);
                break;
            case 3:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX,  -self.dimensionY);
                break;
            case 4:
                bBoxRect=CGRectOffset(bBoxRect, 0, -self.dimensionY/2);
                break;
            case 5:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX/2, -self.dimensionY/2);
                break;
            case 6:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX,  -self.dimensionY/2);
            case 7:
                bBoxRect=CGRectOffset(bBoxRect, 0,0);
                break;
            case 8:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX/2, 0);
                break;
            case 9:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX,  0);
                break;
        }
        
        BBox* _bBox=[[BBox alloc] initWithCGRect: bBoxRect];                 
        self.bBox=_bBox;
        [_bBox release];


        //        [self.bBox multiply:modelScaleFactor];
        
    }
    return self;
}

-(void) setBBoxinRootContextWithParentTransform: (CGAffineTransform) parentTransform{
    if (parentTransform.b!=0 || parentTransform.c!=0){                       
        //!------------------------------------------------------------------------------
        CGPoint bBoxPt0=CGPointMake(self.bBox.minX, self.bBox.minY);
        CGPoint bBoxPt1=CGPointMake(self.bBox.maxX, self.bBox.minY);        
        CGPoint bBoxPt2=CGPointMake(self.bBox.maxX, self.bBox.maxX);
        CGPoint bBoxPt3=CGPointMake(self.bBox.minX, self.bBox.maxX);
        CGPoint transformedToRootPt0=CGPointApplyAffineTransform(bBoxPt0, parentTransform);        
        [self updateBBoxInRootContext:transformedToRootPt0];
        CGPoint transformedToRootPt1=CGPointApplyAffineTransform(bBoxPt1, parentTransform);                
        [self updateBBoxInRootContext:transformedToRootPt1];
        CGPoint transformedToRootPt2=CGPointApplyAffineTransform(bBoxPt2, parentTransform);        
        [self updateBBoxInRootContext:transformedToRootPt2];
        CGPoint transformedToRootPt3=CGPointApplyAffineTransform(bBoxPt3, parentTransform);                
        [self updateBBoxInRootContext:transformedToRootPt3];         

        //!''''''''Close this If Cause To Save Transforming each Point if no Rotation is in parent transformation''''''''''''''''''''''''''''                    
    }else{
        [self setBBoxInRootContext: CGRectApplyAffineTransform([self.bBox cgRect], parentTransform)];    
    }
}
-(void) setBBoxinLocalFrameWithLocalTransform: (CGAffineTransform) localFrameTransform{
    if (localFrameTransform.b!=0 || localFrameTransform.c!=0){               
        CGPoint bBoxPt0=CGPointMake(self.bBox.minX, self.bBox.minY);
        CGPoint bBoxPt1=CGPointMake(self.bBox.maxX, self.bBox.minY);        
        CGPoint bBoxPt2=CGPointMake(self.bBox.maxX, self.bBox.maxX);
        CGPoint bBoxPt3=CGPointMake(self.bBox.minX, self.bBox.maxX);
        
        CGPoint transformedToLocalPt0=CGPointApplyAffineTransform(bBoxPt0, localFrameTransform);        
        [self updateBBoxinLocalFrame:transformedToLocalPt0];
        CGPoint transformedToLocalPt1=CGPointApplyAffineTransform(bBoxPt1, localFrameTransform);                
        [self updateBBoxinLocalFrame:transformedToLocalPt1];
        CGPoint transformedToLocalPt2=CGPointApplyAffineTransform(bBoxPt2, localFrameTransform);        
        [self updateBBoxinLocalFrame:transformedToLocalPt2];
        CGPoint transformedToLocalPt3=CGPointApplyAffineTransform(bBoxPt3, localFrameTransform);                
        [self updateBBoxinLocalFrame:transformedToLocalPt3];         
                
    }else{    
        [self setBBoxInLocalFrame: CGRectApplyAffineTransform([self.bBox cgRect], localFrameTransform)];    
    }
}
/*
- (id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName insertPoint:(uint ) newInsertPoint fixedSize: (bool) newFixedSize mirrorY: (bool) newMirrorY dimensionX: (double) newDimensionX dimensionY: (double) newDimensionY dimensionZ: (double) newDimensionZ parentTransform:(CGAffineTransform) parentTransform localTransform:(CGAffineTransform) localFrameTransform{
//- (id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName insertPoint:(uint ) newInsertPoint fixedSize: (bool) newFixedSize mirrorY: (bool) newMirrorY dimensionX: (double) newDimensionX dimensionY: (double) newDimensionY dimensionZ: (double) newDimensionZ  parentTransform:(CGAffineTransform) parentTransform localFrameTransform:(CGAffineTransform) localFrameTransform{
    
    
    
    self = [super init:(newID) guid:newGUID name:newName];
    if (self){        
//        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//        ViewModelVC* viewModelVC=[appDele activeViewModelVC];
//        double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
        self.insertPoint=newInsertPoint;
        self.fixedSize=newFixedSize;
        self.mirrorY=newMirrorY;
        self.dimensionX=newDimensionX;
        self.dimensionY=newDimensionY;
        self.dimensionZ=newDimensionZ;
        
        
        
        if (self.bBox!=nil){        
            [bBox release];
            bBox=nil;
        }
        CGRect bBoxRect=CGRectMake(0, 0, dimensionX, dimensionY);
        
        switch (self.insertPoint){
            case 1:

                bBoxRect=CGRectOffset(bBoxRect, 0, -self.dimensionY);
                break;
            case 2:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX/2, -self.dimensionY);
                break;
            case 3:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX,  -self.dimensionY);
                break;
            case 4:
                bBoxRect=CGRectOffset(bBoxRect, 0, -self.dimensionY/2);
                break;
            case 5:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX/2, -self.dimensionY/2);
                break;
            case 6:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX,  -self.dimensionY/2);
            case 7:
                bBoxRect=CGRectOffset(bBoxRect, 0,0);
                break;
            case 8:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX/2, 0);
                break;
            case 9:                                
                bBoxRect=CGRectOffset(bBoxRect, -self.dimensionX,  0);
                break;
        }
        
        BBox* _bBox=[[BBox alloc] initWithCGRect: bBoxRect];                 
        self.bBox=_bBox;
        [_bBox release];
        
        [self setBBoxInRootContext: CGRectApplyAffineTransform(bBoxRect, parentTransform)];         
//        [self setBBoxInRootContext: CGRectApplyAffineTransform(bBoxRect, parentTransform)];        
        [self setBBoxInLocalFrame: CGRectApplyAffineTransform(bBoxRect, localFrameTransform)]; 
//        [self.bBox multiply:modelScaleFactor];

    }
    return self;
}
*/
- (void)dealloc
{
    [super dealloc];
}
/*
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
*/
#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/
/*
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
 */

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end

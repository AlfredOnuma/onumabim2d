//
//  BIMReportCatTVC.h
//  ProjectView
//
//  Created by Alfred Man on 4/14/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIMPlanColorCatTVC.h"
@class NavScrollUIToolbar;
@protocol BIMReportCatTVCDelegate 
@end
@interface BIMReportCatTVC : UITableViewController{// <BIMPlanColorCatTVCDelegate>{    
    id <BIMReportCatTVCDelegate> delegate;
}

@property (nonatomic, assign) id <BIMReportCatTVCDelegate> delegate;

- (id) init: (NavScrollUIToolbar*) navScrollUIToolBar;

@end

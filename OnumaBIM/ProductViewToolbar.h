//
//  ProductViewToolbar.h
//  ProjectView
//
//  Created by Alfred Man on 11/24/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ViewModelToolbar.h"

//#import "AttachmentInfoNVC.h"
@class ViewModelVC;


//      Move/Rotate | Reshape | Lock | Copy | Delete | Undo/Redo
@interface ProductViewToolbar : ViewModelToolbar {//<AttachmentInfoNVCDelegate> {    
//    UIPopoverController *popoverController;
}



- (id)initWithFrame:(CGRect)frame viewModelVC:(ViewModelVC*)_viewModelVC;
-(void) browse:(id)sender;

- (void) issue:(id)sender;

-(NSString*) findSelectedProductRepDisplayStr;
//- (void) color:(id)sender;

/*
- (void) move:(id)sender;

- (void) rotate:(id)sender;
    
- (void) reshape:(id)sender;;
    
- (void) lock:(id)sender;

- (void) copy:(id)sender;
    
- (void) remove:(id)sender;
    
*/


- (void) edit:(id)sender;
-(void) refreshToolbar;


@end

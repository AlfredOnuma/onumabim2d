//
//  ViewModelNavCntr.h
//  ProjectView
//
//  Created by Alfred Man on 11/22/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewModelNavCntr : UINavigationController {
    bool _isEnteringGhostSpace;
//    UIViewController* loadingOverlayVC;
//    UIView* loadingOverlay;
//    UILabel* loadingOverlayLabel;    
//    UIActivityIndicatorView  *loadingOverlayActIndi;
//    UIProgressView* loadingOverlayProgressBar;
}
@property (nonatomic, assign) bool isEnteringGhostSpace;
//@property (nonatomic, assign) bool disableCascadedPop;

//@property (nonatomic, retain) UIActivityIndicatorView  *loadingOverlayActIndi;
//@property (nonatomic, retain) UIProgressView* loadingOverlayProgressBar;
//@property (nonatomic, retain) UIViewController* loadingOverlayVC;
//@property (nonatomic, retain) UIView* loadingOverlay;
//@property (nonatomic, retain) UIView* loadingOverlayLabel;
//
//-(void) showLoadingOverlay:(NSString*) overlayLabelText;
//-(void) hideLoadingOverlay;

//- (UIViewController *)popFloor:(BOOL)animated;
//-(UIViewController*) popViewControllerAnimated:(BOOL) animated enterSpaceID:(uint)enterSpaceID;
@end

//
//  ConstructionStatusColor.h
//  ProjectView
//
//  Created by Alfred Man on 7/25/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "StructForBIMPLanColor.h"

@interface ConstructionStatusColor : StructForBIMPlanColor

@end

//
//  ViewPlanSpaceInfoVC.m
//  ProjectView
//
//  Created by Alfred Man on 5/18/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewPlanSpaceInfoVC.h"
#import "ProjectViewAppDelegate.h"
#import "Department.h"
#import "CustomHeaderStruct.h"

#import "FusionTopColor.h"
#import "FusionPGMColor.h"
#import "FusionRoomUseColor.h"
@interface ViewPlanSpaceInfoVC ()

@end

@implementation ViewPlanSpaceInfoVC

static sqlite3 *database = nil;

@synthesize uiSpaceName;
@synthesize uiSpaceNumber;
@synthesize uiNetCalculated;
@synthesize uiHeight;
@synthesize uiCapacity;
@synthesize uiOccupancy;
@synthesize uiNumComponents;
@synthesize uiDepartment;
@synthesize aUICustomInt;
@synthesize aUICustom;
/*
 
 UILabel* uiSpaceName; 
 UILabel* uiSpaceNumber;
 UILabel* uiNetCalculated;
 UILabel* uiHeight;
 UILabel* uiCapacity;
 UILabel* uiOccupancy;
 UILabel* uiNumComponents;
 NSMutableArray* aUICustomInt;
 NSMutableArray* aUICustom;
 */

-(void) dealloc {
    [uiSpaceName release];uiSpaceName=nil;
    [uiSpaceNumber release]; uiSpaceNumber=nil;
    [uiNetCalculated release];uiNetCalculated=nil;
    [uiHeight release];uiHeight=nil;
    [uiCapacity release];uiCapacity=nil;
    [uiOccupancy release];uiOccupancy=nil;
    [uiNumComponents release];uiNumComponents=nil;
    [uiDepartment release];uiDepartment=nil;
    [aUICustomInt release];aUICustomInt=nil;
    [aUICustom release];aUICustom=nil;
    [super dealloc];
}



-(id) initWithViewModelVC:(ViewModelVC*) viewModelVC{
    self=[super initWithViewModelVC:viewModelVC];
    if (self){
        
        

        
        [self readFromSQL];
        
        
        
        
        //        @synthesize liveReportButton=_liveReportButton;
        
        //        @synthesize uiGrossCalculated;
        //        @synthesize uiGrossReported;
        //        @synthesize uiNumFloor;
        //        @synthesize uiCapacity;
        //        @synthesize uiOccupancy;
        //        @synthesize uiOccupancyNumber;
        //        @synthesize uiTotalEstimatedBldgCost;
        //        @synthesize constructionStatus;
        //        
        
    }
    return self;
}

-(void) readFromSQL{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
    OPSProjectSite* projectSite=[appDele activeProjectSite];    
    if ([projectSite dbPath]==Nil) {return;}    
    
//    Bldg* bldg=[appDele currentBldg];
    
    Space* space=nil;    
    if ([appDele modelDisplayLevel]==1){
        ViewProductRep* productRep=[[self.viewModelVC aSelectedProductRep] lastObject];
        if (![[productRep product] isKindOfClass:[Space class]]){
            return;
        }
        space=(Space*) [productRep product];
    }else if ([appDele modelDisplayLevel]==2){
        OPSProduct* lastSelectedProduct=[[[self viewModelVC] lastSelectedProductRep] product];
        if ([lastSelectedProduct isKindOfClass: [Space class]]){
            space=(Space*)lastSelectedProduct;
        }else{
            space=[appDele currentSpace];
        }
    }else{
        return;
    }
    
    
    
    
    uint topBotInset=50;
    
    //        uint leftRightInset=100;
    //        uint numRow=11;
    uint rowSpacing=10;
//    uint pRow=0;
    
    
    uint labelHeight=20;
    uint labelWidth=480;//210;
    uint uiLabelWidth=260; //130
    uint spaceLabelUILabelWidth=10;
    uint contentWidth=labelWidth+uiLabelWidth+spaceLabelUILabelWidth;
    //        uint contentHeight=labelHeight*numRow+rowSpacing*(numRow-1);
    
    
    
    CGRect rootFrame=self.viewModelVC.view.frame;
    rootFrame.origin=CGPointMake(0.0,0.0);
    UIView* tmpRootView=[[UIView alloc] initWithFrame:rootFrame];
    self.rootView=tmpRootView;
    [tmpRootView release];
    [self setView:self.rootView];
            
    
    UIView* tmpView=[[UIView alloc] initWithFrame:rootFrame];
    self.infoView=tmpView;
//    self.infoView.backgroundColor=[UIColor redColor];
//    self.infoView.alpha=0.5;
    [tmpView release];
    
    
    
//    UIView* testRed=[[UIView alloc] initWithFrame:self.rootView.frame];
//    CGRect testRect=testRed.frame;
//    testRed.backgroundColor=[UIColor redColor];
//    testRed.alpha=0.4;
//    [self.rootView addSubview:testRed];
//    [testRed release];
//    
    
    UIScrollView* scrollView=[[UIScrollView alloc] initWithFrame:self.rootView.frame];
    [scrollView setContentSize:self.infoView.frame.size];
    self.infoScrollView=scrollView;
    [scrollView release];
    
    [self.rootView addSubview:self.infoScrollView];
    
    [self.infoScrollView addSubview:self.infoView];
    
    
    //        CGSize buttonSize=CGSizeMake(200, 35);

    /*
    CGSize topButtonSize=CGSizeMake(35, 35);
    UIImage* buttonImage = [UIImage imageNamed:@"Report.png"];   
    UIImage* buttonImageHighlight = [UIImage imageNamed:@"ReportHighlight.png"];        

    UIButton* button=nil;//[UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
    
    */
    
    
    
    
    uint pRow=0;
    uint vPos=0;
    uint hPos=0;
    CGSize topButtonSize=CGSizeMake(200, 35);  
    UIButton* button=nil;
    button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTitle:@"Tab Here for Live Report" forState:UIControlStateNormal];
    
    hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width;
    
    
    vPos=topBotInset;        
    button.frame = CGRectMake(hPos, vPos, topButtonSize.width, topButtonSize.height) ;        
    [button addTarget:self action:@selector(displayWebView:) forControlEvents:UIControlEventTouchUpInside];
    [self.infoView addSubview:button];
    vPos+=topButtonSize.height;
    
    
    
    double totalNetArea=0.0;
//    double totalNumOccup;
//    uint totalNumEquipment;
        
    sqlite3_stmt* infoStmt;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  
        
        
        NSString* netAreaPart1=[NSString stringWithFormat:@""
//                                @"SELECT SUM(DISTINCT SpaceInfo.spaceArea)"];
                                @"SELECT SUM(SpaceInfo.spaceArea)"];                                
        
        
        NSString* netAreaFrom0=[NSString stringWithFormat:@""                      
                               @" FROM Space"
                               @" LEFT JOIN SpaceInfo ON Space.ID=SpaceInfo.spaceID"
                               @" WHERE"];
        NSString* netAreaFrom=[NSString stringWithFormat:@"%@",netAreaFrom0];
        if ([self viewModelVC]){
            for (uint pProductRep=0;pProductRep<[[[self viewModelVC] aSelectedProductRep] count];pProductRep++){
                ViewProductRep* productRep=[[[self viewModelVC] aSelectedProductRep] objectAtIndex:pProductRep];
                OPSProduct* product=[productRep product];
                if (pProductRep==0){
                    netAreaFrom=[NSString stringWithFormat:@"%@ Space.ID=%d",netAreaFrom,[product ID]];                
                }else{
                    netAreaFrom=[NSString stringWithFormat:@"%@ OR Space.ID=%d",netAreaFrom,[product ID]];                                    
                }
            }
            //            reportFrom=[NSString stringWithFormat:@"%@ group by Space.ID",reportFrom];
            
        }else{
            netAreaFrom=[NSString stringWithFormat:@"%@ Space.ID=%d",netAreaFrom,[space ID]];
        }
        
        
        NSString* netAreaSqlStr=[NSString stringWithFormat:@"%@ %@",netAreaPart1,netAreaFrom];
        const char* netAreaSqlChar=[netAreaSqlStr UTF8String];  
        if(sqlite3_prepare_v2(database, netAreaSqlChar, -1, &infoStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(infoStmt) == SQLITE_ROW) {                  
                totalNetArea=sqlite3_column_double(infoStmt,0);
            }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        uint numOccupants=0;
        uint numEquipments=0;
        
        
        NSString* referenceReportPart1=[NSString stringWithFormat:@""
                               @"SELECT SUM(Occupant.quantity) AS numOccupants,"
                               @" SUM(Component.quantity) AS numEquipments"];    
        
        NSString* referenceReportFrom0=[NSString stringWithFormat:@""
                               @" FROM Space"
                               @" LEFT JOIN SpatialStructure ON Space.spatialStructureID=SpatialStructure.ID"
                               @" LEFT JOIN USCG_SpaceInfo ON Space.ID=USCG_SpaceInfo.spaceID"
                               @" LEFT JOIN SpaceInfo ON Space.ID=SpaceInfo.spaceID"
                               //@" LEFT JOIN Space AS ReferenceSpace ON ReferenceSpace.ID=SpaceInfo.referenceID"
                               
                               @" LEFT JOIN Element ON Element.spatialStructureID=Space.spatialStructureID"
                               @" LEFT JOIN Equipment AS Component ON Element.ID=Component.elementID AND ((Component.furnName!='Man_Walking_sys') AND (Component.furnName!='Woman_Walking_sys'))"
                               @" LEFT JOIN Equipment AS Occupant ON Element.ID=Occupant.elementID AND ((Occupant.furnName='Man_Walking_sys') OR (Occupant.furnName='Woman_Walking_sys'))"
                               @" WHERE"];//, Space.ID=%d",[space ID]];//[[appDele currentSite] ID]];
        NSString* referenceReportFrom=[NSString stringWithFormat:@"%@",referenceReportFrom0];
        

        if ([self viewModelVC]){
            for (uint pProductRep=0;pProductRep<[[[self viewModelVC] aSelectedProductRep] count];pProductRep++){
                ViewProductRep* productRep=[[[self viewModelVC] aSelectedProductRep] objectAtIndex:pProductRep];
                Space* space=(Space*) [productRep product];
                
                
                uint spaceID=[space spaceReferenceID]==0?[space ID]:[space spaceReferenceID];
                
                
                
                
                if (pProductRep==0){
                    referenceReportFrom=[NSString stringWithFormat:@"%@ Space.ID=%d",referenceReportFrom,spaceID];
                }else{
                    referenceReportFrom=[NSString stringWithFormat:@"%@ OR Space.ID=%d",referenceReportFrom,spaceID];
                }
            }
            //            reportFrom=[NSString stringWithFormat:@"%@ group by Space.ID",reportFrom];
            
        }else{
            
            uint spaceID=[space spaceReferenceID]==0?[space ID]:[space spaceReferenceID];        
            referenceReportFrom=[NSString stringWithFormat:@"%@ Space.ID=%d",referenceReportFrom,spaceID];
        }
        
        
        NSString* referenceSqlStr=[NSString stringWithFormat:@"%@ %@",referenceReportPart1,referenceReportFrom];
        const char* referenceSQLChar=[referenceSqlStr UTF8String];
        if(sqlite3_prepare_v2(database, referenceSQLChar, -1, &infoStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(infoStmt) == SQLITE_ROW) {
                
                NSString* numOccupantsStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:0];
                if (numOccupantsStr!=nil && (![numOccupantsStr isEqualToString:@""])){
                    numOccupants=[numOccupantsStr intValue];
                }
                NSString* numEquipmentsStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:1];
                if (numEquipmentsStr!=nil && (![numEquipmentsStr isEqualToString:@""])){
                    numEquipments=[numEquipmentsStr intValue];
                }
                
            }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
//        NSString* occupancyNumEquipPart1=[NSString stringWithFormat:@""
//                                          @"SUM(Occupant.quantity) AS numOccupants,"                                        
//                                          @" SUM(Component.quantity) AS numEquipments"
//                                          @" FROM Space"
//                                          @" LEFT JOIN SpatialStructure ON Space.spatialStructureID=SpatialStructure.ID"
//                                          @" LEFT JOIN Element ON Element.spatialStructureID=Space.spatialStructureID"
//                                          @" LEFT JOIN Equipment AS Component ON Element.ID=Component.elementID AND ((Component.furnName!='Man_Walking_sys') AND (Component.furnName!='Woman_Walking_sys'))"
//                                          @" LEFT JOIN Equipment AS Occupant ON Element.ID=Occupant.elementID AND ((Occupant.furnName='Man_Walking_sys') OR (Occupant.furnName='Woman_Walking_sys'))"];
        
        
        
        
        
        
        NSString* reportPart1=[NSString stringWithFormat:@""
                               @"SELECT SpatialStructure.name,"
                               @" Space.spaceNumber,"
                               @" SUM(DISTINCT SpaceInfo.spaceArea),"
                               @" Space.spaceHeight,"
                               @" USCG_SpaceInfo.occupMax,"
                               @" USCG_SpaceInfo.density,"                               
                               @" USCG_SpaceInfo.occup,"
                               @" SUM(Occupant.quantity) AS numOccupants,"                                
                               @" SUM(Component.quantity) AS numEquipments,"
                               @" SpaceInfo.deptID"];
        
            
        for (uint pCustomInt=1;pCustomInt<=30;pCustomInt++){
            reportPart1=[NSString stringWithFormat:@"%@, SpaceInfo.customInt%d",reportPart1,pCustomInt];            
        }
        for (uint pCustomInt=1;pCustomInt<=15;pCustomInt++){
            reportPart1=[NSString stringWithFormat:@"%@, SpaceInfo.custom%d",reportPart1,pCustomInt];            
        }
        

        NSString* reportFrom0=[NSString stringWithFormat:@""                      
                               @" FROM Space"
                               @" LEFT JOIN SpatialStructure ON Space.spatialStructureID=SpatialStructure.ID"
                               @" LEFT JOIN USCG_SpaceInfo ON Space.ID=USCG_SpaceInfo.spaceID"
                               @" LEFT JOIN SpaceInfo ON Space.ID=SpaceInfo.spaceID"
                               //@" LEFT JOIN Space AS ReferenceSpace ON ReferenceSpace.ID=SpaceInfo.referenceID"
                               
                               @" LEFT JOIN Element ON Element.spatialStructureID=Space.spatialStructureID"
                               @" LEFT JOIN Equipment AS Component ON Element.ID=Component.elementID AND ((Component.furnName!='Man_Walking_sys') AND (Component.furnName!='Woman_Walking_sys'))"
                               @" LEFT JOIN Equipment AS Occupant ON Element.ID=Occupant.elementID AND ((Occupant.furnName='Man_Walking_sys') OR (Occupant.furnName='Woman_Walking_sys'))"
                               @" WHERE"];//, Space.ID=%d",[space ID]];//[[appDele currentSite] ID]];
        NSString* reportFrom=[NSString stringWithFormat:@"%@",reportFrom0];
        
        if ([self viewModelVC]){
            for (uint pProductRep=0;pProductRep<[[[self viewModelVC] aSelectedProductRep] count];pProductRep++){
                ViewProductRep* productRep=[[[self viewModelVC] aSelectedProductRep] objectAtIndex:pProductRep];
                OPSProduct* product=[productRep product];
                if (pProductRep==0){
                    reportFrom=[NSString stringWithFormat:@"%@ Space.ID=%d",reportFrom,[product ID]];                
                }else{
                    reportFrom=[NSString stringWithFormat:@"%@ OR Space.ID=%d",reportFrom,[product ID]];                                    
                }
            }
//            reportFrom=[NSString stringWithFormat:@"%@ group by Space.ID",reportFrom];
            
        }else{
            reportFrom=[NSString stringWithFormat:@"%@ Space.ID=%d",reportFrom,[space ID]];
        }
        
        
        
        
        NSString* sqlStr=[NSString stringWithFormat:@"%@ %@",reportPart1,reportFrom];
        const char* sqlChar=[sqlStr UTF8String];  
        if(sqlite3_prepare_v2(database, sqlChar, -1, &infoStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(infoStmt) == SQLITE_ROW) {  
                
                UILabel* labelSpaceName=nil;
                UILabel* uiLabel=nil;
                if ([[self.viewModelVC aSelectedProductRep] count]<=1){
                    vPos+=rowSpacing;
                    hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                    [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];   
                    
                    
                    labelSpaceName= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                    [self prepareLabel:labelSpaceName];        
                    labelSpaceName.text = [NSString stringWithFormat: @"Space Name:"];
                    [self.infoView addSubview:labelSpaceName];
                    [labelSpaceName release];
                
                
                
                    hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ]; 
                    [self prepareUILabel:uiLabel];
                    
                    uiLabel.text=[space name];        
                    self.uiSpaceName=uiLabel;        
                    [self.infoView addSubview:self.uiSpaceName];        
                    [uiLabel release];        
                    vPos+=labelHeight;
                    pRow++;                        
                }
                
                
                
                if ([[self.viewModelVC aSelectedProductRep] count]<=1){
                    vPos+=rowSpacing;
                    hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                    [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];                   
                    UILabel* labelSpaceNumber= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                    [self prepareLabel:labelSpaceNumber];        
                    labelSpaceNumber.text = [NSString stringWithFormat: @"Space Number:"];
                    [self.infoView addSubview:labelSpaceNumber];
                    [labelSpaceNumber release];
                    
                    
                    
                    hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ]; 
                    [self prepareUILabel:uiLabel];
                    uiLabel.text =[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:1];                  
                    self.uiSpaceNumber=uiLabel;        
                    [self.infoView addSubview:self.uiSpaceNumber];        
                    [uiLabel release];  
                    vPos+=labelHeight;              
                    pRow++;
                }
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];                   
                UILabel* labelNetCalculated= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelNetCalculated];
                
                if ([[self.viewModelVC aSelectedProductRep] count]<=1){
                    labelNetCalculated.text = [NSString stringWithFormat: @"Space Net Area:"];
                }else{
                    labelNetCalculated.text = [NSString stringWithFormat: @"Total Space Net Area:"];                    
                }
                [self.infoView addSubview:labelNetCalculated];
                [labelNetCalculated release];
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ]; 
                [self prepareUILabel:uiLabel];
                
                double netArea=totalNetArea;//sqlite3_column_double(infoStmt,2);
//                totalNetArea+=netArea;
                uiLabel.text=[ProjectViewAppDelegate doubleToAreaStr:netArea];
                
                self.uiNetCalculated=uiLabel;        
                [self.infoView addSubview:self.uiNetCalculated];        
                [uiLabel release];
                vPos+=labelHeight;                
                pRow++;
                
                
                
                
                if ([[self.viewModelVC aSelectedProductRep] count]<=1){
                    
                    vPos+=rowSpacing;
                    hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                    [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];          
                    UILabel* labelSpaceHeight = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                    [self prepareLabel:labelSpaceHeight];        
                    labelSpaceHeight.text = [NSString stringWithFormat: @"Space Height:"];
                    [self.infoView addSubview:labelSpaceHeight];
                    [labelSpaceHeight release];
                    
                    
                    
                    hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos,vPos, uiLabelWidth, labelHeight) ]; 
                    [self prepareUILabel:uiLabel];
                    
                    double spaceHeight=sqlite3_column_double(infoStmt,3);                
                    uiLabel.text=[ProjectViewAppDelegate doubleToLengthStr:spaceHeight];
                                   
                    self.uiHeight=uiLabel;        
                    [self.infoView addSubview:self.uiHeight];
                    [uiLabel release];
                    vPos+=labelHeight;                
                    pRow++;
                    
                }
                
                
                bool bShowCapacity=true;
                
                double occupMax=sqlite3_column_double(infoStmt,4);                
                double density=sqlite3_column_double(infoStmt,5);                  
                double capacity=0.0;
                if (occupMax>0) {
                    capacity = occupMax;
                } else {
                    if (density>0){
                        capacity = netArea/density;
                        if ([[appDele currentSite] isImperial]){
                            capacity = [ProjectViewAppDelegate sqMeterToSqFeet:capacity];
                        } 
                    }else{
                        bShowCapacity=false;
                    }
                }
                
                
                if ([[self.viewModelVC aSelectedProductRep] count]<=1){
                    if (bShowCapacity){                    
                        
                        vPos+=rowSpacing;
                        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];    
                        
                        UILabel* labeCapacity = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labeCapacity];        
                        labeCapacity.text = [NSString stringWithFormat: @"Space Capacity (design value):"];
                        [self.infoView addSubview:labeCapacity];
                        [labeCapacity release];
                        
                        
                        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                        [self prepareUILabel:uiLabel];

                        uiLabel.text = [NSString stringWithFormat: @"%.1f",capacity];                 
                        self.uiCapacity=uiLabel;        
                        [self.infoView addSubview:self.uiCapacity];        
                        [uiLabel release];  
                        vPos+=labelHeight;      
                        pRow++;
                    }                    
                }
                 
                 
//                 @" SUM(Occupant.quantity) AS numOccupants,"                                
//                 @" SUM(Component.quantity) AS numEquipments,"
//                 @" SpaceInfo.deptID";
                uint occup=sqlite3_column_int(infoStmt, 6);
//                
//            
//                uint numOccupancy=0;
//                NSString* numOccupancyStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:7];                
//                if (numOccupancyStr!=nil && (![numOccupancyStr isEqualToString:@""])){
//                    numOccupancy=[numOccupancyStr intValue];
//                }    
                
                uint spaceOccupancy=0;
                if (occup>0){
                    spaceOccupancy=occup;
                }else{
                    spaceOccupancy=numOccupants;
                }
//                totalNumOccup+=spaceOccupancy;
                
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                UILabel* labelOccupancy = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelOccupancy];
                
                if ([[self.viewModelVC aSelectedProductRep] count]<=1){       
                    labelOccupancy.text = [NSString stringWithFormat: @"Space Occupancy (anticipated or actual):"];
                }else{
                    labelOccupancy.text = [NSString stringWithFormat: @"Total Space Occupancy (anticipated or actual):"];
                }
                [self.infoView addSubview:labelOccupancy];
                [labelOccupancy release];
                
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                [self prepareUILabel:uiLabel];
                uiLabel.text = [NSString stringWithFormat: @"%d",spaceOccupancy];                 
                self.uiOccupancy=uiLabel;        
                [self.infoView addSubview:self.uiOccupancy];        
                [uiLabel release];        
                vPos+=labelHeight;
                pRow++;
                 
                
//                
//                
//                uint numComponents=0;
//                NSString* numComponentsStr=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:8];                
//                if (numComponentsStr!=nil && (![numComponentsStr isEqualToString:@""])){
//                    numComponents=[numComponentsStr intValue];
//                }    
                
//                totalNumEquipment+=numComponents;
                vPos+=rowSpacing;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                UILabel* labelNumComponents = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                [self prepareLabel:labelNumComponents];
                
                if ([[self.viewModelVC aSelectedProductRep] count]<=1){
                    labelNumComponents.text = [NSString stringWithFormat: @"Number of Components (furniture & equipment):"];
                }else{
                    
                    labelNumComponents.text = [NSString stringWithFormat: @"Total Number of Components (furniture & equipment):"];
                }
                [self.infoView addSubview:labelNumComponents];
                [labelNumComponents release];
                
                
                hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                [self prepareUILabel:uiLabel];
//                uiLabel.text = [NSString stringWithFormat: @"%d",numComponents];                
                uiLabel.text = [NSString stringWithFormat: @"%d",numEquipments];
                self.uiNumComponents=uiLabel;        
                [self.infoView addSubview:self.uiNumComponents];        
                [uiLabel release];    
                vPos+=labelHeight;    
                pRow++;
    
                
                
                
                
                if ([[self.viewModelVC aSelectedProductRep] count]<=1){
                    
                    vPos+=rowSpacing;
                    hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                    [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                    UILabel* labelDept= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                    [self prepareLabel:labelDept];        
                    labelDept.text = [NSString stringWithFormat: @"Department:"];
                    [self.infoView addSubview:labelDept];
                    [labelDept release];
                                                            
                    hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                    uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                    [self prepareUILabel:uiLabel];
                    
                    uint deptID=sqlite3_column_double(infoStmt,9);     
                    if (deptID<=0){                        
                        uiLabel.text=@"";
                    }else{
                        uiLabel.text=[[[[appDele currentSite] aDepartment]objectAtIndex:(deptID-1)] name];
                    }
                    self.uiDepartment=uiLabel;        
                    [self.infoView addSubview:self.uiDepartment];        
                    [uiLabel release];
                    vPos+=labelHeight;                
                    pRow++;                                
                }
                uint pSQLRow=10;
                
                
                if ([[self.viewModelVC aSelectedProductRep] count]<=1){
                    for (uint pCustomInt=0;pCustomInt<30;pCustomInt++){
                        uint customInt=sqlite3_column_int(infoStmt,pSQLRow+pCustomInt); 
                        if (customInt<=0) {continue;}
                        
                        NSMutableArray* aCustomColorStruct=[appDele currentSite].aSpaceCustomColorStruct;
    //                    for (CustomColorStruct* c in aCustomColorStruct){
    //                        NSLog(@"%@",[c customSettingName]);
    //                        
    //                    }
    //                    
                        
    //                  CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:pCustomInt];
                        CustomColorStruct* customColorStruct=nil;
                        for (CustomColorStruct* thisCustomColorStruct in aCustomColorStruct){
                            if ([thisCustomColorStruct customSettingNum]==pCustomInt+1){
                                customColorStruct=thisCustomColorStruct;
                            }
                        }
                        if (customColorStruct==nil){
                            continue;
                        }
//                        CustomColor* customColor=[[customColorStruct aCustomColor] objectAtIndex:(customInt-1)];
                        CustomColor* customColor=[customColorStruct getCustomColorWithCustomInt:customInt-1];
                        
                        if (customColor==nil){
                            continue;
                        }
                        
                        vPos+=rowSpacing;
                        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* labelCustomInt= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:labelCustomInt];        
                        labelCustomInt.text = [NSString stringWithFormat: @"%@:",[customColorStruct customSettingName]];
                        [self.infoView addSubview:labelCustomInt];
                        [labelCustomInt release];                                        
                        
                        
                        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                        [self prepareUILabel:uiLabel];
                                        
                        uiLabel.text=[customColor name];
                        [self.infoView addSubview:uiLabel];
    //                    
    //                    self.uiDepartment=uiLabel;        
    //                    [self.infoView addSubview:self.uiDepartment];        
                        [uiLabel release];                    
                        vPos+=labelHeight;
                        pRow++;  
                        
                        
                                      
                    }
                    pSQLRow+=30;
                    
                    
                    
                    CustomHeaderStruct* customHeaderStruct=[appDele currentSite].customHeaderStruct;            
                    if (customHeaderStruct!=nil){
                            
                        for (uint pCustom=0;pCustom<15;pCustom++){
                            NSString* customValue=[ProjectViewAppDelegate readNameFromSQLStmt:infoStmt column:(pSQLRow+pCustom)]; 
                            if (customValue==nil||[customValue isEqualToString:@""]) {continue;}
                            if ([[[customHeaderStruct aCustomSpace] objectAtIndex:pCustom] isEqualToString:@""]){
                                continue;
                            }
    //                        NSMutableArray* aCustomColorStruct=[appDele currentSite].aSpaceCustomColorStruct;
    //                        //                    for (CustomColorStruct* c in aCustomColorStruct){
    //                        //                        NSLog(@"%@",[c customSettingName]);
    //                        //                        
    //                        //                    }
    //                        //                    
    //                        
    //                        CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:pCustom];
    //                        CustomColor* customColor=[[customColorStruct aCustomColor] objectAtIndex:(custom-1)];
    //                        
    //                        
                            
                            
                            vPos+=rowSpacing;
                            hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                       
                            [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                            UILabel* labelCustom= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                            [self prepareLabel:labelCustom];        
                            labelCustom.text = [NSString stringWithFormat: @"%@:",[[customHeaderStruct aCustomSpace] objectAtIndex:pCustom]];
                            [self.infoView addSubview:labelCustom];
                            [labelCustom release];                                        
                            
                            
                            hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;  
                            uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                            [self prepareUILabel:uiLabel];
                            
                            uiLabel.text=customValue;
                            [self.infoView addSubview:uiLabel];
                            //                    
                            //                    self.uiDepartment=uiLabel;        
                            //                    [self.infoView addSubview:self.uiDepartment];        
                            [uiLabel release];
                            vPos+=labelHeight;                        
                            pRow++;  
                            
                            
                            
                        }
                    }

                }
                
//                pSQLRow=pSQLRow+15;                
//                vPos+=labelHeights;
                
                
                
                
                //FUSION Data if not multiple selected
                if ([self.viewModelVC aSelectedProductRep] && [[self.viewModelVC aSelectedProductRep] count]==1){
                    
                    if ([[appDele activeModel] bHasFusionFloorData]){
                        Site* site=[appDele currentSite];
                        
                        vPos+=rowSpacing;
                        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* label_FusionPlannedSFAssignedSF = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:label_FusionPlannedSFAssignedSF];
                        
                        
                        
                        label_FusionPlannedSFAssignedSF.text = [NSString stringWithFormat: @"FUSION: Assigned SF:"];
                        
                        [self.infoView addSubview:label_FusionPlannedSFAssignedSF];
                        [label_FusionPlannedSFAssignedSF release];
                        
                        
                        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                        [self prepareUILabel:uiLabel];
                        //                uiLabel.text = [NSString stringWithFormat: @"%d",numComponents];
                        
                        
                        
                        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
                        [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
                        NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithInt: space.fusion_SpaceInfoAssignedSF]];
//                        NSLog(@"%@", numberAsString);
                        
                        uiLabel.text =  numberAsString;//[NSString stringWithFormat: @"%d",space.fusion_SpaceInfoAssignedSF];
                        [numberFormatter release];numberFormatter=nil;
                        
                        
                        [self.infoView addSubview:uiLabel];
                        [uiLabel release];
                        vPos+=labelHeight;
                        pRow++;
                        
                        
                        vPos+=rowSpacing;
                        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* label_FusionRecordStatus = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:label_FusionRecordStatus];
                        
                        label_FusionRecordStatus.text = [NSString stringWithFormat: @"FUSION: Record Status"];
                        
                        [self.infoView addSubview:label_FusionRecordStatus];
                        [label_FusionRecordStatus release];
                        
                        
                        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                        [self prepareUILabel:uiLabel];
                        //                uiLabel.text = [NSString stringWithFormat: @"%d",numComponents];
                        if (space.fusion_roomStatus){
                            uiLabel.text = [NSString stringWithFormat: @"%@",space.fusion_roomStatus];
                        }else{
                            uiLabel.text = [NSString stringWithFormat: @""];
                        }
                        
                        [self.infoView addSubview:uiLabel];
                        [uiLabel release];
                        vPos+=labelHeight;
                        pRow++;
                        
                        
                        vPos+=rowSpacing;
                        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* label_FusionTop = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:label_FusionTop];
                        
                        label_FusionTop.text = [NSString stringWithFormat: @"FUSION: TOPCSS"];
                        
                        [self.infoView addSubview:label_FusionTop];
                        [label_FusionTop release];
                        
                        
                        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                        [self prepareUILabel:uiLabel];
                        //                uiLabel.text = [NSString stringWithFormat: @"%d",numComponents];
                        
                        if (site.fusionTopDictionary && [site.fusionTopDictionary count]){
                            
//                            NSString* fusionTopStr=site.fusionTopDictionary objectForKey:space.fusion_topColorColor];
                            FusionTopColor* topColor= [site.fusionTopDictionary objectForKey:[NSString stringWithFormat:@"%d",space.fusion_topCssCode]];
                            uiLabel.text = [NSString stringWithFormat: @"%@",topColor.name ];
                        }else{
                            uiLabel.text = [NSString stringWithFormat: @""];
                        }
                        
                        [self.infoView addSubview:uiLabel];
                        [uiLabel release];
                        vPos+=labelHeight;
                        pRow++;
                        
                        
                        
                        
                        vPos+=rowSpacing;
                        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* label_FusionPGM = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:label_FusionPGM];
                        
                        label_FusionPGM.text = [NSString stringWithFormat: @"FUSION: PGM"];
                        
                        [self.infoView addSubview:label_FusionPGM];
                        [label_FusionPGM release];
                        
                        
                        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                        [self prepareUILabel:uiLabel];
                        //                uiLabel.text = [NSString stringWithFormat: @"%d",numComponents];

                        
                        if (site.fusionPGMDictionary && [site.fusionPGMDictionary count]){
                            
                            //                            NSString* fusionTopStr=site.fusionTopDictionary objectForKey:space.fusion_topColorColor];
                            FusionPGMColor* pgmColor= [site.fusionPGMDictionary objectForKey:[NSString stringWithFormat:@"%d",space.fusion_pgmNum]];
                            uiLabel.text = [NSString stringWithFormat: @"%@",pgmColor.name ];
                        }else{
                            uiLabel.text = [NSString stringWithFormat: @""];
                        }
                        
                        [self.infoView addSubview:uiLabel];
                        [uiLabel release];
                        vPos+=labelHeight;
                        pRow++;
                        
                        
                        
                        
                        
                        
                        
                        vPos+=rowSpacing;
                        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);
                        [self drawGreyBGCellWithRowIndex:pRow hPos:hPos vPos:vPos-rowSpacing/2 width:contentWidth height:rowSpacing+labelHeight];
                        UILabel* label_FusionROOMUSE = [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelWidth, labelHeight) ];
                        [self prepareLabel:label_FusionROOMUSE];
                        
                        label_FusionROOMUSE.text = [NSString stringWithFormat: @"FUSION: ROOM USE"];
                        
                        [self.infoView addSubview:label_FusionROOMUSE];
                        [label_FusionROOMUSE release];
                        
                        
                        hPos=(self.rootView.bounds.size.width / 2)+(contentWidth/2)-uiLabelWidth;
                        uiLabel= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, uiLabelWidth, labelHeight) ];
                        [self prepareUILabel:uiLabel];
                        //                uiLabel.text = [NSString stringWithFormat: @"%d",numComponents];
                        
                        
                        if (site.fusionRoomUseDictionary && [site.fusionRoomUseDictionary count]){
                            
                            //                            NSString* fusionTopStr=site.fusionTopDictionary objectForKey:space.fusion_topColorColor];
                            FusionRoomUseColor* roomUseColor= [site.fusionRoomUseDictionary objectForKey:[NSString stringWithFormat:@"%d",space.fusion_roomUseCode]];
                            uiLabel.text = [NSString stringWithFormat: @"%@",roomUseColor.name ];
                        }else{
                            uiLabel.text = [NSString stringWithFormat: @""];
                        }
                        
                        [self.infoView addSubview:uiLabel];
                        [uiLabel release];
                        vPos+=labelHeight;
                        pRow++;
                        
                        
                        
                        
                        
                    }
                    
                    
                }
                
                
                UILabel* labelCell=nil;
                
                
                
                vPos+=rowSpacing*3;
                hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                
                //                hPos+=labelXSpacing;        
                //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
                [self prepareSelectedProductTitle:labelCell];        
                labelCell.text=@"Selected Element:";
                [self.infoView addSubview:labelCell];        
                [labelCell release];  
                //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
                
                vPos+=[self selectedProductTitleFontSize];
                pRow++;
                
                
                
                if ([self.viewModelVC aSelectedProductRep] && [[self.viewModelVC aSelectedProductRep] count]>0){
                    for (uint pViewProductRep=0; pViewProductRep<[[self.viewModelVC aSelectedProductRep] count];pViewProductRep++){
                        ViewProductRep* viewProductRep=[[self.viewModelVC aSelectedProductRep] objectAtIndex:pViewProductRep];
                        OPSProduct* product=[viewProductRep product];
                        //                    if ([product isKindOfClass:[Floor class]]){
                        //                        Floor* floor=(Floor*) product;
                        //                        Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                        //                        product=bldg;
                        //                    }
                        //                    
                        //                                
                        
                        
                        
                        vPos+=rowSpacing;
                        hPos=(self.rootView.bounds.size.width / 2)-(contentWidth/2);                
                        //                hPos+=labelXSpacing;        
                        //                labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, labelCellWidth, labelHeight) ];
                        labelCell= [ [UILabel alloc ] initWithFrame:CGRectMake(hPos, vPos, 400, labelHeight) ];                
                        [self prepareSelectedProductLabel:labelCell];                            
                        labelCell.text=[product productRepDisplayStr];
                        [self.infoView addSubview:labelCell];        
                        [labelCell release];  
                        //        hPos+=labelCellWidth;   //Temporary commented caused of value stored never read warning   
                        
                        vPos+=labelHeight;
                        pRow++;
                        
                    }
                }else{

                    
                }
                uint contentHeight=vPos+topBotInset;
                
                [self.infoScrollView setContentSize:CGSizeMake(rootFrame.size.width, contentHeight)];
                
            }
        }        
        
        
//        
//        uiNetCalculated.text=[ProjectViewAppDelegate doubleToAreaStr:totalNetArea];
//        uiOccupancy.text=[NSString stringWithFormat: @"%d",totalNumOccup];
//        uiNumComponents.text=[NSString stringWithFormat: @"%d",totalNumEquipment]; 
        
    }
    
    
    sqlite3_close(database);
    
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end

//
//  BIMPlanColorCatNVC.h
//  ProjectView
//
//  Created by Alfred Man on 4/19/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewModelToolbar;
@interface BIMPlanColorCatNVC : UINavigationController{    
    ViewModelToolbar* _viewModelToolbar;
}

@property (nonatomic, assign) ViewModelToolbar* viewModelToolbar;
-(id) initWithRootViewController:(UIViewController *)rootViewController viewModelToolbar:(ViewModelToolbar*) viewModelToolbar;
@end

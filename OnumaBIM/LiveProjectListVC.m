//
//  LiveProjectListVC.m
//  ProjectView
//
//  Created by Alfred Man on 1/5/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import "LiveProjectListVC.h"
#import "ProjectViewAppDelegate.h"
//#import "DataConnect.h"
#import "ViewNeedWebForLiveDB.h"
#import "CXMLDocument.h"
#import "OPSStudio.h"

#import "TableCellTitleProjectCat.h"
#import "LiveProjectListVC.h"
#import "TableCellTitleProject.h"
#import "OPSProjectSite.h"
#import "OPSLocalProjectSite.h"
#import "ViewPlanSiteAndBldgInfoVC.h"
#import "ProjectViewTabBarCtr.h"
//#import "OPSProjectSiteAboutToDownload.h"


@implementation LiveProjectListVC
@synthesize isOnlyLoadingSiteInfo;
@synthesize connectionData;
@synthesize projectSiteSelected;
@synthesize theConnection;
@synthesize completeFileSize;
@synthesize fileBeingDownloaded;
@synthesize projectSiteBeingUploaded=_projectSiteBeingUploaded;

uint const LiveProjectListVC_OVERWRITE_ALERT = 7;
uint const LiveProjectListVC_LAUNCH_LOCAL_ALERT = 8;
uint const LiveProjectListVC_UPLOADBEFOREOVERWRITE_ALERT=9;





/*
- (void)siteDetail:(id)sender event:(id)event{
    
    
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    if ([appDele isLiveDataSourceAvailable]){
        
        
        
        NSSet *touches = [event allTouches];
        UITouch *touch = [touches anyObject];
        CGPoint currentTouchPosition = [touch locationInView:self.theTableView];
        
        NSIndexPath *indexPath = [self.theTableView indexPathForRowAtPoint: currentTouchPosition];
        
        if (indexPath==nil){
            return;
        }
        
        [self setIsOnlyLoadingSiteInfo:true];
        [self downloadSiteFromSelectedRow:[indexPath row] bNoGraphicData:true];
        
        
    }else{        
        
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        
    }   
    
    
    
    
    return;
}
*/


- (uint) readSiteListFromServer{        
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    //    int errorCode=[appDele checkStudioWithUserName:<#(NSString *)#> userPW:<#(NSString *)#>];
    //    if (errorCode!=0) {return errorCode;}
    
    uint errCode=0;
    NSMutableArray* _projectArray=[[NSMutableArray alloc] init];
    self.projectArray=_projectArray;
    [_projectArray release];
    
    NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithString: @"MY PROJECTS:"];
    TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:0 iconName:nil];
    [tbCellTitleMyProjectStr release];
    [self.projectArray addObject:tbCellTitleMyProject];
    [tbCellTitleMyProject release];
    
    //    self.projectArray=[[NSMutableArray alloc] init];
    errCode=[appDele reCheckUserNameAndPW];
    if (errCode!=0) {return errCode;}
    
    NSString *urlString = [NSString stringWithFormat: @"http://www.onuma.com/plan/webservices/baseXML.php?sysID=%d&u=%@&p=%@&sites", [appDele activeLiveStudio].ID, [ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]]];
    NSURL *url = [NSURL URLWithString:urlString ];
    NSError* error;
    CXMLDocument *parser = [[[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:&error] autorelease];
    //    - (id)initWithContentsOfURL:(NSURL *)aURL options:(NSUInteger)mask error:(NSError **)errorPtr
    
    
    //    TableCellTitleProject* tbCellTitleProject=nil;
    //    NSMutableArray *res = [[NSMutableArray alloc] init];                     
    NSArray *rows = [parser nodesForXPath:@"//ROW" error:nil];
    
    for (CXMLElement *row in rows) {        
        
                
        NSArray* siteIDNode=[row nodesForXPath:@"COL[1]/DATA" error:nil];       
        NSString* siteIDStr=[[siteIDNode objectAtIndex:0] stringValue];
        NSUInteger siteID=[siteIDStr integerValue];
        
        NSArray* nameDataNode=[row nodesForXPath:@"COL[2]/DATA" error:nil];       
        NSString *siteName=[[nameDataNode objectAtIndex:0] stringValue];
        
        
//        NSString *firstString = @"I'm a noob at Objective-C", *finalString;
        
        siteName = [siteName stringByReplacingOccurrencesOfString:@"\\" withString:@"_"];
        
        siteName = [siteName stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
        
        
        
        NSArray* siteSharedNode=[row nodesForXPath:@"COL[3]/DATA" error:nil];       
        NSString* siteSharedStr=[[siteSharedNode objectAtIndex:0] stringValue];
        NSUInteger siteShared=[siteSharedStr integerValue];
        
        
        
        NSArray* siteProjectIDNode=[row nodesForXPath:@"COL[11]/DATA" error:nil];       
        NSString* siteProjectIDStr=[[siteProjectIDNode objectAtIndex:0] stringValue];
        NSUInteger siteProjectID=[siteProjectIDStr integerValue];
        
        
        NSArray* siteProjectNameNode=[row nodesForXPath:@"COL[12]/DATA" error:nil];      
        NSString *siteProjectName=[[NSString alloc ] initWithString:[[siteProjectNameNode objectAtIndex:0] stringValue]];
                        
        NSArray* siteThumnailNameNode=[row nodesForXPath:@"COL[13]/DATA" error:nil];   
        NSString *siteThumnailName=[[NSString alloc ] initWithString:[[siteThumnailNameNode objectAtIndex:0] stringValue]];        
        
        siteName=[[NSString alloc] initWithFormat: @"S%d_%d - %@",[appDele activeLiveStudio].ID,siteID,siteName];
      
//        if (siteID==807){
//            int x=0;
//            x=1;
//        }         
        
        
        
        OPSProjectSite* projectSite=[[OPSProjectSite alloc] init:siteID name:siteName shared:siteShared projectID:siteProjectID projectName:siteProjectName iconName:siteThumnailName];
        [siteName release];
        [siteProjectName release];
        [siteThumnailName release];
        
        //        NSLog(@"%@", name);
        //        Project* project= [[Project alloc] init:projectID guid:NULL name:name ];
        
        if (siteShared==1 && ((TableCellTitleObject*) [self.projectArray lastObject]).shared!=1 ){
            NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"READ ONLY PROJECTS:"];
            TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:1 iconName:nil];
            [tbCellTitleMyProjectStr release];
            [self.projectArray addObject:tbCellTitleMyProject];
            [tbCellTitleMyProject release];
        }
        if (siteShared==2 && ((TableCellTitleObject*) [self.projectArray lastObject]).shared!=2 ){
            NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"READ / WRITE PROJECTS:"];
            TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:2 iconName:nil];
            [tbCellTitleMyProjectStr release];
            [self.projectArray addObject:tbCellTitleMyProject];
            [tbCellTitleMyProject release];
        }
        
        
        
        if ([[[self projectArray] lastObject] isKindOfClass:[TableCellTitleProjectCat class]] || 
            (([[self.projectArray lastObject] isKindOfClass:[OPSProjectSite class]]) && 
             ((OPSProjectSite*) [self.projectArray lastObject]).projectID!=siteProjectID)
            ){            
            //        if ([[self projectArray] count]==1 || 
            //            (([[self.projectArray lastObject] isKindOfClass:[OPSProjectSite class]]) && 
            //             ((OPSProjectSite*) [self.projectArray lastObject]).projectID!=siteProjectID)
            //        ){            
            NSString* tbCellTitleProjectNameStr=[[NSString alloc] initWithFormat: @"Project: %@",siteProjectName];
            //            [tbCellTitleProject release];
            TableCellTitleProject* tbCellTitleProject=[[TableCellTitleProject alloc] init:siteProjectID name:tbCellTitleProjectNameStr shared:siteShared iconName:nil];
            [tbCellTitleProjectNameStr release];
            [self.projectArray addObject:tbCellTitleProject];             
            [tbCellTitleProject release];
        }
        
        
        [self.projectArray addObject:projectSite];
        [projectSite release];
        //        [project release];                     
    }
    //    [tbCellTitleProject release];
    
    
    //  and we print our results
    //  NSLog(@"%@", res);
    //    [res release];
    
    return errCode;
    
}



- (void)downloadSiteButton:(id)sender event:(id)event{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
    
    
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.theTableView];
    
	NSIndexPath *indexPath = [self.theTableView indexPathForRowAtPoint: currentTouchPosition];
    //	if (indexPath != nil)
    //	{
    //		[self tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    //	}
    
    if (indexPath==nil){
        return;
    }
    
    OPSProjectSite* projectSite=[tableData objectAtIndex:indexPath.row];  
    self.projectSiteSelected=projectSite;
    [appDele setActiveProjectSite:projectSite];
    
    if ([appDele isLiveDataSourceAvailable]){

//        OPSProjectSiteAboutToDownload* projectSiteAboutToDownload=[[OPSProjectSiteAboutToDownload alloc]initWithLiveOPSProjectList:self] ;  
        
        [self tryDownloadSite];
//        [projectSiteAboutToDownload release];
//        
//        
//        OPSLocalProjectSite* localProjectSite=[[OPSLocalProjectSite alloc] initWithLiveOPSProjectSite:projectSite];
//        
//        if (localProjectSite){
//            int localModifiedDate=[localProjectSite getLocalDatabaseModifiedDate];
//            if (localModifiedDate >=0){ //local Site downloaded before
//                int liveModifiedDate=[localProjectSite liveModifiedDate];            
//                if (liveModifiedDate>localModifiedDate){
//                    
//                }else{
//                    
//                }
//            }
//        }else{
//
////        bool isLocalProjectAttachmentEMpty=[localProjectSite isLocalProjectAttachmentEmpty];        
//            [self setIsOnlyLoadingSiteInfo:false];                
//            [self downloadSiteFromSelectedRow:[indexPath row] bNoGraphicData:false];
//        }
//        
    }else{        
        
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"Internet Access is needed before the live scheme can be downloaded. Please select Local Studio or retry later with internet access" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        
    }   
    
    
        
    
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{                  
        
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    OPSProjectSite* projectSite=[tableData objectAtIndex:[indexPath row]]; 
    self.projectSiteSelected=projectSite;
    [appDele setActiveProjectSite:projectSite];
    if ([appDele isLiveDataSourceAvailable]){
        
        
        
        [self setIsOnlyLoadingSiteInfo:true];
        [self downloadSiteFromSelectedRow:[indexPath row] bNoGraphicData:true];
        
        
    }else{        
        
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"Internet Access is needed before the live scheme can be downloaded. Please select Local Studio or retry later with internet access" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        
    }
    
    
    

    /*
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    if ([appDele isLiveDataSourceAvailable]){
        
        
        [self setIsOnlyLoadingSiteInfo:false];
        [self downloadSiteFromSelectedRow:[indexPath row] bNoGraphicData:false];  
        
    }else{        
        
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        
    }   
    */
    // Navigation logic may go here. Create and push another view controller.  
}


//- (void) openProjectToDisplay:(OPSProjectSite*) projectSite{
//    [self.navigationController:false];
//    [self openProjectToDisplay:projectSite];
//    
//}




- (void)downloadSiteFromSelectedRow:(int) row bNoGraphicData:(bool) bNoGraphicData{
//    int row=[indexPath row];    
    if (!([[tableData objectAtIndex:row] isKindOfClass: [OPSProjectSite class]])){
        return;
    }    
    
    OPSProjectSite* projectSite=[tableData objectAtIndex:row];  
    self.projectSiteSelected=projectSite;

    
    //    OPSProjectSite* projectSite=[[[appDel dataConnecter] projectArray] objectAtIndex:row];                    
        
    self.theTableView.allowsSelection = false;//!active;
    self.theTableView.scrollEnabled = false;//!active;
    //    if (!active) {
    //        [searchDisableViewOverlay removeFromSuperview];
    //        [searchBar resignFirstResponder];
    //    } else {
    self.downloadDisableViewOverlay.alpha = 0;
//    self.navigationItem.leftBarButtonItem.enabled=false;
//    UINavigationController* nvc=self.navigationController;
//    UINavigationBar* nvb=nvc.navigationBar;
    
//    UINavigationItem* nvi=[nvc navigationItem];
    
    self.navigationItem.hidesBackButton = TRUE;
//    [nvi setHidesBackButton:true ];                
//    SEL completeAction=[self copyModelFromServer];
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        
    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Downloading Site Information from Onuma BIM Server for site: \n\r%@",projectSiteSelected.name] invoker:self completeAction:@selector(copyModelFromServer:) actionParamArray:[NSArray arrayWithObjects:[NSNumber numberWithBool:bNoGraphicData], nil]];            
        
    
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
}

- (void) copyModelFromServer:(NSArray*) paramArray{
//- (void) copyModelFromServer{//:(OPSProjectSite*) projectSite{      
    bool bNoGraphicData=false;
    if (paramArray!=nil){
        NSNumber* numNoGraphicData=[paramArray objectAtIndex:0];
        bNoGraphicData=[numNoGraphicData boolValue];
    }
        
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];  
    //    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=44&siteID=%d&filename=OPS", [project ID]];
    
    
    
    
    NSString *urlString = nil;
    if (bNoGraphicData){        
        urlString=[NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=%d&siteID=%d&beta=1&siteInfoOnly=1",[appDele activeLiveStudio].ID, [[appDele activeProjectSite] ID]];
    }else{
        urlString=[NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=%d&siteID=%d&beta=1",[appDele activeLiveStudio].ID, [[appDele activeProjectSite] ID]];
    }
    NSURL *url = [NSURL URLWithString: urlString];
    //    UIImage *image = [[UIImage imageWithData: [NSData dataWithContentsOfURL: url]] retain];
    
    
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:url
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:1200.0];
    // create the connection with the request
    // and start loading the data
    // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file    
//    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];    
    self.theConnection=aConnection;
    [aConnection release];
    if (theConnection) {
        // Create the NSMutableData that will hold
        // the received data
        // receivedData is declared as a method instance elsewhere        
        connectionData=[[NSMutableData data] retain];
    } else {    
        // Inform the user that the connection failed.        
		UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[connectFailMessage show];
		[connectFailMessage release];        
    }
    
    
    /*
    //    NSData *fetchedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"",projectID]];
    
    NSData *fetchedData = [NSData dataWithContentsOfURL:url];    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; 
    
    
    NSString* databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName],[appDele activeLiveStudio].ID,[appDele activeLiveStudio].name,[appDele activeLiveStudio].iconName]] ;
    NSFileManager *NSFm= [NSFileManager defaultManager]; 
    BOOL isDir=YES;    
    if(![NSFm fileExistsAtPath:databaseFolderPath isDirectory:&isDir])
        if(![NSFm createDirectoryAtPath:databaseFolderPath withIntermediateDirectories:isDir attributes:Nil error:Nil])
            NSLog(@"Error: Create folder failed");
    
    //    [projectSite setDbPath: [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%@_I_%@/%@.sqlite",userName,activeLiveStudio.name,activeLiveStudio.iconName, [projectSite name]]] ];     
    
    NSString* dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.sqlite", [projectSite name]]] ];
    [projectSite setDbPath: dbPath];   
    [dbPath release];
    
    [fetchedData writeToFile:projectSite.dbPath atomically:YES];    
    
    
    //-----------------------------------Create PList File of project inside Studio---------------------------------------------------------------
    NSMutableDictionary *dictionary=nil;
    //    NSString *path = [[NSBundle mainBundle] bundlePath];        
    
    NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
    if(![NSFm fileExistsAtPath:dictionaryPath isDirectory:NO]){        
        if(![NSFm createFileAtPath:dictionaryPath contents:[NSData dataWithContentsOfFile:@""] attributes:Nil ]  )        {
            NSLog(@"Error: Create projectInfoFile failed");
        }
        dictionary = [NSMutableDictionary dictionaryWithObject:[projectSite toDictionaryObject] forKey:projectSite.name];
        
    }else{
        dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];           
        [dictionary setObject:[projectSite toDictionaryObject] forKey:projectSite.name];  
    }    
    // write xml representation of dictionary to a file
    [dictionary writeToFile:dictionaryPath atomically:YES];
    //--------------------------------------------------------------------------------------------------    
    
    [self removeDownloadDisableOverlay];
    [super openProjectToDisplay:projectSite];
     */
    return;
    
    
}

/*

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse*)response {            
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; 
    
    
    NSString* databaseFolderPath=nil;
    if (isOnlyLoadingSiteInfo){
        databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",[appDele defUserName],[appDele activeLiveStudio].ID,[appDele activeLiveStudio].name,[appDele activeLiveStudio].iconName]] ;
    }else{
        databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName],[appDele activeLiveStudio].ID,[appDele activeLiveStudio].name,[appDele activeLiveStudio].iconName]] ;
    }
    NSFileManager *NSFm= [NSFileManager defaultManager]; 
    BOOL isDir=YES;    
    if(![NSFm fileExistsAtPath:databaseFolderPath isDirectory:&isDir])
        if(![NSFm createDirectoryAtPath:databaseFolderPath withIntermediateDirectories:isDir attributes:Nil error:Nil])
            NSLog(@"Error: Create folder failed");
    
    //    [projectSite setDbPath: [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%@_I_%@/%@.sqlite",userName,activeLiveStudio.name,activeLiveStudio.iconName, [projectSite name]]] ];     
    
    NSString* dbPath=nil;
    if (isOnlyLoadingSiteInfo){
        dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/tmpSiteInfo.sqlite"]] ];
    }else{
        dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.sqlite", [projectSiteSelected name]]] ];
    }
    [projectSiteSelected setDbPath: dbPath];   
    [dbPath release];
    
    
    
//    filename = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:save_name]; // filename is in .h file
    
//    [[NSFileManager defaultManager] createFileAtPath:filename contents:nil attributes:nil];
    [[NSFileManager defaultManager] createFileAtPath:[projectSiteSelected dbPath] contents:nil attributes:nil];    
    self.fileBeingDownloaded=
    [[NSFileHandle fileHandleForUpdatingAtPath:[projectSiteSelected dbPath]] retain];// file is in .h 
    
    if (self.fileBeingDownloaded)   {        
        [self.fileBeingDownloaded seekToEndOfFile];
    }
    
    

    downloadLabel.text=[NSString stringWithFormat:@"Downloading File from Server for Site: %@",[appDele activeProjectSite].name]; 
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    if (self.fileBeingDownloaded)  { 
        
        [self.fileBeingDownloaded seekToEndOfFile];
        
    } [self.fileBeingDownloaded writeData:data]; 
    
}

- (void)connectionDidFinishLoading:(NSURLConnection*)connection { 
    
    [self.fileBeingDownloaded closeFile]; 
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //-----------------------------------Create PList File of project inside Studio---------------------------------------------------------------
    NSMutableDictionary *dictionary=nil;
    //    NSString *path = [[NSBundle mainBundle] bundlePath];        
    

    NSFileManager *NSFm= [NSFileManager defaultManager]; 
    NSString* databaseFolderPath=[[projectSiteSelected dbPath] stringByDeletingLastPathComponent];
    NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
    if(![NSFm fileExistsAtPath:dictionaryPath isDirectory:NO]){        
        if(![NSFm createFileAtPath:dictionaryPath contents:[NSData dataWithContentsOfFile:@""] attributes:Nil ]  )        {
            NSLog(@"Error: Create projectInfoFile failed");
        }
        dictionary = [NSMutableDictionary dictionaryWithObject:[projectSiteSelected toDictionaryObject] forKey:projectSiteSelected.name];
        
    }else{
        dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];           
        [dictionary setObject:[projectSiteSelected toDictionaryObject] forKey:projectSiteSelected.name];  
    }    
    // write xml representation of dictionary to a file
    [dictionary writeToFile:dictionaryPath atomically:YES];
    //--------------------------------------------------------------------------------------------------    
    
    //    [self removeDownloadDisableOverlay];    
    //    self.navigationItem.hidesBackButton = FALSE;
    [appDele removeLoadingViewOverlay];
    
    
    if (isOnlyLoadingSiteInfo){
        OPSProjectSite* projectSite=self.projectSiteSelected;
        [appDele setActiveProjectSite:self.projectSiteSelected];
        [appDele readUnitStructWithDBPath:[projectSite dbPath]];                
        [appDele displayProjectListSiteInfoVC: self];
    }else{
        [super openProjectToDisplay:projectSiteSelected];        
    }
    

    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    // receivedData is declared as a method instance elsewhere
    
    
    //[connection release
    

//    [theConnection release];
//    theConnection=nil;
//    [connectionData release];
//    connectionData=nil;

    
    // inform the user
    UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"Connection Failed"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
    [didFailWithErrorMessage show];
    [didFailWithErrorMessage release];
	
    //inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];
}
*/

#pragma mark NSURLConnection methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
//	CGFloat size = [[NSString stringWithFormat:@"%lli",[response expectedContentLength]] floatValue];
    
    
//    NSHTTPURLResponse *hr = (NSHTTPURLResponse*)response;
//    NSURLResponse *hr = (NSURLResponse*)response;    
//    NSDictionary *dict = [hr allHeaderFields];
    
    
//	CGFloat size = [[[response allHeaderFields] objectForKey:@"Content-Length"] unsignedIntegerValue];    
//	CGFloat size = [[dict objectForKey:@"Content-Length"] unsignedIntegerValue];        
	//NSLog(@"filesize: %f", size);
//	completeFileSize=size;

    
    
    
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
	
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
	
    // receivedData is an instance variable declared elsewhere.
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    downloadLabel.text=[NSString stringWithFormat:@"Downloading File from Server for Site: %@",[appDele activeProjectSite].name]; 
//    downloadLabel.text=[NSString stringWithFormat:@"Copying file from the Onuma BIM Server for the site: \n\r%@\n\rThis is a one time operation and will take a few moments.\n\rAfter the site is copied to the Onuma Local page on the iPad you will be able to work offline with this site.",[appDele activeProjectSite].name];     

    [connectionData setLength:0];

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.            
    
    [connectionData appendData:data];
    NSLog(@"data length: %d",[connectionData length]);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    // receivedData is declared as a method instance elsewhere
    
    //[connection release
    [theConnection release];
    theConnection=nil;
    [connectionData release];
    connectionData=nil;

    
    // inform the user
    UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"Connection Failed"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
    [didFailWithErrorMessage show];
    [didFailWithErrorMessage release];
	
    //inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    // receivedData is declared as an instance variable elsewhere
    // in this example, convert data (from plist) to a string and then to a dictionary
    
    //    NSData *fetchedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"",projectID]];
//    NSData *fetchedData = [NSData dataWithContentsOfURL:url];
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; 
    
    
    NSString* databaseFolderPath=nil;
    
    uint err=[appDele reCheckUserNameAndPW];
    if (err!=0) return;
    
    
    if (isOnlyLoadingSiteInfo){
        databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName],[appDele activeLiveStudio].ID,[appDele activeLiveStudio].name,[appDele activeLiveStudio].iconName]] ;
    }else{
        databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName],[appDele activeLiveStudio].ID,[appDele activeLiveStudio].name,[appDele activeLiveStudio].iconName]] ;
    }
    NSFileManager *NSFm= [NSFileManager defaultManager]; 
    BOOL isDir=YES;    
    if(![NSFm fileExistsAtPath:databaseFolderPath isDirectory:&isDir])
        if(![NSFm createDirectoryAtPath:databaseFolderPath withIntermediateDirectories:isDir attributes:Nil error:Nil])
            NSLog(@"Error: Create folder failed");
    
    //    [projectSite setDbPath: [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%@_I_%@/%@.sqlite",userName,activeLiveStudio.name,activeLiveStudio.iconName, [projectSite name]]] ];     
    
    NSString* dbPath=nil;
    if (isOnlyLoadingSiteInfo){
        dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/tmpSiteInfo.sqlite"]] ];
    }else{
        dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.sqlite", [projectSiteSelected name]]] ];
    }
    [projectSiteSelected setDbPath: dbPath];   
    [dbPath release];
    
    
    [connectionData writeToFile:projectSiteSelected.dbPath atomically:YES];    
    
    
    //-----------------------------------Create PList File of project inside Studio---------------------------------------------------------------
    NSMutableDictionary *dictionary=nil;
    //    NSString *path = [[NSBundle mainBundle] bundlePath];        
    
    NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
    if(![NSFm fileExistsAtPath:dictionaryPath isDirectory:NO]){        
        if(![NSFm createFileAtPath:dictionaryPath contents:[NSData dataWithContentsOfFile:@""] attributes:Nil ]  )        {
            NSLog(@"Error: Create projectInfoFile failed");
        }
        dictionary = [NSMutableDictionary dictionaryWithObject:[projectSiteSelected toDictionaryObject] forKey:projectSiteSelected.name];
        
    }else{
        dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];           
        [dictionary setObject:[projectSiteSelected toDictionaryObject] forKey:projectSiteSelected.name];  
    }    
    // write xml representation of dictionary to a file
    [dictionary writeToFile:dictionaryPath atomically:YES];
    //--------------------------------------------------------------------------------------------------    
    
//    [self removeDownloadDisableOverlay];    
//    self.navigationItem.hidesBackButton = FALSE;
    [appDele removeLoadingViewOverlay];
    
    
    if (isOnlyLoadingSiteInfo){
        OPSProjectSite* projectSite=self.projectSiteSelected;
        [appDele setActiveProjectSite:self.projectSiteSelected];
        [appDele readUnitStructWithDBPath:[projectSite dbPath]];                
        [appDele displayProjectListSiteInfoVC: self];
    }else{
        UIViewController* vc=[[self.navigationController viewControllers] lastObject];
        
//        UIViewController* vc2=[[self.navigationController viewControllers] objectAtIndex:[self.navigationController viewControllers].count-2];
        if ([vc isKindOfClass:[ViewPlanSiteAndBldgInfoVC class]]){
            [self.navigationController popViewControllerAnimated:false];
        }
        [super openProjectToDisplay:projectSiteSelected];
    }
    

    
//    NSString *dataString = [[NSString alloc] initWithData: receivedData  encoding: 	
//                            NSMutableDictionary *dictionary = [dataString propertyList];
//                            [dataString release];
                            
                            //store data in singleton class for use by other parts of the program
//                            SingletonObject *mySharedObject = [SingletonObject sharedSingleton];
//                            mySharedObject.aDictionary = dictionary;
                            
                            //alert the user
                            //alert the user
//                            NSString *message = [[NSString alloc] initWithFormat:@"Succeeded! Received %d bytes of 	
//                                                 data",[receivedData length]];
//                                                 NSLog(message);
//                                                 NSLog(@"Dictionary:\n%@",dictionary);
//                                                 UIAlertView *finishedLoadingMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message:message  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
//                                                 [finishedLoadingMessage show];
//                                                 [finishedLoadingMessage release];
//                                                 [message release];
                                                 
                                                 // release the connection, and the data object
//    [connection release];
    [theConnection release];
    theConnection=nil;
    [connectionData release];
    connectionData=nil;
}

-(void)updateProgressBar{
//	[[[[rootController.viewControllers objectAtIndex:0] viewControllers] objectAtIndex:0] updateProgressBar:(connectionData.length/completeFileSize)];
    
}

-(void) loadView{
    [super loadView];    
    
    
    int height = 44;//self.frame.size.height;
    int width = 800;
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor blackColor];
    navLabel.font = [UIFont boldSystemFontOfSize:16];
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text=self.title;
    self.navigationItem.titleView = navLabel;
    [navLabel release]; 
    

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {    
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  [super tableView:tableView cellForRowAtIndexPath:indexPath];
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([appDele isLiveDataSourceAvailable]){                  
//        [[appDele dataConnecter] readSiteListFromServer];
//        self.tableData=[[[appDele dataConnecter] projectArray] copy];                
        [self readSiteListFromServer];
        [tableData release];
        tableData=[self.projectArray copy];
        [self.theTableView reloadData];
    }else{
        ViewNeedWebForLiveDB* viewNeedWebForLiveDB=[[ViewNeedWebForLiveDB alloc] initWithNibName:@"ViewNeedWebForLiveDB" bundle:[NSBundle mainBundle]];                 
        [[self navigationController] pushViewController:viewNeedWebForLiveDB animated:YES];
        [viewNeedWebForLiveDB release];                 
    }    
}
- (void) dealloc{
    [_projectSiteBeingUploaded release];_projectSiteBeingUploaded=nil;
    [projectSiteSelected release];
    [fileBeingDownloaded release];
//    [connectionData release]; //This is already released in the connection Finish
//    [theConnection release]; //This is already released in the connection Finish
    [super dealloc];
}











-(void) tryDownloadSite{
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
    if (![appDele isLiveDataSourceAvailable]){     
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"Internet Access is needed before the live scheme can be downloaded. Please select Local Studio or retry later with internet access" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        return;
    }   
    
    
    
    
    OPSProjectSite* liveProjectSite=[appDele activeProjectSite];        
    OPSLocalProjectSite* localProjectSite=[[OPSLocalProjectSite alloc] initWithLiveOPSProjectSite:liveProjectSite];
    
    int localModifiedDate=-1;
    bool isLocalProjectAttachmentEmpty=true;
    
    if (localProjectSite){        
        
        localModifiedDate=[localProjectSite getLocalDatabaseModifiedDate];
        if (localModifiedDate>=0){            
            isLocalProjectAttachmentEmpty=[localProjectSite isLocalProjectAttachmentEmpty];  
        }else{
            [self downloadSite];     
            [localProjectSite release];
            return;
        }
    }else{
        [self downloadSite];
        [localProjectSite release];
        return;
    }        
    
    
    int liveModifiedDate=[localProjectSite liveModifiedDate];  
    [localProjectSite release];
    
    
    
    
    if (liveModifiedDate>localModifiedDate){
        if (isLocalProjectAttachmentEmpty){            
            UIAlertView *alert = [[UIAlertView alloc] 
                                  initWithTitle: @"You downloaded this scheme before. But it has been modified since download, overwrite?"
                                  message: @""
                                  delegate: self
                                  cancelButtonTitle: @"Overwrite"
                                  otherButtonTitles: @"Cancel", nil];
            alert.tag = LiveProjectListVC_OVERWRITE_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
            [alert show];
            [alert release];                                
        }else{
            UIAlertView *alert = [[UIAlertView alloc] 
                                  initWithTitle: @"You downloaded this scheme before. But it has been modified since download. However, the local site has attachment yet to be uploaded"
                                  message: @""
                                  delegate: self
                                  cancelButtonTitle: @"Canel"
                                  otherButtonTitles: @"Upload First", @"Just Overwrite", nil];
            alert.tag = LiveProjectListVC_UPLOADBEFOREOVERWRITE_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
            [alert show];
            [alert release];                       
        }
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle: @"You downloaded this scheme before. It hasn't been modified since then. Launch your downloaded scheme?"
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"Yes"
                              otherButtonTitles: @"No", nil];
        alert.tag = LiveProjectListVC_LAUNCH_LOCAL_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];    
    }
    
    
    
    
    
}





- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case LiveProjectListVC_OVERWRITE_ALERT:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	     
                    
                    [self downloadSite];
                }
                    break;
                case 1: // delete
                {
                    
                    NSLog(@"Delete was cancelled by the user");
                    // do the delete
                }
                    break;
            }
            
            
        }     
            break;
        case LiveProjectListVC_LAUNCH_LOCAL_ALERT:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	                    
                    [self launchLocal];
                }
                    break;
                case 1: // delete
                {                    
                    //                    NSLog(@"Delete was cancelled by the user");
                    // do the delete
                }
                    break;
            }
            
            
        }     
            break;            
            
            
        case LiveProjectListVC_UPLOADBEFOREOVERWRITE_ALERT:
        {
            switch (buttonIndex){                    
                case 0: // cancel
                {	     
                    
                    //                    NSLog (@"0");
                }
                    break;
                case 1: // Upload Before Overwrite
                {                                                     
                    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
                    
                    
                    
                    
                    OPSLocalProjectSite* opsLocalProjectSite=[[OPSLocalProjectSite alloc] initWithLiveOPSProjectSite:[appDele activeProjectSite] ];
                    self.projectSiteBeingUploaded=opsLocalProjectSite;
                    [opsLocalProjectSite release];
                    
                    //                    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[opsLocalProjectSite name]]  invoker:opsLocalProjectSite completeAction:@selector(trashAllAttachmentEntry) actionParamArray:nil];
                    
                    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[opsLocalProjectSite name]]  invoker:self.projectSiteBeingUploaded completeAction:@selector(uploadProjectSite:) actionParamArray:nil];
                    
                    
                    
                    /*                    
                     OPSLocalProjetSiteUploadBeforeDownlaod* opsLocalProjetSiteUploadBeforeDownlaod=[[OPSLocalProjetSiteUploadBeforeDownlaod alloc] initWithLiveOPSProjectSite:[appDele activeProjectSite] viewPlanSiteAndBldgInfoVC:self];
                     
                     
                     
                     [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[opsLocalProjetSiteUploadBeforeDownlaod name]]  invoker:opsLocalProjetSiteUploadBeforeDownlaod completeAction:@selector(uploadProjectSite:) actionParamArray:[NSArray arrayWithObjects: self,nil]];
                     
                     [opsLocalProjetSiteUploadBeforeDownlaod release];
                     */
                    
                    /*
                     
                     OPSLocalProjectSite* localProjectSite=[[OPSLocalProjectSite alloc] initWithLiveOPSProjectSite:[appDele activeProjectSite]];
                     
                     [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[localProjectSite name]]  invoker:appDele completeAction:@selector(uploadSelectedProjectSite:) actionParamArray:[NSArray arrayWithObjects: localProjectSite,localProjectSite.dbPath,self,nil]];
                     [localProjectSite release];
                     
                     
                     //                    [self downloadSite:nil];
                     */
                    //                    NSLog (@"1");
                }                    
                    break;
                case 2: // Overwrite 
                {
                    //                    NSLog (@"2");                                           
                    [self downloadSite];
                    //do nothing , cancel
                }
                    
                    
                    break;
            }
        }
            break;
        default:
            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");            
    }	
}



-(void) downloadSite{
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if (![appDele isLiveDataSourceAvailable]){     
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"Internet Access is needed before the live scheme can be downloaded. Please select Local Studio or retry later with internet access" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        return;
    }   
    
    
    
    
    OPSProjectSite* projectSite=[appDele activeProjectSite];
    
    
    
    NSFileManager* fileManager=[NSFileManager defaultManager];        
    [fileManager removeItemAtPath: projectSite.dbPath error:NULL]; 
    
    
    
    
    
    
    //    LiveProjectListVC* liveProjectListVC=[[[self navigationController] viewControllers] objectAtIndex:( [[[self navigationController] viewControllers] count]-2)];
    
    
    
    
    bool bNoGraphicData=false;
    [self setIsOnlyLoadingSiteInfo:false];
       
//    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Generating File from Server for site: %@",projectSite.name] invoker:self completeAction:@selector(copyModelFromServer:) actionParamArray:[NSArray arrayWithObjects:[NSNumber numberWithBool:bNoGraphicData], nil]];
    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Copying file from the Onuma BIM Server for the site: \n\r%@\n\rThis is a one time operation and will take a few moments.\n\rAfter the site is copied to the Onuma Local page on the iPad you will be able to work offline with this site.",projectSite.name] invoker:self completeAction:@selector(copyModelFromServer:) actionParamArray:[NSArray arrayWithObjects:[NSNumber numberWithBool:bNoGraphicData], nil]];
    
    
}


@end

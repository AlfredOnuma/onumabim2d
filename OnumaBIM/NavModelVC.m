//
//  NavModelVC.m
//  ProjectView
//
//  Created by Alfred Man on 11/2/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "NavModelVC.h"
#import "OPSProduct.h"
#import "ProjectViewAppDelegate.h"
#import "Site.h"
#import "Bldg.h"
#import "Furn.h"
#import "RelAggregate.h"
#import "OPSModel.h"
#import "ViewModelVC.h"
#import "SpatialStructure.h"
#import "NavFloorVC.h"
#import "NavPlanNavCntr.h"
#import "Space.h"
#import "Slab.h"
@implementation NavModelVC
@synthesize delegate;
@synthesize tapCount;
//@synthesize tableSelection;

-(ViewModelVC*) parentViewModelVC{
    return (ViewModelVC*) self.delegate;
}



- (id) init: (ViewModelVC*) viewModelVC{
    
    self=[super init];
    if (self){
        self.delegate=viewModelVC;
    }else{
    }
    return self;
}


//@synthesize selectedProduct;
- (id)initWithStyle:(UITableViewStyle)style
{

    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
//    [tableSelection release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate]; 
//    [[appDele model] readModelNavList];
//    [self.tableView reloadData];    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];  
    if ([appDele modelDisplayLevel]==0){
        Site* site=(Site*) [[[self parentViewModelVC] model] root];

    //    RelAggregate* relAgg=[site relAggregate];
    
        int numBldg=[[[site relAggregate] related] count];

        return numBldg;
    }
    if ([appDele  modelDisplayLevel]==1){

        
        Bldg* bldg=(Bldg*) [[[self parentViewModelVC] model] root];        
        RelAggregate* bldgRelAgg=[bldg relAggregate];
        Floor* floor=[[bldgRelAgg related] objectAtIndex:bldg.selectedFloorIndex];
        RelAggregate* floorRelAgg=[floor relAggregate];
        
        int numSpace=[[floorRelAgg  related] count];
        return numSpace;
    }   
    
    if ([appDele  modelDisplayLevel]==2){
        
        
        
        Space* space=(Space*) [[[self parentViewModelVC] model] root];  
        RelContain* relContain=[space relContain];        
        int numFurn=[[relContain  related] count];
        return numFurn;
    }      
    return 0;    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
     
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];     
//    cell.textLabel.text= [[appDele model] getNavItemByIndex:indexPath.row];    
    
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];        
    if ([appDele  modelDisplayLevel]==0){

        Site* site=(Site*) [[[self parentViewModelVC] model] root];
        RelAggregate* relAgg=[site relAggregate];    
        Bldg* bldg=[[relAgg related] objectAtIndex:(indexPath.row)];
        
        
        
        NSString* bldgLabelStr=nil;
//        if ([space spaceNumber]!=nil && ![[space spaceNumber] isEqualToString:@""]){
            bldgLabelStr=[NSString stringWithFormat:@"%d - %@",bldg.ID,[bldg name]]; 
//        }else{
//            spaceLabelStr=[NSString stringWithFormat:@"%@ (%@)",[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]];             
//        }
        
        
        
        
        cell.textLabel.text=bldgLabelStr;//bldg.name;
    }    
    if ([appDele  modelDisplayLevel]==1){

        Bldg* bldg=(Bldg*) [[[self parentViewModelVC] model] root];  
        RelAggregate* bldgRelAgg=[bldg relAggregate];
        Floor* floor=[[bldgRelAgg related] objectAtIndex:bldg.selectedFloorIndex];
        RelAggregate* floorRelAgg=[floor relAggregate];
        Space* space=[[floorRelAgg related] objectAtIndex:(indexPath.row)];
        
        
        NSString* spaceLabelStr=nil;
        if ([space spaceNumber]!=nil && ![[space spaceNumber] isEqualToString:@""]){
            spaceLabelStr=[NSString stringWithFormat:@"%@ - %@ (%@)",[space spaceNumber],[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]]; 
        }else{
            spaceLabelStr=[NSString stringWithFormat:@"%@ (%@)",[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]];             
        }
        
        
        cell.textLabel.text=spaceLabelStr;
    }      
    if ([appDele  modelDisplayLevel]==2){
        
        Space* space=(Space*) [[[self parentViewModelVC] model] root];  
        RelContain* relContain=[space relContain];
        Furn* furn=[[relContain related] objectAtIndex:(indexPath.row)];
        
        
        
        
        NSString* displayStr=nil;
        if ([furn componentName]){
            if (![[furn componentName] isEqualToString:@""]){
                NSString *trimmedString = [[furn componentName] stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                if (![trimmedString isEqualToString:@""]){                        
                    displayStr=[NSString stringWithFormat:@"%@ - %@",[furn componentName],[furn name]];   
                }
            }
        }
        if (displayStr==nil){
            displayStr=[NSString stringWithFormat:@"%@",[furn name]];
        }
        
        
        cell.textLabel.text=displayStr;
    }   
    return cell;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    
    
//    tableSelection = indexPath;
    tapCount++;
    
    switch (tapCount)
    {
        case 1: //single tap
            [self performSelector:@selector(singleTap:) withObject: [NSNumber numberWithInt: (indexPath.row)] afterDelay: .4];
            break;
        case 2: //double tap
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(singleTap:) object:[NSNumber numberWithInt: (indexPath.row)]];
            [self performSelector:@selector(doubleTap:) withObject: [NSNumber numberWithInt: (indexPath.row)]];
            break;
        default:
            break;
    }    
}

#pragma mark -
#pragma mark Table Tap/multiTap

- (void)singleTap:(NSNumber*) _row
{
//    NSUInteger row = [tableSelection row];
    NSUInteger row = [_row intValue];    
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];  



//    
    [[[self parentViewModelVC] popoverController] dismissPopoverAnimated:YES];
    [self parentViewModelVC].popoverController=nil;      
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Single tap detected" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alert show];	
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    if ([appDele modelDisplayLevel]==2){return;}
    
    OPSProduct* selectedProduct=[self findSelectedProduct: row];
    ViewProductRep* viewProductRep=[selectedProduct firstViewProductRep];
    if (viewProductRep==nil){
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        if ([appDele modelDisplayLevel]==0){
            if ([selectedProduct isKindOfClass:[Bldg class]]){
                Bldg* selectedBldg=(Bldg*) selectedProduct;            
                Floor* selectedBldgFirstFloor=[[[selectedBldg relAggregate] related] objectAtIndex:[selectedBldg firstFloorIndex]];            
                viewProductRep=[selectedBldgFirstFloor firstViewProductRep];
            }
        }else if ([appDele modelDisplayLevel]==1){            
        }                
    }
    if (viewProductRep!=nil){        
        [[self parentViewModelVC] clearSelectedProductRep];
        [[self parentViewModelVC] selectAProductRep:viewProductRep];  
    }
    
    tapCount = 0;
    
}

-(OPSProduct*) findSelectedProduct: (NSUInteger) selectedIndex;{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];  

    
    SpatialStructure* rootSpatialStruct=(SpatialStructure*) [[[self parentViewModelVC] model] root];    
    
    OPSProduct* selectedProduct=nil;
    if ([appDele modelDisplayLevel]==0){
        Site* site=(Site*) rootSpatialStruct;
        Bldg* selectedBldg=[[[site relAggregate] related] objectAtIndex:selectedIndex];    
        selectedProduct=selectedBldg;
//        Floor* floor=[[[selectedBldg relAggregate] related] objectAtIndex:0];    
//        Slab* slab=[[[floor relContain] related] objectAtIndex:0];
//        selectedProduct=slab;
        
    }else if ([appDele modelDisplayLevel]==1){        
        Bldg* selectedBldg=(Bldg*) rootSpatialStruct;
        Floor* floor=[[[selectedBldg relAggregate] related] objectAtIndex:selectedBldg.selectedFloorIndex];    
        Space* space=[[[floor relAggregate] related] objectAtIndex:selectedIndex];
        
        
        //        if ([appDele activeViewModelVC].selectedProduct !=Nil){
        //            [[appDele activeViewModelVC].selectedProduct setSelected:FALSE];
        //            [[[appDele activeViewModelVC].selectedProduct viewProduct] setNeedsDisplay];
        //        }
        //        
        //        [space setSelected:TRUE];
        //        [[space viewProduct] setNeedsDisplay];
        //        [[appDele activeViewModelVC] setSelectedProduct:space];
        selectedProduct=space;
    }else if ([appDele modelDisplayLevel]==2){        
        Space* selectedSpace=(Space*) rootSpatialStruct;
        Furn* furn=[[[selectedSpace relContain] related] objectAtIndex:selectedIndex];    

        
        
        //        if ([appDele activeViewModelVC].selectedProduct !=Nil){
        //            [[appDele activeViewModelVC].selectedProduct setSelected:FALSE];
        //            [[[appDele activeViewModelVC].selectedProduct viewProduct] setNeedsDisplay];
        //        }
        //        
        //        [space setSelected:TRUE];
        //        [[space viewProduct] setNeedsDisplay];
        //        [[appDele activeViewModelVC] setSelectedProduct:space];
        selectedProduct=furn;
    }
    //    [[appDele modelViewTabBarController] setSelectedIndex:0];
    return selectedProduct;
    
    //    [self.delegate popoverControllerDidDismissPopover:self];            
    
}


- (void)doubleTap:(NSNumber*) _row
{
    
    //    NSUInteger row = [tableSelection row];
      
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate]; 
    
    
    [[[self parentViewModelVC] popoverController] dismissPopoverAnimated:YES];
    [self parentViewModelVC].popoverController=nil;            
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([appDele modelDisplayLevel]==2){ return;}  
    
    NSUInteger row = [_row intValue];
    
    OPSProduct* selectedProduct=[self findSelectedProduct:row];
    if ([selectedProduct isKindOfClass:[Bldg class]]){
        selectedProduct=[((Bldg*) selectedProduct) selectedFloor];
    }
    
    [[self parentViewModelVC] selectAProductRep:[selectedProduct  firstViewProductRep]];

    [[[self parentViewModelVC] viewModelToolbar] browse:nil];
//    
//    
//    SpatialStructure* rootSpatialStruct=(SpatialStructure*) [[[self parentViewModelVC] model] root];    
//    SpatialStructure* nextRootSpatialStruct=nil;
//    if (appDele.modelDisplayLevel==0){                             
//        Bldg* selectedBldg=[[[rootSpatialStruct relAggregate] related] objectAtIndex:row];
//        nextRootSpatialStruct=(SpatialStructure*) selectedBldg;
//        
//    }else if (appDele.modelDisplayLevel==1){                        
//        Bldg* bldg=(Bldg*) rootSpatialStruct;
//        Floor* floor=(Floor*) [[[rootSpatialStruct relAggregate] related] objectAtIndex:bldg.selectedFloorIndex];        
//        Space* selectedSpace=(Space*) [[[floor relAggregate] related] objectAtIndex:row];
//        nextRootSpatialStruct=(SpatialStructure*) selectedSpace;
//    }
//    [appDele incrementDisplayLevel];
//    [appDele displayViewModelNavCntr:nextRootSpatialStruct];
//    
    tapCount = 0;
}

/*
- (void)doubleTap:(NSNumber*) _row
{

//    NSUInteger row = [tableSelection row];
    
    NSUInteger row = [_row intValue];    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate]; 

    
    
    [[[self parentViewModelVC] popoverController] dismissPopoverAnimated:YES];
    [self parentViewModelVC].popoverController=nil;            
    
    OPSProduct* selectedProduct=[self findSelectedProduct:row];
    [[self parentViewModelVC] selectAProductRep:[selectedProduct firstViewProductRep]];
    
    SpatialStructure* rootSpatialStruct=(SpatialStructure*) [[[self parentViewModelVC] model] root];    
    SpatialStructure* nextRootSpatialStruct=nil;
    if (appDele.modelDisplayLevel==0){                             
        Bldg* selectedBldg=[[[rootSpatialStruct relAggregate] related] objectAtIndex:row];
        nextRootSpatialStruct=(SpatialStructure*) selectedBldg;
        
    }else if (appDele.modelDisplayLevel==1){                        
        Bldg* bldg=(Bldg*) rootSpatialStruct;
        Floor* floor=(Floor*) [[[rootSpatialStruct relAggregate] related] objectAtIndex:bldg.selectedFloorIndex];        
        Space* selectedSpace=(Space*) [[[floor relAggregate] related] objectAtIndex:row];
        nextRootSpatialStruct=(SpatialStructure*) selectedSpace;
    }
    [appDele incrementDisplayLevel];
    [appDele displayViewModelNavCntr:nextRootSpatialStruct];
    
    tapCount = 0;
}
*/
@end

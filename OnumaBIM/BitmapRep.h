//
//  BitmapRep.h
//  ProjectView
//
//  Created by Alfred Man on 12/6/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RepresentationItem.h"
@interface BitmapRep : RepresentationItem {

    double dimensionX;
    double dimensionY;
    double dimensionZ;
    double insertOffsetX;
    double insertOffsetY;
//    CGRect bbBox;
    CGAffineTransform offsetTransform;
    uint insertPoint;       //[1,2,3]
                            //[4,5,6]
                            //[7,8,9]
    bool fixedSize;
    bool mirrorY;
}
//@property (nonatomic, assign) CGRect bbBox;
@property (nonatomic, assign) uint insertPoint;
@property (nonatomic, assign) bool fixedSize;
@property (nonatomic, assign) bool mirrorY;
@property (nonatomic, assign) double dimensionX;
@property (nonatomic, assign) double dimensionY;
@property (nonatomic, assign) double dimensionZ;
@property (nonatomic, assign) double insertOffsetX;
@property (nonatomic, assign) double insertOffsetY;

-(id)copyWithZone:(NSZone *)zone;
-(void) setBBoxinRootContextWithParentTransform: (CGAffineTransform) parentTransform;
-(void) setBBoxinLocalFrameWithLocalTransform: (CGAffineTransform) localFrameTransform;
- (id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName insertPoint:(uint ) newInsertPoint fixedSize: (bool) newFixedSize mirrorY: (bool) newMirrorY dimensionX: (double) newDimensionX dimensionY: (double) newDimensionY dimensionZ: (double) newDimensionZ;
//- (id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName insertPoint:(uint ) newInsertPoint fixedSize: (bool) newFixedSize mirrorY: (bool) newMirrorY dimensionX: (double) newDimensionX dimensionY: (double) newDimensionY dimensionZ: (double) newDimensionZ parentTransform:(CGAffineTransform) parentTransform localTransform:(CGAffineTransform) localFrameTransform;
@end

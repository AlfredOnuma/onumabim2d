//
//  ViewPlanFurnInfoVC.h
//  ProjectView
//
//  Created by Alfred Man on 5/18/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewPlanInfoVC.h"

@interface ViewPlanFurnInfoVC : ViewPlanInfoVC{


    UILabel* uiComponent;
    UILabel* uiComponentName;
    UILabel* uiSerialNumber;
    UILabel* uiType;
    UILabel* uiManufacturer;
    UILabel* uiModelNumber;
    UILabel* uiInstallationDate;
    UILabel* uiPartsWarrantyGuarantor;
    UILabel* uiPartsWarrantyDuration;
    UILabel* uiLaborWarrantyGuarantor;
    UILabel* uiLaborWarrantyDuration;
}

@property (nonatomic, retain) UILabel* uiComponent;
@property (nonatomic, retain) UILabel* uiComponentName;
@property (nonatomic, retain) UILabel* uiSerialNumber;
@property (nonatomic, retain) UILabel* uiType;
@property (nonatomic, retain) UILabel* uiManufacturer;
@property (nonatomic, retain) UILabel* uiModelNumber;
@property (nonatomic, retain) UILabel* uiInstallationDate;
@property (nonatomic, retain) UILabel* uiPartsWarrantyGuarantor;
@property (nonatomic, retain) UILabel* uiPartsWarrantyDuration;
@property (nonatomic, retain) UILabel* uiLaborWarrantyGuarantor;
@property (nonatomic, retain) UILabel* uiLaborWarrantyDuration;


-(id) initWithViewModelVC:(ViewModelVC*) viewModelVC;
@end

//
//  AttachmentWebViewController.h
//  ProjectView
//
//  Created by Alfred Man on 8/25/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AttachmentWebView;

@interface AttachmentWebViewController : UIViewController{
    UIToolbar* _toolbar;
    NSString* _initURLStr;
    bool _hasNavControlBar;
    AttachmentWebView* _webView;
}
@property (nonatomic, assign) bool hasNavControlBar;
@property (nonatomic, retain) NSString* initURLStr;
@property (nonatomic, retain) UIToolbar* toolbar;
@property (nonatomic, retain) AttachmentWebView* webView;

-(id) initWithFrame:(CGRect)frame initURL:(NSString*)initURLStr hasNavControlBar:(bool)hasNavControlBar;
@end

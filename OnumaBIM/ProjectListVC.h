//
//  ProjectListVC.h
//  ProjectView
//
//  Created by Alfred Man on 1/3/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OPSProjectSite;
@interface ProjectListVC : UIViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *projectArray;
    NSMutableArray *tableData;
    UIView *searchDisableViewOverlay;    
    UIView *downloadDisableViewOverlay;
    UITableView *theTableView;
    UISearchBar *theSearchBar;
    UIActivityIndicatorView  *actIndi;
    UILabel* downloadLabel;

}
@property(retain) NSMutableArray *projectArray;
@property(retain) NSMutableArray *tableData;
@property(retain) UIView *searchDisableViewOverlay;
@property(retain) UIView *downloadDisableViewOverlay;
@property(retain) UIActivityIndicatorView  *actIndi;
@property (nonatomic, retain)  UITableView *theTableView;
@property (nonatomic, retain)  UISearchBar *theSearchBar;
@property (nonatomic, retain) UILabel* downloadLabel;
-(void) displayDownloadDisableViewOverlay;
- (void)siteDetail:(id)sender event:(id)event;

//- (NSData *)generatePostDataForDataNSData: (NSData*)uploadData;
//- (void)uploadFile;

- (void)trashSite:(id)sender event:(id)event;
- (NSData *) uploadSite:(id)sender event:(id)event;
- (void) searchBar:(UISearchBar *)searchBar activate:(BOOL) active;

- (void) openProjectToDisplay:(OPSProjectSite*) projectSite;
- (void) removeDownloadDisableOverlay;


-(int) getLocalDatabaseModifiedDate;
- (void)downloadSite:(id)sender event:(id)event;

-(void) launchLocal;



- (uint) readVersion;
@end

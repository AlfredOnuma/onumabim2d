//
//  EverNoteNoteBookBrowserCell.m
//  ProjectView
//
//  Created by onuma on 31/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "EverNoteNoteBookBrowserCell.h"

@implementation EverNoteNoteBookBrowserCell

@synthesize notebook=_notebook;
-(void) dealloc{
    [_notebook release];_notebook=nil;
    [super dealloc];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setNotebook:(EDAMNotebook *)notebook
{
    _notebook = notebook;
    self.textLabel.text = [notebook name];
}
@end

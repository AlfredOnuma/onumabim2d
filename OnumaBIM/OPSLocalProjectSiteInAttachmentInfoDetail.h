//
//  OPSLocalProjectSiteInAttachmentInfoDetail.h
//  ProjectView
//
//  Created by Alfred Man on 7/4/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "OPSLocalProjectSite.h"
@class AttachmentInfoDetail;
@interface OPSLocalProjectSiteInAttachmentInfoDetail : OPSLocalProjectSite
{    
    AttachmentInfoDetail* _attachmentInfoDetailToClose;    
}

@property (nonatomic, assign) AttachmentInfoDetail* attachmentInfoDetailToClose;

-(id) initWithAttaachmentInfoDetailToClose:(AttachmentInfoDetail*)attachmentInfoDetailToClose;
@end

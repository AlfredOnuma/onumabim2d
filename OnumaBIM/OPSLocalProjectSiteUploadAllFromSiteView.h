//
//  OPSLocalProjectSiteUploadAllFromSiteView.h
//  ProjectView
//
//  Created by Alfred Man on 8/1/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "OPSLocalProjectSite.h"
@class AttachmentInfoTable;
@interface OPSLocalProjectSiteUploadAllFromSiteView : OPSLocalProjectSite{
    AttachmentInfoTable* _attachmentInfoTable;
}
@property (nonatomic, assign) AttachmentInfoTable* attachmentInfoTable;

-(id) initWithAttachmentInfoTable:(AttachmentInfoTable*)attachmentInfoTable;
@end

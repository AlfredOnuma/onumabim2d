//
//  FusionPlannedSFAssignedSFTVC.m
//  ProjectView
//
//  Created by onuma on 15/10/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "FusionPlannedSFAssignedSFTVC.h"
#import "ProjectViewAppDelegate.h"
#import "FusionPlannedSFAssignedSFColor.h"
#import "Site.h"

#import "BIMPlanColorTVC.h"



#import "BIMPlanColorCatNVC.h"
#import "Site.h"
#import "DisplayModelUI.h"
#import "ViewModelVC.h"
#import "ViewModelToolbar.h"
#import "Space.h"
@interface FusionPlannedSFAssignedSFTVC ()

@end

@implementation FusionPlannedSFAssignedSFTVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //  #warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //  #warning Incomplete method implementation.
    // Return the number of rows in the section.
    //    AlignColorTVC* alignColorTVC=[[AlignColorTVC alloc] init];
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    Site* site=[appDele currentSite];
//    //    alignColorTVC.title=@"Alignment Color";
//    uint numRow=0;
//    if (site.alignColorDictionary){numRow= [[site.alignColorDictionary allKeys] count];}
//    if (numRow<1){ numRow=1;}
//
//    return  numRow;
    return 5;
}

-(void)addToSelection:(NSIndexPath *) indexPath{
    uint row=[indexPath row];
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    Site* site=[appDele currentSite];
    //    Department* department=[site.aDepartment objectAtIndex:row];
    
    
    
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    
    
    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                Space* space=(Space*)[ viewProductRep product];
                
                
                
                 if (space.fusion_SpaceInfoRoomPathID>0){
                     if (space.fusion_SpaceInfoAssignedSF==0){ //Assigned SF =0
                         if (row==1){
                             if (!viewProductRep.selected){
                                 [viewModelVC addAProductRepToSelection:viewProductRep];
                             }
                         }
                     }else{
                     double sqMsqFt=10.7639104 ;
                     double ratio=space.spaceArea*sqMsqFt/space.fusion_SpaceInfoAssignedSF;
                     double diff=fabs(ratio-1.0);
                     if (diff < 0.01){ //Difference < 1%
                         if (row==2){
                             if (!viewProductRep.selected){
                                 [viewModelVC addAProductRepToSelection:viewProductRep];
                             }
                         }
                     }else if (diff < 0.1){ //Between 1% and 9%
                         if (row==3){
                             if (!viewProductRep.selected){
                                 [viewModelVC addAProductRepToSelection:viewProductRep];
                             }
                         }
                     }else{ //Difference >= 10%
                         if (row==4){
                             if (!viewProductRep.selected){
                                 [viewModelVC addAProductRepToSelection:viewProductRep];
                             }
                         }
                     }
                     
                 }
                 }else{ //No Assigned SF
                     if (row==0){
                         if (!viewProductRep.selected){
                             [viewModelVC addAProductRepToSelection:viewProductRep];
                         }
                     }
                 }                

                
            }
        }
        
        
    }
    
    
}

-(void)removeFromSelection:(NSIndexPath *) indexPath{
    
    uint row=[indexPath row];
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    
    
    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                Space* space=(Space*)[ viewProductRep product];
                
                
                
                if (space.fusion_SpaceInfoRoomPathID>0){
                    if (space.fusion_SpaceInfoAssignedSF==0){ //Assigned SF =0
                        if (row==1){
                            if (viewProductRep.selected){
                                [viewModelVC removeAProductRepFromSelection:viewProductRep];
                            }
                        }
                    }else{
                        double sqMsqFt=10.7639104 ;
                        double ratio=space.spaceArea*sqMsqFt/space.fusion_SpaceInfoAssignedSF;
                        double diff=fabs(ratio-1.0);
                        if (diff < 0.01){ //Difference < 1%
                            if (row==2){
                                if (viewProductRep.selected){
                                    [viewModelVC removeAProductRepFromSelection:viewProductRep];
                                }
                            }
                        }else if (diff < 0.1){ //Between 1% and 9%
                            if (row==3){
                                if (viewProductRep.selected){
                                    [viewModelVC removeAProductRepFromSelection:viewProductRep];
                                }
                            }
                        }else{ //Difference >= 10%
                            if (row==4){
                                if (viewProductRep.selected){
                                    [viewModelVC removeAProductRepFromSelection:viewProductRep];
                                }
                            }
                        }
                        
                    }
                }else{ //No Assigned SF
                    if (row==0){
                        if (viewProductRep.selected){
                            [viewModelVC removeAProductRepFromSelection:viewProductRep];
                        }
                    }
                }
                
            }
        }
        
        
    }
    
    
    
    
}
@end

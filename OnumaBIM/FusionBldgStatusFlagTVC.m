//
//  FusionBldgStatusFlagTVC.m
//  ProjectView
//
//  Created by onuma on 14/10/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "FusionBldgStatusFlagTVC.h"
#import "FusionBldgStatusFlagColor.h"
#import "ProjectViewAppDelegate.h"
#import "BIMPlanColorCatNVC.h"
#import "Site.h"
#import "DisplayModelUI.h"
#import "ViewModelVC.h"
#import "ViewModelToolbar.h"
#import "Bldg.h"

@interface FusionBldgStatusFlagTVC ()

@end

@implementation FusionBldgStatusFlagTVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    //#warning Incomplete method implementation.
    //    // Return the number of rows in the section.
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    Site* site=[appDele currentSite];
    //    uint numRow=0;
    //    if (site.aDepartment){
    //        numRow=[site.aDepartment count];
    //    }
    //    if (numRow<1) {numRow=1;}
    //    return numRow;
    return 4;
}



-(void)addToSelection:(NSIndexPath *) indexPath{
    uint row=[indexPath row];
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    Site* site=[appDele currentSite];
    //    Department* department=[site.aDepartment objectAtIndex:row];
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    
    
    for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
            if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                Floor* floor=(Floor*)[viewProductRep product];
                Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                

                switch (row){
                    case 0:
                        
                        if (bldg.bldgPathID==0){

                        }else if (bldg.fusionBldgPathID>0){
                            if (bldg.fusionBldgStatusFlag_newAdded==1){
                            }else{
                                if (!viewProductRep.selected){
                                    [viewModelVC addAProductRepToSelection:viewProductRep];
                                }
                            }
                        }else{
                        }
                        break;
                    case 1:
                        if (bldg.bldgPathID==0){
                        }else if (bldg.fusionBldgPathID>0){
                            if (bldg.fusionBldgStatusFlag_newAdded==1){
                            }else{
                            }
                        }else{
                            if (!viewProductRep.selected){
                                [viewModelVC addAProductRepToSelection:viewProductRep];
                            }
                        }
                        break;
                    case 2:
                        if (bldg.bldgPathID==0){
                        }else if (bldg.fusionBldgPathID>0){
                            if (bldg.fusionBldgStatusFlag_newAdded==1){
                                if (!viewProductRep.selected){
                                    [viewModelVC addAProductRepToSelection:viewProductRep];
                                }
                            }else{
                            }
                        }else{
                        }
                        break;
                    case 3:
                        if (bldg.bldgPathID==0){
                            if (!viewProductRep.selected){
                                [viewModelVC addAProductRepToSelection:viewProductRep];
                            }
                        }else if (bldg.fusionBldgPathID>0){
                            if (bldg.fusionBldgStatusFlag_newAdded==1){
                            }else{
                            }
                        }else{
                        }
                        break;

                    default:
                        break;
                }
                

            }
        }
    }
    
    
}


-(void)removeFromSelection:(NSIndexPath *) indexPath{
    
    uint row=[indexPath row];
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    
    
    for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
            if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                Floor* floor=(Floor*)[viewProductRep product];
                Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                
                switch (row){
                    case 0:
                        
                        if (bldg.bldgPathID==0){
                            
                        }else if (bldg.fusionBldgPathID>0){
                            if (bldg.fusionBldgStatusFlag_newAdded==1){
                            }else{
                                if (viewProductRep.selected){
                                    [viewModelVC removeAProductRepFromSelection:viewProductRep];
                                }
                            }
                        }else{
                        }
                        break;
                    case 1:
                        if (bldg.bldgPathID==0){
                        }else if (bldg.fusionBldgPathID>0){
                            if (bldg.fusionBldgStatusFlag_newAdded==1){
                            }else{
                            }
                        }else{
                            if (viewProductRep.selected){
                                [viewModelVC removeAProductRepFromSelection:viewProductRep];
                            }
                        }
                        break;
                    case 2:
                        if (bldg.bldgPathID==0){
                        }else if (bldg.fusionBldgPathID>0){
                            if (bldg.fusionBldgStatusFlag_newAdded==1){
                                if (viewProductRep.selected){
                                    [viewModelVC removeAProductRepFromSelection:viewProductRep];
                                }
                            }else{
                            }
                        }else{
                        }
                        break;
                    case 3:
                        if (bldg.bldgPathID==0){
                            if (viewProductRep.selected){
                                [viewModelVC removeAProductRepFromSelection:viewProductRep];
                            }
                        }else if (bldg.fusionBldgPathID>0){
                            if (bldg.fusionBldgStatusFlag_newAdded==1){
                            }else{
                            }
                        }else{
                        }
                        break;
                        
                    default:
                        break;
                }
                
                
            
            }
        }
    }
    
    
    
}
@end

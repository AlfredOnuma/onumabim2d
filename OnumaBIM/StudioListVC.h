//
//  StudioListVC.h
//  ProjectView
//
//  Created by Alfred Man on 12/30/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudioListVC : UIViewController <UITextFieldDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate> {

        UIView *disableViewOverlay;

        NSMutableArray *tableData;
        NSMutableArray *studioArray;
        UITableView *theTableView;
        UISearchBar *theSearchBar;
    }
    

    @property(retain) UIView *disableViewOverlay;

    @property(retain) NSMutableArray *tableData;    
    @property (nonatomic, retain) NSMutableArray* studioArray;
    @property (nonatomic, retain)  UITableView *theTableView;

    @property (nonatomic, retain)  UISearchBar *theSearchBar;
- (void)loadView;


//    - (void) readStudioListFromLocal;
    - (void)searchBar:(UISearchBar *)searchBar activate:(BOOL) active;
@end
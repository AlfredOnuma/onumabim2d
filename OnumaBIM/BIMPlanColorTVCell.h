//
//  BIMPlanColorTVCell.h
//  ProjectView
//
//  Created by Alfred Man on 4/20/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol BIMPlanColorTVCellDelegate
//- (OPSModel*) getModel;
@end
@interface BIMPlanColorTVCell : UITableViewCell{
    UILabel* _colorLabel;
    UIView* _colorUIView;
    UIButton* _addToSelectionButton;    
    UIButton* _minusFromSelectionButton;
    id<BIMPlanColorTVCellDelegate> _delegate;    
}
@property (nonatomic, assign) id<BIMPlanColorTVCellDelegate>delegate;
@property (nonatomic, retain) IBOutlet  UILabel* colorLabel;
@property (nonatomic, retain) IBOutlet UIView* colorUIView;
@property (nonatomic, retain) IBOutlet UIButton* addToSelectionButton;    
@property (nonatomic, retain) IBOutlet UIButton* minusFromSelectionButton;

-(IBAction) addToSelectionButtonTapped:(id)sender event:(id)event;
-(IBAction) removeFromSelectionButtonTapped:(id)sender event:(id)event;
@end

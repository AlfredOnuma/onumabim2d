//
//  OPSLocalProjectSite.h
//  ProjectView
//
//  Created by Alfred Man on 7/3/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "OPSProjectSite.h"

@interface OPSLocalProjectSite : OPSProjectSite

-(id) initWithLiveOPSProjectSite:(OPSProjectSite*) liveProjectSite;

//
//- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
//
//- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
@end

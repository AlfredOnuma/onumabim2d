//
//  FloorViewTableController.h
//  OPSMobile
//
//  Created by Alfred Man on 10/6/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Bldg;
@interface FloorViewTableController : UITableViewController {
    Bldg* bldg;
}

@property (nonatomic, retain) Bldg *bldg;
@end

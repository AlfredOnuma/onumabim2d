//
//  LocalProjectListVC.h
//  ProjectView
//
//  Created by Alfred Man on 1/5/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProjectListVC.h"
@class OPSProjectSiteVersionUpdater;
@interface LocalProjectListVC : ProjectListVC <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UITableViewDelegate, NSURLConnectionDelegate, UIAlertViewDelegate>{
//    NSMutableData* connectionData;    
//    NSURLConnection* theConnection;
    NSIndexPath* selectedIndexPath;
    OPSProjectSiteVersionUpdater* _versionUpdater;
    
    OPSProjectSite* _projectSiteBeingUploaded;
    
}
@property (nonatomic, retain) OPSProjectSite* projectSiteBeingUploaded;
@property (nonatomic, retain) OPSProjectSiteVersionUpdater* versionUpdater;
@property (nonatomic, retain) NSIndexPath* selectedIndexPath;
//@property (nonatomic, retain) NSMutableData* connectionData;
//@property (nonatomic, retain) NSURLConnection* theConnection;

-(void) alertSiteNeedUpdate:(id)sender;
-(void) addProjectTitleToProjectArrayAtIndex:(uint) index;
-(void) refreshTable;
-(void) alertTrashSiteConfirm:(id)sender title:(NSString*)title;
@end

//
//  FontSizeTableViewController.h
//  ProjectView
//
//  Created by Alfred Man on 11/20/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewModelVC;
@interface FontSizeTableViewController : UITableViewController
{
    ViewModelVC* _viewModelVC;
}
@property (nonatomic, assign) ViewModelVC* viewModelVC;
-(void) increaseFontSize;
-(void) decreaseFontSize;

- (id)initWithViewModelVC:(ViewModelVC*) viewModelVC Style:(UITableViewStyle)style;
@end

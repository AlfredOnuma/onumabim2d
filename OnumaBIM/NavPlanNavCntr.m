//
//  NavPlanNavCntr.m
//  ProjectView
//
//  Created by Alfred Man on 10/27/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//


//#import "DisplayPlanNavCntr.h"
#import "NavPlanNavCntr.h"
//#import "ViewReportNavCntr.h"
#import "ProjectViewAppDelegate.h"
//#import "ViewModelTabBarCtr.h"

@implementation NavPlanNavCntr
//@synthesize disableCascadedPop;
//
//- (UIViewController *)popViewControllerAnimated:(BOOL)animated
//{     
//    /*
//    if ([self disableCascadedPop]){
//        [self setDisableCascadedPop:false];
////        return [super popViewControllerAnimated:animated];
//    }else{
//        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];         
//     
//        [appDele setModelDisplayLevel:([appDele modelDisplayLevel]-1)];
//        
//        NSMutableArray *localViewControllersArray =[appDele modelViewTabBarController].viewControllers ;
//        DisplayPlanNavCntr* displayPlanNavCntr=  [localViewControllersArray  objectAtIndex:0];
//        ViewReportNavCntr *viewReportNavCntr = [localViewControllersArray  objectAtIndex:2];
//        [displayPlanNavCntr setDisableCascadedPop:TRUE];
//        [displayPlanNavCntr popViewControllerAnimated:false];
//        [viewReportNavCntr setDisableCascadedPop:TRUE];
//        [viewReportNavCntr popViewControllerAnimated:false];
//    
//    }
//    */
//    
//    /*
//    if([[self.viewControllers lastObject] class] == [SettingsTableController class]){
//    
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration: 1.00];
//    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
//                           forView:self.view cache:NO];
//    
//    UIViewController *viewController = [super popViewControllerAnimated:NO];
//    
//    [UIView commitAnimations];
//    
//    return viewController;
//    } else {
//        */
//        return [super popViewControllerAnimated:animated];
////    }
//}

/*
- (UIViewController *)popViewControllerAnimated:(BOOL)animated
{
//	if([[self.viewControllers lastObject] class] == [SettingsTableController class]){
        
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration: 1.00];
		[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                               forView:self.view cache:NO];
        
		UIViewController *viewController = [super popViewControllerAnimated:NO];
        
		[UIView commitAnimations];
        
		return viewController;
//	} else {
//		return [super popViewControllerAnimated:animated];
//	}
}
 */
- (id) init{
    self=[super init];
    if (self){        
//        disableCascadedPop=false;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];    
    if (self) {
//        disableCascadedPop=false;
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

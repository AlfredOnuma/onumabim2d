//
//  CustomHeaderStruct.m
//  ProjectView
//
//  Created by Alfred Man on 5/22/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "CustomHeaderStruct.h"
#import "ProjectViewAppDelegate.h"
@implementation CustomHeaderStruct
@synthesize aCustomBldg;
@synthesize aCustomSpace;

@synthesize siteID;
-(void)dealloc{
    [aCustomBldg release];aCustomBldg=nil;
    [aCustomSpace release];aCustomSpace=nil;
    [super dealloc];
}

-(id) initWithDatabase:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt{
    self=[super init];
    if (self){        
//        
//        self.ID= (sqlite3_column_int(sqlStmt,0));
        self.siteID=(sqlite3_column_int(sqlStmt,0));
        
    
        uint pCol=1;    
        
        for (uint pCustomBldg=0;pCustomBldg<20;pCustomBldg++){
            NSString* bldgCustomStr=[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol];
            pCol++;
            if (bldgCustomStr && ![bldgCustomStr isEqualToString:@""]){  
                if (aCustomBldg==nil){
                    aCustomBldg=[[NSMutableArray alloc] init];
                }
                [self.aCustomBldg addObject:bldgCustomStr];
            }
            
            
//            [customColor release];
        }

        if (aCustomSpace==nil){
            aCustomSpace=[[NSMutableArray alloc] init];
        }           
        for (uint pCustomSpace=0;pCustomSpace<15;pCustomSpace++){
            NSString* spaceCustomStr=[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol];
            pCol++;
            
            if (spaceCustomStr && ![spaceCustomStr isEqualToString:@""]){      
                [self.aCustomSpace addObject:spaceCustomStr];
            }else{
                [self.aCustomSpace addObject:[NSString stringWithFormat:@""]];
            }
            
            
            //            [customColor release];
        }
        
    }
    
    
    return self;
}
@end

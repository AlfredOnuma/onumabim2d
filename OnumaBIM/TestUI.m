//
//  TestUI.m
//  OPSMobile
//
//  Created by Alfred Man on 9/15/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "TestUI.h"


@implementation TestUI

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    self.backgroundColor=[UIColor cyanColor];
    self.alpha=0.3;
    
    
    if (self) {
        // Initialization code
    }
    return self;
}





// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    

    CGFloat width=768;
    CGFloat w2=width/2;
    CGFloat height=1004;
    CGFloat h2=height/2;
        CGMutablePathRef path = CGPathCreateMutable();    
      /*  
        
        CGPathMoveToPoint(path,NULL,0,0);          
        CGPathAddLineToPoint(path,NULL,width,0);        
        CGPathAddLineToPoint(path,NULL,width/2,height);
    */
    
    
    CGPathMoveToPoint(path,NULL,-w2,-h2);          
    CGPathAddLineToPoint(path,NULL,-w2+width,-h2);        
    CGPathAddLineToPoint(path,NULL,-w2+width/2,height-h2);
    
    
    

    
    
//        CGPathAddLineToPoint(path,NULL,0,height);
        
        CGContextRef context = UIGraphicsGetCurrentContext();     

        
        CGContextSetRGBStrokeColor(context, 0.0, 0.0,0.0, 1);
        CGContextSetRGBFillColor(context, 1.0, 0.0, 0.0, 0.6);    
        
        CGContextSetLineWidth(context, 8.0);
    
    
    
    /*
    CGContextSetLineWidth(context, 8.0);
    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    CGRect rectangle = CGRectMake(60,170,200,80);
    CGContextAddRect(context, rectangle);
    CGContextStrokePath(context);
    CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
    CGContextFillRect(context, rectangle);    
    CGContextStrokePath(context);
*/
    
    CGContextBeginPath(context);
    CGContextAddPath(context,path);      
    CGContextClosePath(context);
    
    
        CGContextFillPath(context);    
    /*
    CGContextBeginPath(context);
    CGContextAddPath(context,path);      
    CGContextClosePath(context);
    */
        CGContextAddPath(context,path);      
    CGContextStrokePath(context);         
     

    
    
    
        return;

}


- (void)dealloc
{
    [super dealloc];
}

@end

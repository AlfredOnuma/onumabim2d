//
//  OPSNavUI.h
//  ProjectView
//
//  Created by Alfred Man on 10/25/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "DisplayModelUI.h"
#import "NavUIScroll.h"

/*
@class NavUIScroll;
@class DisplayModelUI;
@class TestUI3;
 */

@class AttachmentInfo;

@protocol OPSNavUIDelegate
//- (OPSModel*) getModel;
@end

@interface OPSNavUI : UIView <NavUIScrollDelegate, UIScrollViewDelegate>{
    /*
    int modelNavUILevel; //0=>Site Level  || 1=>Floor Level  || 2=>Space Level 
    IBOutlet NavUIScroll* navUIScrollVC;
    IBOutlet DisplayModelUI* displayModelUI;
     TestUI3* testUI;
     */
//    AttachmentInfo* attachmentInfo;
    id <OPSNavUIDelegate> delegate;
    bool isZooming;
    double zoomingStartScale;
    double zoomingEndScale;
}

@property (nonatomic, assign) id <OPSNavUIDelegate> delegate;
//@property (nonatomic, retain) AttachmentInfo* attachmentInfo;

-(ViewModelVC*) parentViewModelVC;
-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView;

-(NavUIScroll*) navUIScroll;

-(bool) shouldDisplayProductViewRepText: (ViewProductLabel*) viewProductLabel ;

-(bool) isProductRepInFrame:(ViewProductRep*) viewProductRep;
//- (void) displayAttachmentInfo;
- (void) refreshProductLabel;
- (void) refreshProduct;
- (void) showHideProductLable;
- (void)refreshProductThread:(id)inData;
- (void)refreshProductThreadDone:(id)inData;

- (void)refreshProductLabelThread:(id)inData;
- (void)refreshProductLabelThreadDone:(id)inData;


/*
@property (nonatomic, retain)  TestUI3* testUI;
@property (nonatomic, retain) DisplayModelUI* displayModelUI;
@property (nonatomic, assign) int modelNavUILevel;
@property (nonatomic, retain) IBOutlet NavUIScroll* navUIScrollVC;
*/
@end

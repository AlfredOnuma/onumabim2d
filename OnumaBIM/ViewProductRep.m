//
//  ViewProductRep.m
//  OPSMobile
//
//  Created by Alfred Man on 7/28/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

//#include "WEPopoverController.h"
//WEPopoverController *navPopover;

//#import "ProductViewButtonRing.h"
#import "ViewProductRep.h"
#import "ProjectViewAppDelegate.h"
#import "DisplayModelUI.h"
#import "ViewModelVC.h"
#import "OPSModel.h"
#import "Space.h"
#import "RelAggregate.h"
#import "CPoint.h"
#import "BBox.h"
#import "Slab.h"
#import "Site.h"
#import "Space.h"
#import "Floor.h"
#import "Bldg.h"
#import "SpatialStructure.h"
#import "ExtrudedAreaSolid.h"

#import "ViewProductLabel.h"
//#import "TestUI4.h"
#import "NavModelVC.h"
#import "NavUIScroll.h"
#import "BitmapRep.h"
#import "furn.h"
#import "ViewProductSprite.h"
#import "SpritePoly.h"
#import "SpriteBitmap.h"
#import <QuartzCore/QuartzCore.h>
#import "SitePolygon.h"
//static int p=0;
@implementation ViewProductRep;
//@synthesize geoProduct;
@synthesize delegate;
@synthesize selected;
@synthesize product;
//@synthesize testPt;
//@synthesize path;
//@synthesize displayMatrix;

//@synthesize refProduct;
@synthesize viewProductLabel;
//@synthesize imageView;
@synthesize aSprite=_aSprite;
@synthesize frameTransform;
@synthesize outsetBuffer;
//@synthesize deleteMePath;

//@synthesize displayID;
float oldX, oldY;
BOOL dragging;

//float outsetBuffer=10.0f;
-(void) setSelected:(bool)_selected{
    selected=_selected;
    NSMutableArray* aSelectedProductRep=[[self parentViewModelVC] aSelectedProductRep] ;
    int pObjectInSelectedIndex=-1;
    
    if (aSelectedProductRep&& [aSelectedProductRep count]>0){
        for (uint pSelected=0;pSelected<[aSelectedProductRep count];pSelected++){
            if ([aSelectedProductRep objectAtIndex:pSelected] == self){
                pObjectInSelectedIndex=pSelected;
                break;
            }
        }
    }
    
    if (selected){
        if (pObjectInSelectedIndex<0){
            [aSelectedProductRep addObject:self];
        }
    }else{
        if (pObjectInSelectedIndex>=0){
            [aSelectedProductRep removeObjectAtIndex:pObjectInSelectedIndex];
        }
    }

    
//    if (_selected==false){
//        NSLog(@"Alert Selected False:%@ of type: %@",[product name],[product productType]);
//
//    }
}
-(void) addSprite:(ViewProductSprite*) sprite{    
    if (self.aSprite==nil){
        NSMutableArray* aSprite=[[NSMutableArray alloc] init];
        self.aSprite=aSprite;
        [aSprite release];
    }
    [self.aSprite addObject:sprite];
}
- (ViewModelVC*) parentViewModelVC{
    return (ViewModelVC*) [((DisplayModelUI*) self.delegate) parentViewModelVC];
}


- (BOOL)containsPoint:(CGPoint)point //onPath:(UIBezierPath*)path inFillArea:(BOOL)inFill
{
    BOOL    isHit = false;           
//    CGRect myFrameRect=self.frame;
//    isHit = CGPathContainsPoint( path,NULL, point, false);    
//    if (!CGRectContainsPoint(myFrameRect, point)){
//        return false;
//    }

        for (ViewProductSprite* theSprite in self.aSprite){
            CGPoint testPoint=CGPointMake(point.x, point.y);
            
            CGAffineTransform theTransform=CGAffineTransformInvert( self.frameTransform);
            theTransform=CGAffineTransformTranslate(theTransform,-outsetBuffer, -outsetBuffer);                
    //        CGAffineTransform theTransform=( self.frameTransform);        
    //        theTransform=CGAffineTransformMakeTranslation(-outsetBuffer, -outsetBuffer);        
    //        CGAffineTransform theTransform=CGAffineTransformMakeTranslation(-outsetBuffer, -outsetBuffer);                
            
    //                testPt=CGPointApplyAffineTransform(testPoint, theTransform);
            CGAffineTransform invTransform=CGAffineTransformInvert( theSprite.transform);

            
            theTransform=CGAffineTransformConcat(theTransform, invTransform);
            testPoint=CGPointApplyAffineTransform(testPoint, theTransform);
    //        testPt=testPoint;
            isHit = CGPathContainsPoint( theSprite.bPath.CGPath ,NULL, testPoint, false);
            if (isHit) {
    //            [self setNeedsDisplay];
                return true;
            }
        
        }    
    
//    [self setNeedsDisplay];
    return false;
}


- (void) displayProductInfo:(OPSProduct *)product{
    /*
     // Create a label with custom text 
     UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
     [label setText:[space name]];
     [label setBackgroundColor:[UIColor clearColor]];
     [label setTextColor:[UIColor whiteColor]];
     [label setTextAlignment:UITextAlignmentCenter];
     
     UIFont *font = [UIFont boldSystemFontOfSize:20];
     [label setFont:font];
     CGSize size = [label.text sizeWithFont:font];
     CGRect frame = CGRectMake(0, 0, size.width + 10, size.height + 10); // add a bit of a border around the text
     label.frame = frame;
     
     //  place inside a temporary view controller and add to popover
     UIViewController *viewCon = [[UIViewController alloc] init];
     viewCon.view = label;
     viewCon.contentSizeForViewInPopover = frame.size;       // Set the content size
     
     navPopover = [[WEPopoverController alloc] initWithContentViewController:viewCon];
     [navPopover presentPopoverFromRect:CGRectMake(0, 0, 50, 57)
     inView:self
     permittedArrowDirections:UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown
     animated:YES];    
     */    
}

//
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    
//    
////    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];           
//    ViewModelVC* modelVC=[self parentViewModelVC];//[appDele activeViewModelVC];    
//    CGPoint pt = [[touches anyObject] locationInView:self];   
//    if ([self containsPoint:pt]){
//        [modelVC selectAProduct:[self refProduct]];
//    }
////
////    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];       
//
////    ViewModelVC* modelVC=[self parentViewModelVC];//[appDele activeViewModelVC];
//    
////    [modelVC selectAProduct:[self refProduct]];
//}
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {   
//}
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
//}
//    /*
//    if (modelVC.selectedProduct !=Nil){
//        [modelVC.selectedProduct setSelected:FALSE];
//        [[modelVC.selectedProduct viewProduct] setNeedsDisplay];
//    }
//    
//    [[self product] setSelected:TRUE];
//    [modelVC setSelectedProduct:[self product]];
//    /*
//    NavModelVC* navModelVC=[appDele activeNavModelVC];
//    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
//    UITableView* tableView=[navModelVC tableView];
//    [tableView selectRowAtIndexPath:indexPath animated:YES  scrollPosition:UITableViewScrollPositionBottom];
//    
//    */
//    
//    /*
//    SpatialStructure* rootSpatialStruct=[[[appDele activeViewModelVC] model] root];    
//    if ([appDele modelDisplayLevel]==0){
//        Site* site=rootSpatialStruct;
//        Bldg* selectedBldg=[[[site relAggregate] related] objectAtIndex:row];    
//        
//        Floor* floor=[[[selectedBldg relAggregate] related] objectAtIndex:0];    
//        Slab* sla=[[[floor relContain] related] objectAtIndex:0];
//        if ([appDele activeViewModelVC].selectedProduct !=Nil){
//            [[appDele activeViewModelVC].selectedProduct setSelected:FALSE];
//            [[[appDele activeViewModelVC].selectedProduct viewProduct] setNeedsDisplay];
//        }
//        [slab setSelected:TRUE];
//        [[slab viewProduct] setNeedsDisplay];
//        [[appDele activeViewModelVC] setSelectedProduct:slab];
//        
//    }else if ([appDele modelDisplayLevel]==1){        
//        Bldg* selectedBldg=rootSpatialStruct;
//        Floor* floor=[[[selectedBldg relAggregate] related] objectAtIndex:0];    
//        Space* space=[[[floor relAggregate] related] objectAtIndex:row];
//        
//        
//        if ([appDele activeViewModelVC].selectedProduct !=Nil){
//            [[appDele activeViewModelVC].selectedProduct setSelected:FALSE];
//            [[[appDele activeViewModelVC].selectedProduct viewProduct] setNeedsDisplay];
//        }
//        
//        [space setSelected:TRUE];
//        [[space viewProduct] setNeedsDisplay];
//        [[appDele activeViewModelVC] setSelectedProduct:space];
//    }
//    */
//    
//    [self setNeedsDisplay];
//    return;
//    
//    /*
//    UITouch *touch = [[event allTouches] anyObject];
//    //    CGPoint touchLocation = [touch locationInView:self.view]; 
//    if ([touch tapCount]==2){
////    displayProductInfo:[self product];
//    }
//     }
//
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
//    return;
//    UITouch *touch = [[event allTouches] anyObject];
//    //    CGPoint touchLocation = [touch locationInView:self.view];
//    CGPoint touchLocation = [touch locationInView:self.delegate];
//    
//    if (dragging) {        
//        CGRect frame = self.frame;
//        frame.origin.x = self.frame.origin.x + touchLocation.x - oldX;
//        frame.origin.y =  self.frame.origin.y + touchLocation.y - oldY;
//        self.frame = frame;
//        
//        
//        //        CGRect frame = window.frame;
//        //        frame.origin.x = window.frame.origin.x + touchLocation.x - oldX;
//        //        frame.origin.y =  window.frame.origin.y + touchLocation.y - oldY;
//        //        window.frame = frame;
//        
//    }
//    
//    oldX = touchLocation.x;
//    oldY = touchLocation.y;
//}
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {    
//
//    /*
//    UITouch *touch = [[event allTouches] anyObject];  
//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];           
//    ViewModelVC* modelVC=[appDele activeViewModelVC];
//    
//    CGPoint test=[touch locationInView:self];
//    test.x+=self.frame.origin.x;
//    test.y+=self.frame.origin.y;
//    
//    
//    
//    
//    CGPoint touchLocation = test;//[touch locationInView:[modelVC displayModelUI]];        
//    [(DisplayModelUI*) self.delegate showProductRepButton:touchLocation]   ;
//     */
////    [[modelVC displayModelUI] showProductRepButton:touchLocation];
////    [[modelVC displayModelUI] showProductRepButton:touchLocation]; 
//    
////    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];           
////    ViewModelVC* modelVC=[appDele activeViewModelVC];
////    [modelVC switchToolbarToProductViewToolbar];
//
//    return;
//    dragging = NO;
//    self.userInteractionEnabled = YES;
//    DisplayModelUI* displayModelUI=self.delegate;
//    UIScrollView* scrollView=[displayModelUI.delegate getScrollView];
//    scrollView.scrollEnabled=TRUE;    
//
//    
//    
//    
//}
//


- (id)initWithFrame:(CGRect)frame
{    
    self = [super initWithFrame:frame];
    if (self) {
//        [self setClipsToBounds:true];
        // Initialization code
    }
    return self;
}
/*
- (void) clearProductLabel{
    
    if (self.viewProductLabel!=nil){
        [viewProductLabel removeFromSuperview];
        [viewProductLabel release];
        viewProductLabel=nil;
    }
}
- (void) displayProductLabel{    
    
    if (self.refProduct==nil || self.geoProduct==nil){
        return;
    }
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    ViewModelVC* viewModelVC=[self parentViewModelVC];//(ViewModelVC*) [appDele activeViewModelVC];
    if ([appDele modelDisplayLevel]==0){        
        if ([refProduct isKindOfClass:[Site class]]){
            return;
        }
    }
    if ([appDele modelDisplayLevel]==1){        
        if ([refProduct isKindOfClass:[Floor class]]){
            return;
        }        
        if ([refProduct isKindOfClass:[Bldg class]]){
            return;        
        }
        if ([refProduct isKindOfClass:[Furn class]]){
            return;        
        }
    
    }
    if ([appDele modelDisplayLevel]==2){
        
        if ([refProduct isKindOfClass:[Space class]]){
            return;                
        }
    }    
    if (self.viewProductLabel!=nil){
        [self clearProductLabel];
    }
    

//    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];    
//    NSMutableArray* strArray=[[NSMutableArray alloc] initWithCapacity:2];    
    
    NSString* refProductName=[self.refProduct name];
    
//    double area=[(RepresentationItem*) [[[self.geoProduct representation] aRepresentationItem] objectAtIndex:0] area]; //For now only get the 1st representation item 
    
//    [strArray addObject:[self.refProduct name]];            
//    NSString* labelWithArea=[refProductName 
    
    float fontSize=[viewModelVC currentProductLabelSize]/[viewModelVC navUIScrollVC].zoomScale;    
    CGSize productLabelConstraint = CGSizeMake(fontSize*50, fontSize*2);
    
    
    
    CGSize productLabelSize = [refProductName sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] 
                                          constrainedToSize:productLabelConstraint 
                                              lineBreakMode:UILineBreakModeCharacterWrap];
    
    
    UIView* productLabelLayer=[[viewModelVC displayModelUI] productViewLabelLayer];
//    CGRect productLabelLayerRect=[self convertRect:self.bounds toView:productLabelLayer];
//    CGPoint labelPos=CGPointMake(productLabelLayerRect.origin.x+productLabelLayerRect.size.width/2,productLabelLayerRect.origin.y+productLabelLayerRect.size.height/2); //[self convertPoint:CGPointMake(0, 0) toView:productLabelLayer];
    CGPoint origin=CGPointMake(0, 0);
    
    
    
    if ([self.geoProduct isKindOfClass:[Furn class]]){
//        Furn* thisFurn=self.geoProduct;
//        BitmapRep* bitmapRep=[[thisFurn.representation aRepresentationItem] objectAtIndex:0];        
//
//        BBox* imgViewBBox=[[bitmapRep bBox] copy];
//        [imgViewBBox multiply:[[viewModelVC model] modelScaleForScreenFactor]];
//        self.imageView.frame=[imgViewBBox cgRect];
//        
//        CGAffineTransform imageTransform= CGAffineTransformMakeTranslation(0, bitmapRep.dimensionY*modelScaleFactor);
//        
//        [self.imageView setTransform:CGAffineTransformScale(imageTransform, 1, -1)];         
        CGPoint bitmapPathOrigin=CGPointMake (self.imageView.frame.origin.x, self.imageView.frame.origin.y);
        CGFloat bitmapPathWidth=self.imageView.frame.size.width;
        CGFloat bitmapPathHeight=self.imageView.frame.size.height; 
        CGAffineTransform offset=CGAffineTransformMakeTranslation(bitmapPathOrigin.x+bitmapPathWidth/2, bitmapPathOrigin.y+bitmapPathHeight/2);
        origin=CGPointApplyAffineTransform(origin, offset);
    }
    
    
    // Temporarily disable '''''''''''''''''''''''''''''''''''''''''''''''''''''''

    
//    CGPoint labelPos=[self convertPoint:origin toView:productLabelLayer];    
//    ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(labelPos.x-productLabelSize.width/2, labelPos.y-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) name:[self.refProduct name] area:area displayModelUI:(DisplayModelUI*) self.delegate] ;    
//    [_viewProductLabel setText:refProductName];
//    self.viewProductLabel=_viewProductLabel;
//    [_viewProductLabel release];
//    [productLabelLayer addSubview:self.viewProductLabel];
//    
//    [[viewModelVC displayModelUI] bringSubviewToFront:[viewModelVC displayModelUI].productViewLabelLayer];
        
     //Temporarily disable ----------------------------------------------------
    //
    return;
    
        
    
    
}
*/
/*
- (void)initExtrudedRepBase:(CGRect)frame geoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*)newRefProduct  
{
    
    
    
//    Test7* testUI=[[Test7 alloc] initWithFrame:CGRectMake(0,0,100,200)];
//    testUI.backgroundColor=[UIColor redColor];
//    testUI.alpha=0.3;
//    //    [testUI setTransform:rootSpatialStructTransform];
//    //    [testUI setTransform:<#(CGAffineTransform)#>
////    CGPoint origin=CGPointMake(0,0);    
////    CGPoint labelPos= CGPointApplyAffineTransform(origin, rootSpatialStructTransform);       
////    CGPoint labelLayerPos=[self convertPoint:labelPos toView:productViewLabelLayer]; 
////    [testUI setTransform:CGAffineTransformMakeTranslation(labelLayerPos.x, labelLayerPos.y)];
//    [self addSubview:testUI];
//    [testUI release];
    
    
    
//    self = [super initWithFrame:frame];            
//    if (self) {        
        
    
    
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate* ) [[UIApplication sharedApplication] delegate];
        ViewModelVC* viewModelVC=[self parentViewModelVC];// [appDele activeViewModelVC];
        double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
        if (newRefProduct){
            [newRefProduct addARefViewProductRep:self];
        }
        if (newGeoProduct){
            [newGeoProduct addAGeoViewProductRep:self];
        }
//        [newGeoProduct setViewProduct:self];
        geoProduct=newGeoProduct;        
        refProduct=newRefProduct;
                
        if (geoProduct!=NULL && self.geoProduct.representation!=NULL){                     
//            BBox* repBBox=[self.geoProduct.representation bBox];                                                     
            
            
            
//            CGMutablePathRef _path = CGPathCreateMutable();
            path = CGPathCreateMutable();
//            path = CGPathCreateMutable();   
            Representation* rep=[geoProduct representation];    
            if (rep==NULL) {return;}
            
                        
            
            
            NSMutableArray* aRepItem=[rep aRepresentationItem];
            if (aRepItem==NULL || [aRepItem count]<=0){ return ;}
            BBox* boundBBox=[[BBox alloc] init];
            for (int pRepItem=0;pRepItem<[aRepItem count];pRepItem++){
                RepresentationItem* repItem=[aRepItem objectAtIndex:pRepItem];
                if (repItem==NULL) continue;
               
                ExtrudedAreaSolid* thisExt=(ExtrudedAreaSolid*) repItem;
                NSMutableArray* aCurveProfile=thisExt.aCurveProfile;
                if (aCurveProfile==NULL||[aCurveProfile count]<=0) continue;
                for (int pCurveProfile=0;pCurveProfile<[aCurveProfile count];pCurveProfile++){
                    CurveProfile* curveProfile=[aCurveProfile objectAtIndex:pCurveProfile];
                    if (curveProfile==NULL) continue;
                    NSMutableArray* aPoint=curveProfile.aPoint;
                    if (aPoint==NULL||[aPoint count ]<=2) continue;
                    
                    for (int pPoint=0;pPoint < [aPoint count]; pPoint++){                       
                        CPoint* point=[(CPoint*) [aPoint objectAtIndex:(pPoint)]copy ];
                        point.x*=modelScaleFactor;
                        point.y*=modelScaleFactor;
                        [boundBBox updatePoint:point];
                        
//                        NSLog(@"x = %f, y = %f", point.x, point.y);
                        
                        
                        if (pPoint==0){                                   
//                            CGPathMoveToPoint(_path,NULL,point.x,point.y);                            
                            CGPathMoveToPoint(path,NULL,outsetBuffer+ point.x,outsetBuffer+ point.y);  
                        }else{                            
//                            CGPathAddLineToPoint(_path,NULL,point.x,point.y);
                            CGPathAddLineToPoint(path,NULL,outsetBuffer+ point.x,outsetBuffer+ point.y);
                        }       
                        
                        [point release];
                        
                    }     
                    
                    
                    break;
                }
                                
                
                
                
            }
            
            [self setBounds:CGRectOffset(self.bounds,boundBBox.minX,boundBBox.minY)];
            [boundBBox release];
//            CGPathRetain(path);
//            self.path=_path;            
//            CGPathRelease(_path);
        }
        
        
        
//
//    }
//    return self;
    
}

- (void)initBitmapRepBase:(CGRect)frame geoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*)newRefProduct 
{        
//    self = [super initWithFrame:frame];            
//    if (self) {        
        
//        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
        ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];
        double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
        

//        [newGeoProduct setViewProduct:self];
        if (newRefProduct){
            [newRefProduct addARefViewProductRep:self];
        }
        if (newGeoProduct){
            [newGeoProduct addAGeoViewProductRep:self];
        }
        self.geoProduct=newGeoProduct;        
        self.refProduct=newRefProduct;
        
        
        
        
        if (self.geoProduct.representation!=NULL){                             
            
                                            
            Representation* rep=[self.geoProduct representation];    
            if (rep==NULL) {return ;}                
            
            NSMutableArray* aRepItem=[rep aRepresentationItem];
            if (aRepItem==NULL || [aRepItem count]<=0) {return ;}
            
//            BBox* boundBBox=[[BBox alloc] init];
            for (int pRepItem=0;pRepItem<[aRepItem count];pRepItem++){
                RepresentationItem* repItem=[aRepItem objectAtIndex:pRepItem];
                if (repItem==NULL) continue;
              
                if (![repItem isKindOfClass:[BitmapRep class]]){  continue;}
                
                BitmapRep* bitmapRep=(BitmapRep*) repItem;
                NSString* imgName=[NSString stringWithFormat:@"%@.png",[bitmapRep name]];
                
                UIImageView* _imageView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:imgName]];
                self.imageView = _imageView;
                [_imageView release];
                //                    [bitmapRep.bBox ];
                BBox* imgViewBBox=[[bitmapRep bBox] copy];
                [imgViewBBox multiply:modelScaleFactor];

//                self.imageView.frame=CGRectMake(outsetBuffer, outsetBuffer, [imgViewBBox getWidth], [imgViewBBox getHeight]);

                self.imageView.frame=CGRectMake(0.0f, 0.0f, [imgViewBBox getWidth], [imgViewBBox getHeight]);

                
//                CGAffineTransform imageTransform= CGAffineTransformMakeTranslation(0, bitmapRep.dimensionY*modelScaleFactor);                
//                [self.imageView setTransform:CGAffineTransformScale(imageTransform, 1, -1)];                
                [self addSubview:self.imageView];
                //            [self setNeedsDisplay]; 
                
                path = CGPathCreateMutable();                 
                CGPoint bitmapPathOrigin=CGPointMake (self.imageView.frame.origin.x, self.imageView.frame.origin.y);
                CGFloat bitmapPathWidth=self.imageView.frame.size.width;
                CGFloat bitmapPathHeight=self.imageView.frame.size.height;                
                
                CGPoint p0=CGPointMake(bitmapPathOrigin.x,                  bitmapPathOrigin.y);
                CGPoint p1=CGPointMake(bitmapPathOrigin.x+bitmapPathWidth,  bitmapPathOrigin.y);                   
                CGPoint p2=CGPointMake(bitmapPathOrigin.x+bitmapPathWidth,  bitmapPathOrigin.y+bitmapPathHeight);
                CGPoint p3=CGPointMake(bitmapPathOrigin.x,                  bitmapPathOrigin.y+bitmapPathHeight);
                
                CGPathMoveToPoint   (path,NULL,p0.x,p0.y);                  
                CGPathAddLineToPoint(path,NULL,p1.x,p1.y);                   
                CGPathAddLineToPoint(path,NULL,p2.x,p2.y);                                 
                CGPathAddLineToPoint(path,NULL,p3.x,p3.y);                     

                
                
                [imgViewBBox release]; 
                            
            }
            
//            if (![self.geoProduct isKindOfClass:[Furn class]]){   
//                [self setBounds:CGRectMake(boundBBox.minX, boundBBox.minY, [boundBBox getWidth], [boundBBox getHeight])];
//            }
//            [boundBBox release];
        }

//    }
//    return self;
    
}
*/

NSInteger compareSpriteType(id param1, id param2, void *context) 
{
    
    int retour;
    // fist we need to cast all the parameters
    
    ViewProductSprite* sprite1 = param1;
    ViewProductSprite* sprite2 = param2;
    
    //make the comparaison
//    
//    if ((projectSite1.shared==projectSite2.shared)&& projectSite1.projectID < projectSite2.projectID)
//        retour = NSOrderedAscending;
//    else if ((projectSite1.shared==projectSite2.shared)&&projectSite1.projectID > projectSite2.projectID)
//        retour/Users/onuma/iOS/ProjectView/ViewProductRep.m = NSOrderedDescending;
//    else
//        retour = NSOrderedSame;
//    

    
    if ([sprite1 isKindOfClass:[SpriteBitmap class]] && [sprite2 isKindOfClass:[SpritePoly class]])
        retour = NSOrderedAscending;
    else if ([sprite2 isKindOfClass:[SpriteBitmap class]] && [sprite1 isKindOfClass:[SpritePoly class]])
        retour = NSOrderedDescending;
    else
        retour = NSOrderedSame;
    
    return retour;
    
}
    


-(void) sortSpriteOrder{
    [self.aSprite sortUsingFunction:compareSpriteType  context:nil];
}

- (id)initWithFrame:(CGRect)frame frameTransform:(CGAffineTransform)_frameTransform product:(OPSProduct*) _product displayModelUI:(DisplayModelUI*) displayModelUI
{
    selected=false;

    if ([_product isKindOfClass:[Furn class]]){
        outsetBuffer=0.0f;
    }else{
        outsetBuffer=10.0f;
    }

    CGRect frameWithBuffer=CGRectInset(frame, -outsetBuffer, -outsetBuffer);
    self=[super initWithFrame:frameWithBuffer];
    
    if (self){
        
        product=_product;
        self.delegate=displayModelUI;
        frameTransform=_frameTransform;
        [product addViewProductRep:self];
//        [product setViewProductRep:self];
        /*
        if ([newGeoProduct isKindOfClass:[Furn class]]){
            [self initBitmapRepBase:frameWithBuffer geoProduct:newGeoProduct refProduct:newRefProduct ];
//        return [self initBitmapRepBase:frame geoProduct:newGeoProduct refProduct:newRefProduct ]; 
        }else{        
            [self initExtrudedRepBase:frameWithBuffer geoProduct:newGeoProduct refProduct:newRefProduct ]; 
//            return [self initExtrudedRepBase:frame geoProduct:newGeoProduct refProduct:newRefProduct ]; 
        }
        */
    }
    
    self.opaque = NO;        
    self.backgroundColor = [UIColor clearColor];
    self.alpha=1.0;    
    self.viewProductLabel=nil;
    return self;
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//-(void) drawRect:(CGRect)rect{
//    UIView* t=[[UIView alloc] initWithFrame:CGRectMake(0,0,400,400)];
//    t.backgroundColor=[UIColor redColor];
//    [self addSubview:t];
//}

- (void)drawRect:(CGRect)rect
{
        
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
//    CGContextClip(context); 
    CGContextConcatCTM(context, frameTransform );
    CGContextConcatCTM(context, CGAffineTransformMakeTranslation(outsetBuffer, outsetBuffer) );
  
    
    
//    NSLog(@"sprite Type: %@ Drawn size;%f %f",[[self.product class] description], self.frame.size.width, self.frame.size.height);
    
    for (ViewProductSprite* productSprite in self.aSprite){                
        [productSprite draw:context];
    }
    
    CGContextRestoreGState(context);
    
    
    
    
}
//+(Class) layerClass{
//    return [CATiledLayer class];
//}

- (void)dealloc
{
//    NSLog (@"ViewProductRep/ Dealloc****************");
    while ([self.aSprite count]>0){
        [self.aSprite removeLastObject];
    }
//    for (ViewProductSprite* viewProductSprite in self.aSprite){
//        
//        [viewProductSprite release];
////        viewProductSprite=nil;
//    }
    [_aSprite release];
    _aSprite=nil;
//    [viewProductLabel release];
//    [imageView release];
//    CGPathRelease(path);
//    [path release];
    [super dealloc];
}

@end

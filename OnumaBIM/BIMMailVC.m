//
//  BIMMailVC.m
//  ProjectView
//
//  Created by Alfred Man on 4/4/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//
#import "RecipientCell.h"
#import "BIMMailRecipient.h"
#import "BIMMailGroup.h"
#import "BIMMailVC.h"
#import "CXMLDocument.h"
#import "TouchXML.h"
#import "OPSProjectSite.h"
#import "ProjectViewAppDelegate.h"
#import "BIMMailImageView.h"
#import "ViewModelVC.h"
#import "OPSModel.h"
#import "OPSProduct.h"
#import "OPSToken.h"
#import "Bldg.h"
#import "Space.h"
#import "Floor.h"
#import "AROverlayViewController.h"
#import "ViewModelNavCntr.h"
//#import "BIMMailRecipientCell.h"


@interface UIImagePickerController(Rotating)
- (BOOL)shouldAutorotate;
@end

@implementation UIImagePickerController(Rotating)
- (BOOL)shouldAutorotate {
    return YES;
}
@end



//
//@interface UIImagePickerController(Nonrotating)
//- (BOOL)shouldAutorotate;
//@end
//
//@implementation UIImagePickerController(Nonrotating)
//
//- (BOOL)shouldAutorotate {
//    return NO;
//}
//@end
@implementation BIMMailVC
@synthesize viewModelVC;
@synthesize recipientTable;
@synthesize aRecipientGroup;
@synthesize contentTextView;
@synthesize connectionData;
@synthesize theConnection;
@synthesize subjectTextField;
@synthesize imgScrollView;
@synthesize buttonBin;
@synthesize buttonCamera;
@synthesize buttonImgLibrary;
@synthesize imgLibraryPopOverController;
@synthesize aImg;
@synthesize labelImgStrs;

@synthesize preLoadImgView;
@synthesize postLoadImgView;
@synthesize aInsertImg;
@synthesize aDeleteImg;

@synthesize arOverlayViewController=_arOverlayViewController;


uint const BIMMailVCUIAlertViewTag_NoInternetConfirm=0;

uint const BIMMailVCUIAlertViewTag_BIMailSendSuccess=1;
uint const BIMMailVCUIAlertViewTag_BIMailSendFail=2;
uint const BIMMailVCUIAlertViewTag_NoRecipientSelected=3;
uint const BIMMail_MaximumImageReached=4;
//@synthesize bimMailRecipientCell;
//@synthesize testCell;
//@synthesize checked;



//-(BOOL) prefersStatusBarHidden{
//    return YES;
//}

-(void)dealloc{
//    [testCell release];testCell=nil;
//    [bimMailRecipientCell release];bimMailRecipientCell=nil;
    [_arOverlayViewController release];_arOverlayViewController=nil;
    [aDeleteImg release];aDeleteImg=nil;
    [aInsertImg release];aInsertImg=nil;
    [preLoadImgView release];preLoadImgView=nil;
    [postLoadImgView release];postLoadImgView=nil;
    [labelImgStrs release];labelImgStrs=nil;
    [recipientTable release];recipientTable=nil;
    [aRecipientGroup release];aRecipientGroup=nil;
    [contentTextView release];contentTextView=nil;
    [connectionData release];connectionData=nil;
    [theConnection release];theConnection=nil;
    [subjectTextField release];subjectTextField=nil;
    [imgScrollView release];imgScrollView=nil;
    [buttonCamera release];buttonCamera=nil;
    [buttonImgLibrary release];buttonImgLibrary=nil;
    [buttonBin release];buttonBin=nil;
    [imgLibraryPopOverController release];imgLibraryPopOverController=nil;
    [aImg release];aImg=nil;
    [super dealloc];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    BIMMailGroup* mailGroup=[self.aRecipientGroup objectAtIndex:section];
    return mailGroup.name;
//    if(section == 0)
//        return @"Countries to visit";
//    else
//        return @"Countries visited";
}


- (uint) readRecipientsFromServer{        
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    //    int errorCode=[appDele checkStudioWithUserName:<#(NSString *)#> userPW:<#(NSString *)#>];
    //    if (errorCode!=0) {return errorCode;}
    
    uint errCode=0;
    
    
    NSMutableArray* _aRecipientGroup=[[NSMutableArray alloc] init];
    self.aRecipientGroup=_aRecipientGroup;
    [_aRecipientGroup release];
    /*
    NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"MY PROJECTS:",[appDele defUserName]];
    TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:0 iconName:nil];
    [tbCellTitleMyProjectStr release];
    [self.projectArray addObject:tbCellTitleMyProject];
    [tbCellTitleMyProject release];
    
    */
    //    self.projectArray=[[NSMutableArray alloc] init];            
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
//    NSString *urlString = [NSString stringWithFormat:@"https://www.onuma.com/plan/userGroup2.xml"]; 
    NSString *urlString = [NSString stringWithFormat:@"https://www.onuma.com/plan/bimMailUserGroup.php?sysID=%d&siteID=%d&username=%@&password=%@",appDele.activeStudioID,appDele.activeProjectSite.ID,[ProjectViewAppDelegate urlEncodeValue:appDele.defUserName],[ProjectViewAppDelegate urlEncodeValue:appDele.defUserPW]];   
//    NSLog(@"BimMail Link: %@",urlString);
    NSURL *url = [NSURL URLWithString:urlString ];  
    NSError* error;
    CXMLDocument *parser = [[[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:&error] autorelease];

    
//    NSData* xmlData=[[NSData alloc] initWithContentsOfFile:@"/Users/onuma/iOS/ProjectView/userGroup2.xml"];
//    CXMLDocument* parser=[[CXMLDocument alloc] initWithData:xmlData options:nil error:nil];
//    [xmlData release];

    
    //    TableCellTitleProject* tbCellTitleProject=nil;
    //    NSMutableArray *res = [[NSMutableArray alloc] init];                     
    NSArray *groupRows = [parser nodesForXPath:@"//Group" error:nil];
    
    for (CXMLElement *groupRow in groupRows) {        
        
//        NSArray *userRows = [groupRow nodesForXPath:@"//user" error:nil];
//        for (CXMLElement* userRow in userRows){
//                 
        
        NSString* groupName=[[NSString alloc] initWithString:[[groupRow attributeForName:@"groupname"] stringValue]];  
//        NSArray* test=[groupRow nodesForXPath:@"user" error:nil];
        
        NSArray* userRow=[groupRow nodesForXPath:@"user" error:nil];      
//        uint pUser=0;
        
        
        NSMutableArray* aBIMMailRecipient=[[NSMutableArray alloc] init];
        for (CXMLElement* userNode in userRow){
            NSString *name=[[NSString alloc ] initWithString:[[userNode attributeForName:@"name"] stringValue]];            
            NSString *role=[[NSString alloc ] initWithString:[[userNode attributeForName:@"role"] stringValue]];
            NSString *email=[[NSString alloc] initWithString:[[userNode attributeForName:@"emailaddress"] stringValue]];

            NSString* IDString=[[NSString alloc] initWithString:[[userNode attributeForName:@"id"] stringValue]] ;
            uint ID=[IDString intValue];
            
            BIMMailRecipient* bimmailRecipient=[[BIMMailRecipient alloc] initWithID:ID name:name role:role email:email checked:false];
            
            [aBIMMailRecipient addObject:bimmailRecipient];
            [name release];
            [email release];
            [role release];
            [IDString release];
            
            [bimmailRecipient release];
//            pUser++;
        }
 
        BIMMailGroup* bimmailGroup=[[BIMMailGroup alloc] initWithGroupName:groupName aBIMMailRecipient:aBIMMailRecipient];
        [aBIMMailRecipient release];

        [self.aRecipientGroup addObject:bimmailGroup];
                       
        [bimmailGroup release];
           
        [groupName release];
//            NSString *userName=[[NSString alloc ] initWithString:[[userRow objectAtIndex:0] stringValue]];
//            [self.aRecipientGroup addObject:userName];
            
//        }
        /*
        
        NSArray* siteIDNode=[row nodesForXPath:@"user" error:nil];       
        NSString* siteIDStr=[[siteIDNode objectAtIndex:0] stringValue];
        NSUInteger siteID=[siteIDStr integerValue];
        
        NSArray* nameDataNode=[row nodesForXPath:@"COL[2]/DATA" error:nil];       
        NSString *siteName=[[nameDataNode objectAtIndex:0] stringValue];
        
        
        NSArray* siteSharedNode=[row nodesForXPath:@"COL[3]/DATA" error:nil];       
        NSString* siteSharedStr=[[siteSharedNode objectAtIndex:0] stringValue];
        NSUInteger siteShared=[siteSharedStr integerValue];
        
        
        
        NSArray* siteProjectIDNode=[row nodesForXPath:@"COL[11]/DATA" error:nil];       
        NSString* siteProjectIDStr=[[siteProjectIDNode objectAtIndex:0] stringValue];
        NSUInteger siteProjectID=[siteProjectIDStr integerValue];
        
        
        
        NSArray* siteProjectNameNode=[row nodesForXPath:@"COL[12]/DATA" error:nil];      
        NSString *siteProjectName=[[NSString alloc ] initWithString:[[siteProjectNameNode objectAtIndex:0] stringValue]];
        
        
        
        NSArray* siteThumnailNameNode=[row nodesForXPath:@"COL[13]/DATA" error:nil];   
        NSString *siteThumnailName=[[NSString alloc ] initWithString:[[siteThumnailNameNode objectAtIndex:0] stringValue]];
        
        siteName=[[NSString alloc] initWithFormat: @"S%d_%d - %@",[appDele activeLiveStudio].ID,siteID,siteName];
        
        OPSProjectSite* projectSite=[[OPSProjectSite alloc] init:siteID name:siteName shared:siteShared projectID:siteProjectID projectName:siteProjectName iconName:siteThumnailName];
        [siteName release];
        [siteProjectName release];
        [siteThumnailName release];
        
        //        NSLog(@"%@", name);
        //        Project* project= [[Project alloc] init:projectID guid:NULL name:name ];
        
        if (siteShared==1 && ((TableCellTitleObject*) [self.projectArray lastObject]).shared!=1 ){
            NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"READ ONLY PROJECTS:"];
            TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:1 iconName:nil];
            [tbCellTitleMyProjectStr release];
            [self.projectArray addObject:tbCellTitleMyProject];
            [tbCellTitleMyProject release];
        }
        if (siteShared==2 && ((TableCellTitleObject*) [self.projectArray lastObject]).shared!=2 ){
            NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"READ / WRITE PROJECTS:"];
            TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:2 iconName:nil];
            [tbCellTitleMyProjectStr release];
            [self.projectArray addObject:tbCellTitleMyProject];
            [tbCellTitleMyProject release];
        }
        
        
        
        if ([[[self projectArray] lastObject] isKindOfClass:[TableCellTitleProjectCat class]] || 
            (([[self.projectArray lastObject] isKindOfClass:[OPSProjectSite class]]) && 
             ((OPSProjectSite*) [self.projectArray lastObject]).projectID!=siteProjectID)
            ){            
            //        if ([[self projectArray] count]==1 || 
            //            (([[self.projectArray lastObject] isKindOfClass:[OPSProjectSite class]]) && 
            //             ((OPSProjectSite*) [self.projectArray lastObject]).projectID!=siteProjectID)
            //        ){            
            NSString* tbCellTitleProjectNameStr=[[NSString alloc] initWithFormat: @"Project: %@",siteProjectName];
            //            [tbCellTitleProject release];
            TableCellTitleProject* tbCellTitleProject=[[TableCellTitleProject alloc] init:siteProjectID name:tbCellTitleProjectNameStr shared:siteShared iconName:nil];
            [tbCellTitleProjectNameStr release];
            [self.projectArray addObject:tbCellTitleProject];             
            [tbCellTitleProject release];
        }
        
        
        [self.projectArray addObject:projectSite];
        [projectSite release];
        //        [project release];  
         
         */
    }
    //    [tbCellTitleProject release];
    
    
    //  and we print our results
    //  NSLog(@"%@", res);
    //    [res release];
    
    return errCode;
    
}





- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case BIMMailVCUIAlertViewTag_NoInternetConfirm:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	
                    //                    NSLog(@"Delete was cancelled by the user");
                }
                    break;
                case 1: // delete
                {
                    //                    [self uploadProject:nil event:nil];
                    //                    [self trashAttachmentEntry];
                    // do the delete
                }
                    break;
            }            
            break;
            
            default:
            //            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");
            break;
        }
            break;
        case BIMMailVCUIAlertViewTag_BIMailSendSuccess:
        {
            [self removeInsertImgArray];
            [self executeDeleteImgViewFromStorageAndDB];            
            [self.navigationController popViewControllerAnimated:YES];
            
        }
            break;
            
        case BIMMailVCUIAlertViewTag_BIMailSendFail:
        {
            
        }
            break;
        case BIMMailVCUIAlertViewTag_NoRecipientSelected:            
        {
        
        }
            break;
    }	
}



-(void) alertInvoke:(id)sender tag:(uint)tag title:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle: title
                          message: @""
                          delegate: self
                          cancelButtonTitle: nil
                          otherButtonTitles: @"ok", nil];
    alert.tag = tag; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
    [alert show];
    [alert release];
    
    
}
-(void) alertNoInternet:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle: @"No Internet Available"
                          message: @""
                          delegate: self
                          cancelButtonTitle: nil
                          otherButtonTitles: @"ok", nil];
    alert.tag = BIMMailVCUIAlertViewTag_NoInternetConfirm; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
    [alert show];
    [alert release];
    
    
    
}



 
#pragma mark NSURLConnection methods
 
 - (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
 {
     
 //	CGFloat size = [[NSString stringWithFormat:@"%lli",[response expectedContentLength]] floatValue];
 
 
 //    NSHTTPURLResponse *hr = (NSHTTPURLResponse*)response;
 //    NSURLResponse *hr = (NSURLResponse*)response;    
 //    NSDictionary *dict = [hr allHeaderFields];
 
 
 //	CGFloat size = [[[response allHeaderFields] objectForKey:@"Content-Length"] unsignedIntegerValue];    
 //	CGFloat size = [[dict objectForKey:@"Content-Length"] unsignedIntegerValue];        
 //NSLog(@"filesize: %f", size);
 //	completeFileSize=size;
 
 
 
 
 // This method is called when the server has determined that it
 // has enough information to create the NSURLResponse.
 
 // It can be called multiple times, for example in the case of a
 // redirect, so each time we reset the data.
 
 // receivedData is an instance variable declared elsewhere.
 
 
 //    downloadLabel.text=[NSString stringWithFormat:@"Downloading File from Server for Site: %@",projectSiteSelected.name]; 
 [connectionData setLength:0];
 
 
 
 
 
 //    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
 //    if ([response respondsToSelector:@selector(allHeaderFields)]) {
 //        NSDictionary *dictionary = [httpResponse allHeaderFields];
 //        NSLog(@"%@",[dictionary description]);
 //
 ////        NSLog(@"%d",[response statusCode]);
 //    }
 }
 
 - (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
 {
 // Append the new data to receivedData.
 // receivedData is an instance variable declared elsewhere.            
 
 [connectionData appendData:data];
 }
 
 - (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
 {
 // release the connection, and the data object
 // receivedData is declared as a method instance elsewhere
 
     [theConnection release];theConnection=nil;
     [connectionData release];connectionData=nil;
 
 // inform the user
 UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"Connection Fail " message: @"Sending BIM Mail requires an Internet connection"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
 [didFailWithErrorMessage show];
 [didFailWithErrorMessage release];
 
 //inform the user
 NSLog(@"Connection failed! Error - %@ %@",
 [error localizedDescription],
 [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
 
 
 ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
 [appDele removeLoadingViewOverlay];    
 }

 - (void)connectionDidFinishLoading:(NSURLConnection *)connection
 {
 // do something with the data
 // receivedData is declared as an instance variable elsewhere
 // in this example, convert data (from plist) to a string and then to a dictionary
 
 
 //    NSData *fetchedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"",projectID]];
 
 //    NSData *fetchedData = [NSData dataWithContentsOfURL:url];    
 
 
 
 
 NSString* dataStr=[[NSString alloc] initWithData:connectionData encoding:NSASCIIStringEncoding] ;
 //    NSString* dataStr=[[NSString alloc] initWithData:connectionData encoding:NSUTF8StringEncoding] ;
 NSLog(@"RESPONSEDATA: %@",dataStr);
//     NSUInteger=[@"BIM Mail Sent to" rangeOfString:dataStr].location;
//     if ([@"BIM Mail Sent to:" rangeOfString:dataStr].location != NSNotFound) {
     if ([dataStr rangeOfString:@"BIM Mail Sent to:"].location != NSNotFound) {
//         found = YES;
//     if ([dataStr isEqualToString:@"BIM Mail Sent"]){                  
         [self alertInvoke:nil tag:BIMMailVCUIAlertViewTag_BIMailSendSuccess title:dataStr];
         


     }else{
         [self alertInvoke:nil tag:BIMMailVCUIAlertViewTag_BIMailSendFail title:dataStr];
     }

 //    NSLog(@"Connection Data Length: %d",[connectionData length]);
 [dataStr release];
 
 
 
 


//    NSString *dataString = [[NSString alloc] initWithData: receivedData  encoding: 	
//                            NSMutableDictionary *dictionary = [dataString propertyList];
//                            [dataString release];

//store data in singleton class for use by other parts of the program
//                            SingletonObject *mySharedObject = [SingletonObject sharedSingleton];
//                            mySharedObject.aDictionary = dictionary;

//alert the user
//alert the user
//                            NSString *message = [[NSString alloc] initWithFormat:@"Succeeded! Received %d bytes of 	
//                                                 data",[receivedData length]];
//                                                 NSLog(message);
//                                                 NSLog(@"Dictionary:\n%@",dictionary);
//                                                 UIAlertView *finishedLoadingMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message:message  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
//                                                 [finishedLoadingMessage show];
//                                                 [finishedLoadingMessage release];
//                                                 [message release];

// release the connection, and the data object
//    [connection release],connection=nil;
    [theConnection release],theConnection=nil;
    [connectionData release],connectionData=nil;
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];


}
 



-(IBAction)sendMail:(id)sender{
    uint numRecipient=0;
    if (aRecipientGroup){
        for (BIMMailGroup* recipientGroup in aRecipientGroup){            
            if (recipientGroup.aBIMMailRecipient==nil) continue;
            for (BIMMailRecipient* recipient in recipientGroup.aBIMMailRecipient){
                if (recipient.checked){                    
                    numRecipient++;//+=[recipientGroup.aBIMMailRecipient count];
                }
            }
        }
    }else{
    }
    if (numRecipient>0){
        [self startUploadMail:sender];
    }else{
        [self alertInvoke:nil tag:BIMMailVCUIAlertViewTag_NoRecipientSelected title:@"No Recipient Selected"];
    }
    
    

}

//-(NSData *) startUploadMail:(id)sender{
-(void) startUploadMail:(id)sender{// event:(id)event{
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];  
    
    if (![appDele isLiveDataSourceAvailable]){
        [self alertNoInternet:nil];
        ////        [appDele displayProjectUploadWebView:@"htt[://www.google.com"];
        return;
    }
//    
//	NSSet *touches = [event allTouches];
//	UITouch *touch = [touches anyObject];
//	CGPoint currentTouchPosition = [touch locationInView:self.theTableView];
//    
//	NSIndexPath *indexPath = [self.theTableView indexPathForRowAtPoint: currentTouchPosition];
//    //	if (indexPath != nil)
//    //	{
//    //		[self tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
//    //	}
//    
//    if (indexPath==nil){
//        return nil;
//    }    
//    OPSProjectSite* selectedProjectSite=[tableData objectAtIndex:indexPath.row];  
//    if ([selectedProjectSite dbPath]==Nil) {return nil;}  
    
    
    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Sending Mail"]  invoker:self completeAction:@selector(uploadMail:) actionParamArray:nil];
    
    
    return;
    
    
}



-(NSData*) uploadMail:(NSArray*) paramArray{
    
//    OPSProjectSite* selectedProjectSite=[paramArray objectAtIndex:0];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];      
//    OPSProjectSite* projectSite=[appDele activeProjectSite];
//    NSData* sqliteData=[NSData dataWithContentsOfFile:[selectedProjectSite dbPath]];
    
    
    
    
    
    
    NSString *aBoundary = [NSString stringWithFormat:@"vYRNMz5SSe4vXrnc"];        
    NSMutableData *myData = [NSMutableData data];// [NSMutableData dataWithCapacity:1];
    NSString *myBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", aBoundary];
    NSString *closingBoundary = [NSString stringWithFormat:@"\r\n--%@--\r\n", aBoundary];    
    
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
//    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", [ProjectViewAppDelegate urlEncodeValue:appDele.defUserName]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", appDele.defUserName] dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];

//    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", [ProjectViewAppDelegate urlEncodeValue:appDele.defUserPW]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", appDele.defUserPW] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"sysID\"\r\n\r\n%d", appDele.activeStudioID] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"siteID\"\r\n\r\n%d", appDele.activeProjectSite.ID] dataUsingEncoding:NSUTF8StringEncoding]];    
    
    
    

    switch ([appDele modelDisplayLevel]){
        case 0:
            {
            
         
            }
            break;
        case 1:            
            {            
                [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
                Bldg* currentBldg=(Bldg*) [[viewModelVC model] root];
                [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"bldgID\"\r\n\r\n%d", currentBldg.ID ] dataUsingEncoding:NSUTF8StringEncoding]];    
                [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
                [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"floorID\"\r\n\r\n%d",[currentBldg selectedFloor].ID] dataUsingEncoding:NSUTF8StringEncoding]];                    
            }
            break;
        case 2: 
            {           
                [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
                Space* currentSpace=(Space*) [[viewModelVC model] root];
                uint numViewModelVC=[[[viewModelVC navigationController] viewControllers] count];
                if (numViewModelVC<3) {break;}
                uint pViewModelVC=numViewModelVC-3;
                UIViewController* vc=[[[viewModelVC navigationController] viewControllers] objectAtIndex:pViewModelVC];
                while (!([vc isKindOfClass:[ViewModelVC class]])){
                    pViewModelVC--;
                    vc=[[[viewModelVC navigationController] viewControllers] objectAtIndex:pViewModelVC];
                }
                
                ViewModelVC* prevViewModelVC=(ViewModelVC*) vc;
                Bldg* currentBldg=(Bldg*) [[prevViewModelVC model] root];
                
//                Floor* currentFloor=(Floor*) [[currentSpace linkedRel] relating] ;
//                Bldg* currentBldg=(Bldg*) [[currentFloor linkedRel] relating] ;
//                
                [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"bldgID\"\r\n\r\n%d", currentBldg.ID ] dataUsingEncoding:NSUTF8StringEncoding]];    
                [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
                [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"spaceID\"\r\n\r\n%d",currentSpace.ID] dataUsingEncoding:NSUTF8StringEncoding]];   
            }
            break;
    }
    
    
//    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];    
//	[myData appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"uploadFile\"; filename=\"OPS\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];    
//	[myData appendData:[[NSString stringWithString:@"Content-Type: application/x-sqlite3\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"emailSubject\"\r\n\r\n%@", subjectTextField.text] dataUsingEncoding:NSUTF8StringEncoding]];    
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"emailContent\"\r\n\r\n%@", contentTextView.text] dataUsingEncoding:NSUTF8StringEncoding]];    
    
    NSMutableArray* aSentRecipient=[NSMutableArray array];
    
    
    uint pGroup=0;
    for (BIMMailGroup* recipientGroup in self.aRecipientGroup){
        uint pSentRecipient=0;
        for (BIMMailRecipient* recipient in recipientGroup.aBIMMailRecipient){            
            if (recipient.checked){
                bool bSent=false;
                for (BIMMailRecipient* sentRecipient in aSentRecipient){
                    if (sentRecipient.ID==recipient.ID){
                        bSent=true;
                        break;
                    }
                }
                if (!bSent){
                    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
                    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"recipient%04d\"\r\n\r\n%d",pSentRecipient, recipient.ID] dataUsingEncoding:NSUTF8StringEncoding]];                  
//                    NSLog(@"recipient name:%@",recipient.name);
                    pSentRecipient++;
                    [aSentRecipient addObject:recipient];
                }
            }
        }
        pGroup++;
    }
    
    
    
    
    
    
    
    
    uint pAttachedFile=0;
//    NSFileManager* fileManager=[NSFileManager defaultManager];
    if (aImg){
        for (BIMMailImageView* imgView in aImg){ 
            NSString* slImagePath=imgView.path;
            NSString* imageFolder=[slImagePath stringByDeletingLastPathComponent];
            NSString* attachedFileName=[[slImagePath lastPathComponent] substringFromIndex:3];
            NSString* imagePath=[imageFolder stringByAppendingPathComponent:attachedFileName];
            NSLog (@"upload imagepath: %@",imagePath);
            NSData *imageData = [[NSData alloc] initWithContentsOfFile:imagePath];
            
            //Assuming data is not nil we add this to the multipart form
            if (imageData) {
                
                [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];        
                //        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];                
                
                
                [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"AttachedFileName%04d\"; filename=\"%@\"\r\n",pAttachedFile,attachedFileName] dataUsingEncoding:NSUTF8StringEncoding]];    
//                [myData appendData:[[NSString stringWithString:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                
                [myData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [myData appendData:imageData];
                [myData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            }            
            pAttachedFile++;
            [imageData release];
            

//            [fileManager removeItemAtPath: imgView.path error:NULL];
        }
    }
    
    
    
    
    
    
    [myData appendData:[closingBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/postTestAction2.php"];
    //    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=%d&siteID=%d&filename=OPS",[appDele activeLiveStudio].ID, [selectedProjectSite ID]];
    NSURL *url = [NSURL URLWithString: urlString];
    //    UIImage *image = [[UIImage imageWithData: [NSData dataWithContentsOfURL: url]] retain];
    
    
    
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:900.0];
    
    
    [theRequest setHTTPMethod:@"POST"];
    
    
    //    
    //    NSString *post = @"key1=val1&key2=val2";
    //    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];    
    //    NSString *postLength = [NSString stringWithFormat:@"%d", [myData length]]; 
    
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [myData length]];  
    [theRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];  
    //    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", myBoundary];    
    //    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=------%@\r\n", aBoundary];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", aBoundary];
    //    [theRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];        
    
    [theRequest setValue:contentType forHTTPHeaderField:@"Content-Type"];        

    
    [theRequest setHTTPBody:myData];
        // create the connection with the request
    // and start loading the data
    // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file    
    //    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];    
    self.theConnection=aConnection;
    [aConnection release];
    if (theConnection) {
        // Create the NSMutableData that will hold
        // the received data
        // receivedData is declared as a method instance elsewhere
        if (connectionData){
            [connectionData release];
        }
        connectionData=[[NSMutableData data] retain];
    } else {    
        // Inform the user that the connection failed.        
		UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[connectFailMessage show];
		[connectFailMessage release];
        
    }   
    return  myData;
}






- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!self.aRecipientGroup||[self.aRecipientGroup count]<=0){return 0;}
    return [self.aRecipientGroup count];
//    return 1;
}



// Customize the number of rows in the table view.

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    BIMMailGroup* mailGroup=[self.aRecipientGroup objectAtIndex:section];
    int numSectionRow=[mailGroup.aBIMMailRecipient count]+1;
    return numSectionRow;
//    return 5;

}



// Customize the appearance of table view cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
//    }
//    // Set up the cell...
//    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
//
//    
    
    
    
    
    
//    UIViewController*vc=[[UIViewController alloc]initWithNibName:@"BIMMailRecipient" bundle:nil];
 
    RecipientCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BIMMailRecipientCell"];
    
    if (cell == nil){
//        NSLog(@”New Cell Made”);
//        NSString * cellId = @"reuseCell";
//        cell=[[[vc view] retain] autorelease];
        
//        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"BIMMailRecipientCell" owner:nil options:nil];
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RecipientCell" owner:self options:nil];        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (RecipientCell *)currentObject;
                cell.delegate=self;
//                [cell setValue:cellId forKey:@"reuseIdentifier"];
                break;
            }
        }
    }
//    [vc release];
    
    BIMMailGroup* mailGroup=[self.aRecipientGroup objectAtIndex:[indexPath section]];
    uint cellRowInSection=[indexPath row];
    if (cellRowInSection >0){
        
        BIMMailRecipient* mailRecipient=[[mailGroup aBIMMailRecipient] objectAtIndex:cellRowInSection-1];
        cell.nameLabel.text=mailRecipient.name;
        cell.roleLabel.text=mailRecipient.role;
    }else{
        cell.nameLabel.text=@"All recipient in this group";
        cell.roleLabel.text=@"";
    }
    
    
    
    
    uint groupID=[indexPath section];
    uint rowID=[indexPath row];
    
    
    BIMMailGroup* recipientGroup=[self.aRecipientGroup objectAtIndex:groupID];
    NSArray* aRecipientList=recipientGroup.aBIMMailRecipient;
    
    if (rowID==0) {
        //        recipientGroup.checked=!recipientGroup.checked;
        //        for (BIMMailRecipient* groupRecipient in aRecipientList){
        //            groupRecipient.checked=recipientGroup.checked;
        //        }
        
        UIImage *checkImage = (recipientGroup.checked) ? [UIImage imageNamed:@"checked.png"] : [UIImage imageNamed:@"unchecked.png"];
        [cell.checkButton setImage:checkImage forState:UIControlStateNormal];            
    }else{    
        BIMMailRecipient* recipient=[aRecipientList objectAtIndex:(rowID-1) ];
        UIImage *checkImage = (recipient.checked) ? [UIImage imageNamed:@"checked.png"] : [UIImage imageNamed:@"unchecked.png"];
        [cell.checkButton setImage:checkImage forState:UIControlStateNormal];         
    }
    
//	UIImage *checkImage = (cell.checked) ? [UIImage imageNamed:@"checked.png"] : [UIImage imageNamed:@"unchecked.png"];
//	[cell.checkButton setImage:checkImage forState:UIControlStateNormal];

    /*
     
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];     
    if (cell == nil) {
        
        cell=[[[BIMMailRecipientCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        
//        cell = bimMailRecipientCell;//[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
//        mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(80.0, 0.0, 979, 45.0)] ;
//        mainLabel.tag = MAINLABEL_TAG;
//        mainLabel.font = [UIFont systemFontOfSize:30.0];
//        //        mainLabel.textAlignment = UITextAlignmentRight;
//        mainLabel.textAlignment = UITextAlignmentLeft;        
//        mainLabel.textColor = [UIColor blackColor];
//        mainLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
//        cell.selectionStyle=UITableViewCellSelectionStyleNone;
//        [cell.contentView addSubview:mainLabel];
//        [mainLabel release];
//        
//        
//        photo = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 45.0, 45.0)] ;
//        photo.tag = PHOTO_TAG;
//        photo.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
//        [cell.contentView addSubview:photo];
        [photo release];
        
    } else {
//        mainLabel = (UILabel *)[cell.contentView viewWithTag:MAINLABEL_TAG];
//        photo = (UIImageView *)[cell.contentView viewWithTag:PHOTO_TAG];
    }

    
    */
//    BIMMailRecipientCell* recipientCell=(BIMMailRecipientCell*) cell;
    


//    cell.textLabel.text = mailRecipient.name;// [NSString  stringWithFormat:@"Cell Row #%d", [indexPath row]];    
    return cell;
}






//
//- (void)layoutSubviews
//{
//	[super layoutSubviews];
//	
//    CGRect contentRect = [self.contentView bounds];
//	
//	CGRect frame = CGRectMake(contentRect.origin.x + 40.0, 8.0, contentRect.size.width, 30.0);
//	self.textLabel.frame = frame;
//	
//	// layout the check button image
//	UIImage *checkedImage = [UIImage imageNamed:@"checked.png"];
//	frame = CGRectMake(contentRect.origin.x + 10.0, 12.0, checkedImage.size.width, checkedImage.size.height);
//	checkButton.frame = frame;
//	
//	UIImage *image = (self.checked) ? checkedImage: [UIImage imageNamed:@"unchecked.png"];
//	UIImage *newImage = [image stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
//	[checkButton setBackgroundImage:newImage forState:UIControlStateNormal];
//}


// called when the checkmark button is touched 
//- (void)checkAction:(id)sender
//{
//	// note: we don't use 'sender' because this action method can be called separate from the button (i.e. from table selection)
//	self.checked = !self.checked;
//	UIImage *checkImage = (self.checked) ? [UIImage imageNamed:@"checked.png"] : [UIImage imageNamed:@"unchecked.png"];
//	[checkButton setImage:checkImage forState:UIControlStateNormal];
//}









/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    // open a alert with an OK and cancel button

    NSString *alertString = [NSString stringWithFormat:@"Clicked on row #%d", [indexPath row]];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];

    [alert show];

    [alert release];

}

*/


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil viewModelVC:(ViewModelVC*) _viewModelVC
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.viewModelVC=_viewModelVC;
        
        
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        
        NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:2];                        
//        self.attachmentProductType=_attachmentProductType;
//        [_attachmentProductType release];
        
        self.title=[NSString stringWithFormat: @"BIMMail of Site:%@",appDele.activeProjectSite.name];
//        
        UIBarButtonItem* bi = [[UIBarButtonItem alloc]
                               initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(  actionCancel: )];
        bi.style = UIBarButtonItemStyleBordered;
        [buttons addObject:bi];
        //        self.navigationItem.leftBarButtonItem=bi;
        [bi release];
        
        
        
        
        
        
        
        
        
        
     
        
        self.navigationItem.leftBarButtonItems=buttons;  
        
        [buttons release];
        
        

        
        // Custom initialization
        [self readRecipientsFromServer];
        
        
        
        
        
        
//        UIView *myView = [UIView alloc];
        
//        UITableViewController *tableViewController = [[UITableViewController alloc] init];
//        [[self view] addSubview:[tableViewController view]];

//        [tableViewController release];
//        [myView addSubview:[tableViewController view]];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    int height = 44;//self.frame.size.height;
    int width = 800;
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor blackColor];
    navLabel.font = [UIFont boldSystemFontOfSize:16];
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text=self.title;
    self.navigationItem.titleView = navLabel;
    [navLabel release]; 
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
/*

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //    470*470
    // You have the image. You can use this to present the image in the next view like you require in `#3`.
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [imgLibraryPopOverController dismissPopoverAnimated:YES];
    
    UIImage * pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UIImage* image=[ProjectViewAppDelegate correctImageRotation:pickedImage];
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
    
    [self dealWithImageChosen:image];
    
}
*/

-(void) dealWithImageChosen:(UIImage*) image{
    
    
//    
//    
//}
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    //    470*470
    
    
    
    
    if (!aImg){
        NSMutableArray* _aImg=[[NSMutableArray alloc] init ];
        self.aImg=_aImg;
        [_aImg release];
    }
    [self rebuildImgScrollView];
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;    

    
    NSString* localSiteParentFolderPath=[projectSite.dbPath stringByDeletingLastPathComponent];  
    
    
    //    NSString* localImgName=[NSString stringWithFormat: @"studio%d_site%d_type%@_id%d_%d",activeStudioID,[projectSite ID],self.attachmentProductType,[attachedProduct ID],[aImg count]];        
    //    NSString* localImgName=[self generateImgFileNameByIndex:[aImg count]];
    NSString* localImgName=[self generateImgFileNameByDate];
    NSString *fullPath = [localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", localImgName]]; //add our image to the path
    
    NSLog (@"Image Size: %f x %f",image.size.width,image.size.height);
    
    
    uint maxDimen=1455;
    double imageHeightToWidthRatio=image.size.height/image.size.width;
    
    if (image.size.height>maxDimen || image.size.width>maxDimen){
        CGSize optSize=CGSizeMake( imageHeightToWidthRatio>1?maxDimen/imageHeightToWidthRatio:maxDimen,  imageHeightToWidthRatio>1?maxDimen:maxDimen*imageHeightToWidthRatio);
        NSLog (@"Saving to Smaller Size");
        if ([UIImageJPEGRepresentation([ProjectViewAppDelegate resizeImage:image width:optSize.width height:optSize.height],0.7) writeToFile:fullPath atomically:YES]) {
            //        NSLog(@"save ok");
        }
        else {
            //        NSLog(@"save failed");
        }
    }else{
        //Save the image directly with no compression and no resize
        if ([UIImageJPEGRepresentation(image, 1) writeToFile:fullPath atomically:YES]) {
            //        NSLog(@"save ok");
        }
        else {
            //        NSLog(@"save failed");
        }
    }
    
    
    
    NSString* slImagePath=[localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"sl_%@", localImgName]];
    uint slideWidth=470;
    uint slideHeight=470;
    //    double imageHeightToWidthRatio=image.size.height/image.size.width;
    UIImage* slImage=nil;    
//    if (image.size.height>slideHeight || image.size.width>slideWidth){
        CGSize optSize=CGSizeMake( imageHeightToWidthRatio>1?slideHeight/imageHeightToWidthRatio:slideWidth,  imageHeightToWidthRatio>1?slideHeight:slideWidth*imageHeightToWidthRatio);
        NSLog (@"Saving to Smaller Size");
        slImage=[ProjectViewAppDelegate resizeImage:image width:optSize.width height:optSize.height];
        if ([UIImageJPEGRepresentation(slImage,0.7) writeToFile:slImagePath atomically:YES]) {
            //        NSLog(@"save ok");
        }
        else {
            //        NSLog(@"save failed");
        }
//    }else{
//        //Save the image directly with no compression and no resize
//        if ([UIImageJPEGRepresentation(image, 0.7) writeToFile:slImagePath atomically:YES]) {
//            //        NSLog(@"save ok");
//        }
//        else {
//            //        NSLog(@"save failed");
//        }
//    }
//    
    
    
    
    BIMMailImageView* bimMailImageView=[[BIMMailImageView alloc] initWithImage:slImage path:slImagePath ];// attachFileID:0];
    //    [aImg addObject:attachImgView];
    [aImg addObject:bimMailImageView];
    if (aInsertImg==nil){
        NSMutableArray* _aInsertImg=[[NSMutableArray alloc] init];
        self.aInsertImg=_aInsertImg;
        [_aInsertImg release];
    }
    [aInsertImg addObject:bimMailImageView];
    [bimMailImageView release];
    
    
    [self setupScrollView];     
    int numImg=[aImg count];
    
    
	[imgScrollView scrollRectToVisible:CGRectMake(numImg*imgScrollView.frame.size.width,0,imgScrollView.frame.size.width,imgScrollView.frame.size.height) animated:NO];        
    pImg=numImg-1;
    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",numImg,numImg];
    
    
    
    
}





- (void)addImage:(BIMMailImageView*) imageView atPosition:(int)position{
	// add image to scroll view
    CGFloat imgRatio=imageView.image.size.height/imageView.image.size.width;
    CGSize imageSize;
    CGRect imgFrameRect=imgScrollView.frame;
    if (imgRatio<1.0){
        imageSize=CGSizeMake(imgFrameRect.size.width, imgFrameRect.size.width*imgRatio);
    }else{
        imageSize=CGSizeMake(imgFrameRect.size.height/imgRatio, imgFrameRect.size.height);
    }
    
    
    
    //	UIImage *image = [UIImage imageNamed:imageString];
    
    if (position==0 ){
        BIMMailImageView *_preLoadImgView = [[BIMMailImageView alloc] initWithImage:imageView.image];
        self.preLoadImgView=_preLoadImgView;
        [_preLoadImgView release];
        preLoadImgView.frame = CGRectMake(position*imgFrameRect.size.width+(imgFrameRect.size.width-imageSize.width)/2, (imgFrameRect.size.height -imageSize.height)/2,imageSize.width, imageSize.height);
        [imgScrollView addSubview:preLoadImgView];
    }else if (position==([aImg count]+1)){
        
        BIMMailImageView *_postLoadImgView = [[BIMMailImageView alloc] initWithImage:imageView.image];
        self.postLoadImgView=_postLoadImgView;
        [_postLoadImgView release];
        postLoadImgView.frame = CGRectMake(position*imgFrameRect.size.width+(imgFrameRect.size.width-imageSize.width)/2, (imgFrameRect.size.height -imageSize.height)/2, imageSize.width, imageSize.height);
        [imgScrollView addSubview:postLoadImgView];
    }else{
        //	AttachmentImageView *imageView = [[AttachmentImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(position*imgFrameRect.size.width+(imgFrameRect.size.width-imageSize.width)/2, (imgFrameRect.size.height -imageSize.height)/2, imageSize.width, imageSize.height);
        [imgScrollView addSubview:imageView];
    }
    //	[imageView release];
}



- (void)addImage0:(BIMMailImageView*) imageView atPosition:(int)position{
	// add image to scroll view
    CGRect imgRect=imgScrollView.frame;
    
    //	UIImage *image = [UIImage imageNamed:imageString];
    
    if (position==0 ){
        
        
        BIMMailImageView *_preLoadImgView = [[BIMMailImageView alloc] initWithImage:imageView.image];                
        self.preLoadImgView=_preLoadImgView;                                
        [_preLoadImgView release];
        
        preLoadImgView.frame = CGRectMake(position*imgRect.size.width, 0, imgRect.size.width, imgRect.size.height);    
        [imgScrollView addSubview:preLoadImgView];        
    }else if (position==([aImg count]+1)){
        
        BIMMailImageView *_postLoadImgView = [[BIMMailImageView alloc] initWithImage:imageView.image];
        self.postLoadImgView=_postLoadImgView;
        [_postLoadImgView release];
        
        postLoadImgView.frame = CGRectMake(position*imgRect.size.width, 0, imgRect.size.width, imgRect.size.height);    
        [imgScrollView addSubview:postLoadImgView];        
    }else{
        //	AttachmentImageView *imageView = [[AttachmentImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(position*imgRect.size.width, 0, imgRect.size.width, imgRect.size.height);    
        [imgScrollView addSubview:imageView];
    }
    //	[imageView release];
}


-(void) setupScrollView{   
    int numImg=[aImg count];    
    //    UIImage* lastImg=((AttachmentImageView*) [aImg lastObject] ).image;
    //    UIImage* firstImg=((AttachmentImageView*)[aImg objectAtIndex:0]).image;
    BIMMailImageView* lastImgView=[aImg lastObject];
    BIMMailImageView* firstImgView=[aImg objectAtIndex:0];
    //    NSLog(@"subview count: %d",[[imgScrollView subviews] count]);
	// add the last image (image4) into the first position
	[self addImage:lastImgView atPosition:0];
    
    
    
	// add all of the images to the scroll view
	for (int i = 0; i < numImg; i++) {
        //		[self addImage:((AttachmentImageView*) [aImg objectAtIndex:i]).image atPosition:(i+1)];
        
		[self addImage:((BIMMailImageView*) [aImg objectAtIndex:i]) atPosition:(i+1)];        
	}
	
    
	// add the first image (image1) into the last position
	[self addImage:firstImgView atPosition:(numImg+1)];        
	imgScrollView.contentSize = CGSizeMake((numImg+2)*imgScrollView.frame.size.width, imgScrollView.frame.size.height);  
}

-(IBAction)addImageFromLibrary:(id)sender{         
    
//    if ([self.aImg count]>=3){
//        UIAlertView *alert = [[UIAlertView alloc] 
//                              initWithTitle: @"You can only attach 3 photos in one BIMMail."
//                              message: @""
//                              delegate: self
//                              cancelButtonTitle: @"OK"
//                              otherButtonTitles: nil];
//        alert.tag = BIMMail_MaximumImageReached; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//        [alert show];
//        [alert release];
//        
//        return;        
//    }
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    //    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    //    [self.view addSubview:imagePicker.view];
    UIPopoverController* aPopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
    self.imgLibraryPopOverController=aPopover;
    //    CGRect imgLibFrame=buttonImgLibrary.frame;
    //    CGRect pickerFrame = CGRectMake(imgLibFrame.origin.x, imgLibFrame.origin.y, 400, 600);
    //    [aPopover setPopoverContentSize:pickerFrame.size animated:NO];  
    
    
    [self.imgLibraryPopOverController presentPopoverFromRect:[buttonImgLibrary bounds]//pickerFrame//CGRectMake(0.0, 0.0, 800.0, 800.0) 
                                                      inView:buttonImgLibrary
                                    permittedArrowDirections:UIPopoverArrowDirectionAny 
                                                    animated:YES];
    
    //    [self.imgLibraryPopOverController presentPopoverFromBarButtonItem:buttonImgLibrary
    //                                   permittedArrowDirections:UIPopoverArrowDirectionUp
    //                                                   animated:YES];  
    [aPopover release];
    //    [self presentModalViewController:imagePicker animated:YES];
    [imagePicker release];
}



-(IBAction)addImageFromCamera:(id)sender{
    
    
//    if ([self.aImg count]>=3){
//        UIAlertView *alert = [[UIAlertView alloc]
//                              initWithTitle: @"You can only attach 3 photos in one BIMMail."
//                              message: @""
//                              delegate: self
//                              cancelButtonTitle: @"OK"
//                              otherButtonTitles: nil];
//        alert.tag = BIMMail_MaximumImageReached; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//        [alert show];
//        [alert release];
//
//        return;        
//    }

    
    
    AROverlayViewController* arOverlayViewController=[[AROverlayViewController alloc] init ];
    arOverlayViewController.toolbar=nil;
    arOverlayViewController.prevVC=self;
    self.arOverlayViewController=arOverlayViewController;
    [arOverlayViewController release];


    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [[appDele viewModelNavCntr] pushViewController:self.arOverlayViewController animated:NO];
    [appDele viewModelNavCntr].navigationBar.translucent=YES;
    [appDele viewModelNavCntr].navigationBar.alpha=0.7;
}
-(IBAction)addImageFromCamera0:(id)sender{
    
//    if ([self.aImg count]>9){
//        UIAlertView *alert = [[UIAlertView alloc] 
//                              initWithTitle: @"You can only attach 10 photos in one attachment. Please start a new attachment to add more."
//                              message: @""
//                              delegate: self
//                              cancelButtonTitle: @"OK"
//                              otherButtonTitles: nil];
//        alert.tag = BIMMail_MaximumImageReached; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//        [alert show];
//        [alert release];
//        
//        return;        
//    }
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    //    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
//    [self presentModalViewController:imagePicker animated:YES];
    [self presentViewController:imagePicker animated:YES completion:nil];
    [imagePicker release];
}


-(void) scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    //    imgScrollOffX=scrollView.contentOffset.x;    
}
//
//-(void) scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
//    imgScrollOffX=scrollView.contentOffset.x;
//}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {    
    
    //    bool bScrollRight=true;
    //    if (    imgScrollView.contentOffset.x> imgScrollOffX){
    //        bScrollRight=false;
    //        NSLog(@"Scroll Left, now:%f prev:%f",imgScrollView.contentOffset.x,imgScrollOffX);
    //    }else{
    //        NSLog(@"Scroll Right now:%f prev:%f",imgScrollView.contentOffset.x,imgScrollOffX);
    //    }
    //    
    
    //	NSLog(@"%f",scrollView.contentOffset.x);
	// The key is repositioning without animation      
    int numImg=[aImg count];
    CGRect scrollViewFrame=imgScrollView.frame;
    
    
    
    
    float contentOffX=imgScrollView.contentOffset.x;
    uint currentImgIndex = round(contentOffX / scrollViewFrame.size.width);        
    
    //    uint numImg=aImg?[aImg count]:0;
    
    if (currentImgIndex>numImg){
        currentImgIndex=1;
    }
    if (currentImgIndex<1){
        currentImgIndex=numImg;
    }    
    //    currentImgIndex++;
    
    
    [imgScrollView scrollRectToVisible:CGRectMake(scrollViewFrame.size.width*currentImgIndex,0,scrollViewFrame.size.width,scrollViewFrame.size.height) animated:NO];  
    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",currentImgIndex,numImg];
    pImg=currentImgIndex-1;
    
}
- (void)removeImage:(id) sender{
    
    //    [picker dismissModalViewControllerAnimated:YES];
    //    [imgLibraryPopOverController dismissPopoverAnimated:YES];
    if (!aImg){
        return;
    }   
    int currentIndex=pImg;
    
    int numImg=[aImg count];
    if (numImg<=0){
        return;
    }
    BIMMailImageView* imgView=[aImg objectAtIndex:pImg];
    
    if (pImg==0){
        [preLoadImgView removeFromSuperview];
    }
    if (pImg==numImg-1){
        [postLoadImgView removeFromSuperview];
    }
    [imgView removeFromSuperview];
    
    [self toDeleteImgArray:imgView];
 
    
    [aImg removeObjectAtIndex:pImg];
    numImg=[aImg count];
    
    
    
    [self rebuildImgScrollView];
    if (numImg==0){
        pImg=0;
        
        labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",0,0];
        return;
    }
    
    
    [self setupScrollView];
    
    
    if (currentIndex==numImg){
        pImg=0;
    }
	[imgScrollView scrollRectToVisible:CGRectMake((pImg+1)*imgScrollView.frame.size.width,0,imgScrollView.frame.size.width,imgScrollView.frame.size.height) animated:NO];        
    //    pImg=numImg;
    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",( pImg+1),numImg];
    
    
    
    
}
-(void)rebuildImgScrollView{
    CGRect scrollViewFrame=imgScrollView.frame;
    UIColor* scrollBGColor=imgScrollView.backgroundColor;
    if (imgScrollView){
        [imgScrollView removeFromSuperview];
        [imgScrollView release]; imgScrollView=nil;
        UIScrollView* _imgScrollView=[[UIScrollView alloc] initWithFrame:scrollViewFrame];
        _imgScrollView.delegate=self;
        self.imgScrollView=_imgScrollView;
        imgScrollView.backgroundColor=scrollBGColor;
        
        [_imgScrollView release];
        [self.view addSubview:imgScrollView];
    }    
}

- (NSString*) generateImgFileNameByDate{
    
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    OPSProjectSite* projectSite=appDele.activeProjectSite;    
//    //    uint activeStudioID=appDele.activeStudioID;
//    
//    //
//    //    NSString* localSiteParentFolderPath=[projectSite.dbPath stringByDeletingLastPathComponent];  
//    //    
//    //    
//    
//    
//    NSDateFormatter *formatter;      
//    formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"YYYY-D-HH-m-SS-SSS"];
//    //    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];  
//    
//    NSString* _dateStr=[formatter stringFromDate:[NSDate date]];
//    
//    
//    
//    //    NSString* localImgName=[NSString stringWithFormat: @"^user%@^studio%d^site%d^type%@^id%d^%@.png",[appDele defUserName], activeStudioID,[projectSite ID],self.attachmentProductType,[attachedProduct ID],_dateStr];
//    NSString* localImgName=[NSString stringWithFormat: @"BIMMail^%d^%@^%@.jpg",[projectSite ID],[appDele defUserName],_dateStr];
//    
//    [formatter release];
//    return  localImgName;
//    
    
    
    
    
    
    
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;  
    NSTimeInterval ti=[[NSDate date] timeIntervalSince1970];
    NSString* _dateStr=[NSString stringWithFormat:@"%f", ti];                
    NSString* localImgName=[NSString stringWithFormat: @"BIMMail^%d^%@^%@.jpg",[projectSite ID],[appDele defUserName],_dateStr];
    return  localImgName;
}


- (void) toDeleteImgArray:(BIMMailImageView*) imgView{
    if (aDeleteImg==nil){
        NSMutableArray* _aDeleteImg=[[NSMutableArray alloc] init];
        self.aDeleteImg=_aDeleteImg;
        [_aDeleteImg release];
    }
    //    if (aInsertImg){
    //
    //        uint pInd=0;
    //        for (AttachmentImageView* insertView in aInsertImg){
    //            if (insertView==imgView){
    //                [aInsertImg removeObjectAtIndex:pInd];
    //                pInd++;
    //                break;
    //            }
    //        }
    //    }
    [aDeleteImg addObject:imgView];
}

-(void) executeDeleteImgViewFromStorageAndDB{    
    if (aDeleteImg==nil||[aDeleteImg count]<=0) {return;}
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
//    OPSProjectSite* projectSite=appDele.activeProjectSite;       
//    sqlite3 * database=nil;
//    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {          
        for (BIMMailImageView* imgView in aDeleteImg){

            
            
            NSString* slImagePath=imgView.path;
            [fileManager removeItemAtPath: slImagePath error:NULL];
            
            
            NSString* fullImgDirectory=[slImagePath stringByDeletingLastPathComponent];
            NSString* fullImgPathLastComponent=[[slImagePath lastPathComponent] substringFromIndex:3];
            NSString* fullImagePath=[[NSString alloc] initWithFormat:@"%@/%@",fullImgDirectory,fullImgPathLastComponent];
            [fileManager removeItemAtPath: fullImagePath error:NULL];
            [fullImagePath release];
            
            
//            if (imgView.attachFileID!=0){                                            
//                sqlite3_stmt *deleteAttachFileStmt;
//                const char* deleteAttachFileSql=[[NSString stringWithFormat:@"delete from AttachFile where ID=%d",
//                                                  imgView.attachFileID] UTF8String];  
//                
//                //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
//                if(sqlite3_prepare_v2 (database, deleteAttachFileSql, -1, &deleteAttachFileStmt, NULL) == SQLITE_OK) {
//                    while(sqlite3_step(deleteAttachFileStmt) == SQLITE_ROW) {     
//                        
//                    }
//                }
//                
//            }
        }
        
//        
//    }
//    sqlite3_close(database);
}


-(void) removeInsertImgArray{
    if (aInsertImg==nil||[aInsertImg count]<=0) {return;}
    NSFileManager *fileManager = [NSFileManager defaultManager];
    for (BIMMailImageView* imgView in aInsertImg){
        NSString* slImagePath=imgView.path;
        [fileManager removeItemAtPath: slImagePath error:NULL];
        
        
        NSString* fullImgDirectory=[slImagePath stringByDeletingLastPathComponent];
        NSString* fullImgPathLastComponent=[[slImagePath lastPathComponent] substringFromIndex:3];
        NSString* fullImagePath=[[NSString alloc] initWithFormat:@"%@/%@",fullImgDirectory,fullImgPathLastComponent];
        [fileManager removeItemAtPath: fullImagePath error:NULL];
        [fullImagePath release];
    }
}


- (void) actionCancel:(id) sender{
    [self.navigationController popViewControllerAnimated:YES];
    [self removeInsertImgArray];
    
}
/*
- (void) actionOK:(id) sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    if (self.attachmentInfo){
        [self updateRecord];
    }else{
        [self insertNewRecord];
    }
    [self executeDeleteImgViewFromStorageAndDB];
    
}

// Called when an alertview button is clicked
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case ATTACHMENT_CONFIRM_DELETE:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	
                    NSLog(@"Delete was cancelled by the user");
                }
                    break;
                case 1: // delete
                {
                    [self trashAttachmentEntry];
                    // do the delete
                }
                    break;
            }
            
            break;
        default:
            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");
        }
    }	
}
 
-(void) trashAttachmentEntry{//:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];    
    [self removeInsertImgArray];
    
    if (!attachmentInfo){
        return;
    }
    
    //    uint attachCommentID=attachmentInfo.attachCommentID;
    
    
    
    
    
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];    
    
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
    OPSProjectSite* projectSite=appDele.activeProjectSite;  
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  
        
        
        if (aImg){
            for (AttachmentImageView* imgView in aImg){            
                [fileManager removeItemAtPath: imgView.path error:NULL];
                if (imgView.attachFileID!=0){                                            
                    sqlite3_stmt *deleteAttachFileStmt;
                    const char* deleteAttachFileSql=[[NSString stringWithFormat:@"delete from AttachFile where ID=%d",
                                                      imgView.attachFileID] UTF8String];                      
                    //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
                    if(sqlite3_prepare_v2 (database, deleteAttachFileSql, -1, &deleteAttachFileStmt, NULL) == SQLITE_OK) {
                        while(sqlite3_step(deleteAttachFileStmt) == SQLITE_ROW) {     
                            
                        }
                    }
                    
                }
                
                
            }
        }
        
        if (attachmentInfo){
            
            sqlite3_stmt *deleteAttachCommentStmt;
            const char* deleteAttachCommentSql=[[NSString stringWithFormat:@"delete from AttachComment where id=%d",
                                                 attachmentInfo.attachCommentID] UTF8String];                      
            //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
            if(sqlite3_prepare_v2 (database, deleteAttachCommentSql, -1, &deleteAttachCommentStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(deleteAttachCommentStmt) == SQLITE_ROW) {     
                    
                }
            }
            
            
            
            
            
        }        
        
        
        
    }            
    sqlite3_close(database);
    
    
    
    
    
}

-(void) alertTrashAttachmentEntry:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle: @"Trash This Attachment Entry"
                          message: @""
                          delegate: self
                          cancelButtonTitle: @"Cancel"
                          otherButtonTitles: @"Delete", nil];
    alert.tag = ATTACHMENT_CONFIRM_DELETE; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
    [alert show];
    [alert release];
    
}
*/

- (BOOL)shouldAutorotate {
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}
@end

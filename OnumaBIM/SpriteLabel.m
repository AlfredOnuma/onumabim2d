//
//  SpriteLabel.m
//  ProjectView
//
//  Created by Alfred Man on 2/15/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "SpriteLabel.h"

#import "ViewModelVC.h"
#import "OPSModel.h"
#import "ExtrudedAreaSolid.h"
#import "CurveProfile.h"
#import "RepresentationItem.h"
#import "Slab.h"
#import "Site.h"
#import "Furn.h"
@implementation SpriteLabel

@synthesize displayText;


- (id)initWithTransform:(CGAffineTransform) transform viewProductRep:(ViewProductRep*)viewProductRep representationItem:(RepresentationItem*) representationItem displayModelUI:( DisplayModelUI*) displayModelUI{
    //    self=[super initWithTransform:_transform viewproductRep:_viewProductRep representationItem:_representationItem displayModelUI:displayModelUI];
    self=[super initWithTransform:transform viewProductRep:viewProductRep representationItem:representationItem displayModelUI:displayModelUI];
    
    if (self){
//        ViewModelVC* viewModelVC=[self parentViewModelVC];// [appDele activeViewModelVC];
//        double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];                
                
        OPSProduct* parentSpatialStruct=[[self.viewProductRep product] findParentSpatialStruct];
        NSString* tmpDisplayText=[[NSString alloc] initWithString:parentSpatialStruct.name ];
        self.displayText=tmpDisplayText;
        [tmpDisplayText release];
        
        /*
        
        ViewModelVC* viewModelVC=[self parentViewModelVC];
        //    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];   
        //    DisplayModelUI* displayModelUI=self.delegate;
        float fontSize=[viewModelVC currentProductLabelSize]/[viewModelVC navUIScrollVC].zoomScale;    
        CGSize productLabelConstraint = CGSizeMake(fontSize*50, fontSize*2);    
        //    NSString* displayText=[self.displayInfo displayLabelText];
        //    CGSize productLabelSize = [displayText sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] 
        //                                                   constrainedToSize:productLabelConstraint 
        //                                                       lineBreakMode:UILineBreakModeCharacterWrap];   
        CGSize productLabelSize = [displayText sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] 
                                          constrainedToSize:productLabelConstraint 
                                              lineBreakMode:UILineBreakModeCharacterWrap]; 
        
        
        
        
        
        
        
        
        float fontSize=[viewModelVC currentProductLabelSize]/[viewModelVC navUIScrollVC].zoomScale;    
        CGSize productLabelConstraint = CGSizeMake(fontSize*50, fontSize*2);    
        CGSize productLabelSize = [[displayInfo displayLabelText] sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] 
                                                             constrainedToSize:productLabelConstraint 
                                                                 lineBreakMode:UILineBreakModeCharacterWrap];   
        
        
        
        //        CGPoint origin=CGPointMake(0, 0);    
        //        origin=CGPointApplyAffineTransform(origin, rootSpatialStructTransform);    
        //        CGPoint labelPos= [self convertPoint:origin toView:productViewLabelLayer];  
        //        double area=0.0;
        //            double area=[(RepresentationItem*) [[[rootSpatialStruct representation] aRepresentationItem] objectAtIndex:0] area];
        //        CGPoint labelPos=CGPointMake(0, 0);
        //        ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(labelPos.x-productLabelSize.width/2, labelPos.y-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) name:[rootSpatialStruct name] area:area displayModelUI:self] ;    
        
        
        
        
        
        //            ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) name:[rootSpatialStruct name] area:area displayModelUI:self] ;
        
        
        //            ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) displayInfo:displayInfo displayModelUI:self];
        
        //        ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) displayInfo:displayInfo displayModelUI:self productTransform:rootSpatialStructTransform];
        //        ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) displayInfo:displayInfo displayModelUI:self];
        
        ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) displayInfo:displayInfo displayModelUI:self];        
        
        CGPoint origin= CGPointMake(displayInfo.displacement.x*modelScaleFactor, displayInfo.displacement.y*modelScaleFactor);    
        CGPoint labelPos= CGPointApplyAffineTransform(origin, rootSpatialStructTransform);       
        CGPoint labelLayerPos=[spatialStructViewLayer convertPoint:labelPos toView:productViewLabelLayer]; 
        //            [_viewProductLabel setText:[rootSpatialStruct name]];
        //        [_viewProductLabel setTransform:rootTransform];
        [_viewProductLabel setTransform:CGAffineTransformMakeTranslation(labelLayerPos.x, labelLayerPos.y)];        
        //    self.viewProductLabel=_viewProductLabel;
        //    [_viewProductLabel release];
        [productViewLabelLayer addSubview:_viewProductLabel];
        //        [productViewLabelLayer addSubview:_viewProductLabel];
        [_viewProductLabel release];
        */
    }
    
    //
    //    }
    return self;
    
}


- (void) drawBody: (CGContextRef) context
{
    
//    ViewModelVC* modelVC=[self pa rentViewModelVC];   
//    ViewProductRep* selectedProductRep=modelVC.selectedProductRep;    
//    bool selected=(selectedProductRep==self.viewProductRep);
    NSLog(@"label: %@",self.viewProductLabel.text);
    
    UIFont* font = [UIFont fontWithName:@"Arial" size:50];
    
    [displayText drawAtPoint:CGPointMake(0, 0) withFont:font];
//    [displayText drawAtPoint:CGPointMake(0, 0) withFont:[UIFont fontWithName:@"Helvetica-Bold"] size:16.0];
//    displayText drawAtPoint:CGPointMake(0, 0) forWidth:<#(CGFloat)#> withFont:<#(UIFont *)#> fontSize:<#(CGFloat)#> lineBreakMode:<#(UILineBreakMode)#> baselineAdjustment:<#(UIBaselineAdjustment)#>
    
    CGContextSelectFont(context, "Monaco", 1000.0f   , kCGEncodingFontSpecific);
    CGContextSetTextDrawingMode(context, kCGTextFill);
    CGContextSetRGBFillColor(context, 1.0, 0.0, 0.0, 1.0);
//    CGAffineTransform xform = CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0);
//    CGContextSetTextMatrix(context, xform);

    CGContextShowTextAtPoint(context, 0.0, 0.0, [displayText UTF8String], [displayText length]);
    
    
}
-(void) dealloc{
//    
//    NSLog(@"Sprite Label Dealloc");
    [displayText release];displayText=nil;
    [super dealloc];
}

/*
 DisplayInfo* displayInfo=[rootSpatialStruct displayInfo];
 if (displayInfo){        
 //        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
 //        if ([appDele modelDisplayLevel]==0 && [rootSpatialStruct isKindOfClass:[Bldg class]]){

 */
 
@end

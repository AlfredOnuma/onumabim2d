//
//  AttachmentInfoTableUploadComment.h
//  ProjectView
//
//  Created by Alfred Man on 11/7/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AttachmentInfoDetail;
@class OPSProjectSite;
@interface AttachmentInfoTableUploadComment : NSObject <NSURLConnectionDelegate, UIAlertViewDelegate>
{
    uint _attachcommentID;
    AttachmentInfoDetail* _attachmentInfoDetail;
    OPSProjectSite* _projectSite;
    NSString* _commentTitle;
    NSString* _comment;
    NSString* _links;
    NSString* _commentDate;
    NSString* _attachedTo;
    NSInteger _referenceID;
    NSString* _uploadDate;
    
    
    
    NSMutableData* _connectionData;
    NSURLConnection* _theConnection;
}


@property (nonatomic, assign) OPSProjectSite* projectSite;
@property (nonatomic, retain) NSMutableData* connectionData;
@property (nonatomic, retain) NSURLConnection* theConnection;
@property (nonatomic, assign) uint attachcommentID;
@property (nonatomic, assign) AttachmentInfoDetail* attachmentInfoDetail;
@property (nonatomic, retain) NSString* commentTitle;
@property (nonatomic, retain) NSString* comment;
@property (nonatomic, retain) NSString* links;
@property (nonatomic, retain) NSString* commentDate;
@property (nonatomic, retain) NSString* attachedTo;
@property (nonatomic, assign) NSInteger referenceID;


//-(NSData*) upload:(NSArray*) paramArray;
-(void) upload:(NSArray*) paramArray;
-(id) initWithAttachmentInfoDetail:(AttachmentInfoDetail*)attachmentInfoDetail commentTitle:(NSString*)commentTitle comment:(NSString*)comment links:(NSString*)links commentDate:(NSString*)commentDate attachedTo:(NSString*)attachedTo referenceID:(NSInteger)referenceID;

-(id) initWithProjectSite:(OPSProjectSite*)projectSite attachcommentID:(uint)attachcommentID commentTitle:(NSString*)commentTitle comment:(NSString*)comment links:(NSString*)links commentDate:(NSString*)commentDate attachedTo:(NSString*)attachedTo referenceID:(NSInteger)referenceID;

-(void) alertUploadResult:(id)sender title:(NSString*)title;
@end


//
//  UtilityLine.m
//  ProjectView
//
//  Created by onuma on 09/05/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "UtilityLine.h"

#import "ExtrudedAreaSolid.h"
#import "Representation.h"

@implementation UtilityLine

-(UtilityLine *)initWithUtilityLineSQLStmt: (OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt parentTransform:(CGAffineTransform) parentTransform{
    self.model=model;
    NSInteger utilityLineID = sqlite3_column_int(sqlStmt, 0);
    //    NSInteger slabPolylineID = sqlite3_column_int(sqlStmt, 1);
    
    NSString* utilityLineGuid= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 2)!=nil){
        utilityLineGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 2)];
    }else{
        utilityLineGuid=[NSString stringWithFormat:@""];
    }
    
    self=[super init:utilityLineID guid:utilityLineGuid name:NULL];
    if (self){        
        //                                        [slabGuid autorelease];
        //        CGFloat slabThk=sqlite3_column_double(sqlStmt, 3);
        CGFloat placementX = sqlite3_column_double(sqlStmt, 4);//*multiFactor;
        CGFloat placementY = sqlite3_column_double(sqlStmt, 5);//*multiFactor;
        //        CGFloat placementZ = sqlite3_column_double(sqlStmt, 6);//*multiFactor;
        CGFloat placementAngle = sqlite3_column_double(sqlStmt, 7);
        
        char* pPolyStr=(char *) sqlite3_column_text(sqlStmt, 8);
        
        //        NSString* polyStr= [NSString stringWithUTF8String:(char *) sqlite3_column_text16(sqlStmt, 8)];
        CGPoint placementPt=CGPointMake(placementX, placementY);
        Placement* utilityLinePlacement=[[Placement alloc] init:0 guid:nil name:nil pt:placementPt angle:placementAngle isMirrorY:false];
        
        [self setPlacement:utilityLinePlacement];
        [utilityLinePlacement release];
        
        //    NSLog(@"Slab -------------------------------");
        //    NSLog(@"Slab Placement x:%f y:%f angle:%f",placementX,placementY,placementAngle);
        
        
        CGAffineTransform utilityLineTransform=CGAffineTransformConcat([utilityLinePlacement toCGAffineTransform], parentTransform);
        
        if (pPolyStr!=0){
            NSString* polyStr= [NSString stringWithUTF8String:pPolyStr];
            ExtrudedAreaSolid* extrudedAreaSolid=[[ExtrudedAreaSolid alloc] initWithPolyStr:polyStr  parentTransform:utilityLineTransform localFrameTransform:[placement toCGAffineTransform]];
            
            //        ExtrudedAreaSolid* extrudedAreaSolid= [[ExtrudedAreaSolid alloc] initWithExtrudedDatabase:database polylineID:slabPolylineID parentTransform:slabTransform];
            
            Representation* rep=[[Representation alloc] init:(0) guid:(nil) name:(nil)];
            [rep addARepresentationItem:(extrudedAreaSolid)];
            [extrudedAreaSolid release];
            [self setRepresentation:rep];
            [rep release];
        }
        
        
        
    }
    return self;
}

@end

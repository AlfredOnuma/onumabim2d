//
//  NavUIScroll.m
//  OPSMobile
//
//  Created by Alfred Man on 9/30/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "NavUIScroll.h"
#import "OPSModel.h"
#import "DisplayModelUI.h"

//#import "ProductViewButtonRing.h"
/*
#import "MGSplitViewAppDelegate.h"
#import "Site.h"
*/
#import "ProjectViewAppDelegate.h"
#import "ViewModelVC.h"
#import "OPSNavUI.h"
@implementation NavUIScroll
//@synthesize delegate;
//@synthesize displayModelUI;

//
//-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
//    //    return self.testUI;
//    return [[self parentViewModelVC] displayModelUI];
//}



 
 float oldX, oldY;
 BOOL dragging;

-(ViewModelVC*) parentViewModelVC{
    return [ ((OPSNavUI*) self.delegate) parentViewModelVC];
}
/*
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    [super touchesBegan:touches withEvent:event];
//    UITouch *touch = [touches anyObject];
//    UIView* v=touch.view;
//    if ([touch.view  isKindOfClass:[NavUIScroll class]]){
//        int xx=0;
//        xx=1;
//    }else{
//        int yy=0;
//        yy=1;
//    }
//    NSArray *ts = [touches allObjects];	
//	int nb = [touches count];
//	
//	if(nb<2){
//        //instructions
//    }else{
//        //other instructions
//    }
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];                    
//    [modelVC switchToolbarToNavScrollUIToolbar];
//    [modelVC clearSelectedProduct];

    
    
    
    //    ViewModelVC* modelVC=[self parentViewModelVC];//[appDele activeViewModelVC];    
    //    [modelVC clearSelectedProductRep];    
    
}
*/
-(DisplayModelUI*) displayModelUI{
    //    DisplayModelUI* displayModelUI=nil;
    for (UIView* subView in self.subviews){
        if ([subView isKindOfClass:[DisplayModelUI class]]){
            return (DisplayModelUI*) subView;
        }
    }   
    return nil;
}

-(void)scrollRectToVisibleCenteredOn:(CGRect)visibleRect
                            animated:(BOOL)animated
{
    CGRect centeredRect = CGRectMake(visibleRect.origin.x + visibleRect.size.width/2.0 - self.frame.size.width/2.0,
                                     visibleRect.origin.y + visibleRect.size.height/2.0 - self.frame.size.height/2.0,
                                     self.frame.size.width,
                                     self.frame.size.height);
    [self scrollRectToVisible:centeredRect
                     animated:animated];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    NSLog(@" Offset = %@ ",NSStringFromCGPoint(self.contentOffset));
    
/*
        if (scrollView.contentOffset.y > -60.0 && scrollView.contentOffset.y < 0.0 && insetOnReload) {
            [self.tableView setContentInset:UIEdgeInsetsMake(-scrollView.contentOffset.y, 0.0, 0.0, 0.0)];
        }
        else if (scrollView.contentOffset.y >= 0.0 && insetOnReload) {
            [self.tableView setContentInset:UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)];
            insetOnReload = NO;
        }
    */ 
}
//-(void) drawRect:(CGRect)rect{
//    UIView* t=[[UIView alloc] initWithFrame:CGRectMake(0,0,400,400)];
//    t.backgroundColor=[UIColor redColor];
//    [self addSubview:t];
//}


- (void) zoomFitWithDuration:(double)duration bRefreshLabel:(bool)bRefreshLabel invoker:(id)invoker completeAction:(SEL) completeAction actionParam:(id) paramObject{
    
    ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];
    OPSModel* model=[viewModelVC model];
    //    double fitZoom=[model fitZoom];
    double minZoom=[model minimumZoom];          
    
    CGFloat screenFrameWidth=self.frame.size.width;    
    CGFloat screenFrameHeight=self.frame.size.height;
    //    double modelScaleFactor=[model modelScaleForScreenFactor];
    
    CGFloat canvasWidth=screenFrameWidth/minZoom;
    CGFloat canvasHeight=screenFrameHeight/minZoom;
    
    CGFloat screenFitFrameWidth=[[[model root] bBox] getWidth] * [model modelScaleForScreenFactor];// screenFrameWidth/fitZoom;
    CGFloat screenFitFrameHeight=[[[model root] bBox] getHeight] * [model modelScaleForScreenFactor];//screenFrameHeight/fitZoom;
        
    double offsetX=canvasWidth/2-screenFitFrameWidth/2;//-modelWidth/2;
    double offsetY=canvasHeight/2-screenFitFrameHeight/2;//-modelHeight/2;    
    double rectWid=screenFitFrameWidth;//modelWidth;
    double rectHit=screenFitFrameHeight;//modelHeight;
    
    
    
    [self zoomToRectWithDuration:CGRectMake(offsetX,offsetY,rectWid,rectHit) duration:duration invoker:invoker completeAction:completeAction actionParam:paramObject];
    if (bRefreshLabel){
        [((OPSNavUI*) self.delegate) refreshProductLabel];
    }
    
}


- (void) zoomMin{
    

    ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];
    OPSModel* model=[viewModelVC model];
    double minZoom=[model minimumZoom];
//    CGFloat fitZoom=[model fitZoom];
    
    CGFloat screenFrameWidth=self.frame.size.width;
    CGFloat screenFrameHeight=self.frame.size.height;
    
    CGFloat canvasWidth=screenFrameWidth/minZoom;
    CGFloat canvasHeight=screenFrameHeight/minZoom;
    
//    CGFloat screenFitFrameWidth=[[[model root] bBox] getWidth] * [model modelScaleForScreenFactor];// screenFrameWidth/fitZoom;
//    CGFloat screenFitFrameHeight=[[[model root] bBox] getHeight] * [model modelScaleForScreenFactor];//screenFrameHeight/fitZoom;
//   
//    double offsetX=canvasWidth/2-screenFitFrameWidth/2;//-modelWidth/2;
//    double offsetY=canvasHeight/2-screenFitFrameHeight/2;//-modelHeight/2;
//    double rectWid=screenFitFrameWidth;//modelWidth;
//    double rectHit=screenFitFrameHeight;//modelHeight;
//
    

    double offsetX=0.0;//-modelWidth/2;
    double offsetY=0.0;//-modelHeight/2;
    double rectWid=canvasWidth;//modelWidth;
    double rectHit=canvasHeight;//modelHeight;
    
//    NSLog(@"zoomRect: %f, %f, %f, %f",offsetX,offsetY,rectWid,rectHit);
    
    [self zoomToRect:CGRectMake(offsetX,offsetY,rectWid,rectHit) animated:NO];
    
    [((OPSNavUI*) self.delegate) refreshProductLabel];
    
    
    
}

- (void) zoomFit{
    
    /*
    MGSplitViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];        
    Site* site=[[appDele model] root];
    RelAggregate* relAgg=[site relAggregate];
    
    */
    
   
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];   
    ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];
    OPSModel* model=[viewModelVC model];
//    double fitZoom=[model fitZoom];
    double minZoom=[model minimumZoom];

    CGFloat screenFrameWidth=self.frame.size.width;    
    CGFloat screenFrameHeight=self.frame.size.height;
//    double modelScaleFactor=[model modelScaleForScreenFactor];
    
    CGFloat canvasWidth=screenFrameWidth/minZoom;
    CGFloat canvasHeight=screenFrameHeight/minZoom;
    
    CGFloat screenFitFrameWidth=[[[model root] bBox] getWidth] * [model modelScaleForScreenFactor];// screenFrameWidth/fitZoom;
    CGFloat screenFitFrameHeight=[[[model root] bBox] getHeight] * [model modelScaleForScreenFactor];//screenFrameHeight/fitZoom;
    
//    NSLog (@"zoomscale: %f, screenFrameWidth: %f , screenFrameHeight: %f , screenFitFrameWidth: %f, screenFitFrameHeight: %f",[self zoomScale], screenFrameWidth, screenFrameHeight, screenFitFrameWidth, screenFitFrameHeight);
    
    
//    double offsetX=canvasWidth/2-screenFitFrameWidth/2;//-modelWidth/2;
//    double offsetY=canvasHeight/2-screenFitFrameHeight/2;//-modelHeight/2;    
//    double rectWid=screenFitFrameWidth;//modelWidth;
//    double rectHit=screenFitFrameHeight;//modelHeight;
    
    
    
    
    
//    double offsetX=canvasWidth/2- model.root.bBox.minX * modelScaleFactor-screenFitFrameWidth/2;//-modelWidth/2;
//    double offsetY=canvasHeight/2- model.root.bBox.minY * modelScaleFactor-screenFitFrameHeight/2;//-modelHeight/2;    
//    double rectWid=screenFitFrameWidth;//modelWidth;
//    double rectHit=screenFitFrameHeight;//modelHeight;

    double offsetX=canvasWidth/2-screenFitFrameWidth/2;//-modelWidth/2;
    double offsetY=canvasHeight/2-screenFitFrameHeight/2;//-modelHeight/2;    
    double rectWid=screenFitFrameWidth;//modelWidth;
    double rectHit=screenFitFrameHeight;//modelHeight;

    NSLog(@"zoomRect: %f, %f, %f, %f",offsetX,offsetY,rectWid,rectHit);
    
//    [self scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];  
    [self zoomToRect:CGRectMake(offsetX,offsetY,rectWid,rectHit) animated:NO];
//        NSLog (@"zoomscale: %f ,zoomoffsetX: %f, zoomoffsetY: %f ,zoomCanvasWid: %f, zoomCanavsHit:%f, zoomWid: %f , zoomHit: %f:",[self zoomScale], offsetX, offsetY,canvasWidth, canvasHeight, rectWid, rectHit);
    
    
    [((OPSNavUI*) self.delegate) refreshProductLabel];
    
    
    
//    [viewModelVC refreshProductLabel];
    /*
    
    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];   

    CGFloat frameWidth=self.frame.size.width;
    CGFloat frameHeight=self.frame.size.height;
    
    CGFloat canvasWidth=frameWidth;///[self zoomScale];
    CGFloat canvasHeight=frameHeight;///[self zoomScale];
    
    
    CGFloat modelWidth=canvasWidth*[[appDele model] canvasScaleForScreenFactor];
    CGFloat modelHeight=canvasHeight*[[appDele model] canvasScaleForScreenFactor];
    
    
//    double offsetX=canvasWidth/2-frameWidth/2;
//    double offsetY=canvasHeight/2-frameHeight/2;
    double offsetX=canvasWidth/2;//-modelWidth/2;
    double offsetY=canvasHeight/2;//-modelHeight/2;    
    double rectWid=canvasWidth;//modelWidth;
    double rectHit=canvasHeight;//modelHeight;
    [self scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];  
    [self zoomToRect:CGRectMake(offsetX,offsetY,rectWid,rectHit) animated:NO];
    
    return;
    
//    NSLog (@"zoomscale: %f, canvas width: %f , canvas height: %f",[self zoomScale], canvasWidth, canvasHeight);
    /////////////////////
    NSLog (@"zoomscale: %f, canvas width: %f , canvas height: %f , model width: %f, model height: %f",[self zoomScale], canvasWidth, canvasHeight, modelWidth, modelHeight);
    
    
//    double offsetX=canvasWidth/2;
//    double offsetY=canvasHeight/2;
//    double rectWid=canvasWidth/10;
//    double rectHit=canvasHeight/10;        
//    NSLog (@"offsetX: %f, offsetY : %f , canvas height: %f , rectWid: %f, rectHit: %f",rectWid, rectHit); 
    
    double zoomScale=[self zoomScale];
    double offsetX=(canvasWidth/2-modelWidth/2)*zoomScale;
    double offsetY=(canvasHeight/2-modelHeight/2)*zoomScale;
    double rectWid=modelWidth*zoomScale;
    double rectHit=modelHeight*zoomScale;  

//    ViewModelVC* vc=[self delegate];
//    [vc displayModelUI].frame = CGRectMake(0, 0, 1024, 576);
    
//    double rectWid=canvasWidth*[self zoomScale];
//    double rectHit=canvasHeight*[self zoomScale];    
    
    
    NSLog (@"offsetX: %f, offsetY : %f , canvas height: %f , rectWid: %f, rectHit: %f",offsetX, offsetY, rectWid, rectHit); 
    
    //    [self scrollRectToVisibleCenteredOn:(CGRectMake(0, 0, modelWidth, modelHeight)) animated:false];
    //                                return;
    
//        [self reset];
//    [self scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];    
    
    [self zoomToRect:CGRectMake(offsetX,offsetY,rectWid,rectHit) animated:NO];    
    
//    [self zoomToRect:CGRectMake(offsetX,offsetY,rectWid,rectHit) animated:NO];      
    return;
    ///////////////////////////////
    CGFloat canvasWidth=self.frame.size.width/[self zoomScale];    
    CGFloat canvasHeight=self.frame.size.height/[self zoomScale];
    CGFloat modelWidth=canvasWidth/[[appDele model] canvasScaleForScreenFactor];
    CGFloat modelHeight=canvasHeight/[[appDele model] canvasScaleForScreenFactor];
    
    
    NSLog (@"zoomscale: %f, canvas width: %f , canvas height: %f , model width: %f, model height: %f",[self zoomScale], canvasWidth, canvasHeight, modelWidth, modelHeight);
    double offsetX=canvasWidth/2-modelWidth/2;
    double offsetY=canvasHeight/2-modelHeight/2;
    double rectWid=modelWidth;
    double rectHit=modelHeight;
    
    
    NSLog (@"offsetX: %f, offsetY : %f , canvas height: %f , rectWid: %f, rectHit: %f",rectWid, rectHit); 
//    [self scrollRectToVisibleCenteredOn:(CGRectMake(0, 0, modelWidth, modelHeight)) animated:false];
  //                                return;
    
//    [self reset];
    [self scrollRectToVisible:CGRectMake(0, 0, 1, 1)
                            animated:NO];    

    [self zoomToRect:CGRectMake(offsetX,offsetY,rectWid,rectHit) animated:NO]; 
//    [self zoomToRect:CGRectMake(offsetX,offsetY,modelWidth*10,modelHeight*10) animated:NO]; 
//    CGRect viewRect=CGRectMake(offsetX,offsetY,modelWidth,modelHeight);    
    
//    [self zoomToRect:(viewRect) animated:YES];  
    
    /////////////////////////////////////
    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
    
//    DetailViewController* controller=[self delegate];
    CGFloat canvasScaleForScreenFactor=[[appDele model] canvasScaleForScreenFactor];//[[controller getModel]  canvasScaleForScreenFactor];
    
    CGFloat scrollWidth=self.frame.size.width;       
    CGFloat scrollHeight=self.frame.size.height; 
    
    
    CGFloat canvasWidth=scrollWidth*    canvasScaleForScreenFactor;
    CGFloat canvasHeight=scrollHeight*  canvasScaleForScreenFactor;
    CGRect canvasRect;
    canvasRect=CGRectMake(0,0,canvasWidth,canvasHeight );    
    
   
    CGRect viewRect=CGRectMake(canvasWidth/2-scrollWidth/2,canvasHeight/2-scrollHeight/2,scrollWidth,scrollHeight);    
    
//    CGRect zoomRect=CGRectMake(self.frame.origin.x+viewRect.origin.x,self.frame.origin.y+viewRect.origin.y,viewRect.size.width,viewRect.size.height);
    
    CGRect zoomRect=CGRectMake(viewRect.origin.x,viewRect.origin.y,viewRect.size.width,viewRect.size.height);    
    //                    zoomRect=CGRectMake(self.frame.origin.x+viewRect.origin.x,self.frame.origin.y+viewRect.origin.y,viewRect.size.width,viewRect.size.height);      
    
    CGRect zoomRect1;
    zoomRect1=CGRectMake(1024,800,800    ,300); 
    
    
    CGRect zoomRect2;
    zoomRect2=CGRectMake(512,283.5,1024    ,567);    
//    self.contentSize=self.frame.size;
    [self zoomToRect:(zoomRect1) animated:NO];      
 //   [self zoomToRect:(zoomRect2) animated:YES]; 
     */
}

-(void) zoomToRectWithDuration:(CGRect)rect duration:(double) duration invoker:(id)invoker completeAction:(SEL) completeAction actionParam:(id)paramObject{
    [UIView animateWithDuration:(duration)
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self zoomToRect:rect animated:NO];
                     } 
                     completion:^(BOOL finished){//this block starts only when 
                         if (invoker){
                             if (completeAction){
                                 [invoker performSelector:completeAction withObject:paramObject];
                             }
                         }
                     }];
}
/*
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    //Get all the touches.
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];           
//    ViewModelVC* modelVC=[appDele activeViewModelVC];    
//    [modelVC clearSelectedProduct];    
    
    NSSet *allTouches = [event allTouches];

//    CGPoint pt=[self contentOffset];  
//    pt.x+=100;
//    pt.y+=100;
//    [self setContentOffset:pt];

//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];  
//    OPSModelViewController* 
    

//    CGRect zoomRect=CGRectMake(self.contentOffset.x, self.contentOffset.y, self.contentSize.width, self.contentSize.height);
    
    //Number of touches on the screen
    switch ([allTouches count])
    {
        case 1:
        {
            //Get the first touch.

            UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
           NSLog (@"scale : %f tap count: %d",[self zoomScale], [touch tapCount]);
            
            switch([touch tapCount])
            {
//
//                case 3:{
//                    DisplayModelUI* _displayModelUI=[self displayModelUI];
//                    [_displayModelUI captureMapView];
//                    break;
//                }
                case 2://Double tap.                    
                    [self zoomFit];

//                    zoomRect=CGRectMake(self.frame.origin.x+viewRect.origin.x,self.frame.origin.y+viewRect.origin.y,viewRect.size.width,viewRect.size.height);
////                    zoomRect=CGRectMake(self.frame.origin.x+viewRect.origin.x,self.frame.origin.y+viewRect.origin.y,viewRect.size.width,viewRect.size.height);      
//                    [self zoomToRect:(zoomRect) animated:YES];                                
////                    imgView.contentMode = UIViewContentModeCenter;
                    break;
                case 1://Single tap
                    //                    imgView.contentMode = UIViewContentModeScaleAspectFit;
                    

                    break;                    
            }
        }
            break;
    }

//    UITouch *touch = [[event allTouches] anyObject];  

//    CGPoint touchLocation=[touch locationInView:   [(ViewModelVC*) self.delegate displayModelUI]   ];    
//    NSLog (@"TouchLocation x: %f y:%f",touchLocation.x, touchLocation.y);
//    [[(ViewModelVC*) self.delegate displayModelUI] showProductRepButton:touchLocation]; 
    
    
//        CGPoint touchLocation=[touch locationInView:self];
                           
//    [self showProductRepButton:touchLocation]   ;

}
*/
/*

- (void) showProductRepButton: (CGPoint) touchLocation{
    CGFloat rad1=50;
    CGFloat rad2=75;
    CGFloat rad3=100;    
    CGFloat rad4=125;        
    CGRect rect=CGRectMake(touchLocation.x-rad3,touchLocation.y-rad3, rad3*2, rad3*2);
    
    ProductViewButtonRing* productViewButtonRing=[[ProductViewButtonRing alloc] initWithFrame:rect center:touchLocation rad1:rad1 rad2:rad2 rad3:rad3 rad4:rad4];

    [productViewButtonRing setNeedsDisplay];
    
    [self addSubview:productViewButtonRing];
    [productViewButtonRing release];
    
}

*/
- (id) initWithFrame:(CGRect)frame opsNavUI:(OPSNavUI*) opsNavUI{
    
    self=[super initWithFrame:frame];
    if (self){
        self.delegate=opsNavUI;
        self.backgroundColor=[UIColor clearColor];//[UIColor yellowColor];
        self.alpha=1.0;
        [self setClipsToBounds:false];
    }else{
        return NULL;
    }
    return self;
}
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)dealloc
{
//    [displayModelUI release];
    [super dealloc];
}

//- (void)didReceiveMemoryWarning
//{
//    // Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//    
//    // Release any cached data, images, etc that aren't in use.
//}

#pragma mark - View lifecycle
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    // Do any additional setup after loading the view from its nib.
//}
//
//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}
	
@end

//
//  ViewModelToolBarTitleScroll.m
//  ProjectView
//
//  Created by Alfred Man on 4/17/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewModelToolBarTitleScroll.h"
#import "ViewProductRep.h"
#import "ViewModelVC.h"
#import "OPSProduct.h"
#import "ProjectViewAppDelegate.h"
#import "ViewModelToolbar.h"
#import "Site.h"
#import "Floor.h"
#import "Bldg.h"
#import "Space.h"
#import "Furn.h"
#import "OPSProjectSite.h"
@implementation ViewModelToolBarTitleScroll

@synthesize productTitleArrayToolbar;
@synthesize aProductViewRep;
@synthesize titleBarView;
@synthesize viewModelToolbar;

-(void) dealloc{
//    [viewModelToolbar release];viewModelToolbar=nil;
    [productTitleArrayToolbar release];productTitleArrayToolbar=nil;
    [aProductViewRep release];aProductViewRep=nil;
    [titleBarView release];titleBarView=nil;
    [super dealloc];
}
-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    //    return self.testUI;   
    return  [self titleBarView] ;
//    return [self productTitleArrayToolbar];
}


- (void) test:(id)sender{  
}




-(NSString*) findProductRepDisplayStr: (ViewProductRep*) productRep{
    return [[productRep product] productRepDisplayStr];
    /*
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
    ViewModelVC*  viewModelVC=[viewModelToolbar parentViewModelVC];
    //        OPSProduct* selectedProduct=[viewModelVC selectedProduct];
    //    OPSProduct* selectedProduct=[[viewModelVC selectedProductRep] product];
    OPSProduct* product=[productRep product];    
    
    OPSProduct* refProductFromSelectedProduct=nil;
    
    
    NSString* displayStr=nil;
    switch ([appDele modelDisplayLevel]){
        case 0:                        
            if ([product isKindOfClass:[Site class]]){
                refProductFromSelectedProduct=(Site*) [[viewModelVC model] root];
                displayStr=[NSString stringWithFormat:@"Site: (S%d_%d) %@",[appDele activeStudioID],[appDele activeProjectSite].ID,[refProductFromSelectedProduct name]];            
            }else if ([product isKindOfClass:[Floor class]]){
                refProductFromSelectedProduct=(Bldg*) [[product linkedRel]relating];
                displayStr=[NSString stringWithFormat:@"Bldg: %d - %@",[refProductFromSelectedProduct ID],[refProductFromSelectedProduct name]];
            }else{
                refProductFromSelectedProduct=product;                
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];
            }
            break;
        case 1:
            
            if ([product isKindOfClass:[Floor class]]){
                Bldg* bldg=(Bldg*) [[viewModelVC model] root];
                Floor* floor=(Floor*) [[[bldg relAggregate] related] objectAtIndex:[bldg selectedFloorIndex]];
                refProductFromSelectedProduct=floor;
                displayStr=[NSString stringWithFormat:@"Floor: %@",[refProductFromSelectedProduct name]];      
                
            }
            else if ([product isKindOfClass:[Space class]]){
//                refProductFromSelectedProduct=(Space*) product;
                
//                displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]]; 
                
                
                Space* space=(Space*) product;
                
                if ([space spaceNumber]!=nil && ![[space spaceNumber] isEqualToString:@""]){                    
                    displayStr=[NSString stringWithFormat:@"%@ - %@ (%@)",[space spaceNumber],[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]]; 
                }else{                    
                    displayStr=[NSString stringWithFormat:@"%@ (%@)",[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]]; 
                }
                
                //                    refProductFromSelectedProduct=(Floor*) [[selectedProduct linkedRel]relating];                    
                //                    displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]];                    
            }else{
                refProductFromSelectedProduct=product;                    
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];                    
            }
            break;                
        case 2:
            
            if ([product isKindOfClass:[Space class]]){            
//                refProductFromSelectedProduct=(Space*) [[viewModelVC model] root];
//                displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]];
//                
                Space* space=(Space*) [[viewModelVC model] root];
                
                if ([space spaceNumber]!=nil && ![[space spaceNumber] isEqualToString:@""]){                    
                    displayStr=[NSString stringWithFormat:@"%@ - %@ (%@)",[space spaceNumber],[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]]; 
                }else{                    
                    displayStr=[NSString stringWithFormat:@"%@ (%@)",[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]]; 
                }
                
                            
            }else if ([product isKindOfClass:[Furn class]]){
//                refProductFromSelectedProduct=(Furn*) product;
                Furn * furn=(Furn*) product;
                
                
                if ([furn componentName]){
                    if (![[furn componentName] isEqualToString:@""]){
                        NSString *trimmedString = [[furn componentName] stringByTrimmingCharactersInSet:
                                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        if (![trimmedString isEqualToString:@""]){                        
                            displayStr=[NSString stringWithFormat:@"%@ - %@",[furn componentName],[furn name]];   
                        }
                    }
                }
                if (displayStr==nil){
                    displayStr=[NSString stringWithFormat:@"%@",[furn name]];
                }
            }else{
                refProductFromSelectedProduct=product;
                
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];
            }
            break;        
        default:
            break;
    }
    return displayStr;
     */
}




-(void) updateASelectedProductRep:(NSMutableArray *) aProductRep{
    if (aProductRep!=nil){
        [self.titleBarView removeFromSuperview];
        
        
        NSString* buttonText=@"";//aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        
        if ([aProductRep count]==1){            
            buttonText=[self findProductRepDisplayStr:[aProductRep objectAtIndex:0]];
        }else if ([aProductRep count]>1){            
            buttonText=[NSString stringWithFormat:@"Multiple Selections"];// | Last Selected %@",[self findProductRepDisplayStr: [aProductRep lastObject]]];   
        }
//        for (ViewProductRep* productRep in aProductRep){
////            OPSProduct* product=productRep.product;
//            if ([aProductRep count]<=1){
//                buttonText=[self findProductRepDisplayStr:productRep];
//            }else{
//                buttonText=[NSString stringWithFormat:@"%@ | %@",[self findProductRepDisplayStr:productRep], buttonText];                
//            }
//        }
        
   
        CGSize fontSize = [buttonText sizeWithFont:[UIFont boldSystemFontOfSize:16.0]];
        CGRect buttonFrame = CGRectMake(0.0f, 0.0f, fontSize.width + 15.0, fontSize.height + 15.0);
//        uint width=fontSize.width;
//        NSLog(@"%@ | width:%d",buttonText,width);
        UILabel* newLabel=nil;
        newLabel=[[UILabel alloc] initWithFrame:buttonFrame];//CGRectMake(0, 0, 100, 44)];
        newLabel.text=buttonText;
        newLabel.font=[UIFont boldSystemFontOfSize:16];;
        
        newLabel.backgroundColor=[UIColor clearColor];
        
        UIView* _titleBarView=[[UIView alloc] initWithFrame:buttonFrame];//CGRectMake(0, 0, 900, 44)];
        self.titleBarView=_titleBarView;
        [_titleBarView release];
        [titleBarView addSubview:newLabel];
        [newLabel release];
        [self addSubview:titleBarView];        
        self.contentSize=titleBarView.frame.size;
//        self.contentSize= CGSizeMake(buttonFrame.size.width, 44);
//        self.contentSize=CGSizeMake(560, 44);        
//        CGSize test=self.contentSize;
//        NSLog(@"%f, %f",test.width,test.height);
    }
    
}
-(id) initWithFrame:(CGRect)frame viewModelToolbar:(ViewModelToolbar *)_viewModelToolbar{// aProdcutRep:(NSMutableArray *)aProductRep{
    self=[super initWithFrame:frame];
    if (self){
        self.viewModelToolbar=_viewModelToolbar;
        self.delegate=self;
        self.scrollEnabled=true;
//        self.pagingEnabled=true;
        
        self.contentSize=CGSizeMake( 900, 44);

        
        NSString* buttonText=@"aaaaaaa";
//        if (aProductRep!=nil){
        
//        
//        UIButton* bi=nil;
//        bi=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
//        bi.titleLabel.text=@"aaaaaaaaaaa";
////        bi = [[UIButton alloc]initWithFrame:<#(CGRect)#> initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
//////        bi.style = UIBarButtonItemStyleBordered;
//        bi.enabled=TRUE;        
//        
//        //        buttonIndex_Attach=[buttons count];
//        [titleBarView addSubview:bi];
//        [bi release];
        
                                    //            for (ViewProductRep* productRep in aProductRep){
                                    //                OPSProduct* product=productRep.product;
                                    //                buttonText=[NSString stringWithFormat:@"%@ | %@",[product name],buttonText];                
                                    //            }
                                    //            
                                    ////            NSString* buttonText=@"Aaaaaaaaaaa Bbbb Ccccc  Dddd";        
                                    //            CGSize fontSize = [buttonText sizeWithFont:[UIFont systemFontOfSize:24.0]];
                                    //            CGRect buttonFrame = CGRectMake(0.0f, 0.0f, fontSize.width + 15.0, fontSize.height + 15.0);        
                                    //            UILabel* newLabel=nil;
                                    //            newLabel=[[UILabel alloc] initWithFrame:buttonFrame];//CGRectMake(0, 0, 100, 44)];
                                    //            newLabel.text=buttonText;
                                    //            newLabel.font=[UIFont italicSystemFontOfSize:24];;
                                    //            
                                    //            newLabel.backgroundColor=[UIColor clearColor];
                                    //                
                                    //            titleBarView=[[UIView alloc] initWithFrame:buttonFrame];//CGRectMake(0, 0, 900, 44)];
                                    //            [titleBarView addSubview:newLabel];
                                    //            [newLabel release];

        
        
        
   
//        CGSize fontSize = [buttonText sizeWithFont:[UIFont systemFontOfSize:24.0]];
////        CGRect currentFrame = button.frame;
////        CGRect windowFrame = self.view.frame;
////        CGRect buttonFrame = CGRectMake(windowFrame.size.width/2.0 - (fontSize.width + 15.0)/2.0, 0.0f, fontSize.width + 15.0, fontSize.height + 15.0);
//        CGRect buttonFrame = CGRectMake(0.0f, 0.0f, fontSize.width + 15.0, fontSize.height + 15.0);        
//        UIButton* bu=[[UIButton alloc] initWithFrame:buttonFrame];
//        bu.titleLabel.text=buttonText;        
////        [button setFrame:buttonFrame];
//        [titleBarView addSubview:bu];
        
        
        UILabel* newLabel=nil;
        newLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
        newLabel.text=buttonText;
        newLabel.font=[UIFont boldSystemFontOfSize:16];;
        
        newLabel.backgroundColor=[UIColor clearColor];
                
        [titleBarView addSubview:newLabel];
        [newLabel release];
            [self addSubview:titleBarView];        
            self.contentSize=titleBarView.frame.size;
//        }
        //        NSMutableArray *buttons = [[NSMutableArray alloc] init];        
        
        /*
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        
        
        [productTitleArrayToolbar setItems:buttons animated:NO];
        
        [buttons release]; 
        
        */

        /*
        productTitleArrayToolbar=[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 900, 44)];
        
        NSMutableArray *buttons = [[NSMutableArray alloc] init];        
        
        UIBarButtonItem* bi=nil;
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
//        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(test:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        
        //        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        
        
        [productTitleArrayToolbar setItems:buttons animated:NO];

        [buttons release];     

        [self addSubview:productTitleArrayToolbar];
        */
    }
    return self;
}
/*

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
 */

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

//
//  ViewPlanSitePolyInfoVC.h
//  ProjectView
//
//  Created by onuma on 21/05/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "ViewPlanInfoVC.h"

@interface ViewPlanSitePolyInfoVC : ViewPlanInfoVC


{
    
    
    UILabel* _uiStartDate;
    UILabel* _uiEndDate;
    UILabel* _uiDemolitionDate;

    
    
    //    NSString* _spaceNumber;
    //    double _netCalculated;
    //    double _height;
    //    double _capacity;
    //    double _occupancy;
    //    uint _numComponents;
    //    NSString* department;
    
    
}

@property (nonatomic, retain) UILabel* uiStartDate;
@property (nonatomic, retain) UILabel* uiEndDate;
@property (nonatomic, retain) UILabel* uiDemolitionDate;

@end

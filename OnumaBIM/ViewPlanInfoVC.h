//
//  ViewPlanInfoVC.h
//  ProjectView
//
//  Created by Alfred Man on 5/15/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <sqlite3.h>







#import "ProjectViewAppDelegate.h"
#import "OPSProjectSite.h"
#import "ViewModelToolbar.h"
#import "ViewModelVC.h"
#import "Site.h"
#import "ViewProductRep.h"
#import "OPSProduct.h"
#import "OPSModel.h"
#import "Floor.h"
#import "Bldg.h"
#import "Space.h"
#import "Furn.h"
#import "RelAggregate.h"
#import "CustomColorStruct.h"
#import "CustomColor.h"


@interface ViewPlanInfoVC : UIViewController <UIScrollViewDelegate>{
    ViewModelVC* _viewModelVC;
    UIViewController* _vc;
    
    
    
    UIView* _infoView;
    UIView* _rootView;
    UIScrollView* _infoScrollView;    
    UIButton* _liveReportButton;
    
}
@property (nonatomic, assign) UIViewController* vc;
@property (nonatomic, assign) ViewModelVC* viewModelVC;
@property (nonatomic, retain) UIView* rootView;
@property (nonatomic, retain) UIView* infoView;
@property (nonatomic, retain) UIScrollView* infoScrollView;
@property (nonatomic, retain) UIButton* liveReportButton;


//- (NSString*) intToIntStr:(int) i;
//- (NSString*) doubleToAreaStr:(double) d;
//- (NSString*) doubleToLengthStr:(double) d;
//- (NSString*) doubleToCurrenyStr:(double) d;

-(void) prepareLabel:(UILabel*) titlelabel;
-(void) prepareUILabel:(UILabel*) uiLabel;

-(void) prepareSelectedProductLabel:(UILabel*) uiLabel;

-(uint) selectedProductTitleFontSize;
-(uint) selectedProductLabelFontSize;
-(void) prepareSelectedProductTitle:(UILabel*) uiLabel;
    
    
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil viewController:(UIViewController*)viewController;

-(IBAction)displayWebView:(id)sender;
-(id)initWithViewModelVC:(ViewModelVC*)viewModelVC;
-(id)initWithViewController:(UIViewController*)viewController;

-(void) drawGreyBGCellWithRowIndex:(uint)pRow hPos:(uint)hPos vPos:(uint)vPos width:(uint)width height:(uint)height;
@end

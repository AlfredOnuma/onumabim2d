//
//  ArcProfile.m
//  ProjectView
//
//  Created by onuma on 02/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "ArcProfile.h"

@implementation ArcProfile
@synthesize endPointIndex=_endPointIndex;
@synthesize centerX=_centerX;
@synthesize centerY=_centerY;
@synthesize bAntiClock=_bAntiClock;


-(id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName endPointIndex:(uint)endPointIndex centerX:(double)centerX centerY:(double)centerY bAntiClock:(bool)bAntiClock{
    self = [super init:newID guid:newGUID name:newName];
    if (self){
        self.endPointIndex=endPointIndex;
        self.centerX=centerX;
        self.centerY=centerY;
        self.bAntiClock=bAntiClock;
    }
    return self;
}


-(id)copyWithZone:(NSZone *)zone
{
//    NSMutableArray *aNewPoint =nil;
    
    
    ArcProfile* newArcProfile=[[ArcProfile alloc] init:self.ID guid:self.GUID name:self.name endPointIndex:self.endPointIndex centerX:self.centerX centerY:self.centerY bAntiClock:self.bAntiClock] ;
    BBox* selfBBox=[self.bBox copy];
    newArcProfile.bBox=selfBBox;
    [selfBBox release];        
    newArcProfile.area=self.area;
    return newArcProfile;
}
//- (void) calculateAreaAndCheckAntiClock{
//    self.area=0;
//    //    CPoint* thisPt=[self.aPoint objectAtIndex:0];
//    CPoint* prevPt=[self.aPoint lastObject];
//    
//    for (CPoint* thisPt in self.aPoint){
//        self.area+=prevPt.x*thisPt.y-thisPt.x*prevPt.y;
//        prevPt=thisPt;
//    }
//    self.area/=2;
//    self.isAntiClock=(self.area>0);
//    self.area=fabs(self.area);
//}
//


@end

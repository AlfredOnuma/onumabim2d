//
//  AttachmentInfoData.h
//  ProjectView
//
//  Created by Alfred Man on 3/1/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <sqlite3.h>
@interface AttachmentInfoData : NSObject{
    uint attachCommentID;
    uint attachUserID;
    NSString* attachUserName;
    NSString* commentTitle;
    NSString* comment;
    NSString* link;
    NSString* commentDate;
    NSString* attachedProductType;
    uint attachedProductID;
    NSString* fileName;
    bool bSent;
    uint tmpPhotoIndex;
}
@property (nonatomic, assign) uint tmpPhotoIndex;
@property (nonatomic, assign) uint attachCommentID;
@property (nonatomic, assign) uint attachUserID;
@property (nonatomic, retain) NSString* attachUserName;
@property (nonatomic, retain) NSString* commentTitle;
@property (nonatomic, retain) NSString* comment;
@property (nonatomic, retain) NSString* link;
@property (nonatomic, retain) NSString* commentDate;
@property (nonatomic, retain) NSString* attachedProductType;
@property (nonatomic, assign) uint attachedProductID;
@property (nonatomic, retain) NSString* fileName;
@property (nonatomic, assign) bool bSent;

-(id) initWithDatabase:(sqlite3*)database sqlStmt: (sqlite3_stmt*) sqlStmt;
-(id) initWithDatabase:(sqlite3*)database sqlStmt: (sqlite3_stmt*) sqlStmt attachedProductType:(NSString*)_attachedProductType attachedProductID:(uint)_attachedProductID;
@end

//
//  AttachmentInfoData.m
//  ProjectView
//
//  Created by Alfred Man on 3/1/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "AttachmentInfoData.h"
#import "ProjectViewAppDelegate.h"
#import "OPSProjectSite.h"
@implementation AttachmentInfoData
@synthesize bSent;
@synthesize attachCommentID;
@synthesize attachUserID;
@synthesize attachUserName;
@synthesize commentTitle;
@synthesize comment;
@synthesize link;
@synthesize commentDate;
@synthesize attachedProductType;
@synthesize attachedProductID;
@synthesize fileName;
@synthesize tmpPhotoIndex;
-(void) dealloc{
    [attachUserName release],attachUserName=nil;
    [commentTitle release],commentTitle=nil;
    [comment release],comment=nil;
    [link release],link=nil;
    [commentDate release],commentDate=nil;
    [attachedProductType release], attachedProductType=nil;
    [fileName release],fileName=nil;   
    [super dealloc];
}

-(id) initWithDatabase:(sqlite3*)database sqlStmt: (sqlite3_stmt*) sqlStmt{
    
    
    //    @"SELECT AttachComment.ID,"              //0
    //    @" AttachComment.spatialStructureID,"    //1
    //    @" AttachComment.userID,"                //2
    //    @" AttachComment.userName,"              //3
    //    @" AttachComment.commentTitle,"          //4
    //    @" AttachComment.comment,"               //5
    //    @" AttachComment.links,"                 //6
    //    @" AttachComment.commentDate,"           //7
    //    @" AttachComment.attachedTo,"            //8
    //    @" AttachComment.referenceID,"           //9
    //    @" COUNT(*) AS numAttachments,"          //10
    //    @" fileName"                             //11
    self=[super init];
    if (self){
        
        attachCommentID= sqlite3_column_int(sqlStmt, 0);
        attachUserID=sqlite3_column_int(sqlStmt, 1);
        NSString* tokenName= nil;
        if ((char *) sqlite3_column_text(sqlStmt, 3)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 3)];
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.attachUserName=tokenName;
        
        if ((char *) sqlite3_column_text(sqlStmt, 4)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 4)];
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.commentTitle=tokenName;
        
        if ((char *) sqlite3_column_text(sqlStmt, 5)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 5)];
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.comment=tokenName;
        
        
        if ((char *) sqlite3_column_text(sqlStmt, 6)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 6)];
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.link=tokenName;
        
        
        
        if ((char *) sqlite3_column_text(sqlStmt, 7)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 7)];
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.commentDate=tokenName;
        
        
        
        if ((char *) sqlite3_column_text(sqlStmt, 11)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 11)];
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.fileName=tokenName;
        
        self.attachedProductType=[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:8];
        self.attachedProductID= (sqlite3_column_int(sqlStmt, 9));

        
        
        
        
        
        
        
        self.bSent = (sqlite3_column_int(sqlStmt, 12)==1);
        self.tmpPhotoIndex=0;
    }
    return self;
    
}




-(id) initWithDatabase:(sqlite3*)database sqlStmt: (sqlite3_stmt*) sqlStmt attachedProductType:(NSString*)_attachedProductType attachedProductID:(uint)_attachedProductID{
    
        
    //    @"SELECT AttachComment.ID,"              //0
    //    @" AttachComment.spatialStructureID,"    //1
    //    @" AttachComment.userID,"                //2
    //    @" AttachComment.userName,"              //3
    //    @" AttachComment.commentTitle,"          //4
    //    @" AttachComment.comment,"               //5                         
    //    @" AttachComment.links,"                 //6
    //    @" AttachComment.commentDate,"           //7
    //    @" AttachComment.attachedTo,"            //8
    //    @" AttachComment.referenceID,"           //9
    //    @" COUNT(*) AS numAttachments,"          //10
    //    @" fileName"                             //11
    self=[super init];
    if (self){
        
        attachCommentID= sqlite3_column_int(sqlStmt, 0);
        attachUserID=sqlite3_column_int(sqlStmt, 1);
        NSString* tokenName= nil;
        if ((char *) sqlite3_column_text(sqlStmt, 3)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 3)];               
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.attachUserName=tokenName;
        
        if ((char *) sqlite3_column_text(sqlStmt, 4)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 4)];               
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.commentTitle=tokenName;
        
        if ((char *) sqlite3_column_text(sqlStmt, 5)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 5)];               
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.comment=tokenName;
        
        
        if ((char *) sqlite3_column_text(sqlStmt, 6)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 6)];               
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.link=tokenName;
        
        
        
        if ((char *) sqlite3_column_text(sqlStmt, 7)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 7)];               
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.commentDate=tokenName;
        
        
        
        if ((char *) sqlite3_column_text(sqlStmt, 11)!=nil){
            tokenName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 11)];               
        }else{
            tokenName=[NSString stringWithFormat:@""];
        }
        self.fileName=tokenName;        
        self.attachedProductType=_attachedProductType;
        self.attachedProductID=_attachedProductID;
        
        
        
        
        
        
        
        self.bSent = (sqlite3_column_int(sqlStmt, 12)==1);
        self.tmpPhotoIndex=0;
    }
    return self;
    
}


//

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)didReceiveMemoryWarning
//{
//    // Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//    
//    // Release any cached data, images, etc that aren't in use.
//}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

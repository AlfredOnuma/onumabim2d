//
//  ConstructionStatusTVC.m
//  ProjectView
//
//  Created by Alfred Man on 7/23/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ConstructionStatusTVC.h"




#import "ConstructionStatusColor.h"
#import "ProjectViewAppDelegate.h"
#import "BIMPlanColorCatNVC.h"
#import "Site.h"
#import "DisplayModelUI.h"
#import "ViewModelVC.h"
#import "ViewModelToolbar.h"
#import "Bldg.h"



@interface ConstructionStatusTVC ()

@end

@implementation ConstructionStatusTVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}






- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    //#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    Site* site=[appDele currentSite];
//    uint numRow=0;
//    if (site.aDepartment){
//        numRow=[site.aDepartment count];
//    }
//    if (numRow<1) {numRow=1;}
//    return numRow;
    return 3;
}





-(void)addToSelection:(NSIndexPath *) indexPath{
    uint row=[indexPath row];
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    Site* site=[appDele currentSite];
    //    Department* department=[site.aDepartment objectAtIndex:row];            
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    
    
    for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
            if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                Floor* floor=(Floor*)[viewProductRep product];
                Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                if ((bldg.existing+1)==row){                    
                    if (!viewProductRep.selected){
                        [viewModelVC addAProductRepToSelection:viewProductRep];
                    }
                }                
            }  
        }    
    }       
    

}


-(void)removeFromSelection:(NSIndexPath *) indexPath{
    
    uint row=[indexPath row];
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    
    
    for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
            if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                Floor* floor=(Floor*)[viewProductRep product];
                Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                if ((bldg.existing+1)==row){                    
                    if (viewProductRep.selected){                        
                        [viewModelVC removeAProductRepFromSelection:viewProductRep];                                                                        
                    }
                }                
            }  
        }    
    } 
    /*
    
    for (UIView* layerSubView in [displayModelUI.spatialStructViewLayer subviews]){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                Space* space=(Space*)[ viewProductRep product];
                
                if (space.deptID>0 && ((space.deptID-1)==row) ){
                    if (viewProductRep.selected){
                        [viewModelVC removeAProductRepFromSelection:viewProductRep];
                    }
                }
                
            }  
        }                
    }
    
    */
    
    
}


/*

-(void)removeFromSelection:(NSIndexPath *) indexPath{
    
    uint row=[indexPath row];
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    
   
//    
    for (UIView* layerSubView in [displayModelUI.spatialStructSlabViewLayer subviews]){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
            if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                Floor* floor=(Floor*)[viewProductRep product];
                Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                if ((bldg.existing+1)==row){                    
                    if (!viewProductRep.selected){
                        
                        if (viewProductRep.selected){
                            [viewModelVC removeAProductRepFromSelection:viewProductRep];                        
//                        [viewModelVC addAProductRepToSelection:viewProductRep];
                        }
                    }
                }                
            }  
        }    
    } 
    
//    for (UIView* layerSubView in [displayModelUI.spatialStructViewLayer subviews]){
//        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
//            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
//            if ([[viewProductRep product] isKindOfClass:[Space class]]){
//                Space* space=(Space*)[ viewProductRep product];
//                
//                if (space.deptID>0 && ((space.deptID-1)==row) ){
//                    if (viewProductRep.selected){
//                        [viewModelVC removeAProductRepFromSelection:viewProductRep];
//                    }
//                }
//                
//            }  
//        }
//        
//        
//    }
}  
   */ 
    
    

@end

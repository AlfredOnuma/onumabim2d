@protocol DownloaderDelegate;

#import <Foundation/Foundation.h>

@interface Downloader : NSObject {
    
    id <DownloaderDelegate> delegate;
    
    NSString* strFileNameWithPath;
    
    NSURLConnection* connection;
    
    CGFloat floatTotalData;
    CGFloat floatReceivedData;
}

@property(nonatomic, retain) NSString* strFileNameWithPath;
@property(nonatomic, retain) id <DownloaderDelegate> delegate;

-(void)loadData;
-(float)getProgressInPercent;

@end

@protocol DownloaderDelegate <NSObject>

@optional
-(void)didReceiveData:(Downloader *)d; //will be called each time data has been received
-(void)didLoadData:(Downloader *)d; //will be called when the download has finished
@end


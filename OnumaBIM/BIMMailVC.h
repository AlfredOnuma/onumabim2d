//
//  BIMMailVC.h
//  ProjectView
//
//  Created by Alfred Man on 4/4/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecipientCell.h"
//@class BIMMailRecipientCell;
@class ViewModelVC;
@class BIMMailImageView;
@class AROverlayViewController;
@interface BIMMailVC : UIViewController <UITableViewDelegate, UITableViewDataSource,NSURLConnectionDelegate, UIAlertViewDelegate, UIScrollViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,RecipientCellDelegate> {// <UISplitViewControllerDelegate>
    IBOutlet UITableView* recipientTable;
    NSMutableArray* aRecipientGroup;
    IBOutlet UITextView* contentTextView;
    IBOutlet UITextField* subjectTextField;
    ViewModelVC* viewModelVC;
    NSMutableData* connectionData;    
    NSURLConnection* theConnection;  
    
    IBOutlet UIScrollView* imgScrollView;
    
    UIPopoverController* imgLibraryPopOverController;
    IBOutlet UIButton* buttonImgLibrary;
    IBOutlet UIButton* buttonCamera;
    IBOutlet UIButton* buttonBin;
    
    NSMutableArray* aImg; 
    uint pImg;
    IBOutlet UILabel* labelImgStrs;
    NSMutableArray* aInsertImg;
    NSMutableArray* aDeleteImg;
    
    UIImageView* preLoadImgView;
    UIImageView* postLoadImgView;
    AROverlayViewController* _arOverlayViewController;
//    bool checked;
//    IBOutlet UITableViewCell* testCell;
//    IBOutlet BIMMailRecipientCell* bimMailRecipientCell;
//    NSMutableArray* aRecipientList;    
}
//@property (nonatomic, retain) IBOutlet UITableViewCell* testCell;
//@property (nonatomic, retain) IBOutlet BIMMailRecipientCell* bimMailRecipientCell;
@property (nonatomic, retain) AROverlayViewController* arOverlayViewController;
@property (nonatomic,assign) ViewModelVC* viewModelVC;
@property (nonatomic,retain) NSMutableArray* aInsertImg;
@property (nonatomic,retain) NSMutableArray* aDeleteImg;
@property (nonatomic,retain) UIImageView* preLoadImgView;
@property (nonatomic,retain) UIImageView* postLoadImgView;

@property (nonatomic, retain) IBOutlet UILabel* labelImgStrs;
@property (nonatomic, retain) NSMutableArray* aImg; 
@property (nonatomic, retain) UIPopoverController* imgLibraryPopOverController;

@property (nonatomic, retain) IBOutlet UIButton* buttonImgLibrary;
@property (nonatomic, retain) IBOutlet UIButton* buttonCamera;
@property (nonatomic, retain) IBOutlet UIButton* buttonBin;

@property (nonatomic, retain) IBOutlet UITextField* subjectTextField;
@property (nonatomic, retain) NSMutableData* connectionData;
@property (nonatomic, retain) NSURLConnection* theConnection;
@property (nonatomic, retain) IBOutlet UITextView* contentTextView;
@property (nonatomic, retain) IBOutlet UITableView *recipientTable;
@property (nonatomic, retain) NSMutableArray* aRecipientGroup;

@property (nonatomic, retain) IBOutlet UIScrollView* imgScrollView;

- (NSString*) generateImgFileNameByDate;

-(void) dealWithImageChosen:(UIImage*) image;
- (void) toDeleteImgArray:(BIMMailImageView*) imgView;
-(void) setupScrollView;
-(void)rebuildImgScrollView;

-(NSData*) uploadMail:(NSArray*) paramArray;
-(IBAction)sendMail:(id)sender;
-(void) executeDeleteImgViewFromStorageAndDB;

-(void) removeInsertImgArray;
- (IBAction)removeImage:(id) sender;
- (void)removeImage:(id) sender;
-(void) startUploadMail:(id)sender;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil viewModelVC:(ViewModelVC*) _viewModelVC;
//-(NSData *) startUploadMail:(id)sender event:(id)event;
@end

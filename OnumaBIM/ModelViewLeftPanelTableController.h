//
//  ModelViewLeftPanelTableController.h
//  OPSMobile
//
//  Created by Alfred Man on 9/30/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ModelViewLeftPanelTableController : UITableViewController {
    NSMutableArray* tableList;
}
@property (nonatomic, assign) NSMutableArray* tableList;

- (void) setTableList: (NSMutableArray*) newTableList;
@end

//
//  SpriteBitmap.h
//  ProjectView
//
//  Created by Alfred Man on 2/10/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewProductSprite.h"

@interface SpriteBitmap : ViewProductSprite{
    UIImage* image;
    CGRect imageFrame;
//    UIImageView* imageView;

}
@property (nonatomic, retain) UIImage* image;
@property (nonatomic, assign) CGRect imageFrame;
//@property (nonatomic, retain) UIImageView* imageView;
@end

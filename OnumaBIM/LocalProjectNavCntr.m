//
//  LocalProjectNavCntr.m
//  ProjectView
//
//  Created by Alfred Man on 11/22/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "LocalProjectNavCntr.h"
#import "ProjectViewAppDelegate.h"

@implementation LocalProjectNavCntr






- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self setCustomNavigationBarButtons];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    // Return YES for supported orientations
//    
//    // Return YES for supported orientations
//    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
//            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
//}



//// Tell the system what we support
- (NSUInteger)supportedInterfaceOrientations {
    //    return UIInterfaceOrientationMaskAllButUpsideDown;
    return UIInterfaceOrientationMaskLandscape;//UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskPortraitUpsideDown;//UIInterfaceOrientationMaskAllButUpsideDown;
}
//
// Tell the system It should autorotate
- (BOOL) shouldAutorotate {
    return YES;
    
}
// Tell the system which initial orientation we want to have
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft;
}

@end

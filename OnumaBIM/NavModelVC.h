//
//  NavModelVC.h
//  ProjectView
//
//  Created by Alfred Man on 11/2/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NavModelDelegate 
@end
@class OPSProduct;
@class ViewModelVC;
@interface NavModelVC : UITableViewController {
    int tapCount;
//    NSIndexPath* tableSelection;
    id <NavModelDelegate> delegate;
//    OPSProduct* selectedProduct;
}
@property (assign) id <NavModelDelegate> delegate;
@property (nonatomic, assign) int tapCount;
//@property (nonatomic, retain) NSIndexPath* tableSelection;
//@property (nonatomic, assign) OPSProduct* selectedProduct;

- (id) init: (ViewModelVC*) viewModelVC;
-(OPSProduct*) findSelectedProduct: (NSUInteger) selectedIndex;
-(void) singleTap:(NSNumber*) _row;
- (void)doubleTap:(NSNumber*) _row;
@end

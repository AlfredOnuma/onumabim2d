//
//  ProductViewToolbar.m
//  ProjectView
//
//  Created by Alfred Man on 11/24/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "ProductViewToolbar.h"
#import "ProjectViewAppDelegate.h"
//#import "ProjectViewAppDelegate.h"
#import "ViewModelNavCntr.h"
#import "ViewModelVC.h"
#import "OPSProduct.h"
#import "Site.h"
#import "Space.h"
#import "Bldg.h"
#import "OPSNavUI.h"
#import "Slab.h"
#import "Furn.h"
#import "NavFloorVC.h"
//#import "AttachmentInfo.h"
#import "AttachmentInfoTable.h"
#import "AttachmentInfoNVC.h"
@implementation ProductViewToolbar





- (void) issue:(id)sender{
    [self attachmentInfoTable:sender];
//    OPSNavUI* opsNavUI=[[self parentViewModelVC] opsNavUI];    
//    [opsNavUI displayAttachmentInfo];
}
/*
- (void) move:(id)sender{
    
}

- (void) rotate:(id)sender{
    
}

- (void) reshape:(id)sender{
    
}

- (void) lock:(id)sender{
    
}

- (void) copy:(id)sender{
    
}

- (void) remove:(id)sender{
    
}
*/

-(id) attachButton{    
    return [self.items objectAtIndex:3];     
}
- (void) edit:(id)sender{
    
}
//
//
//- (void) color:(id)sender{
//}

- (void) browse:(id)sender{    
    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//
//    ViewModelNavCntr* viewModelNavCntr=[appDele viewModelNavCntr] ;

    ViewModelVC* viewModelVC=[self parentViewModelVC];
//    OPSProduct* selectedProduct=[viewModelVC selectedProduct];
    OPSProduct* selectedProduct=[[viewModelVC selectedProductRep] product];    
    if (selectedProduct == nil) {return;}
    SpatialStructure* selectedSpatialStruct=[selectedProduct findParentSpatialStruct];


//    [appDele setModelDisplayLevel:([appDele modelDisplayLevel]+1)];    
//    OPSModel* model=[[OPSModel alloc] init];             
//    [model setupModelFromProject:[appDele activeProject] rootID:selectedSpatialStruct.ID auxiliaryID:0];            
//    [appDele displayViewModelNavCntr:model];  
    [appDele incrementDisplayLevel];
//    [viewModelVC release];
    [appDele displayViewModelNavCntr:selectedSpatialStruct];
    
//    NSString* name=[selectedSpatialStruct name];
    
    
    //    SpatialStructure* rootSpatialStruct=[[[appDele activeViewModelVC] 
    
    
}

- (void) refreshToolbar{
//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];        
//    ViewModelNavCntr* viewModelNavCntr=[appDele viewModelNavCntr] ;  
    ViewModelVC* viewModelVC=[self parentViewModelVC];
//    OPSProduct* selectedProduct=[viewModelVC selectedProduct];
    OPSProduct* selectedProduct=[[viewModelVC selectedProductRep] product];    
    if (selectedProduct == nil) {return;}
//    SpatialStructure* selectedSpatialStruct=[selectedProduct findParentSpatialStruct];
    UIBarButtonItem *selectedProductLabelItem=[self.items objectAtIndex:0];
    UILabel* label=(UILabel *) [selectedProductLabelItem view];

    

    
    
    
    [label setText:[self findSelectedProductRepDisplayStr]];
    /*
    UIBarButtonItem* browseButton=[self.items objectAtIndex:0]; //Browse
    if  (
         ([appDele modelDisplayLevel]==0&&[selectedSpatialStruct isKindOfClass:[Site class]] )||
         ([appDele modelDisplayLevel]==1 &&[selectedSpatialStruct isKindOfClass:[Bldg class]])||
         ([appDele modelDisplayLevel]==2 &&[selectedSpatialStruct isKindOfClass:[Space class]]) 
         ){
        browseButton.enabled=false;
    }else{
        browseButton.enabled=true;
    }
    */
}
-(void) enableButtons{
    UIBarButtonItem *item=[self.items objectAtIndex:0];
    item.enabled=true;
    
    //    item=[self.items objectAtIndex:1];
    //    item.enabled=true;
    
    item=[self.items objectAtIndex:2];
    item.enabled=true;
    
    
    item=[self.items objectAtIndex:3];
    item.enabled=true;
    
    item=[self.items objectAtIndex:4];
    item.enabled=true;
    
    
    
}

-(void) disableAllButtons{
    
    UIBarButtonItem *item=[self.items objectAtIndex:0];
    item.enabled=false;
    
    item=[self.items objectAtIndex:1];
    item.enabled=false;
    
    item=[self.items objectAtIndex:2];
    item.enabled=false;
    
    
    item=[self.items objectAtIndex:3];
    item.enabled=false;
    
    
    item=[self.items objectAtIndex:4];
    item.enabled=false;
    

}

-(NSString*) findSelectedProductRepDisplayStr{
    
    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
    
    ViewModelVC*  viewModelVC=[self parentViewModelVC];
    //        OPSProduct* selectedProduct=[viewModelVC selectedProduct];
    OPSProduct* selectedProduct=[[viewModelVC selectedProductRep] product];
    
    OPSProduct* refProductFromSelectedProduct=nil;
    NSString* displayStr=nil;
    switch ([appDele modelDisplayLevel]){
        case 0:
            if ([selectedProduct isKindOfClass:[Floor class]]){
                refProductFromSelectedProduct=(Bldg*) [[selectedProduct linkedRel]relating];
                displayStr=[NSString stringWithFormat:@"Bldg: %@",[refProductFromSelectedProduct name]];
            }else{
                refProductFromSelectedProduct=selectedProduct;
                
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];
            }
            break;
        case 1:
            if ([selectedProduct isKindOfClass:[Space class]]){
                refProductFromSelectedProduct=(Space*) selectedProduct;
                
                displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]];                     
                //                    refProductFromSelectedProduct=(Floor*) [[selectedProduct linkedRel]relating];                    
                //                    displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]];                    
            }else{
                refProductFromSelectedProduct=selectedProduct;                    
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];                    
            }
            break;                
        case 2:
            if ([selectedProduct isKindOfClass:[Furn class]]){
                refProductFromSelectedProduct=(Furn*) selectedProduct;
                displayStr=[NSString stringWithFormat:@"Equipment: %@",[refProductFromSelectedProduct name]];
            }else{
                refProductFromSelectedProduct=selectedProduct;
                
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];
            }
            break;        
        default:
            break;
    }
    return displayStr;
}
- (id)initWithFrame:(CGRect)frame viewModelVC:(ViewModelVC*)_viewModelVC{
    self=[super initWithFrame:frame viewModelVC:_viewModelVC];
    if (self){
        
        ViewModelVC*  viewModelVC=[self parentViewModelVC];
//        OPSProduct* selectedProduct=[viewModelVC selectedProduct];
        OPSProduct* selectedProduct=[[viewModelVC selectedProductRep] product];        
        if (selectedProduct == nil) { return self;}
        uint numElem=13;
        
        //        tools.clearsContextBeforeDrawing = NO;
        //        tools.clipsToBounds = NO;
        //        tools.tintColor = [UIColor colorWithWhite:0.575f alpha:0.0f]; // closest I could get by eye to black, translucent style.                   
        //        tools.tintColor = self.navigationController.navigationBar.tintColor;//[UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
        // anyone know how to get it perfect?
        //        tools.barStyle = -1; // clear background        
        NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:numElem];

        
        /*
         // Create a spacer.
         bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
         bi.width = 5.0f;
         [buttons addObject:bi];
         [bi release];
         */
        // Add profile button.
        UIBarButtonItem* bi=nil;        
                
        
//        bi = [[UIBarButtonItem alloc] initWithTitle:@"Color" style:UIBarButtonItemStylePlain target:self action:@selector(color:)];
//        bi.style = UIBarButtonItemStyleBordered;
//        bi.enabled=true;
//        [buttons addObject:bi];
//        [bi release];
//        
//        
        

        UILabel *selectedProductLabel= [[UILabel alloc] initWithFrame:CGRectMake(0.0 , 11.0f, 400.0f, 21.0f)];
        
//        [selectedProductLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
        
        [selectedProductLabel setFont:[UIFont boldSystemFontOfSize:18]];
        
        [selectedProductLabel setBackgroundColor:[UIColor clearColor]];
//        [selectedProductLabel setTextColor:[UIColor colorWithRed:157.0/255.0 green:157.0/255.0 blue:157.0/255.0 alpha:1.0]];
        
        [selectedProductLabel setTextColor:[UIColor colorWithRed:113.0/255.0 green:120.0/255.0 blue:128.0/255.0 alpha:1.0]];        
        [selectedProductLabel setText:[self findSelectedProductRepDisplayStr]];//[refProductFromSelectedProduct name]];
        [selectedProductLabel setTextAlignment:UITextAlignmentLeft];
        
        UIBarButtonItem *productLabelItem = [[UIBarButtonItem alloc] initWithCustomView:selectedProductLabel];
        [selectedProductLabel release];
        [buttons addObject:productLabelItem];
        [productLabelItem release];

        

        
        
        
        bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        bi.style = UIBarButtonItemStyleBordered;
        [buttons addObject:bi];
        [bi release];   
        
        
        
        

        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(edit:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=true;
        [buttons addObject:bi];
        [bi release];
        
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(attachmentInfoTable:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=true;
        [buttons addObject:bi];
        [bi release];

        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Browse" style:UIBarButtonItemStylePlain target:self action:@selector(browse:)];
        bi.style = UIBarButtonItemStyleBordered;
        
        
        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
        
        SpatialStructure* selectedSpatialStruct=[selectedProduct findParentSpatialStruct];
        if  (
             ([appDele modelDisplayLevel]==0&&[selectedSpatialStruct isKindOfClass:[Site class]] )||
             ([appDele modelDisplayLevel]==1 &&[selectedSpatialStruct isKindOfClass:[Bldg class]])||
             ([appDele modelDisplayLevel]==2 &&[selectedSpatialStruct isKindOfClass:[Space class]]) 
             ){
            bi.enabled=false;
        }else{
            bi.enabled=true;
        }
        [buttons addObject:bi];
        [bi release];        
        
        
        /*
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Move" style:UIBarButtonItemStylePlain target:self action:@selector(move:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=true;
        [buttons addObject:bi];
        [bi release];
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Rotate" style:UIBarButtonItemStylePlain target:self action:@selector(rotate:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=true;
        [buttons addObject:bi];
        [bi release];
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Reshape" style:UIBarButtonItemStylePlain target:self action:@selector(reshape:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=true;
        [buttons addObject:bi];
        [bi release];   
        
                        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Lock" style:UIBarButtonItemStylePlain target:self action:@selector(lock:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=true;
        [buttons addObject:bi];
        [bi release];  

        
        
        
        bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
        bi.width=13;
        bi.style = UIBarButtonItemStyleBordered;
        [buttons addObject:bi];
        [bi release];   
        
        
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Copy" style:UIBarButtonItemStylePlain target:self action:@selector(copy:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=true;
        [buttons addObject:bi];
        [bi release];  
        
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(remove:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=true;
        [buttons addObject:bi];
        [bi release];  
         
        */
  
        // Add buttons to toolbar and toolbar to nav bar.
        [self setItems:buttons animated:NO];
    
        [buttons release];   

    }
    return self;
}
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/
- (void)dealloc
{

    [super dealloc];
}
/*
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
*/
#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/
/*

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end

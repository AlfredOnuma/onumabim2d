//
//  TestUI4.m
//  ProjectView
//
//  Created by Alfred Man on 11/17/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//
#import "ProjectViewAppDelegate.h"
#import "ViewModelVC.h"
#import "NavUIScroll.h"
#import "TestUI4.h"
#import "BBox.h"

#import <QuartzCore/QuartzCore.h>

@implementation TestUI4
//-(UIView*) view{
//    return self;
//}
//@synthesize bBox;
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)dealloc
{
    [super dealloc];
}
//
//- (void)didReceiveMemoryWarning
//{
//    // Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//    
//    // Release any cached data, images, etc that aren't in use.
//}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
/*
- (void)viewDidLoad
{
    [super viewDidLoad];
    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
    double zoomScale=[[[appDele activeViewModelVC] navUIScrollVC] zoomScale];
    double frameWidth=self.frame.size.width;
    double frameHeight=self.frame.size.height;
    
    
    
    double wid=900/zoomScale;//frameWidth;
    double hit=300/zoomScale;//frameHeight;
    double offsetX=-wid*0.25;//-frameWidth/2.0;//0;
    double offsetY=0;//-hit;  
    
    
    
    //    [self setTransform:CGAffineTransformMakeTranslation(-offsetX, offsetY)];
    //        [self setBounds:CGRectMake(offsetX, offsetY, wid, hit)];        
    [self setBounds:CGRectMake(0,0, wid  , hit)];     
}
*/
//-(void) viewWillAppear{
//    [super viewWillAppear];
//    
//
//        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//        double zoomScale=[[[appDele activeViewModelVC] navUIScrollVC] zoomScale];
//        double frameWidth=self.frame.size.width;
//        double frameHeight=self.frame.size.height;
//        
//        
//        
//        double wid=900/zoomScale;//frameWidth;
//        double hit=300/zoomScale;//frameHeight;
//        double offsetX=-wid*0.25;//-frameWidth/2.0;//0;
//        double offsetY=0;//-hit;  
//        
//        
//        
//        //    [self setTransform:CGAffineTransformMakeTranslation(-offsetX, offsetY)];
////        [self setBounds:CGRectMake(offsetX, offsetY, wid, hit)];        
//        [self setBounds:CGRectMake(0,0, wid  , hit)];    
//}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
//- (void)drawRect:(NSRect)rect
//{
//    CGContextRef myContext = [[NSGraphicsContext // 1
//                               currentContext] graphicsPort];
//    // ********** Your drawing code here ********** // 2
//    CGContextSetRGBFillColor (myContext, 1, 0, 0, 1);// 3
//    CGContextFillRect (myContext, CGRectMake (0, 0, 200, 100 ));// 4
//    CGContextSetRGBFillColor (myContext, 0, 0, 1, .5);// 5
//    CGContextFillRect (myContext, CGRectMake (0, 0, 100, 200));// 6
//}

/*

- (void)drawRect:(CGRect)rect{
//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//    double zoomScale=[[[appDele activeViewModelVC] navUIScrollVC] zoomScale];
//    double frameWidth= self.frame.size.width;
//    double frameHeight=self.frame.size.height;
    
    

    double wid=1800;//900/zoomScale;//frameWidth;
    double hit=600;//300/zoomScale;//frameHeight;
    
    
    double offsetX=-wid*0.25;//-frameWidth/2.0;//0;
    double offsetY=-hit*0.25;//-hit;  

    
    BBox* bBox=[[BBox alloc] initWithCGRect:CGRectMake(offsetX,offsetY,wid,hit)]; 
    
//    
//    CGAffineTransform anchorTransform=CGAffineTransformTranslate(self.transform,bBox.minX,-1*(bBox.minY+[bBox getHeight]));    
//    CGAffineTransform flipXTransform=CGAffineTransformScale(anchorTransform, 1, -1);
//    [self setTransform:flipXTransform];
//    

    
//    CGAffineTransform anchorTransform=CGAffineTransformTranslate(self.transform,bBox.minX,1*(bBox.minY));    
////    CGAffineTransform flipXTransform=CGAffineTransformScale(anchorTransform, 1, -1);
//    [self setTransform:anchorTransform];
    
    
    
    CGMutablePathRef path = CGPathCreateMutable();  
    
    
    
    
    CGPathMoveToPoint(path,NULL,offsetX+0,offsetY+0);  
    CGPathAddLineToPoint(path,NULL,0,offsetY+0);      
    CGPathAddLineToPoint(path,NULL,0,0);      


    CGPathMoveToPoint(path,NULL,0,0);  
    CGPathAddLineToPoint(path,NULL,offsetX+wid,0);      
    CGPathAddLineToPoint(path,NULL,offsetX+wid,offsetY+hit);      
    

    
//    CGPathMoveToPoint(path,NULL,offsetX+0,offsetY+0);      
//    CGPathAddLineToPoint(path,NULL,offsetX+wid,offsetY+0);    
//    //    CGPathAddLineToPoint(path,NULL,offsetX+wid,offsetY+hit);    
//    CGPathAddLineToPoint(path,NULL,offsetX+wid,offsetY+hit);
//    
    
//    CGPathMoveToPoint(path,NULL,offsetX+0,offsetY+0);  
//    
//    CGPathAddLineToPoint(path,NULL,offsetX+wid,offsetY+0);
//    
////    CGPathAddLineToPoint(path,NULL,offsetX+wid,offsetY+hit);
//    
//    CGPathAddLineToPoint(path,NULL,offsetX+wid/2,offsetY+hit);
    //    CGPathAddLineToPoint(path,NULL,0,400);
    
    
    
    
    NSLog(@"DrawOffsetX: %f DrawOffsetY: %f  drawWid: %f drawHit: %f",offsetX, offsetY, wid, hit);
    
    
    CGContextRef context0 = UIGraphicsGetCurrentContext();             
//    CGAffineTransform t0 = CGContextGetCTM(context0);
//    t0 = CGAffineTransformInvert(t0);
//    CGContextConcatCTM(context0,t0);
//    
//    
//    CGAffineTransform viewTranslate=CGAffineTransformTranslate(CGContextGetCTM(context0), -offsetX, -offsetY);
//    [self setTransform:viewTranslate];
    
    
    CGContextBeginPath(context0);
    CGContextAddPath(context0,path);
    
    CGContextSetRGBStrokeColor(context0, 1.0, 1.0, 1.0, 1.0);
    CGContextSetRGBFillColor(context0, 1.0, 0.0, 0.0, 0.6);    
    
    CGContextSetLineWidth(context0, 5.0);
    
    CGContextClosePath(context0);     
    CGContextFillPath(context0);

//    
//    
//    
//    CGAffineTransform anchorTransform=CGAffineTransformTranslate(self.transform,bBox.minX,1*(bBox.minY+[bBox getHeight]));
//    [self setTransform:anchorTransform];
//    CGAffineTransform anchorTransform=CGAffineTransformTranslate(self.transform,0,0);//-[bBox getHeight]);    
    //    [self setTransform:anchorTransform];
    return;
    
}
 */
/*
-(id) initWithFrame:(CGRect)frame{
    [super initWithFrame:frame];
    if (self){
        self.backgroundColor=[UIColor cyanColor];
        self.alpha=0.7;
        
        
        double wid=1800;//900/zoomScale;//frameWidth;
        double hit=600;//300/zoomScale;//frameHeight;
        double offsetX=-wid*0.25;//-frameWidth/2.0;//0;
        double offsetY=-hit*0.25;//-hit;  
        
        
        BBox* bBox=[[BBox alloc] initWithCGRect:CGRectMake(offsetX,offsetY,wid,hit)];           
//        [self setFrame:CGRectOffset(frame, bBox.minX,bBox.minY)];
//        CGAffineTransform anchorTransform=CGAffineTransformTranslate(self.transform,bBox.minX,-bBox.minY-[bBox getHeight]);
//        [self setTransform:anchorTransform];
        
        
//        CGAffineTransform anchorTransform=CGAffineTransformTranslate(self.transform,bBox.minX,-bBox.minY-[bBox getHeight]);
//        [self setTransform:anchorTransform];
        
        
//        CGAffineTransform flipXTransform=CGAffineTransformScale(anchorTransform, 1, -1);
//        [self setTransform:flipXTransform];

        
        
        [self setBounds:CGRectOffset(self.bounds,bBox.minX,bBox.minY)];
//        [self setTransform:CGAffineTransformMakeScale(self.transform, 1.0, -1.0)];
        
//        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//        double zoomScale=[[[appDele activeViewModelVC] navUIScrollVC] zoomScale];
//        double frameWidth=self.frame.size.width;
//        double frameHeight=self.frame.size.height;
//        
//        
//        
//        double wid=900/zoomScale;//frameWidth;
//        double hit=300/zoomScale;//frameHeight;
//        double offsetX=-wid*0.25;//-frameWidth/2.0;//0;
//        double offsetY=0;//-hit;  
//        
//        
//        
//        //    [self setTransform:CGAffineTransformMakeTranslation(-offsetX, offsetY)];
////        [self setBounds:CGRectMake(offsetX, offsetY, wid, hit)];        
//        [self setBounds:CGRectMake(0,0, wid  , hit)];
    }
    return self;
}
 */
@end

//
//  UnitStruct.h
//  ProjectView
//
//  Created by Alfred Man on 6/12/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OPSProjectSite;
@interface UnitStruct : NSObject
//
//NSString* currency;
//bool isImperial;
{
    NSString* _currency;
    NSString* _currencyUnit;
    NSString* _measureUnitLength;
    NSString* _measureUnitArea;
    bool _isImperial;
}
@property (nonatomic, retain) NSString* currency;
@property (nonatomic, retain) NSString* currencyUnit;
@property (nonatomic, retain) NSString* measureUnitLength;
@property (nonatomic, retain) NSString* measureUnitArea;
@property (nonatomic, assign) bool isImperial;

//-(id) initWithProjectSite:(OPSProjectSite*) projectSite;
-(id) initWithCurrency:(NSString*) currency isImperial:(bool) isImperial ;

-(id) initWithDBPath:(NSString*) dbPath;

@end

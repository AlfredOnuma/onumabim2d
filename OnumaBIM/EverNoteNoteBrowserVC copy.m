//
//  EverNoteNoteBrowserVC.m
//  ProjectView
//
//  Created by onuma on 31/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//
#import "ProjectViewAppDelegate.h"
#import "EverNoteNoteBrowserVC.h"
#import "EvernoteSDK.h"
#import "ENMLUtility.h"
#import "EverNoteGlassNote.h"

#import "OPSProjectSite.h"
#import "EverNoteNoteBookBrowserTVC.h"
@interface EverNoteNoteBrowserVC ()

@end

@implementation EverNoteNoteBrowserVC
@synthesize notebookGUID=_notebookGUID;
@synthesize btnPrev=_btnPrev;
@synthesize btnNext=_btnNext;
@synthesize webView=_webView;
@synthesize bFilteredNoteOnly=_bFilteredNoteOnly;
@synthesize currentNote=_currentNote;
@synthesize activityIndicator=_activityIndicator;
@synthesize noteList=_notelist;
@synthesize buttonID_linkButton=_buttonID_linkButton;

@synthesize buttonID_linkFilter=_buttonID_linkFilter;
@synthesize buttonID_bookList=_buttonID_bookList;
@synthesize buttonID_noteList=_buttonID_noteList;

@synthesize popOverController=_popOverController;
@synthesize selectedNotebook=_selectedNotebook;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil noteBookGUID:(NSString*)notebookGUID bFilteredNoteOnly:(bool)bFilteredNoteOnly{
    self=[super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        self.notebookGUID=notebookGUID;
        self.bFilteredNoteOnly=bFilteredNoteOnly;
        [self addEverNoteNoteBrowserButtonToNavBar];
        
    }
    return self;
}



-(void) dealloc{
    [_selectedNotebook release];_selectedNotebook=nil;
    [_popOverController release];_popOverController=nil;
    [_notebookGUID release];_notebookGUID=nil;
    [_activityIndicator release];_activityIndicator=nil;
    [_notelist release];_notelist=nil;
    [_btnNext release]; _btnNext=nil;
    [_btnPrev release];_btnPrev=nil;
    [_webView release];_webView=nil;
    [super dealloc];
}

//- (bool) checkHasNote:(sqlite3*)database{
//    bool exist=false;
//    sqlite3_stmt *selectStmt;
//    const char* selectSQL=[[NSString stringWithFormat:@"SELECT ID from ENNote"] UTF8String];
//    
//    if(sqlite3_prepare_v2 (database, selectSQL, -1, &selectStmt, NULL) == SQLITE_OK) {
//        while(sqlite3_step(selectStmt) == SQLITE_ROW) {
//            exist=true;
//            break;
//        }
//    }
//    sqlite3_finalize(selectStmt);
//    return exist;
//    
//}


- (uint) numberOfLinkedNote:(sqlite3*)database{
    uint sum=0;
    sqlite3_stmt *selectStmt;
    const char* selectSQL=[[NSString stringWithFormat:@"SELECT count(ID) from ENNote"] UTF8String];
    
    if(sqlite3_prepare_v2 (database, selectSQL, -1, &selectStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(selectStmt) == SQLITE_ROW) {            
            NSInteger sumInt = sqlite3_column_int(selectStmt, 0);
            sum=(uint)sumInt;
            break;
        }
    }
    sqlite3_finalize(selectStmt);
    return sum;
    
}
- (bool) checkNoteExist: (sqlite3*)database noteGUID:(NSString*)noteGUID notebookGUID:(NSString*)notebookGUID{
    //    uint attachCommentID=0;
    
    
    bool exist=false;
    sqlite3_stmt *selectStmt;
    const char* selectSQL=[[NSString stringWithFormat:@"SELECT ID,GUID, notebookID from ENNote where GUID=\"%@\" AND notebookID=\"%@\"",noteGUID,notebookGUID] UTF8String];
        
    if(sqlite3_prepare_v2 (database, selectSQL, -1, &selectStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(selectStmt) == SQLITE_ROW) {
            exist=true;
            break;
        }
    }
    sqlite3_finalize(selectStmt);
    return exist;
    
    
    
    
}



- (void) removeENNoteFromDB: (sqlite3*)database noteGUID:(NSString*)noteGUID notebookGUID:(NSString*)notebookGUID{
    //    uint attachCommentID=0;
    

    sqlite3_stmt *removeStmt;
    const char* removeSQL=[[NSString stringWithFormat:@"DELETE FROM ENNote where GUID=\"%@\" AND notebookID=\"%@\"",
                            noteGUID,notebookGUID] UTF8String];
    
    
    if(sqlite3_prepare_v2 (database, removeSQL, -1, &removeStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(removeStmt) == SQLITE_ROW) {
        }
    }
    sqlite3_finalize(removeStmt);
    
    
    
}




- (void) insertENNoteToDB: (sqlite3*)database noteGUID:(NSString*)noteGUID notebookGUID:(NSString*)notebookGUID content:(NSString*)content{
    //    uint attachCommentID=0;
    
    
    
    
    sqlite3_stmt *insertStmt;
    const char* insertSQL=[[NSString stringWithFormat:@"INSERT INTO ENNote (\"GUID\",\"notebookID\",\"content\") VALUES (\"%@\",\"%@\",\"%@\")",
                            noteGUID,notebookGUID,content] UTF8String];
    
    
    if(sqlite3_prepare_v2 (database, insertSQL, -1, &insertStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(insertStmt) == SQLITE_ROW) {
        }
    }
    sqlite3_finalize(insertStmt);
    
    
    
    //    const char* getLastIDSQL=[[NSString stringWithFormat:@"SELECT last_insert_rowid()"] UTF8String];
    //
    //    sqlite3_stmt *getLastIDStmt;
    //    if(sqlite3_prepare_v2 (database, getLastIDSQL, -1, &getLastIDStmt, NULL) == SQLITE_OK) {
    //        while(sqlite3_step(getLastIDStmt) == SQLITE_ROW) {
    //            attachCommentID=sqlite3_column_int(getLastIDStmt, 0);
    //        }
    //    }
    //    sqlite3_finalize(getLastIDStmt);
    //
    //    for (AttachmentImageView* imageView in aImg){
    //
    //
    //        sqlite3_stmt *attachImgStmt;
    //
    //        const char* insertAttachImgSql=[[NSString stringWithFormat:@"INSERT INTO AttachFile (\"attachCommentID\",\"fileName\",\"fileTitle\",\"attachedTo\",\"referenceID\",\"uploadDate\") VALUES (%d,\"%@\",\"%@\",\"%@\",%d,\"%@\")",
    //                                         attachCommentID,
    //                                         [[[imageView path] lastPathComponent] substringFromIndex:3],
    //                                         @"",
    //                                         self.attachmentProductType,
    //                                         self.attachmentProductID,// attachedProduct.ID,
    //                                         attachmentDate  ] UTF8String];
    //        //        NSString* test=[NSString stringWithUTF8String:insertAttachCommentSql];
    //        if(sqlite3_prepare_v2 (database, insertAttachImgSql, -1, &attachImgStmt, NULL) == SQLITE_OK) {
    //            while(sqlite3_step(attachImgStmt) == SQLITE_ROW) {
    //                //                    int t=0;
    //                //                    t=1;
    //
    //            }
    //        }
    //        sqlite3_finalize(attachImgStmt);
    //        
    //        
    //    }
    //    
    
    
}
-(void) notesList{
    
}

-(void) booksList:(id) sender{
//    if (self.popOverController!=nil){
//        [self.popOverController dismissPopoverAnimated:NO];
//        self.popOverController=nil;
//    }
    
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    //    Bldg* bldg=[[[appDele activeViewModelVC] model] root];
//    Bldg* bldg=(Bldg*) [[self model] root];
    
    //    NavPlanNavCntr* navPlanNavCntr=[[NavPlanNavCntr alloc] init];
    
    EverNoteNoteBookBrowserTVC* evernoteNoteBookBrowserTVC=[[EverNoteNoteBookBrowserTVC alloc] init:self];//init:self];//initWithNibName:@"NavFloorVC" bundle:Nil];
//    [navFloorVC setSelectedBldg:bldg];
    
    NSString* title=[NSString stringWithFormat:@"Books"];
    evernoteNoteBookBrowserTVC.title=title;
    
    
    
//    uint numCell=[[[bldg relAggregate] related] count];
//    if (numCell>14){ numCell=14;}
//    
    uint numCell=14;
    evernoteNoteBookBrowserTVC.contentSizeForViewInPopover=CGSizeMake(360, (numCell*44) );
    
    
    //    [navPlanNavCntr pushViewController:navFloorVC animated:NO];
    //            MyViewController * myViewController = [[[MyViewController alloc] init] autorelease];
    //            myViewController.previousViewController = self;
    UIPopoverController* evernoteNoteBookBrowserPopOverController=[[UIPopoverController alloc]
                                                     initWithContentViewController:evernoteNoteBookBrowserTVC];
    self.popOverController = evernoteNoteBookBrowserPopOverController;// autorelease];
    [evernoteNoteBookBrowserPopOverController release];
    
    [evernoteNoteBookBrowserTVC release];
    
    if ([sender isKindOfClass:[UIButton class]]){
        //       sender= [((UIButton*)sender) ]
        UIButton* button=sender;
        //        sender=[((UIButton*) sender) superview];
        [self.popOverController presentPopoverFromRect:button.bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES ];
    }else{
        [self.popOverController presentPopoverFromBarButtonItem:sender
                                       permittedArrowDirections:UIPopoverArrowDirectionUp
                                                       animated:YES];
    }
    
    //    [popoverController release];
    
    
}

-(void) setSearchLinkedNoteOnlyButtonIcon{
    
    NSArray* buttons =self.navigationItem.rightBarButtonItems;
    UIBarButtonItem *bi=[buttons objectAtIndex:self.buttonID_linkFilter];
    UIButton* button=(UIButton*)[bi customView];
    
    UIImage* buttonImage=nil;
    UIImage *buttonImageHighlight=nil;
    
    if (self.bFilteredNoteOnly){
        buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"searchLinkedOnly.png" ofType:nil]];
        buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"searchLinkedOnly_HighLight.png" ofType:nil]];
    }else{
        buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"searchLinkedOnlyGrey.png" ofType:nil]];
        buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"searchLinkedOnlyGrey.png" ofType:nil]];
    }
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
    
}
-(void) setSearchLinkedNoteOnly{
    self.bFilteredNoteOnly=!self.bFilteredNoteOnly;
    [self setSearchLinkedNoteOnlyButtonIcon];
}

-(void) linkNoteGUID{
    EDAMNoteMetadata* foundNote = self.noteList[self.currentNote];

    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;
    
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
        if ([self checkNoteExist:database noteGUID:foundNote.guid notebookGUID:foundNote.notebookGuid]){
            [self removeENNoteFromDB:database noteGUID:foundNote.guid notebookGUID:foundNote.notebookGuid];
            [self setLinkButtonIcon:FALSE];
        }else{
            [self insertENNoteToDB:database noteGUID:foundNote.guid notebookGUID:foundNote.notebookGuid content:nil];
            [self setLinkButtonIcon:TRUE];
        }
    }
    sqlite3_close(database);
    
    
//    
//    
//    
//    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
//        if ([self checkNoteExist:database noteGUID:foundNote.guid notebookGUID:foundNote.notebookGuid]){
//            [self setLinkButtonIcon:TRUE];
//        }else{
//            [self setLinkButtonIcon:FALSE];
//        }
//    }
//    sqlite3_close(database);
    
    
//
//    EverNoteGlassNote* glassNote=[[EverNoteGlassNote alloc] initWithNoteBookGUID:foundNote.notebookGuid noteGUID:foundNote.guid];
//    [appDele addEverNoteLinkedNote:glassNote];
//    [glassNote release];

}
//-(bool) isCurrentNoteLinked{
//    bool isLinked=false;
//    
//    
//    
//    return isLinked;
//}

-(void) setLinkButtonIcon:(BOOL)isLinked{
    
//    bool isCurrentNoteLinked=[self isCurrentNoteLinked];
    NSArray* buttons =self.navigationItem.rightBarButtonItems;
    UIBarButtonItem *bi=[buttons objectAtIndex:self.buttonID_linkButton];
    UIButton* button=(UIButton*)[bi customView];
    
    UIImage* buttonImage=nil;
    UIImage *buttonImageHighlight=nil;
    
    if (isLinked){
        buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"link_lock Black 44.png" ofType:nil]];
        buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"link_lock Black 44_Highlighted.png" ofType:nil]];
    }else{
        buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"broken_link_cancel_Grey44.png" ofType:nil]];
        buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"broken_link_cancel_Grey44_Highlighted.png" ofType:nil]];
    }
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];


    
}
-(void) addEverNoteNoteBrowserButtonToNavBar{
    
    NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:4];
    UIImage *buttonImage=nil;
    UIImage *buttonImageHighlight=nil;
    UIButton *button=nil;
    UIBarButtonItem *bi=nil;
    uint pButton=0;
    

    

    
    
    buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"broken_link_cancel_Grey44.png" ofType:nil]];
    buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"broken_link_cancel_Grey44_Highlighted.png" ofType:nil]];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(linkNoteGUID) forControlEvents:UIControlEventTouchUpInside];
    bi = [[UIBarButtonItem alloc] initWithCustomView:button];
    [buttons addObject:bi];
    self.buttonID_linkButton=pButton;
    pButton++;
    
    

    
    self.bFilteredNoteOnly=false;
    if (self.bFilteredNoteOnly){
        buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"searchLinkedOnly.png" ofType:nil]];
        buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"searchLinkedOnly_HighLight.png" ofType:nil]];
    }else{
        buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"searchLinkedOnlyGrey.png" ofType:nil]];
        buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"searchLinkedOnlyGrey.png" ofType:nil]];
    }
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(setSearchLinkedNoteOnly) forControlEvents:UIControlEventTouchUpInside];
    bi = [[UIBarButtonItem alloc] initWithCustomView:button];

    [buttons addObject:bi];
    self.buttonID_linkFilter=pButton;
    pButton++;
    
    
    
    buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"notesList.png" ofType:nil]];
    buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"notesList_Highlight.png" ofType:nil]];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(notesList) forControlEvents:UIControlEventTouchUpInside];
    bi = [[UIBarButtonItem alloc] initWithCustomView:button];
    [buttons addObject:bi];
    self.buttonID_noteList=pButton;
    pButton++;
    
    buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"bookList.png" ofType:nil]];
    buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"bookList_Highlight.png" ofType:nil]];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(booksList:) forControlEvents:UIControlEventTouchUpInside];
    bi = [[UIBarButtonItem alloc] initWithCustomView:button];
    [buttons addObject:bi];
    self.buttonID_bookList=pButton;
    pButton++;
    
    
    
//    buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"broken_link_cancel_Grey44.png" ofType:nil]];
//    buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"broken_link_cancel_Grey44_Highlighted.png" ofType:nil]];
//    button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button setImage:buttonImage forState:UIControlStateNormal];
//    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
//    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//    [button addTarget:self action:@selector(linkNoteGUID) forControlEvents:UIControlEventTouchUpInside];
//    bi = [[UIBarButtonItem alloc] initWithCustomView:button];
//    [buttons addObject:bi];
//    
//    
    [bi release];
    
    
    self.navigationItem.rightBarButtonItems=buttons;
    
    [buttons release];
    
    
    //    NSMutableArray* leftButtons = [[NSMutableArray alloc] initWithCapacity:1];
    //
    //    buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ProjectListsButton.png" ofType:nil]];//[UIImage imageNamed:@"ProjectListsButton.png"];
    //    buttonImageHighlight = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ProjectListsButton.png" ofType:nil]];;//[UIImage imageNamed:@"ProjectListsButton.png"];
    //    button = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [button setImage:buttonImage forState:UIControlStateNormal];
    //    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
    //    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    //    [button addTarget:self action:@selector(backToProjectButtonRespond:) forControlEvents:UIControlEventTouchUpInside];
    //    bi = [[UIBarButtonItem alloc] initWithCustomView:button];
    //    //        [button release];
    //    [leftButtons addObject:bi];
    //    [bi release];
    //
    //    self.navigationItem.leftBarButtonItems = leftButtons;
    //    [leftButtons release];
    //
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.btnPrev setEnabled:NO];
    [self.btnNext setEnabled:YES];
    UIActivityIndicatorView* activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator=activityIndicator;
    [activityIndicator release];
    CGRect viewRect = self.webView.frame;
    [self.activityIndicator setFrame:CGRectMake(viewRect.size.width/2, viewRect.size.height/2, 20, 20)];
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.webView addSubview:self.activityIndicator];
    if (self.bFilteredNoteOnly){
        [self loadLinkedNotes];
    }else{
        [self loadMoreNotes];
    }
    
    EDAMNoteMetadata* foundNote = self.noteList[self.currentNote];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    OPSProjectSite* projectSite=appDele.activeProjectSite;
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
        bool isCurrentNoteLinked=[self checkNoteExist:database noteGUID:foundNote.guid notebookGUID:foundNote.notebookGuid];
        [self setLinkButtonIcon:isCurrentNoteLinked];
    }
    sqlite3_close(database);
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [self setBtnPrev:nil];
    [super viewDidUnload];
}

- (IBAction)nextNote:(id)sender {
//    if(self.currentNote%10==0) {
//        self.currentNote++;
//        [self loadMoreNotes];
//    }
//    else {
    
    
    
    if([self.noteList count] <= self.currentNote) {
        return;
    }
    
    
    
        self.currentNote++;
        [self loadCurrentNote];
//    }
    if(self.currentNote > 0) {
        [self.btnPrev setEnabled:YES];
    }
}

- (IBAction)previousNote:(id)sender {
    self.currentNote--;
    [self loadCurrentNote];
    if(self.currentNote==0) {
        [self.btnPrev setEnabled:NO];
    }
}
-(void) addNoteToNoteList:(EDAMNote*)note{
    if (self.noteList==nil){
        NSMutableArray* noteList=[[NSMutableArray alloc]init];
        self.noteList=noteList;
        [noteList release];noteList=nil;
    }
    [self.noteList addObject:note];
}





//NSFileManager *fileManager = [NSFileManager defaultManager];
//ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//OPSProjectSite* projectSite=appDele.activeProjectSite;
//
//sqlite3 * database=nil;
//if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
//    [self deleteAnImgViewFromStorageAndDB:imgView database:database filemanager:fileManager];
//}
//sqlite3_close(database);
//

//
//
//-(void) executeDeleteImgViewFromStorageAndDB{
//    if (aDeleteImg==nil||[aDeleteImg count]<=0) {return;}
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    
//    
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    OPSProjectSite* projectSite=appDele.activeProjectSite;
//    
//    sqlite3 * database=nil;
//    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
//        
//        for (AttachmentImageView* imgView in aDeleteImg){
//            [self deleteAnImgViewFromStorageAndDB:imgView database:database filemanager:fileManager];
//        }
//        
//    }
//    sqlite3_close(database);
//}
//
//
//-(void) deleteAnImgViewFromStorageAndDB:(AttachmentImageView*)imgView database:(sqlite3*)database filemanager:(NSFileManager*) fileManager{
//    NSString* slImagePath=imgView.path;
//    [fileManager removeItemAtPath: slImagePath error:NULL];
//    
//    
//    NSString* fullImgDirectory=[imgView.path stringByDeletingLastPathComponent];
//    NSString* fullImgPathLastComponent=[[imgView.path lastPathComponent] substringFromIndex:3];
//    
//    NSString* fullImagePath=[[NSString alloc] initWithFormat:@"%@/%@",fullImgDirectory,fullImgPathLastComponent];
//    [fileManager removeItemAtPath: fullImagePath error:NULL];
//    [fullImagePath release];
//    
//    
//    NSString* tbImagePath=[[NSString alloc] initWithFormat:@"%@/tb_%@",fullImgDirectory,fullImgPathLastComponent];
//    //            NSLog( @"%@",tbImagePath);
//    [fileManager removeItemAtPath: tbImagePath error:NULL];
//    [tbImagePath release];
//    
//    
//    
//    //
//    //            NSString* viewImagePath=[[NSString alloc] initWithFormat:@"%@/view_%@",fullImgDirectory,fullImgPathLastComponent];
//    ////            NSLog( @"%@",viewImagePath);
//    //            [fileManager removeItemAtPath: viewImagePath error:NULL];
//    //            [viewImagePath release];
//    
//    
//    
//    if (imgView.attachFileID!=0){
//        sqlite3_stmt *deleteAttachFileStmt;
//        const char* deleteAttachFileSql=[[NSString stringWithFormat:@"delete from AttachFile where ID=%d",
//                                          imgView.attachFileID] UTF8String];
//        
//        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
//        if(sqlite3_prepare_v2 (database, deleteAttachFileSql, -1, &deleteAttachFileStmt, NULL) == SQLITE_OK) {
//            while(sqlite3_step(deleteAttachFileStmt) == SQLITE_ROW) {
//                
//            }
//        }
//        sqlite3_finalize(deleteAttachFileStmt);
//        
//    }
//    
//    
//}
//

-(void) loadLinkedNotes{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;
    
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
        if ([self numberOfLinkedNote:database]>0){
            
            [[self activityIndicator] startAnimating];
            [self loadNoteFromDatabase:database];
//            [self insertENNoteToDB:database noteGUID:foundNote.guid notebookGUID:foundNote.notebookGuid content:nil];
            [[self activityIndicator] stopAnimating];
        }
    }
    sqlite3_close(database);
    
    
    
}

- (bool) loadNoteFromDatabase: (sqlite3*)database{
    bool exist=false;
    //    uint attachCommentID=0;
    /*
    ProjectViewAppDelegate* appDele=[(ProjectViewAppDelegate*)[UIApplication sharedApplication] delegate];
   
    sqlite3_stmt *selectStmt;
    
    const char* selectSQL=[[NSString stringWithFormat:@"SELECT ID,GUID, notebookGUID from ENNote"] UTF8String];
    
    if(sqlite3_prepare_v2 (database, selectSQL, -1, &selectStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(selectStmt) == SQLITE_ROW) {

            NSString* noteGUID=[ProjectViewAppDelegate readNameFromSQLStmt:selectStmt column:1];
            NSString* notebookGUID=[ProjectViewAppDelegate readNameFromSQLStmt:selectStmt column:2];
            if (notebookGUID!=nil && [notebookGUID isEqualToString:self.notebookGUID]){
                
                [[EvernoteNoteStore noteStore] getNoteWithGuid:noteGUID withContent:NO withResourcesData:NO withResourcesRecognition:NO withResourcesAlternateData:NO success:^(EDAMNote *note) {
                    //            if (note.notebookGuid==self.notebookGUID){
                    [self addNoteToNoteList:note];
                    uint sumLinkedNote=[self numberOfLinkedNote:database];
                    
                    
                    if (pNote==[[appDele aEverNoteLinkedNote] count]-1){
                        [self loadCurrentNote];
                        [[self activityIndicator] stopAnimating];
                        
                        if ([self currentNote]<[self.noteList count]-1){
                            self.btnNext.enabled=YES;
                        }else{
                            self.btnNext.enabled=NO;
                        }
                    }
                    
        
                } failure:^(NSError *error) {
                    NSLog(@"Failed to get note : %@",error);
                    //            [[self activityIndicator] stopAnimating];
                }];
                pNote++;
            
            

            }
        }
    }
    sqlite3_finalize(selectStmt);

    
    */
    
    return exist;
}

- (void)loadLinkedNotes0 {
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [UIApplication sharedApplication].delegate;
    if (appDele.aEverNoteLinkedNote==Nil||[appDele.aEverNoteLinkedNote count]<=0){
        return;
    }
    
    [[self activityIndicator] startAnimating];
    uint pNote=0;
    for (EverNoteGlassNote* glassNote in [appDele aEverNoteLinkedNote]){

        if (self.notebookGUID!=nil && ![self.notebookGUID isEqualToString: glassNote.notebookGUID]){
            pNote++;
            continue;
        }
//        
        [[EvernoteNoteStore noteStore] getNoteWithGuid:glassNote.noteGUID withContent:NO withResourcesData:NO withResourcesRecognition:NO withResourcesAlternateData:NO success:^(EDAMNote *note) {
//            if (note.notebookGuid==self.notebookGUID){
                [self addNoteToNoteList:note];
                if (pNote==[[appDele aEverNoteLinkedNote] count]-1){
                    [self loadCurrentNote];
                    [[self activityIndicator] stopAnimating];
                    
                    if ([self currentNote]<[self.noteList count]-1){
                        self.btnNext.enabled=YES;
                    }else{
                        self.btnNext.enabled=NO;
                    }
                }
                
//            }

//            ENMLUtility *utltility = [[ENMLUtility alloc] init];
//            [utltility convertENMLToHTML:note.content withResources:note.resources completionBlock:^(NSString *html, NSError *error) {
//                if(error == nil) {
//                    [self.webView loadHTMLString:html baseURL:nil];
//                    NSLog (@"%@",html);
//                    [[self activityIndicator] stopAnimating];
//                }
//            }];
        } failure:^(NSError *error) {
            NSLog(@"Failed to get note : %@",error);
//            [[self activityIndicator] stopAnimating];
        }];
        pNote++;
    }
    
//    if (self.noteList && self.noteList.count>0){
//        [self loadCurrentNote];        
//        [[self activityIndicator] stopAnimating];
//    }
    

    
//    uint noteListCount=[self.noteList count];
//    uint currentNote=self.currentNote;
    


}
- (void)loadMoreNotes {
    [[self activityIndicator] startAnimating];
    EDAMNoteFilter* filter = [[EDAMNoteFilter alloc] initWithOrder:0 ascending:NO words:nil notebookGuid:self.notebookGUID tagGuids:nil timeZone:nil inactive:NO emphasized:nil];
    EDAMNotesMetadataResultSpec *resultSpec = [[EDAMNotesMetadataResultSpec alloc] initWithIncludeTitle:NO includeContentLength:NO includeCreated:NO includeUpdated:NO includeDeleted:NO includeUpdateSequenceNum:NO includeNotebookGuid:YES includeTagGuids:NO includeAttributes:NO includeLargestResourceMime:NO includeLargestResourceSize:NO];
    [[EvernoteNoteStore noteStore] findNotesMetadataWithFilter:filter offset:self.currentNote maxNotes:10 resultSpec:resultSpec success:^(EDAMNotesMetadataList *metadata) {
        if(metadata.notes.count > 0) {
            self.noteList = metadata.notes;
            [self loadCurrentNote];
        }
        else {
            [self.webView loadHTMLString:@"No note found" baseURL:nil];
            [[self activityIndicator] stopAnimating];
        }
    } failure:^(NSError *error) {
        NSLog(@"Failed to find notes : %@",error);
        [[self activityIndicator] stopAnimating];
    }];
}

- (void) loadCurrentNote {
//    if([self.noteList count] > self.currentNote%10) {

    if([self.noteList count] > self.currentNote) {
        
        
        
        
        
        [[self activityIndicator] startAnimating];
//        EDAMNoteMetadata* foundNote = self.noteList[self.currentNote%10];
        EDAMNoteMetadata* foundNote = self.noteList[self.currentNote];
        [[EvernoteNoteStore noteStore] getNoteWithGuid:foundNote.guid withContent:YES withResourcesData:YES withResourcesRecognition:NO withResourcesAlternateData:NO success:^(EDAMNote *note) {
            ENMLUtility *utltility = [[ENMLUtility alloc] init];
            [utltility convertENMLToHTML:note.content withResources:note.resources completionBlock:^(NSString *html, NSError *error) {
                if(error == nil) {
                    
//                    EDAMNoteMetadata* foundNote = self.noteList[self.currentNote];
                    
                    
                    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
                    OPSProjectSite* projectSite=appDele.activeProjectSite;
                    
                    sqlite3 * database=nil;
                    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
                        if ([self checkNoteExist:database noteGUID:foundNote.guid notebookGUID:foundNote.notebookGuid]){
                            [self setLinkButtonIcon:TRUE];
                        }else{
                            [self setLinkButtonIcon:FALSE];
                        }
                    }
                    sqlite3_close(database);
    
                    
                    [self.webView loadHTMLString:html baseURL:nil];
                    NSLog (@"%@",html);
                    [[self activityIndicator] stopAnimating];
                }
            }];
        } failure:^(NSError *error) {
            NSLog(@"Failed to get note : %@",error);
            [[self activityIndicator] stopAnimating];
        }];
        
//        uint noteListCount=[self.noteList count];
//        uint currentNote=self.currentNote;
        
        if ([self currentNote]<[self.noteList count]-1){
            self.btnNext.enabled=YES;
        }else{
            self.btnNext.enabled=NO;
        }
        
    }
}


/*

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


*/







@end

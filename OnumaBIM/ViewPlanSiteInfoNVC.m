//
//  ViewPlanSiteInfoNVC.m
//  ProjectView
//
//  Created by Alfred Man on 5/11/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewPlanSiteInfoNVC.h"
#import "ViewModelToolbar.h"
@interface ViewPlanSiteInfoNVC ()

@end

@implementation ViewPlanSiteInfoNVC
//@synthesize viewModelToolbar=_viewModelToolbar;
@synthesize viewModelVC=_viewModelVC;
//-(id) initWithRootViewController:(UIViewController *)rootViewController viewModelToolbar:(ViewModelToolbar*) viewModelToolbar{

-(id) initWithRootViewController:(UIViewController *)rootViewController viewModelVC:(ViewModelVC*) viewModelVC{
    self=[super initWithRootViewController:rootViewController];
    if (self){
        self.viewModelVC=viewModelVC;
//        self.viewModelToolbar=viewModelToolbar;
    }else{
    }
    return self;    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

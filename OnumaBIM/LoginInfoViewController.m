//
//  LoginInfoViewController.m
//  ProjectView
//
//  Created by Alfred Man on 8/24/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "LoginInfoViewController.h"
#import "ProjectViewAppDelegate.h"
@interface LoginInfoViewController ()

@end

@implementation LoginInfoViewController
@synthesize infoView=_infoView;
@synthesize scrollView=_scrollview;
@synthesize versionStr=_versionStr;


-(void) dealloc{
    [_infoView release];_infoView=nil;
    [_scrollview release];_scrollview=nil;
    [_versionStr release];_versionStr=nil;
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    //    return self.testUI;        
    return [self infoView];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
        
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    
    [self.scrollView setFrame:self.view.frame];
    [self.scrollView setContentSize:self.infoView.frame.size];
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    self.versionStr.text=[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    
    
//    self.view.frame=CGRectInset(self.view.frame,300,300);
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//	return YES;
//}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    //    return (interfaceOrientation == UIInterfaceOrientationPortrait);    
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

//
//  EverNoteNoteListTVC.m
//  ProjectView
//
//  Created by Alfred Man on 11/18/13.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "EverNoteNoteListTVC.h"

#import "EverNoteNoteBookBrowserTVC.h"
#import "EvernoteSDK.h"
#import "EverNoteNoteBrowserVC.h"
#import "ENMLUtility.h"
#import "OPSProjectSite.h"

#import "ProjectViewAppDelegate.h"
@interface EverNoteNoteListTVC ()

@end

@implementation EverNoteNoteListTVC
const uint EverNoteNoteListTVC_linkToSiteButtonTag=1;
- (id)initWithStyle:(UITableViewStyle)style everNoteNoteBrowserVC:(EverNoteNoteBrowserVC*) everNoteNoteBrowserVC
{
    self = [super initWithStyle:style];
    if (self) {
        self.everNoteNoteBrowserVC=everNoteNoteBrowserVC;
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    /*
    [[EvernoteNoteStore noteStore] listNotebooksWithSuccess:^(NSArray *notebooks) {
        NSSortDescriptor* sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
        self.notebooks = [notebooks sortedArrayUsingDescriptors:@[sortDesc]];
        
        [self.tableView reloadData];
        //        if(self.everNoteNoteBrowserVC.selectedNotebook == nil) {
        //            [self setSelectedNotebookWithGUID:nil];
        //        }
        //        else {
        ////            [self setSelectedNotebook:self.selectedNotebook];
        //        }
    } failure:^(NSError *error) {
        ;
    }];
    */
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [self.everNoteNoteBrowserVC.noteList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"EverNoteNoteListCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease ];
//        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        UIView *selectionView = [[UIView alloc]initWithFrame:cell.bounds];
        [selectionView setBackgroundColor:[UIColor grayColor]];
        cell.selectedBackgroundView = selectionView;
        [selectionView release];
        

    }
    
    EDAMNoteMetadata* thisNote = [self.everNoteNoteBrowserVC.noteList objectAtIndex: [indexPath row]];
//    
//    if (self.everNoteNoteBrowserVC.selectedNotebook!=nil && [self.everNoteNoteBrowserVC.selectedNotebook.guid isEqualToString:notebook.guid]){
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    }else{
//        cell.accessoryType = UITableViewCellAccessoryNone;
//    }
    cell.textLabel.text = thisNote.title;
    cell.textLabel.highlightedTextColor=[UIColor whiteColor];
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;
    sqlite3 * database=nil;
    if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
        bool isCurrentNoteLinked=[self.everNoteNoteBrowserVC checkNoteExistInSiteDB:database noteGUID:thisNote.guid notebookGUID:thisNote.notebookGuid];
//        if (isCurrentNoteLinked){
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        }else{
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        }
//        
        
        
        
        
        
        uint itemGap=5;
        CGRect labelFrame=CGRectMake (0,0,60,30);
        CGRect buttonFrame=CGRectMake(0, 0, 30, 30);
        UIView* buttonView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, (labelFrame.size.width+buttonFrame.size.width+itemGap), buttonFrame.size.height)];
        
        
        UILabel* linkToSiteLabel=[[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, labelFrame.size.width, labelFrame.size.height)];
        linkToSiteLabel.text=@"Linked:";
        [buttonView addSubview:linkToSiteLabel];
        [linkToSiteLabel release];linkToSiteLabel=nil;
        
        UIImage *checkBoxCheckedImg = isCurrentNoteLinked?[UIImage imageNamed:@"toolbar_checkedBox44.png"]:[UIImage imageNamed:@"toolbar_uncheckedBox44.png"];
        
        
        UIButton *linkToSiteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        linkToSiteButton.frame =  CGRectMake(labelFrame.size.width+itemGap, 0.0, buttonFrame.size.width, buttonFrame.size.height);
        [linkToSiteButton setImage:checkBoxCheckedImg forState:UIControlStateNormal];
        [linkToSiteButton setUserInteractionEnabled:YES];
        //                UIButton *uploadButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        //                uploadButton.frame =  CGRectMake(pButton*(buttonSize.size.width+buttonGap), 0.0, buttonSize.size.width, buttonSize.size.height);
        // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
        [linkToSiteButton addTarget:self action:@selector(linkToSiteButtonTouchedUp:event:) forControlEvents:UIControlEventTouchUpInside];
        linkToSiteButton.tag=EverNoteNoteListTVC_linkToSiteButtonTag;
        
        [buttonView addSubview:linkToSiteButton];
        
        cell.accessoryView=buttonView;
        
//        [self setLinkButtonIcon:isCurrentNoteLinked];
    }
    sqlite3_close(database);


            
    return cell ;

}
-(void) linkToSiteButtonTouchedUp:(id)sender event:(id)event{
    
    
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [self.everNoteNoteBrowserVC setCurrentNote: indexPath.row];
    [self.everNoteNoteBrowserVC loadCurrentNote_forwardIfNoMatch];
    [self.tableView reloadData];
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];

    //    [self.everNoteNoteBrowserVC viewDidLoad];
}



@end

//
//  BIMPlanColorTVC.m
//  ProjectView
//
//  Created by Alfred Man on 4/20/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "BIMPlanColorTVC.h"
#import "BIMPlanColorTVCell.h"
#import "ProjectViewAppDelegate.h"
#import "Department.h"
#import "AlignColorTVC.h"
#import "AlignColor.h"
#import "ConstructionStatusColor.h"
#import "CustomColorTVC.h"
#import "CustomColor.h"
#import "CustomColorStruct.h"
#import "Site.h"

#import "DepartmentColorTVC.h"
#import "ConstructionStatusTVC.h"
#import "StructForBIMPlanColor.h"

#import "FusionBldgStatusTVC.h"
#import "FusionBldgFCITVC.h"
#import "FusionBldgStatusFlagTVC.h"
#import "FusionPlannedSFAssignedSFTVC.h"
#import "FusionSpaceStatusFlagTVC.h"
#import "FusionRecordStatusTVC.h"
#import "FusionTopTVC.h"
#import "FusionPGMTVC.h"
#import "FusionRoomUseTVC.h"

#import "FusionBldgStatusColor.h"
#import "FusionBldgFCIColor.h"
#import "FusionBldgStatusFlagColor.h"
#import "FusionPlannedSFAssignedSFColor.h"
#import "FusionSpaceStatusFlagColor.h"
#import "FusionRecordStatusColor.h"
#import "FusionTopColor.h"
#import "FusionPGMColor.h"
#import "FusionRoomUseColor.h"

@implementation BIMPlanColorTVC


-(void)addToSelection:(NSIndexPath *) indexPath{
    
}
-(void)removeFromSelection:(NSIndexPath *) indexPath{
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {        
    BIMPlanColorTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BIMPlanColorTVCell"];
    
    if (cell == nil){
        //        NSLog(@”New Cell Made”);
        //        NSString * cellId = @"reuseCell";
        //        cell=[[[vc view] retain] autorelease];
        
        //        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"BIMMailRecipientCell" owner:nil options:nil];
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"BIMPlanColorTVCell" owner:self options:nil];        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (BIMPlanColorTVCell *)currentObject;
                cell.delegate=self;
                //                [cell setValue:cellId forKey:@"reuseIdentifier"];
                break;
            }
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    Site* site=[appDele currentSite];
    if ([self isKindOfClass:[DepartmentColorTVC class]]){
        if (site.aDepartment && [site.aDepartment count] >0){
            for (uint pDepartment=0;pDepartment<[site.aDepartment count];pDepartment++){
                Department* department=[site.aDepartment objectAtIndex:pDepartment];
                if ([indexPath row]==pDepartment){
                    cell.colorLabel.text=department.name;
                    cell.colorUIView.backgroundColor=department.color;
                }
            }
        }else{
            cell.colorLabel.text=@"No Department Set";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;            
        }
    }else if ([self isKindOfClass:[AlignColorTVC class]]){
        if (site.alignColorDictionary && [[site.alignColorDictionary allKeys] count]>0){
            AlignColor* alignColor=[[site.alignColorDictionary allValues] objectAtIndex:[indexPath row]];
//            for (AlignColor* alignColor in [site.alignColorDictionary allValues]){
                cell.colorLabel.text=alignColor.name;
                cell.colorUIView.backgroundColor=alignColor.color;
                
//            }
//            for (uint pAlignColor=0;pAlignColor<[[site.alignColorDictionary allValues] count];pAlignColor++){
//                AlignColor* alignColor=[[site.alignColorDictionary allKeys
//                Department* department=[site.aDepartment objectAtIndex:pDepartment];
//                if ([indexPath row]==pDepartment){
//                    cell.colorLabel.text=department.name;
//                    cell.colorUIView.backgroundColor=department.color;
//                }
//            }
        }else{
            cell.colorLabel.text=@"No Vertical Alignment";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }
    }else if ([self isKindOfClass:[ConstructionStatusTVC class]]){
        if (site.constructionStatusDictionary && [[site.constructionStatusDictionary allKeys] count]>0){
            ConstructionStatusColor* constructionStatusColors=[[site.constructionStatusDictionary allValues] objectAtIndex:[indexPath row]];
            //            for (AlignColor* alignColor in [site.alignColorDictionary allValues]){
            cell.colorLabel.text=constructionStatusColors.name;
            cell.colorUIView.backgroundColor=constructionStatusColors.color;
            
            //            }
            //            for (uint pAlignColor=0;pAlignColor<[[site.alignColorDictionary allValues] count];pAlignColor++){
            //                AlignColor* alignColor=[[site.alignColorDictionary allKeys
            //                Department* department=[site.aDepartment objectAtIndex:pDepartment];
            //                if ([indexPath row]==pDepartment){
            //                    cell.colorLabel.text=department.name;
            //                    cell.colorUIView.backgroundColor=department.color;
            //                }
            //            }
        }else{
            cell.colorLabel.text=@"No Construction Status";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }
    }else if ([self isKindOfClass:[CustomColorTVC class]]){
        NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
        uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
        
        
        
        if (aCustomColorStruct && [aCustomColorStruct count]>0){
            CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:activeCustomColorStruct];
            if (customColorStruct!=nil){
                CustomColor* customColor=[customColorStruct.aCustomColor objectAtIndex:[indexPath row]];
                cell.colorLabel.text=customColor.name;
                cell.colorUIView.backgroundColor=customColor.color;
            }else{
                cell.colorLabel.text=@"No Custom Color Set";
                cell.colorUIView.hidden=true;
                cell.addToSelectionButton.hidden=true;
                cell.minusFromSelectionButton.hidden=true;                
            }
        }else{
            cell.colorLabel.text=@"No Custom Color Set";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }
    }else if ([self isKindOfClass:[FusionBldgStatusTVC class]]){
        if (site.fusionBldgStatusDictionary && [[site.fusionBldgStatusDictionary allKeys] count]>0){
            
            
            FusionBldgStatusColor* fusionBldgStatusColor=nil;
            switch ([indexPath row]){
                case 0:
                    fusionBldgStatusColor=[site.fusionBldgStatusDictionary valueForKey:@"Active"];
                    break;
                case 1:
                    fusionBldgStatusColor=[site.fusionBldgStatusDictionary valueForKey:@"UNCLASSIFIED"];
                    break;
                case 2:
                    fusionBldgStatusColor=[site.fusionBldgStatusDictionary valueForKey:@"OFFLINE"];
                    break;
                case 3:
                    fusionBldgStatusColor=[site.fusionBldgStatusDictionary valueForKey:@"DEACTIVATED"];
                    break;
                case 4:
                    fusionBldgStatusColor=[site.fusionBldgStatusDictionary valueForKey:@"NON-ASSIGNABLE"];
                    break;
                default:
                    fusionBldgStatusColor=[site.fusionBldgStatusDictionary valueForKey:@"DEFAULT"];
                    break;
            }

//            FusionBldgStatusColor* fusionBldgStatusColor=[[site.fusionBldgStatusDictionary allValues] objectAtIndex:[indexPath row]];
            //            for (AlignColor* alignColor in [site.alignColorDictionary allValues]){
            cell.colorLabel.text=fusionBldgStatusColor.name;
            cell.colorUIView.backgroundColor=fusionBldgStatusColor.color;
            
 
        }else{
            cell.colorLabel.text=@"No Fusion Bldg Status";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }
    }else if ([self isKindOfClass:[FusionBldgFCITVC class]]){
        if (site.fusionBldgFCIDictionary && [[site.fusionBldgFCIDictionary allKeys] count]>0){
            FusionBldgFCIColor* fusionBldgFCIColor=nil;
            switch ([indexPath row]){
                case 4:
                    fusionBldgFCIColor=[site.fusionBldgFCIDictionary valueForKey:@">90"];
                    break;
                case 3:
                    fusionBldgFCIColor=[site.fusionBldgFCIDictionary valueForKey:@">50"];
                    break;
                case 2:
                    fusionBldgFCIColor=[site.fusionBldgFCIDictionary valueForKey:@">10"];
                    break;
                case 1:
                    fusionBldgFCIColor=[site.fusionBldgFCIDictionary valueForKey:@">0"];
                    break;
                case 0:
                    fusionBldgFCIColor=[site.fusionBldgFCIDictionary valueForKey:@"=0"];
                    break;
                default:
                    fusionBldgFCIColor=[site.fusionBldgFCIDictionary valueForKey:@">0"];
                    break;
            }
//            FusionBldgFCIColor* fusionBldgFCIColor=[[site.fusionBldgFCIDictionary allValues] objectAtIndex:[indexPath row]];
            //            for (AlignColor* alignColor in [site.alignColorDictionary allValues]){
            cell.colorLabel.text=fusionBldgFCIColor.name;
            cell.colorUIView.backgroundColor=fusionBldgFCIColor.color;
            
            //            }
            //            for (uint pAlignColor=0;pAlignColor<[[site.alignColorDictionary allValues] count];pAlignColor++){
            //                AlignColor* alignColor=[[site.alignColorDictionary allKeys
            //                Department* department=[site.aDepartment objectAtIndex:pDepartment];
            //                if ([indexPath row]==pDepartment){
            //                    cell.colorLabel.text=department.name;
            //                    cell.colorUIView.backgroundColor=department.color;
            //                }
            //            }
        }else{
            cell.colorLabel.text=@"No Fusion Bldg FCI set";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }
    }else if ([self isKindOfClass:[FusionBldgStatusFlagTVC class]]){
        if (site.fusionBldgStatusFlagDictionary && [[site.fusionBldgStatusFlagDictionary allKeys] count]>0){
            FusionBldgStatusFlagColor* fusionBldgStatusFlagColor=nil;//[[site.fusionBldgStatusFlagDictionary allValues] objectAtIndex:[indexPath row]];
            switch ([indexPath row]){
                case 0:
                    fusionBldgStatusFlagColor=[site.fusionBldgStatusFlagDictionary valueForKey:@"Matches FUSION"];
                    break;
                case 1:
                    fusionBldgStatusFlagColor=[site.fusionBldgStatusFlagDictionary valueForKey:@"Deleted in FUSION"];
                    break;
                case 2:
                    fusionBldgStatusFlagColor=[site.fusionBldgStatusFlagDictionary valueForKey:@"Added from FUSION"];
                    break;
                case 3:
                    fusionBldgStatusFlagColor=[site.fusionBldgStatusFlagDictionary valueForKey:@"Not in FUSION"];
                    break;
                default:
                    fusionBldgStatusFlagColor=[site.fusionBldgStatusFlagDictionary valueForKey:@"Not in FUSION"];
                    break;
            }
            cell.colorLabel.text=fusionBldgStatusFlagColor.name;
            cell.colorUIView.backgroundColor=fusionBldgStatusFlagColor.color;
            
        }else{
            cell.colorLabel.text=@"No Fusion Bldg Status Flag";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }
    }else if ([self isKindOfClass:[FusionPlannedSFAssignedSFTVC class]]){
        if (site.fusionPlannedSFAssignedSFDictionary && [[site.fusionPlannedSFAssignedSFDictionary allKeys] count]>0){
            FusionPlannedSFAssignedSFColor* fusionPlannedSFAssignedSFColor=nil;
            switch ([indexPath row]){
                case 0:
                    fusionPlannedSFAssignedSFColor=[site.fusionPlannedSFAssignedSFDictionary valueForKey:@"No Assigned SF"];
                    break;
                case 1:
                    fusionPlannedSFAssignedSFColor=[site.fusionPlannedSFAssignedSFDictionary valueForKey:@"Assigned SF = 0"];
                    break;
                case 2:
                    fusionPlannedSFAssignedSFColor=[site.fusionPlannedSFAssignedSFDictionary valueForKey:@"Difference < 1%"];
                    break;
                case 3:
                    fusionPlannedSFAssignedSFColor=[site.fusionPlannedSFAssignedSFDictionary valueForKey:@"Between 1% and 9%"];
                    break;
                case 4:
                    fusionPlannedSFAssignedSFColor=[site.fusionPlannedSFAssignedSFDictionary valueForKey:@"Difference >= 10%"];
                    break;
                default:
                    fusionPlannedSFAssignedSFColor=[site.fusionPlannedSFAssignedSFDictionary valueForKey:@"No Assigned SF"];
                    break;
            }
            //            FusionBldgFCIColor* fusionBldgFCIColor=[[site.fusionBldgFCIDictionary allValues] objectAtIndex:[indexPath row]];
            //            for (AlignColor* alignColor in [site.alignColorDictionary allValues]){
            cell.colorLabel.text=fusionPlannedSFAssignedSFColor.name;
            cell.colorUIView.backgroundColor=fusionPlannedSFAssignedSFColor.color;
            
            //            }
            //            for (uint pAlignColor=0;pAlignColor<[[site.alignColorDictionary allValues] count];pAlignColor++){
            //                AlignColor* alignColor=[[site.alignColorDictionary allKeys
            //                Department* department=[site.aDepartment objectAtIndex:pDepartment];
            //                if ([indexPath row]==pDepartment){
            //                    cell.colorLabel.text=department.name;
            //                    cell.colorUIView.backgroundColor=department.color;
            //                }
            //            }
        }else{
            cell.colorLabel.text=@"No Fusion Planned SF/Assigned SF";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }
    }else if ([self isKindOfClass:[FusionSpaceStatusFlagTVC class]]){
        if (site.fusionSpaceStatusFlagDictionary && [[site.fusionSpaceStatusFlagDictionary allKeys] count]>0){
            FusionSpaceStatusFlagColor* fusionSpaceStatusFlagColor=nil;
            switch ([indexPath row]){
                case 0:
                    fusionSpaceStatusFlagColor=[site.fusionSpaceStatusFlagDictionary valueForKey:@"Matches FUSION"];
                    break;
                case 1:
                    fusionSpaceStatusFlagColor=[site.fusionSpaceStatusFlagDictionary valueForKey:@"Deleted in FUSION"];
                    break;
                case 2:
                    fusionSpaceStatusFlagColor=[site.fusionSpaceStatusFlagDictionary valueForKey:@"Added from FUSION"];
                    break;
                case 3:
                    fusionSpaceStatusFlagColor=[site.fusionSpaceStatusFlagDictionary valueForKey:@"Ignore Fusion Space Number"];
                    break;
                case 4:
                    fusionSpaceStatusFlagColor=[site.fusionSpaceStatusFlagDictionary valueForKey:@"Not in FUSION"];
                    break;
                default:
                    fusionSpaceStatusFlagColor=[site.fusionSpaceStatusFlagDictionary valueForKey:@"Not in FUSION"];
                    break;
            }
                     
            //            for (AlignColor* alignColor in [site.alignColorDictionary allValues]){
            cell.colorLabel.text=fusionSpaceStatusFlagColor.name;
            cell.colorUIView.backgroundColor=fusionSpaceStatusFlagColor.color;

        }else{
            cell.colorLabel.text=@"No Fusion Space Status Flag";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }
        
    }else if ([self isKindOfClass:[FusionRecordStatusTVC class]]){
        if (site.fusionRecordStatusDictionary && [[site.fusionRecordStatusDictionary allKeys] count]>0){
            FusionRecordStatusColor* fusionRecordStatusColor=nil;
            switch ([indexPath row]){
                case 0:
                    fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"ASSIGNABLE"];
                    break;
                case 1:
                    fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"UNCLASSIFIED"];
                    break;
                case 2:
                    fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"DEACTIVATED"];
                    break;
                case 3:
                    fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"NON-ASSIGNABLE"];
                    break;
                case 4:
                    fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"Not in FUSION"];
                    break;
                default:
                    fusionRecordStatusColor=[site.fusionRecordStatusDictionary valueForKey:@"Not in FUSION"];
                    break;
            }
            
            //            for (AlignColor* alignColor in [site.alignColorDictionary allValues]){
            cell.colorLabel.text=fusionRecordStatusColor.name;
            cell.colorUIView.backgroundColor=fusionRecordStatusColor.color;
            
        }else{
            cell.colorLabel.text=@"No Fusion Record Status";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }
        
        
        
    }else if ([self isKindOfClass:[FusionTopTVC class]]){
        if (site.fusionTopDictionary && [[site.fusionTopDictionary allKeys] count]>0){
            FusionTopColor* fusionTopColor=[[site.fusionTopDictionary allValues] objectAtIndex:[indexPath row]];
            //            for (AlignColor* alignColor in [site.alignColorDictionary allValues]){
            cell.colorLabel.text=fusionTopColor.name;
            cell.colorUIView.backgroundColor=fusionTopColor.color;
            
            //            }
            //            for (uint pAlignColor=0;pAlignColor<[[site.alignColorDictionary allValues] count];pAlignColor++){
            //                AlignColor* alignColor=[[site.alignColorDictionary allKeys
            //                Department* department=[site.aDepartment objectAtIndex:pDepartment];
            //                if ([indexPath row]==pDepartment){
            //                    cell.colorLabel.text=department.name;
            //                    cell.colorUIView.backgroundColor=department.color;
            //                }
            //            }
        }else{
            cell.colorLabel.text=@"No Fusion Top Color";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }

        
        
        
    }else if ([self isKindOfClass:[FusionPGMTVC class]]){
        if (site.fusionPGMDictionary && [[site.fusionPGMDictionary allKeys] count]>0){
            FusionPGMColor* fusionPGMColor=[[site.fusionPGMDictionary allValues] objectAtIndex:[indexPath row]];
            //            for (AlignColor* alignColor in [site.alignColorDictionary allValues]){
            cell.colorLabel.text=fusionPGMColor.name;
            cell.colorUIView.backgroundColor=fusionPGMColor.color;
            
            //            }
            //            for (uint pAlignColor=0;pAlignColor<[[site.alignColorDictionary allValues] count];pAlignColor++){
            //                AlignColor* alignColor=[[site.alignColorDictionary allKeys
            //                Department* department=[site.aDepartment objectAtIndex:pDepartment];
            //                if ([indexPath row]==pDepartment){
            //                    cell.colorLabel.text=department.name;
            //                    cell.colorUIView.backgroundColor=department.color;
            //                }
            //            }
        }else{
            cell.colorLabel.text=@"No Fusion PGM Color";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }
        
        
        
        
    }else if ([self isKindOfClass:[FusionRoomUseTVC class]]){
        if (site.fusionRoomUseDictionary && [[site.fusionRoomUseDictionary allKeys] count]>0){
            FusionRoomUseColor* fusionRoomUseColor=[[site.fusionRoomUseDictionary allValues] objectAtIndex:[indexPath row]];
            //            for (AlignColor* alignColor in [site.alignColorDictionary allValues]){
            cell.colorLabel.text=fusionRoomUseColor.name;
            cell.colorUIView.backgroundColor=fusionRoomUseColor.color;
            
            //            }
            //            for (uint pAlignColor=0;pAlignColor<[[site.alignColorDictionary allValues] count];pAlignColor++){
            //                AlignColor* alignColor=[[site.alignColorDictionary allKeys
            //                Department* department=[site.aDepartment objectAtIndex:pDepartment];
            //                if ([indexPath row]==pDepartment){
            //                    cell.colorLabel.text=department.name;
            //                    cell.colorUIView.backgroundColor=department.color;
            //                }
            //            }
        }else{
            cell.colorLabel.text=@"No Fusion Room Use Color";
            cell.colorUIView.hidden=true;
            cell.addToSelectionButton.hidden=true;
            cell.minusFromSelectionButton.hidden=true;
        }
        
        
        
        
    }





//    cell.backgroundColor=[UIColor colorWithRed:0. green:0.39 blue:0.106 alpha:0.1];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end

//
//  OPSProjectSiteAboutToDownload.h
//  ProjectView
//
//  Created by Alfred Man on 7/6/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "OPSProjectSite.h"
@class LiveProjectListVC;
@class OPSLocalProjectSite;
@interface OPSProjectSiteAboutToDownload : OPSProjectSite <UIAlertViewDelegate>{
    LiveProjectListVC* _liveProjectListVC;
    OPSLocalProjectSite* _uploadLocalProjectSiteBeforeDownload;
}
@property (nonatomic, assign) LiveProjectListVC* liveProjectListVC;
@property (nonatomic, retain) OPSLocalProjectSite* uploadLocalProjectSiteBeforeDownload;
-(id) initWithLiveOPSProjectList: liveProjectListVC;


-(void) tryDownloadSite;
@end

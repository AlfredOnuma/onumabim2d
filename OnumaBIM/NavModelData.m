//
//  NavModelData.m
//  ProjectView
//
//  Created by Alfred Man on 11/18/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "NavModelData.h"
#import "OPSToken.h"
@implementation NavModelData
@synthesize name;
@synthesize opsToken;
-(void)dealloc{
    [name release];
    [opsToken release];
    [super dealloc];
}
@end

//
//  NavUIScroll.h
//  OPSMobile
//
//  Created by Alfred Man on 9/30/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DisplayModelUI.h"


@protocol NavUIScrollDelegate
//- (OPSModel*) getModel;
@end
@class ViewModelVC;
@interface NavUIScroll : UIScrollView <DisplayModelUIDelegate> {
    
//    id <NavUIScrollDelegate> delegate; 
 //   DisplayModelUI* displayModelUI;
}

//@property (assign) id <NavUIScrollDelegate> delegate;
//@property (nonatomic, retain) DisplayModelUI* displayModelUI;
- (void) zoomFit;
- (void) zoomMin;
-(void) zoomToRectWithDuration:(CGRect)rect duration:(double) duration invoker:(id)invoker completeAction:(SEL) completeAction actionParam:(id) paramObject;
- (id) initWithFrame:(CGRect)frame opsNavUI:(OPSNavUI*) opsNavUI;

-(DisplayModelUI*) displayModelUI;
-(ViewModelVC*) parentViewModelVC;
//- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center;  
//-(void)scrollRectToVisibleCenteredOn:(CGRect)visibleRect animated:(BOOL)animated;


- (void) zoomFitWithDuration:(double)duration bRefreshLabel:(bool)bRefreshLabel invoker:(id)invoker completeAction:(SEL) completeAction actionParam:(id) paramObject;
//- (void) zoomFitWithDuration:(double)duration bRefreshLabel:(bool)bRefreshLabel;
@end

//
//  CustomColorStruct.m
//  ProjectView
//
//  Created by Alfred Man on 4/21/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "CustomColorStruct.h"
#import "ProjectViewAppDelegate.h"
#import "CustomColor.h"
@implementation CustomColorStruct
@synthesize ID=_ID;
@synthesize siteID=_siteID;
@synthesize type=_type;
@synthesize customSettingNum=_customSettingNum;
@synthesize customSettingName=_customSettingName;
@synthesize aCustomColor=_aCustomColor;
/*
@synthesize name1=_name1;
@synthesize color1=_color1;
@synthesize name2=_name2;
@synthesize color2=_color2;
@synthesize name3=_name3;
@synthesize color3=_color3;
@synthesize name4=_name4;
@synthesize color4=_color4;
@synthesize name5=_name5;
@synthesize color5=_color5;
@synthesize name6=_name6;
@synthesize color6=_color6;
@synthesize name7=_name7;
@synthesize color7=_color7;
@synthesize name8=_name8;
@synthesize color8=_color8;
@synthesize name9=_name9;
@synthesize color9=_color9;
@synthesize name10=_name10;
@synthesize color10=_color10;
@synthesize name11=_name11;
@synthesize color11=_color11;
@synthesize name12=_name12;
@synthesize color12=_color12;
@synthesize name13=_name13;
@synthesize color13=_color13;
@synthesize name14=_name14;
@synthesize color14=_color14;
@synthesize name15=_name15;
@synthesize color15=_color15;
@synthesize name16=_name16;
@synthesize color16=_color16;
@synthesize name17=_name17;
@synthesize color17=_color17;
@synthesize name18=_name18;
@synthesize color18=_color18;
@synthesize name19=_name19;
@synthesize color19=_color19;
@synthesize name20=_name20;
@synthesize color20=_color20;
@synthesize name21=_name21;
@synthesize color21=_color21;
@synthesize name22=_name22;
@synthesize color22=_color22;
@synthesize name23=_name23;
@synthesize color23=_color23;
@synthesize name24=_name24;
@synthesize color24=_color24;
@synthesize name25=_name25;
@synthesize color25=_color25;
@synthesize name26=_name26;
@synthesize color26=_color26;
@synthesize name27=_name27;
@synthesize color27=_color27;
@synthesize name28=_name28;
@synthesize color28=_color28;
@synthesize name29=_name29;
@synthesize color29=_color29;
@synthesize name30=_name30;
@synthesize color30=_color30;
*/

-(void)dealloc{
    
    [_type release];_type=nil;
    [_customSettingName release];_customSettingName=nil;
    [_aCustomColor release];_aCustomColor=nil;
    /*
    [_name1 release];_name1=nil;    
    [_color1 release];_color1=nil;
    [_name2 release];_name2=nil;    
    [_color2 release];_color2=nil;
    [_name3 release];_name3=nil;    
    [_color3 release];_color3=nil;
    [_name4 release];_name4=nil;    
    [_color4 release];_color4=nil;
    [_name5 release];_name5=nil;    
    [_color5 release];_color5=nil;
    [_name6 release];_name6=nil;    
    [_color6 release];_color6=nil;
    [_name7 release];_name7=nil;    
    [_color7 release];_color7=nil;
    [_name8 release];_name8=nil;    
    [_color8 release];_color8=nil;
    [_name9 release];_name9=nil;    
    [_color9 release];_color9=nil;
    [_name10 release];_name10=nil;    
    [_color10 release];_color10=nil;
    [_name11 release];_name11=nil;    
    [_color11 release];_color11=nil;
    [_name12 release];_name12=nil;    
    [_color12 release];_color12=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    [_name release];_name=nil;    
    [_color release];_color=nil;
    */
    [super dealloc];
}
-(CustomColor*) getCustomColorWithCustomInt:(uint)customInt{
    if (self.aCustomColor==nil){
        return nil;
    }
    for (CustomColor* customColor in self.aCustomColor){
        if (customColor.ID==customInt){
            return customColor;
        }
    }
    return nil;
}
-(id) initWithDatabase:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt{// ID:(uint)ID siteID:(uint)siteID type:(NSString*)type customSettingNum:(uint)customSettingNum customSettingName:(NSString*)customSettingName name1:(NSString*)name1 color1:(NSString*) color1 name2:(NSString*)name2 color2:(NSString*) color2  name3:(NSString*)name3 color3:(NSString*) color3 name4:(NSString*)name4 color4:(NSString*)color4 name5:(NSString*)name5 color5:(NSString*)color5 name6:(NSString*)name6 color6:(NSString*)color6  name7:(NSString*)name7 color7:(NSString*)color7 name8:(NSString*)name8 color8:(NSString*)color8 name9:(NSString*)name9 color9:(NSString*)color9 name10:(NSString*)name10 color10:(NSString*)color10 name11:(NSString*)name11 color11:(NSString*)color11 name12:(NSString*)name12 color12:(NSString*)color12 name13:(NSString*)name13 color13:(NSString*)color13 name14:(NSString*)name14 color14:(NSString*)color14 name15:(NSString*)name15 color15:(NSString*)color15 name16:(NSString*)name16 color16:(NSString*)color16 name17:(NSString*)name17 color17:(NSString*)color17 name18:(NSString*)name18 color18:(NSString*)color18 name19:(NSString*)name19 color19:(NSString*)color19 name20:(NSString*)name20 color20:(NSString*)color20 name21:(NSString*)name21 color21:(NSString*)color21 name22:(NSString*)name22 color22:(NSString*)color22 name23:(NSString*)name23 color23:(NSString*)color23 name24:(NSString*)name24 color24:(NSString*)color24 name25:(NSString*)name25 color25:(NSString*)color25 name26:(NSString*)name26 color26:(NSString*)color26 name27:(NSString*)name27 color27:(NSString*)color27 name28:(NSString*)name28 color28:(NSString*)color28 name29:(NSString*)name29 color29:(NSString*)color29 name30:(NSString*)name30 color30:(NSString*)color30{
    //    self.model=_model;          
    //    int _siteID=0; //Need rework on this siteID later
    
    //    NSInteger sitePolylineID = sqlite3_column_int(sqlStmt, 0);  
    self=[super init];
    if (self){        
        
        self.ID= (sqlite3_column_int(sqlStmt,0));
        self.siteID=(sqlite3_column_int(sqlStmt,1));
        NSString* tmpType=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:2]];
        self.type=tmpType;
        [tmpType release];
        
        self.customSettingNum=(sqlite3_column_int(sqlStmt,3));
        NSString* tmpCustomSettingName=[[NSString alloc]initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:4]];
        self.customSettingName=tmpCustomSettingName;
        [tmpCustomSettingName release];
        

        
        
        if (self.aCustomColor==nil){
            NSMutableArray* aColor=[[NSMutableArray alloc] init];
            self.aCustomColor=aColor;
            [aColor release];
        }
        uint pCol=5;    
        
        for (uint pCustomColor=0;pCustomColor<50;pCustomColor++){
//            if (self.ID==490 && pCustomColor==10){
//                int t=0;
//                t=1;
//            }
            NSString* customColorName=[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol];
            pCol++;
            NSString* customColorStr=[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:pCol];
            pCol++;
            if (customColorName && ![customColorName isEqualToString:@""]){
                CustomColor* customColor=[[CustomColor alloc] initWithID:pCustomColor name:customColorName colorStr:customColorStr];
                [self.aCustomColor addObject:customColor];
                [customColor release];
            }else{
//                if (self.ID==490 ){
//                    int t=0;
//                    t=1;
//                }
            }
            
            
            

        }
        
    
    }
    
        
    return self;
}



@end

//
//  ProjectListSiteInfoVC.h
//  ProjectView
//
//  Created by Alfred Man on 6/12/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>



//
//  ProjectListSiteInfoVC.h
//  ProjectView
//
//  Created by Alfred Man on 5/11/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewPlanInfoVC.h"
//#import <sqlite3.h>
@class ViewModelVC;
@interface ProjectListSiteInfoVC : ViewPlanInfoVC <UITextFieldDelegate, UIScrollViewDelegate>{
    
    IBOutlet UIView* infoView;
    IBOutlet UIScrollView* infoScrollView;
    
    IBOutlet UITextField* siteAreaTextField;
    
    IBOutlet UITextField* numBldgTextField0;
    IBOutlet UITextField* numBldgsNetCalculatedTextField0;
    IBOutlet UITextField* grossCalculatedTextField0;
    IBOutlet UITextField* grossReportedTextField0;
    IBOutlet UITextField* capacityTextField0;
    IBOutlet UITextField* occupancyTextField0;
    IBOutlet UITextField* occupancyNumberTextField0;
    IBOutlet UITextField* totalEstimatedBuildingCostTextField0;
    
    
    NSNumber* tmpNumBldg;        
    NSNumber* tmpSumBldgNetArea;
    NSNumber* tmpSumBldgGrossArea;
    NSNumber* tmpSumReportedGrossArea;
    NSNumber* tmpSumCapicity;
    NSNumber* tmpSumOccupancy;
    NSNumber* tmpSumOccupancyNumber;
    NSNumber* tmpSumEstimateCost;
    
    
    IBOutlet UITextField* numBldgTextField1;
    IBOutlet UITextField* numBldgsNetCalculatedTextField1;
    IBOutlet UITextField* grossCalculatedTextField1;
    IBOutlet UITextField* grossReportedTextField1;
    IBOutlet UITextField* capacityTextField1;
    IBOutlet UITextField* occupancyTextField1;
    IBOutlet UITextField* occupancyNumberTextField1;
    IBOutlet UITextField* totalEstimatedBuildingCostTextField1;
    
    
    
    
    IBOutlet UITextField* numBldgTextField2;
    IBOutlet UITextField* numBldgsNetCalculatedTextField2;
    IBOutlet UITextField* grossCalculatedTextField2;
    IBOutlet UITextField* grossReportedTextField2;
    IBOutlet UITextField* capacityTextField2;
    IBOutlet UITextField* occupancyTextField2;
    IBOutlet UITextField* occupancyNumberTextField2;
    IBOutlet UITextField* totalEstimatedBuildingCostTextField2;
    
    
    
    
    
    
    IBOutlet UITextField* numBldgTextField3;
    IBOutlet UITextField* numBldgsNetCalculatedTextField3;
    IBOutlet UITextField* grossCalculatedTextField3;
    IBOutlet UITextField* grossReportedTextField3;
    IBOutlet UITextField* capacityTextField3;
    IBOutlet UITextField* occupancyTextField3;
    IBOutlet UITextField* occupancyNumberTextField3;
    IBOutlet UITextField* totalEstimatedBuildingCostTextField3;
    
    
    IBOutlet UIButton* liveSiteReportButton;
    /*
     UITextField* siteAreaTextField;
     UITextField* numBldgTextField0;
     UITextField* numBldgsNetCalculatedTextField0;
     UITextField* grossCalculatedTextField0;
     UITextField* grossReportedTextField0;
     UITextField* capacityTextField0;
     UITextField* occupancyTextField0;
     UITextField* occupancyNumberTextField0;
     UITextField* totalEstimatedBuildingCostTextField0;
     
     UITextField* numBldgTextField1;
     UITextField* numBldgsNetCalculatedTextField1;
     UITextField* grossCalculatedTextField1;
     UITextField* grossReportedTextField1;
     UITextField* capacityTextField1;
     UITextField* occupancyTextField1;
     UITextField* occupancyNumberTextField1;
     UITextField* totalEstimatedBuildingCostTextField1;
     
     
     UITextField* numBldgTextField2;
     UITextField* numBldgsNetCalculatedTextField2;
     UITextField* grossCalculatedTextField2;
     UITextField* grossReportedTextField2;
     UITextField* capacityTextField2;
     UITextField* occupancyTextField2;
     UITextField* occupancyNumberTextField2;
     UITextField* totalEstimatedBuildingCostTextField2;
     
     UIButton* liveSiteReportButton;
     */
}

@property (nonatomic, retain) NSNumber* tmpNumBldg;        
@property (nonatomic, retain) NSNumber* tmpSumBldgNetArea;
@property (nonatomic, retain) NSNumber* tmpSumBldgGrossArea;
@property (nonatomic, retain) NSNumber* tmpSumReportedGrossArea;
@property (nonatomic, retain) NSNumber* tmpSumCapicity;
@property (nonatomic, retain) NSNumber* tmpSumOccupancy;
@property (nonatomic, retain) NSNumber* tmpSumOccupancyNumber;
@property (nonatomic, retain) NSNumber* tmpSumEstimateCost;


@property (nonatomic, retain) IBOutlet UIView* infoView;
@property (nonatomic, retain) IBOutlet UIScrollView* infoScrollView;
@property (nonatomic, retain) IBOutlet UIButton* liveSiteReportButton;
@property (nonatomic, retain) IBOutlet UITextField* siteAreaTextField;

@property (nonatomic, retain) IBOutlet UITextField* numBldgTextField0;
@property (nonatomic, retain) IBOutlet UITextField* numBldgsNetCalculatedTextField0;
@property (nonatomic, retain) IBOutlet UITextField* grossCalculatedTextField0;
@property (nonatomic, retain) IBOutlet UITextField* grossReportedTextField0;
@property (nonatomic, retain) IBOutlet UITextField* capacityTextField0;
@property (nonatomic, retain) IBOutlet UITextField* occupancyTextField0;
@property (nonatomic, retain) IBOutlet UITextField* occupancyNumberTextField0;
@property (nonatomic, retain) IBOutlet UITextField* totalEstimatedBuildingCostTextField0;





@property (nonatomic, retain) IBOutlet UITextField* numBldgTextField1;
@property (nonatomic, retain) IBOutlet UITextField* numBldgsNetCalculatedTextField1;
@property (nonatomic, retain) IBOutlet UITextField* grossCalculatedTextField1;
@property (nonatomic, retain) IBOutlet UITextField* grossReportedTextField1;
@property (nonatomic, retain) IBOutlet UITextField* capacityTextField1;
@property (nonatomic, retain) IBOutlet UITextField* occupancyTextField1;
@property (nonatomic, retain) IBOutlet UITextField* occupancyNumberTextField1;
@property (nonatomic, retain) IBOutlet UITextField* totalEstimatedBuildingCostTextField1;


@property (nonatomic, retain) IBOutlet UITextField* numBldgTextField2;
@property (nonatomic, retain) IBOutlet UITextField* numBldgsNetCalculatedTextField2;
@property (nonatomic, retain) IBOutlet UITextField* grossCalculatedTextField2;
@property (nonatomic, retain) IBOutlet UITextField* grossReportedTextField2;
@property (nonatomic, retain) IBOutlet UITextField* capacityTextField2;
@property (nonatomic, retain) IBOutlet UITextField* occupancyTextField2;
@property (nonatomic, retain) IBOutlet UITextField* occupancyNumberTextField2;
@property (nonatomic, retain) IBOutlet UITextField* totalEstimatedBuildingCostTextField2;



@property (nonatomic, retain) IBOutlet UITextField* numBldgTextField3;
@property (nonatomic, retain) IBOutlet UITextField* numBldgsNetCalculatedTextField3;
@property (nonatomic, retain) IBOutlet UITextField* grossCalculatedTextField3;
@property (nonatomic, retain) IBOutlet UITextField* grossReportedTextField3;
@property (nonatomic, retain) IBOutlet UITextField* capacityTextField3;
@property (nonatomic, retain) IBOutlet UITextField* occupancyTextField3;
@property (nonatomic, retain) IBOutlet UITextField* occupancyNumberTextField3;
@property (nonatomic, retain) IBOutlet UITextField* totalEstimatedBuildingCostTextField3;
//https://www.onuma.com/plan/OPS/report/reportSite.php?sysID=141&projectID=9&siteID=9

-(IBAction)displayAttachWebView:(id)sender;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil viewController:(UIViewController*)viewController;
@end

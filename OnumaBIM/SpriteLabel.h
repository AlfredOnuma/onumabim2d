//
//  SpriteLabel.h
//  ProjectView
//
//  Created by Alfred Man on 2/15/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewProductSprite.h"

@interface SpriteLabel : ViewProductSprite{
    NSString* displayText;
}
@property (nonatomic,retain) NSString* displayText;

@end

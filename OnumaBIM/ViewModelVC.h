//
//  ViewModelVC.h
//  ProjectView
//
//  Created by Alfred Man on 11/2/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import "DisplayModelUI.h"
//#import "NavUIScroll.h"
#import "OPSNavUI.h"
#import "NavFloorVC.h"
#import "NavModelVC.h"
#import "MapTypeListTVC.h"
#import "ViewModelToolbar.h"
//#import  <MapKit/MapKit.h>
@class OPSNavUI;



@class NavUIScroll;
//@class DisplayModelUI;
@class TestUI3;
@class OPSModel;
@class OPSProduct;
@class ViewModelToolbar;

//@protocol ViewModelPopoverContentDelegate <NSObject>
//- (void)userSelectedRowInPopover:(NSUInteger)row;
//@end
@interface ViewModelVC : UIViewController <OPSNavUIDelegate,MapTypeListDelegate, NavModelDelegate, NavFloorDelegate, ViewModelToolbarDelegate, UIToolbarDelegate, UIPopoverControllerDelegate>{// DisplayModelUIDelegate,NavUIScrollDelegate, UIScrollViewDelegate, UIPopoverControllerDelegate>{
    
    int _currentColorMethod;
    
    int modelNavUILevel; //0=>Site Level  || 1=>Floor Level  || 2=>Space Level     
//    int k;
    bool _browseRightAfterLoad;
    bool bMultiSelect;
    OPSNavUI* opsNavUI;    
    NavUIScroll* navUIScrollVC;
    DisplayModelUI* displayModelUI;
    ViewModelToolbar* viewModelToolbar;
    UIPopoverController *popoverController;
//    OPSProduct* selectedProduct;
    
//    ViewProductRep* selectedProductRep;
    NSMutableArray* aSelectedProductRep;
    OPSModel* model;
    uint viewProductTextMethod;
    UIImageView* mapLogo;

    NSMutableArray* aFontSize;

//    MKMapView* mapView;
    uint pFontSize;

    ViewProductRep* lastDeselectdViewProductRep;
//    UIView* loadingOverlay;
//    UILabel* loadingOverlayLabel;    
//    UIActivityIndicatorView  *loadingOverlayActIndi;
//    UIProgressView* loadingOverlayProgressBar;
//    IBOutlet TestUI3* testUI;
    
}
//@property (nonatomic, assign) int k;
//@property (nonatomic, retain) MKMapView* mapView;
@property (nonatomic, assign) int currentColorMethod;
@property (nonatomic, assign) bool browseRightAfterLoad;
@property (nonatomic, assign) bool bMultiSelect;
@property (nonatomic, retain) UIImageView* mapLogo;
@property (nonatomic, assign) ViewProductRep* lastDeselectdViewProductRep;
@property (nonatomic, assign) uint viewProductTextMethod;
//@property (nonatomic, assign) OPSProduct* selectedProduct;
@property (nonatomic, retain) NSMutableArray* aSelectedProductRep;
//@property (nonatomic, assign) ViewProductRep* selectedProductRep;
//@property (nonatomic, retain)  IBOutlet TestUI3* testUI;
@property (nonatomic, assign) int modelNavUILevel;
@property (nonatomic, assign) uint pFontSize;

@property (nonatomic, retain) NSMutableArray* aFontSize;
@property (nonatomic, retain) OPSModel* model;
@property (nonatomic, retain) IBOutlet ViewModelToolbar* viewModelToolbar;
@property (nonatomic, retain) UIPopoverController* popoverController;
@property (nonatomic, retain)  IBOutlet OPSNavUI* opsNavUI;
@property (nonatomic, retain) DisplayModelUI* displayModelUI;
@property (nonatomic, retain) IBOutlet NavUIScroll* navUIScrollVC;





//@property (nonatomic, retain) UIActivityIndicatorView  *loadingOverlayActIndi;
//@property (nonatomic, retain) UIProgressView* loadingOverlayProgressBar;
////@property (nonatomic, retain) UIViewController* loadingOverlayVC;
//@property (nonatomic, retain) UIView* loadingOverlay;
//@property (nonatomic, retain) UILabel* loadingOverlayLabel;



/*
- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center ;
- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer;
- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer;
- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer;
*/

- (ViewModelVC*) initWithRootSpatialStruct: (SpatialStructure*) rootSpatialStruct;
- (void) loadViewWithRootSpatialStruct: (SpatialStructure*) rootSpatialStruct;
- (void) relatedAggList:(id)sender;
//-(void) refreshProductLabel;
-(float) currentProductLabelSize;
-(IBAction) listRelAggregate: (id) sender;
- (CGRect) aspectFittedRect:(CGRect)inRect max:(CGRect)maxRect;
//- (void) selectAProduct: (OPSProduct*) product;
- (void) selectAProductRep: (ViewProductRep*) _productRep;

-(void) addAProductRepToSelection:(ViewProductRep*)_productRep;

-(void) removeAProductRepFromSelection:(ViewProductRep*)_productRep;
//- (void) clearSelectedProduct;
- (void) clearSelectedProductRep;

//-(void) showLoadingOverlay:(NSString*) overlayLabelText;
//-(void) hideLoadingOverlay;

-(void) switchToolbarToProductViewToolbar;
-(void) switchToolbarToNavScrollUIToolbar;

- (void) setCustomNavigationBarButtons;
//- (void) showProductRepButton: (CGPoint) touchLocation;

-(OPSProduct*) selectedProductForReference;

-(ViewProductRep*) selectOnlyLastProductRep;

-(ViewProductRep*) lastSelectedProductRep;
- (void) floorList:(id)sender;

-(bool) isSelectingTopLevelSpatialStruct;
-(bool) isSelectingGhostStruct;
-(bool) isSelectingBrowsableProduct;
//-(void) zoomFitSelectedProduct;

-(void) zoomFitSelectedProductWithInvoker:(id)invoker completeAction:(SEL) completeAction actionParam:(id) actionParam;
-(ViewProductRep*) selectOnlyLastDeselctedProductRepInMemory;

-(ViewProductRep*) selectTopLevelProductRep;

-(bool) didSelectOneLevelCascadedProduct;
-(void) backOnLevel;

-(void) drawSiteMap;



-(void) increaseFontSize;
-(void) decreaseFontSize;
-(void) refreshLabel;
-(void) selectSpaceByID:(NSInteger)spaceID;
//-(void) showLoadingOverlay:(NSString*) overlayLabelText;
//-(void) hideLoadingOverlay;
@end


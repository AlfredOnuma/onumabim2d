//
//  OPSNavUI.m
//  ProjectView
//
//  Created by Alfred Man on 10/25/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//
#import "ViewModelNavCntr.h"
#import "OPSNavUI.h"
#import "OPSModel.h"
#import "NAVUIScroll.h"
#import "DisplayModelUI.h"
#import "ProjectViewAppDelegate.h"
#import "ViewModelVC.h"
//#import "testUI3.h"
#import "ViewProductLabel.h"
#import "ViewProductRep.h"
#import "DisplayInfo.h"

#import "OPSProduct.h"
@implementation OPSNavUI
@synthesize delegate;
//@synthesize attachmentInfo;
//-(void) clearAttachmentInfo{
//    if (attachmentInfo!=nil){
//        [attachmentInfo removeFromSuperview];
//        [attachmentInfo release];
//        attachmentInfo=nil;
//    }
//}
//-(void) displayAttachmentInfo{
////    DisplayModelUI* displayModelUI=[[self navUIScroll] DisplayModelUI];
//    
////    ViewProductRep* selectedProductRep=[[self parentViewModelVC] selectedProductRep];
//    
////    OPSProduct* selectedRepProduct=[selectedProductRep product];
//    
//    CGRect initialFrame=self.frame;
//    initialFrame=CGRectOffset(initialFrame, 0, self.bounds.size.height);
//    AttachmentInfo* _attachmentInfo=[[AttachmentInfo alloc] initWithFrame:initialFrame];
//    _attachmentInfo.backgroundColor=[UIColor blackColor];
//    _attachmentInfo.alpha=0.7;
//    self.attachmentInfo=_attachmentInfo;
//    [_attachmentInfo release];
//    
//    [self addSubview:attachmentInfo];
//
////    attachmentInfo.alpha=0.0;
//    /*
//    [UIView transitionFromView:self
//                        toView:attachmentInfo 
//                          duration:1.5 
//                        options:UIViewAnimationTransitionCurlUp 
////                     animations:^{
////                         //animation block                         
//////                         attachmentInfo.alpha=1.0;
////                         //                         loadingView.view.alpha=0.6;
////                     }
//                     completion:^(BOOL finished){
//                         attachmentInfo.alpha=0.3;
//                     }
//     ];
//    */
//    
////    CGRect attachmentInfoRect = attachmentInfo.frame;
////    attachmentInfoRect.origin.y = self.bounds.size.height;
//    [UIView animateWithDuration:0.25
//                          delay:0.0
//                        options: UIViewAnimationCurveEaseOut
//                     animations:^{
////                         attachmentInfo.frame = attachmentInfoRect;
//                         attachmentInfo.frame=self.frame;
//                     } 
//                     completion:^(BOOL finished){
//                         NSLog(@"Done!");
//                     }];    
//    
//    
//    
//    
//    
//    
//}
-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    //    return self.testUI;


    return [[self parentViewModelVC] displayModelUI];
}



-(NavUIScroll*) navUIScroll{
//    NavUIScroll* navUIScroll=nil;
    for (UIView* subView in self.subviews){
        if ([subView isKindOfClass:[NavUIScroll class]]){
            return (NavUIScroll*) subView;
        }
    }   
    return nil;
}

//
//-(bool) shouldDisplayProductViewRepText: (ViewProductRep*) viewProductRep {

/*
 
 -(bool) shouldDisplayProduct: (ViewProductRep*) viewProductRep {      
 NavUIScroll* navUIScroll=[self navUIScroll];
 uint displayTextTriggerSize=50;    
 CGRect screenRectOfNavScrollUI=navUIScroll.bounds;
 
 NSMutableArray* aRefViewProductRep=viewProductRep.product.aRefViewProductRep;
 if (aRefViewProductRep==nil){
 return false;
 }
 CGRect totalProductRepScrollViewRect=CGRectMake(0.0f, 0.0f, 0.0f, 0.0f);    
 CGRect totalProductRepNavUIRect=CGRectMake(0.0f, 0.0f, 0.0f, 0.0f);
 for (uint pViewProductRep=0;pViewProductRep<[aRefViewProductRep count];pViewProductRep++){
 ViewProductRep* refViewProductRep=[aRefViewProductRep objectAtIndex:pViewProductRep];
 
 CGRect productRepScrollViewRect=   [refViewProductRep convertRect:refViewProductRep.bounds toView:navUIScroll];
 CGRect productRepNavUIRect=[refViewProductRep convertRect:refViewProductRep.bounds toView:self];
 
 if (pViewProductRep==0){
 totalProductRepScrollViewRect=productRepScrollViewRect;
 totalProductRepNavUIRect=productRepNavUIRect;
 }else{
 totalProductRepScrollViewRect=CGRectUnion(totalProductRepScrollViewRect,productRepScrollViewRect) ;
 totalProductRepNavUIRect=CGRectUnion(totalProductRepNavUIRect, productRepNavUIRect);
 }
 
 }
 
 
 if (!CGRectIntersectsRect(totalProductRepScrollViewRect,screenRectOfNavScrollUI)){        
 return false; 
 }
 if (totalProductRepNavUIRect.size.width <displayTextTriggerSize && totalProductRepNavUIRect.size.height <displayTextTriggerSize){
 return false;
 }
 
 return true;    
 }
 
 
 */

-(bool) isProductRepInFrame:(ViewProductRep*) viewProductRep{
    
    NavUIScroll* navUIScroll=[self navUIScroll];
    CGRect screenRectOfNavScrollUI=navUIScroll.bounds;
    CGRect uiFrameBBox=[viewProductRep convertRect:viewProductRep.bounds toView:navUIScroll];
    if (!CGRectIntersectsRect(uiFrameBBox,screenRectOfNavScrollUI)){
        return false;
    }    
    return true;
}
//-(bool) shouldDisplayProduct: (ViewProductRep*) viewProductRep {      
//    NavUIScroll* navUIScroll=[self navUIScroll];
//    uint displayTextTriggerSize=200; //50;
//    CGRect screenRectOfNavScrollUI=navUIScroll.bounds;
//    
//    
//
//    CGRect uiFrameBBox=[viewProductRep convertRect:viewProductRep.bounds toView:navUIScroll];                
//    if (!CGRectIntersectsRect(uiFrameBBox,screenRectOfNavScrollUI)){        
//        return false; 
//    }
//    if (uiFrameBBox.size.width <displayTextTriggerSize && uiFrameBBox.size.height <displayTextTriggerSize){
//        return false;
//    }
//    
//    return true;
//    
//    
//    
//}


-(bool) shouldDisplayProductViewRepText: (ViewProductLabel*) viewProductLabel {
    
    
    NavUIScroll* navUIScroll=[self navUIScroll];
    uint displayTextTriggerSize=50;
//    CGRect screenRectOfNavScrollUI=navUIScroll.bounds;    
    
    
    ViewProductRep* viewProductRep=viewProductLabel.productRep;
    
    CGRect uiFrameBBox=[viewProductRep convertRect:viewProductRep.bounds toView:navUIScroll];
    
    
    

//    //    CGRect totalProductRepScrollViewRect=CGRectMake(0.0f, 0.0f, 0.0f, 0.0f);    
//    //    CGRect totalProductRepNavUIRect=CGRectMake(0.0f, 0.0f, 0.0f, 0.0f);
//    //        CGRect productRepScrollViewRect=   [refViewProductRep convertRect:refViewProductRep.bounds toView:navUIScroll];
//    //        CGRect productRepNavUIRect=[refViewProductRep convertRect:refViewProductRep.bounds toView:self];
//    //        	
//    //        if (pViewProductRep==0){
//    //            totalProductRepScrollViewRect=productRepScrollViewRect;
//    //            totalProductRepNavUIRect=productRepNavUIRect;
//    //        }else{
//    //            totalProductRepScrollViewRect=CGRectUnion(totalProductRepScrollViewRect,productRepScrollViewRect) ;
//    //            totalProductRepNavUIRect=CGRectUnion(totalProductRepNavUIRect, productRepNavUIRect);
//    //        }
//    //        
//    //    }
//    //    
    
//    if (!CGRectIntersectsRect(uiFrameBBox,screenRectOfNavScrollUI)){        
//        return false; 
//    }
    if (uiFrameBBox.size.width <displayTextTriggerSize && uiFrameBBox.size.height <displayTextTriggerSize){
        return false;
    }
    
    return true;
    
    
    

    
    
//    
//    NSMutableArray* aRefViewProductRep=viewProductLabel.displayInfo.product.aRefViewProductRep;
//    if (aRefViewProductRep==nil){
//        return false;
//    }
//    CGRect totalProductRepScrollViewRect=CGRectMake(0.0f, 0.0f, 0.0f, 0.0f);    
//    CGRect totalProductRepNavUIRect=CGRectMake(0.0f, 0.0f, 0.0f, 0.0f);
//    for (uint pViewProductRep=0;pViewProductRep<[aRefViewProductRep count];pViewProductRep++){
//        ViewProductRep* refViewProductRep=[aRefViewProductRep objectAtIndex:pViewProductRep];
//        
//        CGRect productRepScrollViewRect=   [refViewProductRep convertRect:refViewProductRep.bounds toView:navUIScroll];
//        CGRect productRepNavUIRect=[refViewProductRep convertRect:refViewProductRep.bounds toView:self];
//        
//        if (pViewProductRep==0){
//            totalProductRepScrollViewRect=productRepScrollViewRect;
//            totalProductRepNavUIRect=productRepNavUIRect;
//        }else{
//            totalProductRepScrollViewRect=CGRectUnion(totalProductRepScrollViewRect,productRepScrollViewRect) ;
//            totalProductRepNavUIRect=CGRectUnion(totalProductRepNavUIRect, productRepNavUIRect);
//        }
//        
//    }
//    
//    
//    if (!CGRectIntersectsRect(totalProductRepScrollViewRect,screenRectOfNavScrollUI)){        
//        return false; 
//    }
//    if (totalProductRepNavUIRect.size.width <displayTextTriggerSize && totalProductRepNavUIRect.size.height <displayTextTriggerSize){
//        return false;
//    }
//    
//    return true;
//    
//
//    
    
    
}


- (void)refreshProductThread:(id)inData//(NSConnection *)connection
{
//    
//    if (isZooming) {
//        
//        return;
//    }
//    if (isZooming) {return;}
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    if (!isZooming){
            ViewProductRep* viewProductRep=(ViewProductRep*) inData;
        [viewProductRep setNeedsDisplay];
    }
//    [self performSelectorOnMainThread:@selector(refreshProductThreadDone:) withObject:nil waitUntilDone:NO];
    [pool drain];
//    [pool release];
}

- (void)refreshProductThreadDone:(id)inData
{
    
}

- (void) refreshProductLabel{

    if (isZooming) {NSLog(@"Zooming");return;}
    NSLog(@"RefreshLabel");
    ViewModelNavCntr* viewModelNavCntr=(ViewModelNavCntr*) [self parentViewModelVC].navigationController;
    if ([viewModelNavCntr isEnteringGhostSpace] ){
        NSLog(@"Ghost Entering");
        return;
    }
    
    
    NavUIScroll* navUIScroll=[self navUIScroll];
    DisplayModelUI* displayModelUI=[navUIScroll displayModelUI];
    
    
    for (UIView* subview in [[displayModelUI productViewLabelLayer] subviews]){
        if (![subview isKindOfClass:([ViewProductLabel class])]){continue;}
        ViewProductLabel* viewProductLabel=(ViewProductLabel*) subview;
        
        
        
        ViewProductRep* viewProductRep=viewProductLabel.productRep;
        if ([self isProductRepInFrame:viewProductRep]){
            if ([self shouldDisplayProductViewRepText: viewProductLabel ]){//screenRectOfNavScrollUI:screenRectOfNavScrollUI]){
                [viewProductLabel display];
            }
        }else{
            
            if ([self shouldDisplayProductViewRepText: viewProductLabel ]){//screenRectOfNavScrollUI:screenRectOfNavScrollUI]){
                if ([ProjectViewAppDelegate useThread]){
                    [NSThread detachNewThreadSelector:@selector(refreshProductLabelThread:) toTarget:self withObject:viewProductLabel];
                }
            }
        }
            
            
//            if ([ProjectViewAppDelegate useThread]){
//                [NSThread detachNewThreadSelector:@selector(refreshProductLabelThread:) toTarget:self withObject:viewProductLabel];
//            }
//        }
        
    }
    
    ////    [displayModelUI bringSubviewToFront: displayModelUI.productViewLabelLayer];
    
}


-(void) refreshProduct{
    return;
    ViewModelNavCntr* viewModelNavCntr=(ViewModelNavCntr*) [self parentViewModelVC].navigationController;
    if ([viewModelNavCntr isEnteringGhostSpace] ){
        NSLog(@"Ghost Entering");
        return;
    }
    
    if (isZooming) {return;}
    NavUIScroll* navUIScroll=[self navUIScroll];
    DisplayModelUI* displayModelUI=[navUIScroll displayModelUI];    
    
        
     for (UIView* subview in displayModelUI.spatialStructViewLayer.subviews){
         if (![subview isKindOfClass:([ViewProductRep class])]){continue;}        
         ViewProductRep* viewProductRep=(ViewProductRep*) subview;  
         if ([self isProductRepInFrame:viewProductRep]){
//         if ([self shouldDisplayProduct: viewProductRep ]){//screenRectOfNavScrollUI:screenRectOfNavScrollUI]){
             [viewProductRep setNeedsDisplay];

         }else{
             if ([ProjectViewAppDelegate useThread]){
                 [NSThread detachNewThreadSelector:@selector(refreshProductThread:) toTarget:self withObject:viewProductRep];
             }
         }
     
     }
     for (UIView* subview in displayModelUI.spatialStructSlabViewLayer.subviews){
         if (![subview isKindOfClass:([ViewProductRep class])]){continue;}
         ViewProductRep* viewProductRep=(ViewProductRep*) subview;

         
//         if ([self shouldDisplayProduct: viewProductRep ]){//screenRectOfNavScrollUI:screenRectOfNavScrollUI]){
         if ([self isProductRepInFrame: viewProductRep ]){//screenRectOfNavScrollUI:screenRectOfNavScrollUI]){
                 [viewProductRep setNeedsDisplay];
         }else{
             if ([ProjectViewAppDelegate useThread]){
                 [NSThread detachNewThreadSelector:@selector(refreshProductThread:) toTarget:self withObject:viewProductRep]; 
             }
         }
     
     }
    
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    if ([appDele modelDisplayLevel]==0){
//        [[[self navUIScroll] displayModelUI] redrawOnumaPin];
//    }
//    
    
}

- (void)refreshProductLabelThread:(id)inData//(NSConnection *)connection
{

    
    
//    if (isZooming) {return;}
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    if (!isZooming){
        ViewProductLabel* viewProductLabel=(ViewProductLabel*) inData;
        [viewProductLabel display];
        [viewProductLabel hide];    
        [self performSelectorOnMainThread:@selector(refreshProductLabelThreadDone:) withObject:nil waitUntilDone:NO];
    }
    [pool release];
}

- (void)refreshProductLabelThreadDone:(id)inData
{
    
}
- (void) showHideProductLable{
    NavUIScroll* navUIScroll=[self navUIScroll];
    if (navUIScroll!=nil){
        DisplayModelUI* displayModelUI=[navUIScroll displayModelUI];        
        
        for (UIView* subview in [[displayModelUI productViewLabelLayer] subviews]){        
            if (![subview isKindOfClass:([ViewProductLabel class])]){continue;}        
            ViewProductLabel* viewProductLabel=(ViewProductLabel*) subview;     
            if ([self shouldDisplayProductViewRepText: viewProductLabel ]){//screenRectOfNavScrollUI:screenRectOfNavScrollUI]){
                viewProductLabel.hidden=false;
            }else{
                viewProductLabel.hidden=true;            
            }
            
        }      
    }
}



- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{

//    NSLog(@"endDrag");
    [self showHideProductLable];
}
- (void)scrollViewDidEndZooming:(UIScrollView *)aScrollView withView:(UIView *)view atScale:(float)scale {        
//    NSLog(@"endZooming");
    isZooming=false;
    zoomingEndScale=[self navUIScroll].zoomScale;           
    [self refreshProduct];
    [self refreshProductLabel];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    if ([appDele modelDisplayLevel]==0){
        [[[self navUIScroll] displayModelUI] redrawOnumaPin];
    }
    
}

-(void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view{
    isZooming=true;                
    zoomingStartScale=[self navUIScroll].zoomScale;
}











//-(void) drawRect:(CGRect)rect{
//    UIView* t=[[UIView alloc] initWithFrame:CGRectMake(0,0,400,400)];
//    t.backgroundColor=[UIColor redColor];
//    [self addSubview:t];
//}

/*
@synthesize modelNavUILevel;
@synthesize navUIScrollVC;
@synthesize displayModelUI;

@synthesize testUI;

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.navUIScrollVC;
}
-(UIScrollView*) getScrollView{
    return self.navUIScrollVC;
}
*/
/*


- (void)drawRect:(CGRect)rect{
    
    self.backgroundColor=[UIColor clearColor];
    
    
    CGMutablePathRef path = CGPathCreateMutable();    
    
    
    CGPathMoveToPoint(path,NULL,0,44);  
    
    CGPathAddLineToPoint(path,NULL,1024,44);
    
    CGPathAddLineToPoint(path,NULL,1024/2,((768-20-44-44)-44
                                           ));
    //    CGPathAddLineToPoint(path,NULL,0,400);    

    CGContextRef context0 = UIGraphicsGetCurrentContext();     
    CGContextBeginPath(context0);
    CGContextAddPath(context0,path);
    
    CGContextSetRGBStrokeColor(context0, 1.0, 1.0, 1.0, 1.0);
    CGContextSetRGBFillColor(context0, 1.0, 0.0, 0.0, 0.6);    
    
    CGContextSetLineWidth(context0, 5.0);
    CGContextFillPath(context0);
    CGContextClosePath(context0);
    
    return;
    
}
 */
- (ViewModelVC*) parentViewModelVC{
    return (ViewModelVC*) self.delegate;
}
- (void) awakeFromNib
{
    [super awakeFromNib]; 
    
}

-(id)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    if (self) {

        // Custom initialization
    }
    return self;
}
 */

- (void)dealloc
{
    
//    [attachmentInfo release];
    [super dealloc];
}
/*
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
 */

#pragma mark - View lifecycle
/*
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
 // e.g. self.myOutlet = nil;
}
 */
- (void) willMoveToSuperview:(UIView *)newSuperview{
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

 //
//  EverNoteNoteBookBrowserTVC.m
//  ProjectView
//
//  Created by onuma on 31/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "EverNoteNoteBookBrowserTVC.h"
#import "EvernoteSDK.h"

#import "EverNoteNoteBrowserVC.h"
//#import "EverNoteNoteBookBrowserCell.h"


@interface EverNoteNoteBookBrowserTVC ()

@end
@implementation EverNoteNoteBookBrowserTVC

@synthesize notebooks=_notebooks;

@synthesize delegate=_delegate;
//@synthesize selectedNotebook=_selectedNotebook;
@synthesize selectedIndex=_selectedIndex;

@synthesize everNoteNoteBrowserVC=_everNoteNoteBrowserVC;

-(id)  init:(EverNoteNoteBrowserVC*) everNoteNoteBrowserVC{
    self=[super init];
    if (self){
        self.everNoteNoteBrowserVC=everNoteNoteBrowserVC;
    
    
    }
    return self;
}

-(void) dealloc{
    [_notebooks release];_notebooks=nil;
    [_selectedIndex release];_selectedIndex=nil;
//    [_selectedNotebook release];_selectedNotebook=nil;
    [super dealloc];
}




- (void)setSelectedNotebookWithGUID:(NSString *)notebookGUID
{
    if(notebookGUID) {
        [[EvernoteNoteStore noteStore] getNotebookWithGuid:notebookGUID success:^(EDAMNotebook *notebook) {
            self.everNoteNoteBrowserVC.selectedNotebook = notebook;
            [self.tableView reloadData];
        } failure:^(NSError *error) {
            ;
        }];
    }
    else {
        [[EvernoteNoteStore noteStore] getDefaultNotebookWithSuccess:^(EDAMNotebook *notebook) {
            self.everNoteNoteBrowserVC.selectedNotebook=notebook;

            [self.tableView reloadData];
        } failure:^(NSError *error) {
            ;
        }];
    }
}

- (void)setSelectedNotebookWithName:(NSString *)notebookName
{
    [[EvernoteNoteStore noteStore] listNotebooksWithSuccess:^(NSArray *notebooks) {
        for (EDAMNotebook* notebook in notebooks) {
            if([notebook.name isEqualToString:notebookName]) {
                self.everNoteNoteBrowserVC.selectedNotebook = notebook;
                return;
            }
        };
    } failure:^(NSError *error) {
        ;
    }];
}

//- (IBAction)accept:(id)sender
//{
//    [self dismissViewControllerAnimated:YES completion:^{
//        if ([self.delegate respondsToSelector:@selector(notebookChooserController:didSelectNotebook:)])
//            [self.delegate notebookChooserController:self didSelectNotebook:self.everNoteNoteBrowserVC.selectedNotebook];
//    }];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self.tableView registerClass:[EverNoteNoteBookBrowserCell class] forCellReuseIdentifier:@"EverNoteNoteBookBrowserCell"];
//    self.navigationItem.hidesBackButton = YES;
//    if (!self.navigationItem.rightBarButtonItem) {
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
//                                                  initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(accept:)];
//    }
    [[EvernoteNoteStore noteStore] listNotebooksWithSuccess:^(NSArray *notebooks) {
        NSSortDescriptor* sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
        self.notebooks = [notebooks sortedArrayUsingDescriptors:@[sortDesc]];

        [self.tableView reloadData];
//        if(self.everNoteNoteBrowserVC.selectedNotebook == nil) {
//            [self setSelectedNotebookWithGUID:nil];
//        }
//        else {
////            [self setSelectedNotebook:self.selectedNotebook];
//        }
    } failure:^(NSError *error) {
        ;
    }];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (CGSize)contentSizeForViewInPopover
{
    return CGSizeMake(320, 480);
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.notebooks count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        
    static NSString *identifier = @"EverNoteNoteBookBrowserCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease ];
//        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        
        UIView *selectionView = [[UIView alloc]initWithFrame:cell.bounds];
        [selectionView setBackgroundColor:[UIColor grayColor]];
        cell.selectedBackgroundView = selectionView;
        [selectionView release];
    }
    
    if (indexPath.row==0){
        cell.textLabel.text=@"All Books";
//        NSLog(@"selected notebook:%@",self.everNoteNoteBrowserVC.selectedNotebook.guid);
        if (self.everNoteNoteBrowserVC.selectedNotebook==nil){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }else{
        EDAMNotebook *notebook = (self.notebooks)[indexPath.row -1];
        if (self.everNoteNoteBrowserVC.selectedNotebook!=nil && [self.everNoteNoteBrowserVC.selectedNotebook.guid isEqualToString:notebook.guid]){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.textLabel.text = [notebook name];
        cell.textLabel.highlightedTextColor=[UIColor whiteColor];
    }
    
//    if ([self.everNoteNoteBrowserVC.selectedNotebook.guid isEqualToString:notebook.guid]) {
//        self.selectedIndex = indexPath;
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    } else {
//        cell.accessoryType = UITableViewCellAccessoryNone;
//    }
    return cell ;
}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath0:(NSIndexPath *)indexPath
//{
//    
//
//    
//    static NSString *identifier = @"EverNoteNoteBookBrowserCell";
//    EverNoteNoteBookBrowserCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
//    
//    if (!cell) {
//        cell = [[[EverNoteNoteBookBrowserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease ];
//        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
//    }
//    EDAMNotebook *notebook = (self.notebooks)[indexPath.row];
//    [cell setNotebook:notebook];
//    if ([self.everNoteNoteBrowserVC.selectedNotebook.guid isEqualToString:notebook.guid]) {
//        self.selectedIndex = indexPath;
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    } else {
//        cell.accessoryType = UITableViewCellAccessoryNone;
//    }
//    return cell ;
//}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if ([self numberOfSectionsInTableView:tableView] == (section + 1)) {
        return [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
    }
    return nil;
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.row==0){
        self.everNoteNoteBrowserVC.selectedNotebook = nil;
    }else{
        self.everNoteNoteBrowserVC.selectedNotebook = (self.notebooks)[indexPath.row -1];
    }
    [self.everNoteNoteBrowserVC setCurrentNote:0];
    [self.everNoteNoteBrowserVC loadNotes];
    [self.tableView reloadData];
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];

//    [self.everNoteNoteBrowserVC viewDidLoad];
}

@end

//@implementation NotbookChooserViewControllerCell
//
//- (void)setNotebook:(EDAMNotebook *)notebook
//{
//    _notebook = notebook;
//    self.textLabel.text = [notebook name];
//}
//
//
//
//
//
//


















//
//
//
//- (id)initWithStyle:(UITableViewStyle)style
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    
//    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"EverNoteNoteBookCell"];
//    // Uncomment the following line to preserve selection between presentations.
//    // self.clearsSelectionOnViewWillAppear = NO;
// 
//    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//}
//
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//#pragma mark - Table view data source
//
////- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
////{
////#warning Potentially incomplete method implementation.
////    // Return the number of sections.
////    return 0;
////}
////
////- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
////{
////#warning Incomplete method implementation.
////    // Return the number of rows in the section.
////    return 0;
////}
////
////- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
////{
////    static NSString *CellIdentifier = @"Cell";
////    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
////    
////    // Configure the cell...
////    
////    return cell;
////}
////
//
//
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return self.notebooks.count;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"EverNoteNoteBookCell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    // Configure the cell...
//    EDAMNotebook* notebook = self.notebooks[indexPath.row];
//    [[cell textLabel] setText:notebook.name];
//    return cell;
//}
//
//
///*
//// Override to support conditional editing of the table view.
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Return NO if you do not want the specified item to be editable.
//    return YES;
//}
//*/
//
///*
//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        // Delete the row from the data source
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    }   
//    else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//    }   
//}
//*/
//
///*
//// Override to support rearranging the table view.
//- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
//{
//}
//*/
//
///*
//// Override to support conditional rearranging of the table view.
//- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Return NO if you do not want the item to be re-orderable.
//    return YES;
//}
//*/
//
//#pragma mark - Table view delegate
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Navigation logic may go here. Create and push another view controller.
//    /*
//     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
//     // ...
//     // Pass the selected object to the new view controller.
//     [self.navigationController pushViewController:detailViewController animated:YES];
//     [detailViewController release];
//     */
//}

//@end

//
//  DisplayInfo.h
//  ProjectView
//
//  Created by Alfred Man on 1/19/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "RepresentationItem.h"
@class OPSProduct;
@interface DisplayInfo : RepresentationItem{
//    NSMutableArray *aDisplayLabelText;
    OPSProduct* product;
    CGPoint displacement;
}
//@property (nonatomic, retain) NSMutableArray* aDisplayLabelText;
@property (nonatomic, assign) OPSProduct* product;
@property (nonatomic, assign) CGPoint displacement;
- (NSString*) displayLabelText;
- (id) initWithProduct: (OPSProduct*) _product displacement:(CGPoint) _displacement; //aDisplayLabelText:(NSMutableArray*) _aDisplayLabelText;
@end

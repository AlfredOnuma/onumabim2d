//
//  AROverlayViewController.h
//  ProjectView
//
//  Created by onuma on 11/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "CaptureSessionManager.h"
@class ViewModelToolbar;
@class ViewModelVC;
@interface AROverlayViewController : UIViewController {
    ViewModelToolbar* _toolbar;
    CGRect _initialFrame;
    CGRect _offsetFrame;
    NSString* _photoCapturedPath;
    UIViewController* _prevVC;
    UIButton* _cameraButton;
    uint _numImageTaken;
}
@property (nonatomic, assign) uint numImageTaken;
@property (nonatomic, retain) UIButton* cameraButton;
@property (nonatomic, assign) UIViewController* prevVC;
@property (nonatomic, retain) NSString* photoCapturedPath;
@property (nonatomic, assign) CGRect initialFrame;
@property (nonatomic, assign) CGRect offsetFrame;
@property (nonatomic, assign) ViewModelToolbar* toolbar;
@property (retain) CaptureSessionManager *captureManager;
@property (nonatomic, retain) UILabel *scanningLabel;


//-(id) initwithToolBar:(ViewModelToolbar*)toolbar;
-(id) initwithToolBar:(ViewModelToolbar*)toolbar prevVC:(UIViewController*)prevVC;
@end

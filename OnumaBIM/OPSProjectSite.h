//
//  OPSProjectSite.h
//  ProjectView
//
//  Created by Alfred Man on 1/3/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableCellTitleObject.h"

@interface OPSProjectSite : TableCellTitleObject< NSURLConnectionDelegate> {
//    uint ID;
//    NSString* name;
//    uint shared;
    
    uint _numFileToUpload;
    uint _numFileUploadSuccess;
    uint _numFileUploadFail;
    uint _numCommentToUpload;
    uint _numCommentUploadSuccess;
    uint _numCommentUploadFail;
    
    
    
    id _afterUploadSuccessInvoker;
    SEL _afterUploadSuccessCompleteAction;
    NSArray* _afterUploadSuccessParamArray;
    
    
    id _afterUploadFailInvoker;
    SEL _afterUploadFailCompleteAction;
    NSArray* _afterUploadFailParamArray;
    
    
    NSMutableArray* _aAttachmentInfoTableUploadFile;
    NSMutableArray* _aAttachmentInfoTableUploadComment;
    NSUInteger projectID;
    NSString* projectName;    
//    NSString* iconName;
    
    NSString* dbPath;
    NSString* _versionLagStr;
    
    NSMutableData* connectionData;
    NSURLConnection* theConnection;
    
//    NSString* _version;
    uint _version;
    bool _bNeedUpdate;
    
    

}
//@property (nonatomic, assign) uint ID;
//@property (nonatomic, retain) NSString* name;
//@property (nonatomic, assign) uint shared;
@property (nonatomic, assign) id afterUploadSuccessInvoker;
@property (nonatomic, assign) SEL afterUploadSuccessCompleteAction;
@property (nonatomic, retain) NSArray* afterUploadSuccessParamArray;


@property (nonatomic, assign) id afterUploadFailInvoker;
@property (nonatomic, assign) SEL afterUploadFailCompleteAction;
@property (nonatomic, retain) NSArray* afterUploadFailParamArray;


@property (nonatomic, assign) uint numFileToUpload;
@property (nonatomic, assign) uint numFileUploadSuccess;
@property (nonatomic, assign) uint numFileUploadFail;
@property (nonatomic, assign) uint numCommentToUpload;
@property (nonatomic, assign) uint numCommentUploadSuccess;
@property (nonatomic, assign) uint numCommentUploadFail;


@property (nonatomic, retain) NSMutableArray* aAttachmentInfoTableUploadFile;
@property (nonatomic, retain) NSMutableArray* aAttachmentInfoTableUploadComment;

@property (nonatomic, assign) NSUInteger projectID;
@property (nonatomic, retain) NSString* projectName;
//@property (nonatomic, retain) NSString* iconName;

@property (nonatomic, retain) NSString* dbPath;




@property (nonatomic, retain) NSMutableData* connectionData;    
@property (nonatomic, retain) NSURLConnection* theConnection;
@property (nonatomic, assign) uint version;

@property (nonatomic, retain) NSString* versionLagStr;
@property (nonatomic, assign) bool bNeedUpdate;
//@property (nonatomic, retain) NSString* version;
- (NSString*) toDictionaryObject;
- (void) checkuploadFileStatus;
- (void) checkuploadCommentStatus;
//- (NSComparisonResult)compareShared:(OPSProjectSite *)otherObject ;


//- (NSComparisonResult)compareProjectID:(OPSProjectSite *)otherObject ;

-(id)copyWithZone:(NSZone *)zone;
- (id) init:(NSUInteger) _ID name:(NSString*) _name shared:(NSUInteger) _shared projectID:(NSUInteger)_projectID projectName:(NSString*) _projectName iconName:(NSString*) _iconName;

-(void) removeEmptyCommentEmptyPhotoAttachmentFromDatabase;


-(void) updateLoadingOverlayLabelStatus;
//-(int)localModifiedDate;
-(bool)isLocalProjectAttachmentEmpty;
- (id) init:(NSString*) pListStr;
-(int) getLocalDatabaseModifiedDate;

-(void) uploadProjectSite:(NSArray*) paramArray;


//- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
//- (void)connectionDidFinishLoading:(NSURLConnection *)connection afterUploadSuccessInvoker:(id)afterUploadSuccessInvoker afterUploadSuccessCompleteAction:(SEL) afterUploadSuccessCompleteAction actionParamArray:(NSArray*) paramArray;


-(void) trashAllAttachmentEntry;

//- (int) readModifiedDateFromServer;
- (int) liveModifiedDate;

-(bool) checkSiteHasAttachment;

-(void) readVersionLagStr;
-(void) uploadProjectSiteComment;
-(void) finishUpload;
-(void) failUpload;
//- (void) updateDBFromServer:(NSArray*) paramArray;
@end

//
//  BIMMailGroup.h
//  ProjectView
//
//  Created by Alfred Man on 4/8/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BIMMailGroup : NSObject{
    NSString* name;
    NSMutableArray* aBIMMailRecipient;
    bool checked;
}
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSMutableArray* aBIMMailRecipient;
@property (nonatomic, assign) bool checked;
-initWithGroupName:(NSString*) _name aBIMMailRecipient:(NSMutableArray*)_aBIMMailRecipient;
@end

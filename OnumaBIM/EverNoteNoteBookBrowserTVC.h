//
//  EverNoteNoteBookBrowserTVC.h
//  ProjectView
//
//  Created by onuma on 31/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EDAMNotebook;
@class EverNoteNoteBrowserVC;
@class EverNoteNoteBookBrowserTVC;
@protocol EverNoteNoteBookBrowserTVCDelegate <NSObject>
-(void)notebookChooserController:(EverNoteNoteBookBrowserTVC*)controller didSelectNotebook:(EDAMNotebook*)notebook;
@end

@interface EverNoteNoteBookBrowserTVC : UITableViewController{
    NSArray* _notebooks;
//    EDAMNotebook* _selectedNotebook;
    NSIndexPath* _selectedIndex;
    id <EverNoteNoteBookBrowserTVCDelegate> _delegate;
    EverNoteNoteBrowserVC* _everNoteNoteBrowserVC;
}
@property (nonatomic, assign) EverNoteNoteBrowserVC* evernoteNoteBrowserVC;
@property(nonatomic, assign) id <EverNoteNoteBookBrowserTVCDelegate> delegate;
@property (nonatomic,retain) NSArray* notebooks;
//@property(nonatomic, retain) EDAMNotebook *selectedNotebook;
@property(nonatomic, retain) NSIndexPath *selectedIndex;
@property(nonatomic, assign) EverNoteNoteBrowserVC* everNoteNoteBrowserVC;
- (void)setSelectedNotebookWithGUID:(NSString *)notebookGUID;
- (void)setSelectedNotebookWithName:(NSString *)notebookName;

-(id) init:(EverNoteNoteBrowserVC*)everNoteNoteBrowserVC;
//- (IBAction)accept:(id)sender;

@end

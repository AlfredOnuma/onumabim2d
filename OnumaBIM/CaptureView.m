//
//  CaptureView.m
//  ProjectView
//
//  Created by Alfred Man on 3/14/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//
#import "CaptureView.h"

// Private
@interface CaptureView (/* Private */)
- (void)settingImageFromView:(UIView *)view;
@end

// Public
@implementation CaptureView

@synthesize imageCapture = _imageCapture;
@synthesize captureFrame= _captureFrame;
// Standard
- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        // Initialization code.
    }
    return self;
}

// Init
- (id)initWithView:(UIView *)view captureFrame:(CGRect)captureFrame{
    //    if ((self = [super initWithFrame:[view frame]])) {
//    if ((self = [super initWithFrame: CGRectMake(0, 0,150,200)])) {    
    if (self = [super initWithFrame: CGRectMake(0, 0,captureFrame.size.width,captureFrame.size.height) ]){
        // Initialization code. 
        _captureFrame=captureFrame;
//        self.captureFrame=captureFrame;
        [self settingImageFromView:view];
    }
    return self;  
}


- (void)settingImageFromView:(UIView *)view {
    //    CGRect rect = [view bounds];  
//    CGRect rect = CGRectMake(100, 150,150,200);  
    
//    CGRect rect = self.captureFrame;// CGRectMake(0, 0,1024*2,(768-20-44-44)*2);    
//    UIGraphicsBeginImageContext(rect.size);  
    UIGraphicsBeginImageContext(_captureFrame.size);      
    CGContextRef context = UIGraphicsGetCurrentContext();  
    [view.layer renderInContext:context];  
    UIImage *imageCaptureRect;
    
    imageCaptureRect = UIGraphicsGetImageFromCurrentImageContext();  
    _imageCapture = [imageCaptureRect retain];
    //    _imageCapture = UIGraphicsGetImageFromCurrentImageContext();  
    //    [_imageCapture retain];
    
//    UIImage *imageCaptureRect0=[[UIImage alloc] init];
//    
//    imageCaptureRect0 = UIGraphicsGetImageFromCurrentImageContext();  
//    //    _imageCapture = imageCaptureRect;
//    self.imageCapture=imageCaptureRect0;
//    [imageCaptureRect0 release];
    
    
    
    
    UIGraphicsEndImageContext();   
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
    CGPoint accPoint = CGPointMake(0,0);
    [_imageCapture drawAtPoint:accPoint];
}


- (void)dealloc {
    [_imageCapture release];
    [super dealloc];
}


//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}



@end

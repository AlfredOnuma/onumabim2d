//
//  OPSProjectSite.m
//  ProjectView
//
//  Created by Alfred Man on 1/3/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import "OPSProjectSite.h"
#import "ProjectViewAppDelegate.h"
#import "OPSStudio.h"
#import "CXMLDocument.h"
//#import "OPSLocalProjectSiteInAttachmentInfoDetail.h"
#import "OPSSiteVersion.h"

#import "AttachmentInfoTableUploadFile.h"
#import "AttachmentInfoTableUploadComment.h"


@implementation OPSProjectSite

//@synthesize ID;
//@synthesize name;
//@synthesize iconName;
//@synthesize shared;
@synthesize aAttachmentInfoTableUploadComment=_aAttachmentInfoTableUploadComment;
@synthesize aAttachmentInfoTableUploadFile=_aAttachmentInfoTableUploadFile;
@synthesize projectID;
@synthesize projectName;
@synthesize dbPath;
@synthesize version=_version;

@synthesize connectionData;
@synthesize theConnection;
@synthesize versionLagStr=_versionLagStr;
@synthesize bNeedUpdate=_bNeedUpdate;


@synthesize numCommentToUpload=_numCommentToUpload;
@synthesize numCommentUploadFail=_numCommentUploadFail;
@synthesize numCommentUploadSuccess=_numCommentUploadSuccess;

@synthesize numFileToUpload=_numFileToUpload;
@synthesize numFileUploadFail=_numFileUploadFail;
@synthesize numFileUploadSuccess=_numFileUploadSuccess;

@synthesize afterUploadSuccessInvoker=_afterUploadSuccessInvoker;
@synthesize afterUploadSuccessCompleteAction=_afterUploadSuccessCompleteAction;
@synthesize afterUploadSuccessParamArray=_afterUploadSuccessParamArray;

@synthesize afterUploadFailInvoker=_afterUploadFailInvoker;
@synthesize afterUploadFailCompleteAction=_afterUploadFailCompleteAction;
@synthesize afterUploadFailParamArray=_afterUploadFailParamArray;

static sqlite3* database;

uint const OPSProjectSite_CONFIRM_UPLOADNOINTERNET=1;

uint const OPSProjectSite_CONFIRM_UPLOADFINISH=2;
//uint const OPSProjectSite_LocalSiteNeedUpdate=3;


//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    switch (alertView.tag) {
//        case OPSProjectSite_CONFIRM_UPLOADNOINTERNET:
//        {
//            switch (buttonIndex) {
//                case 0: // cancel
//                {
//                    //                    NSLog(@"Delete was cancelled by the user");
//                }
//                    break;
//                case 1: // delete
//                {
//                    //                    [self uploadProject:nil event:nil];
//                    //                    [self trashAttachmentEntry];
//                    // do the delete
//                }
//                    break;
//            }
//            
//            break;
//        case LocalProjectList_UIAlertViewTag_TrashSiteConfirm:
//            {
//                switch (buttonIndex) {
//                    case 0: // cancel
//                    {
//                        //                    NSLog(@"Delete was cancelled by the user");
//                    }
//                        break;
//                    case 1: // delete
//                    {
//                        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//                        [appDele displayLoadingViewOverlay:@"Deleting Project Site" invoker:self completeAction:@selector(deleteProjectSite:) actionParamArray:[NSArray arrayWithObjects:selectedIndexPath,nil]];
//                        
//                        //                    [self uploadProject:nil event:nil];
//                        //                    [self trashAttachmentEntry];
//                        // do the delete
//                    }
//                        break;
//                }
//                
//            }
//            break;
//        case LocalProjectList_UIAlertViewTag_LocalSiteNeedUpdate:
//            {
//                switch (buttonIndex) {
//                    case 0: // cancel
//                    {
//                        //                    NSLog(@"Delete was cancelled by the user");
//                    }
//                        break;
//                    case 1: // update
//                    {
//                        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//                        OPSProjectSite* projectSite=[appDele activeProjectSite];
//                        OPSProjectSiteVersionUpdater* projectSiteUpdater=[[OPSProjectSiteVersionUpdater alloc] initWithOPSProjectSite: projectSite localProjectListVC:self];
//                        self.versionUpdater=projectSiteUpdater;
//                        [projectSiteUpdater release];
//                        [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Updating Database from Server for site: %@",  projectSite.name] invoker:self.versionUpdater completeAction:@selector(updateDBFromServer:) actionParamArray:nil];
//                        
//                        //                    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Updating Database from Server for site: %@",  projectSite.name] invoker:self.versionUpdater completeAction:@selector(updateDBFromServer:) actionParamArray: [NSArray arrayWithObjects:self,nil]];
//                        
//                        //                    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Updating Database from Server for site: %@",  projectSite.name] invoker:self completeAction:@selector(copyModelFromServer:) actionParamArray:[NSArray arrayWithObjects:[NSNumber numberWithBool:bNoGraphicData], nil]];
//                        
//                    }
//                        break;
//                }  
//                
//            }
//            break;
//        default:
//            //            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");
//            break;
//        }
//    }	
//}
//
//
//
//
//
//- (void) openProjectToDisplay{
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    [appDele setModelDisplayLevel:0];
//    appDele.activeProjectSite=self;
//    [appDele displayViewModelNavCntr: nil];
//    
//}
//
//
//-(void) attemptLaunchSiteToDisplay{
//    
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    appDele.activeProjectSite=self;
//    
//    
//    uint appCurrentVersion=[appDele readAppCurrentVersion];
//    if (self.version<appCurrentVersion){
//        [self readVersionLagStr];
//        if ([self bNeedUpdate]){
//            [self alertSiteNeedUpdate:nil];
//        }else{
//            [self openProjectToDisplay];
//        }
//        
//    }else{
//        [self openProjectToDisplay];
//    }
//
//}
//
//
//
//-(void) alertNoInternet:(id)sender{
//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle: @"No Internet Available"
//                          message: @""
//                          delegate: self
//                          cancelButtonTitle: nil
//                          otherButtonTitles: @"ok", nil];
//    alert.tag = OPSProjectSite_CONFIRM_UPLOADNOINTERNET; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//    [alert show];
//    [alert release];
//    
//    
//    
//}
//
//-(void) alertSiteNeedUpdate:(id)sender{
//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle: @"The downloaded site need update from Onuma Live."
//                          message: @""
//                          delegate: self
//                          cancelButtonTitle: @"Cancel"
//                          otherButtonTitles: @"Update", nil];
//    alert.tag = OPSProjectSite_LocalSiteNeedUpdate; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//    [alert show];
//    [alert release];
//}
//
//        



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    switch (alertView.tag) {
        case OPSProjectSite_CONFIRM_UPLOADNOINTERNET:
        {
//            [self.attachmentInfoDetailToClose actionOK:nil];
            if (self.afterUploadFailInvoker!=nil&&self.afterUploadFailCompleteAction!=nil){
                [self.afterUploadFailInvoker performSelector:self.afterUploadFailCompleteAction withObject:self.afterUploadFailParamArray];
            }
            [appDele removeLoadingViewOverlay];
        }
            break;
        case OPSProjectSite_CONFIRM_UPLOADFINISH:
        {
            if (self.afterUploadSuccessInvoker!=nil&&self.afterUploadSuccessCompleteAction!=nil){
                [self.afterUploadSuccessInvoker performSelector:self.afterUploadSuccessCompleteAction withObject:self.afterUploadSuccessParamArray];
            }
            [appDele removeLoadingViewOverlay];            
//            [self closeAttachmentInfoDetailAndRemoveEmptySQLTable];
        }
            break;
            
        default:
            //            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");
            break;
    }
}



//
//- (void) updateDBFromServer:(NSArray*) paramArray{
//    
//}

-(bool) checkSiteHasAttachment{
    bool hasAttachment=false;
    if (![self dbPath]) return hasAttachment;
    database=nil;
    
    
    if (sqlite3_open ([[self dbPath] UTF8String], &database) == SQLITE_OK) {   
        
        
        sqlite3_stmt *selectAttachCommentStmt;
//        const char* selectAttachCommenteSql=[[NSString stringWithFormat:@"SELECT COUNT(*) AS num FROM AttachComment"
//                                              ] UTF8String];                      
        const char* selectAttachCommenteSql=[[NSString stringWithFormat:@"SELECT * FROM AttachComment"
                                              ] UTF8String];                      
        
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, selectAttachCommenteSql, -1, &selectAttachCommentStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(selectAttachCommentStmt) == SQLITE_ROW) { 
                
                
                sqlite3_finalize(selectAttachCommentStmt); 
                sqlite3_close(database);                
                return true;
            }
        }
        
        
        
        sqlite3_stmt *selectAttachFileStmt;
//        const char* selectAttachFileSql=[[NSString stringWithFormat:@"SELECT COUNT(*) AS num FROM AttachFile"
//                                          ] UTF8String];                      
        
        const char* selectAttachFileSql=[[NSString stringWithFormat:@"SELECT * FROM AttachFile"
                                          ] UTF8String];          
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, selectAttachFileSql, -1, &selectAttachFileStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(selectAttachFileStmt) == SQLITE_ROW) { 
                
                sqlite3_finalize(selectAttachFileStmt);
                sqlite3_close(database);                
                return true;
            }
        }
        
        
        sqlite3_finalize(selectAttachFileStmt);
        sqlite3_close(database);
        
    }else{
        sqlite3_close(database);
    }
    sqlite3_close(database);
    

    return hasAttachment;
}



-(void) trashAnAttachmentEntry{//:(id)sender{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
    
    //    OPSProjectSite* projectSite=appDele.activeProjectSite;
    //    sqlite3* database=nil;
    
    
    database=nil;
    
    
    if (sqlite3_open ([[self dbPath] UTF8String], &database) == SQLITE_OK) {
        
        
        
        
        sqlite3_stmt *selectAttachFileStmt;
        const char* selectAttachFileSql=[[NSString stringWithFormat:@"select fileName from AttachFile"
                                          ] UTF8String];
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, selectAttachFileSql, -1, &selectAttachFileStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(selectAttachFileStmt) == SQLITE_ROW) {
                NSString* fileName=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:selectAttachFileStmt column:0]];
                
                
                
                
                OPSStudio* studio=[appDele activeStudio];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *databaseFolderPath = [paths objectAtIndex:0];
                databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], studio.ID, studio.name, studio.iconName ]];
                
                
                NSString* fileToBeDeleted=[databaseFolderPath stringByAppendingPathComponent:fileName];
                
                [fileManager removeItemAtPath: fileToBeDeleted error:NULL];
                
                
                
                
                
                
                
                //                NSString* fullImgDirectory=[imgView.path stringByDeletingLastPathComponent];
                //                NSString* fullImgPathLastComponent=[imgView.path lastPathComponent];
                //                NSString* tbImagePath=[[NSString alloc] initWithFormat:@"%@/tb_%@",fullImgDirectory,fullImgPathLastComponent];
                //                NSLog( @"%@",tbImagePath);
                //                [fileManager removeItemAtPath: tbImagePath error:NULL];
                //                [tbImagePath release];
                
                NSString* slideImagePath=[[NSString alloc] initWithFormat:@"%@/sl_%@",databaseFolderPath,fileName];
                [[NSFileManager defaultManager] removeItemAtPath: slideImagePath error:NULL];
                [slideImagePath release];
                
                
                
                NSString* tbImagePath=[[NSString alloc] initWithFormat:@"%@/tb_%@",databaseFolderPath,fileName];
                //                NSLog( @"%@",tbImagePath);
                [fileManager removeItemAtPath: tbImagePath error:NULL];
                [tbImagePath release];
                
                
                
                //                NSString* viewImagePath=[[NSString alloc] initWithFormat:@"%@/view_%@",databaseFolderPath,fileName];
                ////                NSLog( @"%@",viewImagePath);
                //                [fileManager removeItemAtPath: viewImagePath error:NULL];
                //                [viewImagePath release];
                
                [fileName release];
                
            }
        }
        
        
        sqlite3_finalize(selectAttachFileStmt);
        
        sqlite3_stmt *deleteAttachFileStmt=nil;
        const char* deleteAttachFileSql=[[NSString stringWithFormat:@"delete from AttachFile"
                                          ] UTF8String];
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, deleteAttachFileSql, -1, &deleteAttachFileStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(deleteAttachFileStmt) == SQLITE_ROW) {
                
            }
        }
        
        sqlite3_finalize(deleteAttachFileStmt);
        
        
        
        //        if (attachmentInfo){
        
        sqlite3_stmt *deleteAttachCommentStmt;
        const char* deleteAttachCommentSql=[[NSString stringWithFormat:@"delete from AttachComment"
                                             ] UTF8String];
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, deleteAttachCommentSql, -1, &deleteAttachCommentStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(deleteAttachCommentStmt) == SQLITE_ROW) {
                
            }
        }
        
        
        
        sqlite3_finalize(deleteAttachCommentStmt);
        
        sqlite3_stmt *testAttachFileStmt=nil;
        const char* testAttachFileSql=[[NSString stringWithFormat:@"select fileName from AttachFile"
                                        ] UTF8String];
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, testAttachFileSql, -1, &testAttachFileStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(testAttachFileStmt) == SQLITE_ROW) {
                
                NSString* fileName=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:testAttachFileStmt column:0]];
                NSLog(@"%@",fileName);
                [fileName release];
            }
        }
        
        
        sqlite3_finalize(testAttachFileStmt); 
        
    }            
    sqlite3_close(database);
    
    
}



-(void) trashAllAttachmentEntry{//:(id)sender{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
    
    //    OPSProjectSite* projectSite=appDele.activeProjectSite;  
    //    sqlite3* database=nil;
    
    
    database=nil;
    
    
    if (sqlite3_open ([[self dbPath] UTF8String], &database) == SQLITE_OK) {          
        
        
        
        
        sqlite3_stmt *selectAttachFileStmt;
        const char* selectAttachFileSql=[[NSString stringWithFormat:@"select fileName from AttachFile"
                                          ] UTF8String];                      
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, selectAttachFileSql, -1, &selectAttachFileStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(selectAttachFileStmt) == SQLITE_ROW) {     
                NSString* fileName=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:selectAttachFileStmt column:0]];
                
                
                
                
                OPSStudio* studio=[appDele activeStudio];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *databaseFolderPath = [paths objectAtIndex:0]; 
                databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], studio.ID, studio.name, studio.iconName ]]; 	    
                
                
                NSString* fileToBeDeleted=[databaseFolderPath stringByAppendingPathComponent:fileName];
                
                [fileManager removeItemAtPath: fileToBeDeleted error:NULL];
                
                
                
                
                
                
                
//                NSString* fullImgDirectory=[imgView.path stringByDeletingLastPathComponent];
//                NSString* fullImgPathLastComponent=[imgView.path lastPathComponent];                                                
//                NSString* tbImagePath=[[NSString alloc] initWithFormat:@"%@/tb_%@",fullImgDirectory,fullImgPathLastComponent];
//                NSLog( @"%@",tbImagePath);
//                [fileManager removeItemAtPath: tbImagePath error:NULL];           
//                [tbImagePath release];
                
                NSString* slideImagePath=[[NSString alloc] initWithFormat:@"%@/sl_%@",databaseFolderPath,fileName];
                [[NSFileManager defaultManager] removeItemAtPath: slideImagePath error:NULL];
                [slideImagePath release];
                
                
                
                NSString* tbImagePath=[[NSString alloc] initWithFormat:@"%@/tb_%@",databaseFolderPath,fileName];
//                NSLog( @"%@",tbImagePath);
                [fileManager removeItemAtPath: tbImagePath error:NULL];           
                [tbImagePath release];
                
                
                
//                NSString* viewImagePath=[[NSString alloc] initWithFormat:@"%@/view_%@",databaseFolderPath,fileName];
////                NSLog( @"%@",viewImagePath);
//                [fileManager removeItemAtPath: viewImagePath error:NULL];           
//                [viewImagePath release];
                
                [fileName release];
                
            }
        }
        
        
        sqlite3_finalize(selectAttachFileStmt); 
        
        sqlite3_stmt *deleteAttachFileStmt=nil;
        const char* deleteAttachFileSql=[[NSString stringWithFormat:@"delete from AttachFile"
                                          ] UTF8String];                      
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, deleteAttachFileSql, -1, &deleteAttachFileStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(deleteAttachFileStmt) == SQLITE_ROW) {     
                
            }
        }
        
        sqlite3_finalize(deleteAttachFileStmt); 
        
        
        
        //        if (attachmentInfo){
        
        sqlite3_stmt *deleteAttachCommentStmt;
        const char* deleteAttachCommentSql=[[NSString stringWithFormat:@"delete from AttachComment"
                                             ] UTF8String];                      
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, deleteAttachCommentSql, -1, &deleteAttachCommentStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(deleteAttachCommentStmt) == SQLITE_ROW) {     
                
            }
        }
        
        
        
        sqlite3_finalize(deleteAttachCommentStmt); 
        
        sqlite3_stmt *testAttachFileStmt=nil;
        const char* testAttachFileSql=[[NSString stringWithFormat:@"select fileName from AttachFile"
                                        ] UTF8String];                      
        //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
        if(sqlite3_prepare_v2 (database, testAttachFileSql, -1, &testAttachFileStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(testAttachFileStmt) == SQLITE_ROW) {     
                
                NSString* fileName=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:testAttachFileStmt column:0]];
                NSLog(@"%@",fileName);
                [fileName release];
            }
        }
        
        
        sqlite3_finalize(testAttachFileStmt); 
        
    }            
    sqlite3_close(database);
    
    
}

- (int) liveModifiedDate{ 
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
//    uint errorCode=0;
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    //    int errorCode=[appDele checkStudioWithUserName:<#(NSString *)#> userPW:<#(NSString *)#>];
    //    if (errorCode!=0) {return errorCode;}
    
//    NSMutableArray* _studioArray=[[NSMutableArray alloc]init];
//    self.studioArray=_studioArray;
//    [_studioArray release];
//    
    int modifiedDate=-1;
    
    

    
    

    

    
    
    
    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/modulet/getSiteLastModified.php?u=%@&p=%@&sysID=%d&siteID=%d&xml=1", [ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]],[ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]], [appDele activeStudioID],[self ID]];            
    NSURL *url = [[NSURL alloc ]initWithString: urlString ];
//    [urlString release];urlString=nil;
    NSError* error;
    CXMLDocument *parser = [[[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:&error] autorelease];
    
    
    
    [url release];
    url=nil;
    
    
    
    
    
    
    
    NSArray *rows = [parser nodesForXPath:@"//site" error:nil];
    
    for (CXMLElement *row in rows) {        
        
//        NSArray* studioIDNode=[row nodesForXPath:@"COL[1]/dateModified" error:nil];       
        NSArray* modifiedDateNode=[row nodesForXPath:@"dateModified" error:nil];               
        NSString* modifiedDateStr=[[modifiedDateNode objectAtIndex:0] stringValue];
        modifiedDate=[modifiedDateStr integerValue];

//        
//        
//        
//        NSArray* studioNameNode=[row nodesForXPath:@"COL[2]/DATA" error:nil];       
//        NSString *studioName=[[studioNameNode objectAtIndex:0] stringValue];
//        
//        NSArray* studioIconNameNode=[row nodesForXPath:@"COL[3]/DATA" error:nil];       
//        NSString *studioIconName=[[studioIconNameNode objectAtIndex:0] stringValue];
//        
//        NSInteger dotStart = [studioIconName rangeOfString:@"."].location;
//        //        if(dotStart == NSNotFound)
//        //            return nil;
//        //        return [theString substringToIndex:hyphenStart];
//        studioIconName=[studioIconName substringToIndex:dotStart];
//        
//        //        NSLog(@"%@", name);
//        OPSStudio* opsStudio= [[OPSStudio alloc] init:studioID name:studioName  iconName:studioIconName];
//        [self.studioArray addObject:opsStudio];
//        [opsStudio release];
        
    }
    

    return modifiedDate;
    
}




-(bool)isLocalProjectAttachmentEmpty{
    
    
//    sqlite3* database=nil;
    database=nil;
    
    bool isAttachmentEmpty=true;                
    
    if (sqlite3_open ([[self dbPath] UTF8String], &database) == SQLITE_OK) {          
     
        
        sqlite3_stmt *attachedFileAttachCommentStmt; 
        const char* attachedFileAttachCommentSql=[[NSString stringWithFormat:@"SELECT * from attachfile join attachcomment"] UTF8String];            
  
        
        if(sqlite3_prepare_v2(database, attachedFileAttachCommentSql, -1, &attachedFileAttachCommentStmt, NULL) == SQLITE_OK) {
            while( sqlite3_step(attachedFileAttachCommentStmt) == SQLITE_ROW) {                                 
                isAttachmentEmpty=false;
                break;
            }
        }        
        sqlite3_finalize(attachedFileAttachCommentStmt);         
    }  
    sqlite3_close(database);

    
    
    
    
    return isAttachmentEmpty;
    
    
    
}





-(int) getLocalDatabaseModifiedDate{        
    
    int localSiteDateModified=-1.0; //If the local studio wasn't even created or local site wasnt downloaded, the time modified is -1
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
    
//    OPSProjectSite* projectSite=[appDele activeProjectSite];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; 
    
    NSString* databaseFolderPath=nil;
    
//    databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName],[appDele activeLiveStudio].ID,[appDele activeLiveStudio].name,[appDele activeLiveStudio].iconName]] ;

    databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName],[appDele activeStudio].ID,[appDele activeStudio].name,[appDele activeStudio].iconName]] ;
    
    NSFileManager *NSFm= [NSFileManager defaultManager]; 
    BOOL isDir=YES;    
    if([NSFm fileExistsAtPath:databaseFolderPath isDirectory:&isDir]){
        
        
        NSString* localDBPath=nil;       
        
        localDBPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.sqlite", [self name]]] ];
        
        if([NSFm fileExistsAtPath:localDBPath isDirectory:false]){     //DB already existed                     
            
            if (sqlite3_open ([localDBPath UTF8String], &database) == SQLITE_OK) {  
                
                
                sqlite3_stmt* spatialStructStmt;        
                NSString* spatialStructStr=[NSString stringWithFormat:@""
                                            @"SELECT spatialstructure.dateModified"
                                            @" FROM spatialstructure where spatialStructureType=\"Site\""                                ];
                
                const char* spatialStructSQLChar=[spatialStructStr UTF8String];    
                
                if(sqlite3_prepare_v2(database, spatialStructSQLChar, -1, &spatialStructStmt, NULL) == SQLITE_OK) {
                    while(sqlite3_step(spatialStructStmt) == SQLITE_ROW) {                     
                        localSiteDateModified = sqlite3_column_double(spatialStructStmt,0);                        
                    }
                }                                    
            }
            sqlite3_close(database);  
        }
        
        [localDBPath release];
    }
    return localSiteDateModified;
}





/*
-(int)localModifiedDate{
    
    
    //    sqlite3* database=nil;
    database=nil;
    int dateModified=-1;             
    
    if (sqlite3_open ([[self dbPath] UTF8String], &database) == SQLITE_OK) {          
        
        
        
        sqlite3_stmt* spatialStructStmt;        
        NSString* spatialStructStr=[NSString stringWithFormat:@""
                                    @"SELECT spatialstructure.dateModified"
                                    @" FROM spatialstructure where spatialStructureType=\"Site\""                                ];
        
        const char* spatialStructSQLChar=[spatialStructStr UTF8String];    
        //        uint siteDateModified=0.0;
        if(sqlite3_prepare_v2(database, spatialStructSQLChar, -1, &spatialStructStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(spatialStructStmt) == SQLITE_ROW) {                     
                dateModified = sqlite3_column_double(spatialStructStmt,0);
                
            }
        }  
        sqlite3_finalize(spatialStructStmt);      
        

    }  
    sqlite3_close(database);
    
    
    
    
    
    return dateModified;
    
    
    
}
*/




/*
- (NSComparisonResult)compareShared:(OPSProjectSite *)otherObject {
    return [self.shared compare:otherObject.shared];
}

- (NSComparisonResult)compareProjectID:(OPSProjectSite *)otherObject{
    return [self.projectID compare:otherObject.projectID];
}
 */
-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    OPSProjectSite *copiedProjectSite = [[OPSProjectSite allocWithZone:zone] init:ID name:name shared:shared projectID:projectID projectName:projectName iconName:iconName];
    if (dbPath){
        copiedProjectSite.dbPath=dbPath;
    }
    //    another.obj = [obj copyWithZone: zone];
    
    return copiedProjectSite;
}
//
//-(void) toLocalProjectSite:(OPSProjectSite*) localProjectSite{
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];                
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *databaseFolderPath = [paths objectAtIndex:0]; 
//    
//    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], [appDele activeStudio].ID, [appDele activeStudio].name, [appDele activeStudio].iconName ]]; 	    
//    
//    OPSProjectSite* activeLiveProjectSite=self;//[appDele activeProjectSite];
//    
//    NSString* filename = [NSString stringWithFormat:@"%@.sqlite",[activeLiveProjectSite name]];
//    
//    
//    
//    NSString *filenameOnly=[activeLiveProjectSite name];//[NSMutableString stringWithString:filename];
//    //            [filenameOnly replaceCharactersInRange: [filenameOnly rangeOfString: @".sqlite"] withString: @""];            
//    NSMutableDictionary *dictionary=nil;            
//    NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
//    dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];  
//    NSString* pListSt=[dictionary objectForKey:filenameOnly];                        
////    OPSProjectSite* localProjectSite=[[OPSProjectSite alloc] init:pListSt];
//    localProjectSite=[localProjectSite init:pListSt];
//    
//    NSString* localDBPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:filename ] ];
//    localProjectSite.dbPath=localDBPath;
//    [localDBPath release];    
//    
////    [self openProjectToDisplay:localProjectSite];
////    [localProjectSite release];    
//    
//}
//

- (NSString*) toDictionaryObject{    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    NSString* versionStr = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];  
    uint appCurrentVersion=[appDele readAppCurrentVersion];
    NSString* dictionaryStr=[NSString stringWithFormat:@"[SiteID]:{%d} [SiteName]:{%@} [Shared]:{%d} [ProjectID]:{%d} [ProjectName]:{%@} [IconName]:{%@} [Version]:{%d}",ID,name,shared,projectID,projectName,iconName, appCurrentVersion];
    
    return dictionaryStr;    
}


-(void) readVersionLagStr{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    uint currentVersion=[appDele readAppCurrentVersion];
    if (self.version>=currentVersion){
        self.versionLagStr=nil;
        return;
    }
    NSString* versionHistoryStr=nil;
    bool bHistoryStart=false;
    for (OPSSiteVersion* pVersionHistory in appDele.aVersionHistory){
        if (self.version==[pVersionHistory version]){
            bHistoryStart=true;
        }
        if (bHistoryStart){
            if (versionHistoryStr==nil){                
                versionHistoryStr=[NSString stringWithFormat:@"%d",[pVersionHistory version] ];
                if ([pVersionHistory bStructureUpdated]){
                    self.bNeedUpdate=true;
                }
            }else{
                versionHistoryStr=[versionHistoryStr stringByAppendingString:[NSString stringWithFormat:@"|%d",[pVersionHistory version]]];
                if ([pVersionHistory bStructureUpdated]){
                    self.bNeedUpdate=true;
                }                
            }            
        }
    }    
    self.versionLagStr=versionHistoryStr;
//    return versionHistoryStr;
}


- (id) init:(NSString*) pListStr{

//        NSInteger siteIDStart = [pListStr rangeOfString:@"[SiteID]:{"].location;
        NSInteger siteNameStart = [pListStr rangeOfString:@"} [SiteName]:{"].location;
        NSInteger sharedStart = [pListStr rangeOfString:@"} [Shared]:{"].location;
        NSInteger projectIDStart = [pListStr rangeOfString:@"} [ProjectID]:{"].location;
        NSInteger projectNameStart = [pListStr rangeOfString:@"} [ProjectName]:{"].location;
        NSInteger iconNameStart = [pListStr rangeOfString:@"} [IconName]:{"].location;
        NSInteger versionStart = [pListStr rangeOfString:@"} [Version]:{"].location;
        
        
        NSString* testID=[[NSString alloc]initWithString: [pListStr substringWithRange:NSMakeRange(10, siteNameStart-10)]];
        
        NSString* testName=[[NSString alloc]initWithString: [pListStr substringWithRange:NSMakeRange(siteNameStart+14, sharedStart-siteNameStart-14)]];
        
        NSString* testShared=[[NSString alloc]initWithString: [pListStr substringWithRange:NSMakeRange(sharedStart+12, projectIDStart-sharedStart-12)]];
        
        NSString* testProjectID=[[NSString alloc]initWithString: [pListStr substringWithRange:NSMakeRange(projectIDStart+15, projectNameStart-projectIDStart-15)]];
        
        
        NSString* testProjectName=[[NSString alloc]initWithString: [pListStr substringWithRange:NSMakeRange(projectNameStart+17, iconNameStart-projectNameStart-17)]];
        
        
//        NSString* testIconName=[[NSString alloc]initWithString: [pListStr substringWithRange:NSMakeRange(iconNameStart+14, [pListStr length]-iconNameStart-14)]];
    
        NSString* testIconName=[[NSString alloc]initWithString: [pListStr substringWithRange:NSMakeRange(iconNameStart+14, versionStart-iconNameStart-14)]];        
    
        NSString* testVersionStr=[[NSString alloc]initWithString: [pListStr substringWithRange:NSMakeRange(versionStart+13, [pListStr length]-versionStart-13-1)]];
        
    
    self=[super init:[testID intValue] name:testName shared:[testShared intValue] iconName:testIconName];
    

    if (self){
//        self.ID=[testID intValue];
//        [testID release];
//        
//        [name release];
//        self.name=testName;
//        [testName release];
//                
//        self.shared=[testShared intValue];
//        [testShared release];
//        
        projectID=[testProjectID intValue];

        
        [projectName release];
        projectName=[testProjectName retain];
    

        self.version=[testVersionStr intValue];
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];          
//        self.version=[appDele convertToVersionIntFromVersionStr:testVersionStr];
        
//        [iconName release];
//        self.iconName=testIconName;
//        [testIconName release];
        
//        [[NSString alloc]initWithString: [pListStr substringFromIndex:iconNameStart+14]];
        
//        uint t=0;
//        t=1;
    }
    
    [testID release];
    [testName release];
    [testShared release];
    [testIconName release];
    
    
    [testProjectID release];
    [testProjectName release]; 
    [testVersionStr release];
    
    
    
    return self;
//    uint studioID=[[NSString alloc]initWithString: [pListStr substringWithRange:NSMakeRange(9, (studioIconNameStart-studioNameStart-3))]];
    
//    NSString* studioName=[[NSString alloc]initWithString: [studioFolderStr substringWithRange:NSMakeRange((studioNameStart+3), (studioIconNameStart-studioNameStart-3))]];
    
//    NSString* studioIconName=[[NSString alloc]initWithString: [studioFolderStr substringFromIndex:(studioIconNameStart+3)]];
    
    
    
}
- (id) init:(NSUInteger) _ID name:(NSString*) _name shared:(NSUInteger) _shared projectID:(NSUInteger)_projectID projectName:(NSString*) _projectName iconName:(NSString*) _iconName{
    self=[super init:_ID name:_name shared:_shared iconName:_iconName];
    if (self){
//        ID=_ID;
//        if (_name){
//            [name release];
//            [_name retain];
//            name=_name;
//        }        
//        shared=_shared;
        
        
        projectID=_projectID;
        
        if (_projectName){
            [projectName release];
            [_projectName retain];
            projectName = _projectName;
        }
        
//        if (_iconName){
//            [iconName release];
//            [_iconName retain];
//            iconName=_iconName;
//        }
    }
    return self;
}
- (void)dealloc{
//    [name release];
    [_afterUploadFailParamArray release];_afterUploadFailParamArray=nil;
    [_afterUploadSuccessParamArray release];_afterUploadSuccessParamArray=nil;
    [_aAttachmentInfoTableUploadComment release];_aAttachmentInfoTableUploadComment=nil;
    [_aAttachmentInfoTableUploadFile release];_aAttachmentInfoTableUploadFile=nil;
    [_versionLagStr release];_versionLagStr=nil;
    [projectName release];
    projectName=nil;
//    [iconName release];  
    [dbPath release];
    dbPath=nil;
//    [_version release];_version=nil;
    [super dealloc];
}
-(void) updateLoadingOverlayLabelStatus{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    appDele.loadingOverlayLabel.text=[NSString stringWithFormat:@"Uploaded %d/%d photo(s) and %d/%d user entered text from the iPad to the Onuma BIM Server. \n\rOne moment while this uploads.",self.numFileUploadSuccess,[[self aAttachmentInfoTableUploadFile] count],self.numCommentUploadSuccess,[[self aAttachmentInfoTableUploadComment] count]];

    appDele.loadingOverlayLabel.text=[NSString stringWithFormat:@"Uploaded %d/%d photo(s) and %d/%d notes from the iPad to the Onuma BIM Server. \n\rOne moment while this uploads.",self.numFileUploadSuccess,self.numFileToUpload,self.numCommentUploadSuccess,self.numCommentToUpload];
    
    if (self.numFileUploadFail>0){
        appDele.loadingOverlayLabel.text=[appDele.loadingOverlayLabel.text stringByAppendingFormat:@"\n\r%d file(s) failed to be uploaded.",[self numFileUploadFail]];
    }
    
    
    if (self.numFileUploadFail>0){
        appDele.loadingOverlayLabel.text=[appDele.loadingOverlayLabel.text stringByAppendingFormat:@"\n\r%d notes failed to be uploaded.",[self numCommentUploadFail]];
    }
    
    
}
- (void) checkuploadFileStatus{
    NSLog(@"s%d:f%d+/T%d",self.numFileUploadSuccess,self.numFileUploadFail,self.numFileToUpload);
    if (self.numFileUploadFail>0 && self.numFileToUpload==(self.numFileUploadFail+self.numFileUploadSuccess)){
        [self failUpload];
        
    }else if (self.numFileToUpload==self.numFileUploadSuccess){
        [self uploadProjectSiteComment];
    }
}
- (void) checkuploadCommentStatus{
    NSLog (@"F%d+S%d/T%d",self.numCommentUploadFail,self.numCommentUploadSuccess,self.numCommentToUpload);
    if (self.numCommentUploadFail>0 && self.numCommentToUpload==(self.numCommentUploadFail+self.numCommentUploadSuccess)){
        [self failUpload];
    }else if (self.numCommentToUpload==self.numCommentUploadSuccess){
        [self finishUpload];
    }
}
-(void) failUpload{
    
    NSString* alertTitle=@"";
    if (self.numFileUploadSuccess>0){
        alertTitle=[alertTitle stringByAppendingFormat: @"%d/%d photos",self.numFileUploadSuccess,self.numFileToUpload];
    }
    
    if (self.numFileUploadSuccess>0 && self.numCommentUploadSuccess>0){
        alertTitle=[alertTitle stringByAppendingFormat:@" and"];
    }
    
    
    if (self.numCommentUploadSuccess>0){
        alertTitle=[alertTitle stringByAppendingFormat:@" %d/%d notes",self.numCommentUploadSuccess,self.numCommentToUpload];
        
    }
    
    if (self.numFileUploadSuccess>0 || self.numCommentUploadSuccess>0){
        alertTitle=[alertTitle stringByAppendingFormat:@" successfully uploaded to Onuma BIM Server."];
    }
    
    if (self.numFileUploadFail>0){
        alertTitle=[alertTitle stringByAppendingFormat:@" %d/%d Photos",self.numFileUploadFail,self.numFileToUpload];
    }
    
    if (self.numFileUploadFail>0 && self.numCommentUploadFail>0){
        alertTitle=[alertTitle stringByAppendingFormat:@" and"];
    }
    if (self.numCommentUploadFail>0){        
        alertTitle=[alertTitle stringByAppendingFormat:@" %d/%d notes",self.numCommentUploadFail,self.numCommentToUpload];
    }
    
    if (self.numFileUploadFail>0 || self.numCommentUploadFail>0){
        alertTitle=[alertTitle stringByAppendingFormat:@" failed to upload to Onuma BIM Server. They are saved locally. Pls try to upload again later."];
    }
    
    //if This all the upload file has been processed and if any 1 of them failed to be uploaded
    if (self.numCommentUploadFail+self.numCommentUploadSuccess+self.numFileUploadFail+self.numFileUploadSuccess==self.numCommentToUpload+self.numFileToUpload){
        [self removeEmptyCommentEmptyPhotoAttachmentFromDatabase];
    }
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: alertTitle
                          message: @""
                          delegate: self
                          cancelButtonTitle: @"OK"
                          otherButtonTitles: nil];
    alert.tag = OPSProjectSite_CONFIRM_UPLOADNOINTERNET; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
    [alert show];
    [alert release];    
}
-(void) removeEmptyCommentEmptyPhotoAttachmentFromDatabase{
                
    database=nil;
    if (sqlite3_open ([[self dbPath] UTF8String], &database) == SQLITE_OK) {                
        sqlite3_stmt *deleteEmptyAttachCommentEmptyAttachFileStmt=nil;
        const char* deleteEmptyAttachCommentEmptyAttachFileSql=[[NSString stringWithFormat:@"DELETE FROM attachcomment where not exists (select * from attachfile where attachfile.attachcommentID=attachcomment.ID) AND attachcomment.comment=''"] UTF8String];
        
        if(sqlite3_prepare_v2 (database, deleteEmptyAttachCommentEmptyAttachFileSql, -1, &deleteEmptyAttachCommentEmptyAttachFileStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(deleteEmptyAttachCommentEmptyAttachFileStmt) == SQLITE_ROW) {
                
            }
        }
        
        sqlite3_finalize(deleteEmptyAttachCommentEmptyAttachFileStmt);
        
        
                
    }
    sqlite3_close(database);
    
}
-(void) finishUpload{
    
//
//    if (afterUploadSuccessCompleteAction){
//        [afterUploadSuccessInvoker performSelector:afterUploadSuccessCompleteAction withObject:paramArray];
//    }
    
    
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Upload Success."
                          message: @""
                          delegate: self
                          cancelButtonTitle: @"OK"
                          otherButtonTitles: nil];
    alert.tag = OPSProjectSite_CONFIRM_UPLOADFINISH; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
    [alert show];
    [alert release];
    
    
    
}
-(void) uploadProjectSiteComment{
    self.numCommentUploadSuccess=0;
    self.numCommentToUpload=0;
    self.numCommentToUpload=0;
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
    NSMutableArray* tmpAAttachmentInfoTableUploadComment=[[NSMutableArray alloc]init];
    self.aAttachmentInfoTableUploadComment=tmpAAttachmentInfoTableUploadComment;
    [tmpAAttachmentInfoTableUploadComment release];
    
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *databaseFolderPath = [paths objectAtIndex:0];
//    OPSStudio* studio=[appDele activeStudio];
//    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], studio.ID, studio.name, studio.iconName ]];
    
    sqlite3_stmt *attachedCommentStmt;
    const char* attachedCommentSql=[[NSString stringWithFormat:@"select attachcomment.ID, attachcomment.commentTitle, attachcomment.comment, attachcomment.links, attachcomment.commentDate, attachcomment.attachedTo, attachcomment.referenceID from attachcomment where sent=0"] UTF8String];
    sqlite3 *database = nil;
//    uint pAttachedFile=0;
        
    NSString* dbPathToUpload=self.dbPath;
    
    
    //    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    //    NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];
    //
    //    if (![[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {
    //        // database doesn't exist in your library path... copy it from the bundle
    //        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"EmptySQLTable" ofType:@"sqlite"];
    //        NSError *error = nil;
    //
    //        if (![[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:targetPath error:&error]) {
    //            NSLog(@"Error: %@", error);
    //        }
    //    }
    
    
    sqlite3_stmt *countAttachCommentStmt;
    const char* countAttachCommentSql=[[NSString stringWithFormat:@"select count(attachcomment.ID) from attachcomment where sent=0"] UTF8String];
    
    
     
        
    
    
    
    
    
    
    if (sqlite3_open ([dbPathToUpload UTF8String], &database) == SQLITE_OK) {
        
        if(sqlite3_prepare_v2(database, countAttachCommentSql, -1, &countAttachCommentStmt, NULL) == SQLITE_OK) {
            while( sqlite3_step(countAttachCommentStmt) == SQLITE_ROW) {
                self.numCommentToUpload=sqlite3_column_int(countAttachCommentStmt, 0);
            }
        }
        
        
        sqlite3_finalize(countAttachCommentStmt);
        
        if (self.numCommentToUpload>0){
            
            if(sqlite3_prepare_v2(database, attachedCommentSql, -1, &attachedCommentStmt, NULL) == SQLITE_OK) {
                while( sqlite3_step(attachedCommentStmt) == SQLITE_ROW) {
                    
                    
                    //                 attachfile.ID attachfile.fileName attachfile.fileTitle attachfile.attachedTo attachfile.referenceID
                    
                    uint attachedCommentID=sqlite3_column_int(attachedCommentStmt, 0);

                    
                    NSString* commentTitle=[ProjectViewAppDelegate readNameFromSQLStmt:attachedCommentStmt column:1];
                    
                    NSString* comment=[ProjectViewAppDelegate readNameFromSQLStmt:attachedCommentStmt column:2];                    
                    
                    NSString* links=[ProjectViewAppDelegate readNameFromSQLStmt:attachedCommentStmt column:3];
                    
                    NSString* commentDate=[ProjectViewAppDelegate readNameFromSQLStmt:attachedCommentStmt column:4];
                    NSString* attachedTo=[ProjectViewAppDelegate readNameFromSQLStmt:attachedCommentStmt column:5];
                    uint referenceID=sqlite3_column_int(attachedCommentStmt, 6);
                    
                    
                    AttachmentInfoTableUploadComment* attachmentInfoTableUploadComment=[[AttachmentInfoTableUploadComment alloc] initWithProjectSite:self attachcommentID:attachedCommentID commentTitle:commentTitle comment:comment links:links commentDate:commentDate attachedTo:attachedTo referenceID:referenceID];
                    
                    //                [attachedFileName release];
                    //                [attachedFileTitle release];
                    //                [attachedTo release];
                    //                [attachedTo release];
                    //                [uploadDate release];
                    
                    [self.aAttachmentInfoTableUploadComment addObject:attachmentInfoTableUploadComment];
                    [attachmentInfoTableUploadComment release];
                    
                    [[self.aAttachmentInfoTableUploadComment lastObject] upload:nil];
                    
                    
                    //
                    //
                    //
                    //                NSData *imageData = [[NSData alloc] initWithContentsOfFile:imagePath];
                    //                //            while ([imageData length] > maxFileSize && compression > maxCompression)
                    //                //            {
                    //                //                compression -= 0.1;
                    //                //                imageData = UIImageJPEGRepresentation([UIImage imageNamed:imagePath], compression);
                    //                //            }
                    //
                    //                //Assuming data is not nil we add this to the multipart form
                    //                if (imageData) {
                    //
                    //                    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
                    //                    //        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    //
                    //
                    //                    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"AttachedFileName%04d\"; filename=\"%@\"\r\n",pAttachedFile,attachedFileName] dataUsingEncoding:NSUTF8StringEncoding]];
                    //                    [myData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    //                    [myData appendData:imageData];
                    //                    [myData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    //                }
                    //                pAttachedFile++;
                    //                [imageData release];
                    
                    
                }
            }
            
            sqlite3_finalize(attachedCommentStmt);
            sqlite3_close(database);
        }else{
            sqlite3_close(database);
            [self finishUpload];
        }
    }else{
    
        sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
        [self finishUpload];
    }
    
    sqlite3_close(database);    

}


-(void) uploadProjectSite:(NSArray*) paramArray{
        
    
    self.numFileToUpload=0;
    self.numFileUploadFail=0;
    self.numFileUploadSuccess=0;
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    

    NSMutableArray* tmpAAttachmentInfoTableUploadFile=[[NSMutableArray alloc]init];
    self.aAttachmentInfoTableUploadFile=tmpAAttachmentInfoTableUploadFile;
    [tmpAAttachmentInfoTableUploadFile release];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *databaseFolderPath = [paths objectAtIndex:0];
    
    OPSStudio* studio=[appDele activeStudio];
    
    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], studio.ID, studio.name, studio.iconName ]];
    
    
    
    sqlite3_stmt *countAttachedFileStmt;
    const char* countAttachedFileSql=[[NSString stringWithFormat:@"select count(attachfile.ID) from attachfile where sent=0"] UTF8String];
    
    
    
    sqlite3_stmt *attachedFileStmt;
    const char* attachedFileSql=[[NSString stringWithFormat:@"select attachfile.ID, attachfile.fileName, attachfile.fileTitle, attachfile.attachedTo, attachfile.referenceID, attachFile.uploadDate from attachfile where sent=0"] UTF8String];
    sqlite3 *database = nil;
//    uint pAttachedFile=0;
    
    
    
    NSString* dbPathToUpload=self.dbPath;

    
    if (sqlite3_open ([dbPathToUpload UTF8String], &database) == SQLITE_OK) {
        
        
        
        sqlite3_stmt *countAttachCommentStmt;
        const char* countAttachCommentSql=[[NSString stringWithFormat:@"select count(attachcomment.ID) from attachcomment where sent=0"] UTF8String];
        if(sqlite3_prepare_v2(database, countAttachCommentSql, -1, &countAttachCommentStmt, NULL) == SQLITE_OK) {
            while( sqlite3_step(countAttachCommentStmt) == SQLITE_ROW) {
                self.numCommentToUpload=sqlite3_column_int(countAttachCommentStmt, 0);
            }
        }        
        sqlite3_finalize(countAttachCommentStmt);
        
        
        
        
        
        
        
        if(sqlite3_prepare_v2(database, countAttachedFileSql, -1, &countAttachedFileStmt, NULL) == SQLITE_OK) {
            while( sqlite3_step(countAttachedFileStmt) == SQLITE_ROW) {                
                self.numFileToUpload=sqlite3_column_int(countAttachedFileStmt, 0);
            }
        }
        
        if (self.numFileToUpload>0){
            sqlite3_finalize(countAttachedFileStmt);
            if(sqlite3_prepare_v2(database, attachedFileSql, -1, &attachedFileStmt, NULL) == SQLITE_OK) {
                while( sqlite3_step(attachedFileStmt) == SQLITE_ROW) {
                                    
                    uint attachedFileID=sqlite3_column_int(attachedFileStmt, 0);                
                    NSString* attachedFileName=[ProjectViewAppDelegate readNameFromSQLStmt:attachedFileStmt column:1];
                    NSString* attachedFileTitle=[ProjectViewAppDelegate readNameFromSQLStmt:attachedFileStmt column:2];
                    NSString* attachedTo=[ProjectViewAppDelegate readNameFromSQLStmt:attachedFileStmt column:3];
                    uint referenceID=sqlite3_column_int(attachedFileStmt, 4);
                    NSString* uploadDate=[ProjectViewAppDelegate readNameFromSQLStmt:attachedFileStmt column:5];
                    if (![attachedFileName hasSuffix:@".jpg"]){
    //                    [attachedFileName release];
    //                    [attachedFileTitle release];
    //                    [attachedTo release];
    //                    [attachedTo release];
    //                    [uploadDate release];
                        continue;
                    }
                    
                    
                    NSString* imagePath=[databaseFolderPath stringByAppendingPathComponent:attachedFileName];                
                    AttachmentInfoTableUploadFile* attachmentInfoTableUploadFile=[[AttachmentInfoTableUploadFile alloc] initWithProjectSite:self attachFileID:attachedFileID filePath:imagePath fileName:attachedFileName fileTitle:attachedFileTitle attachedTo:attachedTo referenceID:referenceID uploadDate:uploadDate];
                                    
    //                [attachedFileName release];
    //                [attachedFileTitle release];
    //                [attachedTo release];
    //                [attachedTo release];
    //                [uploadDate release];
                    
                    [self.aAttachmentInfoTableUploadFile addObject:attachmentInfoTableUploadFile];
                    [attachmentInfoTableUploadFile release];
                    
                    [[self.aAttachmentInfoTableUploadFile lastObject] upload:nil];

                    
                    
                    
                }
            }
            
            
            sqlite3_finalize(attachedFileStmt);
            sqlite3_close(database);
        }else{            
            [self uploadProjectSiteComment];
        }
    }else{
        sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
    }
    
    sqlite3_close(database);
    
//    appDele.loadingOverlayLabel.text=[NSString stringWithFormat:@"Uploading %d photo(s) and %d user entered text from the iPad to the Onuma BIM Server. \n\rOne moment while this uploads.",[self.aAttachmentInfoTableUploadFile count],[self.aAttachmentInfoTableUploadComment count]];
    
    [self updateLoadingOverlayLabelStatus];
//    appDele.loadingOverlayLabel.text=[NSString stringWithFormat:@"Uploaded %d/%d photo(s) and %d/%d user entered text from the iPad to the Onuma BIM Server. \n\rOne moment while this uploads.",self.numFileUploadSuccess,[[self aAttachmentInfoTableUploadFile] count],self.numCommentUploadSuccess,[[self aAttachmentInfoTableUploadComment] count]];
//    [appDele.loadingOverlayLabel setNeedsDisplay];
}


#pragma mark NSURLConnection methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [connectionData setLength:0];
    
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.  
    [connectionData appendData:data];
    
//    NSLog (@"dataLen:%d",[connectionData length]);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    // receivedData is declared as a method instance elsewhere
    
    //    [connection release];
    [theConnection release];theConnection=nil;
    [connectionData release];connectionData=nil;
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];    
    
    
    
    /*
    if (self.attachmentInfoDetailToClose){
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        
        
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
        NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];        
        [fileManager removeItemAtPath:targetPath error:NULL];  
        
        
        
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle: @"No Internet For Upload, Data Saved locally instead."
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"OK"
                              otherButtonTitles: nil];
        
        alert.tag = APPDELEGATE_CONFIRM_UPLOADNOINTERNET; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];
        
    }else{
        // inform the user
        UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"didFailWithError"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
        [didFailWithErrorMessage show];
        [didFailWithErrorMessage release];
        
        //inform the user
        NSLog(@"Connection failed! Error - %@ %@",
              [error localizedDescription],
              [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
        
    }
     */
}

/*
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {   
        case APPDELEGATE_CONFIRM_UPLOADNOINTERNET:
        {
            [self.attachmentInfoDetailToClose actionOK:nil];
        }
            break;            
        default:
            //            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");            
            break;
    }	
}
*/







-(void) alertUploadResult:(id)sender title:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle: title
                          message: @""
                          delegate: self
                          cancelButtonTitle: nil
                          otherButtonTitles: @"ok", nil];
    alert.tag = OPSProjectSite_CONFIRM_UPLOADFINISH; 
    [alert show];
    [alert release];
    
    
}




@end

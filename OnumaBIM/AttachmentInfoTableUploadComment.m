//
//  AttachmentInfoTableUploadComment.m
//  ProjectView
//
//  Created by Alfred Man on 11/7/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "AttachmentInfoTableUploadComment.h"
#import "AttachmentInfoDetail.h"
#import "ProjectViewAppDelegate.h"
#import "OPSProjectSite.h"
#import "Site.h"
@implementation AttachmentInfoTableUploadComment


uint const AttachmentInfoTableUploadComment_CONFIRM_UPLOADNOINTERNET=1;

uint const AttachmentInfoTableUploadComment_CONFIRM_UPLOADFINISH=2;




@synthesize projectSite=_projectSite;
@synthesize attachcommentID=_attachcommentID;
@synthesize commentTitle=_commentTitle;
@synthesize comment=_comment;
@synthesize links=_links;
@synthesize commentDate=_commentDate;
@synthesize attachedTo=_attachedTo;
@synthesize referenceID=_referenceID;

-(id)initWithAttachmentInfoDetail:(AttachmentInfoDetail*)attachmentInfoDetail commentTitle:(NSString*)commentTitle comment:(NSString*)comment links:(NSString*)links commentDate:(NSString*)commentDate attachedTo:(NSString*)attachedTo referenceID:(NSInteger)referenceID{
    self=[super init];
    if (self) {
        self.attachmentInfoDetail=attachmentInfoDetail;
        self.commentTitle=commentTitle;
        self.comment=comment;
        self.links=links;
        self.commentDate=commentDate;
        self.attachedTo=attachedTo;
        self.referenceID=referenceID;
    }
    return self;
}

-(id) initWithProjectSite:(OPSProjectSite*)projectSite attachcommentID:(uint)attachcommentID commentTitle:(NSString*)commentTitle comment:(NSString*)comment links:(NSString*)links commentDate:(NSString*)commentDate attachedTo:(NSString*)attachedTo referenceID:(NSInteger)referenceID{
    self=[super init];
    if (self) {
        self.projectSite=projectSite;
        self.attachcommentID=attachcommentID;
        self.commentTitle=commentTitle;
        self.comment=comment;
        self.links=links;
        self.commentDate=commentDate;
        self.attachedTo=attachedTo;
        self.referenceID=referenceID;
    }
    return self;
}
-(void) dealloc{
    [_commentTitle release];_commentTitle=nil;
    [_comment release];_comment=nil;
    [_links release];_links=nil;
    [_commentDate release];_commentDate=nil;
    [_attachedTo release];_attachedTo=nil;
    [_uploadDate release];_uploadDate=nil;
    
    [super dealloc];
}



//
//-(NSData*) upload:(NSArray*) paramArray{

-(void) upload:(NSArray*) paramArray{
    NSMutableData *myData = nil;
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    //    NSData* sqliteData=[NSData dataWithContentsOfFile:[self dbPath]];
    
    
    NSString *aBoundary = [NSString stringWithFormat:@"vYRNMz5SSe4vXrnc"];
    myData = [[NSMutableData alloc] init];//[NSMutableData data];// [NSMutableData dataWithCapacity:1];
    NSString *myBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", aBoundary];
    NSString *closingBoundary = [NSString stringWithFormat:@"\r\n--%@--\r\n", aBoundary];
    
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
//    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", [ProjectViewAppDelegate urlEncodeValue:appDele.defUserName]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", appDele.defUserName] dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    
    
//    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", [ProjectViewAppDelegate urlEncodeValue:appDele.defUserPW]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", appDele.defUserPW] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"sysID\"\r\n\r\n%d", appDele.activeStudioID] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"siteID\"\r\n\r\n%d", [appDele activeProjectSite].ID] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"commentTitle\"\r\n\r\n%@", self.commentTitle] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"comment\"\r\n\r\n%@", self.comment] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"links\"\r\n\r\n%@", self.links] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"commentDate\"\r\n\r\n%@", self.commentDate] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"attachedTo\"\r\n\r\n%@", self.attachedTo] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"referenceID\"\r\n\r\n%d", self.referenceID] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    
    
    [myData appendData:[closingBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/postTestAction2.php"];
    NSURL *url = [NSURL URLWithString: urlString];
    
    
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:900.0];
    
    
    [theRequest setHTTPMethod:@"POST"];
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [myData length]];
    [theRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", aBoundary];
    
    [theRequest setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    [theRequest setHTTPBody:myData];
    
    //    NSString* t=[theRequest valueForHTTPHeaderField:@"Content-Type"];
    //    NSLog(@"%@",t);
    
    
    
    // create the connection with the request
    // and start loading the data
    // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file
    //    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    
    
//    NSLog (@"%@",[[NSString alloc] initWithData:myData  encoding:NSASCIIStringEncoding]);
    
    
    NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    self.theConnection=aConnection;
    [aConnection release];
    if (self.theConnection) {
        // Create the NSMutableData that will hold
        // the received data
        // receivedData is declared as a method instance elsewhere
//        if (self.connectionData){
//            [self.connectionData release];
//        }
//        self.connectionData=[[NSMutableData data] retain];
        
        NSMutableData* data=[[NSMutableData alloc] init];
        self.connectionData=data;
        [data release];

    } else {
        // Inform the user that the connection failed.
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in uploading attachment comment"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        [connectFailMessage release];
        
    }
    [myData release];
    return;
//    return  myData;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
//    
//    UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in uploading comment and title"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//    [connectFailMessage show];
//    [connectFailMessage release];
//    
//    
    
    self.projectSite.numCommentUploadFail++;
    [self.projectSite checkuploadCommentStatus];
    
    
    
    [self.projectSite updateLoadingOverlayLabelStatus];
    
    
    
//    [self.. actionOK:nil];
}



#pragma mark NSURLConnection methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.connectionData setLength:0];
    
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [self.connectionData appendData:data];
    
    //    NSLog (@"dataLen:%d",[connectionData length]);
}



-(void) alertUploadResult:(id)sender title:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: title
                          message: @""
                          delegate: self
                          cancelButtonTitle: nil
                          otherButtonTitles: @"ok", nil];
    alert.tag = AttachmentInfoTableUploadComment_CONFIRM_UPLOADFINISH;
    [alert show];
    [alert release];
    
    
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection{    
    
    [_theConnection release],_theConnection=nil;
    [_connectionData release],_connectionData=nil;
    if (self.attachmentInfoDetail){
        [self.attachmentInfoDetail uploadAttachmentCommentFinish];
    }else{
        

        sqlite3 *database = nil;
        
        if (sqlite3_open ([[self.projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
            sqlite3_stmt *deleteAttachCommentStmt;
            const char* deleteAttachCommentSql=[[NSString stringWithFormat:@"delete from attachcomment where ID=%d",
                                              self.attachcommentID] UTF8String];
            
            
            //                NSString* test=[NSString stringWithUTF8String:updateAttachCommentSql];
            if(sqlite3_prepare_v2 (database, deleteAttachCommentSql, -1, &deleteAttachCommentStmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(deleteAttachCommentStmt) == SQLITE_ROW) {
                    
                }
            }
            sqlite3_finalize(deleteAttachCommentStmt);
            
       
        }
        
        sqlite3_close(database);
        
        
        
        self.projectSite.numCommentUploadSuccess++;
        [self.projectSite checkuploadCommentStatus];
        
        
        
        
        
        
        
        [self.projectSite updateLoadingOverlayLabelStatus];
        
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//        appDele.loadingOverlayLabel.text=[NSString stringWithFormat:@"Uploaded %d/%d photo(s) and %d/%d user entered text from the iPad to the Onuma BIM Server. \n\rOne moment while this uploads.",self.projectSite.numFileUploadSuccess,[[self.projectSite aAttachmentInfoTableUploadFile] count],self.projectSite.numCommentUploadSuccess,[[self.projectSite aAttachmentInfoTableUploadComment] count]];
//        
//        if (self.projectSite.numFileUploadFail>0){
//            appDele.loadingOverlayLabel.text=[appDele.loadingOverlayLabel.text stringByAppendingFormat:@"\n\r%d file(s) failed to be uploaded.",[self.projectSite numFileUploadFail]];
//        }
//        
//        
//        if (self.projectSite.numFileUploadFail>0){
//            appDele.loadingOverlayLabel.text=[appDele.loadingOverlayLabel.text stringByAppendingFormat:@"\n\r%d user entered text failed to be uploaded.",[self.projectSite numCommentUploadFail]];
//        }
//        
//        
//        [appDele.loadingOverlayLabel setNeedsDisplay];
        
        
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//        
//        sqlite3 *database = nil;
//        bool bNoMoreAttachComment=true;
//        OPSProjectSite* projectSite=[appDele activeProjectSite];
//        if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
//            sqlite3_stmt *selectAttachFileComment;
//            const char* selectAttachCommentSql=[@"select * from AttachComment" UTF8String];
//            if(sqlite3_prepare_v2 (database, selectAttachCommentSql, -1, &selectAttachFileComment, NULL) == SQLITE_OK) {
//                while(sqlite3_step(selectAttachFileComment) == SQLITE_ROW) {
//                    bNoMoreAttachComment=false;
//                    break;
//                }
//            }
//            sqlite3_finalize(selectAttachFileComment);
//        }
//        
//        sqlite3_close(database);
//        
//        if (bNoMoreAttachComment){
//            
////            NSString* dataStr=[[NSString alloc] initWithData:self.connectionData encoding:NSASCIIStringEncoding] ;
////            //    NSString* dataStr=[[NSString alloc] initWithData:connectionData encoding:NSUTF8StringEncoding] ;
////            NSLog(@"RESPONSEDATA: %@",dataStr);
////            [self alertUploadResult:nil title:dataStr];
////            [dataStr release];
//            [self.projectSite finishUpload];
//        }
    }
}

@end

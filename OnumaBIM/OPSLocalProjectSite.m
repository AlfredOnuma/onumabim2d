//
//  OPSLocalProjectSite.m
//  ProjectView
//
//  Created by Alfred Man on 7/3/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "OPSLocalProjectSite.h"
#import "ProjectViewAppDelegate.h"
#import "OPSStudio.h"
@implementation OPSLocalProjectSite


-(void) uploadProjectSite:(NSArray*) paramArray{
    [super uploadProjectSite:paramArray];   
}
-(id) initWithLiveOPSProjectSite:(OPSProjectSite*) liveProjectSite{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];                
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *databaseFolderPath = [paths objectAtIndex:0]; 
    
    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], [appDele activeStudio].ID, [appDele activeStudio].name, [appDele activeStudio].iconName ]]; 	    
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:databaseFolderPath]) { 
        return nil;
    }
    NSString* filename = [NSString stringWithFormat:@"%@.sqlite",[liveProjectSite name]];
    
        
    NSString *filenameOnly=[liveProjectSite name];//[NSMutableString stringWithString:filename];
    //            [filenameOnly replaceCharactersInRange: [filenameOnly rangeOfString: @".sqlite"] withString: @""];            
    NSMutableDictionary *dictionary=nil;            
    NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];   
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dictionaryPath]) { 
        return nil;
    }
    
    
    
    dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];  
    NSString* pListSt=[dictionary objectForKey:filenameOnly];                        
    //    OPSProjectSite* localProjectSite=[[OPSProjectSite alloc] init:pListSt];
    if (!pListSt){
        return nil;
    }
   
    
    self=[super init:pListSt];
    if (self){
        NSString* localDBPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:filename ] ];
        self.dbPath=localDBPath;
        [localDBPath release];    
        
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:localDBPath]) { 
            return nil;
        }
        
    }
    return self;
    
}

/*

//- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
//{
//    // Append the new data to receivedData.
//    // receivedData is an instance variable declared elsewhere.            
////    [super connection:connection didR
//    [connectionData appendData:data];
//}
//











- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [super connection:connection didFailWithError:error];
//    
//    if (self.attachmentInfoDetailToClose){
//        NSFileManager *fileManager = [NSFileManager defaultManager];
//        
//        
//        
//        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
//        NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];        
//        [fileManager removeItemAtPath:targetPath error:NULL];  
//        
//        
//        
//        UIAlertView *alert = [[UIAlertView alloc] 
//                              initWithTitle: @"No Internet For Upload, Data Saved locally instead."
//                              message: @""
//                              delegate: self
//                              cancelButtonTitle: @"OK"
//                              otherButtonTitles: nil];
//        
//        alert.tag = APPDELEGATE_CONFIRM_UPLOADNOINTERNET; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//        [alert show];
//        [alert release];
//        
//    }else{
        // inform the user
        UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"didFailWithError"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
        [didFailWithErrorMessage show];
        [didFailWithErrorMessage release];
        
        //inform the user
        NSLog(@"Connection failed! Error - %@ %@",
              [error localizedDescription],
              [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
        
//    }
}


//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    switch (alertView.tag) {   
//        case APPDELEGATE_CONFIRM_UPLOADNOINTERNET:
//        {
//            [self.attachmentInfoDetailToClose actionOK:nil];
//        }
//            break;            
//        default:
//            //            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");            
//            break;
//    }	
//}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{        
    [super connectionDidFinishLoading:connection afterUploadSuccessInvoker:self afterUploadSuccessCompleteAction:@selector(trashAllAttachmentEntry) actionParamArray:nil];

}
 */
 
@end

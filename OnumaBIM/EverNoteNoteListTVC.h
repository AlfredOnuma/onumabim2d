//
//  EverNoteNoteListTVC.h
//  ProjectView
//
//  Created by Alfred Man on 11/18/13.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EDAMNotebook;
@class EverNoteNoteBrowserVC;
@interface EverNoteNoteListTVC : UITableViewController
{
    EverNoteNoteBrowserVC* _everNoteNoteBrowserVC;
}

@property (nonatomic, assign) EverNoteNoteBrowserVC* everNoteNoteBrowserVC;
- (id)initWithStyle:(UITableViewStyle)style  everNoteNoteBrowserVC:(EverNoteNoteBrowserVC*) everNoteNoteBrowserVC;

@end

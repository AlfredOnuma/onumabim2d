//
//  EverNoteNoteBookBrowserCell.h
//  ProjectView
//
//  Created by onuma on 31/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EvernoteSDK.h"
@interface EverNoteNoteBookBrowserCell : UITableViewCell
{
    EDAMNotebook* _notebook;
}

- (void)setNotebook:(EDAMNotebook *)notebook;
@property(nonatomic, retain) EDAMNotebook *notebook;
@end

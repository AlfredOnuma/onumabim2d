//
//  ArcProfile.h
//  ProjectView
//
//  Created by onuma on 02/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "RepresentationItem.h"

@interface ArcProfile : RepresentationItem{
    uint _endPointIndex;
    double _centerX;
    double _centerY;
    bool _bAntiClock;
}

@property (nonatomic, assign) uint endPointIndex;
@property (nonatomic, assign) double centerX;
@property (nonatomic, assign) double centerY;
@property (nonatomic, assign) bool bAntiClock;

- (id) init:(NSInteger)newID guid:(NSString *)newGUID name:(NSString *)newName endPointIndex:(uint)endPointIndex centerX:(double)centerX centerY:(double)centerY bAntiClock:(bool) bAntiClock;


-(id)copyWithZone:(NSZone *)zone;

@end


#import <UIKit/UIKit.h>

//#include "OPSMobileViewController.h"

#include "OPSModel.h"
#include "Space.h"
@class NavUI;
@protocol ViewSpaceDelegate
//- (float)smileForFaceView:(FaceView *)requestor;  // -1.0 (frown) to 1.0 (smile)
- (OPSModel*) getModel;
@end

@interface ViewSpace : UIView {    
    id <ViewSpaceDelegate> delegate; 
    Space* space;
    CGMutablePathRef path;

}



@property (assign) id <ViewSpaceDelegate> delegate;
@property (nonatomic, assign) Space* space;
@property (nonatomic, assign) CGMutablePathRef path;
- (void) displaySpaceInfo: (Space*) space;

- (id)initWithFrame:(CGRect)frame space:(Space*) space;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;


@end

//
//  ViewModelToolbar.h
//  ProjectView
//
//  Created by Alfred Man on 11/24/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


//#import "AROverlayViewController.h"
#import "ViewModelToolBarTitleScroll.h"
;
@protocol ViewModelToolbarDelegate
//- (OPSModel*) getModel;
@end
@class ViewModelVC;
@class AROverlayViewController;
//@class ViewModelToolbarCameraPopoverController;
//@class AttachmentPopOverController
@interface ViewModelToolbar : UIToolbar <UINavigationControllerDelegate,UIPopoverControllerDelegate, UIScrollViewDelegate,UIImagePickerControllerDelegate>{
    UIPopoverController* _popoverController;
    UIPopoverController* _imgLibraryPopOverController;
    id<ViewModelToolbarDelegate, UIToolbarDelegate> delegate;
    ViewModelToolBarTitleScroll* titleScroll;
    int _userSelectedColorTableIndex;
    
    
    uint buttonIndex_Evernote;
    uint buttonIndex_FileMakerGO;
    uint buttonIndex_FontSize;
    uint buttonIndex_MultiSelect;  
    uint buttonIndex_titleScroll;
    uint buttonIndex_Tools;
    uint buttonIndex_Edit;    
    uint buttonIndex_Camera;
    uint buttonIndex_Report;    
    uint buttonIndex_Color;    
    uint buttonIndex_BimMail;    
    uint buttonIndex_Attach;
    uint buttonIndex_Sync;
    uint buttonIndex_Info;    
    uint buttonIndex_Browse;    
    uint viewMode; //0=>ScrollViewToolBar | 1=>ProductToolBar
    
    NSString* photoCapturedPath;
    AROverlayViewController* _arOverlayViewController;
}
@property (nonatomic, retain) AROverlayViewController* arOverlayViewController;
@property (nonatomic, assign) int userSelectedColorTableIndex;
@property (nonatomic, retain) ViewModelToolBarTitleScroll* titleScroll;

@property (nonatomic,retain) UIPopoverController* imgLibraryPopOverController;
@property (nonatomic, retain) UIPopoverController *popoverController;
@property (nonatomic, assign) id<ViewModelToolbarDelegate, UIToolbarDelegate> delegate;
@property (nonatomic, assign) uint viewMode;
@property (nonatomic, retain) NSString* photoCapturedPath;

-(ViewModelVC*) parentViewModelVC;
- (id) initWithFrame:(CGRect)frame viewModelVC:(ViewModelVC*)viewModelVC;


//-(NSString*) findProductRepDisplayStr: (ViewProductRep*) productRep;

-(void) displayNextLevel;
- (void) browse:(id)sender;
//- (void) multiSelectButton:(id)sender;
//
//- (void) syncButton:(id)sender;

-(UIButton*) getMultiSelectedButton;
- (void) cameraButton:(id)sender;
-(void) multiSelectButton:(UIButton*) sender;
-(id) attachButton;
-(id) syncButton;
- (void) sync:(id)sender;
-(void) disableAllButtons;
-(void) enableButtons;
- (IBAction) displayAttachInfoTable:(id)sender;
-(void) refreshToolbar;
@end

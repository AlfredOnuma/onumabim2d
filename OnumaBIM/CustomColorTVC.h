//
//  CustomColorTVC.h
//  ProjectView
//
//  Created by Alfred Man on 4/21/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "BIMPlanColorTVC.h"

@interface CustomColorTVC : BIMPlanColorTVC{
    
}
-(void)addToSelection:(NSIndexPath *) indexPath;
-(void)removeFromSelection:(NSIndexPath *) indexPath;


@end

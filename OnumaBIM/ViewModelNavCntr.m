//
//  ViewModelNavCntr.m
//  ProjectView
//
//  Created by Alfred Man on 11/22/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "ViewModelNavCntr.h"



#import "NavPlanNavCntr.h"
//#import "ViewReportNavCntr.h"
#import "ProjectViewAppDelegate.h"
#import "NavUIScroll.h"
#import "ViewModelVC.h"
#import "Project.h"
#import "OPSModel.h"
#import "AttachmentInfoDetail.h"
#import "BIMMailVC.h"
#import "ViewModelToolbar.h"
#import "ViewPlanInfoVC.h"
#import "NavFloorVC.h"
#import "AROverlayViewController.h"
//#import "ViewModelTabBarCtr.h"

@implementation ViewModelNavCntr
@synthesize isEnteringGhostSpace=_isEnteringGhostSpace;
//@synthesize loadingOverlay;
//@synthesize loadingOverlayActIndi;
//@synthesize loadingOverlayLabel;
//@synthesize loadingOverlayProgressBar;
//@synthesize loadingOverlayVC;
//@synthesize disableCascadedPop;



/*
- (UIViewController *)popFloor:(BOOL)animated
{      
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate]; 
    if ([[[self viewControllers] lastObject] isKindOfClass:[ViewModelVC class]]){
        [appDele setModelDisplayLevel:([appDele modelDisplayLevel]-1)];             
        
        [UIView transitionWithView:self.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlDown) animations:^{
            [super popViewControllerAnimated:NO];            
        } completion:^(BOOL finished) {
            ViewModelVC* viewModelVC=[[self viewControllers] lastObject];
            [viewModelVC.navUIScrollVC zoomFitWithDuration:0.5 bRefreshLabel:YES invoker:viewModelVC.opsNavUI completeAction:@selector(refreshProduct) actionParam:nil];           
        }                        
         ];
        return self;
        //        return [super popViewControllerAnimated:animated];
    }    
    return  [super popViewControllerAnimated:animated];
   
}
*/
//-(UIViewController*) popViewControllerAnimated:(BOOL) animated enterSpaceID:(uint)enterSpaceID{
//    [self popViewControllerAnimated:animated];
//    
//}
- (UIViewController *)popViewControllerAnimated:(BOOL)animated
{      
    NSUInteger animationOption=UIViewAnimationOptionTransitionCurlDown;
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    NSInteger ghostSpaceToEnterID=0;
    if ([[[self viewControllers] lastObject] isKindOfClass:[AROverlayViewController class]]){
     
//        AROverlayViewController* arOverlayViewController=(AROverlayViewController*) [self viewControllers].lastObject;
        if ([[[self viewControllers]objectAtIndex:self.viewControllers.count-2] isKindOfClass:[ViewModelVC class]]){
            ViewModelVC* viewModelVC=(ViewModelVC*)[[self viewControllers]objectAtIndex:self.viewControllers.count-2] ;
            [viewModelVC.viewModelToolbar refreshToolbar];
            
//            viewModelVC.viewModelToolbar.arOverlayViewController=nil;
//            [[arOverlayViewController toolbar] refreshToolbar ];
//            [arOverlayViewController.toolbar.arOverlayViewController release];

//            ViewModelVC* viewModelVC=(ViewModelVC*)arOverlayViewController.prevVC;
//            [[viewModelVC viewModelToolbar].arOverlayViewController release];
//            [viewModelVC viewModelToolbar].arOverlayViewController=nil;
        }
        [self.navigationBar setAlpha:1.0];
        [self.navigationBar setTranslucent:NO];
        [super popViewControllerAnimated:YES];
//        ViewModelVC* viewModelVC=[[self viewControllers] lastObject];//
//        [UIView transitionWithView:self.view.window duration:0.5 options:(UIViewAnima) animations:^{
//            [super popViewControllerAnimated:NO];
//            
//        } completion:^(BOOL finished) {
//            //            ViewModelVC* viewModelVC=[[self viewControllers] lastObject];
//            //            ViewModelToolbar* toolbar=[viewModelVC viewModelToolbar];
//            //            [toolbar attachmentInfoTable:[toolbar attachButton]];
//        }
//         ];
//        
        return self;
    }
    
    
    if ([self isEnteringGhostSpace]){//&&[appDele modelDisplayLevel]==2){
        
        
        
//        
//        [self setIsEnteringGhostSpace:false];
        
        
        
        
        if ([[[self viewControllers] lastObject] isKindOfClass:[ViewModelVC class]]){
            ViewModelVC* viewModelVC=[[self viewControllers] lastObject];
            if ([[viewModelVC aSelectedProductRep] count]==1){
                ViewProductRep* viewProductRep=(ViewProductRep*)[[viewModelVC aSelectedProductRep] objectAtIndex:0];
                OPSProduct* product=(OPSProduct*)[viewProductRep product];
                if ([product isKindOfClass:[Space class]]){
                    Space* space=(Space*)product;
                    if ([space isGhostObject]){
                        ghostSpaceToEnterID=space.ID;
                    }
                }
            }

        }
    }
    if ([[[self viewControllers] lastObject] isKindOfClass:[ViewModelVC class]]){
        
        if (![NavFloorVC isPoopingFloor]){// [appDele modelDisplayLevel]>0){
            NSArray* aVC=[self viewControllers];
            
            if ([aVC count]>1){
                ViewModelVC* previousVC=[aVC objectAtIndex:([aVC count]-2)];
                OPSNavUI* previousNavUI=[previousVC opsNavUI];
                NavUIScroll* previousScroll= [previousNavUI navUIScroll];
                DisplayModelUI* previousDisplayModelUI=[previousScroll displayModelUI];
                [previousDisplayModelUI showAllLayer];
//                animationOption=UIViewAnimationOptionTransitionCrossDissolve;
            }   
            
            if ([appDele modelDisplayLevel]==1){
                                
//                [[[viewModelVC  opsNavUI] navUIScroll] zoomToRect:CGRectMake(0, 0, 1024*8, 660*8) animated:NO];
//                if ([appDele currentSite].hasWorldCoord){
//                    
//                    if ([CLLocationManager locationServicesEnabled]){
//                        
//                        double scrollViewZoomScale=[viewModelVC navUIScrollVC].zoomScale;
//                        [[viewModelVC displayModelUI] drawMap: scrollViewZoomScale];                    
//                    }else{
//                        
//                    }
//                    
//                }

            }
        }else{            
               animationOption=UIViewAnimationOptionTransitionCrossDissolve;
        }

        [appDele setModelDisplayLevel:([appDele modelDisplayLevel]-1)];     
        
//        ViewModelVC* viewModelVC=[[self viewControllers] lastObject];
        
        if ([NavFloorVC isPoopingFloor]){
            [super popViewControllerAnimated:NO];
            return self;
        }
//        if ([appDele modelDisplayLevel]==1){
//            [super popViewControllerAnimated:NO];
//            ViewModelVC* viewModelVC=[[self viewControllers] lastObject];
//            if (ghostSpaceToEnterID>0){
//                [viewModelVC selectSpaceByID:ghostSpaceToEnterID];
//                [[viewModelVC viewModelToolbar] performSelector:@selector(browse:) withObject:nil afterDelay:1.0f];
//            }
//            return self;
//        }
        
        [UIView transitionWithView:self.view.window duration:0.5 options:(animationOption) animations:^{
            [super popViewControllerAnimated:NO];            
        } completion:^(BOOL finished) {
            
            ViewModelVC* viewModelVC=[[self viewControllers] lastObject];
//            if ([appDele modelDisplayLevel]==0){
//                [viewModelVC release];
//                viewModelVC=nil;
//            }else{
//                
            [viewModelVC.navUIScrollVC zoomFitWithDuration:0.5 bRefreshLabel:YES invoker:viewModelVC.opsNavUI completeAction:@selector(refreshProduct) actionParam:nil];            
            [appDele setActiveModel:[viewModelVC model]];
            if ([appDele modelDisplayLevel]==0){
                
                [self setIsEnteringGhostSpace:false];
                
                [viewModelVC drawSiteMap];                
                [[viewModelVC displayModelUI] addAllViewProductLabel];

            }else if ([appDele modelDisplayLevel]==1){
                [appDele clearGhostElement];
                if (ghostSpaceToEnterID>0){
                    [viewModelVC selectSpaceByID:ghostSpaceToEnterID];
                    [[viewModelVC viewModelToolbar] performSelector:@selector(browse:) withObject:nil afterDelay:1.0f];

                }else{
                    
                    [self setIsEnteringGhostSpace:false];
                    
                    
                    [[viewModelVC displayModelUI] addAllViewProductLabel];
                    [[viewModelVC opsNavUI] showHideProductLable];
                    
                    
//                    [[viewModelVC opsNavUI] refreshProductLabel];
                }
            
            }
      
        }                        
         ];
        return self;
//        return [super popViewControllerAnimated:animated];
    }
    
    if ([[[self viewControllers] lastObject] isKindOfClass:[BIMMailVC class]]){
        [UIView transitionWithView:self.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlDown) animations:^{
            [super popViewControllerAnimated:NO];            

        } completion:^(BOOL finished) {
//            ViewModelVC* viewModelVC=[[self viewControllers] lastObject]; 
//            ViewModelToolbar* toolbar=[viewModelVC viewModelToolbar];            
//            [toolbar attachmentInfoTable:[toolbar attachButton]];            
        }                        
         ];

        return self;        
    }
    
    if ([[[self viewControllers] lastObject] isKindOfClass:[ViewPlanInfoVC class]]){
        [UIView transitionWithView:self.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlDown) animations:^{
            [super popViewControllerAnimated:NO];            
            
        } completion:^(BOOL finished) {
//            uint t=0;
//            t=1;
            //            ViewModelVC* viewModelVC=[[self viewControllers] lastObject];
            //            ViewModelToolbar* toolbar=[viewModelVC viewModelToolbar];            
            //            [toolbar attachmentInfoTable:[toolbar attachButton]];            
        }                        
         ];
        
        return self;        
    }
    
    if ([[[self viewControllers] lastObject] isKindOfClass:[AttachmentInfoDetail class]]){
        
//        ViewModelVC* viewModelVC=[[self viewControllers] objectAtIndex:([[ self viewControllers] count]-2)]; 
        if ([appDele isEnteringNoteDetailFromSyncTable]){
            [UIView transitionWithView:self.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlDown) animations:^{
    //            [viewModelVC.viewModelToolbar.popoverController dismissPopoverAnimated:NO];
                 [super popViewControllerAnimated:NO];
                
                //        viewModelVC.view.frame = CGRectOffset(viewModelVC.view.frame, 0, -viewModelVC.view.frame.size.height);
            } completion:^(BOOL finished) {
                [appDele setIsEnteringNoteDetailFromSyncTable:false];
                ViewModelVC* viewModelVC=[[self viewControllers] lastObject]; 
                ViewModelToolbar* toolbar=[viewModelVC viewModelToolbar];
                id sender=[toolbar syncButton];
                [toolbar sync:sender];//[toolbar attachButton]];
            }                        
             ];
            return self;

        }else{
            
            ViewModelVC* viewModelVC=[[self viewControllers] objectAtIndex:([[ self viewControllers] count]-2)];
            ViewModelToolbar* toolbar=[viewModelVC viewModelToolbar];
            [toolbar refreshToolbar];
            
            return  [super popViewControllerAnimated:animated];
        }
//        [[[toolbar items] objectAtIndex:3] sendEvent:UITouchPhaseBegan];
    }
    return  [super popViewControllerAnimated:animated];
    //  
    /*
    
    //    UIViewController* returnVC=[super popViewControllerAnimated:animated];
    //    id test=[self.viewControllers lastObject];
    //    if([[self.viewControllers lastObject] class] == [MyCOntroller class]){
    if (self.disableCascadedPop){
        [self setDisableCascadedPop:false];
        //        return [super popViewControllerAnimated:animated];
    }else{
        
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate]; 
        [appDele setModelDisplayLevel:([appDele modelDisplayLevel]-1)];
        
        NSMutableArray *localViewControllersArray =[appDele modelViewTabBarController].viewControllers ;
        NavPlanNavCntr *navPlanNavCntr = [localViewControllersArray  objectAtIndex:1];
        ViewReportNavCntr *viewReportNavCntr = [localViewControllersArray  objectAtIndex:2];
        //    if ([appDele modelDisplayLevel]==0){        
        //        [navPlanNavCntr popViewControllerAnimated:false];
        //    }
        [navPlanNavCntr setDisableCascadedPop:TRUE];
        [navPlanNavCntr popViewControllerAnimated:false];
        [viewReportNavCntr setDisableCascadedPop:TRUE];
        [viewReportNavCntr popViewControllerAnimated:false];

//         if([[self.viewControllers lastObject] class] == [SettingsTableController class]){
//         
//         [UIView beginAnimations:nil context:NULL];
//         [UIView setAnimationDuration: 1.00];
//         [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown
//         forView:self.view cache:NO];
//         
//         UIViewController *viewController = [super popViewControllerAnimated:NO];
//         
//         [UIView commitAnimations];
//         
//         return viewController;
//         } else {
//         return [super popViewControllerAnimated:animated];
//         }

        
    }
    //    return returnVC;
*/    
//    return [super popViewControllerAnimated:animated];
}
//@synthesize viewModelVC;

- (id) init{
    self=[super init];
    if (self){
        self.isEnteringGhostSpace=false;
//        disableCascadedPop=false;                
    }
    return self;
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {     

/*
        
         UIView* _loadingOverlay=[[UIView alloc]
         initWithFrame:CGRectMake(0.0f,0.0f,self.view.frame.size.width,self.view.frame.size.height)];
         self.loadingOverlay = _loadingOverlay;
         [_loadingOverlay release];
         loadingOverlay.backgroundColor=[UIColor blackColor];
         loadingOverlay.alpha = 0.4;
         //        [opsNavUI addSubview:loadingOverlay];
         
         
         loadingOverlayLabel=[[UILabel alloc]
         initWithFrame:CGRectMake(0.0f, 0.0f, 800.0f, 60.0f)];
         loadingOverlayLabel.center=loadingOverlay.center;
         loadingOverlayLabel.textAlignment=UITextAlignmentCenter;
         loadingOverlayLabel.textColor = [UIColor whiteColor];
         //        downloadLabel.text=@"asdfffffffffffffffffffffffff";
         loadingOverlayLabel.backgroundColor = [UIColor clearColor];
         loadingOverlayLabel.font = [UIFont boldSystemFontOfSize:24];
         
         loadingOverlayLabel.frame=CGRectOffset(loadingOverlayLabel.frame, 0, -40.0f);
         [loadingOverlay addSubview:loadingOverlayLabel];
         
         
         
         loadingOverlayActIndi = [[UIActivityIndicatorView alloc] 
         initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, 80.0f)];
         [loadingOverlayActIndi setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
         loadingOverlayActIndi.center=loadingOverlay.center;
         [loadingOverlay addSubview:loadingOverlayActIndi];
         
        
        UIViewController* _loadingOverlayVC=[[UIViewController alloc] init ];
        [_loadingOverlayVC setView:loadingOverlay];
        self.loadingOverlayVC=_loadingOverlayVC;
        [_loadingOverlayVC release];
                  */                          
//        disableCascadedPop=false;
        // Custom initialization
    }
    /*
     ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];   
     
     
     switch ([[appDele model] displayLevel]) {
     case 0:
     self.title=@"Site View";
     break;
     case 1:
     self.title=@"Floor View";
     break;
     case 2:
     self.title=@"Space View";
     break;
     
     default:
     break;
     } 
     */
    
    return self;
}

- (void)dealloc
{
//    [loadingOverlayVC release], loadingOverlayVC=nil;
//    [loadingOverlay release],loadingOverlay=nil;
//    [loadingOverlayLabel release], loadingOverlayLabel=nil;
//    [loadingOverlayActIndi release], loadingOverlayActIndi=nil;
//    [loadingOverlayProgressBar release], loadingOverlayProgressBar=nil;     
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

/*

//// Tell the system what we support
- (NSUInteger)supportedInterfaceOrientations {
    //    return UIInterfaceOrientationMaskAllButUpsideDown;
    return UIInterfaceOrientationMaskLandscapeRight;//UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskPortraitUpsideDown;//UIInterfaceOrientationMaskAllButUpsideDown;
    //    return UIInterfaceOrientationMaskPortrait;
}
//
// Tell the system It should autorotate
- (BOOL) shouldAutorotate {
    return YES;
    
}
// Tell the system which initial orientation we want to have
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}

*/
@end

//
//  ViewModelVC.m
//  ProjectView
//
//  Created by Alfred Man on 11/2/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//
#import "ViewModelNavCntr.h"
#import "Representation.h"
#import "RepresentationItem.h"
#import "ViewProductRep.h"
//#import "NavScrollUIToolbar.h"
//#import "ProductViewToolbar.h"
#import "ViewModelToolbar.h"
#import "ViewModelVC.h"
//#import "ViewModelTabBarCtr.h"
//#import "NavPlanNavCntr.h"
#import "ProjectViewAppDelegate.h"
#import "DisplayModelUI.h"
#import "OPSNavUI.h"
#import "NavUIScroll.h"

#import "TestUI3.h"

#import "OPSModel.h"
#import "OPSProduct.h"
#import "TransparentUIToolBar.h"
#import "NavModelVC.h"
//#import "ProductViewButtonRing.h"
#import "Bldg.h"
#import "NavPlanNavCntr.h"
#import "NavFloorVC.h"
#import "TestUI4.h"
#import "Placement.h"


#import "site.h"
#import "Floor.h"
#import "Space.h"
#import "MapTypeListTVC.h"
#import "Slab.h"
#define ZOOM_STEP 1.5  
#import "AttachmentWebViewController.h"

#import <QuartzCore/QuartzCore.h> 

@implementation ViewModelVC
//@synthesize k;
@synthesize currentColorMethod=_currentColorMethod;
@synthesize mapLogo;
@synthesize opsNavUI;
@synthesize model;

@synthesize modelNavUILevel;
@synthesize navUIScrollVC;
//@synthesize selectedProduct;
@synthesize displayModelUI;
@synthesize viewModelToolbar;
@synthesize popoverController;
@synthesize viewProductTextMethod;

//@synthesize selectedProductRep;
@synthesize aSelectedProductRep;
@synthesize aFontSize;
@synthesize pFontSize;
//@synthesize mapView;
@synthesize lastDeselectdViewProductRep;
@synthesize bMultiSelect;
@synthesize browseRightAfterLoad=_browseRightAfterLoad;


-(void) increaseFontSize{
    if (self.pFontSize<[[self aFontSize] count]-1){
        self.pFontSize++;
    }
    [self refreshLabel];
}
-(void) decreaseFontSize{
    if (self.pFontSize>0){
        self.pFontSize--;
    }
    [self refreshLabel];
}
-(void) refreshLabel{
    [[self opsNavUI] refreshProductLabel];
}

- (void)dealloc
{    

    [mapLogo release];mapLogo=nil;
    [aFontSize release],aFontSize=nil;
    [navUIScrollVC release],navUIScrollVC=nil;
    [viewModelToolbar release],viewModelToolbar=nil;
    
    [popoverController release],popoverController=nil;
    
     
     [displayModelUI release],displayModelUI=nil;     
     [opsNavUI release],opsNavUI=nil;
    
    [model release];    
     
    model=nil;    
    
    
    [aSelectedProductRep release];aSelectedProductRep=nil;
    [super dealloc];
}



//@synthesize loadingOverlay;
//@synthesize loadingOverlayActIndi;
//@synthesize loadingOverlayLabel;
//@synthesize loadingOverlayProgressBar;



//@synthesize  testUI;
//- (void)userSelectedRowInPopover:(NSUInteger)row {
//    [self.popoverController dismissPopoverAnimated:YES];
//    self.popoverController = nil;
//}

//-(void) drawRect:(CGRect)rect{
//    UIView* t=[[UIView alloc] initWithFrame:CGRectMake(0,0,400,400)];
//    t.backgroundColor=[UIColor redColor];
//    [self addSubview:t];
//}
- (void) floorList:(id)sender{  
    if (self.popoverController!=nil){
        [self.popoverController dismissPopoverAnimated:NO];
        self.popoverController=nil;        
    }
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];     
    
//    Bldg* bldg=[[[appDele activeViewModelVC] model] root];
    Bldg* bldg=(Bldg*) [[self model] root];  
    
//    NavPlanNavCntr* navPlanNavCntr=[[NavPlanNavCntr alloc] init];
    
    NavFloorVC* navFloorVC=[[NavFloorVC alloc] init:self];//initWithNibName:@"NavFloorVC" bundle:Nil];
    [navFloorVC setSelectedBldg:bldg];
    
    NSString* title=[NSString stringWithFormat:@"Bldg: %d - %@ Floor List",[bldg ID],[bldg name] ];    
    navFloorVC.title=title;    
    
    

    uint numCell=[[[bldg relAggregate] related] count];
    if (numCell>14){ numCell=14;}
    navFloorVC.contentSizeForViewInPopover=CGSizeMake(360, (numCell*44) );
    
    
//    [navPlanNavCntr pushViewController:navFloorVC animated:NO];      
    //            MyViewController * myViewController = [[[MyViewController alloc] init] autorelease];
    //            myViewController.previousViewController = self;
    UIPopoverController* floorListPopOverController=[[UIPopoverController alloc] 
                                                     initWithContentViewController:navFloorVC];
    self.popoverController = floorListPopOverController;// autorelease];
    [floorListPopOverController release];

    [navFloorVC release];
    
    if ([sender isKindOfClass:[UIButton class]]){
        //       sender= [((UIButton*)sender) ] 
        UIButton* button=sender;
        //        sender=[((UIButton*) sender) superview];
        [self.popoverController presentPopoverFromRect:button.bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES ];
    }else{
        [self.popoverController presentPopoverFromBarButtonItem:sender
                              permittedArrowDirections:UIPopoverArrowDirectionUp
                                              animated:YES];      
    }
//    [popoverController release];
    
    
}


- (void) mapTypeList:(id)sender{
    
    if (self.popoverController!=nil){
        [self.popoverController dismissPopoverAnimated:NO];
        self.popoverController=nil;        
    }

    MapTypeListTVC* mapTypeListTVC=[[MapTypeListTVC alloc] init:self];//initWithNibName:@"NavModelVC" bundle:Nil];
    uint numCell=3;
    mapTypeListTVC.contentSizeForViewInPopover=CGSizeMake(150, (numCell*44));
    UIPopoverController* mapTypeListPopoverController=[[UIPopoverController alloc] 
                                                          initWithContentViewController:mapTypeListTVC];
    self.popoverController = mapTypeListPopoverController;// autorelease];
    [mapTypeListPopoverController release];
    
    [mapTypeListTVC release];
    
//    [self.popoverController presentPopoverFromBarButtonItem:sender
//                                   permittedArrowDirections:UIPopoverArrowDirectionUp
//                                                   animated:YES]; 
    
    if ([sender isKindOfClass:[UIButton class]]){
        //       sender= [((UIButton*)sender) ] 
        UIButton* button=sender;
        //        sender=[((UIButton*) sender) superview];
        [self.popoverController presentPopoverFromRect:button.bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES ];
    }else{
        [self.popoverController presentPopoverFromBarButtonItem:sender
                                       permittedArrowDirections:UIPopoverArrowDirectionAny
                                                       animated:YES];      
    }    
    //    [popoverController release];
    
    
}
- (void) relatedAggList:(id)sender{  
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];   
    
    if (self.popoverController!=nil){
        [self.popoverController dismissPopoverAnimated:NO];
        self.popoverController=nil;        
    }
//    Bldg* bldg=(Bldg*) [(OPSModel*)[(ViewModelVC*) [appDele activeViewModelVC] model] root];    
    //    NavPlanNavCntr* navPlanNavCntr=[[NavPlanNavCntr alloc] init];
    NavModelVC* navModelVC=[[NavModelVC alloc] init:self];//initWithNibName:@"NavModelVC" bundle:Nil];
//    [navModelVC setSelectedBldg:bldg];
    
    
    SpatialStructure* spatialStruct=(SpatialStructure*)[ [self model] root];   
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    uint numCell=0;
    if ([appDele modelDisplayLevel]==0){
        
//        Bldg* bldg=(Bldg*) spatialStruct;
//        Floor* floor=[bldg sel];
//        numCell=[[[floor relAggregate] related] count];
        
        
        numCell=[[[spatialStruct relAggregate] related] count];
    }else if ([appDele modelDisplayLevel]==1){
        Bldg* bldg=(Bldg*) spatialStruct;
        Floor* floor=[bldg selectedFloor];
        
        numCell=[[[floor relAggregate] related] count];                
    }else if ([appDele modelDisplayLevel]==2){
        numCell=[[[spatialStruct relContain] related] count];
    }
    

    if (numCell>14){ numCell=14;}
    if ([appDele modelDisplayLevel]==1){
        navModelVC.contentSizeForViewInPopover=CGSizeMake(500, (numCell*44) );
    }else{
        navModelVC.contentSizeForViewInPopover=CGSizeMake(360, (numCell*44) );
    }
    
    
//    NSString* title=[NSString stringWithFormat:@"Bldg: %@ Floor List",[bldg name] ];    
//    navModelVC.title=title;                                                
    //    [navPlanNavCntr pushViewController:navFloorVC animated:NO];      
    //            MyViewController * myViewController = [[[MyViewController alloc] init] autorelease];
    //            myViewController.previousViewController = self;
    UIPopoverController* relatedAggListPopoverController=[[UIPopoverController alloc] 
                                                          initWithContentViewController:navModelVC];
    self.popoverController = relatedAggListPopoverController;// autorelease];
    [relatedAggListPopoverController release];
    
    [navModelVC release];
    
    if ([sender isKindOfClass:[UIButton class]]){
//       sender= [((UIButton*)sender) ] 
        UIButton* button=sender;
//        sender=[((UIButton*) sender) superview];
        [self.popoverController presentPopoverFromRect:button.bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES ];
    }else{
        [self.popoverController presentPopoverFromBarButtonItem:sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp
                                                   animated:YES];       
    }
    //    [popoverController release];
    
    
}
/*
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self refreshProductLabel];
}
- (void)scrollViewDidEndZooming:(UIScrollView *)aScrollView withView:(UIView *)view atScale:(float)scale {        
    
    [self refreshProductLabel];
    
    
     
//     for (UIView* subview in [self.spatialStructViewLayer subviews]){
//     if ([appDele modelDisplayLevel]==0) {break;}
//     if ([appDele modelDisplayLevel]==2) {break;}        
//     if (![subview isKindOfClass:([ViewProductRep class])]){continue;}
//     ViewProductRep* viewProductRep=subview;                
//     CGPoint pt = [[touches anyObject] locationInView:viewProductRep]; 
//     
//     if ([viewProductRep containsPoint:pt]){
//     [viewModelVC selectAProduct: ([viewProductRep product])];
//     
//     [viewProductRep setNeedsDisplay];
//     numTouchedProduct++;
//     }        
//     }    
//     for (UIView* subview in [self.spatialStructSlabViewLayer subviews]){
//     
//     if ([appDele modelDisplayLevel]==1) {break;}        
//     if (![subview isKindOfClass:([ViewProductRep class])]){continue;}
//     ViewProductRep* viewProductRep=subview;                
//     CGPoint pt = [[touches anyObject] locationInView:viewProductRep]; 
//     
//     if ([viewProductRep containsPoint:pt]){
//     [viewModelVC selectAProduct: ([viewProductRep product])];
//     
//     [viewProductRep setNeedsDisplay];
//     numTouchedProduct++;
//     
//     }
//     }    
//     for (UIView* subview in [self.elemViewLayer subviews]){
//     if (![subview isKindOfClass:([ViewProductRep class])]){continue;}
//     ViewProductRep* viewProductRep=subview;                
//     CGPoint pt = [[touches anyObject] locationInView:viewProductRep];         
//     if ([viewProductRep containsPoint:pt]){
//     [viewModelVC selectAProduct: ([viewProductRep product])];
//     [viewProductRep setNeedsDisplay];
//     numTouchedProduct++;
//     }
//     }       
//     
//    self.navUIScrollVC;
}
*/

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];   
    
    
            
//    CGRect appWindowFrame= [[appDele window] frame];
//    CGFloat statusBarHeight=20;
    //    CGRect statusBarFrame= [[UIApplication sharedApplication] statusBarFrame];
//    CGRect navigationBarFrame=self.navigationController.navigationBar.frame;
    CGRect navigationToolbarFrame = self.navigationController.toolbar.frame;
    //    CGRect customToolbarFrame = CGRectOffset(navigationToolbarFrame, 0.0, appWindowFrame.size.height-navigationToolbarFrame.size.height-navigationBarFrame.size.height-statusBarFrame.size.height);    
    /*
     CGFloat opsNavUIOffsetY=statusBarHeight+navigationBarFrame.size.height;
     self.opsNavUI.frame=CGRectMake(0, opsNavUIOffsetY, appWindowFrame.size.width, appWindowFrame.size.height-opsNavUIOffsetY);
     CGFloat test=[self.opsNavUI frame].size.height-toolbarHeight;
     //    CGRect toolBarRect=CGRectMake(0,[self.opsNavUI frame].size.height-toolbarHeight,[self.opsNavUI frame].size.width, toolbarHeight);
     
     */
    
    
    CGRect customToolbarFrame =CGRectMake(0, self.opsNavUI.frame.size.height-navigationToolbarFrame.size.height, navigationToolbarFrame.size.width, navigationToolbarFrame.size.height);
    //CGRectOffset(navigationToolbarFrame, 0.0, -navigationToolbarFrame.size.height-statusBarFrame.size.height);     
    self.viewModelToolbar.frame=customToolbarFrame;

    

}


-(ViewProductRep*) lastSelectedProductRep{
    if (aSelectedProductRep==nil || ([aSelectedProductRep count]<=0)){ return nil;}
    
    return [aSelectedProductRep lastObject];
}


-(ViewProductRep*) selectOnlyLastDeselctedProductRepInMemory{
    if (self.lastDeselectdViewProductRep==nil) {return nil;}
    if (aSelectedProductRep==nil){ 
        NSMutableArray* _aSelectedProductRep=[[NSMutableArray alloc] init];
        self.aSelectedProductRep=_aSelectedProductRep;
        [_aSelectedProductRep release];                
    }
//    if ([aSelectedProductRep count]<=0){ //if nothing is selected, select the top level object's 1st representation view
//        [self.viewModelToolbar refreshToolbar];
//        return nil;
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//        ViewProductRep* topLevelProduct1stRep=nil;
//        if ([appDele modelDisplayLevel]==1){
//            Floor* floor=[((Bldg*) [model root])selectedFloor];
//            topLevelProduct1stRep=[floor firstViewProductRep];
//            //            topLevelProduct1stRep=[[[[floor relContain] related] objectAtIndex:0] firstViewProductRep];
//        }else{
//            topLevelProduct1stRep=[((OPSProduct*) [model root]) firstViewProductRep];
//        }
//        [topLevelProduct1stRep setSelected:TRUE];        
//        [topLevelProduct1stRep.viewProductLabel display];
//        [topLevelProduct1stRep setNeedsDisplay];
//        [aSelectedProductRep addObject:topLevelProduct1stRep];
//        [self.viewModelToolbar refreshToolbar];
//        return topLevelProduct1stRep;
//    }
    [self clearSelectedProductRep];
    [self.lastDeselectdViewProductRep setSelected:true];
    [self.lastDeselectdViewProductRep.viewProductLabel display];
    [self.lastDeselectdViewProductRep setNeedsDisplay];
//    [aSelectedProductRep addObject:self.lastDeselectdViewProductRep];
    lastDeselectdViewProductRep=nil;
//    
//    while ([aSelectedProductRep count]>1){
//        ViewProductRep* productRep=[aSelectedProductRep objectAtIndex:0];
//        [productRep setSelected:false];
//        [productRep.viewProductLabel display];
//        [productRep setNeedsDisplay];
//        [aSelectedProductRep removeObjectAtIndex:0];        
//    }
    [self.viewModelToolbar refreshToolbar];
    return [aSelectedProductRep objectAtIndex:0];
    
    
    
}


-(ViewProductRep*) selectTopLevelProductRep{
    if (aSelectedProductRep==nil){ 
        NSMutableArray* _aSelectedProductRep=[[NSMutableArray alloc] init];
        self.aSelectedProductRep=_aSelectedProductRep;
        [_aSelectedProductRep release];                
    }
//    if ([aSelectedProductRep count]<=0){ //if nothing is selected, select the top level object's 1st representation view
//        [self.viewModelToolbar refreshToolbar];
//        return nil;
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        ViewProductRep* topLevelProduct1stRep=nil;
        if ([appDele modelDisplayLevel]==1){
            Floor* floor=[((Bldg*) [model root])selectedFloor];
            topLevelProduct1stRep=[floor firstViewProductRep];
            //            topLevelProduct1stRep=[[[[floor relContain] related] objectAtIndex:0] firstViewProductRep];
        }else{
            topLevelProduct1stRep=[((OPSProduct*) [model root]) firstViewProductRep];
        }
        [topLevelProduct1stRep setSelected:TRUE];        
        [topLevelProduct1stRep.viewProductLabel display];
        [topLevelProduct1stRep setNeedsDisplay];
//        [aSelectedProductRep addObject:topLevelProduct1stRep];
        [self.viewModelToolbar refreshToolbar];
        return topLevelProduct1stRep;
//    }
    //    uint pProductRep=0;
    //    for (uint pProductRep=0;pProductRep<[aSelectedProductRep count]-1;pProductRep++){
//    while ([aSelectedProductRep count]>1){
//        ViewProductRep* productRep=[aSelectedProductRep objectAtIndex:0];
//        [productRep setSelected:false];
//        [productRep.viewProductLabel display];
//        [productRep setNeedsDisplay];
//        [aSelectedProductRep removeObjectAtIndex:0];        
//    }
//    [self.viewModelToolbar refreshToolbar];
//    return [aSelectedProductRep objectAtIndex:0];
    
    
    
}


-(ViewProductRep*) selectOnlyLastProductRep{

    if (aSelectedProductRep==nil){ 
        NSMutableArray* _aSelectedProductRep=[[NSMutableArray alloc] init];
        self.aSelectedProductRep=_aSelectedProductRep;
        [_aSelectedProductRep release];                
    }
    if ([aSelectedProductRep count]<=0){ //if nothing is selected, select the top level object's 1st representation view
        [self.viewModelToolbar refreshToolbar];
        return nil;
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        ViewProductRep* topLevelProduct1stRep=nil;
        if ([appDele modelDisplayLevel]==1){
            Floor* floor=[((Bldg*) [model root])selectedFloor];
            topLevelProduct1stRep=[floor firstViewProductRep];
//            topLevelProduct1stRep=[[[[floor relContain] related] objectAtIndex:0] firstViewProductRep];
        }else{
            topLevelProduct1stRep=[((OPSProduct*) [model root]) firstViewProductRep];
        }
        [topLevelProduct1stRep setSelected:TRUE];        
        [topLevelProduct1stRep.viewProductLabel display];
        [topLevelProduct1stRep setNeedsDisplay];
//        [aSelectedProductRep addObject:topLevelProduct1stRep];
        [self.viewModelToolbar refreshToolbar];
        return topLevelProduct1stRep;
    }
//    uint pProductRep=0;
//    for (uint pProductRep=0;pProductRep<[aSelectedProductRep count]-1;pProductRep++){
    while ([aSelectedProductRep count]>1){
        ViewProductRep* productRep=[aSelectedProductRep objectAtIndex:0];
        [productRep setSelected:false];
        [productRep.viewProductLabel display];
        [productRep setNeedsDisplay];
//        [aSelectedProductRep removeObjectAtIndex:0];        
    }
    [self.viewModelToolbar refreshToolbar];
    return [aSelectedProductRep objectAtIndex:0];
    
    
     
}
-(bool) isSelectingBrowsableProduct{
//    bool bBrowsableProductSelected=false;
    if ([self.aSelectedProductRep count]<1){return false;}
    if ([self isSelectingTopLevelSpatialStruct]){return false;}
    return true;
}
-(bool) isSelectingGhostStruct{
    bool bSelectedGhost=false;
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    if ([self.aSelectedProductRep count]!=1) {return false;}
    
    if (self.aSelectedProductRep==nil || [self.aSelectedProductRep count]<=0){
        return false;
    }
    ViewProductRep* productRep=[self.aSelectedProductRep objectAtIndex:0];
    
    if ([[productRep product] isGhostObject]){
        bSelectedGhost=true;
    }
    return bSelectedGhost;
}
-(bool) isSelectingTopLevelSpatialStruct{
    
    bool bTopLevelSelected=false;
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([self.aSelectedProductRep count]!=1) {return false;}
    ViewProductRep* productRep=[self.aSelectedProductRep objectAtIndex:0];
    
    switch ([appDele modelDisplayLevel]) {
        case 0:
            if ([[productRep product] isKindOfClass:[Site class]]){                
                bTopLevelSelected=true;//[self switchToolbarToNavScrollUIToolbar];
                //                return;
            }
            break;            
        case 1:
            if ([[productRep product] isKindOfClass:[Floor class]]){
                bTopLevelSelected=true;//[self switchToolbarToNavScrollUIToolbar];
                //                return;
            }
            break;                 
        case 2:
            if ([[productRep product] isKindOfClass:[Space class]]){
                if (![[productRep product] isGhostObject]){
                    bTopLevelSelected=true;//[self switchToolbarToNavScrollUIToolbar];
//                    //                return;                    
                }
            }
            break;             
        default:
            break;
    }
    return bTopLevelSelected;
}

-(bool) didSelectOneLevelCascadedProduct{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if ([[self aSelectedProductRep] count]>1){
        return true;
    }
    if ([[self aSelectedProductRep] count]==1){
        ViewProductRep* _productRep=[[self aSelectedProductRep] objectAtIndex:0];
        
        switch ([appDele modelDisplayLevel]) {
            case 0:
                if ([[_productRep product] isKindOfClass:[Site class]]){
                    return false;
                }else{
                    return true;
                }
                break;            
            case 1:
                if ([[_productRep product] isKindOfClass:[Floor class]]){
                    return false;
                }else{
                    return true;
                }
                break;                 
            case 2:
                if ([[_productRep product] isKindOfClass:[Space class]]){
                    return false;
                }else{
                    return true;
                }
                break;             
            default:
                return false;
                break;
        }
        
    }
    return false;         
}

-(void) addAProductRepToSelection:(ViewProductRep*)_productRep{
    if (_productRep.selected) {return;}
    
    
    bool bMultiSelectState=self.bMultiSelect;
    self.bMultiSelect=true;
    [self selectAProductRep:_productRep];
    
    if ([[self aSelectedProductRep] count]>1){
        self.bMultiSelect=true;
    }else{
        
        self.bMultiSelect=bMultiSelectState;
    }
//    self.bMultiSelect=bMultiSelectState;

}

-(void) removeAProductRepFromSelection:(ViewProductRep*)_productRep{
    if (!_productRep.selected) {return;}
    bool bMultiSelectState=self.bMultiSelect;
    self.bMultiSelect=true;
    [self selectAProductRep:_productRep];
    self.bMultiSelect=bMultiSelectState;
    
}

-(void) selectSpaceByID:(NSInteger)spaceID{
//    OPSProduct* rootProduct=(OPSProduct*)[[self model] root];
//    if (![rootProduct isKindOfClass:[Floor class]]){
//        return;
//    }
//    Floor* rootFloor=(Floor*) rootProduct;
//    if ([rootFloor relAggregate]==nil){return;}
//    for (UIView* subView in [[[self displayModelUI] spatialStructViewLayer] subviews]){
    
    for (UIView* subView in [self displayModelUI].spatialStructViewLayer.subviews){
        if ([subView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*)subView;
            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                Space* subSpace=(Space*)[viewProductRep product];
                if (subSpace.ID==spaceID){
                    [self selectAProductRep:viewProductRep];
                }
            }
        }
    }
         
//    for (OPSProduct* relAggregateProduct in [rootFloor relAggregate].related){
//        if ([relAggregateProduct isKindOfClass:[Space class]]){
//            Space* relatedSpace=(Space*) relAggregateProduct;
//            if (relatedSpace.ID=spaceID){
//                [[[relatedSpace representation] aRepresentationItem] objectAtIndex:0];                
//            }
//        }
//    }
}
-(void) selectAProductRep:(ViewProductRep*)_productRep{
    

    lastDeselectdViewProductRep=nil;
    if (aSelectedProductRep==nil){
        NSMutableArray* _aSelectedProductRep=[[NSMutableArray alloc] init];
        self.aSelectedProductRep=_aSelectedProductRep;
        [_aSelectedProductRep release];
    }

    
    if ([[_productRep product] isGhostObject]){
        if (bMultiSelect ){            
            return;
        }
//        bool selected=_productRep.selected;
//        [self clearSelectedProductRep];
//        [_productRep setSelected:!selected];
//        [_productRep.viewProductLabel display];
//        [_productRep setNeedsDisplay];
//        [aSelectedProductRep addObject:_productRep];
//        
//        [self.viewModelToolbar refreshToolbar];
//        return;
    }

    NSLog (@"Selected Product type: %@ ID: %d",[[[_productRep product] class]  description], [[_productRep product] ID]);
    bool bTopLevelSelected=false;
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    switch ([appDele modelDisplayLevel]) {
        case 0:            
            if ([[_productRep product] isKindOfClass:[Site class]]){
                bTopLevelSelected=true;//[self switchToolbarToNavScrollUIToolbar];
//                return;
            }
            break;            
        case 1:
            if ([[_productRep product] isKindOfClass:[Floor class]]){
                bTopLevelSelected=true;//[self switchToolbarToNavScrollUIToolbar];
//                return;
            }
            break;                 
        case 2:
            if ([[_productRep product] isKindOfClass:[Space class]]){
                if (![[_productRep product] isGhostObject]){
                    bTopLevelSelected=true;//[self switchToolbarToNavScrollUIToolbar];
                }
//                return;
            }
            break;             
        default:
            break;
    }
    
    if (bTopLevelSelected){  
        if (!bMultiSelect){
            bool selected=_productRep.selected;
            [self clearSelectedProductRep];
//****************
            //****************//****************//****************//****************//****************//****************//****************//****************//****************//****************
            //****************//****************
            //****************
            //****************
            //****************
            //****************//****************
            //****************
            //****************
            //****************
            //****************
            //****************
            [_productRep setSelected:!selected];
            [_productRep.viewProductLabel display];
            [_productRep setNeedsDisplay];
            
            
            
//            [aSelectedProductRep addObject:_productRep];
        }
        
        /*
        bool bAddSiteToSelectionAfterRemoveAll=true;
        for (ViewProductRep* productRep in aSelectedProductRep){            
            if ([productRep product]==[_productRep product]){
                bAddSiteToSelectionAfterRemoveAll=false; //if site is already selected, this will deselect site and don't add site back to selection again;
            }
            [productRep setSelected:false];
            [productRep.viewProductLabel display];
            [productRep setNeedsDisplay];
        }
        [aSelectedProductRep removeAllObjects];
        
        if (bAddSiteToSelectionAfterRemoveAll){
            [_productRep setSelected:true];
            [_productRep.viewProductLabel display];
            [_productRep setNeedsDisplay];
            [aSelectedProductRep addObject:_productRep];
        }
        */
    }else{//If user just tap on a non-top level product
        if (!self.bMultiSelect){            
            bool selected=_productRep.selected;
            [self clearSelectedProductRep];            
            [_productRep setSelected:!selected];
            [_productRep.viewProductLabel display];
            [_productRep setNeedsDisplay];
//            [aSelectedProductRep addObject:_productRep];
        }else{
            if ([[self aSelectedProductRep] count]==1 && [self isSelectingTopLevelSpatialStruct]){                
                [self clearSelectedProductRep];     
            }
            bool selected=_productRep.selected;
//            [self clearSelectedProductRep];
            
            [_productRep setSelected:!selected];
            [_productRep.viewProductLabel display];
            [_productRep setNeedsDisplay];
//            [aSelectedProductRep addObject:_productRep];
            
        }
        /*
        uint pSelectedProductRep=0;
        //make sure no top level product is already in selection pool, otherwise deselect it graphically and  remove from selection pool
        if ([aSelectedProductRep count]==1){
            ViewProductRep* productRep=[aSelectedProductRep objectAtIndex:0];
            if (
                ([appDele modelDisplayLevel]==0 && [[productRep product] isKindOfClass:[Site class]])||
                ([appDele modelDisplayLevel]==1 && [[productRep product] isKindOfClass:[Floor class]])||                                
                ([appDele modelDisplayLevel]==2 && [[productRep product] isKindOfClass:[Space class]])                
                ){
                [productRep setSelected:false];
                [productRep.viewProductLabel display];
                [productRep setNeedsDisplay];
                [self.aSelectedProductRep removeObjectAtIndex:0];                
            }            
        }
        //Check is user tapping on an already selected product
        for (ViewProductRep* productRep in aSelectedProductRep){
            
            if ([productRep product]==[_productRep product]){
                [productRep setSelected:false];
                [productRep.viewProductLabel display];
                [productRep setNeedsDisplay];
                lastDeselectdViewProductRep=productRep;
                [self.aSelectedProductRep removeObjectAtIndex:pSelectedProductRep];
                [self.viewModelToolbar refreshToolbar];
                return;//break;
            }
            pSelectedProductRep++;
        }
        
        [_productRep setSelected:true];        
        [_productRep.viewProductLabel display];
        [_productRep setNeedsDisplay];        
        [self.aSelectedProductRep addObject:_productRep];
        */
    }
    [self.viewModelToolbar refreshToolbar];
    
     
}

-(void) clearSelectedProductRep{    
    if (self.aSelectedProductRep==nil){
        return;
    }
    uint pSelected=0;
    for (ViewProductRep* productRep in aSelectedProductRep){
        [productRep setSelected:false];
        [productRep setNeedsDisplay];
        if ([productRep viewProductLabel]){
            [[productRep viewProductLabel] display];
        }
        pSelected++;
    }
    [aSelectedProductRep removeAllObjects];
    
    
    [self.viewModelToolbar refreshToolbar];
    
//    
//    if (self.selectedProductRep !=Nil){
//
////        [self.selectedProduct setSelected:FALSE];
//
//       ViewProductRep* tmp=self.selectedProductRep;
//        self.selectedProductRep=Nil;        
//        [tmp setNeedsDisplay];
//        [tmp.viewProductLabel display];
////        [[tmp viewProduct] setNeedsDisplay];
//
//    }        
}

-(IBAction) listRelAggregate: (id) sender{    
    if (self.popoverController == nil) {
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//        NSString* title= Nil;//
//        switch ([appDele modelDisplayLevel]){
//            case 0:                
//                title=[NSString stringWithFormat:@"Site: %@",[[self.model root] name] ]; 
//                break;
//            case 1:                
//                title=[NSString stringWithFormat:@"Floor: %@",[[self.model root] name] ];                 
//                break;
//            case 2:
//                title=[NSString stringWithFormat:@"Space: %@",[[self.model root] name] ]; 
//                break;                
//            default:
//                break;
//        }
            
        NavModelVC* navModelVC=[[NavModelVC alloc] init:self];//initWithNibName:@"NavModelVC" bundle:Nil];
        
        

                        
//         initWithNibName:@"MoviesViewController" 
//         bundle:[NSBundle mainBundle]]; 
        
        UIPopoverController *popover = 
        [[UIPopoverController alloc] initWithContentViewController:navModelVC]; 
        
        
        
        
                
        
        
        popover.delegate = self;
        [navModelVC release];        
        self.popoverController = popover;
        [popover release];
    }
    
    [self.popoverController 
     presentPopoverFromBarButtonItem:sender 
     permittedArrowDirections:UIPopoverArrowDirectionAny 
     animated:YES];   
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
//    return self.testUI;
//    NSLog(@"ScrollView ZoomScale %f",scrollView.zoomScale);
//    NSLog(@"ScrollView MaxZoomScale %f", scrollView.maximumZoomScale);
    return self.displayModelUI;
}
//-(UIScrollView*) getScrollView{
//    return self.navUIScrollVC;
//}

-(void) drawSiteMap{
    
    
    Site* site=(Site*) [[self model] root];
    
    
    double scrollViewZoomScale=navUIScrollVC.zoomScale;
    
//    [[[self  opsNavUI] navUIScroll] zoomToRect:CGRectMake(0, 0, 1024*8, 660*8) animated:NO];
    [[[self  opsNavUI] navUIScroll] zoomMin];
//    [[[self  opsNavUI] navUIScroll] zoomToRect:CGRectMake(0, 0, 1024*8, 660*8) animated:NO];
    if (site.hasWorldCoord){
        
        if ([CLLocationManager locationServicesEnabled]){
            [self.displayModelUI drawMap:scrollViewZoomScale];
            
        }else{
            
        }
        
    }
}

- (void) loadViewWithRootSpatialStruct: (SpatialStructure*) rootSpatialStruct{
    
    self.bMultiSelect=false;
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate]; 

    
    [self setCustomNavigationBarButtons]; 
    
    //        CGRect appWindowFrame= [[appDele window] frame];
    CGRect appWindowFrame= CGRectMake(0,0,1024,768) ;                                                     
    CGFloat statusBarHeight=20;
    uint toolbarHeight=44;//85;//44;
//    uint navigationToolBarHeight=44;
    CGRect opsNavUIFrame=CGRectMake(0,0,appWindowFrame.size.width,appWindowFrame.size.height-statusBarHeight-toolbarHeight);
    
    
    
    OPSNavUI* _opsNavUI=[[OPSNavUI alloc] initWithFrame:opsNavUIFrame];
    self.opsNavUI=_opsNavUI;
    [_opsNavUI release];
    //        self.opsNavUI=[[OPSNavUI alloc] initWithFrame:opsNavUIFrame];
    
    
    [self setView:opsNavUI];
    opsNavUI.backgroundColor=[UIColor clearColor];
    
    [opsNavUI setDelegate:self];
    
    
    //        
    //        UIView* test=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 400, 300)];
    //        test.backgroundColor=[UIColor redColor];
    //        [self.opsNavUI addSubview:test];
    
    //        NSLog (@"Step 0 model count: %d" ,[self.model retainCount] );
    OPSModel* _model=[[OPSModel alloc] init];
    self.model=_model;
    [_model release];
    //        self.model=[[OPSModel alloc] init];
    
    
    //        NSLog (@"Step 1 model count: %d" ,[self.model retainCount] );
    [model setupModelFromProjectSite:[appDele activeProjectSite] rootSpatialStruct: rootSpatialStruct];                  
    
    
    self.viewProductTextMethod=0;
    self.pFontSize=3;
//    NSMutableArray* _aFontSize=[[ NSMutableArray alloc] initWithObjects:@"5",@"8",@"10.0",@"15.0",@"30.0",@"40.0",@"60.0",@"80.0",nil];
    NSMutableArray* _aFontSize=[[NSMutableArray alloc] initWithObjects:@"3",@"5",@"8",@"10",@"14",@"20", nil];
//    NSMutableArray* _aFontSize=[[ NSMutableArray alloc] initWithObjects:@"3",@"5",@"8",@"10",@"14","20",nil];
    self.aFontSize=_aFontSize;
    [_aFontSize release];        
    //        self.aFontSize=[[ NSMutableArray alloc] initWithObjects:@"10.0",@"15.0",@"30.0",@"40.0",@"60.0",@"80.0",nil];
    
    
    
    
    
    
    
    
    
    
        
    
    self.viewProductTextMethod=0;
    //        
    //        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];               
    //        
    //        CGRect appWindowFrame= [[appDele window] frame];
    //        CGFloat statusBarHeight=20;
    //    CGRect statusBarFrame= [[UIApplication sharedApplication] statusBarFrame];
    //        CGRect navigationBarFrame=self.navigationController.navigationBar.frame;
//    CGRect navigationToolbarFrame =CGRectMake(0,0, appWindowFrame.size.width,navigationToolBarHeight);// self.navigationController.toolbar.frame;
    
    //    CGRect customToolbarFrame = CGRectOffset(navigationToolbarFrame, 0.0, appWindowFrame.size.height-navigationToolbarFrame.size.height-navigationBarFrame.size.height-statusBarFrame.size.height);    
    
    
    CGRect customToolbarFrame =CGRectMake(0, opsNavUIFrame.size.height-toolbarHeight, appWindowFrame.size.width, toolbarHeight); 

//    NavScrollUIToolbar* _viewModelToolBar=[[NavScrollUIToolbar alloc] initWithFrame:customToolbarFrame viewModelVC:self];
    ViewModelToolbar* _viewModelToolBar=[[ViewModelToolbar alloc] initWithFrame:customToolbarFrame viewModelVC:self];
    self.viewModelToolbar=_viewModelToolBar;
    
    [_viewModelToolBar release];
    //        self.viewModelToolbar=[[NavScrollUIToolbar alloc] initWithFrame:customToolbarFrame viewModelVC:self];
    [self.viewModelToolbar setNeedsDisplay];
    [[self opsNavUI] addSubview:self.viewModelToolbar];
        
    double fitZoom=0.5;//0.375;//0.5;//0.5;// 0.5;//1.0;//0.5;//0.5;//0.25;// //0.5
//    if ([appDele modelDisplayLevel]==1){
//        fitZoom=fitZoom*2;
//    }
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [UIScreen mainScreen].scale > 1)
    {//retina display
        
    }else{
        //non-retina display
        fitZoom=fitZoom/2; //Non retina display can fitzoom with more density;//0.25;// //0.5
    }
    

    if ([appDele modelDisplayLevel]==1 || [appDele modelDisplayLevel]==2){
        fitZoom=fitZoom*2;
    }
    

    [[self model] setFitZoom: fitZoom ];
    if ([appDele modelDisplayLevel]==0){
        [[self model] setMinimumZoom:(([[self model] fitZoom])*0.5)];
        [[self model] setMaximumZoom:(([[self model] fitZoom])*4.0)];//*2.0
    }
    if ([appDele modelDisplayLevel]==1){
        fitZoom=fitZoom*0.75;
        [[self model] setFitZoom:fitZoom];
        [[self model] setMinimumZoom:(([[self model] fitZoom])*1.0 )];
        [[self model] setMaximumZoom:(([[self model] fitZoom])*4.0)];
    }
    if ([appDele modelDisplayLevel]==2){
        
        [[self model] setMinimumZoom:(([[self model] fitZoom])*0.75)];
        [[self model] setMaximumZoom:(([[self model] fitZoom])*4.0)];
    }
    
    //    [[self model] setMinimumZoom:(([[self model] fitZoom])/1.0)];///2.0)];//Resolution Tuned down on 23 June 2012 from 2.0 to 1.0 //2.0)];//1.5 //2.0    //8.0
    //        [[self model] setMaxioomumZoom:(([[self model] fitZoom])*2.0)];
    
    
    
    if ([appDele modelDisplayLevel]==2){
        [[self model] setMaximumZoom:[self model].maximumZoom*0.5];
    }

    
    
    [[self model] setCanvasScaleForScreenFactor:(1/[[self model] minimumZoom])];
    
    double frameWidth=opsNavUI.frame.size.width;    
    double frameHeight=opsNavUI.frame.size.height-toolbarHeight;//(-44*2);
    //        CGRect frameRect=CGRectMake(0,0,frameWidth,frameHeight);
    
    double modelScreenFitWidth=frameWidth/[[self model] fitZoom];
    double modelScreenFitHeight=frameHeight/[[self model] fitZoom];
    CGRect modelScreenFitRect=CGRectMake(0, 0, modelScreenFitWidth, modelScreenFitHeight);
    
    
    
    double canvasWidth=frameWidth*    [[self model] canvasScaleForScreenFactor];
    double canvasHeight=frameHeight*  [[self model] canvasScaleForScreenFactor];
    CGRect canvasRect=CGRectMake(0,0,canvasWidth,canvasHeight ); 
    
    CGRect scrollFrameRect=CGRectMake(0,0,frameWidth,frameHeight);        //(0,0+44,frameWidth,frameHeight);        
    CGRect displayFrameRect=CGRectMake(0,0,canvasWidth,canvasHeight);        
    
    
    CGRect modelRect=[[[[self model] root] bBox] cgRect];    
    CGFloat screenBuffer=0;        
    CGRect modelScreenFitInsetRect=CGRectInset(modelScreenFitRect,screenBuffer,screenBuffer);    
    CGRect screenFitModelRect=[self aspectFittedRect:modelRect max:modelScreenFitInsetRect];
    
    //        double modelScaleFactor=1;//screenFitModelRect.size.width/modelRect.size.width;    
    double modelScaleFactor=screenFitModelRect.size.width/modelRect.size.width;            
    //        double modelScaleFactor=1.0;//screenFitModelRect.size.width/modelRect.size.width;                    
    [[self model] setModelScaleForScreenFactor:modelScaleFactor ];    
    
    double modelScreenWidth=[[self model].root.bBox getWidth]*modelScaleFactor;
    double modelScreenHeight=[[self model].root.bBox getHeight]*modelScaleFactor;
    double modelScreenMinX=[[self model].root.bBox minX]*modelScaleFactor;
    double modelScreenMinY=[[self model].root.bBox minY]*modelScaleFactor;    
    CGAffineTransform modelCenterScreenTransform=CGAffineTransformMakeTranslation( (canvasWidth-modelScreenWidth)/2-modelScreenMinX,(canvasHeight-modelScreenHeight)/2-modelScreenMinY);
    
    
    NavUIScroll* _navUIScroll=[[NavUIScroll alloc] initWithFrame:scrollFrameRect opsNavUI:self.opsNavUI];
    self.navUIScrollVC=_navUIScroll;
    [_navUIScroll release];
    
    //        navUIScrollVC=[[NavUIScroll alloc] initWithFrame:scrollFrameRect opsNavUI:self.opsNavUI];
    //        navUIScrollVC.delegate=self;    
    navUIScrollVC.scrollsToTop=FALSE;
    [navUIScrollVC setScrollEnabled:TRUE];
    navUIScrollVC.exclusiveTouch=NO;
    navUIScrollVC.contentSize=displayFrameRect.size;   
    [navUIScrollVC setClipsToBounds:YES];
    navUIScrollVC.bouncesZoom=true;
    navUIScrollVC.bounces=true;
    navUIScrollVC.pagingEnabled=NO;
    
    navUIScrollVC.minimumZoomScale =[[self model] minimumZoom];
    navUIScrollVC.maximumZoomScale =[[self model] maximumZoom];
    
    //        [navUIScrollVC setNeedsDisplay];
    [opsNavUI addSubview:navUIScrollVC]; 
    //        [navUIScrollVC release];
    
    
    
    
    DisplayModelUI* _displayModelUI=[[DisplayModelUI alloc] initWithFrame:canvasRect rootSpatialStruct:(SpatialStructure*) [self model].root modelCenterScreenTransform:modelCenterScreenTransform navUIScroll:navUIScrollVC];                    
    self.displayModelUI=_displayModelUI;
    [_displayModelUI release];
    
    
    
    
    
    
    
    
    //        self.displayModelUI=[[DisplayModelUI alloc] initWithFrame:canvasRect rootSpatialStruct:(SpatialStructure*) [self model].root modelCenterScreenTransform:modelCenterScreenTransform navUIScroll:navUIScrollVC];                    
    
    //        [self.displayModelUI setNeedsDisplay];
    displayModelUI.userInteractionEnabled=YES;
    [displayModelUI setClipsToBounds:false];          
    
    [navUIScrollVC addSubview:(displayModelUI)]; 
    
    
    
    
    //        UIView* testUI=[[UIView alloc] initWithFrame:CGRectMake(0,0,[model.root.bBox getWidth]*modelScaleFactor,[model.root.bBox getHeight]*modelScaleFactor)];
    //        testUI.backgroundColor=[UIColor greenColor];
    //        testUI.alpha=0.3;
    //        [testUI setTransform:modelCenterScreenTransform];
    //        [displayModelUI addSubview:testUI];
    //        [testUI release];
    
    
    //        [displayModelUI release];
    //        [opsNavUI release];
    [navUIScrollVC zoomFit];   
    
       
    
//    double scrollViewZoomScale=navUIScrollVC.zoomScale;
    
    
    NSString* navTitle=nil;
    switch ([appDele modelDisplayLevel]){
        case 0:{
            Site* site=(Site*) [[self model] root];

//            self.title=[NSString stringWithFormat:@"Site: %@",[site name]]; 
            navTitle=[NSString stringWithFormat:@"Site: (S%d_%d) %@",[appDele activeStudioID],[site ID],[site name]];
            
            
            
            
            
            [self drawSiteMap];
//            
//            [[[self  opsNavUI] navUIScroll] zoomToRect:CGRectMake(0, 0, 1024*8, 660*8) animated:NO];
//            if (site.hasWorldCoord){
//                
//                if ([CLLocationManager locationServicesEnabled]){
//                    [displayModelUI drawMap:scrollViewZoomScale];                    
//                }else{
//                    
//                }
//
//            }

            break;
        }
        case 1:{
            Bldg* bldg= (Bldg*) [[self model] root];
            RelAggregate* relAgg=[bldg relAggregate];    
            Floor* floor=[[relAgg related] objectAtIndex:bldg.selectedFloorIndex];               
//            self.title=[NSString stringWithFormat:@"Bldg: %@ | Floor: %@",[bldg name] , [floor name]]; 
            navTitle=[NSString stringWithFormat:@"Bldg: %d - %@ | Floor: %@",[bldg ID], [bldg name] , [floor name]]; 
            

//            Site* site=[appDele currentSite];
//            if (site.hasWorldCoord){
//                
//                if ([CLLocationManager locationServicesEnabled]){
//                    [displayModelUI drawMap:scrollViewZoomScale]; 
//                    if (self.displayModelUI.siteMapView){
////                        [self.displayModelUI.siteMapView setAlpha:0.0];
//                        [self.displayModelUI.siteMapView setAlpha:1.0];                        
//                    }
//                }else{
//                    
//                }
//                
//            }
            
            break;
        }
        case 2:{
            
            Space* space=(Space*) [[self model] root];            
//            self.title=[NSString stringWithFormat:@"Space: %@",[space name]]; 
            
            if ([space spaceNumber]!=nil && ![[space spaceNumber] isEqualToString:@""]){
                navTitle=[NSString stringWithFormat:@"Space: %@ - %@ (%@)",[space spaceNumber],[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]];             
                
            }else{
                navTitle=[NSString stringWithFormat:@"Space: %@ (%@)",[space name], [ProjectViewAppDelegate doubleToAreaStr:[space spaceArea]]];             
                
            }
            break;
        }
        default:{
            break;
        }                    
            

            
    
    }
    
    int height = 44;//self.frame.size.height;
    int width = 750;
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor blackColor];
    navLabel.font = [UIFont boldSystemFontOfSize:16];
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text=navTitle;
    self.navigationItem.titleView = navLabel;
    [navLabel release]; 
        
    
    bool preV6=false;
    NSComparisonResult order = [[UIDevice currentDevice].systemVersion compare: @"6.0" options: NSNumericSearch];
    if (order == NSOrderedSame || order == NSOrderedDescending) {
        // OS version >= 3.1.3
        preV6=false;
    } else {
        preV6=true;
        // OS version < 3.1.3
    }
    
    if (preV6){
        if ([appDele modelDisplayLevel]==0 && [[appDele currentSite] hasWorldCoord]){        
            
            uint mapLogoHeight=29;
            uint mapLogoWidth=62;
            uint mapLogoBuffer=25;
        //    UIImage* image=[UIImage imageNamed:@"poewredby.png"];
            UIImage* image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"poweredby.png" ofType:nil]];//[UIImage imageNamed:@"poweredby.png"];    
            
            UIImageView* imageView=[[UIImageView alloc] initWithImage:image];

            imageView.frame=CGRectMake(mapLogoBuffer, frameHeight-mapLogoHeight-mapLogoBuffer, mapLogoWidth, mapLogoHeight);
        //    imageView.frame=CGRectMake(mapLogoBuffer, frameHeight/2, mapLogoWidth, mapLogoHeight);    
            self.mapLogo=imageView;
    //        [self.view addSubview:imageView];
            [imageView release];
            [self.view addSubview:self.mapLogo];
            
        }
        
    
    }
    
    //    CGPoint pt=[mapView convertCoordinate:userLocation.coordinate toPointToView:self];  
    //    
    //    
    //    double scrollViewzoomScale=[[[self parentViewModelVC] opsNavUI] navUIScroll].zoomScale;;
    //    double pinSize=50/scrollViewzoomScale;
    //    //    [userLocationImageView setFrame:CGRectMake(pt.x, pt.y, pinSize*1.25, pinSize)];
    //    [imageView setFrame:CGRectMake(pt.x-pinSize/2, pt.y-pinSize/2, pinSize, pinSize)];     
    //    [self.siteMapView addSubview:imageView];
    //    [imageView release];    

}

- (ViewModelVC*) initWithRootSpatialStruct: (SpatialStructure*) rootSpatialStruct
{
    self = [super init];

    if (self) 
    {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
        [self setBrowseRightAfterLoad:false];
        [self loadViewWithRootSpatialStruct:rootSpatialStruct];        
    }
    return self;
}
- (float) currentProductLabelSize{
    return [[[self aFontSize] objectAtIndex:[self pFontSize]] floatValue];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
}

- (CGRect) aspectFittedRect:(CGRect)inRect max:(CGRect)maxRect
{
	float originalAspectRatio = inRect.size.width / inRect.size.height;
	float maxAspectRatio = maxRect.size.width / maxRect.size.height;
    
	CGRect newRect = maxRect;
	if (originalAspectRatio > maxAspectRatio) { // scale by width
		newRect.size.height = maxRect.size.width * inRect.size.height / inRect.size.width;
		newRect.origin.y += (maxRect.size.height - newRect.size.height)/2.0;
	} else {
		newRect.size.width = maxRect.size.height  * inRect.size.width / inRect.size.height;
		newRect.origin.x += (maxRect.size.width - newRect.size.width)/2.0;
	}
    return (newRect);
    
//	return CGRectIntegral(newRect);
}

-(void) switchToolbarToNavScrollUIToolbar{ 
    /*
    float animationTime=0.25;
    
    
    if ([self.viewModelToolbar isKindOfClass:[NavScrollUIToolbar class]]) {
        //        [self.viewModelToolbar removeFromSuperview];
        //        [self.viewModelToolbar release]; 
        //        CGRect customToolbarFrame =CGRectMake(0, self.opsNavUI.frame.size.height-navigationToolbarFrame.size.height, navigationToolbarFrame.size.width, navigationToolbarFrame.size.height);
        //        
        //        self.viewModelToolbar=[[NavScrollUIToolbar alloc] initWithFrame:customToolbarFrame];        
        //        [[self opsNavUI] addSubview:self.viewModelToolbar];        
        return;
    }
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];   
    
    
//    CGRect appWindowFrame= [[appDele window] frame];
//    CGFloat statusBarHeight=20;
    //    CGRect statusBarFrame= [[UIApplication sharedApplication] statusBarFrame];
//    CGRect navigationBarFrame=self.navigationController.navigationBar.frame;
    CGRect navigationToolbarFrame = self.navigationController.toolbar.frame;
    


    
    
    [UIView animateWithDuration:animationTime
                     animations:^{    self.viewModelToolbar.frame= CGRectOffset (self.viewModelToolbar.frame,-navigationToolbarFrame.size.width,0);}
                     completion:^(BOOL finished){ }];//[self.viewModelToolbar removeFromSuperview]; }];
    

    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector: @selector(toolbarAnimationDidStop:finished:context:)];
//    [UIView setAnimationDuration:5];
//    //    [UIView setAnimationBeginsFromCurrentState:YES];
//    
//    self.viewModelToolbar.frame= CGRectOffset (self.viewModelToolbar.frame,-navigationToolbarFrame.size.width,0);
//    
//    [self.viewModelToolbar removeFromSuperview];
//    [self.viewModelToolbar release];
//    
//    //CGRectMake(self.viewModelToolbar.frame.origin.x+1024, self.viewModelToolbar.frame.origin.y,self.viewModelToolbar.frame.size.width,self.viewModelToolbar.frame.size.height);
//    //    [volumeSlider setAlpha: alpha];
//    [UIView commitAnimations];

    
    
    
    

    
        
    CGRect customToolbarFrame =CGRectMake(navigationToolbarFrame.size.width, self.opsNavUI.frame.size.height-navigationToolbarFrame.size.height, navigationToolbarFrame.size.width, navigationToolbarFrame.size.height);
    //CGRectOffset(navigationToolbarFrame, 0.0, -navigationToolbarFrame.size.height-statusBarFrame.size.height);     
    NavScrollUIToolbar* newToolbar=[[NavScrollUIToolbar alloc] initWithFrame:customToolbarFrame viewModelVC:self];        
    [[self opsNavUI] addSubview:newToolbar];
    
//    self.viewModelToolbar=[[NavScrollUIToolbar alloc] initWithFrame:customToolbarFrame];        
//    [[self opsNavUI] addSubview:self.viewModelToolbar];
//    [UIView commitAnimations];
    
    
//    [UIView animateWithDuration:animationTime
//                     animations:^{    self.viewModelToolbar.frame= CGRectOffset (self.viewModelToolbar.frame,-navigationToolbarFrame.size.width,0);}
//                     completion:^(BOOL finished){ }];
    
    
    [UIView animateWithDuration:animationTime
                     animations:^{    newToolbar.frame= CGRectOffset (newToolbar.frame,-navigationToolbarFrame.size.width,0);}
                     completion:^(BOOL finished){ 
                         [self.viewModelToolbar removeFromSuperview]; 
//                         [viewModelToolbar release]; 
                         self.viewModelToolbar=newToolbar;
                         [newToolbar release];
                     }];//[self.viewModelToolbar removeFromSuperview]; }];
    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector: @selector(toolbarAnimationDidStop:finished:context:)];
//    [UIView setAnimationDuration:animationTime];
////    [UIView setAnimationBeginsFromCurrentState:YES];
//
//    self.viewModelToolbar.frame= CGRectOffset (self.viewModelToolbar.frame,-navigationToolbarFrame.size.width,0);
//    //CGRectMake(self.viewModelToolbar.frame.origin.x+1024, self.viewModelToolbar.frame.origin.y,self.viewModelToolbar.frame.size.width,self.viewModelToolbar.frame.size.height);
////    [volumeSlider setAlpha: alpha];
//    [UIView commitAnimations];

    
    //         
//    [[self opsNavUI] addSubview:self.viewModelToolbar];
    
    */
}

-(void) switchToolbarToProductViewToolbar{
    /*
    float animationTime=0.25;
    
    //    if ([self.viewModelToolbar isKindOfClass:[ProductViewToolbar class]]) {
    //        [(ProductViewToolbar*) self.viewModelToolbar refreshToolbar];        
    //        return;}
    
    if ([self.viewModelToolbar isKindOfClass:[ProductViewToolbar class]]) {
        [(ProductViewToolbar*) self.viewModelToolbar refreshToolbar];        
        return;}
    CGRect navigationToolbarFrame = self.navigationController.toolbar.frame;
    
    

    [UIView animateWithDuration:animationTime
                     animations:^{    self.viewModelToolbar.frame= CGRectOffset (self.viewModelToolbar.frame,+navigationToolbarFrame.size.width,0);}
                     completion:^(BOOL finished){ }];//[self.viewModelToolbar removeFromSuperview]; }];
    
    
    
    CGRect customToolbarFrame =CGRectMake(-navigationToolbarFrame.size.width, self.opsNavUI.frame.size.height-navigationToolbarFrame.size.height, navigationToolbarFrame.size.width, navigationToolbarFrame.size.height);

    ProductViewToolbar* newToolbar=[[ProductViewToolbar alloc] initWithFrame:customToolbarFrame viewModelVC:self]; 


    [[self opsNavUI] addSubview:newToolbar];
        [UIView animateWithDuration:animationTime
                     animations:^{    newToolbar.frame= CGRectOffset (newToolbar.frame,+navigationToolbarFrame.size.width,0);}
                     completion:^(BOOL finished){ 
                         [self.viewModelToolbar removeFromSuperview]; 
//                         [viewModelToolbar release]; 
                         self.viewModelToolbar=newToolbar;
                         [newToolbar release];
                     }];
    
     */
}


//UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
//                                           initWithTarget:self 
//                                           action:@selector(handleLongPress:)];
//longPress.minimumPressDuration = 2.0;
//[self addGestureRecognizer:longPress];
//[longPress release];


-(OPSProduct*) selectedProductForReference{

    if (aSelectedProductRep==nil||([aSelectedProductRep count]<=0)){
        return nil;
    }
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];  

    OPSProduct* selectedRefProduct=[((ViewProductRep*)[aSelectedProductRep lastObject]) product];//[selectedProductRep product];

    switch ([appDele modelDisplayLevel]) {
        case 0:{
            if (selectedRefProduct==nil){
                Site* selectedSite=(Site*) [model root];
                selectedRefProduct=selectedSite;
            }         
            if ([selectedRefProduct isKindOfClass:[Floor class]]){
                Floor* selectedFloor=(Floor*) selectedRefProduct;
                Bldg* selectedBldg=(Bldg*) selectedFloor.linkedRel.relating;
                selectedRefProduct=selectedBldg; 
            }
            break;
        }
        case 1:{
            if (selectedRefProduct==nil){
                Bldg* selectedBldg=(Bldg*) [model root];
                Floor* selectedFloor=(Floor*) [[[selectedBldg relAggregate] related] objectAtIndex:selectedBldg.selectedFloorIndex];  
                selectedRefProduct=selectedFloor;
            }   
            
//            if ([selectedRefProduct isKindOfClass:[Slab class]]){
//                Slab* selectedSlab=(Slab*) selectedRefProduct;
//                Floor* selectedFloor=(Floor*) selectedSlab.linkedRel.relating;
//                selectedRefProduct=selectedFloor; 
//            }
            
        }
        case 2:{
            if (selectedRefProduct==nil){
                Space* selectedSpace=(Space*) [model root];
                selectedRefProduct=selectedSpace;
            }         
            break;
            
        }
            
        default:
            break;

    }
    return selectedRefProduct;
}

-(void) zoomFitSelectedProductWithInvoker:(id)invoker completeAction:(SEL) completeAction actionParam:(id) actionParam{
//-(void) zoomFitSelectedProductWithInvoker:<#(id)#> completeAction:<#(SEL)#> actionParam:<#(id)#>{
    if (!self.aSelectedProductRep || [self.aSelectedProductRep count]<=0) {return;}
    ViewProductRep* productRep=[self.aSelectedProductRep lastObject];
    CGRect productRepRect=[productRep bounds];
    CGRect convertedRect=[productRep convertRect:productRepRect toView:self.displayModelUI ];
//    [self.navUIScrollVC zoomToRect:convertedRect animated:YES];
    [self.navUIScrollVC zoomToRectWithDuration:convertedRect duration:0.5 invoker:invoker completeAction:completeAction actionParam:actionParam];      

}

-(IBAction) backToProjectButtonRespond: (id) sender{  
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    [appDele displayProjectViewFromViewModelVC];
}



-(void) addHelpViewController: (id) sender{    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if ([appDele isLiveDataSourceAvailable]){
        NSString* urlAddress=nil;
        if ([appDele modelDisplayLevel]==0){
            urlAddress=@"https://www.onuma.com/products/OnumaHelp.php#ViewSiteLocally";
        }else if ([appDele modelDisplayLevel]==1){
            urlAddress=@"https://www.onuma.com/products/OnumaHelp.php#FloorPlan";
        }else if ([appDele modelDisplayLevel]==2){
            urlAddress=@"https://www.onuma.com/products/OnumaHelp.php#ViewEquipment";
        }
        uint width= self.view.bounds.size.width;
        uint height=self.view.bounds.size.height+44+20; //748
        AttachmentWebViewController* webViewController=[[AttachmentWebViewController alloc] initWithFrame:CGRectMake(0, 0,width,height) initURL:urlAddress hasNavControlBar:false];
        
        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
            [self.navigationController pushViewController:webViewController animated:NO];
            [webViewController release];
        } completion:^(BOOL finished) {
            
        }
         ];
        
        
    }else{
        
        NSString* urlAddress=nil;
        
        
        urlAddress=[[NSBundle mainBundle]pathForResource:@"ONUMA - Help" ofType:@"html" inDirectory:nil];
        UIWebView *web=[[UIWebView alloc]initWithFrame:self.view.frame];
        NSURL* url=[NSURL fileURLWithPath:urlAddress];
        
        //        NSString *absoluteURLwithQueryString = [urlAddress stringByAppendingString: @"#ViewLocalStudios"];
        //        NSURL *finalURL = [NSURL URLWithString: absoluteURLwithQueryString];
        
        
        [web loadRequest:[NSURLRequest requestWithURL:  url   ]];
        
        UIViewController* webViewController=[[UIViewController alloc] init];
        [webViewController setView:web];
        [web release];web=nil;
        
        
        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
            [self.navigationController pushViewController:webViewController animated:NO];
            [webViewController release];
        } completion:^(BOOL finished) {
            
        }
         ];
        
        

        
    }
    
}

- (void) setCustomNavigationBarButtons{    
    
    
//    ViewModelNavCntr* navCntr=(ViewModelNavCntr*)[self navigationController];
//    if ([navCntr isEnteringGhostSpace]){
//        return;
//    }
//    
//    
//    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    if ([appDele modelDisplayLevel]==0){  
        
        NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:3];
        
//        UIBarButtonItem *bi  = [[UIBarButtonItem alloc] initWithTitle:@"BldgList" style:UIBarButtonItemStylePlain target:self action:@selector(relatedAggList:)];
//        bi.style = UIBarButtonItemStyleBordered;                
////        self.navigationItem.rightBarButtonItem = bi;        
//        [buttons addObject:bi];
//        [bi release];
//        bi  = [[UIBarButtonItem alloc] initWithTitle:@"MapType" style:UIBarButtonItemStylePlain target:self action:@selector(mapTypeList:)];
//        bi.style = UIBarButtonItemStyleBordered;       
//        
//        //        self.navigationItem.rightBarButtonItem = bi;
//        [buttons addObject:bi];
//        [bi release];
 
        
        UIImage *buttonImage=nil;
        UIImage *buttonImageHighlight=nil;
        UIButton *button=nil;
        UIBarButtonItem *bi=nil;
        
        
        buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2.png" ofType:nil]];
        buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2Highlight.png" ofType:nil]];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(addHelpViewController:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];
        [buttons addObject:bi];
        //        [button release];
        [bi release];
        
        	
        buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Buildings.png" ofType:nil]];//:@"Buildings.png"];// [UIImage imageNamed:@"Buildings.png"];   
        buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"BuildingsHighlight.png" ofType:nil]];// [UIImage imageWithContentsOfFile:@"BuildingsHighlight.png"];//[UIImage imageNamed:@"BuildingsHighlight.png"];                        
//        buttonImage = [UIImage imageNamed:@"Buildings.png"];   
//        buttonImageHighlight = [UIImage imageNamed:@"BuildingsHighlight.png"];        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(relatedAggList:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];           
        [buttons addObject:bi];        
//        [button release];
        [bi release];  
        
        
        buttonImage =  [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Map Type.png" ofType:nil]];//[UIImage imageNamed:@"Map Type.png"];   
        buttonImageHighlight =  [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Map TypeHighlight.png" ofType:nil]];//[UIImage imageNamed:@"Map TypeHighlight.png"];        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(mapTypeList:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
//        [button release];
        [buttons addObject:bi];
        [bi release];  
        
        

        
        self.navigationItem.rightBarButtonItems=buttons;
        [buttons release];
        
        

/*
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonImage = [UIImage imageNamed:@"Project.png"];   
        buttonImageHighlight = [UIImage imageNamed:@"ProjectHighlight.png"];        

        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
         button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height); 
        [button addTarget:self action:@selector(backToProjectButtonRespond:) forControlEvents:UIControlEventTouchUpInside];
 
         bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
         //        [button release];
         [leftButtons addObject:bi];
         
         [bi release];  
 */
        
//        button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//        button.titleLabel.text=@"Studio";        
////        button.titleLabel.textColor=[UIColor blackColor];
////        button.titleLabel.font=[UIFont systemFontOfSize:10];
//        button.frame = CGRectMake(0.0, 0.0, 150, 40);
//        [button addTarget:self action:@selector(backToProjectButtonRespond:) forControlEvents:UIControlEventTouchUpInside];

   
//Code Below add Custom Button as back , don't remove yet*************************************************
        
//        NSMutableArray* leftButtons = [[NSMutableArray alloc] initWithCapacity:1];
//        bi = [[UIBarButtonItem alloc] initWithTitle:@"Studio List" style:UIBarButtonItemStylePlain target:self action:@selector(backToProjectButtonRespond:)];           
//        [leftButtons addObject:bi];        
//        [bi release];          
//        self.navigationItem.leftBarButtonItems = leftButtons;
//        [leftButtons release];        
//                               
//        if ([appDele modelDisplayLevel]==0){
            NSMutableArray* leftButtons = [[NSMutableArray alloc] initWithCapacity:1];
            
            buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ProjectListsButton.png" ofType:nil]];//[UIImage imageNamed:@"ProjectListsButton.png"];   
        buttonImageHighlight = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ProjectListsButton.png" ofType:nil]];;//[UIImage imageNamed:@"ProjectListsButton.png"];        
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setImage:buttonImage forState:UIControlStateNormal];
            [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
            button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(backToProjectButtonRespond:) forControlEvents:UIControlEventTouchUpInside];
            bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
            //        [button release];
            [leftButtons addObject:bi];
            [bi release];  
            
            self.navigationItem.leftBarButtonItems = leftButtons;
            [leftButtons release];
//        }
//---------------------------------------------------------------------------------------------------------        
        
//        self.navigationController.navigationBar.backItem.title=@"Studio";  
        
        //        self.navigationItem.leftBarButtonItem = backToProjectButton;
        //        [backToProjectButton release];
        
//        
//        UIBarButtonItem *backToProjectButton  = [[UIBarButtonItem alloc] initWithTitle:@"Project" style:UIBarButtonItemStylePlain target:self action:@selector(backToProjectButtonRespond:)];
//        backToProjectButton.style = UIBarButtonItemStyleBordered;                
//        self.navigationItem.leftBarButtonItem = backToProjectButton;
//        [backToProjectButton release];
//        
        
    }
    if ([appDele modelDisplayLevel]==1){  
//        CGRect navBarFrame=CGRectMake(0, 0, 200, 44.01);
//        UIToolbar* tools = [[UIToolbar alloc] initWithFrame:navBarFrame];             
//        tools.barStyle=-1;        
//        tools.tintColor = [UIColor colorWithWhite:0.575f alpha:0.0f]; // closest I could get by eye to black, translucent style.                         
        //        tools.tintColor =[UIColor colorWithRed:(108.0/256.0) green:(115.0/256.0) blue:(126.0/256.0) alpha:1];
        //        self.navigationController.navigationBar.tintColor;
        //            //        tools.tintColor = self.navigationController.navigationBar.tintColor;//[UIColor                 
        
        // create the array to hold the buttons, which then gets added to the toolbar
        
        

        
        
        
        NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:3];
        
//        UIBarButtonItem * b0 = [[UIBarButtonItem alloc]
//                                initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//        [buttons addObject:b0];
//        [b0 release];
//        
//        UIBarButtonItem *b1 = [[UIBarButtonItem alloc] initWithTitle:@"FloorList" style:UIBarButtonItemStylePlain target:self action:@selector(floorList:)];            
//        b1.style = UIBarButtonItemStyleBordered;
//        [buttons addObject:b1];
//        [b1 release];
//    
        
        
//        UIBarButtonItem *b2 = [[UIBarButtonItem alloc] initWithTitle:@"SpaceList" style:UIBarButtonItemStylePlain target:self action:@selector(relatedAggList:)];            
//        b2.style = UIBarButtonItemStyleBordered;                        
//        [buttons addObject:b2];
//        [b2 release];            
//        
        
        
        
        
        
        
//        b2.style = UIBarButtonItemStyleBordered;
        
        UIImage *buttonImage=nil;
        UIImage *buttonImageHighlight=nil;
        UIButton *button=nil;
        UIBarButtonItem *bi=nil;
        
        buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2.png" ofType:nil]];
        buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2Highlight.png" ofType:nil]];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(addHelpViewController:) forControlEvents:UIControlEventTouchUpInside];
        button.hidden=true;
        
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];
        [buttons addObject:bi];
        //        [button release];
        [bi release];
        
        
        
        
        UIImage *buttonImageSpaceList = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SpaceList.png" ofType:nil]];//[UIImage imageNamed:@"SpaceList.png"];   
        UIImage *buttonImageSpaceListHighlight = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SpaceListHighlight.png" ofType:nil]];//[UIImage imageNamed:@"SpaceListHighlight.png"];        
        UIButton *buttonSpaceList = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonSpaceList setImage:buttonImageSpaceList forState:UIControlStateNormal];
        [buttonSpaceList setImage:buttonImageSpaceListHighlight forState:UIControlStateHighlighted];
        buttonSpaceList.frame = CGRectMake(0.0, 0.0, buttonImageSpaceList.size.width, buttonImageSpaceList.size.height);
        [buttonSpaceList addTarget:self action:@selector(relatedAggList:) forControlEvents:UIControlEventTouchUpInside];
        
        buttonSpaceList.hidden=true;
        UIBarButtonItem *b2 = [[UIBarButtonItem alloc] initWithCustomView:buttonSpaceList];
        
        
        [buttons addObject:b2];
        [b2 release];            
        
                                
        
        
        
      
        
        UIImage *buttonImageFloorList = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"FloorList.png" ofType:nil]];//[UIImage imageNamed:@"FloorList.png"];  
        UIImage *buttonImageFloorListHighlight = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"FloorListHighlight.png" ofType:nil]];//[UIImage imageNamed:@"FloorListHighlight.png"];        
        UIButton *buttonFloorList = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonFloorList setImage:buttonImageFloorList forState:UIControlStateNormal];
        [buttonFloorList setImage:buttonImageFloorListHighlight forState:UIControlStateHighlighted];
        buttonFloorList.frame = CGRectMake(0.0, 0.0, buttonImageFloorList.size.width, buttonImageFloorList.size.height);
        [buttonFloorList addTarget:self action:@selector(floorList:) forControlEvents:UIControlEventTouchUpInside];                
        buttonFloorList.hidden=true;
        
        UIBarButtonItem *b1 = [[UIBarButtonItem alloc] initWithCustomView:buttonFloorList];  
        
        [buttons addObject:b1];
        [b1 release];
        
//        [tools setItems:buttons animated:NO];
//        UIBarButtonItem* uiBarButtonItem=[[UIBarButtonItem alloc] initWithCustomView:tools];
        self.navigationItem.rightBarButtonItems = buttons;// uiBarButtonItem;//[[UIBarButtonItem alloc] initWithCustomView:tools];
//        [uiBarButtonItem release];
        [buttons release];
//        [tools release];
        
        
        
//        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] 
//                                       initWithTitle: @"Site Plan" 
//                                       style: UIBarButtonItemStyleBordered
//                                       target: nil action: nil];
//        self.navigationItem.leftBarButtonItem=backButton;
////        [self.navigationItem setBackBarButtonItem: backButton];
//        [backButton release];

        
        
//Code below is for adding icon to the back button toolbar, don't remove for now******************************
//        UIImage *buttonImage=nil;
//        UIImage *buttonImageHighlight=nil;
//        UIButton *button=nil;
//        UIBarButtonItem *bi=nil;        
//        NSMutableArray* leftButtons = [[NSMutableArray alloc] initWithCapacity:1];
//        
//        buttonImage = [UIImage imageNamed:@"Site_Return.png"];   
//        buttonImageHighlight = [UIImage imageNamed:@"Site_ReturnHighlight.png"];        
//        button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [button setImage:buttonImage forState:UIControlStateNormal];
//        [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
//        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//        [button addTarget:self action:@selector(backOnLevel) forControlEvents:UIControlEventTouchUpInside];
//        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
//        //        [button release];
//        [leftButtons addObject:bi];
//        [bi release];  
//        
//        self.navigationItem.leftBarButtonItems = leftButtons;
//        [leftButtons release];
//-----------------------------------------------------------------------------------------------------------
    }
    if ([appDele modelDisplayLevel]==2){  
//        UIBarButtonItem *bi  = [[UIBarButtonItem alloc] initWithTitle:@"EquipList" style:UIBarButtonItemStylePlain target:self action:@selector(relatedAggList:)];
//        bi.style = UIBarButtonItemStyleBordered;                
//        self.navigationItem.rightBarButtonItem = bi;
//        [bi release];        
        NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:1];
        
        
        
        UIImage *buttonImage=nil;
        UIImage *buttonImageHighlight=nil;
        UIButton *button=nil;
        UIBarButtonItem *bi=nil;
        
        buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2.png" ofType:nil]];
        buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2Highlight.png" ofType:nil]];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(addHelpViewController:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];
        [buttons addObject:bi];
        //        [button release];
        [bi release];
        
        
        
        UIImage *buttonImageFloorList = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Furniture List.png" ofType:nil]];//[UIImage imageNamed:@"Furniture List.png"];  
        UIImage *buttonImageFloorListHighlight = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Furniture ListHighlight.png" ofType:nil]];//[UIImage imageNamed:@"Furniture ListHighlight.png"];        
        UIButton *buttonFloorList = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonFloorList setImage:buttonImageFloorList forState:UIControlStateNormal];
        [buttonFloorList setImage:buttonImageFloorListHighlight forState:UIControlStateHighlighted];
        buttonFloorList.frame = CGRectMake(0.0, 0.0, buttonImageFloorList.size.width, buttonImageFloorList.size.height);
        [buttonFloorList addTarget:self action:@selector(relatedAggList:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        buttonFloorList.hidden=true;
        
        UIBarButtonItem *b1 = [[UIBarButtonItem alloc] initWithCustomView:buttonFloorList];
        
        [buttons addObject:b1];
        [b1 release];
        
        
        
        self.navigationItem.rightBarButtonItems=buttons;
        [buttons release];
        
        
        
        
//        Code Below is for adding image icon to back button, don't remove yet *********************************
//        UIImage *buttonImage=nil;
//        UIImage *buttonImageHighlight=nil;
//        UIButton *button=nil;
//        UIBarButtonItem *bi=nil;
//        
//        NSMutableArray* leftButtons = [[NSMutableArray alloc] initWithCapacity:1];
//        
//        buttonImage = [UIImage imageNamed:@"Return to Floor.png"];   
//        buttonImageHighlight = [UIImage imageNamed:@"Return to FloorHighlight.png"];        
//        button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [button setImage:buttonImage forState:UIControlStateNormal];
//        [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
//        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//        [button addTarget:self action:@selector(backOnLevel) forControlEvents:UIControlEventTouchUpInside];
//        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
//        //        [button release];
//        [leftButtons addObject:bi];
//        [bi release];  
//        
//        self.navigationItem.leftBarButtonItems = leftButtons;
//        [leftButtons release];
//        ------------------------------------------------------------------------------------------------------
    }    
    
    
}

- (UINavigationItem *)navigationItem{
    UINavigationItem *item = [super navigationItem];
    if (item != nil && item.backBarButtonItem == nil)
    {
        item.backBarButtonItem = [[[UIBarButtonItem alloc] init] autorelease];
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [UIApplication sharedApplication].delegate;
        if ([appDele modelDisplayLevel]==0){
            item.backBarButtonItem.title=@"Site Plan";
        }else if ([appDele modelDisplayLevel]==1){
            item.backBarButtonItem.title=@"Floor Plan";
        }
//        else if ([appDele modelDisplayLevel]==2){
//            item.backBarButtonItem.title=@"Space Plan":
//        }
    }
    
    return item;
}


-(void) backOnLevel{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication]delegate];
    [appDele browseAndSelect];
    
}
//-(void) viewDidAppear:(BOOL)animated{
//    [super viewDidAppear:animated];
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    if ([appDele modelDisplayLevel]==2){
//            
//        NSArray* aButtons=self.navigationItem.rightBarButtonItems;
//        for (int pButton=0;pButton<[aButtons count];pButton++){
//            UIBarButtonItem *bi=[aButtons objectAtIndex:pButton];
//            UIButton *button=(UIButton*)bi.customView;
//            button.hidden=false;
//        }
//    }
//    
//    
//}
//-(void) viewDidAppear:(BOOL)animated{
////    NSLog(@"VMC Load");
////    ViewModelNavCntr* navCntr=(ViewModelNavCntr*)[self navigationController];
////    if ([navCntr isEnteringGhostSpace]){
////        [navCntr setIsEnteringGhostSpace:false];
////    }
//    [super viewDidAppear:animated];
////    if ([self browseRightAfterLoad]){
////        self.browseRightAfterLoad=false;
////        
////    }
////    [self performSelector:@selector(myPopHelperMethod:) withObject:nil afterDelay:0.8f]
//}
/*
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [navUIScrollVC zoomFit];     
 

    

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
*/

-(void) viewDidLoad{
    [super viewDidLoad];

}
//-(BOOL) prefersStatusBarHidden{
//    return YES;
//}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}




//// Tell the system what we support
- (NSUInteger)supportedInterfaceOrientations {
    //    return UIInterfaceOrientationMaskAllButUpsideDown;
    return UIInterfaceOrientationMaskLandscapeLeft;//UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskPortraitUpsideDown;//UIInterfaceOrientationMaskAllButUpsideDown;
    //    return UIInterfaceOrientationMaskPortrait;
}
//
// Tell the system It should autorotate
- (BOOL) shouldAutorotate {
    return YES;
    
}
// Tell the system which initial orientation we want to have
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}


@end

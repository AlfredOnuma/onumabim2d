//
//  ViewProductRep.m
//  OPSMobile
//
//  Created by Alfred Man on 7/28/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

//#include "WEPopoverController.h"
//WEPopoverController *navPopover;

//#import "ProductViewButtonRing.h"
#import "ViewProductSprite.h"
#import "ProjectViewAppDelegate.h"
#import "DisplayModelUI.h"
#import "ViewModelVC.h"
#import "OPSModel.h"
#import "Space.h"
#import "RelAggregate.h"
#import "CPoint.h"
#import "BBox.h"
#import "Slab.h"
#import "Site.h"
#import "Space.h"
#import "Floor.h"
#import "Bldg.h"
#import "SpatialStructure.h"
#import "RepresentationItem.h"
#import "ExtrudedAreaSolid.h"

#import "ViewProductLabel.h"
//#import "TestUI4.h"
#import "NavModelVC.h"
#import "NavUIScroll.h"
#import "BitmapRep.h"
#import "furn.h"


//static int p=0;
@implementation ViewProductSprite;
@synthesize viewProductRep=_viewProductRep;
//@synthesize x;
//@synthesize y;
@synthesize r=_r;
@synthesize g=_g;
@synthesize b=_b;
//@synthesize alpha;
//@synthesize angle;
//@synthesize rotation;
@synthesize width=_width;
@synthesize height=_height;
//@synthesize scale;
@synthesize box=_box;
@synthesize render=_render;
@synthesize offScreen=_offScreen;
//@synthesize image;

@synthesize representationItem=_representationItem;
//@synthesize geoProduct;
@synthesize delegate=_delegate;
//@synthesize path=_path;
//@synthesize aPoint=_aPoint;
//@synthesize objNSMutablePath=_objNSMutablePath;
@synthesize bPath=_bPath;
//@synthesize displayMatrix;

//@synthesize refProduct;
@synthesize viewProductLabel=_viewProductLabel;
//@synthesize imageView;
@synthesize transform=_transform;
//@synthesize deleteMePath;
//
//@synthesize displayID;
float oldX, oldY;
BOOL dragging;

float outsetBuffer;//=0.0f;

- (void)dealloc
{
    //    NSLog (@"ViewProductDealloc****************");
    [_viewProductLabel release];
    _viewProductLabel=nil;
    //    [imageView release];
//    CGPathRelease(_path);
//    _path=nil;
    [_bPath release];_bPath=nil;
//    [_objNSMutablePath release];_objNSMutablePath=nil;
    
    //    [path release];
    [super dealloc];
}


- (ViewModelVC*) parentViewModelVC{
    return (ViewModelVC*) [((DisplayModelUI*) self.delegate) parentViewModelVC];
}
- (BOOL)containsPoint:(CGPoint)point //onPath:(UIBezierPath*)path inFillArea:(BOOL)inFill
{
    BOOL    isHit = NO;
    //    isHit = CGPathContainsPoint( self.path,NULL, point, false);
    isHit = CGPathContainsPoint( self.bPath.CGPath,NULL, point, false);
    return isHit;
}

- (void) displayProductInfo:(OPSProduct *)product{

     // Create a label with custom text 
//     UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
//     [label setText:[space name]];
//     [label setBackgroundColor:[UIColor clearColor]];
//     [label setTextColor:[UIColor whiteColor]];
//     [label setTextAlignment:UITextAlignmentCenter];
//     
//     UIFont *font = [UIFont boldSystemFontOfSize:20];
//     [label setFont:font];
//     CGSize size = [label.text sizeWithFont:font];
//     CGRect frame = CGRectMake(0, 0, size.width + 10, size.height + 10); // add a bit of a border around the text
//     label.frame = frame;
//     
//     //  place inside a temporary view controller and add to popover
//     UIViewController *viewCon = [[UIViewController alloc] init];
//     viewCon.view = label;
//     viewCon.contentSizeForViewInPopover = frame.size;       // Set the content size
//     
//     navPopover = [[WEPopoverController alloc] initWithContentViewController:viewCon];
//     [navPopover presentPopoverFromRect:CGRectMake(0, 0, 50, 57)
//     inView:self
//     permittedArrowDirections:UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown
//     animated:YES];    

}


//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    
//    //
//    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];       
//    
//    ViewModelVC* modelVC=[self parentViewModelVC];//[appDele activeViewModelVC];
//    
//    [modelVC selectAProduct:[self refProduct]];
//}
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {   
//}
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
//}
/*
- (void)initExtrudedRepBaseWithGeoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*)newRefProduct  
{
    //        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate* ) [[UIApplication sharedApplication] delegate];
    ViewModelVC* viewModelVC=[self parentViewModelVC];// [appDele activeViewModelVC];
    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
    if (newRefProduct){
        [newRefProduct addARefViewProductRep:self];
    }
    if (newGeoProduct){
        [newGeoProduct addAGeoViewProductRep:self];
    }
    //        [newGeoProduct setViewProduct:self];
    geoProduct=newGeoProduct;        
    refProduct=newRefProduct;
    
    if (self.geoProduct.representation!=NULL){                     
        //            BBox* repBBox=[self.geoProduct.representation bBox];                                                     
        
        
        
        //            CGMutablePathRef _path = CGPathCreateMutable();
        path = CGPathCreateMutable();
        //            path = CGPathCreateMutable();   
        Representation* rep=[geoProduct representation];    
        if (rep==NULL) {return;}
        
        
        
        
        NSMutableArray* aRepItem=[rep aRepresentationItem];
        if (aRepItem==NULL || [aRepItem count]<=0){ return ;}
        BBox* boundBBox=[[BBox alloc] init];
        for (int pRepItem=0;pRepItem<[aRepItem count];pRepItem++){
            RepresentationItem* repItem=[aRepItem objectAtIndex:pRepItem];
            if (repItem==NULL) continue;
            
            ExtrudedAreaSolid* thisExt=(ExtrudedAreaSolid*) repItem;
            NSMutableArray* aCurveProfile=thisExt.aCurveProfile;
            if (aCurveProfile==NULL||[aCurveProfile count]<=0) continue;
            for (int pCurveProfile=0;pCurveProfile<[aCurveProfile count];pCurveProfile++){
                CurveProfile* curveProfile=[aCurveProfile objectAtIndex:pCurveProfile];
                if (curveProfile==NULL) continue;
                NSMutableArray* aPoint=curveProfile.aPoint;
                if (aPoint==NULL||[aPoint count ]<=2) continue;
                
                for (int pPoint=0;pPoint < [aPoint count]; pPoint++){                       
                    CPoint* point=[(CPoint*) [aPoint objectAtIndex:(pPoint)]copy ];
                    point.x*=modelScaleFactor;
                    point.y*=modelScaleFactor;
                    [boundBBox updatePoint:point];
                    
                    //                        NSLog(@"x = %f, y = %f", point.x, point.y);
                    
                    
                    if (pPoint==0){                                   
                        //                            CGPathMoveToPoint(_path,NULL,point.x,point.y);                            
                        CGPathMoveToPoint(path,NULL,outsetBuffer+ point.x,outsetBuffer+ point.y);  
                    }else{                            
                        //                            CGPathAddLineToPoint(_path,NULL,point.x,point.y);
                        CGPathAddLineToPoint(path,NULL,outsetBuffer+ point.x,outsetBuffer+ point.y);
                    }       
                    
                    [point release];
                    
                }     
                
                
                break;
            }
            
            
            
            
        }
//         //---------------------------Temporarily remove-------------------------------------------------------------------------
//        [self setBounds:CGRectOffset(self.bounds,boundBBox.minX,boundBBox.minY)];
//        [boundBBox release];
//        self.opaque = NO;        
//        self.backgroundColor = [UIColor clearColor];
//         //-----------------------------------------------------------------------------------------------------------------

    }
    
    
    
    //
    //    }
    //    return self;
    
}

- (void)initBitmapRepBaseWithGeoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*)newRefProduct 
{        

    //    self = [super initWithFrame:frame];            
    //    if (self) {        
    
    //        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
    ViewModelVC* viewModelVC=[self parentViewModelVC];//[appDele activeViewModelVC];
    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];
    
    
    //        [newGeoProduct setViewProduct:self];
    if (newRefProduct){
        [newRefProduct addARefViewProductRep:self];
    }
    if (newGeoProduct){
        [newGeoProduct addAGeoViewProductRep:self];
    }
    self.geoProduct=newGeoProduct;        
    self.refProduct=newRefProduct;
    
    
    
    
    if (self.geoProduct.representation!=NULL){                             
        
        
        Representation* rep=[self.geoProduct representation];    
        if (rep==NULL) {return ;}                
        
        NSMutableArray* aRepItem=[rep aRepresentationItem];
        if (aRepItem==NULL || [aRepItem count]<=0) {return ;}
        
        //            BBox* boundBBox=[[BBox alloc] init];
        for (int pRepItem=0;pRepItem<[aRepItem count];pRepItem++){
            RepresentationItem* repItem=[aRepItem objectAtIndex:pRepItem];
            if (repItem==NULL) continue;
            
            if (![repItem isKindOfClass:[BitmapRep class]]){  continue;}
            
            BitmapRep* bitmapRep=(BitmapRep*) repItem;
            NSString* imgName=[NSString stringWithFormat:@"%@.png",[bitmapRep name]];
            
            
            BBox* imgViewBBox=[[bitmapRep bBox] copy];
            
            [imgViewBBox multiply:modelScaleFactor];
            
            width=[imgViewBBox getWidth];
            Height=[imgViewBBox getHeight];
            
//            UIImageView* _imageView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:imgName]];
//            self.imageView = _imageView;
//            [_imageView release];
//            
//            
//            //                    [bitmapRep.bBox ];
//            
//            //                self.imageView.frame=CGRectMake(outsetBuffer, outsetBuffer, [imgViewBBox getWidth], [imgViewBBox getHeight]);
//            
//            
//            
//            
//            self.imageView.frame=CGRectMake(0.0f, 0.0f, [imgViewBBox getWidth], [imgViewBBox getHeight]);
        
            
            image=[UIImage imageNamed:imgName];
            
//            [self addSubview:self.imageView];
            
            
            
            path = CGPathCreateMutable();                 
            CGPoint bitmapPathOrigin=CGPointMake (self.imageView.frame.origin.x, self.imageView.frame.origin.y);
            CGFloat bitmapPathWidth=self.imageView.frame.size.width;
            CGFloat bitmapPathHeight=self.imageView.frame.size.height;                
            
            CGPoint p0=CGPointMake(bitmapPathOrigin.x,                  bitmapPathOrigin.y);
            CGPoint p1=CGPointMake(bitmapPathOrigin.x+bitmapPathWidth,  bitmapPathOrigin.y);                   
            CGPoint p2=CGPointMake(bitmapPathOrigin.x+bitmapPathWidth,  bitmapPathOrigin.y+bitmapPathHeight);
            CGPoint p3=CGPointMake(bitmapPathOrigin.x,                  bitmapPathOrigin.y+bitmapPathHeight);
            
            CGPathMoveToPoint   (path,NULL,p0.x,p0.y);                  
            CGPathAddLineToPoint(path,NULL,p1.x,p1.y);                   
            CGPathAddLineToPoint(path,NULL,p2.x,p2.y);                                 
            CGPathAddLineToPoint(path,NULL,p3.x,p3.y);                     
            
            
            
            [imgViewBBox release]; 
            
        }

//        // ---------------------------Temporarily remove-------------------------------------------------------------------------
//        self.opaque = NO;        
//        self.backgroundColor = [UIColor clearColor];
//         ----------------------------------------------------------------------------------------------------
//         //
    }
    
}

*/
//- (id)initWithFrame:(CGRect)frame geoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*)newRefProduct displayModelUI:(DisplayModelUI*) displayModelUI
- (id)initWithTransform:(CGAffineTransform) transform viewProductRep:(ViewProductRep*)viewProductRep representationItem:(RepresentationItem*) representationItem displayModelUI:( DisplayModelUI*) displayModelUI{
//- (id)initWithTransform:(CGAffineTransform) _transform  geoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*) newRefProduct displayModelUI:( DisplayModelUI*) displayModelUI
    self=[super init];
    
//    double outset=outsetBuffer;
    
//    if ([represenationItem isKindOfClass:[BitmapRep class]]){
////    if ([newGeoProduct isKindOfClass:[Furn class]]){
//        outset=0.0f;
//    }
    
//    CGRect frameWithBuffer=CGRectInset(frame, -outset, -outset);
//    self=[super initWithFrame:frameWithBuffer];

    if (self){
        self.viewProductRep=viewProductRep;
        self.transform=transform;
        self.representationItem=representationItem;        
        self.delegate=displayModelUI;
//        if ([newGeoProduct isKindOfClass:[Furn class]]){
//            [self initBitmapRepBaseWithGeoProduct:newGeoProduct refProduct:newRefProduct ];
//            //        return [self initBitmapRepBase:frame geoProduct:newGeoProduct refProduct:newRefProduct ]; 
//        }else{        
//            [self initExtrudedRepBaseWithGeoProduct:newGeoProduct refProduct:newRefProduct ]; 
//            //            return [self initExtrudedRepBase:frame geoProduct:newGeoProduct refProduct:newRefProduct ]; 
//        }
    }

    return self;
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//-(void) drawRect:(CGRect)rect{
//    UIView* t=[[UIView alloc] initWithFrame:CGRectMake(0,0,400,400)];
//    t.backgroundColor=[UIColor redColor];
//    [self addSubview:t];
//}

    
- (void) drawBody: (CGContextRef) context
{
/*    
if ( [self.geoProduct isKindOfClass:[Furn class]]){  
    CGImageRef imageRef = image.CGImage;
    CGContextDrawImage(context,CGRectMake(0, 0, width, Height), imageRef);
    return;
}
    
    ViewModelVC* modelVC=[self parentViewModelVC];//[appDele activeViewModelVC]; 
    OPSProduct* selectedProduct=modelVC.selectedProduct;
    bool selected=(selectedProduct==[self refProduct]);
    double zoomScale=[modelVC navUIScrollVC].zoomScale;
    
    if ([self refProduct]==nil || [[self geoProduct] isKindOfClass:[Furn class]]){
        return;
    }
    
    
    
    
    float alpha=(selected)?1.0:0.3;
    float lineThk=(selected)?6.0/zoomScale:1.0/zoomScale;
    
    
    CGContextBeginPath(context);
    CGContextAddPath(context,path);            
    CGContextClosePath(context);

    if ([self.geoProduct isKindOfClass:[Slab class]]){        
        CGContextSetRGBFillColor(context, 0.48, 0.67, 0.75, alpha);             
        
    }else if ( [self.geoProduct isKindOfClass:[Site class]]){  
        CGContextSetRGBFillColor(context, 0.59, 0.8, 0.8, alpha);        
    }else if ( [self.geoProduct isKindOfClass:[Furn class]]){                
        CGContextSetRGBFillColor(context, 1.0,0.0,0.0,0.7);   
    }else{                
        CGContextSetRGBFillColor(context, 234.0/256.0, 234.0/256.0, 234.0/256.0,alpha);   
    }
 
    
    
    CGContextFillPath(context);    
    CGContextBeginPath(context);
    CGContextAddPath(context,path); 
    
    
    
    CGContextClosePath(context);
    CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
    //    CGContextSetRGBFillColor(context, 1.0, 0.0, 0.0, 0.6);       
    CGContextSetLineWidth(context, lineThk);
    
    
    //    CGContextFillPath(context);
    CGContextStrokePath(context);     
    //    CGContextEndTransparencyLayer (context);// 6    
    */
}

- (void) draw: (CGContextRef) context
{
    
    CGContextSaveGState(context);
    
//    CGAffineTransform t=CGAffineTransformIdentity;
//    t=CGAffineTransformTranslate(t, x, y);
//    t=CGAffineTransformRotate(t, rotation);
//    t=CGAffineTransformScale(t, scale, scale);
//        
//    CGContextConcatCTM(context, t);
    
    CGContextConcatCTM(context, self.transform);
    [self drawBody:context];    
    CGContextRestoreGState(context);
    
}




@end

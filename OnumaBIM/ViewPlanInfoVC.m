//
//  ViewPlanInfoVC.m
//  ProjectView
//
//  Created by Alfred Man on 5/15/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewPlanInfoVC.h"
#import "ProjectViewAppDelegate.h"
#import "Site.h"
#import "Bldg.h"
#import "OPSModel.h"
#import "ViewModelVC.h"
@interface ViewPlanInfoVC ()

@end

@implementation ViewPlanInfoVC
@synthesize vc=_vc;
@synthesize viewModelVC=_viewModelVC;

@synthesize rootView=_rootView;
@synthesize infoView=_infoView;
@synthesize infoScrollView=_infoScrollView;    
@synthesize liveReportButton=_liveReportButton;

-(void) viewDidLoad{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}
//-(BOOL) prefersStatusBarHidden{
//    return YES;
//}
-(void) dealloc{
    [_rootView release];_rootView=nil;
    [_infoView release];_infoView=nil;
    [_infoScrollView release];_infoScrollView=nil;    
    [_liveReportButton release];_liveReportButton=nil;
    [super dealloc];
}


-(UIView*) viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return [self infoView];
}



-(IBAction)displayWebView:(id)sender{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=[appDele activeProjectSite];
    
    if (![appDele isLiveDataSourceAvailable]){
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"For access to the live reports on the Onuma Server an internet connection is needed. Please connect to the Internet and try again or use the local reporting on the Onuma BIM App." message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];
        return;
    }
    uint err=[appDele reCheckUserNameAndPW];
    if (err!=0) {return;}
    
    
    int liveModifiedDate=[projectSite liveModifiedDate];
    int localModifiedDate=[[appDele currentSite] dateModified];
    if (liveModifiedDate>localModifiedDate){        
        UIAlertView *liveAttachmentFromEditedSchemeMsg = [[UIAlertView alloc] initWithTitle:@"Scheme has been edited since download. Please download this scheme later for more accurate live info" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [liveAttachmentFromEditedSchemeMsg show];
        [liveAttachmentFromEditedSchemeMsg release];        
        
    }
    
    
    NSString* urlStr=nil;    
    if ([appDele modelDisplayLevel]==0){

        if ((self.viewModelVC!=nil) && [self.viewModelVC didSelectOneLevelCascadedProduct]){
            ViewProductRep* viewProductRep=[[self.viewModelVC aSelectedProductRep] objectAtIndex:0];
            Bldg* bldg=nil;
            OPSProduct* product=[viewProductRep product];  
            if ([product isKindOfClass:[Floor class]]){
                RelAggregate* relAgg=(RelAggregate*) [product linkedRel];            
                bldg=(Bldg*) [relAgg relating];
            }                    
            urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS-beta/report/reportBldg.php?sysID=%d&projectID=%d&siteID=%d&bldgID=%d&u=%@&p=%@",[appDele activeStudioID],[projectSite projectID ], [projectSite ID],[bldg ID], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]]];
            
        }else{
            urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS-beta/report/reportSite.php?sysID=%d&projectID=%d&siteID=%d&u=%@&p=%@",[appDele activeStudioID],[projectSite projectID ], [projectSite ID],[ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]]];
        }

        NSLog(@"%@",urlStr);
    }else if ([appDele modelDisplayLevel]==1){
        
//        ViewProductRep* viewProductRep=[[self.viewModelVC aSelectedProductRep] objectAtIndex:0];
//        Bldg* bldg=nil;
//        OPSProduct* product=[viewProductRep product];  
//        if ([product isKindOfClass:[Floor class]]){
//            RelAggregate* relAgg=(RelAggregate*) [product linkedRel];            
//            bldg=(Bldg*) [relAgg relating];
//        }                    
        if ((self.viewModelVC!=nil)&&[self.viewModelVC didSelectOneLevelCascadedProduct]){
//
//            urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS/report/reportBldg.php?sysID=%d&projectID=%d&siteID=%d&bldgID=%d&u=%@&p=%@",[appDele activeStudioID],[projectSite projectID ], [projectSite ID],[bldg ID], [appDele defUserName], [appDele defUserPW]];

            Bldg* bldg=(Bldg*) [[self.viewModelVC model] root];
            ViewProductRep* spaceProductRep=(ViewProductRep*) [[self.viewModelVC aSelectedProductRep] lastObject];
            Space* space=(Space*) [spaceProductRep product];
            
            urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS-beta/report/reporter_spaceDetails.php?sysID=%d&projectID=%d&siteID=%d&bldgID=%d&floorID=%d&spaceID=%d&u=%@&p=%@",[appDele activeStudioID],[projectSite projectID ], [projectSite ID],[bldg ID], [[bldg selectedFloor] ID ],[space ID],[ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]]];
            
        }else{
            if (self.viewModelVC!=nil){
                Bldg* bldg=(Bldg*) [[self.viewModelVC model] root];
                
                
                urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS-beta/report/reportBldg.php?sysID=%d&projectID=%d&siteID=%d&bldgID=%d&floorID=%d&u=%@&p=%@",[appDele activeStudioID],[projectSite projectID ], [projectSite ID],[bldg ID], [[bldg selectedFloor] ID ],[ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]]];
            }
        }
        
        NSLog(@"%@",urlStr);  
    }else if ([appDele modelDisplayLevel]==2){                    
        if ((self.viewModelVC!=nil) &&[self.viewModelVC didSelectOneLevelCascadedProduct]){
            
//            Space* space=(Space*) [[self.viewModelVC model] root];
//            
//            urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS/report/reportBldg.php?sysID=%d&projectID=%d&siteID=%d&bldgID=%d&floorID=%d&showSpace=1&u=%@&p=%@",[appDele activeStudioID],[projectSite projectID ], [projectSite ID],[bldg ID], [[bldg selectedFloor] ID ],[appDele defUserName], [appDele defUserPW]];
            
        }else{
            if (self.viewModelVC!=nil){
                Bldg* bldg=[appDele currentBldg];
                Space* space=(Space*) [[self.viewModelVC model] root];
                
                
                urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS-beta/report/reporter_spaceDetails.php?sysID=%d&projectID=%d&siteID=%d&bldgID=%d&floorID=%d&spaceID=%d&u=%@&p=%@",[appDele activeStudioID],[projectSite projectID ], [projectSite ID],[bldg ID], [[bldg selectedFloor] ID ],[space ID],[ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]]];
            }
        }
        
        NSLog(@"%@",urlStr);  
    }
    [appDele displayInfoWebView:urlStr];
    
}
-(id)initWithViewModelVC:(ViewModelVC*)viewModelVC{
    self=[super init];
    if (self){
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
        
        self.viewModelVC=viewModelVC;
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil viewController:(UIViewController*)viewController
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.vc=viewController;
        if ([viewController isKindOfClass:[ViewModelVC class]]){
            self.viewModelVC=(ViewModelVC*) viewController;
        }else{
            self.viewModelVC=nil;
        }
        // Custom initialization
    }
    return self;
}

-(void) drawGreyBGCellWithRowIndex:(uint)pRow hPos:(uint)hPos vPos:(uint)vPos width:(uint)width height:(uint)height{
    if (pRow%2==0){            
        UIView* greyBGCell=[[UIView alloc] initWithFrame:CGRectMake(hPos, vPos, width, height)];
//        greyBGCell.backgroundColor=[UIColor lightGrayColor];                             
        
        greyBGCell.backgroundColor=[UIColor colorWithRed:0.90 green:0.90 blue:0.90 alpha:1.0];         
        [self.infoView addSubview:greyBGCell];   
        [greyBGCell release];            
    }else{
        UIView* greyBGCell=[[UIView alloc] initWithFrame:CGRectMake(hPos, vPos, width, height)];
        
        greyBGCell.backgroundColor=[UIColor colorWithRed:0.70 green:0.70 blue:0.70 alpha:1.0];        
        [self.infoView addSubview:greyBGCell];   
        [greyBGCell release];           
    }
}

- (id)initWithViewController:(UIViewController*)viewController
{
    self = [super init];
    if (self) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
        
        
        self.vc=viewController;
        if ([viewController isKindOfClass:[ViewModelVC class]]){
            self.viewModelVC=(ViewModelVC*) viewController;
        }else{
            self.viewModelVC=nil;
        }
        // Custom initialization
    }
    return self;
}




-(void) prepareLabel:(UILabel*) titlelabel{    
    titlelabel.textAlignment =  NSTextAlignmentLeft;
    titlelabel.textColor = [UIColor blackColor];
    titlelabel.backgroundColor = [UIColor clearColor];
//    titlelabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(17.0)];
    
    titlelabel.font = [UIFont systemFontOfSize:17.0];
        
    
}
-(void) prepareUILabel:(UILabel*) uiLabel{    
    uiLabel.textAlignment =  NSTextAlignmentRight;
    uiLabel.textColor = [UIColor blackColor];
    uiLabel.backgroundColor = [UIColor clearColor];
    
    uiLabel.font = [UIFont systemFontOfSize:14.0];
//    uiLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(15.0)];            
}
-(uint) selectedProductTitleFontSize{
    return 28;
}

-(uint) selectedProductLabelFontSize{
    return 14;
}
-(void) prepareSelectedProductTitle:(UILabel*) uiLabel{    
    uiLabel.textAlignment =  NSTextAlignmentLeft;
    uiLabel.textColor = [UIColor blackColor];
    uiLabel.backgroundColor = [UIColor clearColor];    
    uiLabel.font = [UIFont systemFontOfSize:[self selectedProductTitleFontSize]];
    //    uiLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(15.0)];            
}
-(void) prepareSelectedProductLabel:(UILabel*) uiLabel{    
    uiLabel.textAlignment =  NSTextAlignmentLeft;
    uiLabel.textColor = [UIColor blackColor];
    uiLabel.backgroundColor = [UIColor clearColor];    
    uiLabel.font = [UIFont systemFontOfSize:[self selectedProductLabelFontSize]];
    //    uiLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(15.0)];            
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}
//
//- (NSString*) doubleToAreaStr:(double) d{    
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    NSString* measureUnitArea=[[[appDele currentSite] model] measureUnitArea];
//    
//    
//    return [NSString stringWithFormat:@"%@ %@",[ProjectViewAppDelegate toSeperatedNumberStr:d numberOfFraction:0],measureUnitArea];
//    
//}
//- (NSString*) doubleToLengthStr:(double) d{    
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    NSString* measureUnitLength=[[[appDele currentSite] model] measureUnitLength];
//    
//    
//    return [NSString stringWithFormat:@"%@ %@",[ProjectViewAppDelegate toSeperatedNumberStr:d numberOfFraction:0],measureUnitLength];
//    
//}
//
//
//- (NSString*) intToIntStr:(int) i{    
//    
//    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];        
//    return [NSString stringWithFormat:@"%@",[ProjectViewAppDelegate toSeperatedNumberStr:i numberOfFraction:0]];
//    
//}
//
//- (NSString*) doubleToCurrenyStr:(double) d{    
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    NSString* currencyUnit=[[[appDele currentSite] model] currencyUnit];
//    
//    
//    return [NSString stringWithFormat:@"%@%@",currencyUnit,[ProjectViewAppDelegate toSeperatedNumberStr:d numberOfFraction:0]];
//
//}

@end

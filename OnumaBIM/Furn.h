//
//  Furn.h
//  ProjectView
//
//  Created by Alfred Man on 12/6/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Element.h"
@class BitmapRep;
@interface Furn : Element {
//    NSMutableDictionary* tempDataForReadingDB;
    BitmapRep* tmpBitmapRepForDBReading;
    NSString* componentName;
//    bool _isCascadedFurn;
//    double furnZ;
}
//@property (nonatomic,assign) double furnZ;
@property (nonatomic, retain) NSString* componentName;
@property (nonatomic, retain) BitmapRep* tmpBitmapRepForDBReading;
//@property (nonatomic, assign) bool isCascadedFurn;
//-(id) initWithFurnSQLStmt: (OPSModel*) _model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*)sqlStmt parentTransform:(CGAffineTransform) parentTransform;
//-init: (OPSModel*) _model ID:(NSInteger) _ID guid:(NSString*) _GUID name:(NSString*) _name;

- (id) initWithFurnSQLStmt_mustSetParentTransformLater: (OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*)sqlStmt isGhost:(bool)isGhost;// isCascadedFurn:(bool)isCascadedFurn;


-(void) setParentTransformAndSetupRepresentation:(CGAffineTransform) parentTransform;
@end

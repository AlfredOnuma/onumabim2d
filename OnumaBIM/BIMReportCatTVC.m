//
//  BIMReportCatTVC.m
//  ProjectView
//
//  Created by Alfred Man on 4/14/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//
#import "NavScrollUIToolbar.h"
#import "BIMReportCatTVC.h"
#import "BIMPlanColorCatTVC.h"

@implementation BIMReportCatTVC

@synthesize delegate;
- (id) init: (NavScrollUIToolbar*) navScrollUIToolBar{    
    self=[super init];
    if (self){
        self.delegate=navScrollUIToolBar;
    }else{
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{    // CGSizeMake(150, 135);    
//    self.contentSizeForViewInPopover=CGSizeMake(150, 135);     
    [super viewWillAppear:animated];
    
//    self.contentSizeForViewInPopover=CGSizeMake(150, 135);    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];    
//    CGRect defaultFrame=CGRectMake( self.view.frame.origin.x,self.view.frame.origin.y,150,135);
//    self.contentSizeForViewInPopover=CGSizeMake(150, 135);
//    self.tableView.frame=defaultFrame;
    
    /*
    [UIView animateWithDuration:0.5 
                          delay:0.0 
                        options:UIViewAnimationOptionBeginFromCurrentState 
                     animations:^{
                         //animation block                         
                         self.tableView.frame=defaultFrame;                         
                     }
                     completion:^(BOOL finished){//this block starts only when
                         
                     }];
    */

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    switch ([indexPath row]) {
        case 0:
            cell.textLabel.text=@"Color";
            break;
            
        case 1:
            cell.textLabel.text=@"Report";
            break;
            
        case 2:
            cell.textLabel.text=@"BIMMail";
            break;
            
        default:
            break;
    } 
    // Configure the cell...
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    if ([indexPath row]==0){
//        NavScrollUIToolbar* navScrollUIToolbar=self.delegate;
//         [navScrollUIToolbar.popoverController ]
        BIMPlanColorCatTVC* bimPlanColorCatTVC=[[BIMPlanColorCatTVC alloc] init:self];
//        testtvc* bimPlanColorCatTVC=[[testtvc alloc] initWithNibName:@"testtvc" bundle:nil];
        [self.navigationController pushViewController:bimPlanColorCatTVC  animated:YES];
        [bimPlanColorCatTVC release];
    }
     */
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}


/*
 
 
 @synthesize delegate;
 
 
 - (id) init: (ViewModelVC*) viewModelVC{
 
 self=[super init];
 if (self){
 self.delegate=viewModelVC;
 }else{
 }
 return self;
 }
 
 
 
 - (id)initWithStyle:(UITableViewStyle)style
 {
 self = [super initWithStyle:style];
 if (self) {
 // Custom initialization
 }
 return self;
 }
 
 - (void)didReceiveMemoryWarning
 {
 // Releases the view if it doesn't have a superview.
 [super didReceiveMemoryWarning];
 
 // Release any cached data, images, etc that aren't in use.
 }
 
 #pragma mark - View lifecycle
 
 - (void)viewDidLoad
 {
 [super viewDidLoad];
 
 // Uncomment the following line to preserve selection between presentations.
 // self.clearsSelectionOnViewWillAppear = NO;
 
 // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
 // self.navigationItem.rightBarButtonItem = self.editButtonItem;
 }
 
 - (void)viewDidUnload
 {
 [super viewDidUnload];
 // Release any retained subviews of the main view.
 // e.g. self.myOutlet = nil;
 }
 
 - (void)viewWillAppear:(BOOL)animated
 {
 [super viewWillAppear:animated];
 }
 
 - (void)viewDidAppear:(BOOL)animated
 {
 [super viewDidAppear:animated];
 }
 
 - (void)viewWillDisappear:(BOOL)animated
 {
 [super viewWillDisappear:animated];
 }
 
 - (void)viewDidDisappear:(BOOL)animated
 {
 [super viewDidDisappear:animated];
 }
 
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
 {
 // Return YES for supported orientations
 return YES;
 }
 
 #pragma mark - Table view data source
 
 - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
 {
 //#warning Potentially incomplete method implementation.
 // Return the number of sections.
 return 1;
 }
 
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
 {
 //#warning Incomplete method implementation.
 // Return the number of rows in the section.
 return 3;
 }
 
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 static NSString *CellIdentifier = @"Cell";
 
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (cell == nil) {
 cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
 }
 
 switch (indexPath.row) {
 case 0:{
 cell.textLabel.text=@"RoadMap";
 }
 break;
 case 1:
 cell.textLabel.text=@"Satellite";
 break;
 case 2:
 cell.textLabel.text=@"Hybrid";            
 break;
 default:
 break;
 }
 //	NSDictionary *itemAtIndex = (NSDictionary *)[menuList objectAtIndex:indexPath.row];
 //	cell.text = [itemAtIndex objectForKey:@"option"];
 //	return cell;
 // Configure the cell...
 
 return cell;
 }
 

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    ViewModelVC* viewModelVC=(ViewModelVC*) delegate;
    [[viewModelVC displayModelUI] setMapType:indexPath.row];
    [[viewModelVC popoverController] dismissPopoverAnimated:true];
    

}
 
 */
@end

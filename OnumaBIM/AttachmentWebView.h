//
//  AttachmentWebView.h
//  ProjectView
//
//  Created by Alfred Man on 4/26/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttachmentWebView : UIWebView{
//    NSString* _initURL;
//    CGRect _initFrame;
}
//@property (nonatomic, retain) NSString* initURL;
//@property (nonatomic, assign) CGRect initFrame;
//#import "SVWebViewController.h"
//@interface AttachmentWebView : SVWebViewController
-(id) initWithFrame:(CGRect)frame initURLStr:(NSString*)initURLStr;
@end

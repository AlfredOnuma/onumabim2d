//
//  ViewPlanSiteAndBldgInfoVC.h
//  ProjectView
//
//  Created by Alfred Man on 6/22/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewPlanInfoVC.h"

@interface ViewPlanSiteAndBldgInfoVC : ViewPlanInfoVC{
    
    UIView* infoView;
    UIScrollView* infoScrollView;
                
    
    uint dateModified;
    
    UILabel* siteAreaTextField;
    
    UILabel* numBldgTextField0;
    UILabel* numBldgsNetCalculatedTextField0;
    UILabel* grossCalculatedTextField0;
    UILabel* grossReportedTextField0;
    UILabel* capacityTextField0;
    UILabel* occupancyTextField0;
    UILabel* occupancyNumberTextField0;
    UILabel* totalEstimatedBuildingCostTextField0;
    
    
    NSNumber* tmpNumBldg;        
    NSNumber* tmpSumBldgNetArea;
    NSNumber* tmpSumBldgGrossArea;
    NSNumber* tmpSumReportedGrossArea;
    NSNumber* tmpSumCapicity;
    NSNumber* tmpSumOccupancy;
    NSNumber* tmpSumOccupancyNumber;
    NSNumber* tmpSumEstimateCost;
    
    
    UILabel* numBldgTextField1;
    UILabel* numBldgsNetCalculatedTextField1;
    UILabel* grossCalculatedTextField1;
    UILabel* grossReportedTextField1;
    UILabel* capacityTextField1;
    UILabel* occupancyTextField1;
    UILabel* occupancyNumberTextField1;
    UILabel* totalEstimatedBuildingCostTextField1;
    
    
    
    
    UILabel* numBldgTextField2;
    UILabel* numBldgsNetCalculatedTextField2;
    UILabel* grossCalculatedTextField2;
    UILabel* grossReportedTextField2;
    UILabel* capacityTextField2;
    UILabel* occupancyTextField2;
    UILabel* occupancyNumberTextField2;
    UILabel* totalEstimatedBuildingCostTextField2;
    
    
    
    
    
    
    UILabel* numBldgTextField3;
    UILabel* numBldgsNetCalculatedTextField3;
    UILabel* grossCalculatedTextField3;
    UILabel* grossReportedTextField3;
    UILabel* capacityTextField3;
    UILabel* occupancyTextField3;
    UILabel* occupancyNumberTextField3;
    UILabel* totalEstimatedBuildingCostTextField3;
    
    
    UIButton* liveSiteReportButton;
    UIButton* downloadButton;
    UIButton* viewButton;
 
}
@property (nonatomic, assign) uint dateModified;

@property (nonatomic, retain) NSNumber* tmpNumBldg;        
@property (nonatomic, retain) NSNumber* tmpSumBldgNetArea;
@property (nonatomic, retain) NSNumber* tmpSumBldgGrossArea;
@property (nonatomic, retain) NSNumber* tmpSumReportedGrossArea;
@property (nonatomic, retain) NSNumber* tmpSumCapicity;
@property (nonatomic, retain) NSNumber* tmpSumOccupancy;
@property (nonatomic, retain) NSNumber* tmpSumOccupancyNumber;
@property (nonatomic, retain) NSNumber* tmpSumEstimateCost;


@property (nonatomic, retain) UIView* infoView;
@property (nonatomic, retain) UIScrollView* infoScrollView;
@property (nonatomic, retain) UIButton* liveSiteReportButton;
@property (nonatomic, retain) UIButton* viewButton;
@property (nonatomic, retain) UIButton* downloadButton;
@property (nonatomic, retain) UILabel* siteAreaTextField;

@property (nonatomic, retain) UILabel* numBldgTextField0;
@property (nonatomic, retain) UILabel* numBldgsNetCalculatedTextField0;
@property (nonatomic, retain) UILabel* grossCalculatedTextField0;
@property (nonatomic, retain) UILabel* grossReportedTextField0;
@property (nonatomic, retain) UILabel* capacityTextField0;
@property (nonatomic, retain) UILabel* occupancyTextField0;
@property (nonatomic, retain) UILabel* occupancyNumberTextField0;
@property (nonatomic, retain) UILabel* totalEstimatedBuildingCostTextField0;




@property (nonatomic, retain) UILabel* numBldgTextField1;
@property (nonatomic, retain) UILabel* numBldgsNetCalculatedTextField1;
@property (nonatomic, retain) UILabel* grossCalculatedTextField1;
@property (nonatomic, retain) UILabel* grossReportedTextField1;
@property (nonatomic, retain) UILabel* capacityTextField1;
@property (nonatomic, retain) UILabel* occupancyTextField1;
@property (nonatomic, retain) UILabel* occupancyNumberTextField1;
@property (nonatomic, retain) UILabel* totalEstimatedBuildingCostTextField1;


@property (nonatomic, retain) UILabel* numBldgTextField2;
@property (nonatomic, retain) UILabel* numBldgsNetCalculatedTextField2;
@property (nonatomic, retain) UILabel* grossCalculatedTextField2;
@property (nonatomic, retain) UILabel* grossReportedTextField2;
@property (nonatomic, retain) UILabel* capacityTextField2;
@property (nonatomic, retain) UILabel* occupancyTextField2;
@property (nonatomic, retain) UILabel* occupancyNumberTextField2;
@property (nonatomic, retain) UILabel* totalEstimatedBuildingCostTextField2;



@property (nonatomic, retain) UILabel* numBldgTextField3;
@property (nonatomic, retain) UILabel* numBldgsNetCalculatedTextField3;
@property (nonatomic, retain) UILabel* grossCalculatedTextField3;
@property (nonatomic, retain) UILabel* grossReportedTextField3;
@property (nonatomic, retain) UILabel* capacityTextField3;
@property (nonatomic, retain) UILabel* occupancyTextField3;
@property (nonatomic, retain) UILabel* occupancyNumberTextField3;
@property (nonatomic, retain) UILabel* totalEstimatedBuildingCostTextField3;

-(void) displayAttachWebView:(id)sender;
-(void) tryDownloadSite:(id)sender;

-(void) viewSite:(id)sender;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil viewController:(UIViewController *)viewController;


@end

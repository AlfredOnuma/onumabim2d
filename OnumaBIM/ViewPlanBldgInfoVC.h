//
//  ViewPlanBldgInfoVC.h
//  ProjectView
//
//  Created by Alfred Man on 5/18/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewPlanInfoVC.h"

@interface ViewPlanBldgInfoVC : ViewPlanInfoVC {
//    ViewModelVC* _viewModelVC;
//    UIView* _infoView;
//    UIView* _rootView;
//    UIScrollView* _infoScrollView;    
//    UIButton* _liveReportButton;
//        
    
    UILabel* uiBldgName;
    UILabel* uiBldgNumber;
    UILabel* uiNetCalculated;
    UILabel* uiGrossCalculated;
    UILabel* uiGrossReported;
    UILabel* uiNumFloor;
    UILabel* uiCapacity;
    UILabel* uiOccupancy;
    UILabel* uiOccupancyNumber;
    UILabel* uiTotalEstimatedBldgCost;
    UILabel* uiConstructionStatus;
    
    
}
//@property (nonatomic, assign) ViewModelVC* viewModelVC;

//@property (nonatomic, retain) UIView* rootView;
//@property (nonatomic, retain) UIView* infoView;
//@property (nonatomic, retain) UIScrollView* infoScrollView;
//@property (nonatomic, retain) UIButton* liveReportButton;

@property (nonatomic, retain) UILabel* uiBldgName;
@property (nonatomic, retain) UILabel* uiBldgNumber;
@property (nonatomic, retain) UILabel* uiNetCalculated;
@property (nonatomic, retain) UILabel* uiGrossCalculated;
@property (nonatomic, retain) UILabel* uiGrossReported;
@property (nonatomic, retain) UILabel* uiNumFloor;
@property (nonatomic, retain) UILabel* uiCapacity;
@property (nonatomic, retain) UILabel* uiOccupancy;
@property (nonatomic, retain) UILabel* uiOccupancyNumber;
@property (nonatomic, retain) UILabel* uiTotalEstimatedBldgCost;
@property (nonatomic, retain) UILabel* uiConstructionStatus;

-(id) initWithViewModelVC:(ViewModelVC*) viewModelVC;
@end

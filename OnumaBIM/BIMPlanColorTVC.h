//
//  BIMPlanColorTVC.h
//  ProjectView
//
//  Created by Alfred Man on 4/20/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIMPlanColorTVCell.h"


@interface BIMPlanColorTVC : UITableViewController <BIMPlanColorTVCellDelegate>{

    
}

-(void)addToSelection:(NSIndexPath *) indexPath;
-(void)removeFromSelection:(NSIndexPath *) indexPath;

//
//-(IBAction) addToSelectionButtonTapped:(id)sender event:(id)event;
//-(IBAction) removeFromSelectionButtonTapped:(id)sender event:(id)event;
@end

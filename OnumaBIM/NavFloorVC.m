//
//  NavFloorVC.m
//  ProjectView
//
//  Created by Alfred Man on 11/18/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "NavFloorVC.h"

#import "Bldg.h"
#import "Floor.h"
#import "ProjectViewAppDelegate.h"
#import "OPSModel.h"
#import "NavPlanNavCntr.h"
#import "ViewModelVC.h"
#import "ViewModelNavCntr.h"
#import "OPSNavUI.h"
#import "NavUIScroll.h"
#import "DisplayModelUI.h"
@implementation NavFloorVC
@synthesize selectedBldg;
@synthesize delegate;



static bool bPoopingFloor;

+(BOOL) isPoopingFloor{
    return bPoopingFloor;
}
+(void) setIsPoppingFloor:(BOOL) bPopping{
    bPoopingFloor=bPopping;
}


-(ViewModelVC*) parentViewModelVC{
    return (ViewModelVC*) self.delegate;
}


- (id) init: (ViewModelVC*) viewModelVC{
    
    self=[super init];
    if (self){
        self.delegate=(id) viewModelVC;
    }else{

    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    if (self.selectedBldg!=nil){
        int numFloors=[selectedBldg.relAggregate.related count];
        return numFloors;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
        
    RelAggregate* relAgg=[[self selectedBldg] relAggregate];    
    int numFloors=[relAgg.related count];
    int pFloor=numFloors-1-indexPath.row;
//    int displayFloorIndex=pFloor-selectedBldg.firstFloorIndex;
    Floor* floor=[[relAgg related] objectAtIndex:(pFloor)];    
//    NSString* displayName = [NSString stringWithFormat:@"%d-> %@",displayFloorIndex, floor.name];
    NSString* displayName = [NSString stringWithFormat:@"%@",floor.name];    
    cell.textLabel.text=displayName;//floor.name;


    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



#pragma mark - Table view delegate

/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    //     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
    //     // ...
    //     // Pass the selected object to the new view controller.
    //     [self.navigationController pushViewController:detailViewController animated:YES];
    //     [detailViewController release];
        
    //        [[self parentViewModelVC] showLoadingOverlay:[NSString stringWithFormat:@"Loading Floor"]];
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];          
    
    [[[self parentViewModelVC] popoverController] dismissPopoverAnimated:YES];
    [self parentViewModelVC].popoverController=nil;
    
    
    RelAggregate* relAgg=[[self selectedBldg] relAggregate];    
    int numFloors=[relAgg.related count];
    int pFloor=numFloors-1-indexPath.row;
    [self.selectedBldg setSelectedFloorIndex:pFloor];
    
    //    [[appDele viewModelNavCntr]  popFloor:false];    
    
//    [[appDele viewModelNavCntr]  popViewControllerAnimated:false];
//    [appDele incrementDisplayLevel];    
    
    
    //    [NSThread detachNewThreadSelector:@selector(displayViewModelThread:) toTarget:self withObject:self.selectedBldg];
//    [appDele displayViewModelNavCntr:self.selectedBldg]; 
    [appDele displayViewModelNavCntr1:self.selectedBldg bPopPreviousPage:true];
}

*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.

//     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
//     // ...
//     // Pass the selected object to the new view controller.
//     [self.navigationController pushViewController:detailViewController animated:YES];
//     [detailViewController release];

    
//        [[self parentViewModelVC] showLoadingOverlay:[NSString stringWithFormat:@"Loading Floor"]];
    
    [NavFloorVC setIsPoppingFloor:true];
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];          

    [[[self parentViewModelVC] popoverController] dismissPopoverAnimated:YES];
    [self parentViewModelVC].popoverController=nil;
    
        
    RelAggregate* relAgg=[[self selectedBldg] relAggregate];    
    int numFloors=[relAgg.related count];
    int pFloor=numFloors-1-indexPath.row;
    [self.selectedBldg setSelectedFloorIndex:pFloor];

//    [[appDele viewModelNavCntr]  popFloor:false];    
    NSArray* aVC=[[appDele viewModelNavCntr] viewControllers];
    if ([aVC count]>1){
        ViewModelVC* previousVC=[aVC objectAtIndex:([aVC count]-2)];
        OPSNavUI* previousNavUI=[previousVC opsNavUI];
        NavUIScroll* previousScroll= [previousNavUI navUIScroll];
        DisplayModelUI* previousDisplayModelUI=[previousScroll displayModelUI];
        [previousDisplayModelUI hideAllLayer];
        
    }
    
    [[appDele viewModelNavCntr]  popViewControllerAnimated:false];
    
    [appDele incrementDisplayLevel];    
    [appDele displayViewModelNavCntr:self.selectedBldg]; 
    
//    [NSThread detachNewThreadSelector:@selector(displayViewModelThread:) toTarget:self withObject:self.selectedBldg];

//  Must set  [NavFloorVC setIsPoppingFloor:false] in AppDele's completeSwitchingViewModelVC function;
    
//    [appDele displayViewModelNavCntr1:self.selectedBldg bPopPreviousPage:true];
}

/*

- (void)displayViewModelThread:(id)inData//(NSConnection *)connection
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
//    ViewProductLabel* viewProductLabel=(ViewProductLabel*) inData;
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];      
    [appDele displayViewModelNavCntr:self.selectedBldg]; 
       
    [self performSelectorOnMainThread:@selector(displayViewModelThreadDone:) withObject:nil waitUntilDone:NO];
    [pool release];
}

- (void)displayViewModelThreadDone:(id)inData
{
    
}
 */

@end

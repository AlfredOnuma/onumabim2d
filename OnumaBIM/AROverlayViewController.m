//
//  AROverlayViewController.m
//  ProjectView
//
//  Created by onuma on 11/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.

#import "AROverlayViewController.h"
#import "ViewModelToolbar.h"
#import "ViewModelVC.h"
#import <QuartzCore/QuartzCore.h>
#import "OPSProjectSite.h"
#import "ProjectViewAppDelegate.h"
#import "AttachmentInfoDetail.h"
#import "BIMMailVC.h"
@interface AROverlayViewController ()
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
@end

@implementation AROverlayViewController
uint const AROverlayViewController_MaximumImageReached=0;


@synthesize captureManager;
@synthesize scanningLabel;
@synthesize toolbar=_toolbar;
@synthesize photoCapturedPath=_photoCapturedPath;
@synthesize initialFrame=_initialFrame;
@synthesize offsetFrame=_offsetFrame;
@synthesize prevVC=_prevVC;
@synthesize cameraButton=_cameraButton;
@synthesize numImageTaken=_numImageTaken;
-(id) initwithN:(uint)n{
    self=[super init];
    if (self){
    }
    return self;
}
-(void) setButtonOrientation{
    
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    
     NSLog(@"orientation: %d", [[UIDevice currentDevice] orientation]);
    
    UIInterfaceOrientation uiOrient=[UIApplication sharedApplication].statusBarOrientation;
    NSLog(@"UI Orientation: %d", uiOrient);
    
    if (deviceOrientation==UIDeviceOrientationPortrait){
        if (uiOrient==UIInterfaceOrientationLandscapeRight){
            
            [self.cameraButton setFrame:CGRectMake(self.view.frame.size.width/2-30,0+44, 60, 60)];
            [self.cameraButton  setTransform:CGAffineTransformMakeRotation(-90*M_PI/180)];
        }else{
            [self.cameraButton setFrame:CGRectMake(self.view.frame.size.width/2-30, self.view.frame.size.height-60 -44, 60, 60)];
            [self.cameraButton  setTransform:CGAffineTransformMakeRotation(90*M_PI/180)];
        }
        //        [self.cameraButton setFrame:CGRectMake(self.initialFrame.size.width-60*1.5, (self.initialFrame.size.height/2), 60, 60)];
    }else if (deviceOrientation==UIDeviceOrientationLandscapeRight){
        
        [self.cameraButton setFrame:CGRectMake(self.view.frame.size.width-60, (self.view.frame.size.height/2)-30, 60, 60)];
         [self.cameraButton  setTransform:CGAffineTransformIdentity];
//        [self.cameraButton setFrame:CGRectMake(self.frame.size.width-60, (self.frame.size.height/2), 60, 60)];
    }else if (deviceOrientation==UIDeviceOrientationLandscapeLeft){
        
        [self.cameraButton setFrame:CGRectMake(self.view.frame.size.width-60, (self.view.frame.size.height/2)-30, 60, 60)];
        [self.cameraButton  setTransform:CGAffineTransformIdentity];
    }else if (deviceOrientation==UIDeviceOrientationPortraitUpsideDown){
        if (uiOrient==UIInterfaceOrientationLandscapeLeft){            
            [self.cameraButton setFrame:CGRectMake(self.view.frame.size.width/2-30,0+44, 60, 60)];
            [self.cameraButton  setTransform:CGAffineTransformMakeRotation(-90*M_PI/180)];
        }else{
            [self.cameraButton setFrame:CGRectMake(self.view.frame.size.width/2-30, self.view.frame.size.height-60, 60, 60)];
            [self.cameraButton  setTransform:CGAffineTransformMakeRotation(90*M_PI/180)];
        }
        //        [self.cameraButton setFrame:CGRectMake(self.initialFrame.size.width-60*1.5, (self.initialFrame.size.height/2), 60, 60)];
    }else{
        
        [self.cameraButton setFrame:CGRectMake(self.view.frame.size.width-60, (self.view.frame.size.height/2)-30, 60, 60)];

         [self.cameraButton  setTransform:CGAffineTransformIdentity];
    }
}
- (void)deviceOrientationDidChange:(NSNotification *)notification {
    //Obtaining the current device orientation
//    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
//    UIImageOrientation orientation = image0.imageOrientation;
//    UIImageOrientation t=image0.imageOrientation;
    UIInterfaceOrientation uiOrient=[UIApplication sharedApplication].statusBarOrientation;
//    if (uiOrient!=deviceOrientation){//UIInterfaceOrientationLandscapeRight){
//    if (uiOrient!=UIInterfaceOrientationLandscapeRight){
//        self.view.frame=self.offsetFrame
//        ;
////    }else if (uiOrient<deviceOrientation){
////        self.view.frame=CGRectOffset(self.initialFrame,20,0.0);        
////    }else if (uiOrient==deviceOrientation){
//    }else{
//        self.view.frame=self.initialFrame;
//    }
    [[self.captureManager previewLayer].connection  setVideoOrientation:uiOrient];
    
    [self setButtonOrientation];
}
-(id) init{
    self=[super init];
    if (self){
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [notificationCenter addObserver:self
                               selector:@selector(deviceOrientationDidChange:)
                                   name:UIDeviceOrientationDidChangeNotification object:nil];

        
    }
    return self;
}


-(id) initwithToolBar:(ViewModelToolbar*)toolbar prevVC:(UIViewController *)prevVC{
    self=[super init];
    if (self){
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
        self.toolbar=toolbar;
        self.prevVC=prevVC;
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [notificationCenter addObserver:self
                               selector:@selector(deviceOrientationDidChange:)
                                   name:UIDeviceOrientationDidChangeNotification object:nil];
        

    }
    return self;
}


//
//- (void) drawLayer:(CALayer*) layer inContext:(CGContextRef) ctx
//{
//	NSLog(@"draw");
//    
//    
//    ViewModelVC* viewModelVC=[[self toolbar] parentViewModelVC];
//    CGRect frame0=viewModelVC.view.frame;
//    double wid=frame0.size.width;
//    double hit=frame0.size.height;
//    
//    double ox=frame0.origin.x;
//    double oy=frame0.origin.y;
//    
//    CGContextSetStrokeColorWithColor(ctx, [UIColor blackColor].CGColor);
//    CGContextSetLineWidth(ctx, 5);    
//    CGContextMoveToPoint(ctx, 0, hit/2);
//    CGContextAddLineToPoint(ctx,wid/2, hit);
//    CGContextAddLineToPoint(ctx,wid, hit/2);
//    CGContextAddLineToPoint(ctx,wid/2, 0);
//    CGContextAddLineToPoint(ctx,0,hit/2);
//    
//    
//    CGContextStrokePath(ctx);
//}

//
//-(BOOL) prefersStatusBarHidden{
//    return YES;
//}
- (void)viewDidLoad {
    [super viewDidLoad];

//    ViewModelVC* viewModelVC=[[self toolbar] parentViewModelVC];
//    CALayer* testLayer=[[[CALayer alloc] init] autorelease];
//	[[[self view] layer] addSublayer:testLayer];
////    [testLayer setFrame:viewModelVC.view.frame];
//	CGRect layerRect = [[[viewModelVC view] layer] bounds];
//    [testLayer setBounds:layerRect];
//    [testLayer setPosition:CGPointMake(CGRectGetMidX(layerRect),CGRectGetMidY(layerRect))];
//    [testLayer setBackgroundColor:[UIColor yellowColor].CGColor];
//    [testLayer setDelegate:self];
//    [testLayer setNeedsDisplay];
//    return;
    CGRect viewModelVCFrame=self.prevVC.view.frame;//[self.toolbar.parentViewModelVC view].frame;
    CGRect navBarFrame=CGRectMake(0,0,viewModelVCFrame.size.width,44);
    
    self.view.frame= CGRectUnion(navBarFrame,CGRectOffset(viewModelVCFrame,0,44));;//[self.toolbar.parentViewModelVC view].frame;

    
	[self setCaptureManager:[[[CaptureSessionManager alloc] init] autorelease]];
    
	[[self captureManager] addVideoInputFrontCamera:NO]; // set to YES for Front Camera, No for Back camera
    
    [[self captureManager] addStillImageOutput];
    
	[[self captureManager] addVideoPreviewLayer];
    
    
    CGRect layerRect=[[[self view] layer] bounds];
    [[[self captureManager] previewLayer] setBounds:layerRect];
    [[[self captureManager] previewLayer] setPosition:CGPointMake(CGRectGetMidX(layerRect),CGRectGetMidY(layerRect))];
    
//    [[[self captureManager] previewLayer] setTransform:CATransform3DMakeScale(0.5, 0.5, 1.0)];
	[[[self view] layer] addSublayer:[[self captureManager] previewLayer]];
     self.initialFrame=[self.view frame];
    
    self.offsetFrame=self.initialFrame;
    self.offsetFrame=CGRectOffset(self.offsetFrame, 20.0, 0.0);
    
//    UIImageView *overlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlaygraphic.png"]];
//    [overlayImageView setFrame:CGRectMake(30, 100, 260, 200)];
//    [[self view] addSubview:overlayImageView];
//    [overlayImageView release];
//    
    UIButton *overlayCameraButton = [[UIButton alloc]init];// buttonWithType:UIButtonTypeCustom];

    [overlayCameraButton setImage:[UIImage imageNamed:@"camera_Capture60.png"] forState:UIControlStateNormal];
    [overlayCameraButton setFrame:CGRectMake(self.initialFrame.size.width/2, (self.initialFrame.size.height-60*1.5), 60, 60)];
    
//    [overlayCameraButton setFrame:CGRectMake(self.view.frame.size.width-60, (self.view.frame.size.height/2), 60, 60)];
//    [overlayCameraButton setTransform:CGAffineTransformMakeRotation(90*M_PI/180)];
    [overlayCameraButton addTarget:self action:@selector(scanButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    self.cameraButton=overlayCameraButton;
    [overlayCameraButton release];overlayCameraButton=nil;
    [self setButtonOrientation];
    
    
    [[self view] addSubview:self.cameraButton];
    
    
    
//    
//    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 50, 120, 30)];
//    [self setScanningLabel:tempLabel];
//    [tempLabel release];
//	[scanningLabel setBackgroundColor:[UIColor clearColor]];
//	[scanningLabel setFont:[UIFont fontWithName:@"Courier" size: 18.0]];
//	[scanningLabel setTextColor:[UIColor redColor]];
//	[scanningLabel setText:@"Saving..."];
//    [scanningLabel setHidden:YES];
//	[[self view] addSubview:scanningLabel];
    self.title=@"";
    self.numImageTaken=0;
    /*
    
    
    
    
    
    CGRect cameraToolbarFrame =CGRectMake(0, self.initialFrame.size.height-60, self.initialFrame.size.width, 60);
    UIToolbar* cameraToolBar=[[UIToolbar alloc]initWithFrame:cameraToolbarFrame];
    
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:2];
    UIBarButtonItem* bi=nil; 
    UIButton *button=nil;
    UIImage *buttonHighlightImage=nil;
    UIImage *buttonImage=nil;
    

    
    bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonItemStyleDone target:self action:@selector(done)];    
//    bi.width=spacingWidth;
//    bi.style = UIBarButtonItemStyleBordered;
    [buttons addObject:bi];
    [bi release];
    
        
    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 300.0f, 30.0f)];
    [self setScanningLabel:tempLabel];
    [tempLabel release];
	[scanningLabel setBackgroundColor:[UIColor clearColor]];
	[scanningLabel setFont:[UIFont fontWithName:@"Courier" size: 18.0]];
	[scanningLabel setTextColor:[UIColor blackColor]];
	[scanningLabel setText:@"Saving..."];
    [scanningLabel setHidden:YES];
    
//    
//    ViewModelToolBarTitleScroll* _titleScroll=[[ViewModelToolBarTitleScroll alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 480.0f, 44.0f ) viewModelToolbar:self];
//    //        titleScroll.delegate=self;
//    self.titleScroll=_titleScroll;
//    [_titleScroll release];
    
    bi = [[UIBarButtonItem alloc] initWithCustomView:self.scanningLabel];
//    [titleScroll release];
//    buttonIndex_titleScroll=[buttons count];
    [buttons addObject:bi];
    [bi release];

//    bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
//    bi.style = UIBarButtonItemStyleBordered;
//    [buttons addObject:bi];
//    [bi release];
//
//    
//    
    
    buttonImage = [UIImage imageNamed:@"camera_60.png"];
    buttonHighlightImage = [UIImage imageNamed:@"camera_60.png"];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
    
    button.frame = CGRectMake(self.initialFrame.size.width/2-22, 0.0, 44,44);// buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(scanButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    bi = [[UIBarButtonItem alloc] initWithCustomView:button];
    bi.enabled=true;
    
    [bi setEnabled:true];
    
//    buttonIndex_BimMail=[buttons count];
    [buttons addObject:bi];
    [bi release];

    
    
    
    
    
    
    cameraToolBar.items=buttons;
    cameraToolBar.alpha=0.7;
    [[self view] addSubview:cameraToolBar];
    [cameraToolBar release];cameraToolBar=nil;
    
    */
    
////    
////    
//    UIButton *overlayDoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [overlayDoneButton setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
//    [overlayDoneButton setFrame:CGRectMake(self.initialFrame.size.width-60*1.5, (self.initialFrame.size.height/2), 60, 60)];;
//    [overlayDoneButton addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
//    [[self view] addSubview:overlayDoneButton];
//    
//    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 50, 120, 30)];
//    [self setScanningLabel:tempLabel];
//    [tempLabel release];
//	[scanningLabel setBackgroundColor:[UIColor clearColor]];
//	[scanningLabel setFont:[UIFont fontWithName:@"Courier" size: 18.0]];
//	[scanningLabel setTextColor:[UIColor redColor]];
//	[scanningLabel setText:@"Saving..."];
//    [scanningLabel setHidden:YES];
//	[[self view] addSubview:scanningLabel];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveImageToPhotoAlbum) name:kImageCapturedSuccessfully object:nil];
    
	[[captureManager captureSession] startRunning];
}

- (void)scanButtonPressed {
	[[self scanningLabel] setHidden:NO];
    self.title=@"Saving...";
    [[self captureManager] captureStillImage];
    self.cameraButton.enabled=false;
}
//
//-(void) done{
//    [[self view]  removeFromSuperview];    
//    [[self toolbar] refreshToolbar];
//    
//}

- (void)saveImageToPhotoAlbum
{
    
    UIImage* image0=[[self captureManager] stillImage];
    
    
//    UIImageOrientation t=image0.imageOrientation;
//    UIInterfaceOrientation uiOrient=[UIApplication sharedApplication].statusBarOrientation;
    UIDeviceOrientation deviceOrient = [[UIDevice currentDevice] orientation];
    UIImage* image=nil;

    if (deviceOrient==UIDeviceOrientationPortraitUpsideDown){
        image = [[UIImage alloc] initWithCGImage: image0.CGImage
                                           scale: 1.0
                                     orientation: UIImageOrientationLeft];
    }else if (deviceOrient==UIDeviceOrientationPortrait){
        image = [[UIImage alloc] initWithCGImage: image0.CGImage
                                           scale: 1.0
                                     orientation: UIImageOrientationRight];
    }else{
        image = [[UIImage alloc] initWithCGImage: image0.CGImage
                                           scale: 1.0
                                     orientation: image0.imageOrientation];
    }
    
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
    UIImage* correctedImg=nil;
    
    
    NSLog(@"image orientation: %d", image.imageOrientation);
    
    UIImageOrientation orientation = image.imageOrientation;
    UIGraphicsBeginImageContext(image.size);
    [image drawAtPoint:CGPointMake(0, 0)];
    
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    //        CGContextSaveGState(bitmap);
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (bitmap, M_PI);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (bitmap, M_PI);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (bitmap, 0);
    }
    //        correctedImg=UIGraphicsGetImageFromCurrentImageContext();
    //        CGContextRestoreGState(bitmap);
    
    
    
    //        CGContextDrawImage(bitmap, CGRectMake(-image.size.width / 2, -image.size.height / 2, image.size.width, image.size.height), [image CGImage]);
    
    correctedImg=[ [UIImage alloc] initWithCGImage: UIGraphicsGetImageFromCurrentImageContext().CGImage];
    UIGraphicsEndImageContext();

    /*
//
    if (image.imageOrientation==UIImageOrientationUp){
        
        correctedImg=[[UIImage alloc] initWithCGImage:image.CGImage scale:1.0 orientation:UIImageOrientationUp];
    }else if (image.imageOrientation==UIImageOrientationRight){
//        UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,image.size.width, image.size.height)];
//        CGAffineTransform t = CGAffineTransformMakeRotation(90*M_PI/180);
//        rotatedViewBox.transform = t;
//        CGSize rotatedSize = rotatedViewBox.frame.size;
//        
//        // Create the bitmap context
//        UIGraphicsBeginImageContext(rotatedSize);
//        CGContextRef bitmap = UIGraphicsGetCurrentContext();
//        
//        // Move the origin to the middle of the image so we will rotate and scale around the center.
//        CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
//        
//        // Rotate the image context
//        CGContextRotateCTM(bitmap, 90*M_PI/180);
//        
//        // Now, draw the rotated/scaled image into the context
//        CGContextScaleCTM(bitmap, 1.0, -1.0);
//        CGContextDrawImage(bitmap, CGRectMake(-image.size.width / 2, -image.size.height / 2, image.size.width, image.size.height), [image CGImage]);
//        
//        correctedImg=[ [UIImage alloc] initWithCGImage: UIGraphicsGetImageFromCurrentImageContext().CGImage];
//        UIGraphicsEndImageContext();
        
    }else if (image.imageOrientation==UIImageOrientationDown){
        correctedImg=[[UIImage alloc] initWithCGImage:image.CGImage scale:1.0 orientation:UIImageOrientationDown];
    }else if (image.imageOrientation==UIImageOrientationLeft){
        correctedImg=[[UIImage alloc] initWithCGImage:image.CGImage scale:1.0 orientation:UIImageOrientationLeft];
    }
    */
    /*
    UIImageOrientation orientation = src.imageOrientation;
    UIGraphicsBeginImageContext(src.size);
    [src drawAtPoint:CGPointMake(0, 0)];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, M_PI);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, M_PI);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, 0);
    }
    UIImage* image=UIGraphicsGetImageFromCurrentImageContext();
    CGContextRestoreGState(context);
    //    CGContextRelease(context);
    */
    
    
    if ([self.prevVC isKindOfClass:[AttachmentInfoDetail class]]){
        AttachmentInfoDetail* attachmentInfoDetail=(AttachmentInfoDetail*) self.prevVC;
//        UIImage* correctedImg=[ProjectViewAppDelegate correctImageRotation:image];
        [attachmentInfoDetail dealWithImageChosen:correctedImg];
    }
    if ([self.prevVC isKindOfClass:[BIMMailVC class]]){
        BIMMailVC* bimMailVC=(BIMMailVC*) self.prevVC;
//        UIImage* correctedImg=[ProjectViewAppDelegate correctImageRotation:image];
        [bimMailVC dealWithImageChosen:correctedImg];
    }
    
    if ([self.prevVC isKindOfClass:[ViewModelVC class]]){
        
        
        
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//        UIImage* correctedImg=[ProjectViewAppDelegate correctImageRotation:image];
        

//        image=[ProjectViewAppDelegate correctImageRotation:image];
        
        OPSProjectSite* projectSite=appDele.activeProjectSite;
        NSString* localSiteParentFolderPath=[projectSite.dbPath stringByDeletingLastPathComponent];
        
        NSString* localImgName=[self generateImgFileNameByDate];
        NSString *fullPath = [localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", localImgName]]; //add our image to the path
        self.photoCapturedPath=fullPath;
        
        
        NSLog (@"Image Size: %f x %f",correctedImg.size.width,correctedImg.size.height);
        
        uint maxDimen=1455;
        double imageHeightToWidthRatio=correctedImg.size.height/correctedImg.size.width;
        
        if (correctedImg.size.height>maxDimen || correctedImg.size.width>maxDimen){
            CGSize optSize=CGSizeMake( imageHeightToWidthRatio>1?maxDimen/imageHeightToWidthRatio:maxDimen,  imageHeightToWidthRatio>1?maxDimen:maxDimen*imageHeightToWidthRatio);
            NSLog (@"Saving to Smaller Size");
            if ([UIImageJPEGRepresentation([ProjectViewAppDelegate resizeImage:correctedImg width:optSize.width height:optSize.height],0.7) writeToFile:fullPath atomically:YES]) {
                //        NSLog(@"save ok");
            }
            else {
                //        NSLog(@"save failed");
            }
        }else{
            //Save the image directly with no compression and no resize
            if ([UIImageJPEGRepresentation(correctedImg, 1) writeToFile:fullPath atomically:YES]) {
                //        NSLog(@"save ok");
            }
            else {
                //        NSLog(@"save failed");
            }
        }
                
        
        NSString* slImagePath=[localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"sl_%@", localImgName]];
        uint slideWidth=470;
        uint slideHeight=470;
        //    double imageHeightToWidthRatio=image.size.height/image.size.width;
        UIImage* slImage=nil;
        //    if (image.size.height>slideHeight || image.size.width>slideWidth){
        CGSize optSize=CGSizeMake( imageHeightToWidthRatio>1?slideHeight/imageHeightToWidthRatio:slideWidth,  imageHeightToWidthRatio>1?slideHeight:slideWidth*imageHeightToWidthRatio);
        NSLog (@"Saving to Smaller Size");
        
        slImage=[ProjectViewAppDelegate resizeImage:correctedImg width:optSize.width height:optSize.height];
        if ([UIImageJPEGRepresentation(slImage,0.7) writeToFile:slImagePath atomically:YES]) {
            //        NSLog(@"save ok");
        }
        else {
            //        NSLog(@"save failed");
        }

        
        
        NSString* tbImagePath=[localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"tb_%@", localImgName]];
        
        if ([UIImageJPEGRepresentation([ProjectViewAppDelegate resizeImage:correctedImg width:44 height:44], 1.0) writeToFile:tbImagePath atomically:YES]) {
            //        NSLog(@"save ok");
        }
        else {
            //        NSLog(@"save failed");
        }
        
        sqlite3 * database=nil;
        if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {
            [self insertNewRecordWithDatabase:database];
        }
        sqlite3_close(database);
        //    }
    }
    [correctedImg release];correctedImg=nil;
    [image release];image=nil;
    
    
    if ([self.prevVC isKindOfClass:[BIMMailVC class]]){
        BIMMailVC* bimmailVC=(BIMMailVC*) self.prevVC;
        if (self.numImageTaken+[bimmailVC.aImg count] >=3) {
            
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"You can only attach 3 photos in one BIMMail."
                                  message: @""
                                  delegate: self
                                  cancelButtonTitle: @"OK"
                                  otherButtonTitles: nil];
            alert.tag = AROverlayViewController_MaximumImageReached; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
            [alert show];
            [alert release];

            

        }
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case AROverlayViewController_MaximumImageReached:
        {
            BIMMailVC* bimmailVC=(BIMMailVC*) self.prevVC;            
            [bimmailVC.navigationController popViewControllerAnimated:YES];
//            switch (buttonIndex) {
//                case 0: // cancel
//                {
//                    NSLog(@"Delete was cancelled by the user");
//                }
//                    break;
//                case 1: // delete
//                {
//                 
//                }
//                    break;
//            }
        }
        default:
            break;
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error != NULL) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Image couldn't be saved" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else {
        self.cameraButton.enabled=YES;
        self.numImageTaken++;
        if (self.numImageTaken==1){
            NSString* title=[NSString stringWithFormat: @"%d Photo taken",self.numImageTaken];
            self.title=title;
        }else if (self.numImageTaken>1){
            
            NSString* title=[NSString stringWithFormat: @"%d Photos taken",self.numImageTaken];
            self.title=title;
            
        }
//        [[self scanningLabel] setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [_cameraButton release];_cameraButton=nil;
    
    [_photoCapturedPath release], _photoCapturedPath=nil;

    [captureManager release], captureManager = nil;
    [scanningLabel release], scanningLabel = nil;
    [super dealloc];
}









- (NSString*) generateImgFileNameByDate{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;
    NSTimeInterval ti=[[NSDate date] timeIntervalSince1970];
    NSString* _dateStr=[NSString stringWithFormat:@"%f", ti];
    NSString* localImgName=[NSString stringWithFormat: @"^%d^%@^%@.jpg",[projectSite ID],[appDele defUserName],_dateStr];
    return  localImgName;
}



//- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker{
//    
//    
//    [picker dismissViewControllerAnimated:YES completion:nil];
//    
//    //
//    //    [self.imgLibraryPopOverController dismissPopoverAnimated:YES];
//    //    [_imgLibraryPopOverController release];
//    //    _imgLibraryPopOverController=nil;
//    //
//    [[self toolbar] refreshToolbar];
//    
//}
/*

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    //    [picker dismissModalViewControllerAnimated:YES];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self.imgLibraryPopOverController dismissPopoverAnimated:YES];
    [_imgLibraryPopOverController release];
    _imgLibraryPopOverController=nil;
    

    UIImage * pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    
    //    UIImage * image=[ProjectViewAppDelegate imageByRotatingImage:cameraImage fromImageOrientation:imageOrientation];
    UIImage* image=[ProjectViewAppDelegate correctImageRotation:pickedImage];

    
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        //        UIImageWriteToSavedPhotosAlbum( image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        UIImageWriteToSavedPhotosAlbum( image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
    
        //    [self executeDeleteImgViewFromStorageAndDB];
    
}



*/

- (void) insertNewRecordWithDatabase: (sqlite3*)database{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    //    OPSProjectSite* projectSite=appDele.activeProjectSite;
    
    NSString* attachmentTitle=@"";    
    NSString* attachmentComment=@"";
    NSString* attachmentURL=@"";
    
    NSDateFormatter *formatter;
    //        NSString  /Users/onuma/iOS/ProjectView/ViewModelToolbar.m      *dateString;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY/MM/dd h:mm:ss a"];
    //    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSString* _dateStr=[[NSString alloc] initWithString:[formatter stringFromDate:[NSDate date]]];
    NSString* attachmentDate=_dateStr;
    //    OPSProduct* lastSelectedProduct=[[[self parentViewModelVC] lastSelectedProductRep] product];
            

    OPSProduct* lastSelectedProduct=[[self.toolbar parentViewModelVC] selectedProductForReference];
    
    
    NSString* attachmentProductType=[lastSelectedProduct productType];
    
    
    uint attachCommentID=0;
    
    
    sqlite3_stmt *attachCommentStmt;
    const char* insertAttachCommentSql=[[NSString stringWithFormat:@"INSERT INTO AttachComment (\"userName\",\"commentTitle\",\"comment\",\"links\",\"commentDate\", \"attachedTo\", \"referenceID\") VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",%d)",
                                         [appDele defUserName],
                                         attachmentTitle,
                                         attachmentComment,
                                         attachmentURL,
                                         attachmentDate,
                                         attachmentProductType,
                                         lastSelectedProduct.ID] UTF8String];
    
    
    [_dateStr release];
    [formatter release];
    
    
    //        NSString* test=[NSString stringWithUTF8String:insertAttachCommentSql];
    if(sqlite3_prepare_v2 (database, insertAttachCommentSql, -1, &attachCommentStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(attachCommentStmt) == SQLITE_ROW) {
            //                int t=0;
            //                t=1;
            
        }
    }
    sqlite3_finalize(attachCommentStmt);
    
    const char* getLastIDSQL=[[NSString stringWithFormat:@"SELECT last_insert_rowid()"] UTF8String];
    
    sqlite3_stmt *getLastIDStmt;
    if(sqlite3_prepare_v2 (database, getLastIDSQL, -1, &getLastIDStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(getLastIDStmt) == SQLITE_ROW) {
            attachCommentID=sqlite3_column_int(getLastIDStmt, 0);
        }
    }
    sqlite3_finalize(getLastIDStmt);
    
    //    for (AttachmentImageView* imageView in aImg){
    
    
    sqlite3_stmt *attachImgStmt;
    
    const char* insertAttachImgSql=[[NSString stringWithFormat:@"INSERT INTO AttachFile (\"attachCommentID\",\"fileName\",\"fileTitle\",\"attachedTo\",\"referenceID\",\"uploadDate\") VALUES (%d,\"%@\",\"%@\",\"%@\",%d,\"%@\")",
                                     attachCommentID,
                                     [self.photoCapturedPath lastPathComponent],
                                     @"",
                                     attachmentProductType,
                                     lastSelectedProduct.ID,
                                     attachmentDate  ] UTF8String];
    //        NSString* test=[NSString stringWithUTF8String:insertAttachCommentSql];
    if(sqlite3_prepare_v2 (database, insertAttachImgSql, -1, &attachImgStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(attachImgStmt) == SQLITE_ROW) {
            //                    int t=0;
            //                    t=1;
            
        }
        //        }
        sqlite3_finalize(attachImgStmt);
        
        
    }            
    
    
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
}




//// Tell the system what we support
- (NSUInteger)supportedInterfaceOrientations {
    //    return UIInterfaceOrientationMaskAllButUpsideDown;
    return UIInterfaceOrientationMaskLandscapeLeft;//UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskPortraitUpsideDown;//UIInterfaceOrientationMaskAllButUpsideDown;
    //    return UIInterfaceOrientationMaskPortrait;
}
//
// Tell the system It should autorotate
- (BOOL) shouldAutorotate {
    return YES;
    
}
// Tell the system which initial orientation we want to have
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}





@end


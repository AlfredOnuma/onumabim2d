//
//  LoginNavCntr.m
//  ProjectView
//
//  Created by onuma on 05/04/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "LoginNavCntr.h"

@interface LoginNavCntr ()

@end

@implementation LoginNavCntr

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIViewController*) popViewControllerAnimated:(BOOL)animated{
    [UIView transitionWithView:self.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlDown) animations:^{
        [super popViewControllerAnimated:NO];
        
    } completion:^(BOOL finished) {
    }
     ];
    return self;
    
//    return  [super popViewControllerAnimated:animated];
    
}

@end

//
//  EverNoteGlassNote.h
//  ProjectView
//
//  Created by onuma on 12/09/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EverNoteGlassNote : NSObject{
    NSString* _notebookGUID;
    NSString* _noteGUID;
}

@property (nonatomic, retain) NSString* notebookGUID;
@property (nonatomic, retain) NSString* noteGUID;
-(id) initWithNoteBookGUID:(NSString*)bookGUID noteGUID:(NSString*)noteGUID;
@end

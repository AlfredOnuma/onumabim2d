//
//  CustomColorTVC.m
//  ProjectView
//
//  Created by Alfred Man on 4/21/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "CustomColorTVC.h"
#import "CustomColorStruct.h"
#import "ProjectViewAppDelegate.h"
#import "Site.h"

#import "Bldg.h"
#import "BIMPlanColorTVC.h"




#import "BIMPlanColorCatNVC.h"
#import "Site.h"
#import "DisplayModelUI.h"
#import "ViewModelVC.h"
#import "ViewModelToolbar.h"
#import "Space.h"




@implementation CustomColorTVC

-(void)addToSelection:(NSIndexPath *) indexPath{
    
    uint row=[indexPath row];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    Site* site=[appDele currentSite];

    
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    if ([appDele modelDisplayLevel]==0){

        for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
            if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                    Floor* floor=(Floor*)[ viewProductRep product];
                    Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                    
                                        
                    NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
                    uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
                    
                    if (aCustomColorStruct && [aCustomColorStruct count]>0){
                        CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:activeCustomColorStruct];
                        if (customColorStruct!=nil){
                            
                            
                            
//                            if ([bldg.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",activeCustomColorStruct+1]]){
//                                NSNumber* customColorIndex=[bldg.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",activeCustomColorStruct+1]];
                            if ([bldg.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",customColorStruct.customSettingNum]]){
                                NSNumber* customColorIndex=[bldg.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",customColorStruct.customSettingNum]];
                                uint pCustomColorIndexDict=customColorIndex.intValue ;
                                if (pCustomColorIndexDict>0){
                                    if ((pCustomColorIndexDict-1)==row){
                                        
                                        if (!viewProductRep.selected){
                                            [viewModelVC addAProductRepToSelection:viewProductRep];
                                        }
                                    }
                                }
                            }
                            
                            
                            //                        CustomColor* customColor=[customColorStruct.aCustomColor objectAtIndex:[indexPath row]];
                            //                        cell.colorLabel.text=customColor.name;
                            //                        cell.colorUIView.backgroundColor=customColor.color;
                        }else{
                            //                        cell.colorLabel.text=@"No Custom Color Set";
                        }
                    }else{
                        //                    cell.colorLabel.text=@"No Custom Color Set";
                        //                    cell.colorUIView.hidden=true;
                        //                    cell.addToSelectionButton.hidden=true;
                        //                    cell.minusFromSelectionButton.hidden=true;
                    }
                    
                    
                    
                    
                    
                    
                }  
            }
        }
    } else if ([appDele modelDisplayLevel]==1){   
        for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
            if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                if ([[viewProductRep product] isKindOfClass:[Space class]]){
                    Space* space=(Space*)[ viewProductRep product];
                    

                    
                    
                    
                    
                    
                    
                    
                    
                    
                    NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
                    uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
                                                    
                    if (aCustomColorStruct && [aCustomColorStruct count]>0){
                        CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:activeCustomColorStruct];
                        if (customColorStruct!=nil){

                            
//                            if ([space.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",activeCustomColorStruct+1]]){
//                                NSNumber* customColorIndex=[space.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",activeCustomColorStruct+1]];
                            
                            if ([space.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",customColorStruct.customSettingNum]]){
                                NSNumber* customColorIndex=[space.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",customColorStruct.customSettingNum]];
                                
                                uint pCustomColorIndexDict=customColorIndex.intValue ;
                                if (pCustomColorIndexDict>0){
                                    if ((pCustomColorIndexDict-1)==row){
                                        
                                        if (!viewProductRep.selected){
                                            [viewModelVC addAProductRepToSelection:viewProductRep];
                                        }
                                    }
                                }
                            }
                            
                            
    //                        CustomColor* customColor=[customColorStruct.aCustomColor objectAtIndex:[indexPath row]];
    //                        cell.colorLabel.text=customColor.name;
    //                        cell.colorUIView.backgroundColor=customColor.color;
                        }else{
    //                        cell.colorLabel.text=@"No Custom Color Set";
                        }
                    }else{
    //                    cell.colorLabel.text=@"No Custom Color Set";
    //                    cell.colorUIView.hidden=true;
    //                    cell.addToSelectionButton.hidden=true;
    //                    cell.minusFromSelectionButton.hidden=true;
                    }

                    
                    
                    
     
                    
                }  
            }
        }
        
    }
    
    
}

-(void)removeFromSelection:(NSIndexPath *) indexPath{
    
    uint row=[indexPath row];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    Site* site=[appDele currentSite];
    
    
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    if ([appDele modelDisplayLevel]==0){
        
        for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
            if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                    Floor* floor=(Floor*)[ viewProductRep product];
                    Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                    
                    
                    
                    NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
                    uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
                    
                    if (aCustomColorStruct && [aCustomColorStruct count]>0){
                        CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:activeCustomColorStruct];
                        if (customColorStruct!=nil){
                            
                            
//                            if ([bldg.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",activeCustomColorStruct+1]]){
//                                NSNumber* customColorIndex=[bldg.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",activeCustomColorStruct+1]];
                            
                            if ([bldg.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",customColorStruct.customSettingNum]]){
                                NSNumber* customColorIndex=[bldg.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",customColorStruct.customSettingNum]];
                                
                                uint pCustomColorIndexDict=customColorIndex.intValue ;
                                if (pCustomColorIndexDict>0){
                                    if ((pCustomColorIndexDict-1)==row){
                                        
                                        if (viewProductRep.selected){
                                            [viewModelVC removeAProductRepFromSelection:viewProductRep];
                                        }
                                    }
                                }
                            }
                            
                        }else{
                        }
                    }else{
                    }
                    
                    
                    
                    
                    
                    
                }  
            }
            
            
        }

    }else if ([appDele modelDisplayLevel]==1){
        for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
            if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                if ([[viewProductRep product] isKindOfClass:[Space class]]){
                    Space* space=(Space*)[ viewProductRep product];
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
                    uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
                    
                    if (aCustomColorStruct && [aCustomColorStruct count]>0){
                        CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:activeCustomColorStruct];
                        if (customColorStruct!=nil){
                            
                            
//                            if ([space.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",activeCustomColorStruct+1]]){
//                                NSNumber* customColorIndex=[space.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",activeCustomColorStruct+1]];
                            
                            if ([space.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",customColorStruct.customSettingNum]]){
                                NSNumber* customColorIndex=[space.customColorIndexDictionary objectForKey:[NSString stringWithFormat:@"%d",customColorStruct.customSettingNum]];
                                
                                uint pCustomColorIndexDict=customColorIndex.intValue ;
                                if (pCustomColorIndexDict>0){
                                    if ((pCustomColorIndexDict-1)==row){
                                        
                                        if (viewProductRep.selected){
                                            [viewModelVC removeAProductRepFromSelection:viewProductRep];
                                        }
                                    }
                                }
                            }
                            
                            
                            //                        CustomColor* customColor=[customColorStruct.aCustomColor objectAtIndex:[indexPath row]];
                            //                        cell.colorLabel.text=customColor.name;
                            //                        cell.colorUIView.backgroundColor=customColor.color;
                        }else{
                            //                        cell.colorLabel.text=@"No Custom Color Set";
                        }
                    }else{
                        //                    cell.colorLabel.text=@"No Custom Color Set";
                        //                    cell.colorUIView.hidden=true;
                        //                    cell.addToSelectionButton.hidden=true;
                        //                    cell.minusFromSelectionButton.hidden=true;
                    }
                    
                    
                    
                    
                    
                    
                }  
            }
            
            
        }
    }
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //  #warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //  #warning Incomplete method implementation.
    // Return the number of rows in the section.
    //    AlignColorTVC* alignColorTVC=[[AlignColorTVC alloc] init];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    Site* site=[appDele currentSite];
    //    alignColorTVC.title=@"Alignment Color";
    uint numRow=0;
    NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
    uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
    
    
    if (aCustomColorStruct){
        CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:(activeCustomColorStruct)];
        if (customColorStruct && ([customColorStruct.aCustomColor count]>0)){
            numRow=[customColorStruct.aCustomColor count];
        }
    }
    
    if (numRow<1){ numRow=1;}
    
    
    return  numRow;
}

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

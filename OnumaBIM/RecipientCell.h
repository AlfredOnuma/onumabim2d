//
//  RecipientCell.h
//  ProjectView
//
//  Created by Alfred Man on 4/8/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RecipientCellDelegate
//- (OPSModel*) getModel;
@end
@interface RecipientCell : UITableViewCell{
    IBOutlet UILabel* nameLabel;
    IBOutlet UILabel* roleLabel;
    IBOutlet UIButton* checkButton;
    bool checked;
    id <RecipientCellDelegate> delegate;
}
@property (nonatomic, assign) bool checked;
@property (nonatomic, retain) IBOutlet UILabel* nameLabel;
@property (nonatomic, retain) IBOutlet UILabel* roleLabel;
@property (nonatomic, retain) IBOutlet UIButton* checkButton;

@property (nonatomic, assign) id <RecipientCellDelegate> delegate;

-(IBAction) checkAction:(id)sender event:(id)event;

@end

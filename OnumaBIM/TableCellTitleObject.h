//
//  TableCellTitleObject.h
//  ProjectView
//
//  Created by Alfred Man on 1/3/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TableCellTitleObject : NSObject   {  
    NSUInteger ID;
    NSString* name;
    NSUInteger shared;
    NSString* iconName;
}
@property (nonatomic, assign) NSUInteger ID;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, assign) NSUInteger shared;
@property (nonatomic, retain) NSString* iconName;


- (id) init:(NSUInteger)_ID name:(NSString*)_name shared:(NSUInteger)_shared iconName:(NSString*)_iconName;
@end

//
//  ProjectListVC.m
//  ProjectView
//
//  Created by Alfred Man on 12/30/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "ProjectListVC.h"
#import "ProjectViewAppDelegate.h"

#import "Project.h"
//#import "DataConnect.h"
#import "ViewNeedWebForLiveDB.h"

#import "OPSProjectSite.h"

#import "TableCellTitleProject.h"
#import "TableCellTitleProjectCat.h"
#import "LocalProjectListVC.h"
#import "LiveProjectListVC.h"
#import "OPSStudio.h"
#import "OPSLocalProjectSite.h"
#import "LocalStudioListVC.h"



#import "AttachmentWebViewController.h"


//#import "testtvc.h"
@implementation ProjectListVC


//
//
//uint const OVERWRITE_ALERT = 0;
//uint const LAUNCH_LOCAL_ALERT = 1;



static sqlite3 *database = nil;

@synthesize projectArray;
@synthesize tableData;
@synthesize searchDisableViewOverlay;
@synthesize downloadDisableViewOverlay;
@synthesize theSearchBar;
@synthesize theTableView;
@synthesize actIndi;
@synthesize downloadLabel;

const uint LiveMAINLABEL_TAG=1;

const uint LocalMAINLABEL_TAG=2;

const uint LocalTRASHBUTTON_TAG=3;
const uint LocalUPLOADBUTTON_TAG=4;
const uint LocalSITEDETAILBUTTON_TAG=5;


const uint LiveTRASHBUTTON_TAG=3;
const uint LiveUPLOADBUTTON_TAG=4;
const uint LiveSITEDETAILBUTTON_TAG=5;

const uint DOWNLOADSITEBUTTON_TAG=6;



-(int) getLocalDatabaseModifiedDate{        
    
    int localSiteDateModified=-1.0; //If the local studio wasn't even created or local site wasnt downloaded, the time modified is -1
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
    
    OPSProjectSite* projectSite=[appDele activeProjectSite];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; 
    
    NSString* databaseFolderPath=nil;
    
    databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName],[appDele activeLiveStudio].ID,[appDele activeLiveStudio].name,[appDele activeLiveStudio].iconName]] ;
    
    NSFileManager *NSFm= [NSFileManager defaultManager]; 
    BOOL isDir=YES;    
    if([NSFm fileExistsAtPath:databaseFolderPath isDirectory:&isDir]){
        
        
        NSString* localDBPath=nil;       
        
        localDBPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.sqlite", [projectSite name]]] ];
        
        if([NSFm fileExistsAtPath:localDBPath isDirectory:false]){     //DB already existed                     
            
            if (sqlite3_open ([localDBPath UTF8String], &database) == SQLITE_OK) {  
                 
                
                sqlite3_stmt* spatialStructStmt;        
                NSString* spatialStructStr=[NSString stringWithFormat:@""
                                            @"SELECT spatialstructure.dateModified"
                                            @" FROM spatialstructure where spatialStructureType=\"Site\""                                ];
                
                const char* spatialStructSQLChar=[spatialStructStr UTF8String];    
                
                if(sqlite3_prepare_v2(database, spatialStructSQLChar, -1, &spatialStructStmt, NULL) == SQLITE_OK) {
                    while(sqlite3_step(spatialStructStmt) == SQLITE_ROW) {                     
                        localSiteDateModified = sqlite3_column_double(spatialStructStmt,0);                        
                    }
                }                                    
            }
            sqlite3_close(database);  
        }
        
        [localDBPath release];
    }
    return localSiteDateModified;
}



-(void) launchLocal{    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];                
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *databaseFolderPath = [paths objectAtIndex:0]; 
//    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], [appDele activeStudio].ID, [appDele activeStudio].name, [appDele activeStudio].iconName ]]; 	    
    
    OPSProjectSite* activeLiveProjectSite=[appDele activeProjectSite];    
    OPSLocalProjectSite* localProjectSite=[[OPSLocalProjectSite alloc] initWithLiveOPSProjectSite:activeLiveProjectSite];
    
    [self openProjectToDisplay:localProjectSite];
    [localProjectSite release];    
   
}






-(void) downloadSiteButton:(id)sender{
}












/*
-(void) tryDownloadSite:(id)sender{        
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
    if (![appDele isLiveDataSourceAvailable]){     
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        return;
    }   
    
    LiveProjectListVC* liveProjectListVC=[[[self navigationController] viewControllers] objectAtIndex:( [[[self navigationController] viewControllers] count]-2)];
    int localSiteDateModified=[liveProjectListVC getLocalDatabaseModifiedDate];
    if (localSiteDateModified<0){        
        [self downloadSite:sender];
        return;        
    }
 
    
    if (self.dateModified>localSiteDateModified){
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle: @"You downloaded this scheme before. But it has been modified since download, overwrite?"
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"Yes"
                              otherButtonTitles: @"No", nil];
        alert.tag = OVERWRITE_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];                                
    }else{
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle: @"You downloaded this scheme before. It hasn't been modified since then. Launch your downloaded scheme?"
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"Yes"
                              otherButtonTitles: @"No", nil];
        alert.tag = LAUNCH_LOCAL_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];                                
    }
    
    
}





- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case OVERWRITE_ALERT:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	                    
                    [self downloadSite:nil];
                }
                    break;
                case 1: // delete
                {
                    
                    NSLog(@"Delete was cancelled by the user");
                    // do the delete
                }
                    break;
            }
            
            
        }     
            break;
        case LAUNCH_LOCAL_ALERT:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	                    
                    [self launchLocal];
                }
                    break;
                case 1: // delete
                {
                    
                    NSLog(@"Delete was cancelled by the user");
                    // do the delete
                }
                    break;
            }
            
            
        }     
            break;            
            
        default:
            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");            
    }	
}



-(void) downloadSite:(id)sender{
    
    
    
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
    
    if (![appDele isLiveDataSourceAvailable]){     
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        return;
    }   
    
    
    
    
    
    
    
    
    OPSProjectSite* projectSite=[appDele activeProjectSite];
    
    
    
    NSFileManager* fileManager=[NSFileManager defaultManager];        
    [fileManager removeItemAtPath: projectSite.dbPath error:NULL]; 
    
    
    
    
    
    LiveProjectListVC* liveProjectListVC=[[[self navigationController] viewControllers] objectAtIndex:( [[[self navigationController] viewControllers] count]-2)];
    bool bNoGraphicData=false;
    [liveProjectListVC setIsOnlyLoadingSiteInfo:false];
    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Generating File from Server for site: %@",projectSite.name] invoker:liveProjectListVC completeAction:@selector(copyModelFromServer:) actionParamArray:[NSArray arrayWithObjects:[NSNumber numberWithBool:bNoGraphicData], nil]];
    
    
    
    
    
    
    
}


*/


/*
-(void) launchLocal{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *databaseFolderPath = [paths objectAtIndex:0]; 
    
    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], [appDele activeStudio].ID, [appDele activeStudio].name, [appDele activeStudio].iconName ]]; 	    
    
    
    NSFileManager *manager = [NSFileManager defaultManager];    
    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:databaseFolderPath];        
    NSString *filename;
    
    while ((filename = [direnum nextObject] )) {
        
        
        
        
        if ([filename hasSuffix:@".sqlite"]) { 
            
            
            
            NSMutableString *filenameOnly=[NSMutableString stringWithString:filename];
            [filenameOnly replaceCharactersInRange: [filenameOnly rangeOfString: @".sqlite"] withString: @""];            
            NSMutableDictionary *dictionary=nil;            
            NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
            dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];  
            NSString* pListSt=[dictionary objectForKey:filenameOnly];                        
            OPSProjectSite* projectSite=[[OPSProjectSite alloc] init:pListSt];
            
            NSString* dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:filename ] ];
            projectSite.dbPath=dbPath;
            [dbPath release];    
                                  
//            UINavigationController* nvc=self.navigationController;
//            NSArray* aVC=[nvc viewControllers];
//            uint numVC=[aVC count];
//            LiveProjectListVC* liveProjectListVC=[aVC objectAtIndex:( numVC-2)];
//            [[self navigationController] popViewControllerAnimated:false];            
//            [liveProjectListVC openProjectToDisplay:projectSite];
            [self openProjectToDisplay:projectSite];
            [projectSite release];
        }
    }
}
*/
/*
-(void) tryDownloadSite:(id)sender{
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if (![appDele isLiveDataSourceAvailable]){     
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        return;
    }   
    
    
    
    
    
    LiveProjectListVC* liveProjectListVC=[[[self navigationController] viewControllers] objectAtIndex:( [[[self navigationController] viewControllers] count]-2)];
    int localSiteDateModified=[liveProjectListVC getLocalDatabaseModifiedDate];
    if (localSiteDateModified<0){        
        [self downloadSite:sender];
        return;
        
    }
    
    if (self.dateModified>localSiteDateModified){
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle: @"You downloaded this scheme before. But it has been modified since download, overwrite?"
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"Yes"
                              otherButtonTitles: @"No", nil];
        alert.tag = OVERWRITE_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];                                
    }else{
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle: @"You downloaded this scheme before. It hasn't been modified since then. Launch your downloaded scheme?"
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"Yes"
                              otherButtonTitles: @"No", nil];
        alert.tag = LAUNCH_LOCAL_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];                                
    }
    
    
}





- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case OVERWRITE_ALERT:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	                    
                    [self downloadSite:nil];
                }
                    break;
                case 1: // delete
                {
                    
                    NSLog(@"Delete was cancelled by the user");
                    // do the delete
                }
                    break;
            }
            
            
        }     
            break;
        case LAUNCH_LOCAL_ALERT:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	                    
                    [self launchLocal];
                }
                    break;
                case 1: // delete
                {
                    
                    NSLog(@"Delete was cancelled by the user");
                    // do the delete
                }
                    break;
            }
            
            
        }     
            break;            
            
        default:
            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");            
    }	
}



-(void) downloadSite:(id)sender{
    
    
    
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
    
    if (![appDele isLiveDataSourceAvailable]){     
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        return;
    }   
    
    
    
    
    
    
    
    
    OPSProjectSite* projectSite=[appDele activeProjectSite];
    
    
    
    NSFileManager* fileManager=[NSFileManager defaultManager];        
    [fileManager removeItemAtPath: projectSite.dbPath error:NULL]; 
    
    
    
    
    
    LiveProjectListVC* liveProjectListVC=[[[self navigationController] viewControllers] objectAtIndex:( [[[self navigationController] viewControllers] count]-2)];
    bool bNoGraphicData=false;
    [liveProjectListVC setIsOnlyLoadingSiteInfo:false];
    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Generating File from Server for site: %@",projectSite.name] invoker:liveProjectListVC completeAction:@selector(copyModelFromServer:) actionParamArray:[NSArray arrayWithObjects:[NSNumber numberWithBool:bNoGraphicData], nil]];
    
    
    
    
    
    
    
}
*/




// Initialize tableData and disabledViewOverlay 
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    self.tableData =[[NSMutableArray alloc]init];
//    self.searchDisableViewOverlay = [[UIView alloc]
//                               initWithFrame:CGRectMake(0.0f,44.0f,320.0f,416.0f)];
//    self.searchDisableViewOverlay.backgroundColor=[UIColor blackColor];
//    self.searchDisableViewOverlay.alpha = 0;
//}

// Since this view is only for searching give the UISearchBar 
// focus right away
-(void) displayDownloadDisableViewOverlay{
    
}


-(void) setLocalStudioFromLiveStudio{
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    
//    documentsDirectory=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",[appDele defUserName]]];
//    NSArray* studioFolder=[self arrayOfFoldersInFolder:documentsDirectory];        
//
//        if ([studioFolderStr isEqualToString:@"0_N_(null)_I_(null)"]){
//            [[NSFileManager defaultManager] removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:studioFolderStr] error:nil];
//            continue;
//        }
//
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    
    
    OPSStudio* opsLocalStudio=[[OPSStudio alloc] init:[appDele activeLiveStudio].ID name:[appDele activeLiveStudio].name iconName:[appDele activeLiveStudio].iconName];
    
    [appDele setActiveLocalStudio:opsLocalStudio];
    [opsLocalStudio release];
    
    
    
}
-(bool) switchToLocalProjectView{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];


    UITabBarController *tabBarController = self.navigationController.tabBarController;
    if (tabBarController.selectedIndex==0){
        [appDele displayViewModelNavCntr: nil];
        return false;
    }else{
        if ([appDele activeLiveStudio]!=NULL){
            [self setLocalStudioFromLiveStudio];
        }else{
            
        }
    }
    NSArray *tabViewControllerVCs = self.navigationController.tabBarController.viewControllers;
    UIView * fromView = tabBarController.selectedViewController.view;
//    UIView* toView=[(UIViewController*)[tabViewControllerVCs objectAtIndex:0] view];
    UINavigationController* localProjectNVC=[tabViewControllerVCs objectAtIndex:0];
    uint numLocalProjectVC=[[localProjectNVC viewControllers] count];

    if (numLocalProjectVC==4){
        [localProjectNVC popViewControllerAnimated:false];
        [localProjectNVC popViewControllerAnimated:false];
    }
    if (numLocalProjectVC==3){
        [localProjectNVC popViewControllerAnimated:false];
    }
    
    numLocalProjectVC=[[localProjectNVC viewControllers] count];
    if (numLocalProjectVC==0){ //Shouldnt happen that nothing is loaded on localProject Navigation View Controllers, the [setupProject] in AppDele level should setup at least an empty local studio vc
        
        
//        LocalStudioListVC* localStudioListVC=[[LocalStudioListVC alloc]init];                                    
//        localStudioListVC.title=@"Local Studios";
//        [localProjectNVC pushViewController:localStudioListVC animated:NO];
//        localProjectNavCntr.tabBarItem.title=@"Onuma Local";
//        UIImage* localImage=[UIImage imageNamed:@"OnumaLocalTab.png"];
//        localProjectNavCntr.tabBarItem.image=localImage;
//        [localStudioListVC release];
//        
//        [projectViewControllersArray addObject:localProjectNavCntr];
//        [localProjectNavCntr release];

        
    }else if (numLocalProjectVC==1){
        LocalProjectListVC* localProjectListVC=[[LocalProjectListVC alloc]init];
        NSString* localProjectTitleStr=[[NSString alloc] initWithFormat: @"Local projects in Studio: %@", [appDele activeLiveStudio].name ];
        localProjectListVC.title=localProjectTitleStr;
        [localProjectTitleStr release];
        localProjectTitleStr=nil;
                
        [localProjectNVC pushViewController:localProjectListVC animated:false];
        [localProjectListVC release];
        localProjectListVC=nil;
        UIView * toView =[localProjectNVC view];
        NSUInteger fromIndex = [tabViewControllerVCs indexOfObject: tabBarController.selectedViewController];
        NSUInteger toIndex = 0;//[tabViewControllerVCs indexOfObject:viewController];
        
        [UIView transitionFromView:fromView
                            toView:toView
                          duration:0.3
                           options: toIndex > fromIndex ? UIViewAnimationOptionTransitionFlipFromLeft : UIViewAnimationOptionTransitionFlipFromRight
                        completion:^(BOOL finished) {
                            if (finished) {
                                tabBarController.selectedIndex = toIndex;
                                [appDele displayViewModelNavCntr: nil];
                            }
                        }];                
    }else if (numLocalProjectVC==2){
        //if local project nvc is loaded with project list
        if ([appDele activeLocalStudio]!=[appDele activeLiveStudio]){
            [localProjectNVC popViewControllerAnimated:NO];
            LocalProjectListVC* localProjectListVC=[[LocalProjectListVC alloc]init];
            NSString* localProjectTitleStr=[[NSString alloc] initWithFormat: @"Local projects in Studio: %@", [appDele activeLiveStudio].name ];
            localProjectListVC.title=localProjectTitleStr;
            [localProjectTitleStr release];
            localProjectTitleStr=nil;
            
            [localProjectNVC pushViewController:localProjectListVC animated:false];
            
            [localProjectListVC release];
            localProjectListVC=nil;
            UIView * toView =[localProjectNVC view];
            NSUInteger fromIndex = [tabViewControllerVCs indexOfObject: tabBarController.selectedViewController];
            NSUInteger toIndex = 0;//[tabViewControllerVCs indexOfObject:viewController];
            
            [UIView transitionFromView:fromView
                                toView:toView
                              duration:0.3
                               options: toIndex > fromIndex ? UIViewAnimationOptionTransitionFlipFromLeft : UIViewAnimationOptionTransitionFlipFromRight
                            completion:^(BOOL finished) {
                                if (finished) {
                                    tabBarController.selectedIndex = toIndex;
                                    [appDele displayViewModelNavCntr: nil];
                                }
                            }];
        }else{
            
            UIView * toView =[localProjectNVC view];
            NSUInteger fromIndex = [tabViewControllerVCs indexOfObject: tabBarController.selectedViewController];
            NSUInteger toIndex = 0;//[tabViewControllerVCs indexOfObject:viewController];
            
            [UIView transitionFromView:fromView
                                toView:toView
                              duration:0.3
                               options: toIndex > fromIndex ? UIViewAnimationOptionTransitionFlipFromLeft : UIViewAnimationOptionTransitionFlipFromRight
                            completion:^(BOOL finished) {
                                if (finished) {
                                    tabBarController.selectedIndex = toIndex;
                                    [appDele displayViewModelNavCntr: nil];
                                }
                            }];

        }
    }
    
    
//    UIViewController* localProjectVC=[localProjectNVC viewControllers] ;
//    
//    UIViewController* localProjectVC=[(UIViewController*)[tabViewControllerVCs objectAtIndex:0] view];
//    
//    UIView * toView =[(UIViewController*)[tabViewControllerVCs objectAtIndex:0] view];
    
//    if (fromView == toView)
//        return false;
    return true;
    
    
}
- (void) openProjectToDisplay:(OPSProjectSite*) projectSite{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDele setModelDisplayLevel:0];        
    appDele.activeProjectSite=projectSite;
    

    
    [self switchToLocalProjectView];
//    return;
    
    
    //    [selectedProjectSite release];    
//    [appDele displayViewModelNavCntr: nil];

}
- (void)viewDidAppear:(BOOL)animated {
    //    [self.theSearchBar becomeFirstResponder];
    [super viewDidAppear:animated];
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];
}
//
//-(IBAction) liveStudioBackButtonResond: (id) sender{  
//}
#pragma mark -
#pragma mark UISearchBarDelegate Methods




- (void)siteDetail:(id)sender event:(id)event{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
                
//    if (![appDele isLiveDataSourceAvailable]){
//        [self alertNoInternet:nil];
//        ////        [appDele displayProjectUploadWebView:@"htt[://www.google.com"];
//        return nil;
//    }
    
    
    
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.theTableView];
    
	NSIndexPath *indexPath = [self.theTableView indexPathForRowAtPoint: currentTouchPosition];
    //	if (indexPath != nil)
    //	{
    //		[self tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    //	}
    
    if (indexPath==nil){
        return;
    }
    
    OPSProjectSite* selectedProjectSite=[tableData objectAtIndex:indexPath.row];  
    if ([selectedProjectSite dbPath]==Nil) {
        //Live Site 
        
        [appDele setActiveProjectSite:selectedProjectSite];
        
        
        
        return;
    }    
    [appDele setActiveProjectSite:selectedProjectSite];
    [appDele readUnitStructWithDBPath:[selectedProjectSite dbPath]];
    
    
    [appDele displayProjectListSiteInfoVC: self];
    
    return;
}

- (void)trashSite:(id)sender event:(id)event{
    return;
}
- (NSData *) uploadSite:(id)sender event:(id)event 
{

    /*
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
		[self tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
	}
    
	NSMutableDictionary *item = [dataArray objectAtIndex:indexPath.row];
	
	BOOL checked = [[item objectForKey:@"checked"] boolValue];
    //	[item setObject:[NSNumber numberWithBool:!checked] forKey:@"checked"];
	UITableViewCell *cell = [item objectForKey:@"cell"];
    UIView* buttonView=(UIView*) cell.accessoryView;
    UIButton* button=[buttonView viewWithTag:1];
    
    //	UIButton *button = (UIButton *)cell.accessoryView;
	UIImage *newImage = (checked) ? [UIImage imageNamed:@"checked.png"] : [UIImage imageNamed:@"unchecked.png"];    
    
    
	[button setBackgroundImage:newImage forState:UIControlStateNormal];    
    */
    return nil;
}




//- (void)searchBar:(UISearchBar *)searchBar
//    textDidChange:(NSString *)searchText {
//    // We don't want to do anything until the user clicks 
//    // the 'Search' button.
//    // If you wanted to display results as the user types 
//    // you would do that here.
//}


- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    // We don't want to do anything until the user clicks 
    // the 'Search' button.
    // If you wanted to display results as the user types 
    // you would do that here.
    
    if([searchBar.text isEqualToString: @""]){
        
        
        [self searchBar:searchBar activate:NO];
        [tableData release];
        tableData=[[self projectArray] copy];
        [self.theTableView reloadData];
        
        [searchBar resignFirstResponder];
    }
    
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // searchBarTextDidBeginEditing is called whenever 
    // focus is given to the UISearchBar
    // call our activate method so that we can do some 
    // additional things when the UISearchBar shows.
    [self searchBar:searchBar activate:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    // searchBarTextDidEndEditing is fired whenever the 
    // UISearchBar loses focus
    // We don't need to do anything here.
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    // Clear the search text
    // Deactivate the UISearchBar
    searchBar.text=@"";
    [self searchBar:searchBar activate:NO];
//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];    
//    [tableData release];
    [tableData release];
    tableData=[[self projectArray] copy];
    [self.theTableView reloadData];
}

- (void)downloadSite:(id)sender event:(id)event{
}


- (void) validateDisplayList{
    uint pDisplayedItem=0;
    uint pPrevDisplayedItem=0;
    for (pDisplayedItem=0;pDisplayedItem<[self.tableData count];pDisplayedItem++){
        Project* currentItem=[self.tableData objectAtIndex:pDisplayedItem];
        if ([currentItem isKindOfClass:[OPSProjectSite class]] ){
            OPSProjectSite* thisProjectSite=(OPSProjectSite*) currentItem;
            Project* prevItem=[self.tableData objectAtIndex:pPrevDisplayedItem];
            
            bool bNoProjectTitle=false;
            
            if (pDisplayedItem==0){
                bNoProjectTitle=true;
            }else if ([prevItem isKindOfClass:[OPSProjectSite class]]){
                OPSProjectSite* prevProjectSite=(OPSProjectSite*) prevItem;
                if (prevProjectSite.projectID!=thisProjectSite.projectID){
                    bNoProjectTitle=true;
                }
            }
            
            
            if (bNoProjectTitle){
                TableCellTitleProject* titleProjectToAdd=nil;
                for (Project* projectArrayItem in self.projectArray){
                    if ([projectArrayItem isKindOfClass:[TableCellTitleProject class]]){
                        TableCellTitleProject* project=(TableCellTitleProject*) projectArrayItem;
                        if (project.ID == thisProjectSite.projectID){
                            titleProjectToAdd=project;
                            break;
                        }
                    }
                }
                if (titleProjectToAdd){
                    [[self tableData] insertObject:titleProjectToAdd atIndex:pDisplayedItem];
                }
            }
        
        }
        pPrevDisplayedItem=pDisplayedItem;
    }
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // Do the search and show the results in tableview
    // Deactivate the UISearchBar
	
    // You'll probably want to do this on another thread
    // SomeService is just a dummy class representing some 
    // api that you are using to do the search
    
    //
    //    if([searchText isEqualToString:@""]searchText==nil){
    //        [myTableView reloadData];
    //        return;
    //    }
    
    NSMutableArray* results=[[NSMutableArray alloc] init ];
    NSInteger counter = 0;
    /*
    for(Project *project in tableData)
    {
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
        NSRange r = [[project.name lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
        if(r.location != NSNotFound)
            [results addObject:project];
        
            
        counter++;
        
        
        [pool release];
    }
    */

    TableCellTitleProject* matchedProject=NULL;
    uint pProjectArray=0;
    for(Project *project in self.projectArray)
    {
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
        if (matchedProject==NULL){            
            NSRange r = [[project.name lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
            if(r.location != NSNotFound){
                [results addObject:project];
                if ([project isKindOfClass:[TableCellTitleProject class]]){
                    matchedProject=(TableCellTitleProject*) project; 
                            
                }else{                    
                    matchedProject=NULL;
//                    if ([project isKindOfClass:[OPSProjectSite class]]){
//                        OPSProjectSite* thisProjectSite=(OPSProjectSite*)project;
//                        if (pProjectArray>1){
//                            uint pPrevProjectArray=pProjectArray-1;
//                            Project* testPrevProject=(Project*) [projectArray objectAtIndex:pPrevProjectArray];
//                            if ([results count]>1){
//                                uint pPrevResult=[results count]-2;
//                                Project* testPrevResult=(Project*) [results objectAtIndex:pPrevResult];
//                                if ([testPrevResult isKindOfClass:[OPSProjectSite class]]){
//                                    OPSProjectSite* testPrevProjectSite=(OPSProjectSite*)testPrevProject;
//                                    if (testPrevProjectSite.projectID!=thisProjectSite.projectID){
//                                        Project* prevProjectData=(Project*) [self.projectArray objectAtIndex:pProjectArray-1];
//                                        if ([prevProjectData isKindOfClass:[TableCellTitleProject class]]){
//                                            TableCellTitleProject* prevTitleProject=(TableCellTitleProject*) prevProjectData;
//                                            [results addObject:prevTitleProject];
//                                        }
//                                    }
//                                }
//                            }
//
//                        
//                        
////                        if ([results count]>0 ){
////                            uint numResult=[results count];
////                            Project* testPrevProject=(Project*) [results objectAtIndex:numResult-1];
////                            if ([testPrevProject isKindOfClass:[
////                            
//                        
//                        
//                        
////                            uint pResult=[results count]-1;
////                            for (pResult;pResult>=0;pResult--){
////                                Project* testProject=(Project*) [results objectAtIndex:pResult];
////                                if ([testProject isKindOfClass:[TableCellTitleProject class]]){
////                                    break;
////                                }else{
////                                    if ([testProject isKindOfClass:[OPSProjectSite class]]){
////                                        OPSProjectSite* testProjectSite=(OPSProjectSite*) testProject;
////                                        if (testProjectSite
////                                    }
////                                }
////                            }
//                        }
//                    }
                }
            }
                        

        }else{
            if ([project isKindOfClass:[OPSProjectSite class]]){
                OPSProjectSite* projectSite=(OPSProjectSite*) project;
                if (projectSite.projectID==matchedProject.ID){
                    [results addObject:project];
                }else{
                    matchedProject=NULL;
                }
                
            }else{                                
                NSRange r = [[project.name lowercaseString] rangeOfString:[searchBar.text lowercaseString]];
                if(r.location != NSNotFound){
                    [results addObject:project];
                    if ([project isKindOfClass:[TableCellTitleProject class]]){
                        matchedProject=(TableCellTitleProject*) project; 
                        
                    }else{                    
                        matchedProject=NULL;
                    }
                }
            }
            
        }
        counter++;
        pProjectArray++;
        
        [pool release];
    }
        
    
    //    NSArray *results = [SomeService doSearch:searchBar.text];
	
    [self searchBar:searchBar activate:NO];

    
    NSMutableArray* _tableData=[[NSMutableArray alloc] init ];
//	[tableData release];    
    self.tableData=_tableData;
    [_tableData release];
    //    [tableData removeAllObjects];
    [self.tableData addObjectsFromArray:results];
    [self validateDisplayList];
    [self.theTableView reloadData];    
    [results release];
}

// We call this when we want to activate/deactivate the UISearchBar
// Depending on active (YES/NO) we disable/enable selection and 
// scrolling on the UITableView
// Show/Hide the UISearchBar Cancel button
// Fade the screen In/Out with the searchDisableViewOverlay and 
// simple Animations




- (void) removeDownloadDisableOverlay{
    self.theTableView.allowsSelection = true;//enable;
    self.theTableView.scrollEnabled = true;//enable;
//    if (enable) {
        [actIndi stopAnimating];        
//        [actIndi removeFromSuperview];
        [downloadDisableViewOverlay removeFromSuperview];
//        [searchBar resignFirstResponder];
//    } else {
//        self.searchDisableViewOverlay.alpha = 0;
//        [self.view addSubview:self.searchDisableViewOverlay];
//		
//        [UIView beginAnimations:@"FadeIn" context:nil];
//        [UIView setAnimationDuration:0.5];
//        self.searchDisableViewOverlay.alpha = 0.6;
//        [UIView commitAnimations];
//		
////        // probably not needed if you have a details view since you 
////        // will go there on selection
////        NSIndexPath *selected = [self.theTableView 
////                                 indexPathForSelectedRow];
////        if (selected) {
////            [self.theTableView deselectRowAtIndexPath:selected 
////                                             animated:NO];
////        }
//    }
//    [searchBar setShowsCancelButton:active animated:YES];
}

- (void) searchBar:(UISearchBar *)searchBar activate:(BOOL) active{	
    self.theTableView.allowsSelection = !active;
    self.theTableView.scrollEnabled = !active;
    if (!active) {
        [searchDisableViewOverlay removeFromSuperview];
        [searchBar resignFirstResponder];
    } else {
        self.searchDisableViewOverlay.alpha = 0;
        [self.view addSubview:self.searchDisableViewOverlay];
		
        [UIView beginAnimations:@"FadeIn" context:nil];
        [UIView setAnimationDuration:0.5];
        self.searchDisableViewOverlay.alpha = 0.6;
        [UIView commitAnimations];
		
        // probably not needed if you have a details view since you 
        // will go there on selection
        NSIndexPath *selected = [self.theTableView 
                                 indexPathForSelectedRow];
        if (selected) {
            [self.theTableView deselectRowAtIndexPath:selected 
                                             animated:NO];
        }
    }
    [searchBar setShowsCancelButton:active animated:YES];
}


#pragma mark -
#pragma mark UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {    
    return [tableData count];
}


//- (BOOL) prefersStatusBarHidden
//{
//    return YES;
//}

- (uint) readVersion{    
    NSString*	versionStr = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];    
//    NSLog(@"Version: %@",versionStr);
//    NSString* versionStr=@"1.1.2";        
    NSRange textRangeDot0;
    textRangeDot0 =[versionStr rangeOfString:@"."];    
    NSUInteger dotLocation0=textRangeDot0.location;    
    NSString* remainStr=[versionStr substringFromIndex:(dotLocation0+1)];    
    NSString* versionStr0=[versionStr substringToIndex:dotLocation0];
//    NSLog(@"%@",versionStr0);
    
    
    
    NSRange textRangeDot1;
    textRangeDot1 =[remainStr rangeOfString:@"."];    
    NSUInteger dotLocation1=textRangeDot1.location;    
    NSString* versionStr1=[remainStr substringToIndex:dotLocation1];
//    NSLog(@"%@",versionStr1);
            
    
    NSString* versionStr2=[remainStr substringFromIndex:(dotLocation1+1)];
//    NSLog(@"%@",versionStr2);
        
    uint versionInt=[versionStr0 intValue]*1000000+[versionStr1 intValue]*1000+[versionStr2 intValue];       
//    NSLog(@"%d",versionInt);
    return versionInt;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    uint MAINLABEL_TAG=1;
//    uint SECONDLABEL_TAG=2;
//    uint PHOTO_TAG=3;
    static NSString *CellIdentifier = @"ProjectListCell";
    uint mainLabelTag=[self isKindOfClass:[LocalProjectListVC class]]?LocalMAINLABEL_TAG:LiveMAINLABEL_TAG;
    uint uploadTag=[self isKindOfClass:[LocalProjectListVC class]]?LocalUPLOADBUTTON_TAG:LiveUPLOADBUTTON_TAG;
    uint trashTag=[self isKindOfClass:[LocalProjectListVC class]]?LocalTRASHBUTTON_TAG:LiveTRASHBUTTON_TAG;
    uint detailTag=[self isKindOfClass:[LocalProjectListVC class]]?LocalSITEDETAILBUTTON_TAG:LiveSITEDETAILBUTTON_TAG;

    
    UILabel *mainLabel;//, *secondLabel;
//    UIImageView *photo;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        mainLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 1024, 45.0)] autorelease];
        mainLabel.tag = mainLabelTag;
        mainLabel.font = [UIFont systemFontOfSize:30.0];
        //        mainLabel.textAlignment = UITextAlignmentRight;
        mainLabel.textAlignment = NSTextAlignmentLeft;
        mainLabel.textColor = [UIColor blackColor];
        mainLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        cell.selectionStyle=UITableViewCellSelectionStyleBlue;

        
        
        
        
        
        
        
        
        
        
        
        
        uint numButton=0;        
        if ([self isKindOfClass:[LocalProjectListVC class]]){
            numButton=3;
        }else{
            numButton=1;
        }
        
        uint pButton=0;            
        uint buttonGap=5;
        
        
        
        CGRect buttonSize=CGRectMake(0, 0, 30, 30);
        UIView* buttonView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, numButton*(buttonSize.size.width+buttonGap)-buttonGap, buttonSize.size.height)];            
        
        
        
        
        if ([self isKindOfClass:[LocalProjectListVC class]]){
            
            
            UIImage *trashImage = [UIImage imageNamed:@"trashSite.png"] ;            
            UIImage *trashHighlightImage = [UIImage imageNamed:@"trashSiteHighlight.png"] ;            
            
            UIButton *trashButton = [UIButton buttonWithType:UIButtonTypeCustom];            
            trashButton.frame =  CGRectMake(pButton*(buttonSize.size.width+buttonGap), 0.0, buttonSize.size.width, buttonSize.size.height);                
            [trashButton setImage:trashImage forState:UIControlStateNormal];            
            [trashButton setImage:trashHighlightImage forState:UIControlStateHighlighted];
            [trashButton setUserInteractionEnabled:YES];
            //                UIButton *uploadButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];        
            //                uploadButton.frame =  CGRectMake(pButton*(buttonSize.size.width+buttonGap), 0.0, buttonSize.size.width, buttonSize.size.height);                
            // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
            [trashButton addTarget:self action:@selector(trashSite:event:) forControlEvents:UIControlEventTouchUpInside];
            trashButton.tag=trashTag;
            
            [buttonView addSubview:trashButton];
            pButton++;
                        
            
            
            
            UIImage *uploadImage = [UIImage imageNamed:@"uploadSite.png"] ;            
            UIImage *uploadHighlightImage = [UIImage imageNamed:@"uploadSiteHighlight.png"] ;            
            
            UIButton *uploadButton = [UIButton buttonWithType:UIButtonTypeCustom];            
            uploadButton.frame =  CGRectMake(pButton*(buttonSize.size.width+buttonGap), 0.0, buttonSize.size.width, buttonSize.size.height);                
            [uploadButton setImage:uploadImage forState:UIControlStateNormal];            
            [uploadButton setImage:uploadHighlightImage forState:UIControlStateHighlighted];
            [uploadButton setUserInteractionEnabled:YES];
    //                UIButton *uploadButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];        
    //                uploadButton.frame =  CGRectMake(pButton*(buttonSize.size.width+buttonGap), 0.0, buttonSize.size.width, buttonSize.size.height);                
            // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
            
            [uploadButton addTarget:self action:@selector(uploadSite:event:) forControlEvents:UIControlEventTouchUpInside];
            uploadButton.tag=uploadTag;
            
            if ([[tableData objectAtIndex:indexPath.row] isKindOfClass:[OPSProjectSite class]]){
                OPSProjectSite* projectSite=(OPSProjectSite*) [tableData objectAtIndex:indexPath.row];
                if (![projectSite checkSiteHasAttachment]){
                    uploadButton.enabled=false;
                }else{
                    uploadButton.enabled=true;
                }
            }
            
            
            [buttonView addSubview:uploadButton];
            pButton++;
            
            
            
            UIImage *detailImage = [UIImage imageNamed:@"siteInfo.png"] ;            
            UIImage *detailHighlightImage = [UIImage imageNamed:@"siteInfoHighlight.png"] ;  
            UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeCustom];            
            detailButton.frame =  CGRectMake(pButton*(buttonSize.size.width+buttonGap), 0.0, buttonSize.size.width, buttonSize.size.height);                
            [detailButton setImage:detailImage forState:UIControlStateNormal];            
            [detailButton setImage:detailHighlightImage forState:UIControlStateHighlighted];
            [detailButton setUserInteractionEnabled:YES];
            
            
            
            // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
            [detailButton addTarget:self action:@selector(siteDetail:event:) forControlEvents:UIControlEventTouchUpInside];
            
            
            detailButton.tag=detailTag;
            [buttonView addSubview:detailButton];
            
            pButton++;
            
            
            
            
             

        }else{
            
            UIImage *downloadImage = [UIImage imageNamed:@"download.png"] ;            
            UIImage *downloadHighlightImage = [UIImage imageNamed:@"downloadHL.png"] ;  
            UIButton *downloadlButton = [UIButton buttonWithType:UIButtonTypeCustom];            
            downloadlButton.frame =  CGRectMake(pButton*(buttonSize.size.width+buttonGap), 0.0, buttonSize.size.width, buttonSize.size.height);                
            [downloadlButton setImage:downloadImage forState:UIControlStateNormal];            
            [downloadlButton setImage:downloadHighlightImage forState:UIControlStateHighlighted];
            [downloadlButton setUserInteractionEnabled:YES];
            
            
            
            // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
            [downloadlButton addTarget:self action:@selector(downloadSiteButton:event:) forControlEvents:UIControlEventTouchUpInside];
                        
            downloadlButton.tag=DOWNLOADSITEBUTTON_TAG;
            [buttonView addSubview:downloadlButton];
            
            pButton++;
             
        }
        
//        UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];        
//        detailButton.frame = CGRectMake(pButton*(buttonSize.size.width+buttonGap), 0.0, buttonSize.size.width, buttonSize.size.height);
        
        
        
        
        
        cell.backgroundColor = [UIColor clearColor];
        
        
        cell.accessoryView=buttonView;
        //	cell.accessoryView = button;
        [buttonView release];
        
//        cell.accessoryView.hidden=true;

                
        
        [cell.contentView addSubview:mainLabel];
        
//        secondLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0.0, 20.0, 220, 25.0)] autorelease];
//        secondLabel.tag = SECONDLABEL_TAG;
//        secondLabel.font = [UIFont systemFontOfSize:12.0];
//        //        secondLabel.textAlignment = UITextAlignmentRight;
//        secondLabel.textAlignment = UITextAlignmentLeft;        
//        secondLabel.textColor = [UIColor darkGrayColor];
//        secondLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
//        [cell.contentView addSubview:secondLabel];
        
        //        photo = [[[UIImageView alloc] initWithFrame:CGRectMake(225.0, 0.0, 80.0, 45.0)] autorelease];
        //        photo.tag = PHOTO_TAG;
        //        photo.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        //        [cell.contentView addSubview:photo];
    } else {
        mainLabel = (UILabel *)[cell.contentView viewWithTag:mainLabelTag];
//        secondLabel = (UILabel *)[cell.contentView viewWithTag:SECONDLABEL_TAG];
        //        photo = (UIImageView *)[cell.contentView viewWithTag:PHOTO_TAG];
    }
    
    
//    id tableRow=[tableData objectAtIndex:indexPath.row];

    if ([[tableData objectAtIndex:indexPath.row] isKindOfClass:[TableCellTitleProject class]]){
        //        cell.backgroundColor=[[UIColor alloc]initWithRed:0.5f green:0.5f blue:0.5f alpha:1.0];

//        cell.contentView.backgroundColor=[[UIColor alloc]initWithRed:0.5f green:0.5f blue:0.5f alpha:1.0];
        mainLabel.textColor=[UIColor whiteColor];         
//        mainLabel.font = [UIFont systemFontOfSize:24.0];
        
        [mainLabel setFont:[UIFont boldSystemFontOfSize:22]];
        TableCellTitleProject* projectCell=(TableCellTitleProject*) [tableData objectAtIndex:indexPath.row];
        
        switch  ( [projectCell shared]){
            case 0:
//                       mainLabel.backgroundColor=[[UIColor alloc]initWithRed:0.5f green:0.5f blue:0.5f alpha:1.0]; 
                mainLabel.backgroundColor=[UIColor colorWithRed:0.5f green:0.5f blue:0.5f alpha:1.0f];
                
                break;
            case 1:
//                       mainLabel.backgroundColor=[[UIColor alloc]initWithRed:0.6328125f green:0.6015625f blue:0.54296875f alpha:1.0]; 
                mainLabel.backgroundColor=[UIColor colorWithRed:0.6328125f green:0.6015625f blue:0.54296875f alpha:1.0f];
                break;
            case 2:
//                       mainLabel.backgroundColor=[[UIColor alloc]initWithRed:0.4140625f green:0.62890625f blue:0.55078125f alpha:1.0]; 
                mainLabel.backgroundColor=[UIColor colorWithRed:0.4140625f green:0.62890625f blue:0.55078125f alpha:1.0f];
                break;
            default:
                break;
                
        }

//        cell.accessoryType = UITableViewCellAccessoryNone;
        
        
        cell.accessoryView.hidden=true;
        
    }else if ([[tableData objectAtIndex:indexPath.row] isKindOfClass:[TableCellTitleProjectCat class]]){
        
        switch  ( ((TableCellTitleProjectCat*) [tableData objectAtIndex:indexPath.row]).shared){
            case 0:
                mainLabel.backgroundColor=[UIColor whiteColor];
                break;
            case 1:
//                mainLabel.backgroundColor=[[UIColor alloc]initWithRed:0.87109375f green:0.82421875f blue:0.734375f alpha:1.0]; 
                mainLabel.backgroundColor=[UIColor colorWithRed:0.87109375f green:0.82421875f blue:0.734375f alpha:1.0f];
                break;
            case 2:
                mainLabel.backgroundColor=[UIColor colorWithRed:0.7265625f green:0.91796875f blue:0.84765625f alpha:1.0f];                
                break;
            default:
                break;
                
        }        
//        cell.contentView.backgroundColor=[UIColor whiteColor];
        mainLabel.textColor=[UIColor blackColor];        
//        mainLabel.font = [UIFont systemFontOfSize:30.0];         
        [mainLabel setFont:[UIFont boldSystemFontOfSize:24]];
        
        cell.accessoryView.hidden=true;
//        cell.accessoryType = UITableViewCellAccessoryNone;
       
    }else if ([[tableData objectAtIndex:indexPath.row] isKindOfClass:[OPSProjectSite class]])
    {        
//        NSLog(@"***:%@",[tableRow name]);
        switch  ( ((OPSProjectSite*) [tableData objectAtIndex:indexPath.row]).shared){
            case 0:
                mainLabel.backgroundColor=[UIColor whiteColor];
                break;
            case 1:
//                mainLabel.backgroundColor=[[UIColor alloc]initWithRed:0.87109375f green:0.82421875f blue:0.734375f alpha:1.0]; 
                mainLabel.backgroundColor=[UIColor colorWithRed:0.87109375f green:0.82421875f blue:0.734375f alpha:1.0f];
                break;
            case 2:
//                mainLabel.backgroundColor=[[UIColor alloc]initWithRed:0.7265625f green:0.91796875f blue:0.84765625f alpha:1.0];                 
                mainLabel.backgroundColor=[UIColor colorWithRed:0.7265625f green:0.91796875f blue:0.84765625f alpha:1.0f];
                break;
            default:
                break;
                
        }
      
//        mainLabel.backgroundColor=[UIColor whiteColor];
//        cell.contentView.backgroundColor=[UIColor whiteColor];
        mainLabel.textColor=[UIColor blackColor];
        
        mainLabel.font = [UIFont systemFontOfSize:18.0];

        if ([self isKindOfClass:[LocalProjectListVC class]]){
            
            cell.accessoryView.hidden=false;
            
            
            
            cell.accessoryView.hidden=false;
            OPSProjectSite* projectSite=(OPSProjectSite*) [tableData objectAtIndex:indexPath.row];
            UIView* buttonView=cell.accessoryView;
            for (UIButton* uploadButton in buttonView.subviews){
                if (uploadButton.tag==uploadTag){
                    if (![projectSite checkSiteHasAttachment]){
                        uploadButton.enabled=false;
                    }else{
                        uploadButton.enabled=true;
                    }
                }
            }
            
            
            /*
            uint numButton=2;
            
            uint pButton=0;            
            uint buttonGap=0;
            
            

            CGRect buttonSize=CGRectMake(0, 0, 35, 35);
            UIView* buttonView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, numButton*(buttonSize.size.width+buttonGap)-buttonGap, buttonSize.size.height)];            

            
            UIImage *uploadImage = [UIImage imageNamed:@"upload.png"] ;            
            UIImage *uploadHighlightImage = [UIImage imageNamed:@"uploadHighlight.png"] ;            

            UIButton *uploadButton = [UIButton buttonWithType:UIButtonTypeCustom];            
            uploadButton.frame =  CGRectMake(pButton*(buttonSize.size.width+buttonGap), 0.0, buttonSize.size.width, buttonSize.size.height);

            
            [uploadButton setBackgroundImage:uploadImage forState:UIControlStateNormal];            
            [uploadButton setBackgroundImage:uploadHighlightImage forState:UIControlStateHighlighted];
            
            // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
            [uploadButton addTarget:self action:@selector(uploadProject:event:) forControlEvents:UIControlEventTouchUpInside];
            uploadButton.tag=1;
            
            [buttonView addSubview:uploadButton];
            pButton++;
            
            UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];

            detailButton.frame = CGRectMake(pButton*(buttonSize.size.width+buttonGap), 0.0, buttonSize.size.width, buttonSize.size.height);
            
            
            // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
            [detailButton addTarget:self action:@selector(projectDetail:event:) forControlEvents:UIControlEventTouchUpInside];
            
            
            detailButton.tag=2;
            [buttonView addSubview:detailButton];
            
            cell.backgroundColor = [UIColor clearColor];
            
            
            cell.accessoryView=buttonView;
            //	cell.accessoryView = button;
            [buttonView release];

            */
            
            
        }else{
            
            cell.accessoryView.hidden=false;
//            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }
//        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }
    //    NSDictionary *aDict = [self.list objectAtIndex:indexPath.row];        
    Project *project = [self.tableData objectAtIndex:indexPath.row];
//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//    uint studioID=0;    
//    if ([self isKindOfClass:[LocalProjectListVC class]]){
//        studioID=[appDele activeLocalStudio].ID;        
//    }else if ([self isKindOfClass:[LiveProjectListVC class]]){
//        studioID=[appDele activeLiveStudio].ID;
//    }
//    if ([[tableData objectAtIndex:indexPath.row] isKindOfClass:[OPSProjectSite class]]){
//        
//        mainLabel.text =[NSString stringWithFormat:@"S%d_%d: %@",    studioID, project.ID, project.name];//[aDict 
//    }else{
        mainLabel.text =[NSString stringWithFormat:@"%@",project.name];        
//    }

//[aDict objectForKey:@"mainTitleKey"];
//    secondLabel.text = @"_";//[aDict objectForKey:@"secondaryTitleKey"];
    //    NSString *imagePath =[[NSBundle mainBundle] pathForResource:@"aamark" ofType:@"jpg"];//[[NSBundle mainBundle] pathForResource:[aDict objectForKey:@"imageKey"] ofType:@"png"];
    //    UIImage *theImage = [UIImage imageWithContentsOfFile:imagePath];
    //    photo.image = theImage;
    
    return cell;
}


/*

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    uint MAINLABEL_TAG=1;
    uint SECONDLABEL_TAG=2;
    uint PHOTO_TAG=3;
    static NSString *CellIdentifier = @"ImageOnRightCell";
    
    UILabel *mainLabel, *secondLabel;
    UIImageView *photo;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];

        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        
        mainLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 220, 15.0)] autorelease];
        mainLabel.tag = MAINLABEL_TAG;
        mainLabel.font = [UIFont systemFontOfSize:14.0];
//        mainLabel.textAlignment = UITextAlignmentRight;
        mainLabel.textAlignment = UITextAlignmentLeft;        
        mainLabel.textColor = [UIColor blackColor];
        mainLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        [cell.contentView addSubview:mainLabel];
        
        secondLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0.0, 20.0, 220, 25.0)] autorelease];
        secondLabel.tag = SECONDLABEL_TAG;
        secondLabel.font = [UIFont systemFontOfSize:12.0];
//        secondLabel.textAlignment = UITextAlignmentRight;
        secondLabel.textAlignment = UITextAlignmentLeft;        
        secondLabel.textColor = [UIColor darkGrayColor];
        secondLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        [cell.contentView addSubview:secondLabel];

//        photo = [[[UIImageView alloc] initWithFrame:CGRectMake(225.0, 0.0, 80.0, 45.0)] autorelease];
//        photo.tag = PHOTO_TAG;
//        photo.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
//        [cell.contentView addSubview:photo];
    } else {
        mainLabel = (UILabel *)[cell.contentView viewWithTag:MAINLABEL_TAG];
        secondLabel = (UILabel *)[cell.contentView viewWithTag:SECONDLABEL_TAG];
//        photo = (UIImageView *)[cell.contentView viewWithTag:PHOTO_TAG];
    }
    
    if ([[tableData objectAtIndex:indexPath.row] isKindOfClass:[TableCellTitleProject class]]){
//        cell.backgroundColor=[[UIColor alloc]initWithRed:0.5f green:0.5f blue:0.5f alpha:1.0];
        mainLabel.backgroundColor=[[UIColor alloc]initWithRed:0.5f green:0.5f blue:0.5f alpha:1.0];
        cell.contentView.backgroundColor=[[UIColor alloc]initWithRed:0.5f green:0.5f blue:0.5f alpha:1.0];
        mainLabel.textColor=[UIColor whiteColor];        
    }else{
        mainLabel.backgroundColor=[UIColor whiteColor];
        cell.contentView.backgroundColor=[UIColor whiteColor];
        mainLabel.textColor=[UIColor blackColor];
    }
//    NSDictionary *aDict = [self.list objectAtIndex:indexPath.row];        
    Project *project = [self.tableData objectAtIndex:indexPath.row];
    
    mainLabel.text = project.name;//[aDict objectForKey:@"mainTitleKey"];
    secondLabel.text = @"_";//[aDict objectForKey:@"secondaryTitleKey"];
//    NSString *imagePath =[[NSBundle mainBundle] pathForResource:@"aamark" ofType:@"jpg"];//[[NSBundle mainBundle] pathForResource:[aDict objectForKey:@"imageKey"] ofType:@"png"];
//    UIImage *theImage = [UIImage imageWithContentsOfFile:imagePath];
//    photo.image = theImage;
    
    return cell;
}
*/
/*

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"LiveProjectSiteList";
    
    
//    CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
//    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
//    UITableViewCell *cell = [tableView
//                             dequeueReusableCellWithIdentifier:MyIdentifier];
	
    ProjectListTableSiteCell* cell=[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        
        ProjectListTableSiteCell* cell = [[[ProjectListTableSiteCell alloc] initWithFrame:CGRectZero reuseIdentifier:MyIdentifier] autorelease];
        
        
        
//        cell = [[[UITableViewCell alloc] 
//                 initWithStyle:UITableViewCellStyleDefault 
//                 reuseIdentifier:MyIdentifier] autorelease];
    }
	
    Project *project = [self.tableData objectAtIndex:indexPath.row];
//    cell.textLabel.text = project.name;
    
    
    
    
    ((ProjectListTableSiteCell*) cell).primaryLabel.text =project.name;
    ((ProjectListTableSiteCell*) cell).secondaryLabel.text = @"a";//Sat 10:30″;
//    ((ProjectListTableSiteCell*) cell).myImageView.image = [UIImage imageNamed:@"aamark.jpg"];
    return cell;
}

*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    // Navigation logic may go here. Create and push another view controller.        
    
/*    
    int row=[indexPath row];
    
    if (!([[tableData objectAtIndex:row] isKindOfClass: [OPSProjectSite class]])){
        return;
    }
    OPSProjectSite* projectSite=[tableData objectAtIndex:row];    
//    OPSProjectSite* projectSite=[[[appDel dataConnecter] projectArray] objectAtIndex:row];            

    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];     
    [[appDele dataConnecter ]copyModelFromServer:projectSite];
 */

}


#pragma mark -
#pragma mark Memory Management Methods

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];	
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {    
    
    
    
    [projectArray release], projectArray = nil;
    [theTableView release], theTableView = nil;
    [theSearchBar release], theSearchBar = nil;
    [tableData release],tableData=nil;
    [downloadDisableViewOverlay release];downloadDisableViewOverlay=nil;
    [actIndi release];actIndi=nil;
    [searchDisableViewOverlay release];searchDisableViewOverlay=nil;
    [downloadLabel release],downloadLabel=nil;
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



-(void) addHelpViewController: (id) sender{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if ([appDele isLiveDataSourceAvailable]){
        NSString* urlAddress=nil;

        urlAddress=@"https://www.onuma.com/products/OnumaHelp.php#ViewLocalProjects";

        
        
        
        
        
        
        uint width= self.view.bounds.size.width;
        uint height=self.view.bounds.size.height+44+20; //748
        
        
        
        AttachmentWebViewController* webViewController=[[AttachmentWebViewController alloc] initWithFrame:CGRectMake(0, 0, width, height) initURL:urlAddress hasNavControlBar:false];
        
        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
            [self.navigationController pushViewController:webViewController animated:NO];
            [webViewController release];
        } completion:^(BOOL finished) {
            
        }
         ];
        
        
    }else{
        
        NSString* urlAddress=nil;
        
        
        urlAddress=[[NSBundle mainBundle]pathForResource:@"ONUMA - Help" ofType:@"html" inDirectory:nil];
        UIWebView *web=[[UIWebView alloc]initWithFrame:self.view.frame];
        NSURL* url=[NSURL fileURLWithPath:urlAddress];
        
        //        NSString *absoluteURLwithQueryString = [urlAddress stringByAppendingString: @"#ViewLocalStudios"];
        //        NSURL *finalURL = [NSURL URLWithString: absoluteURLwithQueryString];
        
        
        [web loadRequest:[NSURLRequest requestWithURL:  url   ]];
        
        UIViewController* webViewController=[[UIViewController alloc] init];
        [webViewController setView:web];
        [web release];web=nil;
        
        
        [UIView transitionWithView:self.navigationController.view.window duration:0.5 options:(UIViewAnimationOptionTransitionCurlUp) animations:^{
            [self.navigationController pushViewController:webViewController animated:NO];
            [webViewController release];
        } completion:^(BOOL finished) {
            
        }
         ];
        
        

        
    }
    
}


- (void) setCustomNavigationBarButtons{
    
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:3];
    UIImage *buttonImage=nil;
    UIImage *buttonImageHighlight=nil;
    UIButton *button=nil;
    UIBarButtonItem *bi=nil;
    
    buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2.png" ofType:nil]];
    buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Help Icon V2Highlight.png" ofType:nil]];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(addHelpViewController:) forControlEvents:UIControlEventTouchUpInside];
    bi = [[UIBarButtonItem alloc] initWithCustomView:button];
    [buttons addObject:bi];
    [bi release];
    
    
    
    
    self.navigationItem.rightBarButtonItems=buttons;
    [buttons release];
    
    
    
    
}



// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    [super loadView];
    
    [self setCustomNavigationBarButtons];
//    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//    if ([appDele isLiveDataSourceAvailable]){  
    
    
        
        
        //     <string>UIInterfaceOrientationPortrait</string>
        //     <string>UIInterfaceOrientationPortraitUpsideDown</string>
        //     <string>UIInterfaceOrientationLandscapeLeft</string>
        //     <string>UIInterfaceOrientationLandscapeRight</string>
        
        CGRect frame=CGRectMake(0, 0, 768, 1024);         
        if ([UIApplication sharedApplication].statusBarOrientation==UIInterfaceOrientationLandscapeLeft ||
            [UIApplication sharedApplication].statusBarOrientation==UIInterfaceOrientationLandscapeRight ){
            frame=CGRectMake(0, 0, 1024, 768);
        }
        
        
        
//        CGRect frame = [[UIScreen mainScreen] bounds];
        // Use the max dimension for width and height
//        if (frame.size.width > frame.size.height)
//            frame.size.height = frame.size.width;
//        else
//            frame.size.width = frame.size.height;
        
        
        
//        downloadDisableViewOverlay = [[UIView alloc] initWithFrame:frame];
        
        

        
        
        frame.origin.y = 44.0f; // Offset the UISearchBar
//        searchDisableViewOverlay = [[UIView alloc] initWithFrame:frame];
        
        
        
        

        

//        
//        ...
//        [self.view addSubview:activityIndicator];
//        // release it
//        [activityIndicator release];
//        
//        actIndi = [[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
//        actIndi.frame = CGRectMake(round((frame.size.width - 25) / 2), round((frame.size.height - 25) / 2), 25, 25);
//        actIndi.tag  = 4;
//        [searchDisableViewOverlay addSubview:actIndi];
//        [actIndi startAnimating];
        
        
        
        
        UISearchBar* _searchBar=[[UISearchBar alloc]initWithFrame:CGRectMake(0,0,frame.size.width,44.0f)];
        self.theSearchBar=_searchBar;
        [_searchBar release];
        theSearchBar.delegate = self;
        [self.view addSubview:theSearchBar];
        UITableView* _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 44.0f, frame.size.width, frame.size.height-44.0f-44.0f-49.0f-20.0f)];
        
        //44.0f Navigation Bar
        //44.0f Search Bar
        //49.0f Tab Bar at Bottom
        //20.0f Status Bar at Top
        self.theTableView =_tableView;
        [_tableView release];
        theTableView.delegate = self;
        theTableView.dataSource = self;
        [self.view addSubview:theTableView];
        
        
//        
//        [[appDele dataConnecter] readSiteListFromServer];
//        self.tableData=[[[appDele dataConnecter] projectArray] copy];
        //     NSMutableArray* _tableData=[[NSMutableArray alloc]init];
        //     self.tableData =_tableData;
        //     [_tableData release];
        UIView* _searchDisableViewOverlay=[[UIView alloc]
                                     initWithFrame:CGRectMake(0.0f,44.0f,frame.size.width,frame.size.height-44.0f)];
        self.searchDisableViewOverlay = _searchDisableViewOverlay;
        [_searchDisableViewOverlay release];
        self.searchDisableViewOverlay.backgroundColor=[UIColor blackColor];
        self.searchDisableViewOverlay.alpha = 0;
        
        

    
    
        UIView* _downloadDisableViewOverlay=[[UIView alloc]
                                             initWithFrame:CGRectMake(0.0f,0.0f,frame.size.width,frame.size.height)];
        self.downloadDisableViewOverlay = _downloadDisableViewOverlay;
        [_downloadDisableViewOverlay release];
        self.downloadDisableViewOverlay.backgroundColor=[UIColor blackColor];
        self.downloadDisableViewOverlay.alpha = 0;
        

        downloadLabel=[[UILabel alloc]
                       initWithFrame:CGRectMake(0.0f, 0.0f, 800.0f, 60.0f)];
        downloadLabel.center=downloadDisableViewOverlay.center;
        downloadLabel.textAlignment=NSTextAlignmentCenter;
        downloadLabel.textColor = [UIColor whiteColor];
//        downloadLabel.text=@"asdfffffffffffffffffffffffff";
        downloadLabel.backgroundColor = [UIColor clearColor];
        downloadLabel.font = [UIFont boldSystemFontOfSize:24];

        downloadLabel.frame=CGRectOffset(downloadLabel.frame, 0, -40.0f);
        [downloadDisableViewOverlay addSubview:downloadLabel];
    
    
    
    


    
    
        actIndi = [[UIActivityIndicatorView alloc] 
                   initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, 80.0f)];
        [actIndi setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
        actIndi.center=downloadDisableViewOverlay.center;
        [downloadDisableViewOverlay addSubview:actIndi];

        //         NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:3];         
        //         UIBarButtonItem * liveStudioBackButton = [[UIBarButtonItem alloc]
        //                                 initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];         
//        UIBarButtonItem *liveStudioBackButton  = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(liveStudioBackButtonResond:)];
//        liveStudioBackButton.style = UIBarButtonItemStyleBordered;                
//        self.navigationItem.leftBarButtonItem = liveStudioBackButton;
//        [liveStudioBackButton release];
//        
        
        
        
    
//    The Code below help to add image icon for navigation back to studio list. Don't remoeve yet *****************
//    UIImage *buttonImage=nil;
//    UIImage *buttonImageHighlight=nil;
//    UIButton *button=nil;
//    UIBarButtonItem *bi=nil;    
//    NSMutableArray* leftButtons = [[NSMutableArray alloc] initWithCapacity:1];    
//    if ([self isKindOfClass:[LocalProjectListVC class]]){
//        buttonImage = [UIImage imageNamed:@"Local Studios.png"];   
//        buttonImageHighlight = [UIImage imageNamed:@"Local StudiosHighlight.png"];        
//    }else{
//        buttonImage = [UIImage imageNamed:@"Live Studios.png"];   
//        buttonImageHighlight = [UIImage imageNamed:@"Live StudiosHighlight.png"];                
//    }
//    button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button setImage:buttonImage forState:UIControlStateNormal];
//    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
//    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//    [button addTarget:self action:@selector(navBack) forControlEvents:UIControlEventTouchUpInside];
//    bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
//    //        [button release];
//    [leftButtons addObject:bi];
//    [bi release];  
//    
//    self.navigationItem.leftBarButtonItems = leftButtons;
//    [leftButtons release];
//-----------------------------------------------------------------------------------------------------------------


//    if ([self isKindOfClass:[LocalProjectListVC class]]){
//        self.navigationItem.backBarButtonItem.title=@"Local Studios";
//    }else{
//        self.navigationItem.backBarButtonItem.title=@"Live Studios";              
//    }

    
    
    
    
    
    
    
    
//    }else{
//        ViewNeedWebForLiveDB* viewNeedWebForLiveDB=[[ViewNeedWebForLiveDB alloc] initWithNibName:@"ViewNeedWebForLiveDB" bundle:[NSBundle mainBundle]];                 
//        [[self navigationController] pushViewController:viewNeedWebForLiveDB animated:YES];
//        [viewNeedWebForLiveDB release];         
//    }
    //initialize the two arrays; dataSource will be initialized and populated by appDelegate
    //     searchedData = [[NSMutableArray alloc]init];
    //     tableData = [[NSMutableArray alloc]init];
    //     [tableData addObjectsFromArray:dataSource];//on launch it should display all the records      
}
-(void) navBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) viewDidLoad{
    [super viewDidLoad];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}
/*
 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad
 {
 [super viewDidLoad];
 }
 */

//-(void) viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];    
//    [self setCustomNavigationBarButtons];
//}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        // Return YES for supported orientations
        return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
                (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
}

@end




//
//  LoginInfoViewController.h
//  ProjectView
//
//  Created by Alfred Man on 8/24/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginInfoViewController : UIViewController <UIScrollViewDelegate>{
    IBOutlet UIScrollView* _scrollview;
    IBOutlet UIView* _infoView;
    IBOutlet UILabel* _versionStr;
}
@property (nonatomic, retain) UIScrollView* scrollView;
@property (nonatomic, retain) UIView* infoView;
@property (nonatomic, retain) UILabel* versionStr;


@end

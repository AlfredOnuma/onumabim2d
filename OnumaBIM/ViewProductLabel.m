//
//  ViewProductLabel.m
//  ProjectView
//
//  Created by Alfred Man on 12/2/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "ViewProductLabel.h"

#import "ViewProductRep.h"
#import "ProjectViewAppDelegate.h"
#import "ViewModelVC.h"
#import "NavUIScroll.h"
#import "DisplayInfo.h"
#import "ViewModelVC.h"
@implementation ViewProductLabel
@synthesize productRep;
//@synthesize displacement;
@synthesize delegate;
@synthesize displayInfo;
//@synthesize productTransform;
//@synthesize displayTextArray;
//@synthesize productName;
//@synthesize area;
/*
 
 - (void) clearProductLabel{
 
 if (self.viewProductLabel!=nil){
 [viewProductLabel removeFromSuperview];
 [viewProductLabel release];
 viewProductLabel=nil;
 }
 }
 - (void) displayProductLabel{    
 
 if (self.refProduct==nil || self.geoProduct==nil){
 return;
 }
 ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
 ViewModelVC* viewModelVC=[self parentViewModelVC];//(ViewModelVC*) [appDele activeViewModelVC];
 if ([appDele modelDisplayLevel]==0){        
 if ([refProduct isKindOfClass:[Site class]]){
 return;
 }
 }
 if ([appDele modelDisplayLevel]==1){        
 if ([refProduct isKindOfClass:[Floor class]]){
 return;
 }        
 if ([refProduct isKindOfClass:[Bldg class]]){
 return;        
 }
 if ([refProduct isKindOfClass:[Furn class]]){
 return;        
 }
 
 }
 if ([appDele modelDisplayLevel]==2){
 
 if ([refProduct isKindOfClass:[Space class]]){
 return;                
 }
 }    
 if (self.viewProductLabel!=nil){
 [self clearProductLabel];
 }
 
 
 //    double modelScaleFactor=[[viewModelVC model] modelScaleForScreenFactor];    
 //    NSMutableArray* strArray=[[NSMutableArray alloc] initWithCapacity:2];    
 
 NSString* refProductName=[self.refProduct name];
 
 //    double area=[(RepresentationItem*) [[[self.geoProduct representation] aRepresentationItem] objectAtIndex:0] area]; //For now only get the 1st representation item 
 
 //    [strArray addObject:[self.refProduct name]];            
 //    NSString* labelWithArea=[refProductName 
 
 float fontSize=[viewModelVC currentProductLabelSize]/[viewModelVC navUIScrollVC].zoomScale;    
 CGSize productLabelConstraint = CGSizeMake(fontSize*50, fontSize*2);
 
 
 
 CGSize productLabelSize = [refProductName sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] 
 constrainedToSize:productLabelConstraint 
 lineBreakMode:UILineBreakModeCharacterWrap];
 
 
 UIView* productLabelLayer=[[viewModelVC displayModelUI] productViewLabelLayer];
 //    CGRect productLabelLayerRect=[self convertRect:self.bounds toView:productLabelLayer];
 //    CGPoint labelPos=CGPointMake(productLabelLayerRect.origin.x+productLabelLayerRect.size.width/2,productLabelLayerRect.origin.y+productLabelLayerRect.size.height/2); //[self convertPoint:CGPointMake(0, 0) toView:productLabelLayer];
 CGPoint origin=CGPointMake(0, 0);
 
 
 
 if ([self.geoProduct isKindOfClass:[Furn class]]){
 //        Furn* thisFurn=self.geoProduct;
 //        BitmapRep* bitmapRep=[[thisFurn.representation aRepresentationItem] objectAtIndex:0];        
 //
 //        BBox* imgViewBBox=[[bitmapRep bBox] copy];
 //        [imgViewBBox multiply:[[viewModelVC model] modelScaleForScreenFactor]];
 //        self.imageView.frame=[imgViewBBox cgRect];
 //        
 //        CGAffineTransform imageTransform= CGAffineTransformMakeTranslation(0, bitmapRep.dimensionY*modelScaleFactor);
 //        
 //        [self.imageView setTransform:CGAffineTransformScale(imageTransform, 1, -1)];         
 CGPoint bitmapPathOrigin=CGPointMake (self.imageView.frame.origin.x, self.imageView.frame.origin.y);
 CGFloat bitmapPathWidth=self.imageView.frame.size.width;
 CGFloat bitmapPathHeight=self.imageView.frame.size.height; 
 CGAffineTransform offset=CGAffineTransformMakeTranslation(bitmapPathOrigin.x+bitmapPathWidth/2, bitmapPathOrigin.y+bitmapPathHeight/2);
 origin=CGPointApplyAffineTransform(origin, offset);
 }
 
 
 // Temporarily disable '''''''''''''''''''''''''''''''''''''''''''''''''''''''
 
 
 //    CGPoint labelPos=[self convertPoint:origin toView:productLabelLayer];    
 //    ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(labelPos.x-productLabelSize.width/2, labelPos.y-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) name:[self.refProduct name] area:area displayModelUI:(DisplayModelUI*) self.delegate] ;    
 //    [_viewProductLabel setText:refProductName];
 //    self.viewProductLabel=_viewProductLabel;
 //    [_viewProductLabel release];
 //    [productLabelLayer addSubview:self.viewProductLabel];
 //    
 //    [[viewModelVC displayModelUI] bringSubviewToFront:[viewModelVC displayModelUI].productViewLabelLayer];
 
 //Temporarily disable ----------------------------------------------------
 //
 return;
 
 
 
 
 }*/
-(void) display{
   
    self.hidden=false;
    ViewModelVC* viewModelVC=[self parentViewModelVC];
          
//    ViewModelVC* modelVC=[self parentViewModelVC];//[appDele activeViewModelVC]; 
//    ViewProductRep* selectedProductRep=modelVC.selectedProductRep;
//    bool selected=(selectedProductRep==[self productRep]); 
    
    bool selected=[[self productRep] selected];
    
    
    float fontSize=[viewModelVC currentProductLabelSize]/[viewModelVC navUIScrollVC].zoomScale*(selected?1.5:1);    
    CGSize productLabelConstraint = CGSizeMake(fontSize*50, fontSize*2*3);    
    NSString* displayText=[self.displayInfo displayLabelText];
//    CGSize productLabelSize = [displayText sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] 
//                                                   constrainedToSize:productLabelConstraint 
//                                                       lineBreakMode:UILineBreakModeCharacterWrap];  
    
    
    
    [self setNumberOfLines:0];
    [self setAdjustsFontSizeToFitWidth:NO];
    
    
//    CGSize productLabelSize = [displayText sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] 
//                                      constrainedToSize:productLabelConstraint 
//                                          lineBreakMode:UILineBreakModeCharacterWrap];   
//    
    CGSize productLabelSize = [displayText sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] 
                                      constrainedToSize:productLabelConstraint 
                                          lineBreakMode:NSLineBreakByWordWrapping];
    
    
    
//    NSLog (@"product: %@ 's fontsize: %f productLabelSize %f , %f",displayText,fontSize, productLabelSize.width, productLabelSize.height);
//    self.frame=CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height);
//    CGAffineTransform currentTransform=self.transform;
//    [self setTextColor:[UIColor blackColor]];
//    [self setFrame:CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height)];

//    CGRect newFrame=CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height);
    
    CGRect newFrame=CGRectMake(self.frame.origin.x,self.frame.origin.y, productLabelSize.width, productLabelSize.height);
    [self setFrame:newFrame];
    [self setFont:[UIFont boldSystemFontOfSize:fontSize]];//    
//    
//    CGPoint origin= CGPointMake(self.displayInfo.displacement.x*modelScaleFactor, self.displayInfo.displacement.y*modelScaleFactor);    
//    CGPoint labelPos= CGPointApplyAffineTransform(origin, productTransform);       
//    CGPoint labelLayerPos=[displayModelUI.spatialStructViewLayer convertPoint:labelPos toView:displayModelUI.productViewLabelLayer]; 
//    //            [_viewProductLabel setText:[rootSpatialStruct name]];
//    //        [_viewProductLabel setTransform:rootTransform];
//    [self setTransform:CGAffineTransformMakeTranslation(labelLayerPos.x, labelLayerPos.y)];      
//    
//    
//    [self setBackgroundColor:[UIColor clearColor]];
//    [self setNeedsDisplay];
    
//    CGPoint origin= CGPointMake(displayInfo.displacement.x*modelScaleFactor, displayInfo.displacement.y*modelScaleFactor);    
//    CGPoint labelPos= CGPointApplyAffineTransform(origin, rootSpatialStructTransform);       
//    CGPoint labelLayerPos=[spatialStructViewLayer convertPoint:labelPos toView:productViewLabelLayer]; 
    
    //            ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) name:[rootSpatialStruct name] area:area displayModelUI:self] ;
    
    
//    ViewProductLabel* _viewProductLabel=[[ViewProductLabel alloc] initWithFrame:CGRectMake(-productLabelSize.width/2,-productLabelSize.height/2, productLabelSize.width, productLabelSize.height) displayInfo:displayInfo displayModelUI:self];
//    
//    
//    
//    //            [_viewProductLabel setText:[rootSpatialStruct name]];
//    //        [_viewProductLabel setTransform:rootTransform];
//    [_viewProductLabel setTransform:CGAffineTransformMakeTranslation(labelLayerPos.x, labelLayerPos.y)];        
//    //    self.viewProductLabel=_viewProductLabel;
//    //    [_viewProductLabel release];
//    [productViewLabelLayer addSubview:_viewProductLabel];
//    //        [productViewLabelLayer addSubview:_viewProductLabel];
//    [_viewProductLabel release];
    
//    [self bringSubviewToFront:[viewModelVC displayModelUI].productViewLabelLayer];
}
-(void) hide{
    self.hidden=true;
}
- (ViewModelVC*) parentViewModelVC{
    return (ViewModelVC*) [((DisplayModelUI*) self.delegate) parentViewModelVC];
}
//- (id) initWithFrame:(CGRect)frame name: (NSString*) newProductName area:(double) newArea displayModelUI:(DisplayModelUI *)displayModelUI

//- (id) initWithFrame:(CGRect)frame displayInfo:(DisplayInfo *)_displayInfo displayModelUI:(DisplayModelUI *)_displayModelUI productTransform:(CGAffineTransform)_productTransform
- (id) initWithFrame:(CGRect)frame displayInfo:(DisplayInfo *)_displayInfo displayModelUI:(DisplayModelUI *)_displayModelUI viewProductRep:(ViewProductRep*)_viewProductRep
{
    self=[super initWithFrame:frame];
    if (self){        
        
//        
//        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
//        ViewModelVC* viewModelVC=[appDele activeViewModelVC];
//      
//        self.productTransform=_productTransform;


        
        
        
        
        self.productRep=_viewProductRep;
        _viewProductRep.viewProductLabel=self;
        self.delegate=_displayModelUI;
        self.displayInfo=_displayInfo;
        ViewModelVC* viewModelVC=[self parentViewModelVC];
        float fontSize=[viewModelVC currentProductLabelSize];//[viewModelVC currentProductLabelSize] /[viewModelVC navUIScrollVC].zoomScale;
        
//        self.productName=newProductName;
//        self.area=newArea;
        
        
        
        [self setTextColor:[UIColor blackColor]];
        [self setFont:[UIFont boldSystemFontOfSize:fontSize]];
        [self setBackgroundColor:[UIColor clearColor]];
//        [self setText:self.productName];
        NSString* tmpText=[[NSString alloc]initWithString:[displayInfo displayLabelText]];
        [self setText:tmpText];
        [tmpText release];
        
        
                NSLog(@"label: %@",self.text);
        
        
        
        /*
        uint maxStrLen=0;
        uint pMaxStr=0;
        for (int pStr=0; pStr<[textArray count];pStr++){
            NSString* productLabelStr=[textArray objectAtIndex:pStr];
            uint thisStrLen=[productLabelStr length] ;
            if ( thisStrLen>maxStrLen){
                maxStrLen=thisStrLen;
                pMaxStr=pStr;
            }            
        }
        */
        return self;
    }
    return nil;
}
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/
- (void)dealloc
{
    
//    NSLog(@"viewproduct Label Dealloc");
//    [productName release];
    [super dealloc];
}
/*
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
*/
#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/
/*
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

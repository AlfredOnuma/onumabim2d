//
//  CVector.h
//  ProjectView
//
//  Created by onuma on 08/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CVector : NSObject{
    double _x;
    double _y;
    double _z;
    bool _b3;
}
@property (nonatomic,assign) double x;
@property (nonatomic,assign) double y;
@property (nonatomic,assign) double z;
@property (nonatomic,assign) bool b3;

- (id) initWithX:(double)x y:(double)y;
- (id) initWithX:(double)x y:(double)y z:(double)z;


-(void) incrementX:(double) x ;
-(void) incrementY:(double) y ;
-(void) incrementZ:(double) z ;
-(void) incrementComponentsWithX:(double)x y:(double)y;
-(void) addsVector:(CVector*)v;
-(void) subtractVector:(CVector*)v;
-(void) divide:(double)s;
-(void) scalar:(double)s;
-(double) dotProductWithVector:(CVector*) v ;
-(void) crossProduct:(CVector*)v resultCrossVector:(CVector*)cross ;
-(double) norm;
-(void) unitVectorWithResultUnitVector:(CVector*)unit ;
-(void) normalize ;
-(void) perp:(CVector*)resultPerpVector ;
-(double) exteriorPerpProduct:(CVector*)v2;
-(void) perp;
-(double) scalarComponent:(CVector*) v;
-(void) swapWithVector:(CVector*)v;
-(double) angleVector:(CVector*)v;
@end

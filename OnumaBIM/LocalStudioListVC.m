//
//  LocalStudioListVC.m
//  ProjectView
//
//  Created by Alfred Man on 1/4/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import "LocalStudioListVC.h"
#import "LocalProjectListVC.h"
#import "ProjectViewAppDelegate.h"
//#import "DataConnect.h"
#import "OPSStudio.h"
@implementation LocalStudioListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

-(NSArray*)arrayOfFoldersInFolder:(NSString*) folder {
    NSError* error=nil;
	NSFileManager *fm = [NSFileManager defaultManager];

//	NSArray* files = [fm directoryContentsAtPath:folder];
	NSArray* files = [fm contentsOfDirectoryAtPath:folder error:&error];    
	NSMutableArray *directoryList = [NSMutableArray arrayWithCapacity:10];
    
	for(NSString *file in files) {
		NSString *path = [folder stringByAppendingPathComponent:file];
		BOOL isDir = NO;
		[fm fileExistsAtPath:path isDirectory:(&isDir)];
		if(isDir) {
			[directoryList addObject:file];
		}
	}
    
	return directoryList;
}

- (void) readStudioListFromLocal{        
    //    int errorCode=0;
        
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    self.studioArray=[[NSMutableArray alloc] init];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; 
    
    documentsDirectory=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",[appDele defUserName]]]; 	    
    NSArray* studioFolder=[self arrayOfFoldersInFolder:documentsDirectory];
    if (studioFolder!=nil){
//        NSMutableArray* _studioArray=[[NSMutableArray alloc] init];
        
//        NSLog(@"0 _studioArray RetainCount: %d",[_studioArray retainCount]);
        [studioArray release];
        studioArray=[[NSMutableArray alloc]init];
//        NSLog(@"Local Studio Alloc 0_StudioArray RetainCount: %d",[studioArray retainCount]);
//        [studioArray release];
//        [_studioArray retain];
//        studioArray=_studioArray;
////        self.studioArray=_studioArray;        
//        
//        NSLog(@"Local Studio Alloc 1_StudioArray RetainCount: %d",[studioArray retainCount]);
//        [studioArray release];
//        
//        NSLog(@"Local Studio Alloc 2_StudioArray RetainCount: %d",[studioArray retainCount]);
//        
//        self.studioArray=_studioArray;
//        self.studioArray=[NSMutableArray arrayWithArray:_studioArray];
//        NSLog(@"Local Studio Alloc 3_StudioArray RetainCount: %d",[studioArray retainCount]);
        
//        [_studioArray release];
        for (NSString* studioFolderStr in studioFolder){
            if ([studioFolderStr isEqualToString:@"0_N_(null)_I_(null)"]){
                [[NSFileManager defaultManager] removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:studioFolderStr] error:nil];
                continue;
            }
            
            
            
            
            bool bStudioHasSchemeFile=false;
            NSString* studioDir=[documentsDirectory stringByAppendingPathComponent:studioFolderStr];
            NSDirectoryEnumerator *studioDirEnum = [[NSFileManager defaultManager] enumeratorAtPath:studioDir];
            NSString *filename;
            while ((filename = [studioDirEnum nextObject] )) {
                if ([filename hasSuffix:@".sqlite"] && ![filename isEqualToString:@"tmpSiteInfo.sqlite"]) {   //change the suffix to what you are looking for                    
                    bStudioHasSchemeFile=true;
                    break;


                }
            }
            if (!bStudioHasSchemeFile){                
                [[NSFileManager defaultManager] removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:studioFolderStr] error:nil];
                continue;
            }
            
            
            
            
            
            
            
            
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            NSString *documentsDirectory = [paths objectAtIndex:0];            
//            //    NSString *bundleRoot = [[NSBundle mainBundle] resourcePath];
//            NSFileManager *manager = [NSFileManager defaultManager];
//            //    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:bundleRoot];            
//            NSString *userDir = [documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/%@",[self defUserName]]   ];
//            //    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:documentsDirectory];
//            NSDirectoryEnumerator *userDirEnum = [manager enumeratorAtPath:userDir];
//            NSString* studioName;
//            while ((studioName = [userDirEnum nextObject])){
//                
//                
//                NSString *studioDir = [userDir stringByAppendingString:[NSString stringWithFormat:@"/%@",studioName]   ];
//                NSDirectoryEnumerator *studioDirEnum = [manager enumeratorAtPath:studioDir];
//                
//                NSString *filename;
//                while ((filename = [studioDirEnum nextObject] )) {
//                    if ([filename hasSuffix:@".sqlite"]) {   //change the suffix to what you are looking for
//                        return true;
//                        // Do work here
//                    }
//                }            
//            }
//            
//            
//            
//            
//            
//            
//            
            
            
            
            
            
            
            
            
            
            NSInteger studioNameStart = [studioFolderStr rangeOfString:@"_N_"].location;
            
            NSInteger studioIconNameStart = [studioFolderStr rangeOfString:@"_I_"].location;            
            uint studioID=[[studioFolderStr substringToIndex:studioNameStart] integerValue ];
            
            NSString* studioName=[[NSString alloc]initWithString: [studioFolderStr substringWithRange:NSMakeRange((studioNameStart+3), (studioIconNameStart-studioNameStart-3))]];
            
            NSString* studioIconName=[[NSString alloc]initWithString: [studioFolderStr substringFromIndex:(studioIconNameStart+3)]];
                   
            OPSStudio* opsStudio=[[OPSStudio alloc] init:studioID name:studioName iconName:studioIconName];
            [studioName release];
            [studioIconName release];            
            [studioArray addObject:opsStudio];
            
//            NSLog(@"Local Studio AddObject 1_StudioArray RetainCount: %d",[studioArray retainCount]);
            [opsStudio release];
            opsStudio=nil;
        }
    }    

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    // Navigation logic may go here. Create and push another view controller.                
    
    int row=[indexPath row];
    OPSStudio* selectedStudio=[tableData objectAtIndex:row];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];

    [appDele setActiveLocalStudio:selectedStudio];
    
    
    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Loading local project list for studio: %@",selectedStudio.name] invoker:self completeAction:@selector(loadLocalProjectTableListVC:) actionParamArray:[NSArray arrayWithObjects:indexPath, nil]];
    
    
    
      
    
}

-(void) loadLocalProjectTableListVC:(NSArray*) paramArray{
    NSIndexPath* indexPath=[paramArray objectAtIndex:0];
    int row=[indexPath row];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];     

    OPSStudio* selectedStudio=[tableData objectAtIndex:row];
    [appDele setActiveLocalStudio:selectedStudio];    

    LocalProjectListVC* localProjectTableListVC=[[LocalProjectListVC alloc] init];
    NSString* localProjectTitleStr=[[NSString alloc] initWithFormat: @"Local projects in Studio: %@", selectedStudio.name ];
    localProjectTableListVC.title=localProjectTitleStr;    
    [localProjectTitleStr release];
    
    
    
    
//    int height = 44;//self.frame.size.height;
//    int width = 800;
//    
//    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
//    navLabel.backgroundColor = [UIColor clearColor];
//    navLabel.textColor = [UIColor blackColor];
//    navLabel.font = [UIFont boldSystemFontOfSize:16];
//    navLabel.textAlignment = UITextAlignmentCenter;
//    navLabel.text=localProjectTableListVC.title;
//    self.navigationItem.titleView = navLabel;
//    [navLabel release];
//    
    
    
    [self.navigationController pushViewController:localProjectTableListVC animated:NO];      

    [localProjectTableListVC release];
    
    

//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];
    


}
- (void)loadView
{
    [super loadView];
    
    int height = 44;//self.frame.size.height;
    int width = 800;
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor blackColor];
    navLabel.font = [UIFont boldSystemFontOfSize:16];
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text=self.title;
    self.navigationItem.titleView = navLabel;
    [navLabel release]; 
    
    
//    [self readStudioListFromLocal];         
//    self.tableData=[studioArray copy];
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    [self readStudioListFromLocal];         
    [tableData release];
    tableData=[studioArray copy];
//    self.tableData=studioArray;
//    [studioArray release];
    
//    NSLog(@"Local Studio ViewWillApear 0_StudioArray RetainCount: %d",[studioArray retainCount]);
    [self.theTableView reloadData];
    
    
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    
//    [[appDele dataConnecter] readStudioListFromServer];         
//    self.tableData=[[[appDele dataConnecter] studioArray] copy];
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



//// Tell the system what we support
- (NSUInteger)supportedInterfaceOrientations {
    //    return UIInterfaceOrientationMaskAllButUpsideDown;
    return UIInterfaceOrientationMaskLandscape;//UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskPortraitUpsideDown;//UIInterfaceOrientationMaskAllButUpsideDown;
}
//
// Tell the system It should autorotate
- (BOOL) shouldAutorotate {
    return YES;
    
}
//-(BOOL) shouldAutorotateToInterfaceOrientation{
//    return YES;
//}

// Tell the system which initial orientation we want to have
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}


//
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    
//    
//    // Return YES for supported orientations
//    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
//            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));    
//    // Return YES for supported orientations
//
//}
//
@end

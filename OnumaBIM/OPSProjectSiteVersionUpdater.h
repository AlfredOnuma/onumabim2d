//
//  OPSProjectSiteVersionUpdater.h
//  ProjectView
//
//  Created by Alfred Man on 8/15/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "OPSProjectSite.h"
@class LocalProjectListVC;
@interface OPSProjectSiteVersionUpdater: OPSProjectSite<NSURLConnectionDelegate>{    
//    NSMutableData* connectionData;
    LocalProjectListVC* _localProjectListVC;
}

@property (nonatomic, retain) LocalProjectListVC* localProjectListVC;
- (void) updateDBFromServer:(NSArray*) paramArray;

-(id) initWithOPSProjectSite:(OPSProjectSite*) projectSite  localProjectListVC:(LocalProjectListVC*) localProjectListVC;

//@property (nonatomic, retain) NSMutableData* connectionData;
@end

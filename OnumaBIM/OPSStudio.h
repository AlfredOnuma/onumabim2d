//
//  OPSStudio.h
//  ProjectView
//
//  Created by Alfred Man on 12/30/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OPSStudio : NSObject {
    uint ID;
    NSString* name;
    NSString* iconName;
}
@property (nonatomic, assign) uint ID;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* iconName;


- (id) init:(uint) _ID name:(NSString*) _name iconName:(NSString*) _iconName;
@end

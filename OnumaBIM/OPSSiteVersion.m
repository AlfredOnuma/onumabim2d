//
//  OPSSiteVersion.m
//  ProjectView
//
//  Created by Alfred Man on 8/15/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "OPSSiteVersion.h"

@implementation OPSSiteVersion
@synthesize version=_version;
@synthesize bStructureUpdated=_bStructureUpdated;
-(id) initWithVersion:(uint)version bStructureUpdated:(bool) bStructureUpdated{
    self=[super init];
    if (self){
        self.version=version;
        self.bStructureUpdated=bStructureUpdated;
    }
    return self;
}
@end

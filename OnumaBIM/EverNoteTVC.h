//
//  EverNoteTVC.h
//  ProjectView
//
//  Created by onuma on 31/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EvernoteSDK.h"
#import "EverNoteNoteBookBrowserTVC.h"

@class ViewModelToolbar;

//
//typedef NS_ENUM(NSInteger, ENSDKSampleType) {
//    ENSDKListNotebooks,
//    ENListSharedNotes,
//    ENCreatePhotoNote,
//    ENCreateReminderNote,
//    ENShowBusinessAPI,
//    ENSaveNewNoteToEvernote,
//    ENInstallEvernoteForiOS,
//    ENViewNoteInEvernote,
//    ENNoteBrowser,
//    ENNotebookChooser,
//    ENLastRow
//};


typedef NS_ENUM(NSInteger, ENSDKSampleType) {
//    ENNotebookChooser,
    ENNoteBrowser,
//    ENNoteBrowserByFilter,
    ENCreatePhotoNote,
    ENInstallEvernoteForiOS,
    ENLastRow
};
@interface EverNoteTVC : UITableViewController <EverNoteNoteBookBrowserTVCDelegate>
{
    ViewModelToolbar* _viewModelToolbar;
    
    NSString* _consoleText;
    NSString* _selectdNotebookGUID;
    UIActivityIndicatorView* _activityIndicatorView;
    NSArray* _notebooks;
    NSString* _skitchNoteBookGUID;

    
}
@property(nonatomic,retain) NSString* skitchNoteBookGUID;
@property(nonatomic,retain) NSString* consoleText;
@property(nonatomic,retain) NSString* selectedNotebookGUID;
@property(nonatomic,retain) UIActivityIndicatorView* activityIndicatorView;
@property(nonatomic,retain) NSArray* notebooks;


@property (nonatomic, assign) ViewModelToolbar* viewModelToolbar;
-(id) initWithViewModelToolbar:(ViewModelToolbar*)viewModelToolbar;

@end

//
//  ViewPlanSiteInfoNVC.h
//  ProjectView
//
//  Created by Alfred Man on 5/11/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ViewModelToolbar;
@class ViewModelVC;
@interface ViewPlanSiteInfoNVC : UINavigationController{
//    ViewModelToolbar* _viewModelToolbar;
    ViewModelVC* _viewModelVC;
}
//@property (nonatomic, assign) ViewModelToolbar* viewModelToolbar;
@property (nonatomic, assign) ViewModelVC* viewModelVC;
//-(id) initWithRootViewController:(UIViewController *)rootViewController viewModelToolbar:(ViewModelToolbar*) viewModelToolbar;
-(id) initWithRootViewController:(UIViewController *)rootViewController viewModelVC:(ViewModelVC*) viewModelVC;

@end

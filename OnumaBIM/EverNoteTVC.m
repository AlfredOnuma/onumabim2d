//
//  EverNoteTVC.m
//  ProjectView
//
//  Created by onuma on 31/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "EverNoteTVC.h"
#import "ViewModelToolbar.h"
#import "ViewModelVC.h"
#import "EvernoteSDK.h"
#import "EverNoteNoteBrowserVC.h"
#import "EverNoteNoteBookBrowserTVC.h"
#import "ProjectViewAppDelegate.h"
#import "TestT.h"
#import "NSData+EvernoteSDK.h"
#import "OPSProjectSite.h"
@interface EverNoteTVC ()

@end

@implementation EverNoteTVC
@synthesize viewModelToolbar=_viewModelToolbar;

@synthesize consoleText=_consoleText;
@synthesize selectedNotebookGUID=_selectdNotebookGUID;
@synthesize activityIndicatorView=_activityIndicatorView;
@synthesize notebooks=notebooks;
@synthesize skitchNoteBookGUID=_skitchNoteBookGUID;

-(void) dealloc{
    [_consoleText release];_consoleText=nil;
    [_selectdNotebookGUID release]; _selectdNotebookGUID=nil;
    [_activityIndicatorView release];_activityIndicatorView=nil;
    [_notebooks release];_notebooks=nil;
    [_skitchNoteBookGUID release];_skitchNoteBookGUID=nil;
    [super dealloc];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"EverNoteTableCell"];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return ENLastRow;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"EverNoteTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
//    
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    switch (indexPath.row) {
//        case ENNotebookChooser:
//            [[cell textLabel] setText:@"Notebook Chooser"];
//            break;
        case ENNoteBrowser:
            [[cell textLabel] setText:@"Bind EverNote to Site"];
            break;
//        case ENNoteBrowserByFilter:
//            [[cell textLabel] setText:@"Browser Linked Note"];
//            break;
        case ENCreatePhotoNote:
            [[cell textLabel] setText:@"Save ScreenShot to Evernote"];
            break;
        case ENInstallEvernoteForiOS:
            [[cell textLabel] setText:@"Install Evernote for iOS"];
            break;
        default:
            break;
    }
    [[cell textLabel] setNumberOfLines:0];
    [[cell textLabel] sizeToFit];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



- (void)notebookChooserController:(EverNoteNoteBookBrowserTVC *)controller didSelectNotebook:(EDAMNotebook *)notebook {
    self.selectedNotebookGUID = notebook.guid;
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notebook chosen" message:notebook.name delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark - Table view delegate
- (void)invokeNotebookChooser{
    EverNoteNoteBookBrowserTVC *nbc = [[EverNoteNoteBookBrowserTVC alloc] init];
    [nbc setDelegate:self];
    [nbc setSelectedNotebookWithGUID:self.selectedNotebookGUID];

    UINavigationController* notbookChooserNav = [[UINavigationController alloc] initWithRootViewController:nbc];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        nbc.modalPresentationStyle = UIModalPresentationFormSheet;
        notbookChooserNav.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    [[self navigationController] presentViewController:notbookChooserNav animated:YES completion:^{
        [nbc.navigationItem setTitle:@"Select a notebook"];
    }];
}



//
//-(void) addEverNoteNoteBrowserButtonToNavBar:(UIViewController*)vc{
//    
//    NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:3];
//    
//    UIImage *buttonImage=nil;
//    UIImage *buttonImageHighlight=nil;
//    UIButton *button=nil;
//    UIBarButtonItem *bi=nil;
//    
//    
//    buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MultiSelectDown.png" ofType:nil]];
//    buttonImageHighlight =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MultiSelectDown.png" ofType:nil]];
//    button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button setImage:buttonImage forState:UIControlStateNormal];
//    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
//    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//    [button addTarget:self action:@selector(addHelpViewController:) forControlEvents:UIControlEventTouchUpInside];
//    bi = [[UIBarButtonItem alloc] initWithCustomView:button];
//    [buttons addObject:bi];
//    
//    [bi release];
//    
//    
//    vc.navigationItem.rightBarButtonItems=buttons;
//    
//    [buttons release];
//    
//    
//    //    NSMutableArray* leftButtons = [[NSMutableArray alloc] initWithCapacity:1];
//    //
//    //    buttonImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ProjectListsButton.png" ofType:nil]];//[UIImage imageNamed:@"ProjectListsButton.png"];
//    //    buttonImageHighlight = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ProjectListsButton.png" ofType:nil]];;//[UIImage imageNamed:@"ProjectListsButton.png"];
//    //    button = [UIButton buttonWithType:UIButtonTypeCustom];
//    //    [button setImage:buttonImage forState:UIControlStateNormal];
//    //    [button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
//    //    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//    //    [button addTarget:self action:@selector(backToProjectButtonRespond:) forControlEvents:UIControlEventTouchUpInside];
//    //    bi = [[UIBarButtonItem alloc] initWithCustomView:button];
//    //    //        [button release];
//    //    [leftButtons addObject:bi];
//    //    [bi release];
//    //
//    //    self.navigationItem.leftBarButtonItems = leftButtons;
//    //    [leftButtons release];
//    //    
//}


- (void)invokeNoteBrowser:(bool) bLoadLinkedNoteOnly {
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    
//    [appDele displayEverNoteNoteBrowser];
//
//

    EverNoteNoteBrowserVC* nbvc=[[EverNoteNoteBrowserVC alloc]initWithNibName:@"EverNoteNoteBrowserVC" bundle:nil noteBookGUID:self.selectedNotebookGUID bLoadLinkedNoteOnly:bLoadLinkedNoteOnly];
////    UINavigationController* notbookBrowserNVC = [[UINavigationController alloc] initWithRootViewController:nbvc]; 
////    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
//    
//    
//    [self addEverNoteNoteBrowserButtonToNavBar:nbvc];
    
    
    
    
    [[[self viewModelToolbar] parentViewModelVC].navigationController pushViewController:nbvc animated:YES];
    [nbvc release];
    
//    [
//    [[appDele viewModelNavCntr] pushViewController:arOverlayViewController animated:NO];
//    [appDele viewModelNavCntr].navigationBar.translucent=YES;
//    
//    [appDele viewModelNavCntr].navigationBar.alpha=0.7;
//    [arOverlayViewController release];
//    
    
    
    
//    
//    EverNoteNoteBrowserVC* nbvc=[[EverNoteNoteBrowserVC alloc]initWithNibName:@"EverNoteNoteBrowserVC" bundle:nil];
//    
////    testT* nbvc=[[testT alloc]initWithNibName:@"testT" bundle:nil];
//    
////
////    [nbvc setDelegate:self];
////    [nbvc setSelectedNotebookWithGUID:self.selectedNotebookGUID];
//    UINavigationController* notbookBrowserNVC = [[UINavigationController alloc] initWithRootViewController:nbvc];
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        nbvc.modalPresentationStyle = UIModalPresentationFormSheet;
//        notbookBrowserNVC.modalPresentationStyle = UIModalPresentationFormSheet;
//    }
//    [[self navigationController] presentViewController:notbookBrowserNVC animated:YES completion:^{
//        [nbvc.navigationItem setTitle:@"Browsing Note"];
//    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case ENCreatePhotoNote:
            [self createPhotoNote];
            break;


        case ENInstallEvernoteForiOS:
            [self installEvernote];
            break;

        case ENNoteBrowser:
            [self invokeNoteBrowser:NO];
            break;
//        case ENNoteBrowserByFilter:
//            [self invokeNoteBrowser:YES];
//            break;
//        case ENNotebookChooser:
//            [self invokeNotebookChooser];
//            break;
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[[self viewModelToolbar] popoverController] dismissPopoverAnimated:NO];
}


-(id) initWithViewModelToolbar:(ViewModelToolbar*)viewModelToolbar{
    self=[super init];
    if (self){
        self.viewModelToolbar=viewModelToolbar;
    }
    return self;
}



- (IBAction)createPhotoNote {
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    NSString* overlayText=@"Saving Screenshot to EverNote";
    [appDele displayLoadingViewOverlay:overlayText invoker:self completeAction:@selector(saveScreenShotToEverNote) actionParamArray:nil];// [NSArray arrayWithObject:rootSpatialStruct]
}
-(void) saveScreenShotToEverNote{
    
    ViewModelVC* viewModelVC=[self.viewModelToolbar parentViewModelVC];
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
//    CGRect appDeleContextRect=[appDele window].bounds;
//    CGRect viewModelVCRect=[[viewModelVC view] bounds];
//    CGFloat scale=[UIScreen mainScreen].scale;
//    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]){
////        UIGraphicsBeginImageContextWithOptions(self.window.bounds.size, NO, [UIScreen mainScreen].scale);
//        UIGraphicsBeginImageContextWithOptions(appDele.window.bounds.size, NO, [UIScreen mainScreen].scale);
//    }else{
////        UIGraphicsBeginImageContext(self.window.bounds.size);
//        UIGraphicsBeginImageContext(appDele.window.bounds.size);
//    }

    
    
//    UIGraphicsBeginImageContextWithOptions(appDele.window.bounds.size, NO, 0.0);
//    [appDele.window.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIGraphicsBeginImageContextWithOptions([viewModelVC navigationController].view.bounds.size, NO, 0.0);
    [[viewModelVC navigationController].view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    
    
//    [self.window.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData * data = UIImagePNGRepresentation(image);
    
    
//    UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:data], nil, nil, nil);
    
    
    OPSProjectSite* projectSite=appDele.activeProjectSite;
    NSString* folderPath=[projectSite.dbPath stringByDeletingLastPathComponent];
    NSString* capturePath=[folderPath stringByAppendingPathComponent:@"ScreenShot.png"];
    bool captureSuccess=[data writeToFile:capturePath atomically:YES];
    
    if (captureSuccess){
        NSLog (@"Screen Capture Success");
    }else{
        NSLog (@"Screen Capture Fail");
    }
    
    if (!captureSuccess){
        return;
        
    }
    
//    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"1024 Logo no border.001" ofType:@"png"];
//    NSString* filePath = [[NSBundle mainBundle] pathForResource:capturePath ofType:@"png"];
  

    [[EvernoteNoteStore noteStore] listNotebooksWithSuccess:^(NSArray *listNoteBooks) {
        for (EDAMNotebook *notebook in listNoteBooks){
            if ([[notebook name] isEqualToString:@"Skitch"]){
//                self.skitchNoteBookGUID=[NSString stringWithString: notebook.guid];
                [self exportToEverNoteWithScreenCapturePath:capturePath skitchNoteBookGUID:notebook.guid];
                return;
            }
        }
        [self exportToEverNoteWithScreenCapturePath:capturePath skitchNoteBookGUID:nil];

    } failure:^(NSError *error) {
        [self exportToEverNoteWithScreenCapturePath:capturePath skitchNoteBookGUID:nil];
        
    }];
    
}

- (void) exportToEverNoteWithScreenCapturePath:(NSString*)capturePath skitchNoteBookGUID:(NSString*) skitchNoteBookGUID{    
    NSData *myFileData = [NSData dataWithContentsOfFile:capturePath];
    NSData *dataHash = [myFileData enmd5];
    EDAMData *edamData = [[EDAMData alloc] initWithBodyHash:dataHash size:myFileData.length body:myFileData];
    EDAMResource* resource = [[EDAMResource alloc] initWithGuid:nil noteGuid:nil data:edamData mime:@"image/png" width:0 height:0 duration:0 active:0 recognition:0 attributes:nil updateSequenceNum:0 alternateData:nil];
    [edamData release];
    ENMLWriter* myWriter = [[ENMLWriter alloc] init];
    [myWriter startDocument];
    [myWriter startElement:@"span"];
    [myWriter startElement:@"br"];
    [myWriter endElement];
    [myWriter writeResource:resource];
    [myWriter endElement];
    [myWriter endDocument];
    NSString *noteContent = myWriter.contents;
    NSMutableArray* resources = [NSMutableArray arrayWithArray:@[resource]];
    
    [resource release];
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
    EDAMNote *newNote = [[EDAMNote alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd 'at' HH:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:162000];
    
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    NSLog(@"formattedDateString: %@", formattedDateString);
    
    
    [dateFormatter release];dateFormatter=nil;
    
    NSString* title=[NSString stringWithFormat:@"ScreenShot From OnumaBIM by %@ on %@",[appDele defUserName],formattedDateString ];
    [newNote setTitle:title];
    
    [newNote setContent:noteContent];
    [newNote setContentLength:noteContent.length];
    [newNote setResources:resources];
    if (skitchNoteBookGUID && ![skitchNoteBookGUID isEqualToString:@""]){
        [newNote setNotebookGuid:skitchNoteBookGUID];
    }
    [[EvernoteNoteStore noteStore] setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Total bytes written : %lld , Total bytes expected to be written : %lld",totalBytesWritten,totalBytesExpectedToWrite);
    }];
    NSLog(@"Contents : %@",myWriter.contents);
    [myWriter release];
    [self.activityIndicatorView startAnimating];
    [[EvernoteNoteStore noteStore] createNote:newNote success:^(EDAMNote *note) {
        [self.activityIndicatorView stopAnimating];
        NSLog(@"Note created successfully.");
        
        
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDele removeLoadingViewOverlay];
    } failure:^(NSError *error) {
        NSLog(@"Error creating note : %@",error);
        [self.activityIndicatorView stopAnimating];
        
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDele removeLoadingViewOverlay];
    }];

}
- (IBAction)installEvernote {
    [[EvernoteSession sharedSession] installEvernoteAppUsingViewController:self];
}
@end

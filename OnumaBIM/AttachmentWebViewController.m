//
//  AttachmentWebViewController.m
//  ProjectView
//
//  Created by Alfred Man on 8/25/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "AttachmentWebViewController.h"
#import "AttachmentWebView.h"
#import "ProjectViewAppDelegate.h"
@interface AttachmentWebViewController ()

@end

@implementation AttachmentWebViewController
@synthesize webView=_webView;
@synthesize toolbar=_toolbar;
@synthesize initURLStr=_initURLStr;
@synthesize hasNavControlBar=_hasNavControlBar;
-(void) dealloc{
    [_webView release];_webView=nil;
    [_initURLStr release];_initURLStr=nil;
    [_toolbar release];_toolbar=nil;
//    [_attachmentWebView release];_attachmentWebView=nil;
    [super dealloc];
}


//
//CGRect webFrame = self.viewModelNavCntr.view.frame;// CGRectMake(0.0, 0.0, 320.0, 460.0);
//AttachmentWebView *webView = [[AttachmentWebView alloc] initWithFrame:webFrame];
//[webView setBackgroundColor:[UIColor clearColor]];
//
//
//NSURL *url = [NSURL URLWithString:urlAddress];
//NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//[webView loadRequest:requestObj];
//
//
//
//UIViewController* webViewController=[[UIViewController alloc] init];
//[webViewController setView:webView];
//[webView release];
//
//[self.viewModelNavCntr pushViewController:webViewController animated:YES]; 
//[webViewController release];
//
//
-(void) refresh:(id) sender{
    
    NSURL *url = [NSURL URLWithString:self.initURLStr];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:requestObj];
    return;
}
-(id) initWithFrame:(CGRect)frame initURL:(NSString*)initURLStr hasNavControlBar:(bool)hasNavControlBar{
    self=[super init];
    if (self){
        self.hasNavControlBar=hasNavControlBar;
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [UIApplication sharedApplication].delegate;
        self.initURLStr=initURLStr;
        
//        
//        uint width= [self parentViewController].view.bounds.size.width;//  1024;//frame.size.width; //tmp added here
//        uint webViewHeight=[self parentViewController].view.bounds.size.height-44-20;//704;//frame.size.height-44-20;
        
        
        uint width= frame.size.width;//  1024;//frame.size.width; //tmp added here
        uint webViewHeight=frame.size.height-44-20;//704;//frame.size.height-44-20;
        
//        CGRect webToolbarFrame=CGRectMake(0, frame.size.height-44, 1024, 44);
//        CGRect webViewFrame=CGRectMake(0,0,1024,frame.size.height-44);

//        CGRect webToolbarFrame=CGRectMake(0, frame.size.height-44, 1024, 44);
//        CGRect webViewFrame=CGRectMake(0,0,1024,frame.size.height-44);
//        uint t=frame.size.height;
        
        /*
        uint height=768;
        uint width=1024;
        UIDeviceOrientation orientation=[[UIApplication sharedApplication] statusBarOrientation];
        if (orientation==UIDeviceOrientationLandscapeLeft){
//            [loadingOverlay setTransform:CGAffineTransformMakeRotation(M_PI_2)];
//            loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);          
            
            if ([appDele viewModelNavCntr]){
                height=frame.size.width;
                width=frame.size.height;
            }else{
                height=frame.size.height;
                width=frame.size.width;
            }
        }else if (orientation==UIDeviceOrientationLandscapeRight){
//            [loadingOverlay setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
//            loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);
            if ([appDele viewModelNavCntr]){
                height=frame.size.width;
                width=frame.size.height;                
            }else{
                height=frame.size.height;
                width=frame.size.width;
            }
        }else {        
//            [loadingOverlay setTransform:CGAffineTransformMakeRotation(M_PI_2)];
//            loadingOverlay.bounds=CGRectMake(0, 0, 1024, 768);     
            
            if ([appDele viewModelNavCntr]){
                height=frame.size.height;
                width=frame.size.width;
            }else{
                height=frame.size.width;
                width=frame.size.height;
            }
        }
        uint frameHeight=height-20-44;
        if ([appDele viewModelNavCntr]){

//        CGRect webToolbarFrame=CGRectMake(0,frameHeight-44, 1024, 44);
        
        }else{
            frameHeight=height;
        }
        CGRect webViewFrame=CGRectMake(0,0,width,frameHeight);
        
        
        
        
        AttachmentWebView* webView=[[AttachmentWebView alloc] initWithFrame:webViewFrame initURLStr:initURLStr];
        */
        


        
        AttachmentWebView* webView=[[AttachmentWebView alloc] initWithFrame:CGRectMake(0,0,width,webViewHeight) initURLStr:initURLStr];
        self.webView=webView;                
        [webView release];
        
        

        

        
        UIView* viewCanvas=[[UIView alloc] initWithFrame:frame];
        [self setView:viewCanvas];
        [viewCanvas release];
        
        [self.view addSubview:self.webView];
        
        
        
//        [self.view addSubview:self.toolbar];
        
        
        
        
        
        
        
        
        
        if (self.hasNavControlBar){
        
            
            
            if ([appDele viewModelNavCntr]){  
                
                UIBarButtonItem* bi=nil;                        
                NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:3];        bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:webView action:@selector(reload)];                 
                [buttons addObject:bi];
                [bi release];         
                
                bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFastForward target:webView action:@selector(goForward)];                 
                [buttons addObject:bi];
                [bi release];
                
                bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRewind target:webView action:@selector(goBack)];                 
                [buttons addObject:bi];
                [bi release];
                self.navigationItem.rightBarButtonItems=buttons;
                
                [buttons release];
            }else{
                
                
                
                
                
                UIToolbar* toolbar=[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, width, 44)];
                self.toolbar=toolbar;
                [toolbar release];

                
                
                NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];                        
                UIBarButtonItem* bi=nil;   
                
                
                bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRewind target:webView action:@selector(goBack)];                 
                [buttons addObject:bi];
                [bi release];
                                                         
                bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFastForward target:webView action:@selector(goForward)];                 
                [buttons addObject:bi];
                [bi release];
                
    //            bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reload)];
    //            bi.style = UIBarButtonItemStyleBordered;
    //            [buttons addObject:bi];
    //            [bi release];   

                
                
                [self.toolbar setItems:buttons];
                
                [buttons release];            
                
                [self.webView addSubview:toolbar];
            }
            
            
        }
        
        
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) viewWillAppear:(BOOL)animated{
    [self refresh:self];
    [super viewWillAppear:animated];
}
- (void)viewDidLoad
{    
    [super viewDidLoad];    
//    [self refresh:self];
    
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}




- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
}




//// Tell the system what we support
- (NSUInteger)supportedInterfaceOrientations {
    //    return UIInterfaceOrientationMaskAllButUpsideDown;
    return UIInterfaceOrientationMaskLandscapeLeft;//UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskPortraitUpsideDown;//UIInterfaceOrientationMaskAllButUpsideDown;
    //    return UIInterfaceOrientationMaskPortrait;
}
//
// Tell the system It should autorotate
- (BOOL) shouldAutorotate {
    return YES;
    
}
// Tell the system which initial orientation we want to have
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}


@end

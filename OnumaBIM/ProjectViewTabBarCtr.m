//
//  ProjectViewTabBarCtr.m
//  ProjectView
//
//  Created by Alfred Man on 11/21/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "ProjectViewTabBarCtr.h"

#import "ProjectViewAppDelegate.h"
#import "LocalProjectNavCntr.h"
//#import "LocalProjectListTableVC.h"
#import "LiveProjectNavCntr.h"
#import "LocalStudioListVC.h"
//#import "LiveProjectListTableVC.h"

#import "LiveStudioListVC.h"
//#import "LiveStudioListTableVC.h"
//#import "StudioListTableCell.h"
@implementation ProjectViewTabBarCtr

// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[LocalProjectNavCntr class]]){
//        [((LocalProjectNavCntr*) viewController)   ]
//        int y=0;
//        y=2;
    }
    if ([viewController isKindOfClass:[LiveProjectNavCntr class]]){
        //        [((LocalProjectNavCntr*) viewController)   ]
//        int v=0;
//        v=2;
    }    
//    uint x=0;
//    x=1;
}

// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}



-(id) init{
    
    self=[super init];
    if (self){
        ProjectViewAppDelegate* appDele= (ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
        NSMutableArray* projectViewControllersArray = [[NSMutableArray alloc] initWithCapacity:2];    
        
        
        
        
//        LocalProjectListTableVC* localProjectListTableVC=[[LocalProjectListTableVC alloc] init ];
//        NSString* title=[NSString stringWithFormat:@"Local Project List"];//"Site: %@",[[model root] name] ];    
//        localProjectListTableVC.title=title;        
//        LocalProjectNavCntr *localProjectNavCntr =  [[LocalProjectNavCntr alloc] init];            
//        [localProjectNavCntr pushViewController:localProjectListTableVC animated:NO];        
//        localProjectNavCntr.tabBarItem.title=@"Onuma Local";     //This Must Come AFTER THE PUSH!!!
//        [localProjectListTableVC release];        
//        
//        [projectViewControllersArray addObject:localProjectNavCntr];    
//        [localProjectNavCntr release];
  
     
        LocalProjectNavCntr *localProjectNavCntr =[[LocalProjectNavCntr alloc] init];  
        LocalStudioListVC* localStudioListVC=[[LocalStudioListVC alloc] init];        
        localStudioListVC.title=@"Local Studios";    
        [localProjectNavCntr pushViewController:localStudioListVC animated:NO];      
        localProjectNavCntr.tabBarItem.title=@"Onuma Local";
        UIImage* localImage=[UIImage imageNamed:@"OnumaLocalTab.png"];        
        localProjectNavCntr.tabBarItem.image=localImage;
        [localStudioListVC release];
        
        [projectViewControllersArray addObject:localProjectNavCntr];
        [localProjectNavCntr release];        
                        
        
        LiveProjectNavCntr *liveProjectNavCntr =[[LiveProjectNavCntr alloc] init];  
        LiveStudioListVC* liveStudioListVC=[[LiveStudioListVC alloc] init];        
        liveStudioListVC.title=@"Live Studios";  
        
        
        
//        int height = 44;//self.frame.size.height;
//        int width = 800;
//        
//        UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
//        navLabel.backgroundColor = [UIColor clearColor];
//        navLabel.textColor = [UIColor blackColor];
//        navLabel.font = [UIFont boldSystemFontOfSize:16];
//        navLabel.textAlignment = UITextAlignmentCenter;
//        navLabel.text=@"Live Studio list";//liveStudioListVC.title;
//        liveProjectNavCntr.navigationController.navigationItem.titleView = navLabel;
//        [navLabel release];
//        
        
        [liveProjectNavCntr pushViewController:liveStudioListVC animated:NO];      
        liveProjectNavCntr.tabBarItem.title=@"Onuma Live";
        
        UIImage* liveImage=[UIImage imageNamed:@"OnumaLiveTab.png"];        
        liveProjectNavCntr.tabBarItem.image=liveImage;
        [liveStudioListVC release];
        
        [projectViewControllersArray addObject:liveProjectNavCntr];
        [liveProjectNavCntr release];
        
        /*
         17MAY
         
         155 GBP
         AUG FEB
         
         
         003770
         
         10 AUGUST
         SEPTEMBER
         0194
         27 FEB
         
         
         
         ELECT
         72577
         02256
         
         JAN 380.6
         
         17 MAY 
         
         18 MONTH
         
         
         08000480707 BILLING TEAM
         
         43-33=10
            
         */
         

//        LiveProjectNavCntr *liveProjectNavCntr =[[LiveProjectNavCntr alloc] init];    
//        LiveProjectListTableVC* liveProjectListTableVC=[[LiveProjectListTableVC alloc] init];
//        liveProjectListTableVC.title=title;    
//        [liveProjectNavCntr pushViewController:liveProjectListTableVC animated:NO];      
//        liveProjectNavCntr.tabBarItem.title=@"Live Project";
//        [liveProjectListTableVC release];
//        
//        [projectViewControllersArray addObject:liveProjectNavCntr];
//        [liveProjectNavCntr release];
        
        
        
        self.viewControllers = projectViewControllersArray;    
        [projectViewControllersArray release];

        
        
        
        
        
        
        if ([appDele containLocalDatabase]){
            //If there exist at least one local project, goto local project view
            [self setSelectedIndex:0];
        }else{
            //If not evensa single local project exist, goto live project view to download first
            [self setSelectedIndex:1];       
        }   
    }
    return self;
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{

    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//// Tell the system what we support
//- (NSUInteger)supportedInterfaceOrientations {
//    //    return UIInterfaceOrientationMaskAllButUpsideDown;
//    return UIInterfaceOrientationMaskLandscapeRight;//UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskPortraitUpsideDown;//UIInterfaceOrientationMaskAllButUpsideDown;
////    return UIInterfaceOrientationMaskPortrait;
//}
//
// Tell the system It should autorotate
//- (BOOL) shouldAutorotate {
//        return YES;
//    
//}
//// Tell the system which initial orientation we want to have
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationLandscapeRight;
//} 

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
}

@end

//
//  ViewPlanSitePolyInfoVC.m
//  ProjectView
//
//  Created by onuma on 21/05/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import "ViewPlanSitePolyInfoVC.h"

@interface ViewPlanSitePolyInfoVC ()

@end

@implementation ViewPlanSitePolyInfoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

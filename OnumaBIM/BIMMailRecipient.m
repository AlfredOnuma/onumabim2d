//
//  BIMMailRecipient.m
//  ProjectView
//
//  Created by Alfred Man on 4/7/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "BIMMailRecipient.h"

@implementation BIMMailRecipient
@synthesize ID;
@synthesize name;
@synthesize role;
@synthesize email;
@synthesize checked;

-(void)dealloc{
    [name release];name=nil;
    [role release];role=nil;
    [email release];email=nil;
    [super dealloc];
}

-initWithID:(uint) _ID name:(NSString*) _name role:(NSString*)_role email:(NSString*)_email checked:(bool)_checked{
    self=[super init];
    if (self){
        self.ID=_ID;
        self.name=_name;
        self.role=_role;
        self.email=_email;
        self.checked=_checked;
    }
    return self;
}

@end

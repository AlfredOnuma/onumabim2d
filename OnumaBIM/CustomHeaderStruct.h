//
//  CustomHeaderStruct.h
//  ProjectView
//
//  Created by Alfred Man on 5/22/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//
#import "StructForBIMPlanColor.h"
#import <Foundation/Foundation.h>

#import "sqlite3.h"
@interface CustomHeaderStruct : StructForBIMPlanColor

{
    uint siteID;
    NSMutableArray* aCustomBldg;
    NSMutableArray* aCustomSpace;    
}
@property (nonatomic, assign) uint siteID;
@property (nonatomic, retain) NSMutableArray* aCustomBldg;
@property (nonatomic, retain) NSMutableArray* aCustomSpace;

-(id) initWithDatabase:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt;
@end

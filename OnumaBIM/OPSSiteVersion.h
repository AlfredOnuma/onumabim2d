//
//  OPSSiteVersion.h
//  ProjectView
//
//  Created by Alfred Man on 8/15/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OPSSiteVersion : NSObject

{
    uint _version;
    bool _bStructureUpdated;
}

@property (nonatomic, assign) uint version;
@property (nonatomic, assign) bool bStructureUpdated;


-(id) initWithVersion:(uint)version bStructureUpdated:(bool) bStructureUpdated;

@end

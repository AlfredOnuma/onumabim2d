//
//  EverNoteNoteBrowserVC.h
//  ProjectView
//
//  Created by onuma on 31/08/2013.
//  Copyright (c) 2013 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sqlite3.h"
@class EDAMNotebook;

@interface EverNoteNoteBrowserVC : UIViewController <UIPopoverControllerDelegate>{
    UIPopoverController* _popOverController;
    
    NSArray* _notebooks;
    EDAMNotebook* _selectedNotebook;

    UIBarButtonItem* _btnPrev;
    UIBarButtonItem* _btnNext;
    UIWebView* _webView;
    bool _bFilteredNoteOnly;
    NSInteger _currentNote;
    UIActivityIndicatorView* _activityIndicator;
    NSMutableArray* _notelist;
    NSString* _notebookGUID;
    
    uint _buttonID_linkButton;
    uint _buttonID_linkFilter;
    uint _buttonID_noteList;
    uint _buttonID_bookList;
    
}

@property (nonatomic, retain) EDAMNotebook* selectedNotebook;
@property (nonatomic, retain) UIPopoverController* popOverController;
@property (nonatomic, assign) uint buttonID_linkButton;
@property (nonatomic, assign) uint buttonID_linkFilter;
@property (nonatomic, assign) uint buttonID_noteList;
@property (nonatomic, assign) uint buttonID_bookList;
@property (nonatomic, assign) bool bFilteredNoteOnly;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *btnPrev;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *btnNext;
@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, retain) NSString* notebookGUID;
@property (nonatomic,assign) NSInteger currentNote;
@property (nonatomic,retain) UIActivityIndicatorView* activityIndicator;
@property (nonatomic,retain) NSMutableArray* noteList;

- (IBAction)nextNote:(id)sender;
- (IBAction)previousNote:(id)sender;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil noteBookGUID:(NSString*)notebookGUID bFilteredNoteOnly:(bool)bFilteredNoteOnly;


//-(bool) isCurrentNoteLinked;
-(void) setLinkButtonIcon:(BOOL)isLinked;

- (void) removeENNoteFromDB: (sqlite3*)database noteGUID:(NSString*)noteGUID notebookGUID:(NSString*)notebookGUID;

- (void) insertENNoteToDB: (sqlite3*)database noteGUID:(NSString*)noteGUID notebookGUID:(NSString*)notebookGUID content:(NSString*)content;
@end

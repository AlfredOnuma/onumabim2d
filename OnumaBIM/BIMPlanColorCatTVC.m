//
//  BIMPlanColorCatTVC.m
//  ProjectView
//
//  Created by Alfred Man on 4/14/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "BIMPlanColorCatNVC.h"
#import "ViewModelToolbar.h"
#import "ViewModelVC.h"
#import "ViewProductRep.h"
#import "Space.h"
#import "DisplayModelUI.h"
#import "AlignColor.h"
#import "ProjectViewAppDelegate.h"
#import "Site.h"
#import "CustomColorStruct.h"
#import "Floor.h"

#import "AlignColorTVC.h"
#import "BIMPlanColorCatTVC.h"
#import "DepartmentColorTVC.h"
#import "CustomColorTVC.h"
#import "ConstructionStatusTVC.h"



#import "FusionBldgStatusTVC.h"
#import "FusionBldgFCITVC.h"
#import "FusionBldgStatusFlagTVC.h"

#import "FusionPlannedSFAssignedSFTVC.h"
#import "FusionSpaceStatusFlagTVC.h"
#import "FusionRecordStatusTVC.h"
#import "FusionTopTVC.h"
#import "FusionPGMTVC.h"
#import "FusionRoomUseTVC.h"

#import "Bldg.h"
@implementation BIMPlanColorCatTVC


@synthesize aColorCatTypeForRow=_aColorCatTypeForRow;


-(void)dealloc{
    [_aColorCatTypeForRow release];_aColorCatTypeForRow=nil;
    [super dealloc];
}
//-(uint) numColorCatOnFloorLevel{
//    uint sum=3;//No Color, Departments, Vertical Alignment
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    Site* site=appDele.currentSite;
//    
//    if (site.fusionPlannedSFAssignedSFDictionary && [site.fusionPlannedSFAssignedSFDictionary count]>0){
//        sum++;
//    }
//    if (site.fusionSpaceStatusFlagDictionary && [site.fusionSpaceStatusFlagDictionary count]>0){
//        sum++;
//    }
//    if (site.fusionRecordStatusDictionary && [site.fusionRecordStatusDictionary count]>0){
//        sum++;
//    }
//    if (site.fusionTopDictionary && [site.fusionTopDictionary count]>0){
//        sum++;
//    }
//    if (site.fusionPGMDictionary && [site.fusionPGMDictionary count]>0){
//        sum++;
//    }
//    if (site.fusionRoomUseDictionary && [site.fusionRoomUseDictionary count]>0){
//        sum++;
//    }
//    NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
//    //    uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
//    
//    uint numCustColorStruct=[aCustomColorStruct count];
//    return sum+numCustColorStruct;
//
//}
//-(uint) numColorCatOnSiteLevel{
//    
//    uint sum=2;//No Color,Construction Status
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    Site* site=appDele.currentSite;
//    
//    if (site.fusionBldgStatusDictionary && [site.fusionBldgStatusDictionary count]>0){
//        sum++;
//    }
//    if (site.fusionBldgFCIDictionary && [site.fusionBldgFCIDictionary count]>0){
//        sum++;
//    }
//    if (site.fusionRecordStatusDictionary && [site.fusionRecordStatusDictionary count]>0){
//        sum++;
//    }
//    if (site.fusionBldgStatusFlagDictionary && [site.fusionBldgStatusFlagDictionary count]>0){
//        sum++;
//    }
//
//    NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
//    //    uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
//    
//    uint numCustColorStruct=[aCustomColorStruct count];
//    return sum+numCustColorStruct;
//}
- (id) init{// (ViewModelToolbar*) viewModelToolbar{
    
    self=[super init];
    if (self){
        [self initColorCatForRow];
//        self.viewModelToolbar=viewModelToolbar;
//        self.delegate=self;
    }else{
    }
    return self;    
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}
-(void) initColorCatForRow{
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    Site* site=appDele.currentSite;
    NSMutableArray* aColorCatTypeForRow=[[NSMutableArray alloc] init];
    self.aColorCatTypeForRow=aColorCatTypeForRow;
    [aColorCatTypeForRow release];
    
    
    
    if ([appDele modelDisplayLevel]==0){
        [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:sitePlan_NONE] ];
        [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:sitePlan_CONSTRUCTION_STATUS]];
        
        if ([[appDele activeModel] bHasFusionSiteData]){
            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:sitePlan_FUSION_BLDG_STATUS ]];
            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:sitePlan_FUSION_BLDG_FCI ]];
            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:sitePlan_FUSION_BLDG_STATUS_FLAG] ];
        }
//        if (site.fusionBldgStatusDictionary && [site.fusionBldgStatusDictionary count]>0){
//        }
//        if (site.fusionBldgFCIDictionary && [site.fusionBldgFCIDictionary count]>0){
//        }
//        
//        if (site.fusionBldgStatusFlagDictionary && [site.fusionBldgStatusFlagDictionary count]>0){
//        }
        
    }else if ([appDele modelDisplayLevel]==1){

        [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_NONE]];
        [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_DEPARTMENT]];
        [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_VERTICAL_ALIGNMENT]];

        if ([appDele activeModel].bHasFusionFloorData){
            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_PLANNEDSFASSIGNEDSF]];
            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_SPACE_STATUS_FLAG]];
            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_RECORDSTATUS]];
            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_TOP]];
            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_PGM]];
            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_ROOM_USE]];

        }
//        if (site.fusionPlannedSFAssignedSFDictionary && [site.fusionPlannedSFAssignedSFDictionary count]>0){
//            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_PLANNEDSFASSIGNEDSF]];
//        }
//        if (site.fusionSpaceStatusFlagDictionary && [site.fusionSpaceStatusFlagDictionary count]>0){
//            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_SPACE_STATUS_FLAG]];
//        }
//        if (site.fusionRecordStatusDictionary && [site.fusionRecordStatusDictionary count]>0){
//            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_RECORDSTATUS]];
//        }
//        if (site.fusionTopDictionary && [site.fusionTopDictionary count]>0){
//            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_TOP]];
//        }
//        if (site.fusionPGMDictionary && [site.fusionPGMDictionary count]>0){
//            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_PGM]];
//        }
//        if (site.fusionRoomUseDictionary && [site.fusionRoomUseDictionary count]>0){
//            [self.aColorCatTypeForRow addObject:[NSNumber numberWithInt:floorPlan_FUSION_ROOM_USE]];
//        }
    }
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BIMPlanColorCatNVC* nvc=( BIMPlanColorCatNVC*) [self navigationController];
    ViewModelToolbar* toolBar=[nvc viewModelToolbar];
    if (toolBar.userSelectedColorTableIndex<0){
        
    }else{

        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:(toolBar.userSelectedColorTableIndex) inSection:0];
        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
        
    }
//    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    Site* site=appDele.currentSite;
    NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
    uint numCustColorStruct=[aCustomColorStruct count];
    return [self.aColorCatTypeForRow count]+numCustColorStruct;
//    
//    
////    
////    NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
////
////    
//
//    if ([appDele modelDisplayLevel]==0){
//        return [self numColorCatOnSiteLevel];
////        return appDele.numFixedColorCatOnSiteLevel+numCustColorStruct;
//    }else if ([appDele modelDisplayLevel]==1){
//        return [self numColorCatOnFloorLevel];
////        return appDele.numFixedColorCatOnSFloorLevel+numCustColorStruct;
//    }
//    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([appDele modelDisplayLevel]==0){
        if ([indexPath row]<[self.aColorCatTypeForRow count]){
            NSNumber* thisColorCatType=[self.aColorCatTypeForRow objectAtIndex:[indexPath row]];            
            switch ([thisColorCatType integerValue]){
                case sitePlan_NONE:
                    cell.textLabel.text=@"No Color Coding";
                    break;    
                case sitePlan_CONSTRUCTION_STATUS:
                    cell.textLabel.text=@"Construction Status";
                    break;
                case sitePlan_FUSION_BLDG_STATUS:
                    cell.textLabel.text=@"Fusion – Building Status";
                    break;
                case sitePlan_FUSION_BLDG_FCI:
                    cell.textLabel.text=@"Fusion – Building FCI";
                    break;
                case sitePlan_FUSION_BLDG_STATUS_FLAG:
                    cell.textLabel.text=@"Fusion – Status Flag";
                    break;
                default:{

                }
                    break;
                    
            }
        }else{
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            Site* site=appDele.currentSite;
            NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;

            uint row=[indexPath row];
//            if (row <appDele.numFixedColorCatOnSiteLevel || (row-appDele.numFixedColorCatOnSiteLevel)>=[aCustomColorStruct count]) {break;}
            CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:(row-[self.aColorCatTypeForRow count]) ];//  appDele.numFixedColorCatOnSiteLevel)];
            cell.textLabel.text=customColorStruct.customSettingName;
        }

    }else if ([appDele modelDisplayLevel]==1){
        if ([indexPath row]<[self.aColorCatTypeForRow count]){
            NSNumber* thisColorCatType=[self.aColorCatTypeForRow objectAtIndex:[indexPath row]];
            switch ([thisColorCatType integerValue]){

                case floorPlan_NONE:
                    cell.textLabel.text=@"No Color Coding";
                    break;    
                case floorPlan_DEPARTMENT:
                    cell.textLabel.text=@"Departments";
                    break;   
                case floorPlan_VERTICAL_ALIGNMENT:
                    cell.textLabel.text=@"Vertical Alignment";
                    break;
                case floorPlan_FUSION_PLANNEDSFASSIGNEDSF:
                    cell.textLabel.text=@"Fusion Planned SF/Assigned SF";
                    break;
                case floorPlan_FUSION_SPACE_STATUS_FLAG:
                    cell.textLabel.text=@"Fusion – Status Flag";
                    break;
                case floorPlan_FUSION_RECORDSTATUS:
                    cell.textLabel.text=@"Fusion – Record Status";
                    break;
                case floorPlan_FUSION_TOP:
                    cell.textLabel.text=@"Fusion – TOP";
                    break;
                case floorPlan_FUSION_PGM:
                    cell.textLabel.text=@"Fusion – PGM";
                    break;
                case floorPlan_FUSION_ROOM_USE:
                    cell.textLabel.text=@"Fusion – Room Use";
                    break;

   
                default:{

                }
                    break;
            }
                    
    // Configure the cell...
        }else{
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            Site* site=appDele.currentSite;
            NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
            //            uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
            
            uint row=[indexPath row];
//            if (row <appDele.numFixedColorCatOnSFloorLevel || (row-appDele.numFixedColorCatOnSFloorLevel)>=[aCustomColorStruct count]) {break;}
            CustomColorStruct* customColorStruct=[aCustomColorStruct objectAtIndex:(row- [self.aColorCatTypeForRow count] )];// appDele.numFixedColorCatOnSFloorLevel)];
            cell.textLabel.text=customColorStruct.customSettingName;
        }
    }
    
//    cell.backgroundColor=[UIColor colorWithRed:0. green:0.39 blue:0.106 alpha:0.1];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    Site* site=[appDele currentSite];
    
    BIMPlanColorCatNVC* nvc=( BIMPlanColorCatNVC*) [self navigationController];
    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
    [[viewModelVC model] setColorMethod:[indexPath row]];
    
    if ([appDele modelDisplayLevel]==0){
        
        if ([indexPath row]<[self.aColorCatTypeForRow count]){
            NSNumber* thisColorCatType=[self.aColorCatTypeForRow objectAtIndex:[indexPath row]];
            
            [viewModelVC setCurrentColorMethod:([thisColorCatType integerValue])];
            switch ([thisColorCatType integerValue]){

                case sitePlan_NONE:
                {
                    [[[nvc viewModelToolbar]  popoverController] dismissPopoverAnimated:YES];
                    [nvc viewModelToolbar].popoverController=nil;
                    
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    for (UIView* subView in [[viewModelVC displayModelUI] subviews]){
                        for (UIView* subSubView in [subView subviews]){
                            if ([subSubView isKindOfClass:[ViewProductRep class]]){
                                ViewProductRep* viewProductRep=(ViewProductRep*) subSubView;
                                    [viewProductRep setNeedsDisplay];                                                            
                            }
                        }
                    }
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:nil];

                }
                    break;
                case sitePlan_CONSTRUCTION_STATUS:{
                    ConstructionStatusTVC* constructionStatusTVC=[[ConstructionStatusTVC alloc] init];

                    constructionStatusTVC.title=@"Construction Status";
                    uint numRow=3;
    //                if (site.aDepartment){
    //                    numRow=[site.aDepartment count];
    //                }
    //                if (numRow>14) {numRow=14;}
    //                if (numRow<1) {numRow=1;}
                    [constructionStatusTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:constructionStatusTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [constructionStatusTVC release];
                    
                    
                    
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }

                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
                            if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                                ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                                if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                                    [viewProductRep setNeedsDisplay];
                                    
                                }  
                            }
                        
                        
                    }
                }
                    break; 

                    
                case sitePlan_FUSION_BLDG_STATUS:{
                    FusionBldgStatusTVC* fusionBldgStatusTVC=[[FusionBldgStatusTVC alloc] init];
                    fusionBldgStatusTVC.title=@"Fusion – Building Status";
                    uint numRow=6;
                    [fusionBldgStatusTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:fusionBldgStatusTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [fusionBldgStatusTVC release];
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
                        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                            if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                                [viewProductRep setNeedsDisplay];
                                
                            }
                        }
                    }
                }
                    break; 
                    
                case sitePlan_FUSION_BLDG_FCI:{
                    FusionBldgFCITVC* fusionBldgFCITVC=[[FusionBldgFCITVC alloc] init];
                    fusionBldgFCITVC.title=@"Fusion – Building FCI";
                    uint numRow=5;
                    [fusionBldgFCITVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:fusionBldgFCITVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [fusionBldgFCITVC release];
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
                        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                            if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                                [viewProductRep setNeedsDisplay];
                                
                            }
                        }
                    }
                }
                    break;
                    
                    
                case sitePlan_FUSION_BLDG_STATUS_FLAG:{
                    FusionBldgStatusFlagTVC* fusionBldgStatusFlagTVC=[[FusionBldgStatusFlagTVC alloc] init];
                    fusionBldgStatusFlagTVC.title=@"Fusion – Statuis Flag";
                    uint numRow=4;
                    [fusionBldgStatusFlagTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:fusionBldgStatusFlagTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [fusionBldgStatusFlagTVC release];
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
                        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                            if ([[viewProductRep product] isKindOfClass:[Floor class]]){
                                [viewProductRep setNeedsDisplay];
                                
                            }
                        }
                    }
                }
                    break;
                default:
                {            
                }
                    break;
            }
        }else{
            [viewModelVC setCurrentColorMethod:sitePlan_CUSTOM];
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            Site* site=[appDele currentSite];
            
            int pCustomColorStruct=[indexPath row]- [self.aColorCatTypeForRow count];//-appDele.numFixedColorCatOnSiteLevel;
            if (pCustomColorStruct>=0) {
            
                
                if ([appDele modelDisplayLevel]==0){
                    site.activeBldgCustomColorStruct=pCustomColorStruct;
                }else if ([appDele modelDisplayLevel]==1){
                    site.activeSpaceCustomColorStruct=pCustomColorStruct;
                }
                NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
                uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
                
                
                uint numRow=0;
                CustomColorStruct* customColorStruct=nil;
                if (aCustomColorStruct){
                    customColorStruct=[aCustomColorStruct objectAtIndex:(activeCustomColorStruct)];
                    if (customColorStruct && ([customColorStruct.aCustomColor count]>0)){
                        numRow=[customColorStruct.aCustomColor count];
                    }
                }
                
                if (numRow>14) {numRow=14;}
                if (numRow<1){ numRow=1;}
                if (customColorStruct!=nil){
                    
                    CustomColorTVC* customColorTVC=[[CustomColorTVC alloc] init];
                    customColorTVC.title=customColorStruct.customSettingName;
                    [customColorTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:customColorTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [customColorTVC release];
                    
                    
                    
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]!=[indexPath row]){
                        
                        ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                        DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                        //                for (UIView* layerSubView in [displayModelUI.spatialStructViewLayer subviews]){
                        
                        for (UIView* layerSubView in displayModelUI.spatialStructSlabViewLayer.subviews){
                            if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                                ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                                OPSProduct* product=[viewProductRep product];
                                if ([product isKindOfClass:[Floor class]]){
                                    [viewProductRep setNeedsDisplay];
                                }
                            }
                            
                            
                        }
                    }
                    
    
                }
            }
            
        
        }

    }else if ([appDele modelDisplayLevel]==1){
        
        if ([indexPath row]<[self.aColorCatTypeForRow count]){
            NSNumber* thisColorCatType=[self.aColorCatTypeForRow objectAtIndex:[indexPath row]];
            
            [viewModelVC setCurrentColorMethod:([thisColorCatType integerValue])];
            switch ([thisColorCatType integerValue]){

                case floorPlan_NONE:{
                    //            BIMPlanColorCatNVC* nvc=( BIMPlanColorCatNVC*) [self navigationController];
                    
                    [[[nvc viewModelToolbar]  popoverController] dismissPopoverAnimated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [nvc viewModelToolbar].popoverController=nil;
                    
                    
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    for (UIView* subView in [[viewModelVC displayModelUI] subviews]){
                        for (UIView* subSubView in [subView subviews]){
                            if ([subSubView isKindOfClass:[ViewProductRep class]]){
                                ViewProductRep* viewProductRep=(ViewProductRep*) subSubView;
                                //                        if ([[viewProductRep product] isKindOfClass:[Space class]]){
                                [viewProductRep setNeedsDisplay];
                                
                                //                        }
                                
                            }
                        }
                    }
                    
                }
                    break;
                case floorPlan_DEPARTMENT:{
                    //            BIMPlanColorCatNVC* nvc=( BIMPlanColorCatNVC*) [self navigationController];
                    
                    DepartmentColorTVC* departColorTVC=[[DepartmentColorTVC alloc] init];
                    
                    departColorTVC.title=@"Department Color";
                    uint numRow=0;
                    if (site.aDepartment){
                        numRow=[site.aDepartment count];
                    }
                    if (numRow>14) {numRow=14;}
                    if (numRow<1) {numRow=1;}
                    [departColorTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:departColorTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [departColorTVC release];
                    
                    
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    
                    
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
                        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                                [viewProductRep setNeedsDisplay];
                                
                            }  
                        }
                        
                        
                    }
                    //            for (UIView* subView in [[viewModelVC displayModelUI] subviews]){
                    //                for (UIView* subSubView in [subView subviews]){
                    //                    if (subSubView isKindOfClass:[
                    //                    if ([subSubView isKindOfClass:[ViewProductRep class]]){
                    //                        ViewProductRep* viewProductRep=(ViewProductRep*) subSubView;
                    //                        if ([[viewProductRep product] isKindOfClass:[Space class]]){
                    //                            [viewProductRep setNeedsDisplay];
                    //                            
                    //                        }
                    //                        
                    //                    }
                    //                }
                    //            }
                }
                    break; 
                    
                case floorPlan_VERTICAL_ALIGNMENT:{
                    
                    
                    
                    uint numRow=0;
                    
                    if (site.alignColorDictionary) {numRow=[[site.alignColorDictionary allKeys] count];}
                    if (numRow<1) {numRow=1;}
                    if (numRow>14) {numRow=14;}
                    
                    AlignColorTVC* alignColorTVC=[[AlignColorTVC alloc] init];            
                    alignColorTVC.title=@"Alignment Color";
                    [alignColorTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:alignColorTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [alignColorTVC release];
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
                        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                                //                        Space* space=(Space*)[viewProductRep product];
                                //                        if (space.alignID!=0){
                                [viewProductRep setNeedsDisplay];                        
                                //                        }
                            }  
                        }
                        
                        
                    }
               
                }
                    break;
                    
                case floorPlan_FUSION_PLANNEDSFASSIGNEDSF:{
                    FusionPlannedSFAssignedSFTVC* fusionPlannedSFAssignedSFTVC=[[FusionPlannedSFAssignedSFTVC alloc] init];
                    fusionPlannedSFAssignedSFTVC.title=@"Fusion–Planned SF/Assigned SF";
                    uint numRow=5;
                    [fusionPlannedSFAssignedSFTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:fusionPlannedSFAssignedSFTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [fusionPlannedSFAssignedSFTVC release];
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
                        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                                //                        Space* space=(Space*)[viewProductRep product];
                                //                        if (space.alignID!=0){
                                [viewProductRep setNeedsDisplay];
                                //                        }
                            }
                        }
                        
                        
                    }
                }
                    break;
                    
                case floorPlan_FUSION_SPACE_STATUS_FLAG:{
                    FusionSpaceStatusFlagTVC* fusionSpaceStatusFlagTVC=[[FusionSpaceStatusFlagTVC alloc] init];
                    fusionSpaceStatusFlagTVC.title=@"Fusion Status Flag";
                    uint numRow=5;
                    [fusionSpaceStatusFlagTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:fusionSpaceStatusFlagTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [fusionSpaceStatusFlagTVC release];
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
                        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                                //                        Space* space=(Space*)[viewProductRep product];
                                //                        if (space.alignID!=0){
                                [viewProductRep setNeedsDisplay];
                                //                        }
                            }
                        }
                        
                        
                    }
                }
                    break;
                case floorPlan_FUSION_RECORDSTATUS:{
                    FusionRecordStatusTVC* fusionRecordStatusTVC=[[FusionRecordStatusTVC alloc] init];
                    fusionRecordStatusTVC.title=@"Fusion Record Status";
                    uint numRow=5;
                    [fusionRecordStatusTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:fusionRecordStatusTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [fusionRecordStatusTVC release];
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
                        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                                //                        Space* space=(Space*)[viewProductRep product];
                                //                        if (space.alignID!=0){
                                [viewProductRep setNeedsDisplay];
                                //                        }
                            }
                        }                    
                    }
                }
                    break;
                case floorPlan_FUSION_TOP:{
                    FusionTopTVC* fusionTopTVC=[[FusionTopTVC alloc] init];
                    fusionTopTVC.title=@"Fusion - Top";
                    Site* site=[appDele currentSite];
                    uint numRow=0;
                    if (site.fusionTopDictionary){
                        numRow=[site.fusionTopDictionary count];
                    }
                    [fusionTopTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:fusionTopTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [fusionTopTVC release];
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
                        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                                //                        Space* space=(Space*)[viewProductRep product];
                                //                        if (space.alignID!=0){
                                [viewProductRep setNeedsDisplay];
                                //                        }
                            }
                        }
                    }
                }
                    break;
                case floorPlan_FUSION_PGM:{
                    FusionPGMTVC* fusionPGMTVC=[[FusionPGMTVC alloc] init];
                    fusionPGMTVC.title=@"Fusion - PGM";
                    Site* site=[appDele currentSite];
                    uint numRow=0;
                    if (site.fusionPGMDictionary){
                        numRow=[site.fusionPGMDictionary count];
                    }
                    [fusionPGMTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:fusionPGMTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [fusionPGMTVC release];
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
                        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                                //                        Space* space=(Space*)[viewProductRep product];
                                //                        if (space.alignID!=0){
                                [viewProductRep setNeedsDisplay];
                                //                        }
                            }
                        }
                    }
                }
                    break;
                case floorPlan_FUSION_ROOM_USE:{
                    FusionRoomUseTVC* fusionRoomUseTVC=[[FusionRoomUseTVC alloc] init];
                    fusionRoomUseTVC.title=@"Fusion - Room Use";
                    Site* site=[appDele currentSite];
                    uint numRow=0;
                    if (site.fusionRoomUseDictionary){
                        numRow=[site.fusionRoomUseDictionary count];
                    }
                    [fusionRoomUseTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:fusionRoomUseTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [fusionRoomUseTVC release];
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]==[indexPath row]){
                        break;
                    }
                    
                    ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
                        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                                //                        Space* space=(Space*)[viewProductRep product];
                                //                        if (space.alignID!=0){
                                [viewProductRep setNeedsDisplay];
                                //                        }
                            }
                        }
                    }
                }
                    break;
                default:
                {            
                                       
                    
                }
                    break;
            }
            
        }else{
            [viewModelVC setCurrentColorMethod:floorPlan_CUSTOM];
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            Site* site=[appDele currentSite];
            
            int pCustomColorStruct=[indexPath row]- [self.aColorCatTypeForRow count];// appDele.numFixedColorCatOnSFloorLevel;
            if (pCustomColorStruct>=0) {
                
                if ([appDele modelDisplayLevel]==0){
                    site.activeBldgCustomColorStruct=pCustomColorStruct;
                }else if ([appDele modelDisplayLevel]==1){
                    site.activeSpaceCustomColorStruct=pCustomColorStruct;
                }
                NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
                uint activeCustomColorStruct=([appDele modelDisplayLevel]==0)?site.activeBldgCustomColorStruct:site.activeSpaceCustomColorStruct;
                
                
                uint numRow=0;
                CustomColorStruct* customColorStruct=nil;
                if (aCustomColorStruct){
                    customColorStruct=[aCustomColorStruct objectAtIndex:(activeCustomColorStruct)];
                    if (customColorStruct && ([customColorStruct.aCustomColor count]>0)){
                        numRow=[customColorStruct.aCustomColor count];
                    }
                }
                
                if (numRow>14) {numRow=14;}
                if (numRow<1){ numRow=1;}
                if (customColorStruct!=nil){
                    
                    CustomColorTVC* customColorTVC=[[CustomColorTVC alloc] init];
                    customColorTVC.title=customColorStruct.customSettingName;
                    [customColorTVC setContentSizeForViewInPopover:CGSizeMake(360, 44*numRow)];
                    [nvc pushViewController:customColorTVC animated:YES];
//                    [[viewModelVC model] setCurrentBIMPlanColorTVC:[nvc.viewControllers lastObject]];
                    [customColorTVC release];
                    
                    
                    
                    if ([[nvc viewModelToolbar] userSelectedColorTableIndex]!=[indexPath row]){
                        ViewModelVC* viewModelVC=[[nvc viewModelToolbar] parentViewModelVC];
                        DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
                        for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
                            if ([layerSubView isKindOfClass:[ViewProductRep class]]){
                                ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
                                if ([[viewProductRep product] isKindOfClass:[Space class]]){
                                    [viewProductRep setNeedsDisplay];
                                }
                            }
                            
                            
                        }
                        
                    }
                    
                }
                
        
            }
            
        }

    }
    [[nvc viewModelToolbar] setUserSelectedColorTableIndex:[indexPath row]];
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end

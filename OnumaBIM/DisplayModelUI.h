//
//  DisplayModelUI.h
//  ProjectView
//
//  Created by Alfred Man on 11/10/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewProductRep.h"
#import "ViewProductSprite.h"
#import "ViewProductLabel.h"
//#import "ViewProductLabel.h"

#import  <MapKit/MapKit.h>
@protocol DisplayModelUIDelegate 

//- (UIScrollView*) getScrollView;
@end
@class SubViewSpatialStructSlab;
@class ViewProductRep;
@class RepresentationItem;
@class SpatialStructure;
@class OPSProduct;
@class ViewModelVC;
@class OPSNavUI;
@class NavUIScroll;
@class DisplayInfo;
//@class CaptureView;
@interface DisplayModelUI : UIView <ViewProductRepDelegate,ViewProductSpriteDelegate,ViewProductLabelDelegate,MKMapViewDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate>{//, ViewProductLabelDelegate> {
    id <DisplayModelUIDelegate> delegate; 
    double threeFingerPinchStartScale;
    double threeFingerPinchEndScale;

    MKMapType _userSelectedMapType;
    
    
    
    MKMapView* siteMapView;
    
    UIView* productViewLabelLayer;
    
    UIView* _ghostLayer;
    UIView* _ghostFurnLayer;
    UIView* elemViewLayer;
    UIView* _sitePolygonLayer;
    UIView* spatialStructViewLayer;
    UIView* spatialStructSlabViewLayer;

    double mapTransformScale;
    UIImageView* userLocationImageView;
    CGRect mapFrame;
    MKUserLocation* _userLocation;
    CGAffineTransform _modelCenterScreenTransform;
    CLLocationManager* _locationManager;
    
}
@property (nonatomic, retain) CLLocationManager* locationManager;
@property (nonatomic, assign) MKMapType userSelectedMapType;

@property (nonatomic, assign) CGAffineTransform modelCenterScreenTransform;

@property (nonatomic, retain) MKUserLocation* userLocation;
@property (nonatomic, assign) CGRect mapFrame;
@property (nonatomic, retain) UIImageView* userLocationImageView;
@property (nonatomic, assign) double mapTransformScale;
@property (assign) id <DisplayModelUIDelegate> delegate;
@property (nonatomic, retain) UIView* productViewLabelLayer;


@property (nonatomic, retain) MKMapView* siteMapView;

@property (nonatomic, retain) UIView* ghostLayer;
@property (nonatomic, retain) UIView* ghostFurnLayer;
@property (nonatomic, retain) UIView* sitePolygonLayer;
@property (nonatomic, retain) UIView* elemViewLayer;
@property (nonatomic, retain) UIView* spatialStructViewLayer;
@property (nonatomic, retain) UIView* spatialStructSlabViewLayer;



//-(bool) shouldAttachDisplayInfo: (OPSProduct*) product;
//-(void) attachDisplayInfo: (OPSProduct*) product productTransform:(CGAffineTransform) productTransform;
//- (CGAffineTransform) setupShapePlacementTransform:(CGAffineTransform) placementTransform repItem:(RepresentationItem*) repItem;
//- (CGAffineTransform) setupShapePlacementTransform:(CGAffineTransform) placementTransform bBox:(BBox*) bBox bFlipX:(Boolean)bFlipX;
- (CGAffineTransform) setupShapePlacementTransform:(CGAffineTransform) placementTransform productBBox:(CGRect) productBBox bFlipX:(Boolean)bFlipX;
- (CGAffineTransform) setupBitmapPlacementTransform:(CGAffineTransform) placementTransform productBBox:(CGRect) productBBox;
- (OPSProduct*) findRefProduct:(OPSProduct*) product;
- (id)initWithFrame:(CGRect)frame rootSpatialStruct:(SpatialStructure*)rootSpatialStruct  modelCenterScreenTransform:(CGAffineTransform) modelCenterScreenTransform   navUIScroll:(NavUIScroll*) navUIScroll;
- (void)initDislayFromRootSpatialStruct:(SpatialStructure*) rootSpatialStruct rootTransform: (CGAffineTransform) rootTransform ;
//- (void) showProductRepButton: (CGPoint) touchLocation;

- (ViewModelVC*) parentViewModelVC;

-(Boolean) shouldDisplayLabel:(DisplayInfo*) displayInfo;
-(void) addDisplayLabelToViewProductRep:(ViewProductRep*) viewProductRep displayInfo:(DisplayInfo*) displayInfo rootSpatialStructTransform:(CGAffineTransform) rootSpatialStructTransform;
- (void)oneFingerTwoTaps;

-(void) setMapType:(uint) mapType;

- (void) drawMap:(double) scale;

- (BOOL) searchAndSelectSpatialStruct:(NSSet *)touches withEvent:(UIEvent *)event;
- (BOOL) searchAndSelectSpatialStructSlab:(NSSet *)touches withEvent:(UIEvent *)event;
- (BOOL) searchAndSelectElem:(NSSet *)touches withEvent:(UIEvent *)event;

//- (void) threeFingerPinchDetected: (UIPinchGestureRecognizer *)recognizer;
-(void) redrawOnumaPin;
//-(void) captureMapView;
-(void) hideAllLayer;
-(void) showAllLayer;

-(void) removeMap;
-(void) removeAllViewProductLabel;
-(void) addAllViewProductLabel;
@end

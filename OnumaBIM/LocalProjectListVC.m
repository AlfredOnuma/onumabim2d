//
//  LocalProjectListVC.m
//  ProjectView
//
//  Created by Alfred Man on 1/5/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import "LocalProjectListVC.h"
#import "ProjectViewAppDelegate.h"
//#import "DataConnect.h"
#import "ViewNeedWebForLiveDB.h"
#import "CXMLDocument.h"
#import "OPSStudio.h"
#import "TableCellTitleObject.h"
#import "TableCellTitleProjectCat.h"
#import "LiveProjectListVC.h"
#import "TableCellTitleProject.h"
#import "OPSProjectSite.h"
#import "OPSProjectSiteVersionUpdater.h"

#import "ViewNoLocalDB.h"
#import <sqlite3.h>

#import "Project.h"
#import "TableCellTitleProjectCat.h"
#import "OPSLocalProjectSite.h"
//#import <sqlite3.h>
@implementation LocalProjectListVC

//@synthesize connectionData;
//@synthesize theConnection;

@synthesize selectedIndexPath;
@synthesize versionUpdater=_versionUpdater;
@synthesize projectSiteBeingUploaded=_projectSiteBeingUploaded;
uint const LocalProjectList_UIAlertViewTag_NoInternetConfirm=1;
uint const LocalProjectList_UIAlertViewTag_TrashSiteConfirm=0;
uint const LocalProjectList_UIAlertViewTag_LocalSiteNeedUpdate=2;
-(void) dealloc{
//    [connectionData release],connectionData=nil;
//    [theConnection release], theConnection=nil;
    
    [_projectSiteBeingUploaded release];_projectSiteBeingUploaded=nil;
    [_versionUpdater release],_versionUpdater=nil;
    [selectedIndexPath release],selectedIndexPath=nil;
    [super dealloc];
}


- (void)siteDetail:(id)sender event:(id)event{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
    
    
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.theTableView];
    
	NSIndexPath *indexPath = [self.theTableView indexPathForRowAtPoint: currentTouchPosition];

    if (indexPath==nil){
        return;
    }
    
    OPSProjectSite* selectedProjectSite=[tableData objectAtIndex:indexPath.row];  
    if ([selectedProjectSite dbPath]==Nil) {
        return;
    }    
    [appDele setActiveProjectSite:selectedProjectSite];
    [appDele readUnitStructWithDBPath:[selectedProjectSite dbPath]];
    
    
    [appDele displayProjectListSiteInfoVC: self];
    
    return;
}


//static sqlite3 *database = nil;


/*

- (NSData *)generatePostDataForDataNSData: (NSData*)uploadData
{
    // Generate the post header:
    NSString *post = [NSString stringWithCString:
                      "--AaB03x\r\nContent-Disposition: form-data; name=\"upload[file]\"; filename=\"test.txt\"\r\nContent-Type: application/octet-stream\r\nContent-Transfer-Encoding: binary\r\n\r\n"
                                        encoding:NSASCIIStringEncoding];
    NSLog(@"Post Data........ %@", post);
    // Get the post header int ASCII format:
    NSData *postHeaderData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    // Generate the mutable data variable:
    NSMutableData *postData = [[NSMutableData alloc] initWithLength:[postHeaderData length] ];
    [postData setDataPostHeaderData];
    
    // Add the image:
    [postData appendData: uploadData];
    
    // Add the closing boundry:
    [postData appendData: [@"\r\n--AaB03x--" dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
    
    // Return the post data:
    return postData;
}


- (void)uploadFile
{
    
    NSData *yourData ;
    NSLog(@"NSData----yourData---------%@", yourData);
    // Generate the postdata:
    NSData *postData = [self generatePostDataForData: yourData];
    
    NSLog(@"NSData----postData---------%@", postData);
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    // Setup the request:
    NSMutableURLRequest *uploadRequest = [[[NSMutableURLRequest alloc]
                                           initWithURL:[NSURL URLWithString:@"The server URL"]
                                           cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval: 30 ] autorelease];
    [uploadRequest setHTTPMethod:@"POST"];
    [uploadRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [uploadRequest setValue:@"multipart/form-data; boundary=AaB03x" forHTTPHeaderField:@"Content-Type"];
    [uploadRequest setHTTPBody:postData];
    
    // Execute the reqest:
    NSURLConnection *conn=[[NSURLConnection alloc] initWithRequest:uploadRequest delegate:self];
    if (conn)
    {
        // Connection succeeded (even if a 404 or other non-200 range was returned).
        receivedData = [[NSMutableData data] retain];
    }
    else
    {
        // Connection failed (cannot reach server).
    }
    
}
*/

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case LocalProjectList_UIAlertViewTag_NoInternetConfirm:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	
//                    NSLog(@"Delete was cancelled by the user");
                }
                    break;
                case 1: // delete
                {
//                    [self uploadProject:nil event:nil];
//                    [self trashAttachmentEntry];
                    // do the delete
                }
                    break;
            }
            
            break;
        case LocalProjectList_UIAlertViewTag_TrashSiteConfirm:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	
                    //                    NSLog(@"Delete was cancelled by the user");
                }
                    break;
                case 1: // delete
                {
                    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
                    [appDele displayLoadingViewOverlay:@"Deleting Project Site" invoker:self completeAction:@selector(deleteProjectSite:) actionParamArray:[NSArray arrayWithObjects:selectedIndexPath,nil]];

                    //                    [self uploadProject:nil event:nil];
                    //                    [self trashAttachmentEntry];
                    // do the delete
                }
                    break;
            }
            
        }
        break;
        case LocalProjectList_UIAlertViewTag_LocalSiteNeedUpdate:
        {
            switch (buttonIndex) {
                case 0: // cancel
                {	
                    //                    NSLog(@"Delete was cancelled by the user");
                }
                    break;
                case 1: // update
                {
                    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
                    OPSProjectSite* projectSite=[appDele activeProjectSite];
                    OPSProjectSiteVersionUpdater* projectSiteUpdater=[[OPSProjectSiteVersionUpdater alloc] initWithOPSProjectSite: projectSite localProjectListVC:self];    
                    self.versionUpdater=projectSiteUpdater;
                    [projectSiteUpdater release];
                    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Updating Database from Server for site: %@",  projectSite.name] invoker:self.versionUpdater completeAction:@selector(updateDBFromServer:) actionParamArray:nil];
                     
//                    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Updating Database from Server for site: %@",  projectSite.name] invoker:self.versionUpdater completeAction:@selector(updateDBFromServer:) actionParamArray: [NSArray arrayWithObjects:self,nil]];                                        
                    
//                    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Updating Database from Server for site: %@",  projectSite.name] invoker:self completeAction:@selector(copyModelFromServer:) actionParamArray:[NSArray arrayWithObjects:[NSNumber numberWithBool:bNoGraphicData], nil]];
                    
                }
                    break;
            }  
            
        }
        break;
        default:
//            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");
            break;
        }
    }	
}




//-(void) alertUploadSuccess:(id)sender{
//    UIAlertView *alert = [[UIAlertView alloc] 
//                          initWithTitle: @"Upload Succcess"
//                          message: @""
//                          delegate: self
//                          cancelButtonTitle: nil
//                          otherButtonTitles: @"ok", nil];
//    alert.tag = NoInternetConfirmOK; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//    [alert show];
//    [alert release];
//    
//    
//    
//}

-(void) alertNoInternet:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle: @"No Internet Available"
                          message: @""
                          delegate: self
                          cancelButtonTitle: nil
                          otherButtonTitles: @"ok", nil];
    alert.tag = LocalProjectList_UIAlertViewTag_NoInternetConfirm; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
    [alert show];
    [alert release];
    
    
    
}

-(void) alertSiteNeedUpdate:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle: @"The downloaded site need update from Onuma Live."
                          message: @""
                          delegate: self
                          cancelButtonTitle: @"Cancel"
                          otherButtonTitles: @"Update", nil];
    alert.tag = LocalProjectList_UIAlertViewTag_LocalSiteNeedUpdate; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
    [alert show];
    [alert release];    
}

/*
-(NSData *) uploadProject:(id)sender event:(id)event
{   
    
    NSURL *url = [NSURL URLWithString:@"http://www.onuma.com/plan/postTestAction.php"];
    
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url 
                                                       cachePolicy:NSURLRequestReloadIgnoringCacheData 
                                                   timeoutInterval:60];
    
    [req setHTTPMethod:@"POST"];        
    [req setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSString *postData = [NSString stringWithFormat:@"page=%@&amp;param1=%@&amp;param2=%@param3=%@", @"item1", @"item 2",@"item 3",@"item 4"];
    
    NSString *length = [NSString stringWithFormat:@"%d", [postData length]];    
    [req setValue:length forHTTPHeaderField:@"Content-Length"]; 
    
    [req setHTTPBody:[postData dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSHTTPURLResponse* urlResponse = nil;
    NSError *error = [[[NSError alloc] init] autorelease];  
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:req
                                                 returningResponse:&urlResponse 
                                                             error:&error];  
    
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
    NSLog(@"%@",responseString);
}
*/




- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if (self.editing == NO || !indexPath) return UITableViewCellEditingStyleNone;
    
    if (self.editing && indexPath.row == ([self.tableData count]))        
    {
        
        return UITableViewCellEditingStyleInsert;
        
    } else        
    {
        
        return UITableViewCellEditingStyleDelete;
        
    }    
    return UITableViewCellEditingStyleNone;
    
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle

forRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)        
    {        
        [self.tableData removeObjectAtIndex:indexPath.row];
        
        [self.theTableView reloadData];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert)
        
    {
        
        [self.tableData insertObject:@"Tutorial" atIndex:[tableData count]];
        [self.theTableView reloadData];

        
    }
    
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return YES;
    
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath

      toIndexPath:(NSIndexPath *)toIndexPath

{
    
    NSString *item = [[tableData objectAtIndex:fromIndexPath.row] retain];
    
    [tableData removeObject:item];
    
    [tableData insertObject:item atIndex:toIndexPath.row];
    
    [item release];
    
}

-(void) deleteProjectSite:(NSArray*) paramArray{            
//    OPSProjectSite* selectedProjectSite=[paramArray objectAtIndex:0];
    NSIndexPath *indexPath=[paramArray objectAtIndex:0];
        
    OPSProjectSite* selectedProjectSite=[tableData objectAtIndex:indexPath.row];  
    if ([selectedProjectSite dbPath]==Nil) {return;}  
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate]; 
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *databaseFolderPath = [paths objectAtIndex:0]; 
    
    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], [appDele activeLocalStudio].ID, [appDele activeLocalStudio].name, [appDele activeLocalStudio].iconName ]]; 	    
    
    sqlite3_stmt *attachedFileStmt; 
    const char* attachedFileSql=[[NSString stringWithFormat:@"select attachfile.fileName from attachfile"] UTF8String];            
    sqlite3 *database = nil;    
    
    if (sqlite3_open ([[selectedProjectSite dbPath] UTF8String], &database) == SQLITE_OK) {      
        if(sqlite3_prepare_v2(database, attachedFileSql, -1, &attachedFileStmt, NULL) == SQLITE_OK) {
            while( sqlite3_step(attachedFileStmt) == SQLITE_ROW) {                                 
                
                NSString* attachedFileName= nil;
                if ((char *) sqlite3_column_text(attachedFileStmt, 0)!=nil){
                    attachedFileName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(attachedFileStmt, 0)];               
                }else{
                    attachedFileName=[NSString stringWithFormat:@""];
                }                  
                
//                NSLog(@"fileName:%@",attachedFileName);
                if (![attachedFileName hasSuffix:@".jpg"]){
                    continue;
                }
                
                
                //            NSString *FileParamConstant = @"image";
                
                // add image data and compress to send if needed.
                //            CGFloat compression         = 0.9f;
                //            CGFloat maxCompression = 0.1f;
                //            int maxFileSize             = 250*1024;
                
                NSString* fullImagePath=[databaseFolderPath stringByAppendingPathComponent:attachedFileName];
                NSLog(@"deleting Img at:%@",fullImagePath);
                [[NSFileManager defaultManager] removeItemAtPath: fullImagePath error:NULL];  

                NSString* slideImagePath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"sl_%@",attachedFileName] ];
                NSLog(@"deleting Img at:%@",slideImagePath);
                [[NSFileManager defaultManager] removeItemAtPath: slideImagePath error:NULL];
                
                NSString* tbImagePath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"tb_%@",attachedFileName]];
                NSLog(@"deleting Img at:%@",tbImagePath);
                [[NSFileManager defaultManager] removeItemAtPath: tbImagePath error:NULL];
                
            }
        }
        
        sqlite3_finalize(attachedFileStmt);
        sqlite3_close(database);
    }else{
        sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
    }
    
    
    
    
//    [appDele removeLoadingViewOverlay];    return;
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath: [selectedProjectSite dbPath] error:NULL]; 
    
    
    
    

    
    NSString* filename=[[selectedProjectSite dbPath] lastPathComponent];    
    //        NSMutableString *filenameOnly=[NSMutableString stringWithString:filename];
    //        [filenameOnly replaceCharactersInRange: [filenameOnly rangeOfString: @".sqlite"] withString: @""]; 
    NSString* filenameOnly=[filename stringByDeletingPathExtension];
    NSMutableDictionary *dictionary=nil;            
    NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
    dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];
    [dictionary removeObjectForKey:filenameOnly];
    
    [dictionary writeToFile:dictionaryPath atomically:YES];
    uint indexPathRow=indexPath.row;
    
    
    [theTableView beginUpdates];
    
    NSMutableArray* newDataArray=[[NSMutableArray alloc] init ];
    uint pRow=0;
    NSMutableArray* aIndexPathToDelete=[[NSMutableArray alloc] initWithObjects:indexPath, nil];
    
    for (TableCellTitleObject* cellObj in tableData){
        //        if (pRow==indexPathRow) {pRow++;continue;}
        
        
        if (pRow!=indexPathRow){
            [newDataArray addObject:cellObj];//[cellObj copy] ];
            
        }else{
            
            TableCellTitleObject* prevPrevRow=[tableData objectAtIndex:indexPathRow-2];
            TableCellTitleObject* prevRow=[tableData objectAtIndex:indexPathRow-1];

            if (![prevRow isKindOfClass:[OPSProjectSite class]]){   
                if (indexPathRow<([tableData count]-1) ){
                    if ( !([[tableData objectAtIndex:indexPathRow+1] isKindOfClass:[OPSProjectSite class]])   ){
                        //If previous row isn't a site and next row is a project Cat or project title. It means this is a singleton site inside a Project. Hence, remove the project cell                        
                        [aIndexPathToDelete addObject:[NSIndexPath indexPathForRow:indexPathRow-1 inSection:0]];
                        [newDataArray removeLastObject];
                        
                        
                    }
                }else{
                     //If previous row isn't a site and it is already last row. Rmeove the project cell                    
                    [aIndexPathToDelete addObject:[NSIndexPath indexPathForRow:indexPathRow-1 inSection:0]];
                    [newDataArray removeLastObject];
                    
                    
                }
            }
            
            if ([prevPrevRow isKindOfClass:[TableCellTitleProjectCat class]]){
                if (indexPathRow<([tableData count]-1) ){
                    TableCellTitleObject* nextRow=[tableData objectAtIndex:indexPathRow+1];
                    
//                    if (![nextRow isKindOfClass:[OPSProjectSite class]] && [prevPrevRow isKindOfClass:[TableCellTitleProjectCat class]]){
                    if (![nextRow isKindOfClass:[OPSProjectSite class]] && 
                        ![nextRow isKindOfClass:[TableCellTitleProject class]]){
                            
                        [aIndexPathToDelete addObject:[NSIndexPath indexPathForRow:indexPathRow-2 inSection:0]];
                        [newDataArray removeLastObject];   
                        
                    }                    
                }else{
                    
                    [aIndexPathToDelete addObject:[NSIndexPath indexPathForRow:indexPathRow-2 inSection:0]];
                    [newDataArray removeLastObject];                                           
                    
                }
            }
            
                                            
        }
        pRow++;
    }    
    /*
    for (TableCellTitleObject* cellObj in tableData){
//        if (pRow==indexPathRow) {pRow++;continue;}
    
        
        if (pRow!=indexPathRow){
            [newDataArray addObject:cellObj];//[cellObj copy] ];
          
        }else{
            TableCellTitleObject* prevRow=[tableData objectAtIndex:indexPathRow-1];
            if (![prevRow isKindOfClass:[OPSProjectSite class]]){                
                if ( (indexPathRow==([tableData count]-1)) || !([[tableData objectAtIndex:indexPathRow+1] isKindOfClass:[OPSProjectSite class]])   ){
                    
                    [aIndexPathToDelete addObject:[NSIndexPath indexPathForRow:indexPathRow-1 inSection:0]];
                    [newDataArray removeLastObject];
                    

                    if (indexPathRow==([tableData count]-1)||[[tableData objectAtIndex:indexPathRow+1] isKindOfClass:[TableCellTitleProjectCat class]]){                        
                        [aIndexPathToDelete addObject:[NSIndexPath indexPathForRow:indexPathRow-2 inSection:0]];
                        [newDataArray removeLastObject];   
                        
                        
                        
                        
                        NSString* studioProjectListPath=[databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];                                                
                        NSFileManager *fileManager = [NSFileManager defaultManager]; 
                        [fileManager removeItemAtPath: studioProjectListPath error:NULL]; 
                        
                        
                                       
                        [fileManager removeItemAtPath: databaseFolderPath error:NULL]; 
                        
                    }
                }
                
            }                                
        }
        pRow++;
    }
    */
    if ([newDataArray count]==0 || 
        (
         ([newDataArray count]==1)&&
         ([[newDataArray objectAtIndex:0] isKindOfClass:[TableCellTitleProjectCat class]])
        )
    ){  
//        [appDele removeCurrentStudio];
        NSString* studioProjectListPath=[databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];                                                
        NSFileManager *fileManager = [NSFileManager defaultManager]; 
        [fileManager removeItemAtPath: studioProjectListPath error:NULL]; 
        
        [fileManager removeItemAtPath: databaseFolderPath error:NULL]; 
        [self.navigationController popViewControllerAnimated:TRUE];
        
    }
    
    self.tableData=newDataArray;
    [newDataArray release];
    
    
//    [self.theTableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationFade];
    
    
    [self.theTableView deleteRowsAtIndexPaths:aIndexPathToDelete withRowAnimation:UITableViewRowAnimationFade];
    
    [aIndexPathToDelete release];
    
    [theTableView endUpdates];
    
    [self.theTableView reloadData];
    
    
    [appDele removeLoadingViewOverlay];
    
    
    
    
//    [self.navigationController popViewControllerAnimated:TRUE];
    
}

-(void) trashSite:(id)sender event:(id)event{
    
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];   

//    NSData* sqliteData=[NSData dataWithContentsOfFile:[selectedProjectSite dbPath]];
    
    
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.theTableView];
    
	NSIndexPath *indexPath = [self.theTableView indexPathForRowAtPoint: currentTouchPosition];
    //	if (indexPath != nil)
    //	{
    //		[self tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    //	}
    
    if (indexPath==nil){
        return;
    }

    
    self.selectedIndexPath=indexPath;

    [self alertTrashSiteConfirm:nil title:@"Delete selected site from Local Storage?"];
//    [appDele displayLoadingViewOverlay:@"Deleting Project Site" invoker:self completeAction:@selector(deleteProjectSite:) actionParamArray:[NSArray arrayWithObjects:indexPath,nil]];
        
        
    
    
    return;
}

-(void) alertTrashSiteConfirm:(id)sender title:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle: title
                          message: @""
                          delegate: self
                          cancelButtonTitle: @"cancel"
                          otherButtonTitles: @"ok", nil];
    alert.tag = LocalProjectList_UIAlertViewTag_TrashSiteConfirm; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
    [alert show];
    [alert release];
    
    
}




-(NSData *) uploadSite:(id)sender event:(id)event{
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];  

    if (![appDele isLiveDataSourceAvailable]){
        [self alertNoInternet:nil];
////        [appDele displayProjectUploadWebView:@"htt[://www.google.com"];
        return nil;
    }
    
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.theTableView];
    
	NSIndexPath *indexPath = [self.theTableView indexPathForRowAtPoint: currentTouchPosition];
    //	if (indexPath != nil)
    //	{
    //		[self tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    //	}
    
    if (indexPath==nil){
        return nil;
    }    
    OPSProjectSite* selectedProjectSite=[tableData objectAtIndex:indexPath.row];  
    if ([selectedProjectSite dbPath]==Nil) {return nil;}  
    
    
    
    [appDele setActiveProjectSite:selectedProjectSite];
//    NSString* dbPath=[selectedProjectSite dbPath];
//    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[selectedProjectSite name]]  invoker:self completeAction:@selector(uploadSelectedProjectSite:) actionParamArray:[NSArray arrayWithObjects: selectedProjectSite,dbPath,nil]];
        
    OPSLocalProjectSite* localProjectSite=[[OPSLocalProjectSite alloc] initWithLiveOPSProjectSite:selectedProjectSite];
    localProjectSite.afterUploadFailInvoker=self;
    localProjectSite.afterUploadFailCompleteAction=@selector(refreshTable);
    localProjectSite.afterUploadSuccessInvoker=self;
    localProjectSite.afterUploadSuccessCompleteAction=@selector(refreshTable);
    self.projectSiteBeingUploaded=localProjectSite;
    [localProjectSite release];
    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[selectedProjectSite name]]  invoker:self.projectSiteBeingUploaded completeAction:@selector(uploadProjectSite:) actionParamArray:nil]; 
    
//     [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[selectedProjectSite name]]  invoker:appDele completeAction:@selector(uploadSelectedProjectSite:) actionParamArray:[NSArray arrayWithObjects: selectedProjectSite,dbPath,nil]];   
    
    return nil;        
}



/*
 
 -(NSData*) uploadSelectedProjectSite:(NSArray*) paramArray{
 
 OPSProjectSite* selectedProjectSite=[paramArray objectAtIndex:0];
 NSString* dbPath=[paramArray objectAtIndex:1];
 
 
 ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];  
    
 NSData* sqliteData=[NSData dataWithContentsOfFile:[selectedProjectSite dbPath]];
 
 
 
 
 
 
 NSString *aBoundary = [NSString stringWithFormat:@"vYRNMz5SSe4vXrnc"];        
 NSMutableData *myData = [NSMutableData data];// [NSMutableData dataWithCapacity:1];
 NSString *myBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", aBoundary];
 NSString *closingBoundary = [NSString stringWithFormat:@"\r\n--%@--\r\n", aBoundary];    
 
 
 
 
 [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
 [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", appDele.defUserName] dataUsingEncoding:NSUTF8StringEncoding]];        
 [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
 
 
 [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", appDele.defUserPW] dataUsingEncoding:NSUTF8StringEncoding]];
 
 
 [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
 [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"sysID\"\r\n\r\n%d", appDele.activeStudioID] dataUsingEncoding:NSUTF8StringEncoding]];
 
 
 [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
 [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"siteID\"\r\n\r\n%d", selectedProjectSite.ID] dataUsingEncoding:NSUTF8StringEncoding]];    
 
 
 
 [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];    
 [myData appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"uploadFile\"; filename=\"OPS\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];    
 [myData appendData:[[NSString stringWithString:@"Content-Type: application/x-sqlite3\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
 [myData appendData:sqliteData];
 [myData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]]; //Ken
 //    
 //    
 //    
 
 
 
 
 
 
 
 
 
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *databaseFolderPath = [paths objectAtIndex:0]; 
 
 databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], [appDele activeLocalStudio].ID, [appDele activeLocalStudio].name, [appDele activeLocalStudio].iconName ]]; 	    
 
 sqlite3_stmt *attachedFileStmt; 
 const char* attachedFileSql=[[NSString stringWithFormat:@"select attachfile.fileName from attachfile where sent=0"] UTF8String];            
 sqlite3 *database = nil;    
 uint pAttachedFile=0;    
 if (sqlite3_open ([dbPath UTF8String], &database) == SQLITE_OK) {      
 if(sqlite3_prepare_v2(database, attachedFileSql, -1, &attachedFileStmt, NULL) == SQLITE_OK) {
 while( sqlite3_step(attachedFileStmt) == SQLITE_ROW) {                                 
 
 NSString* attachedFileName= nil;
 if ((char *) sqlite3_column_text(attachedFileStmt, 0)!=nil){
 attachedFileName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(attachedFileStmt, 0)];               
 }else{
 attachedFileName=[NSString stringWithFormat:@""];
 }                  
 
 if (![attachedFileName hasSuffix:@".jpg"]){
 continue;
 }
 
 
 //            NSString *FileParamConstant = @"image";
 
 // add image data and compress to send if needed.
 //            CGFloat compression         = 0.9f;
 //            CGFloat maxCompression = 0.1f;
 //            int maxFileSize             = 250*1024;
 
 NSString* imagePath=[databaseFolderPath stringByAppendingPathComponent:attachedFileName];
 
 NSData *imageData = [[NSData alloc] initWithContentsOfFile:imagePath];    
 //            while ([imageData length] > maxFileSize && compression > maxCompression)
 //            {
 //                compression -= 0.1;
 //                imageData = UIImageJPEGRepresentation([UIImage imageNamed:imagePath], compression);
 //            }
 
 //Assuming data is not nil we add this to the multipart form
 if (imageData) {
 
 [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];        
 //        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];                
 
 
 [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"AttachedFileName%04d\"; filename=\"%@\"\r\n",pAttachedFile,attachedFileName] dataUsingEncoding:NSUTF8StringEncoding]];    
 [myData appendData:[[NSString stringWithString:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];                                
 [myData appendData:imageData];
 [myData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
 }            
 pAttachedFile++;
 [imageData release];
 
 
 }
 }        
 sqlite3_close(database);
 }else{
 sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
 }  
 
 
 
 
 
 
 
 
 
 
 
 
 
 [myData appendData:[closingBoundary dataUsingEncoding:NSUTF8StringEncoding]];
 
 NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/postTestAction.php"];
 //    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=%d&siteID=%d&filename=OPS",[appDele activeLiveStudio].ID, [selectedProjectSite ID]];
 NSURL *url = [NSURL URLWithString: urlString];
 //    UIImage *image = [[UIImage imageWithData: [NSData dataWithContentsOfURL: url]] retain];
 
 
 
 NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:url
 cachePolicy:NSURLRequestUseProtocolCachePolicy
 timeoutInterval:900.0];
 
 
 [theRequest setHTTPMethod:@"POST"];
 
 
 
 NSString *postLength = [NSString stringWithFormat:@"%d", [myData length]];  
 [theRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];  
 
 NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", aBoundary];
 
 [theRequest setValue:contentType forHTTPHeaderField:@"Content-Type"];        
 
 
 [theRequest setHTTPBody:myData];
 
 //    NSString* t=[theRequest valueForHTTPHeaderField:@"Content-Type"];
 //    NSLog(@"%@",t);
 
 
 
 // create the connection with the request
 // and start loading the data
 // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file    
 //    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
 NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];    
 self.theConnection=aConnection;
 [aConnection release];
 if (theConnection) {
 // Create the NSMutableData that will hold
 // the received data
 // receivedData is declared as a method instance elsewhere
 if (connectionData){
 [connectionData release];
 }
 connectionData=[[NSMutableData data] retain];
 } else {    
 // Inform the user that the connection failed.        
 UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
 [connectFailMessage show];
 [connectFailMessage release];
 
 }   
 return  myData;
 }
 

 -(void) alertUploadResult:(id)sender title:(NSString*)title{
 UIAlertView *alert = [[UIAlertView alloc] 
 initWithTitle: title
 message: @""
 delegate: self
 cancelButtonTitle: nil
 otherButtonTitles: @"ok", nil];
 alert.tag = LocalProjectList_UIAlertViewTag_NoInternetConfirm; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
 [alert show];
 [alert release];
 
 
 }



#pragma mark NSURLConnection methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    //	CGFloat size = [[NSString stringWithFormat:@"%lli",[response expectedContentLength]] floatValue];
    
    
    //    NSHTTPURLResponse *hr = (NSHTTPURLResponse*)response;
    //    NSURLResponse *hr = (NSURLResponse*)response;    
    //    NSDictionary *dict = [hr allHeaderFields];
    
    
    //	CGFloat size = [[[response allHeaderFields] objectForKey:@"Content-Length"] unsignedIntegerValue];    
    //	CGFloat size = [[dict objectForKey:@"Content-Length"] unsignedIntegerValue];        
	//NSLog(@"filesize: %f", size);
    //	completeFileSize=size;
    
    
    
    
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
	
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
	
    // receivedData is an instance variable declared elsewhere.
    
    
//    downloadLabel.text=[NSString stringWithFormat:@"Downloading File from Server for Site: %@",projectSiteSelected.name]; 
    [connectionData setLength:0];
    
    
    
    
    
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
//    if ([response respondsToSelector:@selector(allHeaderFields)]) {
//        NSDictionary *dictionary = [httpResponse allHeaderFields];
//        NSLog(@"%@",[dictionary description]);
//
////        NSLog(@"%d",[response statusCode]);
//    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.            
    
    [connectionData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    // receivedData is declared as a method instance elsewhere
    
    [connection release];
    [connectionData release];
    
    // inform the user
    UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"didFailWithError"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
    [didFailWithErrorMessage show];
    [didFailWithErrorMessage release];
	
    //inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    // receivedData is declared as an instance variable elsewhere
    // in this example, convert data (from plist) to a string and then to a dictionary
    
    
    //    NSData *fetchedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"",projectID]];
    
    //    NSData *fetchedData = [NSData dataWithContentsOfURL:url];    
    
    

    
    NSString* dataStr=[[NSString alloc] initWithData:connectionData encoding:NSASCIIStringEncoding] ;
//    NSString* dataStr=[[NSString alloc] initWithData:connectionData encoding:NSUTF8StringEncoding] ;
    NSLog(@"RESPONSEDATA: %@",dataStr);
    
    [self alertUploadResult:nil title:dataStr];
    if ([dataStr isEqualToString:@"Upload Success"]){
        
        
        
        
        
        
        
        sqlite3_stmt *updateAttachCommentStmt; 
        const char* updateAttachCommentSQL=[[NSString stringWithFormat:@"update AttachComment set sent=1"] UTF8String];   
        
        sqlite3_stmt *updateAttachFileStmt; 
        const char* updateAttachFileSQL=[[NSString stringWithFormat:@"update AttachFile set sent=1"] UTF8String];    
        
//        update AttachFile set sent=1
        sqlite3 *database = nil;    
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        OPSProjectSite* selectedProjectSite=appDele.activeProjectSite;
        if (sqlite3_open ([[selectedProjectSite dbPath] UTF8String], &database) == SQLITE_OK) {      
            if(sqlite3_prepare_v2(database, updateAttachCommentSQL, -1, &updateAttachCommentStmt, NULL) == SQLITE_OK) {
                while( sqlite3_step(updateAttachCommentStmt) == SQLITE_ROW) { 
                    
                }
            }        
            
            if(sqlite3_prepare_v2(database, updateAttachFileSQL, -1, &updateAttachFileStmt, NULL) == SQLITE_OK) {
                while( sqlite3_step(updateAttachFileStmt) == SQLITE_ROW) { 
                    
                }
            }                   
            sqlite3_close(database);
        }else{
            sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
        }  

        
        
        
    }
    [dataStr release];
    
    
    
    

//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0]; 
//    
//    
//    NSString* databaseFolderPath=[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName],[appDele activeLiveStudio].ID,[appDele activeLiveStudio].name,[appDele activeLiveStudio].iconName]] ;
//    NSFileManager *NSFm= [NSFileManager defaultManager]; 
//    BOOL isDir=YES;    
//    if(![NSFm fileExistsAtPath:databaseFolderPath isDirectory:&isDir])
//        if(![NSFm createDirectoryAtPath:databaseFolderPath withIntermediateDirectories:isDir attributes:Nil error:Nil])
//            NSLog(@"Error: Create folder failed");
//    
//    //    [projectSite setDbPath: [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%@_I_%@/%@.sqlite",userName,activeLiveStudio.name,activeLiveStudio.iconName, [projectSite name]]] ];     
//    
//    NSString* dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.sqlite", [projectSiteSelected name]]] ];
//    [projectSiteSelected setDbPath: dbPath];   
//    [dbPath release];
//    
//    [connectionData writeToFile:projectSiteSelected.dbPath atomically:YES];    
//    
//    
//    //-----------------------------------Create PList File of project inside Studio---------------------------------------------------------------
//    NSMutableDictionary *dictionary=nil;
//    //    NSString *path = [[NSBundle mainBundle] bundlePath];        
//    
//    NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
//    if(![NSFm fileExistsAtPath:dictionaryPath isDirectory:NO]){        
//        if(![NSFm createFileAtPath:dictionaryPath contents:[NSData dataWithContentsOfFile:@""] attributes:Nil ]  )        {
//            NSLog(@"Error: Create projectInfoFile failed");
//        }
//        dictionary = [NSMutableDictionary dictionaryWithObject:[projectSiteSelected toDictionaryObject] forKey:projectSiteSelected.name];
//        
//    }else{
//        dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];           
//        [dictionary setObject:[projectSiteSelected toDictionaryObject] forKey:projectSiteSelected.name];  
//    }    
//    // write xml representation of dictionary to a file
//    [dictionary writeToFile:dictionaryPath atomically:YES];
//    //--------------------------------------------------------------------------------------------------    
//    
//    [self removeDownloadDisableOverlay];
//    [super openProjectToDisplay:projectSiteSelected];

    
    
    //    NSString *dataString = [[NSString alloc] initWithData: receivedData  encoding: 	
    //                            NSMutableDictionary *dictionary = [dataString propertyList];
    //                            [dataString release];
    
    //store data in singleton class for use by other parts of the program
    //                            SingletonObject *mySharedObject = [SingletonObject sharedSingleton];
    //                            mySharedObject.aDictionary = dictionary;
    
    //alert the user
    //alert the user
    //                            NSString *message = [[NSString alloc] initWithFormat:@"Succeeded! Received %d bytes of 	
    //                                                 data",[receivedData length]];
    //                                                 NSLog(message);
    //                                                 NSLog(@"Dictionary:\n%@",dictionary);
    //                                                 UIAlertView *finishedLoadingMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message:message  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
    //                                                 [finishedLoadingMessage show];
    //                                                 [finishedLoadingMessage release];
    //                                                 [message release];
    
    // release the connection, and the data object
//    [connection release],connection=nil;
    [theConnection release],theConnection=nil;
    [connectionData release],connectionData=nil;
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];
    
    

//    NSString* resultStr=[[NSString alloc] initWithData:connectionData encoding:NSASCIIStringEncoding] ;        
////    if ([resultStr isEqualToString:@"1"]){
////        
////    }else if ([resultStr isEqualToString:@"0"]){
////        
////    }
//    [self alertUploadResult:nil title:resultStr];
//    NSLog(@"%@",resultStr);
//    [resultStr release];
    
            
//    update AttachComment set sent=1;
//    update AttachFile set sent=1;
    
}
*/
//
//+(NSData *)dataForPOSTWithDictionary:(NSDictionary *)aDictionary boundary:(NSString *)aBoundary {
//    NSArray *myDictKeys = [aDictionary allKeys];
//    NSMutableData *myData = [NSMutableData dataWithCapacity:1];
//    NSString *myBoundary = [NSString stringWithFormat:@"--%@\r\n", aBoundary];
//    
//    for(int i = 0;i < [myDictKeys count];i++) {
//        id myValue = [aDictionary valueForKey:[myDictKeys objectAtIndex:i]];
//        [myData appendData:[myBoundary dataUsingEncoding:NSUTF8StringEncoding]];
//        //if ([myfValue class] == [NSString class]) {
//        if ([myValue isKindOfClass:[NSString class]]) {
//            [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", [myDictKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
//            [myData appendData:[[NSString stringWithFormat:@"%@", myValue] dataUsingEncoding:NSUTF8StringEncoding]];
//        } else if(([myValue isKindOfClass:[NSURL class]]) && ([myValue isFileURL])) {
//            [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", [myDictKeys objectAtIndex:i], [[myValue path] lastPathComponent]] dataUsingEncoding:NSUTF8StringEncoding]];
//            [myData appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//            [myData appendData:[NSData dataWithContentsOfFile:[myValue path]]];
//        } else if(([myValue isKindOfClass:[NSData class]])) {
//            [myData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", [myDictKeys objectAtIndex:i], [myDictKeys objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
//            [myData appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//            [myData appendData:myValue];
//        } // eof if()
//        
//        [myData appendData:[[NSString stringWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    } // eof for()
//    [myData appendData:[[NSString stringWithFormat:@"--%@--\r\n", aBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    return myData;
//} // eof dataForPOSTWithDictionary:boundary:


/*
- (void) copyModelFromServer:(NSArray*) paramArray{
    //- (void) copyModelFromServer{//:(OPSProjectSite*) projectSite{      
    bool bNoGraphicData=false;
    if (paramArray!=nil){
        NSNumber* numNoGraphicData=[paramArray objectAtIndex:0];
        bNoGraphicData=[numNoGraphicData boolValue];
    }
        
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];  
    //    NSString *urlString = [NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/saveSQLite.php?sysID=44&siteID=%d&filename=OPS", [project ID]];
                
    NSString *urlString = nil;
//    if (bNoGraphicData){        
//        urlString=[NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/updateSQLite.php?sysID=%d&siteID=%d&beta=1&siteInfoOnly=1",[appDele activeLiveStudio].ID, [[appDele activeProjectSite] ID]];
//    }else{
    urlString=[NSString stringWithFormat: @"https://www.onuma.com/plan/OPS-beta/export/updateSQLite.php?sysID=%d&siteID=%d&beta=1",[appDele activeLiveStudio].ID, [[appDele activeProjectSite] ID]];
//    }
    NSURL *url = [NSURL URLWithString: urlString];
    //    UIImage *image = [[UIImage imageWithData: [NSData dataWithContentsOfURL: url]] retain];
    
    
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:url
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:1200.0];
    // create the connection with the request
    // and start loading the data
    // note that the delegate for the NSURLConnection is self, so delegate methods must be defined in this file    
    //    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];    
    self.theConnection=aConnection;
    [aConnection release];
    if (theConnection) {
        // Create the NSMutableData that will hold
        // the received data
        // receivedData is declared as a method instance elsewhere
        
        connectionData=[[NSMutableData data] retain];
    } else {    
        // Inform the user that the connection failed.        
		UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"NSURLConnection " message:@"Failed in viewDidLoad"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[connectFailMessage show];
		[connectFailMessage release];        
    }
    
    
    return;        
}
*/

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self refreshTable];
    
//    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    [appDele removeLoadingViewOverlay];
//    


}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{


    // Navigation logic may go here. Create and push another view controller.
    

    if (![[tableData objectAtIndex:indexPath.row] isKindOfClass:[OPSProjectSite class]]){
        return;
    }

    OPSProjectSite* selectedProjectSite=[tableData objectAtIndex:indexPath.row];  

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    appDele.activeProjectSite=selectedProjectSite;
    
    
    uint appCurrentVersion=[appDele readAppCurrentVersion];    
    if (selectedProjectSite.version<appCurrentVersion){
        [selectedProjectSite readVersionLagStr];
        if ([selectedProjectSite bNeedUpdate]){
            [self alertSiteNeedUpdate:nil];            
        }else{            
            [super openProjectToDisplay:selectedProjectSite];
        }
        
        
        
        
        
        
        
        
        
//        NSLog(@"%@",selectedProjectSite.versionLagStr);        

//        
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];        
//        [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Generating File from Server for site: %@",projectSiteSelected.name] invoker:self completeAction:@selector(copyModelFromServer:) actionParamArray:[NSArray arrayWithObjects:[NSNumber numberWithBool:bNoGraphicData], nil]];   
//                
    }else{
        [super openProjectToDisplay:selectedProjectSite];
    }
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    [appDele setModelDisplayLevel:0];        
//    appDele.activeProjectSite=selectedProjectSite;
////    [selectedProjectSite release];    
//    [appDele displayViewModelNavCntr: nil];
    
    
}

-(void) attemptLaunchSite:(OPSProjectSite*) projectSite{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    

//    
//
//    
//    
//    if (![appDele isLiveDataSourceAvailable]){
//        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [noInternetMessage show];
//        [noInternetMessage release];
//        return;
//    }
//    
//    
//    
//    
//    OPSProjectSite* liveProjectSite=[appDele activeProjectSite];
//    OPSLocalProjectSite* localProjectSite=[[OPSLocalProjectSite alloc] initWithLiveOPSProjectSite:liveProjectSite];
//    
//    int localModifiedDate=-1;
//    bool isLocalProjectAttachmentEmpty=true;
//    
//    if (localProjectSite){
//        
//        localModifiedDate=[localProjectSite getLocalDatabaseModifiedDate];
//        if (localModifiedDate>=0){
//            isLocalProjectAttachmentEmpty=[localProjectSite isLocalProjectAttachmentEmpty];
//        }else{
//            [self downloadSite];
//            [localProjectSite release];
//            return;
//        }
//    }else{
//        [self downloadSite];
//        [localProjectSite release];
//        return;
//    }
//    
//    
//    int liveModifiedDate=[localProjectSite liveModifiedDate];
//    [localProjectSite release];
//    
//    
//    
//    
//    if (liveModifiedDate>localModifiedDate){
//        if (isLocalProjectAttachmentEmpty){
//            UIAlertView *alert = [[UIAlertView alloc]
//                                  initWithTitle: @"You downloaded this scheme before. But it has been modified since download, overwrite?"
//                                  message: @""
//                                  delegate: self
//                                  cancelButtonTitle: @"Overwrite"
//                                  otherButtonTitles: @"Cancel", nil];
//            alert.tag = LiveProjectListVC_OVERWRITE_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//            [alert show];
//            [alert release];
//        }else{
//            UIAlertView *alert = [[UIAlertView alloc]
//                                  initWithTitle: @"You downloaded this scheme before. But it has been modified since download. However, the local site has attachment yet to be uploaded"
//                                  message: @""
//                                  delegate: self
//                                  cancelButtonTitle: @"Canel"
//                                  otherButtonTitles: @"Upload First", @"Just Overwrite", nil];
//            alert.tag = LiveProjectListVC_UPLOADBEFOREOVERWRITE_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//            [alert show];
//            [alert release];
//        }
//        
//    }else{
//        UIAlertView *alert = [[UIAlertView alloc]
//                              initWithTitle: @"You downloaded this scheme before. It hasn't been modified since then. Launch your downloaded scheme?"
//                              message: @""
//                              delegate: self
//                              cancelButtonTitle: @"Yes"
//                              otherButtonTitles: @"No", nil];
//        alert.tag = LiveProjectListVC_LAUNCH_LOCAL_ALERT; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//        [alert show];
//        [alert release];
//    }
    ///////////////////////////////////////////////////////////////////////
    
    uint appCurrentVersion=[appDele readAppCurrentVersion];
    if (projectSite.version<appCurrentVersion){
        [projectSite readVersionLagStr];
        if ([projectSite bNeedUpdate]){
            [self alertSiteNeedUpdate:nil];
        }else{
            [super openProjectToDisplay:projectSite];
        }
    }else{
        [super openProjectToDisplay:projectSite];
    }

}

NSInteger compareProjectID(id param1, id param2, void *context) 
{
    
    int retour;
    // fist we need to cast all the parameters
    
    OPSProjectSite* projectSite1 = param1;
    OPSProjectSite* projectSite2 = param2;
    
    //make the comparaison
    
    if ((projectSite1.shared==projectSite2.shared)&& projectSite1.projectID < projectSite2.projectID)
        retour = NSOrderedAscending;
    else if ((projectSite1.shared==projectSite2.shared)&&projectSite1.projectID > projectSite2.projectID)
        retour = NSOrderedDescending;
    else
        retour = NSOrderedSame;
    
    
    return retour;
    
}

NSInteger compareShared(id param1, id param2, void *context) 
{
    
    int retour;
    // fist we need to cast all the parameters
    
    OPSProjectSite* projectSite1 = param1;
    OPSProjectSite* projectSite2 = param2;
    
    //make the comparaison
    if (projectSite1.shared < projectSite2.shared)
        retour = NSOrderedAscending;
    else if (projectSite1.shared > projectSite2.shared)
        retour = NSOrderedDescending;
    else
        retour = NSOrderedSame;
    
    
    return retour;
    
}



/*
- (void) readAttachedImageFromLocalSiteWithID:(uint)siteID imageArray:(NSArray*)_imageArray{        
    //    int errorCode=0;
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];

        
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *databaseFolderPath = [paths objectAtIndex:0]; 
    
    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], [appDele activeLocalStudio].ID, [appDele activeLocalStudio].name, [appDele activeLocalStudio].iconName ]]; 	    
    
    
    NSFileManager *manager = [NSFileManager defaultManager];    
    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:databaseFolderPath];        
    NSString *filename;    
    while ((filename = [direnum nextObject] )) {        
        
        if ([filename hasSuffix:@".jpg"]) {   
            
            NSMutableString *filenameOnly=[NSMutableString stringWithString:filename];
            [filenameOnly replaceCharactersInRange: [filenameOnly rangeOfString: @".jpg"] withString: @""];            
            NSMutableDictionary *dictionary=nil;            
            NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
            dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];  
            NSString* pListSt=[dictionary objectForKey:filenameOnly];                        
            OPSProjectSite* projectSite=[[OPSProjectSite alloc] init:pListSt];
            
            NSString* dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:filename ] ];
            projectSite.dbPath=dbPath;
            [dbPath release];    
            
            [projectArray addObject:projectSite];
            [projectSite release];    
            
        }        
        
        
    }   
    
    [projectArray sortUsingFunction:compareShared  context:nil];    
    [projectArray sortUsingFunction:compareProjectID  context:nil];
    
    
    
    
    NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"MY PROJECTS:",[appDele defUserName]];
    TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:0 iconName:nil];
    [tbCellTitleMyProjectStr release];
    [self.projectArray insertObject:tbCellTitleMyProject atIndex:0];
    [tbCellTitleMyProject release];
    
    uint pArray=1;
    while (pArray < [projectArray count]){
        id prevObj=[projectArray objectAtIndex:pArray-1];
        id thisObj=[projectArray objectAtIndex:pArray];
        if ([thisObj isKindOfClass:[OPSProjectSite class]]){
            if (  ((OPSProjectSite*) thisObj).shared==1  ){
                if (([prevObj isKindOfClass:[OPSProjectSite class]]&& ((OPSProjectSite*) prevObj).shared!=1 )||
                    ([prevObj isKindOfClass:[TableCellTitleProjectCat class]] && ((TableCellTitleProjectCat*) prevObj).shared!=1)
                    ) {
                    NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"READ ONLY PROJECTS:"];
                    TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:1 iconName:nil];
                    [tbCellTitleMyProjectStr release];
                    [self.projectArray insertObject:tbCellTitleMyProject atIndex:pArray];
                    [tbCellTitleMyProject release];
                    pArray++;
                    continue;
                    
                }
            }
            
        }
        
        if ([thisObj isKindOfClass:[OPSProjectSite class]]){
            if (  ((OPSProjectSite*) thisObj).shared==2  ){
                if (([prevObj isKindOfClass:[OPSProjectSite class]]&& ((OPSProjectSite*) prevObj).shared!=2 )||
                    ([prevObj isKindOfClass:[TableCellTitleProjectCat class]] && ((TableCellTitleProjectCat*) prevObj).shared!=2)
                    ) {
                    NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"READ / WRITE PROJECTS:"];
                    TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:2 iconName:nil];
                    [tbCellTitleMyProjectStr release];
                    [self.projectArray insertObject:tbCellTitleMyProject atIndex:pArray];
                    [tbCellTitleMyProject release];
                    pArray++;
                    continue;
                    
                }
            }
            
        }      
        if ([prevObj isKindOfClass:[TableCellTitleProjectCat class]]  ) {
            //Previous object is a project category class (My Projects | Read Only | Read Write)
            if ([thisObj isKindOfClass:[OPSProjectSite class]]){
                //This object is a projectSite. So must add project title to this location
                [self addProjectTitleToProjectArrayAtIndex:pArray];
                pArray++;
                continue;
            }
        }else if ([prevObj isKindOfClass:[OPSProjectSite class]] && [thisObj isKindOfClass:[OPSProjectSite class]]){
            if ( ((OPSProjectSite*) prevObj).projectID != ((OPSProjectSite*) thisObj).projectID ){
                //This object is a projectSite. So must add project title to this location
                [self addProjectTitleToProjectArrayAtIndex:pArray];
                pArray++;
                continue;
            }            
        }
        pArray++;
    }
    
    
    //    projectArrayAsLoaded = [projectArrayAsLoaded sortedArrayUsingSelector:@selector(compareShared:)];
    
    
    
    
    if (projectArray==Nil || [projectArray count]<=0 ) {
        return false;        
    }else{
        return true;
    }
    
    
}

*/

- (BOOL) readSiteListFromLocal{        
    //    int errorCode=0;


    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//        uint currentVersion=[self readVersion];
//    uint currentVesion=[appDele readAppCurrentVersion];
    
//    self.projectArray=[[NSMutableArray alloc] init];
//    NSMutableArray* projectArrayAsLoaded=[[NSMutableArray alloc] init];
    NSMutableArray* _projectArray=[[NSMutableArray alloc] init];    
    self.projectArray=_projectArray;
    [_projectArray release];
                  
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *databaseFolderPath = [paths objectAtIndex:0]; 
    
    databaseFolderPath=[databaseFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/%d_N_%@_I_%@",[appDele defUserName], [appDele activeLocalStudio].ID, [appDele activeLocalStudio].name, [appDele activeLocalStudio].iconName ]]; 	    
    

    
    
    //    NSMutableArray* studioFolder=[DataConnect arrayOfFoldersInFolder:documentsDirectory];
    
    
    //    NSString *bundleRoot = [[NSBundle mainBundle] resourcePath];        
    NSFileManager *manager = [NSFileManager defaultManager];    
    //    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:bundleRoot];
    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:databaseFolderPath];    
    
    NSString *filename;
    
//    int pProjectID=0;
    while ((filename = [direnum nextObject] )) {
        

                
        if ([filename isEqualToString:@"tmpSiteInfo.sqlite"]){
            [[NSFileManager defaultManager] removeItemAtPath:[databaseFolderPath stringByAppendingPathComponent:filename ] error:nil];
            continue;
        }
        if ([filename hasSuffix:@".sqlite"] && ![filename isEqualToString:@"tmpSiteInfo.sqlite"]) {   //change the suffix to what you are looking for
//            OPSProjectSite* projectSite=[[OPSProjectSite alloc] init:0 name:@"aa" shared:0 projectID:1 projectName:@"bb" iconName:@""]; 
            
            NSMutableString *filenameOnly=[NSMutableString stringWithString:filename];
            [filenameOnly replaceCharactersInRange: [filenameOnly rangeOfString: @".sqlite"] withString: @""];
            NSMutableDictionary *dictionary=nil;            
            NSString *dictionaryPath = [databaseFolderPath stringByAppendingPathComponent:@"StudioProjectList.plist"];    
            dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:dictionaryPath];  
            NSString* pListSt=[dictionary objectForKey:filenameOnly];                        
            OPSProjectSite* projectSite=[[OPSProjectSite alloc] init:pListSt];
 
            NSString* dbPath=[[NSString alloc ] initWithString: [databaseFolderPath stringByAppendingPathComponent:filename ] ];
            projectSite.dbPath=dbPath;
            [dbPath release];    

            [self.projectArray addObject:projectSite];
            [projectSite release];

        }        
        
        
    }   
    
    [self.projectArray sortUsingFunction:compareShared  context:nil];
    [self.projectArray sortUsingFunction:compareProjectID  context:nil];
    
    
    uint pArray=0;
    
//    if ([[projectArray objectAtIndex:0] shared]==0){        
//    if (![[projectArray objectAtIndex:1] isKindOfClass:[TableCellTitleProjectCat class]]){
        NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithString: @"MY PROJECTS:"];
        TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:0 iconName:nil];
        [tbCellTitleMyProjectStr release];
        [self.projectArray insertObject:tbCellTitleMyProject atIndex:0];
        [tbCellTitleMyProject release];
        
        pArray=1;
//    }else{                        
//        
//        
//    }
//    
    
    while (pArray < [projectArray count]){
        id prevObj=[projectArray objectAtIndex:pArray-1];
        id thisObj=[projectArray objectAtIndex:pArray];
        if ([thisObj isKindOfClass:[OPSProjectSite class]]){
            if (  ((OPSProjectSite*) thisObj).shared==1  ){
                if (([prevObj isKindOfClass:[OPSProjectSite class]]&& ((OPSProjectSite*) prevObj).shared!=1 )||
                    ([prevObj isKindOfClass:[TableCellTitleProjectCat class]] && ((TableCellTitleProjectCat*) prevObj).shared!=1)
                    ) {
                    NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"READ ONLY PROJECTS:"];
                    TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:1 iconName:nil];
                    [tbCellTitleMyProjectStr release];
                    [self.projectArray insertObject:tbCellTitleMyProject atIndex:pArray];
                    [tbCellTitleMyProject release];
                    pArray++;
                    continue;
                    
                }
            }
                
        }
        
        if ([thisObj isKindOfClass:[OPSProjectSite class]]){
            if (  ((OPSProjectSite*) thisObj).shared==2  ){
                if (([prevObj isKindOfClass:[OPSProjectSite class]]&& ((OPSProjectSite*) prevObj).shared!=2 )||
                    ([prevObj isKindOfClass:[TableCellTitleProjectCat class]] && ((TableCellTitleProjectCat*) prevObj).shared!=2)
                    ) {
                    NSString* tbCellTitleMyProjectStr=[[NSString alloc] initWithFormat: @"READ / WRITE PROJECTS:"];
                    TableCellTitleProjectCat* tbCellTitleMyProject=[[TableCellTitleProjectCat alloc] init:0 name:tbCellTitleMyProjectStr shared:2 iconName:nil];
                    [tbCellTitleMyProjectStr release];
                    [self.projectArray insertObject:tbCellTitleMyProject atIndex:pArray];
                    [tbCellTitleMyProject release];
                    pArray++;
                    continue;
                    
                }
            }
            
        }      
        if ([prevObj isKindOfClass:[TableCellTitleProjectCat class]]  ) {
        //Previous object is a project category class (My Projects | Read Only | Read Write)
            if ([thisObj isKindOfClass:[OPSProjectSite class]]){
            //This object is a projectSite. So must add project title to this location
                [self addProjectTitleToProjectArrayAtIndex:pArray];
                pArray++;
                continue;
            }
        }else if ([prevObj isKindOfClass:[OPSProjectSite class]] && [thisObj isKindOfClass:[OPSProjectSite class]]){
            if ( ((OPSProjectSite*) prevObj).projectID != ((OPSProjectSite*) thisObj).projectID ){
                //This object is a projectSite. So must add project title to this location
                [self addProjectTitleToProjectArrayAtIndex:pArray];
                pArray++;
                continue;
            }            
        }
        pArray++;
    }
    
    
//    projectArrayAsLoaded = [projectArrayAsLoaded sortedArrayUsingSelector:@selector(compareShared:)];
    

    
    
    if (projectArray==Nil || [projectArray count]<=0 ) {
        return false;        
    }else{
        return true;
    }
    
    
}

                       
-(void) addProjectTitleToProjectArrayAtIndex:(uint) index{
    OPSProjectSite* projectSite=[projectArray objectAtIndex:index];
    NSString* tbCellTitleProjectNameStr=[[NSString alloc] initWithFormat: @"Project: %@",projectSite.projectName];
    //            [tbCellTitleProject release];
    TableCellTitleProject* tbCellTitleProject=[[TableCellTitleProject alloc] init:projectSite.projectID name:tbCellTitleProjectNameStr shared:projectSite.shared iconName:nil];
    [tbCellTitleProjectNameStr release];
    [self.projectArray insertObject:tbCellTitleProject atIndex:index];             
    [tbCellTitleProject release];
}
-(void) loadView{
    [super loadView];    
    int height = 44;//self.frame.size.height;
    int width = 800;
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor blackColor];
    navLabel.font = [UIFont boldSystemFontOfSize:16];
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text=self.title;
    self.navigationItem.titleView = navLabel;
    [navLabel release]; 
}








-(void) refreshTable{

    [self readSiteListFromLocal];
    [tableData release];
    tableData=[projectArray copy];
    [self.theTableView reloadData];
//    [self.theTableView reloadData];
//    [self.theTableView performSelectorOnMainThread:@selector(reloadData:) withObject:nil waitUntilDone:NO];
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([appDele containLocalDatabase]){

        [self refreshTable];
    }else{

        ViewNoLocalDB* viewNoLocalDB=[[ViewNoLocalDB alloc] initWithNibName:@"ViewNoLocalDB" bundle:[NSBundle mainBundle]];                 
        [[self navigationController] pushViewController:viewNoLocalDB animated:YES];
        [viewNoLocalDB release];         
        
    }    
     
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {    
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  [super tableView:tableView cellForRowAtIndexPath:indexPath];
}
@end

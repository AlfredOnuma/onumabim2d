//
//  Furn.m
//  ProjectView
//
//  Created by Alfred Man on 12/6/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//
#import "BitmapRep.h"
#import "Furn.h"
#import "DisplayInfo.h"

#import <sqlite3.h>
#import "ProjectViewAppDelegate.h"

@implementation Furn
//(OPSModel*) _model database:
@synthesize tmpBitmapRepForDBReading;
@synthesize componentName;
//@synthesize isCascadedFurn=_isCascadedFurn;
//@synthesize furnZ;

-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    Furn* newFurn=[[Furn alloc] init:self.model ID:self.ID guid:self.GUID name:self.name] ;
    
    if (placement!=nil){
        Placement* _placement=[[Placement alloc] init:placement.ID guid:placement.GUID name:placement.name pt:placement.pt angle:placement.angle isMirrorY:placement.mirrorY];
        newFurn.placement=_placement;
        [_placement release];
//        [newFurn setPlacement:[placement copy]];
    }
    if (displayInfo!=nil ){
        DisplayInfo* _displayInfo=[[DisplayInfo alloc] initWithProduct:newFurn displacement:CGPointMake(displayInfo.displacement.x, displayInfo.displacement.y)];        
        newFurn.displayInfo=_displayInfo;
        [_displayInfo release];        
    }
    if (tmpBitmapRepForDBReading!=nil){
//        [newFurn setTmpBitmapRepForDBReading: [[BitmapRep alloc] in
        BitmapRep* _bitmapRep=[tmpBitmapRepForDBReading copyWithZone:zone];
        newFurn.tmpBitmapRepForDBReading=_bitmapRep;
        [_bitmapRep release];
    }
    return newFurn;
}


- (id) initWithFurnSQLStmt_mustSetParentTransformLater: (OPSModel*) model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*)sqlStmt isGhost:(bool)isGhost// isCascadedFurn:(bool)isCascadedFurn
{
    self.model=model;
    
    NSInteger furnID = sqlite3_column_int(sqlStmt, 0);          
    NSString* furnGuid= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 1)!=nil){
        furnGuid=[[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 1)];
//        furnGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 1)];               
    }else{
        furnGuid=[[NSString alloc]initWithFormat:@""];
//        furnGuid=[NSString stringWithFormat:@""];
    }
    
    NSString* furnName= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 2)!=nil){
        furnName=[[NSString alloc]initWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 2)];
//        furnName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 2)];
        
    }else{
        furnName=[[NSString alloc] initWithFormat:@""];
//        furnName=[NSString stringWithFormat:@""];
    }
    
    self=[super init:model ID:furnID  guid:furnGuid name:furnName];
    [furnGuid release]; furnGuid=nil;
    [furnName release]; furnName=nil;
    if (self){
        
        [self setIsGhostObject:isGhost];
//        self.isCascadedFurn=isCascadedFurn;
        CGFloat furnPlacementX = sqlite3_column_double(sqlStmt, 3);//*multiFactor;
        CGFloat furnPlacementY = sqlite3_column_double(sqlStmt, 4);//*multiFactor;                          
        //    CGFloat furnPlacementZ = sqlite3_column_double(sqlStmt, 5);//*multiFactor;                          
        CGFloat furnPlacementAngle = sqlite3_column_double(sqlStmt, 6); 
        
        NSString* furnDataName=nil;
        if ((char *) sqlite3_column_text(sqlStmt, 7)!=nil){
            furnDataName= [[NSString alloc]initWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 7)];
//            furnDataName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 7)];
        }else{
            furnDataName=[[NSString alloc]initWithFormat:@"Placeholder"];
//            furnDataName=[NSString stringWithFormat:@"Placeholder"];
        }
        
        NSInteger furnInsertPoint = sqlite3_column_int(sqlStmt, 8);          
        NSInteger furnFixedSize = sqlite3_column_int(sqlStmt, 9);          
        NSInteger furnMirrorY = sqlite3_column_int(sqlStmt, 10);
        
        CGFloat furnDimensionX=sqlite3_column_double(sqlStmt, 11);
        CGFloat furnDimensionY=sqlite3_column_double(sqlStmt, 12);
        CGFloat furnDimensionZ=sqlite3_column_double(sqlStmt, 13);        
                
        NSString* tmpComponentName=[[NSString alloc] initWithString:[ProjectViewAppDelegate readNameFromSQLStmt:sqlStmt column:14]];
        self.componentName=tmpComponentName;
        [tmpComponentName release];

//        [_componentName release];
        
        
        
        
        
        CGPoint furnPlacementPt=CGPointMake(furnPlacementX, furnPlacementY); 
        
        Placement* furnPlacement=[[Placement alloc]  init:0 guid:nil name:nil pt:furnPlacementPt angle:furnPlacementAngle isMirrorY:furnMirrorY];    
        
//        CGAffineTransform furnTransform=CGAffineTransformConcat([furnPlacement toCGAffineTransform], parentTransform);
        
        
        [self setPlacement:furnPlacement];
        [furnPlacement release];
        
        
        CGFloat furnLabelDisplacementX = sqlite3_column_double(sqlStmt, 15);         
        CGFloat furnLabelDisplacementY = sqlite3_column_double(sqlStmt, 16); 
        
//        self.furnZ=furnLabelDisplacementZ;
        

        
//        if ( isGhost){
////            NSLog(@"GhsotFurn");
//        }
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication]delegate];
        if (!isGhost && [appDele modelDisplayLevel]==2){

            DisplayInfo* _displayInfo=[[DisplayInfo alloc] initWithProduct:self displacement:CGPointMake(furnLabelDisplacementX, furnLabelDisplacementY)];
            self.displayInfo=_displayInfo;
            [_displayInfo release];
        }

        tmpBitmapRepForDBReading=[[BitmapRep alloc] init:0 guid:nil name:furnDataName insertPoint:furnInsertPoint fixedSize:(furnFixedSize>0) mirrorY:(furnMirrorY!=0) dimensionX:furnDimensionX dimensionY:furnDimensionY dimensionZ:furnDimensionZ];
        
        [furnDataName release];furnDataName=nil;
        /*
         BitmapRep* bitmapRep=[[BitmapRep alloc] init:0 guid:nil name:furnDataName insertPoint:furnInsertPoint fixedSize:(furnFixedSize>0) mirrorY:(furnMirrorY>0) dimensionX:furnDimensionX dimensionY:furnDimensionY dimensionZ:furnDimensionZ];
        //                    [furnDataName release];
        Representation* rep=[[Representation alloc] init:(0) guid:(nil) name:(nil)];
        [rep addARepresentationItem:(bitmapRep)];                      
        [bitmapRep release];    
        [self setRepresentation:rep];  
        [rep release];     
         */
            
        
    }
    return self;
    
}
-(void) setParentTransformAndSetupRepresentation:(CGAffineTransform) parentTransform{    
    CGAffineTransform furnTransform=CGAffineTransformConcat([self.placement toCGAffineTransform], parentTransform);
    
    
    [tmpBitmapRepForDBReading setBBoxinRootContextWithParentTransform:furnTransform];
    [tmpBitmapRepForDBReading setBBoxinLocalFrameWithLocalTransform:[self.placement toCGAffineTransform]];
    
    Representation* rep=[[Representation alloc] init:(0) guid:(nil) name:(nil)];
    [rep addARepresentationItem:tmpBitmapRepForDBReading];                      
//    [tmpBitmapRepForDBReading release];    
    [self setRepresentation:rep];  
    [rep release];  
    
    
//    for (RepresentationItem* repItem in [representation aRepresentationItem]){
//        if ([repItem isKindOfClass:[BitmapRep class]]){
//            [((BitmapRep*) repItem) setBBoxinRootContextWithParentTransform:furnTransform];
//        }
//
//    }
}

/*
-(id) initWithFurnSQLStmt: (OPSModel*) _model database:(sqlite3*) database sqlStmt: (sqlite3_stmt*)sqlStmt parentTransform:(CGAffineTransform) parentTransform{
    
    //                    NSInteger slabID = sqlite3_column_int(slabStmt, 0);
    //                    NSInteger furnPolylineID = sqlite3_column_int(sqlStmt, 2);                                        
    //                    NSString* slabGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(slabStmt, 2)];
    //                                        [slabGuid autorelease];                                              
    //                    CGFloat slabThk=sqlite3_column_double(slabStmt, 3);
    
    self.model=_model;    
    NSInteger furnID = sqlite3_column_int(sqlStmt, 0);          
    NSString* furnGuid= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 1)!=nil){
        furnGuid= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 1)];               
    }else{
        furnGuid=[NSString stringWithFormat:@""];
    }                              
    
    NSString* furnName= nil;
    if ((char *) sqlite3_column_text(sqlStmt, 2)!=nil){
        furnName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 2)];
        
    }else{
        furnName=[NSString stringWithFormat:@""];
    }
    
    self=[super init:_model ID:furnID  guid:furnGuid name:furnName];
    if (self){
    
        
        CGFloat furnPlacementX = sqlite3_column_double(sqlStmt, 3);// *multiFactor;                           
        CGFloat furnPlacementY = sqlite3_column_double(sqlStmt, 4);// *multiFactor;                          
        //    CGFloat furnPlacementZ = sqlite3_column_double(sqlStmt, 5);// *multiFactor;                          
        CGFloat furnPlacementAngle = sqlite3_column_double(sqlStmt, 6); 
        
        NSString* furnDataName=nil;
        if ((char *) sqlite3_colum n_text(sqlStmt, 7)!=nil){
            furnDataName= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStmt, 7)];
        }else{
            furnDataName=[NSString stringWithFormat:@"Placeholder"];
        }
        
        NSInteger furnInsertPoint = sqlite3_column_int(sqlStmt, 8);  
        
        NSInteger furnFixedSize = sqlite3_column_int(sqlStmt, 9);  
        
        NSInteger furnMirrorY = sqlite3_column_int(sqlStmt, 10);
        
        CGFloat furnDimensionX=sqlite3_column_double(sqlStmt, 11);
        CGFloat furnDimensionY=sqlite3_column_double(sqlStmt, 12);
        CGFloat furnDimensionZ=sqlite3_column_double(sqlStmt, 13);
        
        

        
        
        //                    [furnName release];
        
        
        
        
        CGPoint furnPlacementPt=CGPointMake(furnPlacementX, furnPlacementY); 
        
        Placement* furnPlacement=[[Placement alloc]  init:0 guid:nil name:nil pt:furnPlacementPt angle:furnPlacementAngle];    
        
        CGAffineTransform furnTransform=CGAffineTransformConcat([furnPlacement toCGAffineTransform], parentTransform);
        
        
        [self setPlacement:furnPlacement];
        [furnPlacement release];
        
        //                                        [floor addElement:slab];
        
        CGFloat furnLabelDisplacementX = sqlite3_column_double(sqlStmt, 14);         
        CGFloat furnLabelDisplacementY = sqlite3_column_double(sqlStmt, 15); 
        
//        [self setLabelDisplacement:CGPointMake(furnLabelDisplacementX, furnLabelDisplacementY)];
        
        
        
        DisplayInfo* _displayInfo=[[DisplayInfo alloc] initWithProduct:self displacement:CGPointMake(furnLabelDisplacementX, furnLabelDisplacementY)];        
        self.displayInfo=_displayInfo;
        [_displayInfo release];
        
//        NSLog(@"BitmapName: %@",furnDataName);
        
        
        
        BitmapRep* bitmapRep=[[BitmapRep alloc] init:0 guid:nil name:furnDataName insertPoint:furnInsertPoint fixedSize:(furnFixedSize>0) mirrorY:(furnMirrorY>0) dimensionX:furnDimensionX dimensionY:furnDimensionY dimensionZ:furnDimensionZ parentTransform:furnTransform];
        //                    [furnDataName release];
        Representation* rep=[[Representation alloc] init:(0) guid:(nil) name:(nil)];
        [rep addARepresentationItem:(bitmapRep)];                      
        [bitmapRep release];    
        [self setRepresentation:rep];  
        [rep release];
    }
    return self;

}
 */
/*

-init: (OPSModel*) _model ID:(NSInteger) _ID guid:(NSString*) _GUID name:(NSString*) _name{    
    self=[super init:_model ID:_ID  guid:_GUID name:_name];
    if (self){
    }
    return self;
}
*/
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/
- (void)dealloc
{
    [tmpBitmapRepForDBReading release];tmpBitmapRepForDBReading=nil;
    [componentName release];componentName=nil;
    [super dealloc];
}

/*
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
 */

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/
/*

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end

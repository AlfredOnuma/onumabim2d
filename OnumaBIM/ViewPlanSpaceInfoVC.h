//
//  ViewPlanSpaceInfoVC.h
//  ProjectView
//
//  Created by Alfred Man on 5/18/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "ViewPlanInfoVC.h"

@interface ViewPlanSpaceInfoVC : ViewPlanInfoVC
{
    
    
    UILabel* uiSpaceName;
    UILabel* uiSpaceNumber;
    UILabel* uiNetCalculated;
    UILabel* uiHeight;
    UILabel* uiCapacity;
    UILabel* uiOccupancy;
    UILabel* uiNumComponents;
    UILabel* uiDepartment;
    NSMutableArray* aUICustomInt;
    NSMutableArray* aUICustom;
    
    
//    NSString* _spaceNumber;
//    double _netCalculated;
//    double _height;
//    double _capacity;
//    double _occupancy;
//    uint _numComponents;
//    NSString* department;
    

}

@property (nonatomic, retain) UILabel* uiSpaceName;
@property (nonatomic, retain) UILabel* uiSpaceNumber;
@property (nonatomic, retain) UILabel* uiNetCalculated;
@property (nonatomic, retain) UILabel* uiHeight;
@property (nonatomic, retain) UILabel* uiCapacity;
@property (nonatomic, retain) UILabel* uiOccupancy;
@property (nonatomic, retain) UILabel* uiNumComponents;
@property (nonatomic, retain) UILabel* uiDepartment;
@property (nonatomic, retain) NSMutableArray* aUICustomInt;
@property (nonatomic, retain) NSMutableArray* aUICustom;
@end

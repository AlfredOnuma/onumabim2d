//
//  SiteViewTableController.h
//  OPSMobile
//
//  Created by Alfred Man on 10/6/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FloorViewTableController;

@interface SiteViewTableController : UITableViewController {

    FloorViewTableController* floorViewTableController;
}

@property (nonatomic, retain) IBOutlet FloorViewTableController *floorViewTableController;
- (void) jumptToBldg;
@end

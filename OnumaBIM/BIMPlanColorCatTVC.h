//
//  BIMPlanColorCatTVC.h
//  ProjectView
//
//  Created by Alfred Man on 4/14/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, sitePlanColorCat){
    sitePlan_NONE,
    sitePlan_CONSTRUCTION_STATUS,
    sitePlan_FUSION_BLDG_STATUS,
    sitePlan_FUSION_BLDG_FCI,
    sitePlan_FUSION_BLDG_STATUS_FLAG,
    sitePlan_CUSTOM
};

typedef NS_ENUM(NSInteger, floorPlanColorCat){
    floorPlan_NONE,
    floorPlan_DEPARTMENT,
    floorPlan_VERTICAL_ALIGNMENT,
    floorPlan_FUSION_PLANNEDSFASSIGNEDSF,
    floorPlan_FUSION_SPACE_STATUS_FLAG,
    floorPlan_FUSION_RECORDSTATUS,
    floorPlan_FUSION_TOP,
    floorPlan_FUSION_PGM,
    floorPlan_FUSION_ROOM_USE,
    floorPlan_CUSTOM
};
//@class BIMReportCatTVC;
//@class BIMPlanColorCatNVC;
//@protocol BIMPlanColorCatTVCDelegate 
//@end
@interface BIMPlanColorCatTVC : UITableViewController{// <BIMPlanColorCatTVCDelegate>{    
//    id <BIMPlanColorCatTVCDelegate> delegate;
//    ViewModelToolbar* _viewModelToolbar;
    NSMutableArray* _aColorCatTypeForRow;
}
@property (nonatomic, retain) NSMutableArray* aColorCatTypeForRow;
//@property (nonatomic, assign) ViewModelToolbar* viewModelToolbar;
//@property (nonatomic, assign) id <BIMPlanColorCatTVCDelegate> delegate;

//- (id) init: (ViewModelToolbar*) viewModelToolbar;// (BIMReportCatTVC*) bimReportCatTVC;
//-(uint) numFixedColorCatOnFloorLevel;
//-(uint) numFixedColorCatOnSiteLevel;
//

//-(uint) numColorCatOnFloorLevel;
//-(uint) numColorCatOnSiteLevel;
@end

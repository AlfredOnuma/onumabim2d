//
//  TableCellTitleProject.m
//  ProjectView
//
//  Created by Alfred Man on 1/3/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import "TableCellTitleProject.h"


@implementation TableCellTitleProject

-(id) init:(uint)_ID name:(NSString *)_name shared:(uint)_shared iconName:(NSString *)_iconName{
    self=[super init:_ID name:_name shared:_shared iconName:_iconName];
    if (self){
        
    }
    return self;
}



-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    TableCellTitleProject *copiedTableCellTitleProject = [[TableCellTitleProject allocWithZone:zone] init:ID name:name shared:shared  iconName:iconName];
    //    if (dbPath){
    //        copiedProjectSite.dbPath=dbPath;
    //    }
    //    another.obj = [obj copyWithZone: zone];
    
    return copiedTableCellTitleProject;
}



/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/
- (void)dealloc
{
    [super dealloc];
}
/*
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
*/
#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/
/*
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end

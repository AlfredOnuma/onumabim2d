//
//  NavScrollUIToolbar.h
//  ProjectView
//
//  Created by Alfred Man on 11/24/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ViewModelVC;
#import "ViewModelToolbar.h"
#import "BIMReportCatTVC.h"
@interface NavScrollUIToolbar : ViewModelToolbar <BIMReportCatTVCDelegate, UIPopoverControllerDelegate> {

}

- (id)initWithFrame:(CGRect)frame viewModelVC:(ViewModelVC*)_viewModelVC;

- (void) report:(id)sender;

- (void) attach:(id)sender;

//- (void) issue:(id)sender;

- (void) bimmail:(id)sender;


- (void) bim:(id)sender;
- (void) tools:(id)sender;
/*
- (void) insert:(id)sender;

- (void) marquee:(id)sender;

- (void) ruler:(id)sender;

- (void) textstyle:(id)sender;

- (void) undo:(id)sender;
- (void) redo:(id)sender;
 */
@end

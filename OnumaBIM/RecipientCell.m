//
//  RecipientCell.m
//  ProjectView
//
//  Created by Alfred Man on 4/8/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "RecipientCell.h"
#import "BIMMailVC.h"
#import "BIMMailGroup.h"
#import "BIMMailRecipient.h"
@implementation RecipientCell

@synthesize nameLabel;
@synthesize roleLabel;
@synthesize checkButton;
@synthesize checked;
@synthesize delegate;

- (IBAction) checkAction:(id)sender event:(id)event
{
//    BIMMailVC* bimmailVC=(BIMMailVC*)[self superview];
    BIMMailVC* bimmailVC=(BIMMailVC*) self.delegate;
//    uint groupID=[[bimmailVC.recipientTable indexPathForSelectedRow] section];
//    uint rowID=[[bimmailVC.recipientTable indexPathForSelectedRow] row];
    
    
    
        
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:bimmailVC.recipientTable];
    
	NSIndexPath *indexPath = [bimmailVC.recipientTable indexPathForRowAtPoint: currentTouchPosition];
    //	if (indexPath != nil)
    //	{
    //		[self tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    //	}
    
    if (indexPath==nil){
        return ;
    }    
    
    
    uint groupID=[indexPath section];
    uint rowID=[indexPath row];
    
    
    BIMMailGroup* recipientGroup=[bimmailVC.aRecipientGroup objectAtIndex:groupID];
    NSArray* aRecipientList=recipientGroup.aBIMMailRecipient;
                             
    if (rowID==0) {
        recipientGroup.checked=!recipientGroup.checked;
        for (BIMMailRecipient* groupRecipient in aRecipientList){
            groupRecipient.checked=recipientGroup.checked;
            
            
            
            
            
            uint pGroup=0;
            for (BIMMailGroup* group in bimmailVC.aRecipientGroup){
                if (pGroup==groupID) {
                    pGroup++;
                    continue;
                }
                for (BIMMailRecipient* recipientInOtherGroup in group.aBIMMailRecipient){
                    if (recipientInOtherGroup.ID==groupRecipient.ID){
                        recipientInOtherGroup.checked=groupRecipient.checked;
                    }
                }
                pGroup++;
            }
            
            
            
        }
//        [bimmailVC.recipientTable reloadData];
    }else{
        
        
        BIMMailRecipient* recipient=[aRecipientList objectAtIndex:(rowID-1) ];
        recipientGroup.checked=false;

        recipient.checked=!recipient.checked;

        uint pGroup=0;
        for (BIMMailGroup* group in bimmailVC.aRecipientGroup){
            if (pGroup==groupID) {
                pGroup++;
                continue;
            }
            for (BIMMailRecipient* recipientInOtherGroup in group.aBIMMailRecipient){
                if (recipientInOtherGroup.ID==recipient.ID){
                    recipientInOtherGroup.checked=recipient.checked;
                }
            }
            pGroup++;
        }

        
        UIImage *checkImage = (recipient.checked) ? [UIImage imageNamed:@"checked.png"] : [UIImage imageNamed:@"unchecked.png"];
        [checkButton setImage:checkImage forState:UIControlStateNormal];  
        
//        [bimmailVC.recipientTable reloadData];
    }
    
    
    
    
    //Check whether the group is all selected or all deselected****************************************************

    for (BIMMailGroup* recipientGroup in bimmailVC.aRecipientGroup){
        
        uint overallCheckFlag=0; //0=initial State, 1=all Checked, 2=all unchecked, 3=not some check some unchecked
        for (uint pBIMMailRecipient=0; pBIMMailRecipient<[recipientGroup.aBIMMailRecipient count];pBIMMailRecipient++){
            BIMMailRecipient* recipient=[recipientGroup.aBIMMailRecipient objectAtIndex:pBIMMailRecipient];
//        for (BIMMailRecipient* recipient in recipientGroup.aBIMMailRecipient){
            uint thisCheckFlag=recipient.checked?1:2;
            if (overallCheckFlag==0){
                overallCheckFlag=thisCheckFlag;
            }else{
                if (overallCheckFlag!=thisCheckFlag){
                    overallCheckFlag=3;
                    break;
                }
                
            }
            
        }
        if (overallCheckFlag==1){
            recipientGroup.checked=true;
            
//            
//            uint overallCheckFlag=0; //0=initial State, 1=all Checked, 2=all unchecked, 3=not some check some unchecked
//            for (uint pBIMMailRecipient=0; pBIMMailRecipient<[recipientGroup.aBIMMailRecipient count];pBIMMailRecipient++){
//                BIMMailRecipient* recipient=[recipientGroup.aBIMMailRecipient objectAtIndex:pBIMMailRecipient];
//                //        for (BIMMailRecipient* recipient in recipientGroup.aBIMMailRecipient){
//                uint thisCheckFlag=recipient.checked?1:2;
//                if (overallCheckFlag==0){
//                    overallCheckFlag=thisCheckFlag;
//                }else{
//                    if (overallCheckFlag!=thisCheckFlag){
//                        overallCheckFlag=3;
//                        break;
//                    }
//                    
//                }
//                
//            }
        }else if (overallCheckFlag==2){
            recipientGroup.checked=false;
        }
    }
    

    //************************************************************************************************************
    
    [bimmailVC.recipientTable reloadData];
    
    
    
//    if (rowID==0){
//
//    }
//
//       for (BIMMailGroup* group in aRecipientGroup){
//        for (BIMMailRecipient* recipient in group.aBIMMailRecipient){
//            NSLog(@"name:%@",recipient.name);
//        }
//    }
	// note: we don't use 'sender' because this action method can be called separate from the button (i.e. from table selection)
//	self.checked = !self.checked;
//	UIImage *checkImage = (self.checked) ? [UIImage imageNamed:@"checked.png"] : [UIImage imageNamed:@"unchecked.png"];
//	[checkButton setImage:checkImage forState:UIControlStateNormal];
}



-(void)dealloc{
    [nameLabel release];nameLabel=nil;
    [roleLabel release];roleLabel=nil;
    [checkButton release];checkButton=nil; 
    [super dealloc];
}
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.checked=false;
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
 */

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

@end

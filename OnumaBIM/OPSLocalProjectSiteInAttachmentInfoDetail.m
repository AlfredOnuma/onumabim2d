//
//  OPSLocalProjectSiteInAttachmentInfoDetail.m
//  ProjectView
//
//  Created by Alfred Man on 7/4/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "OPSLocalProjectSiteInAttachmentInfoDetail.h"
#import "ProjectViewAppDelegate.h"
#import "OPSStudio.h"
#import "AttachmentInfoDetail.h"
@implementation OPSLocalProjectSiteInAttachmentInfoDetail
@synthesize attachmentInfoDetailToClose=_attachmentInfoDetailToClose;


uint const OPSLocalProjectSiteInAttachmentInfoDetail_CONFIRM_UPLOADFINISH=2;
uint const OPSLocalProjectSiteInAttachmentInfoDetail_CONFIRM_UPLOADNOINTERNET=101;


-(id) initWithAttaachmentInfoDetailToClose:(AttachmentInfoDetail*)attachmentInfoDetailToClose{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    self=[super initWithLiveOPSProjectSite:[appDele activeProjectSite]];
    if (self){
        self.attachmentInfoDetailToClose=attachmentInfoDetailToClose;
    
            
//            
//        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
//        NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];
//        
//        if (![[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {
//            // database doesn't exist in your library path... copy it from the bundle
//            NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"EmptySQLTable" ofType:@"sqlite"];
//            NSError *error = nil;
//            
//            if (![[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:targetPath error:&error]) {
//                NSLog(@"Error: %@", error);
//            }
//        }
//        self.dbPath=targetPath;        

        
        
    }
    return self;    
}






 
 
 
 -(void) uploadProjectSite:(NSArray*) paramArray{
//     OPSProjectSite* selectedProjectSite=self;//[paramArray objectAtIndex:0];
//     NSString* dbPath=self.dbPath;//[paramArray objectAtIndex:1];
     //    ViewPlanSiteAndBldgInfoVC* viewPlanSiteAndBldgInfoVC=nil;
     
//     if ([paramArray count]>2){
//     //        if ([[paramArray objectAtIndex:2] isKindOfClass:[ViewPlanSiteAndBldgInfoVC class]]){
//     //            viewPlanSiteAndBldgInfoVC=[paramArray objectAtIndex:2];        
//     //        }else{    
//         self.attachmentInfoDetailToClose=[paramArray objectAtIndex:2];
//     //        }
//     }else{
//         self.attachmentInfoDetailToClose=nil;
//     }
     [super uploadProjectSite:paramArray];
 }
 


/*

-(void) closeAttachmentInfoDetailAndRemoveEmptySQLTable{
    
    if (self.attachmentInfoDetailToClose){
        [self.attachmentInfoDetailToClose trashAttachmentEntry];
                        
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
        NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {                
            NSFileManager *fileManager = [NSFileManager defaultManager];
            [fileManager removeItemAtPath:targetPath error:NULL];  
        }                        
        
    }    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{    
//    
//    [super connectionDidFinishLoading:connection afterUploadSuccessInvoker:self afterUploadSuccessCompleteAction:@selector(closeAttachmentInfoDetailAndRemoveEmptySQLTable) actionParamArray:nil];
//        
    
    [super connectionDidFinishLoading:connection afterUploadSuccessInvoker:self afterUploadSuccessCompleteAction:nil actionParamArray:nil];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [super connection:connection didFailWithError:error];
    
    
    if (self.attachmentInfoDetailToClose){
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        
        
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
        NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];        
        [fileManager removeItemAtPath:targetPath error:NULL];  
        
        
        
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle: @"No Internet For Upload, Data Saved locally instead."
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"OK"
                              otherButtonTitles: nil];        
        alert.tag = OPSLocalProjectSiteInAttachmentInfoDetail_CONFIRM_UPLOADNOINTERNET; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];
    }

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {   
        case OPSLocalProjectSiteInAttachmentInfoDetail_CONFIRM_UPLOADNOINTERNET:
        {
            [self.attachmentInfoDetailToClose actionOK:nil];                   
        }
            break;   
        case OPSLocalProjectSiteInAttachmentInfoDetail_CONFIRM_UPLOADFINISH:
        {            
            [self closeAttachmentInfoDetailAndRemoveEmptySQLTable];
        }
            break;
        
        default:
            //            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");            
            break;
    }	
}

 #pragma mark NSURLConnection methods
 
 - (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
 {
 [connectionData setLength:0];
 
 
 
 }
 
 - (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
 {
 // Append the new data to receivedData.
 // receivedData is an instance variable declared elsewhere.            
 
 [connectionData appendData:data];
 }
 
 - (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
 {
 // release the connection, and the data object
 // receivedData is declared as a method instance elsewhere
 
 //    [connection release];
 [theConnection release];theConnection=nil;
 [connectionData release];connectionData=nil;
 
 
 
 ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
 [appDele removeLoadingViewOverlay];    
 
 
 
 
 if (self.attachmentInfoDetailToClose){
 NSFileManager *fileManager = [NSFileManager defaultManager];
 
 
 
 NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
 NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];        
 [fileManager removeItemAtPath:targetPath error:NULL];  
 
 
 
 UIAlertView *alert = [[UIAlertView alloc] 
 initWithTitle: @"No Internet For Upload, Data Saved locally instead."
 message: @""
 delegate: self
 cancelButtonTitle: @"OK"
 otherButtonTitles: nil];
 
 alert.tag = APPDELEGATE_CONFIRM_UPLOADNOINTERNET; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
 [alert show];
 [alert release];
 
 }else{
 // inform the user
 UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"didFailWithError"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
 [didFailWithErrorMessage show];
 [didFailWithErrorMessage release];
 
 //inform the user
 NSLog(@"Connection failed! Error - %@ %@",
 [error localizedDescription],
 [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
 
 }
 }
 
 
 - (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
 switch (alertView.tag) {   
 case APPDELEGATE_CONFIRM_UPLOADNOINTERNET:
 {
 [self.attachmentInfoDetailToClose actionOK:nil];
 }
 break;            
 default:
 //            NSLog(@"WebAppListVC.alertView: clickedButton at index. Unknown alert type");            
 break;
 }	
 }
 
 - (void)connectionDidFinishLoading:(NSURLConnection *)connection
 {    
 
 NSString* dataStr=[[NSString alloc] initWithData:connectionData encoding:NSASCIIStringEncoding] ;
 //    NSString* dataStr=[[NSString alloc] initWithData:connectionData encoding:NSUTF8StringEncoding] ;
 NSLog(@"RESPONSEDATA: %@",dataStr);
 
 [self alertUploadResult:nil title:dataStr];
 if ([dataStr isEqualToString:@"Upload Success"]){
 
 sqlite3_stmt *updateAttachCommentStmt; 
 const char* updateAttachCommentSQL=[[NSString stringWithFormat:@"update AttachComment set sent=1"] UTF8String];   
 
 sqlite3_stmt *updateAttachFileStmt; 
 const char* updateAttachFileSQL=[[NSString stringWithFormat:@"update AttachFile set sent=1"] UTF8String];    
 
 //        update AttachFile set sent=1
 sqlite3 *database = nil;    
 ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
 OPSProjectSite* selectedProjectSite=appDele.activeProjectSite;
 if (sqlite3_open ([[selectedProjectSite dbPath] UTF8String], &database) == SQLITE_OK) {      
 if(sqlite3_prepare_v2(database, updateAttachCommentSQL, -1, &updateAttachCommentStmt, NULL) == SQLITE_OK) {
 while( sqlite3_step(updateAttachCommentStmt) == SQLITE_ROW) {                     
 }
 }                    
 if(sqlite3_prepare_v2(database, updateAttachFileSQL, -1, &updateAttachFileStmt, NULL) == SQLITE_OK) {
 while( sqlite3_step(updateAttachFileStmt) == SQLITE_ROW) {                     
 }
 }                   
 sqlite3_close(database);
 }else{
 sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
 }  
 
 if (self.attachmentInfoDetailToClose){
 [self.attachmentInfoDetailToClose trashAttachmentEntry];
 
 
 
 NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
 NSString *targetPath = [libraryPath stringByAppendingPathComponent:@"EmptySQLTable.sqlite"];
 
 if ([[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {                
 NSFileManager *fileManager = [NSFileManager defaultManager];
 [fileManager removeItemAtPath:targetPath error:NULL];  
 }
 
 
 
 
 }else{
 [self trashAllAttachmentEntryFromSelectedProjectSite:[self activeProjectSite]];
 }
 
 }
 [dataStr release];
 
 
 
 [theConnection release],theConnection=nil;
 [connectionData release],connectionData=nil;
 ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
 [appDele removeLoadingViewOverlay];
 
 if (self.attachmentInfoDetailToClose){                
 [self.attachmentInfoDetailToClose.navigationController popViewControllerAnimated:YES];
 [self.attachmentInfoDetailToClose executeDeleteImgViewFromStorageAndDB];        
 }
 
 }
 
 
 

*/

@end

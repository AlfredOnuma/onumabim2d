//
//  AttachmentInfoTable.h
//  ProjectView
//
//  Created by Alfred Man on 2/23/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AttachmentInfoTableDelegate 
@end
@class ViewModelToolbar;
@class OPSLocalProjectSiteUploadAllFromSiteView;
@interface AttachmentInfoTable : UITableViewController{
    id <AttachmentInfoTableDelegate> delegate;
    NSString* folderPath;    
    NSMutableArray *tableData;
//    NSMutableArray *attachmentInfoArray;
    UITableView *theTableView;
    ViewModelToolbar* _viewModelToolbar;
    OPSLocalProjectSiteUploadAllFromSiteView* _projectSiteBeingUpload;
}



@property (nonatomic, retain) OPSLocalProjectSiteUploadAllFromSiteView* projectSiteBeingUpload;
@property (nonatomic, assign) ViewModelToolbar* viewModelToolbar;
@property (nonatomic, assign) id <AttachmentInfoTableDelegate> delegate;
@property (nonatomic, retain) NSString* folderPath;

@property (nonatomic, retain) NSMutableArray *tableData;    
//@property (nonatomic, retain) NSMutableArray* attachmentInfoArray;
@property (nonatomic, retain)  UITableView *theTableView;

- (void) addAttachmentEntry:(id)sender;

-(id) initWithViewModelToolbar:(ViewModelToolbar*)viewModelToolbar;

-(void) refreshTable;
@end

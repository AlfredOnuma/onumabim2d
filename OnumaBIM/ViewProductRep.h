//
//  ViewProductRep.h
//  OPSMobile
//
//  Created by Alfred Man on 9/8/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//


#import <UIKit/UIKit.h>

//#include "OPSMobileViewController.h"

//#include "DisplayModelUI.h"
#include "OPSModel.h"
#include "OPSProduct.h"
//#include "ViewProductLabel.h"

@protocol ViewProductRepDelegate
//- (OPSModel*) getModel;
@end
@class ViewProductLabel;
@class DisplayModelUI;
@class ViewProductSprite;
@interface ViewProductRep : UIView {    
    id <ViewProductRepDelegate> delegate; 
    OPSProduct* product;    
    bool selected;
//    OPSProduct* geoProduct;
//    OPSProduct* refProduct;    
//    CGMutablePathRef path;

    
    NSMutableArray* _aSprite;
    
    ViewProductLabel* viewProductLabel;
//    UIImageView* imageView;
    float outsetBuffer;
    CGAffineTransform frameTransform;
//    CGPoint testPt;
//    NSMutableArray* deleteMePath;
//    NSString* displayID;
    
}


//@property (nonatomic, retain) NSMutableArray* deleteMePath;

//@property (nonatomic, retain) NSString* displayID;


//@property (nonatomic, retain) NSMutableArray* viewProductLabelStrArray;
@property (nonatomic, assign) float outsetBuffer;
@property (nonatomic, retain) NSMutableArray* aSprite;
//@property (nonatomic, retain) UIImageView* imageView;
@property (nonatomic, assign) bool selected;
@property (assign) id <ViewProductRepDelegate> delegate;
@property (nonatomic, assign) OPSProduct* product;
//@property (nonatomic, assign) OPSProduct* geoProduct;
//@property (nonatomic, assign) OPSProduct* refProduct;
//@property (nonatomic, assign) CGMutablePathRef path;
@property (nonatomic, assign) ViewProductLabel* viewProductLabel;
@property (nonatomic, assign) CGAffineTransform frameTransform;
//@property (nonatomic, assign) CGPoint testPt;

- (BOOL)containsPoint:(CGPoint)point ;
- (void) displayProductInfo: (OPSProduct*) product;
//- (void) displayProductLabel;
//- (void) clearProductLabel;
//- (void)initExtrudedRepBase:(CGRect)frame geoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*)newRefProduct  ;

//- (void)initBitmapRepBase:(CGRect)frame geoProduct:(OPSProduct*) newGeoProduct refProduct:(OPSProduct*)newRefProduct  ;
- (id)initWithFrame:(CGRect)frame frameTransform:(CGAffineTransform)_frameTransform product:(OPSProduct*) _product displayModelUI:(DisplayModelUI*) displayModelUI;
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
//
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

NSInteger compareSpriteType(id param1, id param2, void *context);
-(void) sortSpriteOrder;

-(void) addSprite:(ViewProductSprite*) sprite;
@end

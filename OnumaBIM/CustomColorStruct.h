//
//  CustomColorStruct.h
//  ProjectView
//
//  Created by Alfred Man on 4/21/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "sqlite3.h"
@class CustomColor;
@interface CustomColorStruct : NSObject{
    uint _ID;
    uint _siteID;
    NSString* _type;
    uint _customSettingNum;
    NSString* _customSettingName;
    NSMutableArray* _aCustomColor;
    /*
    NSString* _name1;
    NSString* _color1;
    NSString* _name2;
    NSString* _color2;
    NSString* _name3;
    NSString* _color3;
    NSString* _name4;
    NSString* _color4;
    NSString* _name5;
    NSString* _color5;
    NSString* _name6;
    NSString* _color6;
    NSString* _name7;
    NSString* _color7;
    NSString* _name8;
    NSString* _color8;
    NSString* _name9;
    NSString* _color9;
    NSString* _name10;
    NSString* _color10;
    NSString* _name11;
    NSString* _color11;
    NSString* _name12;
    NSString* _color12;
    NSString* _name13;
    NSString* _color13;
    NSString* _name14;
    NSString* _color14;
    NSString* _name15;
    NSString* _color15;
    NSString* _name16;
    NSString* _color16;
    NSString* _name17;
    NSString* _color17;
    NSString* _name18;
    NSString* _color18;
    NSString* _name19;
    NSString* _color19;
    NSString* _name20;
    NSString* _color20;
    NSString* _name21;
    NSString* _color21;
    NSString* _name22;
    NSString* _color22;
    NSString* _name23;
    NSString* _color23;
    NSString* _name24;
    NSString* _color24;
    NSString* _name25;
    NSString* _color25;
    NSString* _name26;
    NSString* _color26;
    NSString* _name27;
    NSString* _color27;
    NSString* _name28;
    NSString* _color28;
    NSString* _name29;
    NSString* _color29;
    NSString* _name30;
    NSString* _color30;
    */
}
@property (nonatomic, assign) uint ID;
@property (nonatomic, assign) uint siteID;
@property (nonatomic, retain) NSString* type;
@property (nonatomic, assign) uint customSettingNum;

@property (nonatomic, retain) NSString* customSettingName;
@property (nonatomic, retain) NSMutableArray* aCustomColor;
/*
@property (nonatomic, retain) NSString* name1;
@property (nonatomic, retain) NSString* color1;
@property (nonatomic, retain) NSString* name2;
@property (nonatomic, retain) NSString* color2;
@property (nonatomic, retain) NSString* name3;
@property (nonatomic, retain) NSString* color3;
@property (nonatomic, retain) NSString* name4;
@property (nonatomic, retain) NSString* color4;
@property (nonatomic, retain) NSString* name5;
@property (nonatomic, retain) NSString* color5;
@property (nonatomic, retain) NSString* name6;
@property (nonatomic, retain) NSString* color6;
@property (nonatomic, retain) NSString* name7;
@property (nonatomic, retain) NSString* color7;
@property (nonatomic, retain) NSString* name8;
@property (nonatomic, retain) NSString* color8;
@property (nonatomic, retain) NSString* name9;
@property (nonatomic, retain) NSString* color9;
@property (nonatomic, retain) NSString* name10;
@property (nonatomic, retain) NSString* color10;
@property (nonatomic, retain) NSString* name11;
@property (nonatomic, retain) NSString* color11;
@property (nonatomic, retain) NSString* name12;
@property (nonatomic, retain) NSString* color12;
@property (nonatomic, retain) NSString* name13;
@property (nonatomic, retain) NSString* color13;
@property (nonatomic, retain) NSString* name14;
@property (nonatomic, retain) NSString* color14;
@property (nonatomic, retain) NSString* name15;
@property (nonatomic, retain) NSString* color15;
@property (nonatomic, retain) NSString* name16;
@property (nonatomic, retain) NSString* color16;
@property (nonatomic, retain) NSString* name17;
@property (nonatomic, retain) NSString* color17;
@property (nonatomic, retain) NSString* name18;
@property (nonatomic, retain) NSString* color18;
@property (nonatomic, retain) NSString* name19;
@property (nonatomic, retain) NSString* color19;
@property (nonatomic, retain) NSString* name20;
@property (nonatomic, retain) NSString* color20;
@property (nonatomic, retain) NSString* name21;
@property (nonatomic, retain) NSString* color21;
@property (nonatomic, retain) NSString* name22;
@property (nonatomic, retain) NSString* color22;
@property (nonatomic, retain) NSString* name23;
@property (nonatomic, retain) NSString* color23;
@property (nonatomic, retain) NSString* name24;
@property (nonatomic, retain) NSString* color24;
@property (nonatomic, retain) NSString* name25;
@property (nonatomic, retain) NSString* color25;
@property (nonatomic, retain) NSString* name26;
@property (nonatomic, retain) NSString* color26;
@property (nonatomic, retain) NSString* name27;
@property (nonatomic, retain) NSString* color27;
@property (nonatomic, retain) NSString* name28;
@property (nonatomic, retain) NSString* color28;
@property (nonatomic, retain) NSString* name29;
@property (nonatomic, retain) NSString* color29;
@property (nonatomic, retain) NSString* name30;
@property (nonatomic, retain) NSString* color30;

*/
-(CustomColor*) getCustomColorWithCustomInt:(uint)customInt;
-(id) initWithDatabase:(sqlite3*) database sqlStmt: (sqlite3_stmt*) sqlStmt;
@end

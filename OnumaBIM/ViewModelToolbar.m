//
//  ViewModelToolbar.m
//  ProjectView
//
//  Created by Alfred Man on 11/24/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//
#import "ViewModelVC.h"
#import "ViewProductLabel.h"
#import "ViewModelNavCntr.h"
#import "ViewModelToolbar.h"
//#import "AttachmentInfo.h"
#import "AttachmentInfoDetail.h"
#import "AttachmentInfoNVC.h"
#import "AttachmentInfoTable.h"
#import "ProjectViewAppDelegate.h"
#import "OPSProjectSite.h"
#import "Site.h"
#import "Bldg.h"
#import "Space.h"
#import "Floor.h"
#import "Furn.h"
#import "SitePolygon.h"
#import "OPSProjectSite.h"
#import "BIMPlanColorCatNVC.h"
#import "BIMPlanColorCatTVC.h"
#import "ViewModelToolBarTitleScroll.h"
#import "ViewPlanSiteInfoVC.h"
#import "ViewPlanSiteAndBldgInfoVC.h"
#import "ViewPlanSiteInfoNVC.h"
#import "SitePolygon.h"
#import "FontSizeTableViewController.h"
#import "attachmentinfodata.h"

#import "AROverlayViewController.h"
#import <ImageIO/ImageIO.h>

#import "EvernoteSession.h"
#import "EvernoteUserStore.h"
#import "EvernoteNoteStore+Extras.h"
#import "ENMLUtility.h"
#import "EverNoteNVC.h"
#import "EverNoteVC.h"
#import "EverNoteTVC.h"

//#import "ViewModelToolbarCameraPopoverController.h"
//#import "AttachmentPopOverController.h"


//@interface UIImagePickerController(Rotating)
//- (BOOL)shouldAutorotate;
//@end
//
//@implementation UIImagePickerController(Rotating)
//
//- (BOOL)shouldAutorotate {
//    return YES;
//}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationLandscapeRight;
//}
//@end

@interface UIImagePickerController(Rotating)
- (BOOL)shouldAutorotate;
@end

@implementation UIImagePickerController(Rotating)
- (BOOL)shouldAutorotate {
    return YES;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}
@end



//@interface UIImagePickerController(Nonrotating)
//- (BOOL)shouldAutorotate;
//@end
//
//@implementation UIImagePickerController(Nonrotating)
//
//- (BOOL)shouldAutorotate {
//    return NO;
//}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationLandscapeRight;
//}
//@end

@implementation ViewModelToolbar
@synthesize photoCapturedPath;
@synthesize imgLibraryPopOverController=_imgLibraryPopOverController;
@synthesize viewMode;
@synthesize delegate;
@synthesize popoverController=_popoverController;
@synthesize titleScroll;
@synthesize userSelectedColorTableIndex=_userSelectedColorTableIndex;
@synthesize arOverlayViewController=_arOverlayViewController;


-(void) fileMakerSearch:(id) sender{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    OPSProduct* selectedProduct=[[[self parentViewModelVC] lastSelectedProductRep] product];
    int id=0;
    int level=0;

    
    if ([[self parentViewModelVC] aSelectedProductRep]==nil || [[[self parentViewModelVC] aSelectedProductRep] count]<=0){
        switch ([appDele modelDisplayLevel]) {
            case 0:
            {
                level=1;
                Site* site=(Site*)[[[self parentViewModelVC] model] root];
                id=site.ID;
            }
                break;
            case 1:
            {
                level=3;
                Bldg* bldg=(Bldg*)[[[self parentViewModelVC] model] root];
                Floor* floor=(Floor*) [bldg selectedFloor];
                id=floor.ID;
                
            }
                
                break;
            case 2:
            {
                level=4;
                Space* space=(Space*)[[[self parentViewModelVC] model] root];
                id=space.ID;
            }

                break;
                
            default:
                break;
        }
    }else{

        if ([selectedProduct isKindOfClass:[Site class]]){
            level=1;
            id=(int) selectedProduct.ID;
        }else if ([selectedProduct isKindOfClass:[Floor class]] && [appDele modelDisplayLevel]==0 ){
            level=2;
            Bldg* bldg=(Bldg*)[selectedProduct findParentSpatialStruct];
            NSLog (@"BldgName:%@",bldg.name);
            id=(int) bldg.ID;
        }else if ([selectedProduct isKindOfClass:[Floor class]] && [appDele modelDisplayLevel]==1 ){
            level=3;
    //        Floor* bldg=(Bldg*)[selectedProduct findParentSpatialStruct];
            id=(int) selectedProduct.ID;
        }else if ([selectedProduct isKindOfClass:[Space class]]){
            level=4;
            id=(int) selectedProduct.ID;
        }else if ([selectedProduct isKindOfClass:[Furn class]]){
            level=5;
            id=(int) selectedProduct.ID;
        }else if ([selectedProduct isKindOfClass:[SitePolygon class]]){
            level=6;
            id=(int) selectedProduct.ID;
        }
    }
    
    NSLog (@"Level: %d id:%d",level,id);
    if (level==0){
        return;
    }


    int studioID=[appDele activeStudioID];
    int siteID=[appDele activeProjectSite].ID;
    
    
//    NSString* urlStrl=[NSString stringWithFormat:@"fmp://~/S%d_%d?script=spacesearch&param=%d",studioID,siteID,spaceID];
//    NSString* urlStr=@"fmp://%7E/Assess?script=search&param=";
//    urlStr=[urlStr stringByAppendingFormat:@"S%d_%d&level=%d&ID=%d",studioID,siteID,level,id];
//    ONUMA-Link.fmp12
    
    NSString* urlStrl=[NSString stringWithFormat:@"fmp://%%7E/ONUMA-Link?script=search&param=S%d_%d&$level=%d&$ID=%d",studioID,siteID,level,id];
    
//    NSString* urlStrl=[NSString stringWithFormat:@"fmp://%%7E/Assess?script=search&param=S%d_%d&$level=%d&$ID=%d",studioID,siteID,level,id];
    NSLog(@"%@",urlStrl);
    NSURL *appStoreUrl = [NSURL URLWithString:urlStrl] ;//]@"fmp://%7e/Assessment?script=spacesearch&param=2336"];
    [[UIApplication sharedApplication] openURL:appStoreUrl];
    return;
}

- (IBAction) takePhoto:(id)sender{

    
    
//    [self launchEverNoteNVC:sender];
//    return;
    AROverlayViewController* arOverlayViewController=[[AROverlayViewController alloc] init ];//]initwithToolBar:self prevVC:self.parentViewModelVC];
    arOverlayViewController.toolbar=self;
   arOverlayViewController.prevVC=self.parentViewModelVC;

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];

    [[appDele viewModelNavCntr] pushViewController:arOverlayViewController animated:NO];
    [appDele viewModelNavCntr].navigationBar.translucent=YES;

    [appDele viewModelNavCntr].navigationBar.alpha=0.7;
    [arOverlayViewController release];    

    return;
    
        
    
}


//
//- (IBAction) takePhoto1:(id)sender{
//    
//    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init] ;
//    imagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
//    
//    //        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    imagePicker.delegate = self;
//    
//    
//    
//    
//    CGAffineTransform cameraTransform=CGAffineTransformIdentity;
//    UIInterfaceOrientation orient=[UIApplication sharedApplication].statusBarOrientation;
//    
//    UIDeviceOrientation deviceOrient = [[UIDevice currentDevice] orientation];
//    
//    //        UIImageOrientation orient = image.imageOrientation;
//    switch(orient) {
//   
//        case UIInterfaceOrientationLandscapeLeft: //EXIF = 3
//        {
//            NSLog(@"UIInterfaceOrientationLandscapeLeft");
//            
//            
//            switch (deviceOrient){
//                case UIDeviceOrientationUnknown:
//                {
//                    NSLog(@"UIDeviceOrientationUnknown");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationPortrait:
//                {
//                    NSLog(@"UIDeviceOrientationPortrait");
//                    //                    cameraTransform = CGAffineTransformMakeRotation(-90*M_PI/180);;
//                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
//                }
//                    break;
//                case UIDeviceOrientationPortraitUpsideDown:
//                {
//                    NSLog(@"UIDeviceOrientationPortraitUpsideDown");
//                    cameraTransform = CGAffineTransformMakeRotation(-90*M_PI/180);
//                    //                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
//                }
//                    break;
//                case UIDeviceOrientationLandscapeLeft:
//                {
//                    NSLog(@"UIDeviceOrientationLandscapeLeft");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationLandscapeRight:
//                {
//                    NSLog(@"UIDeviceOrientationLandscapeRight");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationFaceUp:
//                {
//                    NSLog(@"UIDeviceOrientationFaceUp");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationFaceDown:
//                {
//                    NSLog(@"UIDeviceOrientationFaceDown");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                default:
//                    cameraTransform = CGAffineTransformIdentity;
//                    break;
//            }
//            
//            
//        }
//            break;
//            
//        case UIInterfaceOrientationLandscapeRight: //EXIF = 4
//        {
//            NSLog(@"UIInterfaceOrientationLandscapeRight");            
//            switch (deviceOrient) {
//                case UIDeviceOrientationUnknown:
//                {
//                    NSLog(@"UIDeviceOrientationUnknown");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationPortrait:
//                {
//                    NSLog(@"UIDeviceOrientationPortrait");
//                    cameraTransform = CGAffineTransformMakeRotation(-90*M_PI/180);;
//                    //                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
//                }
//                    break;
//                case UIDeviceOrientationPortraitUpsideDown:
//                {
//                    NSLog(@"UIDeviceOrientationPortraitUpsideDown");
//                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
//                }
//                    break;
//                case UIDeviceOrientationLandscapeLeft:
//                {
//                    NSLog(@"UIDeviceOrientationLandscapeLeft");
//                    //                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationLandscapeRight:
//                {
//                    NSLog(@"UIDeviceOrientationLandscapeRight");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationFaceUp:
//                {
//                    NSLog(@"UIDeviceOrientationFaceUp");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationFaceDown:
//                {
//                    NSLog(@"UIDeviceOrientationFaceDown");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                default:
//                    cameraTransform = CGAffineTransformIdentity;
//                    break;
//            }
//            
//        
//            
//            //            cameraTransform = CGAffineTransformIdentity;
//        }
//            break;
//            
//        default:
//            cameraTransform = CGAffineTransformIdentity;
//            break;
//    }
//     
//    imagePicker.view.transform=[self parentViewModelVC].view.transform;
//    [[self parentViewModelVC] presentViewController:imagePicker animated:YES completion:nil];
//    [imagePicker release];
//    return;
//    
//    UIPopoverController* aPopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
//    [imagePicker release];
//    
//    //    [[[UIApplication sharedApplication]keyWindow]setRootViewController:imagePicker];
//    //    [[self parentViewModelVC] presentViewController:imagePicker animated:NO completion:nil];
//    //			
//    //    return;
//    
//    aPopover.delegate=self;
//    self.imgLibraryPopOverController=aPopover;
//    [aPopover release];
//    
//    [self.imgLibraryPopOverController presentPopoverFromRect:[sender bounds]
//                                                      inView:sender
//                                    permittedArrowDirections:UIPopoverArrowDirectionAny
//                                                    animated:YES];
//
//    
//}
//
//
//
//
//- (IBAction) takePhoto0:(id)sender{
//
//    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init] ;
//    imagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
//    
//    //        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    imagePicker.delegate = self;
//    
//    
//    
//    
//    CGAffineTransform cameraTransform=CGAffineTransformIdentity;
//    UIInterfaceOrientation orient=[UIApplication sharedApplication].statusBarOrientation;
//    
//    UIDeviceOrientation deviceOrient = [[UIDevice currentDevice] orientation];
//    
//    //        UIImageOrientation orient = image.imageOrientation;
//    switch(orient) {
//            
////        case UIInterfaceOrientationPortrait: //EXIF = 1
////        {
////            NSLog(@"UIInterfaceOrientationPortrait");
//////            cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);
////            cameraTransform = CGAffineTransformIdentity;
////        }
////            break;
////            
////        case UIInterfaceOrientationPortraitUpsideDown: //EXIF = 2
////        {
////            NSLog(@"UIInterfaceOrientationPortraitUpsideDown");
//////            cameraTransform = CGAffineTransformMakeRotation(270*M_PI/180);
////            //transform = CGAffineTransformMakeRotation(180*M_PI/180);
////            cameraTransform = CGAffineTransformIdentity;
////        }
////            break;
//        case UIInterfaceOrientationLandscapeLeft: //EXIF = 3
//        {
//            NSLog(@"UIInterfaceOrientationLandscapeLeft");
//
//            
//            switch (deviceOrient){
//                case UIDeviceOrientationUnknown:
//                {
//                    NSLog(@"UIDeviceOrientationUnknown");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationPortrait:
//                {
//                    NSLog(@"UIDeviceOrientationPortrait");
////                    cameraTransform = CGAffineTransformMakeRotation(-90*M_PI/180);;
//                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
//                }
//                    break;
//                case UIDeviceOrientationPortraitUpsideDown:
//                {
//                    NSLog(@"UIDeviceOrientationPortraitUpsideDown");
//                    cameraTransform = CGAffineTransformMakeRotation(-90*M_PI/180);
////                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
//                }
//                    break;
//                case UIDeviceOrientationLandscapeLeft:
//                {
//                    NSLog(@"UIDeviceOrientationLandscapeLeft");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationLandscapeRight:
//                {
//                    NSLog(@"UIDeviceOrientationLandscapeRight");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationFaceUp:
//                {
//                    NSLog(@"UIDeviceOrientationFaceUp");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationFaceDown:
//                {
//                    NSLog(@"UIDeviceOrientationFaceDown");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                default:
//                    cameraTransform = CGAffineTransformIdentity;
//                    break;
//            }
//            
//            
//        }
//            break;
//            
//        case UIInterfaceOrientationLandscapeRight: //EXIF = 4
//        {
//            NSLog(@"UIInterfaceOrientationLandscapeRight");
//            
//            
//            
//            switch (deviceOrient) {
//                case UIDeviceOrientationUnknown:
//                {
//                    NSLog(@"UIDeviceOrientationUnknown");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationPortrait:
//                {
//                    NSLog(@"UIDeviceOrientationPortrait");
//                    cameraTransform = CGAffineTransformMakeRotation(-90*M_PI/180);;
////                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
//                }
//                    break;
//                case UIDeviceOrientationPortraitUpsideDown:
//                {
//                    NSLog(@"UIDeviceOrientationPortraitUpsideDown");
//                    cameraTransform = CGAffineTransformMakeRotation(90*M_PI/180);;
//                }
//                    break;
//                case UIDeviceOrientationLandscapeLeft:
//                {
//                    NSLog(@"UIDeviceOrientationLandscapeLeft");
////                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationLandscapeRight:
//                {
//                    NSLog(@"UIDeviceOrientationLandscapeRight");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationFaceUp:
//                {
//                    NSLog(@"UIDeviceOrientationFaceUp");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                case UIDeviceOrientationFaceDown:
//                {
//                    NSLog(@"UIDeviceOrientationFaceDown");
//                    cameraTransform = CGAffineTransformIdentity;
//                }
//                    break;
//                default:
//                    cameraTransform = CGAffineTransformIdentity;
//                    break;
//            }
//            
//            
////            cameraTransform = CGAffineTransformIdentity;
//        }
//            break;
//            
//        default:
//            cameraTransform = CGAffineTransformIdentity;
//            break;
//        }
//    imagePicker.view.transform=cameraTransform;
////    OverlayViewConroller* overlay=[[OverlayViewController alloc] init];
////    
////    
////    self.overlay = [[OverlayViewController alloc] initWithNibName:@"Overlay" bundle:nil];
////    self.overlay.pickerReference = self.picker;
//    
//    
////    imagePicker.showsCameraControls=YES;
////    imagePicker.delegate=self;
////    imagePicker.showsCameraControls=YES;
//
//    
//    
////    imagePicker.view.transform=CGAffineTransformMakeRotation(M_PI*90/180);
//    
//    
////    imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
//    /*
//    
//    if ([info objectForKey:@"UIImagePickerControllerMediaMetadata"]) {
//        //Rotate based on orientation
//        switch ([[[info objectForKey:@"UIImagePickerControllerMediaMetadata"] objectForKey:@"Orientation"] intValue]) {
//            case 3:
//                //Rotate image to the left twice.
//                img = [UIImage imageWithCGImage:[img CGImage]];  //Strip off that pesky meta data!
//                img = [rotateImage rotateImage:[rotateImage rotateImage:img withRotationType:rotateLeft] withRotationType:rotateLeft];
//                break;
//                
//            case 6:
//                img = [UIImage imageWithCGImage:[img CGImage]];
//                img = [rotateImage rotateImage:img withRotationType:rotateRight];
//                break;
//                
//            case 8:
//                img = [UIImage imageWithCGImage:[img CGImage]];
//                img = [rotateImage rotateImage:img withRotationType:rotateLeft];
//                break;
//                
//            default:
//                break;
//        }
//    }
// */
//    
//    /*
////    - (UIImage *)scaleAndRotateImage:(UIImage *)image {
////        int kMaxResolution = 640; // Or whatever
////        
////        CGImageRef imgRef = image.CGImage;
////        
////        CGFloat width = CGImageGetWidth(imgRef);
////        CGFloat height = CGImageGetHeight(imgRef);
////        
////        
////        CGAffineTransform transform = CGAffineTransformIdentity;
////        CGRect bounds = CGRectMake(0, 0, width, height);
////        if (width > kMaxResolution || height > kMaxResolution) {
////            CGFloat ratio = width/height;
////            if (ratio > 1) {
////                bounds.size.width = kMaxResolution;
////                bounds.size.height = roundf(bounds.size.width / ratio);
////            }
////            else {
////                bounds.size.height = kMaxResolution;
////                bounds.size.width = roundf(bounds.size.height * ratio);
////            }
////        }
////        
////        CGFloat scaleRatio = bounds.size.width / width;
////        CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
////        CGFloat boundHeight;
//        UIInterfaceOrientation orient=[UIApplication sharedApplication].statusBarOrientation;
//        
//        
////        UIImageOrientation orient = image.imageOrientation;
//        switch(orient) {
//                
//            case UIImageOrientationUp: //EXIF = 1
//                transform = CGAffineTransformIdentity;
//                break;
//                
//            case UIImageOrientationUpMirrored: //EXIF = 2
//                transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
//                transform = CGAffineTransformScale(transform, -1.0, 1.0);
//                break;
//                
//            case UIImageOrientationDown: //EXIF = 3
//                transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
//                transform = CGAffineTransformRotate(transform, M_PI);
//                break;
//                
//            case UIImageOrientationDownMirrored: //EXIF = 4
//                transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
//                transform = CGAffineTransformScale(transform, 1.0, -1.0);
//                break;
//                
//            case UIImageOrientationLeftMirrored: //EXIF = 5
//                boundHeight = bounds.size.height;
//                bounds.size.height = bounds.size.width;
//                bounds.size.width = boundHeight;
//                transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
//                transform = CGAffineTransformScale(transform, -1.0, 1.0);
//                transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
//                break;
//                
//            case UIImageOrientationLeft: //EXIF = 6
//                boundHeight = bounds.size.height;
//                bounds.size.height = bounds.size.width;
//                bounds.size.width = boundHeight;
//                transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
//                transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
//                break;
//                
//            case UIImageOrientationRightMirrored: //EXIF = 7
//                boundHeight = bounds.size.height;
//                bounds.size.height = bounds.size.width;
//                bounds.size.width = boundHeight;
//                transform = CGAffineTransformMakeScale(-1.0, 1.0);
//                transform = CGAffineTransformRotate(transform, M_PI / 2.0);
//                break;
//                
//            case UIImageOrientationRight: //EXIF = 8
//                boundHeight = bounds.size.height;
//                bounds.size.height = bounds.size.width;
//                bounds.size.width = boundHeight;
//                transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
//                transform = CGAffineTransformRotate(transform, M_PI / 2.0);
//                break;
//                
//            default:
//                [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
//                
//        }
//        
////        UIGraphicsBeginImageContext(bounds.size);
////        
////        CGContextRef context = UIGraphicsGetCurrentContext();
////        
////        if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
////            CGContextScaleCTM(context, -scaleRatio, scaleRatio);
////            CGContextTranslateCTM(context, -height, 0);
////        }
////        else {
////            CGContextScaleCTM(context, scaleRatio, -scaleRatio);
////            CGContextTranslateCTM(context, 0, -height);
////        }
////        
////        CGContextConcatCTM(context, transform);
////        
////        CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
////        UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
////        UIGraphicsEndImageContext();
////        
////        return imageCopy;
//    	
//    */
//    
//    //    [self.view addSubview:imagePicker.view];
//    
//    
////    [[[self parentViewModelVC] navigationController] pushViewController:imagePicker.view animated:NO];
//    
////    [[self parentViewModelVC] presentViewController:imagePicker animated:NO completion:nil];
//////    [self addSubview:imagePicker.view];
////    return;
////    
//    
//    
//    UIPopoverController* aPopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
//    [imagePicker release];
//    
//    
//    
////    [[[UIApplication sharedApplication]keyWindow]setRootViewController:imagePicker];
////    [[self parentViewModelVC] presentViewController:imagePicker animated:NO completion:nil];
////    
////    return;
//    
//    aPopover.delegate=self;
//    self.imgLibraryPopOverController=aPopover;
//    [aPopover release];
//    //    CGRect imgLibFrame=buttonImgLibrary.frame;
//    //    CGRect pickerFrame = CGRectMake(imgLibFrame.origin.x, imgLibFrame.origin.y, 400, 600);
//    //    [aPopover setPopoverContentSize:pickerFrame.size animated:NO];
//    
//    
//    [self.imgLibraryPopOverController presentPopoverFromRect:[sender bounds]
//     
//     //        [self.imgLibraryPopOverController presentPopoverFromRect:[buttonImgLibrary bounds]//pickerFrame//CGRectMake(0.0, 0.0, 800.0, 800.0)
//                                                      inView:sender
//                                    permittedArrowDirections:UIPopoverArrowDirectionAny
//                                                    animated:YES];
//    
//    //    [self.imgLibraryPopOverController presentPopoverFromBarButtonItem:buttonImgLibrary
//    //                                   permittedArrowDirections:UIPopoverArrowDirectionUp
//    //                                                   animated:YES];
//
//    //    [self presentModalViewController:imagePicker animated:YES];
//
//    
//    
//    /*
//     UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
//     //    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
//     
//     imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//UIImagePickerControllerSourceTypeCamera;
//     imagePicker.delegate = self;
//     //    [[self parentViewModelVC] presentViewController:imagePicker animated:YES completion:nil];
//     [[self parentViewModelVC] presentModalViewController:imagePicker animated:YES];
//     [imagePicker release];
//     */
//    
//}



/*

- (void)scaleAndRotateImage {
    
    CGImageRef imgRef = self.image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    if (width <= kMaxResolution && height <= kMaxResolution && orient == UIImageOrientationUp) {
        return;
    }
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.image = imageCopy;
}

*/

- (void)dealloc
{
    [_arOverlayViewController release];_arOverlayViewController=nil;
    [photoCapturedPath release];photoCapturedPath=nil;
    [_imgLibraryPopOverController release];_imgLibraryPopOverController=nil;
    [_popoverController release], _popoverController=nil;
    [titleScroll release];titleScroll=nil;
    [super dealloc];
}


// return 'best' size to fit given size. does not actually resize view. Default is return existing view size
//-(CGSize) sizeThatFits:(CGSize)size{
//    CGSize result = [super sizeThatFits:size];
//    result.height = 75;
//    return result;    
//}

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
 */






/*
 - (void) move:(id)sender{
 
 }
 
 - (void) rotate:(id)sender{
 
 }
 
 - (void) reshape:(id)sender{
 
 }
 
 - (void) lock:(id)sender{
 
 }
 
 - (void) copy:(id)sender{
 
 }
 
 - (void) remove:(id)sender{
 
 }
 */


- (void) report:(id)sender{
    
}
- (void) info:(id)sender{
    
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];

    [appDele displayViewPlanSiteInfoVC];
    /*
    
    
    if (self.popoverController!=nil){
        [self.popoverController dismissPopoverAnimated:NO];
        self.popoverController=nil;        
    }
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    Site* site=[appDele currentSite];
//    uint numCustColorCat=0;
    
    if ([appDele modelDisplayLevel]==0){
//        
//        NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
//        numCustColorCat=[aCustomColorStruct count];
//        BIMPlanColorCatTVC* colorTVC=[[BIMPlanColorCatTVC alloc] init];//initWithNibName:@"NavModelVC" bundle:Nil];
//        uint numCell=3+numCustColorCat;
//        if (numCell>14) {numCell=14;}
//        colorTVC.contentSizeForViewInPopover=CGSizeMake(360, (numCell*44) );
//        //    bimReportCatTVC.contentSizeForViewInPopover=CGSizeMake(150, 135);    
        
        ViewPlanSiteInfoVC* siteInfoVC=[[ViewPlanSiteInfoVC alloc] initWithNibName:@"ViewPlanSiteInfoVC" bundle:nil];
        
        siteInfoVC.contentSizeForViewInPopover=CGSizeMake(633,383 );
        ViewPlanSiteInfoNVC* siteInfoNVC=[[ViewPlanSiteInfoNVC alloc] initWithRootViewController:siteInfoVC viewModelToolbar:self];

        
//        siteInfoNVC.contentSizeForViewInPopover=CGSizeMake(633,383 );
        
//        BIMPlanColorCatNVC* colorNVC=[[BIMPlanColorCatNVC alloc] initWithRootViewController:colorTVC viewModelToolbar:self];
        //    ToolbarPopoverController* bimReportCatPopoverController=[[ToolbarPopoverController alloc] 
        //                                                       initWithContentViewController:bimReportNVC];
        
        UIPopoverController* siteInfoPopoverController=[[UIPopoverController alloc] 
                                                     initWithContentViewController:siteInfoNVC];
        siteInfoVC.title=@"Site Info";   
        ;
        self.popoverController = siteInfoPopoverController;// autorelease];
        [siteInfoPopoverController release];
        
        [siteInfoVC release];
        [siteInfoNVC release];
        
    }
    
    
    if ([sender isKindOfClass:[UIButton class]]){
        //       sender= [((UIButton*)sender) ] 
        UIButton* button=sender;
        //        sender=[((UIButton*) sender) superview];
        [self.popoverController presentPopoverFromRect:button.bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES ];
    }else{
        [self.popoverController presentPopoverFromBarButtonItem:sender
                                       permittedArrowDirections:UIPopoverArrowDirectionUp
                                                       animated:YES];      
    }
    */
    
}


- (void) color:(id)sender{

    if (self.popoverController!=nil){
        [self.popoverController dismissPopoverAnimated:NO];
        self.popoverController=nil;        
    }
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    Site* site=[appDele currentSite];
    uint numCustColorCat=0;
    
    if ([appDele modelDisplayLevel]!=2){

        NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
        numCustColorCat=[aCustomColorStruct count];
        BIMPlanColorCatTVC* colorTVC=[[BIMPlanColorCatTVC alloc] init];//initWithNibName:@"NavModelVC" bundle:Nil];
        uint numCell=0;
        if ([appDele modelDisplayLevel]==0){
            numCell=2;
        }else if ([appDele modelDisplayLevel]==1){
            numCell=3;
        }
        numCell+=numCustColorCat;
        if (numCell>14) {numCell=14;}
        colorTVC.contentSizeForViewInPopover=CGSizeMake(360, (numCell*44) );
    //    bimReportCatTVC.contentSizeForViewInPopover=CGSizeMake(150, 135);    
        
        BIMPlanColorCatNVC* colorNVC=[[BIMPlanColorCatNVC alloc] initWithRootViewController:colorTVC viewModelToolbar:self];
        //    ToolbarPopoverController* bimReportCatPopoverController=[[ToolbarPopoverController alloc] 
        //                                                       initWithContentViewController:bimReportNVC];

        UIPopoverController* colorPopoverController=[[UIPopoverController alloc] 
                                                            initWithContentViewController:colorNVC];
        if ([appDele modelDisplayLevel]==0){            
            colorTVC.title=@"Color By Bldg Attribute";
        }else if ([appDele modelDisplayLevel]==1){
            colorTVC.title=@"Color By Space Attribute";
        }
        self.popoverController = colorPopoverController;// autorelease];
        [colorPopoverController release];
        
        [colorTVC release];
        [colorNVC release];
        
    }
    
    
    if ([sender isKindOfClass:[UIButton class]]){
        //       sender= [((UIButton*)sender) ] 
        UIButton* button=sender;
        //        sender=[((UIButton*) sender) superview];
        [self.popoverController presentPopoverFromRect:button.bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES ];
    }else{
        [self.popoverController presentPopoverFromBarButtonItem:sender
                                       permittedArrowDirections:UIPopoverArrowDirectionUp
                                                       animated:YES];      
    }
      
    
}
- (void) tools:(id)sender{
    
}
- (void) edit:(id)sender{
    
}
//
//
//- (void) color:(id)sender{
//}

- (void) fontSize:(id) sender{
    
    if (self.popoverController!=nil){
        [self.popoverController dismissPopoverAnimated:NO];
        self.popoverController=nil;
    }
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    FontSizeTableViewController* fontSizeTableVC=[[FontSizeTableViewController alloc] initWithViewModelVC:[self parentViewModelVC] Style:UITableViewStylePlain];//initWithNibName:@"NavModelVC" bundle:Nil];
    uint numCell=2;

    fontSizeTableVC.contentSizeForViewInPopover=CGSizeMake(44, (numCell*44) );
    //    bimReportCatTVC.contentSizeForViewInPopover=CGSizeMake(150, 135);
    
    
    
    UIPopoverController* fontSizePopoverController=[[UIPopoverController alloc]
                                                 initWithContentViewController:fontSizeTableVC];
    
    fontSizeTableVC.title=@"Font Size";
    self.popoverController = fontSizePopoverController;// autorelease];
    [fontSizePopoverController release];
    [fontSizeTableVC release];
    


    
    if ([sender isKindOfClass:[UIButton class]]){
        //       sender= [((UIButton*)sender) ]
        UIButton* button=sender;
        //        sender=[((UIButton*) sender) superview];
        [self.popoverController presentPopoverFromRect:button.bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES ];
    }else{
        [self.popoverController presentPopoverFromBarButtonItem:sender
                                       permittedArrowDirections:UIPopoverArrowDirectionUp
                                                       animated:YES];
    }
//
//    UISlider* slider=[[UISlider alloc]init];
//
//    UIPopoverController* popoverController=[[UIPopoverController alloc]
//                                                 initWithContentViewController:slider];
//    
    /*
    Site* site=[appDele currentSite];
    uint numCustColorCat=0;
    
    if ([appDele modelDisplayLevel]!=2){
        
        NSMutableArray* aCustomColorStruct=([appDele modelDisplayLevel]==0)?site.aBldgCustomColorStruct:site.aSpaceCustomColorStruct;
        numCustColorCat=[aCustomColorStruct count];
        BIMPlanColorCatTVC* colorTVC=[[BIMPlanColorCatTVC alloc] init];//initWithNibName:@"NavModelVC" bundle:Nil];
        uint numCell=0;
        if ([appDele modelDisplayLevel]==0){
            numCell=2;
        }else if ([appDele modelDisplayLevel]==1){
            numCell=3;
        }
        numCell+=numCustColorCat;
        if (numCell>14) {numCell=14;}
        colorTVC.contentSizeForViewInPopover=CGSizeMake(360, (numCell*44) );
        //    bimReportCatTVC.contentSizeForViewInPopover=CGSizeMake(150, 135);
        
        
        BIMPlanColorCatNVC* colorNVC=[[BIMPlanColorCatNVC alloc] initWithRootViewController:colorTVC viewModelToolbar:self];
        //    ToolbarPopoverController* bimReportCatPopoverController=[[ToolbarPopoverController alloc]
        //                                                       initWithContentViewController:bimReportNVC];
        
        UIPopoverController* colorPopoverController=[[UIPopoverController alloc]
                                                     initWithContentViewController:colorNVC];
        if ([appDele modelDisplayLevel]==0){
            colorTVC.title=@"Color By Bldg Attribute";
        }else if ([appDele modelDisplayLevel]==1){
            colorTVC.title=@"Color By Space Attribute";
        }
        self.popoverController = colorPopoverController;// autorelease];
        [colorPopoverController release];
        
        [colorTVC release];
        [colorNVC release];
        
    }
    
    
    if ([sender isKindOfClass:[UIButton class]]){
        //       sender= [((UIButton*)sender) ]
        UIButton* button=sender;
        //        sender=[((UIButton*) sender) superview];
        [self.popoverController presentPopoverFromRect:button.bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES ];
    }else{
        [self.popoverController presentPopoverFromBarButtonItem:sender
                                       permittedArrowDirections:UIPopoverArrowDirectionUp
                                                       animated:YES];
    }
    
    */
}
- (void) bimmail:(id)sender{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if ([appDele currentSite].shareEdit>0 || [appDele currentSite].shareView>0){
        [appDele displayBIMMailVC];
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"This scheme is not shared with anyboty. A scheme needs to be shared with others to be able to send BIM Mail."
                              message: @""
                              delegate: self
                              cancelButtonTitle: @"OK"
                              otherButtonTitles: nil];
        //        alert.tag = ATTACHMENT_MaximumImageReached; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
        [alert show];
        [alert release];
//        [bi setEnabled:false];
    }
    
    
//    [appDele displayBIMMailVC];
}


-(id) attachButton{    
    return [self.items objectAtIndex:buttonIndex_Attach];     
}

-(id) syncButton{
    return [self.items objectAtIndex:buttonIndex_Sync];
}
/*
-(NSString*) findSelectedProductRepDisplayStr{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];    
    ViewModelVC*  viewModelVC=[self parentViewModelVC];
    //        OPSProduct* selectedProduct=[viewModelVC selectedProduct];
//    OPSProduct* selectedProduct=[[viewModelVC selectedProductRep] product];
    OPSProduct* lastSelectedProduct=[[viewModelVC lastSelectedProductRep] product];    
    
    OPSProduct* refProductFromSelectedProduct=nil;
    NSString* displayStr=nil;
    switch ([appDele modelDisplayLevel]){
        case 0:
            
            if ([lastSelectedProduct isKindOfClass:[SitePolygon class]]){
                SitePolygon* sitePoly=(SitePolygon*) [[viewModelVC model] root];
                displayStr=[NSString stringWithFormat:@"%@: %@",[sitePoly type],[sitePoly name]];
            }else if ([lastSelectedProduct isKindOfClass:[Site class]]){
                refProductFromSelectedProduct=(Site*) [[viewModelVC model] root];
                displayStr=[NSString stringWithFormat:@"Site: %@",[refProductFromSelectedProduct name]];            
            }else if ([lastSelectedProduct isKindOfClass:[Floor class]]){
                refProductFromSelectedProduct=(Bldg*) [[lastSelectedProduct linkedRel]relating];
                displayStr=[NSString stringWithFormat:@"Bldg: %@",[refProductFromSelectedProduct name]];
            }else{
                refProductFromSelectedProduct=lastSelectedProduct;                
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];
            }
            break;
        case 1:
            
            if ([lastSelectedProduct isKindOfClass:[Floor class]]){
                Bldg* bldg=(Bldg*) [[viewModelVC model] root];
                Floor* floor=(Floor*) [[[bldg relAggregate] related] objectAtIndex:[bldg selectedFloorIndex]];
                refProductFromSelectedProduct=floor;
                displayStr=[NSString stringWithFormat:@"Floor: %@",[refProductFromSelectedProduct name]];      
                
            }
            else if ([lastSelectedProduct isKindOfClass:[Space class]]){
                refProductFromSelectedProduct=(Space*) lastSelectedProduct;
                
                displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]];                     
                //                    refProductFromSelectedProduct=(Floor*) [[selectedProduct linkedRel]relating];                    
                //                    displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]];                    
            }else{
                refProductFromSelectedProduct=lastSelectedProduct;                    
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];                    
            }
            break;                
        case 2:
            
            if ([lastSelectedProduct isKindOfClass:[Space class]]){            
                refProductFromSelectedProduct=(Space*) [[viewModelVC model] root];
                displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]];
            }else if ([lastSelectedProduct isKindOfClass:[Furn class]]){
                refProductFromSelectedProduct=(Furn*) lastSelectedProduct;
                displayStr=[NSString stringWithFormat:@"Equipment: %@",[refProductFromSelectedProduct name]];
            }else{
                refProductFromSelectedProduct=lastSelectedProduct;
                
                displayStr=[NSString stringWithFormat:@"%@",[refProductFromSelectedProduct name]];
            }
            break;        
        default:
            break;
    }
    return displayStr;
}
*/
- (void) refreshToolbar{
//    if (self.arOverlayViewController!=nil){
//        [[self arOverlayViewController] release];
//        self.arOverlayViewController=nil;
//    }

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    
    
    
    
    [[self.items objectAtIndex:buttonIndex_Evernote] setEnabled:true];
    
    [[self.items objectAtIndex:buttonIndex_FileMakerGO] setEnabled:true];
    
    
    
    
    [[self.items objectAtIndex:buttonIndex_MultiSelect] setEnabled:true];
    bool isMultiSelect=[[self parentViewModelVC] bMultiSelect];
    [[self getMultiSelectedButton] setSelected:isMultiSelect];
    
    bool bTopLevelSelected=[[self parentViewModelVC] isSelectingTopLevelSpatialStruct];
    
//    bool bTopLevelSelected=false;
//    if ([[[self parentViewModelVC] aSelectedProductRep] count] ==1){
//        ViewProductRep* selectedProductRep=[[[self parentViewModelVC] aSelectedProductRep] objectAtIndex:0];
//        switch ([appDele modelDisplayLevel]) {
//            case 0:
//                if ([[selectedProductRep product] isKindOfClass:[Site class]]){
//                    bTopLevelSelected=true;//[self switchToolbarToNavScrollUIToolbar];
//                    //                return;
//                }
//                break;            
//            case 1:
//                if ([[selectedProductRep product] isKindOfClass:[Floor class]]){
//                    bTopLevelSelected=true;//[self switchToolbarToNavScrollUIToolbar];
//                    //                return;
//                }
//                break;                 
//            case 2:
//                if ([[selectedProductRep product] isKindOfClass:[Space class]]){
//                    if (![[selectedProductRep product] isGhostObject]){
//                        bTopLevelSelected=true;//[self switchToolbarToNavScrollUIToolbar];
//                    //                return;
//                    }
//                }
//                break;             
//            default:
//                break;
//        }
//        
//       
//    }

    if ([appDele modelDisplayLevel]==2){
//        [[self.items objectAtIndex:buttonIndex_Browse] setEnabled:false];
        [[self.items objectAtIndex:buttonIndex_Browse] setEnabled:false];
        
        if (!bTopLevelSelected){
            NSMutableArray* aSelectedProductRep=[[self parentViewModelVC] aSelectedProductRep];
            if ([aSelectedProductRep count]==1){
                ViewProductRep* viewProductRep=[aSelectedProductRep objectAtIndex:0];
                if ([[viewProductRep product] isKindOfClass:[Space class]]){
                    [[self.items objectAtIndex:buttonIndex_Browse] setEnabled:true];
                }                
            }


        }
    }else{
        if (!bTopLevelSelected  && [[[self parentViewModelVC] aSelectedProductRep] count]>0){
            ViewProductRep* viewProductRep= [[self parentViewModelVC].aSelectedProductRep objectAtIndex:0];
            
            if (![[viewProductRep product] isKindOfClass:[SitePolygon class]]){
                [[self.items objectAtIndex:buttonIndex_Browse] setEnabled:true];
            }
        }else{
            [[self.items objectAtIndex:buttonIndex_Browse] setEnabled:false];                
        }
    }
    
    [[self.items objectAtIndex:buttonIndex_FontSize] setEnabled:true];
    
//    [[self.items objectAtIndex:buttonIndex_Tools] setEnabled:true];
//    [[self.items objectAtIndex:buttonIndex_Edit] setEnabled:true];
//    [[self.items objectAtIndex:buttonIndex_Report] setEnabled:true];
    
    
    if ([appDele modelDisplayLevel]==2){
        [[self.items objectAtIndex:buttonIndex_Color] setEnabled:false];  
    }else{
        [[self.items objectAtIndex:buttonIndex_Color] setEnabled:true];  
    }
    
    
//    if ([appDele currentSite].shareEdit>0 || [appDele currentSite].shareView>0){
//        [[self.items objectAtIndex:buttonIndex_BimMail] setEnabled:true];  
//    }else{
//        [[self.items objectAtIndex:buttonIndex_BimMail] setEnabled:false];  
//    }
    [[self.items objectAtIndex:buttonIndex_BimMail] setEnabled:TRUE];

    
    [[self.items objectAtIndex:buttonIndex_Camera] setEnabled:true];
//    if ([self parentViewModelVC].aSelectedProductRep && [[self parentViewModelVC].aSelectedProductRep count]>0){
    [[self.items objectAtIndex:buttonIndex_Attach] setEnabled:true];
    [[self.items objectAtIndex:buttonIndex_Sync] setEnabled:true];
//    }else{
//        [[self.items objectAtIndex:buttonIndex_Attach] setEnabled:false];                  
//    }

    
    [[self.items objectAtIndex:buttonIndex_Info] setEnabled:true];
    NSMutableArray* aSelectedProductRep=[[self parentViewModelVC] aSelectedProductRep];
    if ([aSelectedProductRep count]==1){
        ViewProductRep* viewProductRep=[aSelectedProductRep objectAtIndex:0];
        if ([[viewProductRep product] isKindOfClass:[SitePolygon class]]){
            [[self.items objectAtIndex:buttonIndex_Info] setEnabled:false];
        }
    }



    
//    ViewModelNavCntr* viewModelNavCntr=[appDele viewModelNavCntr] ;  
//    ViewModelVC* viewModelVC=[self parentViewModelVC];
    //    OPSProduct* selectedProduct=[viewModelVC selectedProduct];


    
    
//    OPSProduct* lastSelectedProduct=[[viewModelVC lastSelectedProductRep] product];  
//    if (lastSelectedProduct!=nil){
//        UIBarButtonItem *selectedProductLabelItem=[self.items objectAtIndex:0];
//        UILabel* label=(UILabel *) [selectedProductLabelItem view];                
//        [label setText:[self findSelectedProductRepDisplayStr]];
//    }else{
//        UIBarButtonItem *selectedProductLabelItem=[self.items objectAtIndex:0];
//        UILabel* label=(UILabel *) [selectedProductLabelItem view];                
//        [label setText:@""];        
//    }
    
//    if ([self parentViewModelVC].aSelectedProductRep && [[self parentViewModelVC].aSelectedProductRep count]>0){
////        [self.titleScroll removeFromSuperview];
////        [self.items ]
//        ViewModelToolBarTitleScroll* _titleScroll=[[ViewModelToolBarTitleScroll alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 440.0f, 44.0f ) aProdcutRep:[self parentViewModelVC].aSelectedProductRep];
//        //        titleScroll.delegate=self;
//        self.titleScroll=_titleScroll;
//        [_titleScroll release];
//        
//    }else{
        [self.titleScroll updateASelectedProductRep:[self parentViewModelVC].aSelectedProductRep];        
//    }
    
    
//    OPSProduct* selectedProduct=[[viewModelVC selectedProductRep] product];    

    //    SpatialStructure* selectedSpatialStruct=[selectedProduct findParentSpatialStruct];

    /*
     UIBarButtonItem* browseButton=[self.items objectAtIndex:0]; //Browse
     if  (
     ([appDele modelDisplayLevel]==0&&[selectedSpatialStruct isKindOfClass:[Site class]] )||
     ([appDele modelDisplayLevel]==1 &&[selectedSpatialStruct isKindOfClass:[Bldg class]])||
     ([appDele modelDisplayLevel]==2 &&[selectedSpatialStruct isKindOfClass:[Space class]]) 
     ){
     browseButton.enabled=false;
     }else{
     browseButton.enabled=true;
     }
     */
}


-(void) displayNextLevelWithAfterActionInvoker:(id) afterActionInvoker completeAction:(SEL) afterAction actionParamArray:(NSArray*) afterActionParamArray{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //
    //    ViewModelNavCntr* viewModelNavCntr=[appDele viewModelNavCntr] ;
    
    ViewModelVC* viewModelVC=[self parentViewModelVC];
    
    
    
    if (![viewModelVC selectOnlyLastProductRep]){return;}
    //    OPSProduct* selectedProduct=[viewModelVC selectedProduct];
    OPSProduct* selectedProduct=[[viewModelVC lastSelectedProductRep] product];
    if (selectedProduct == nil) {return;}
    
    SpatialStructure* selectedSpatialStruct=[selectedProduct findParentSpatialStruct];
    
    [appDele incrementDisplayLevel];
    
    //    [viewModelVC.navUIScrollVC zoomToRectWithDuration:convertedRect duration:0.5 invoker:nil completeAction:nil actionParam:nil];
    [appDele displayViewModelNavCntr:selectedSpatialStruct];
    [afterActionInvoker performSelector:afterAction withObject:afterActionParamArray];
    
}
- (void) browse:(id)sender afterActionInvoker:(id) afterActionInvoker completeAction:(SEL) afterAction actionParamArray:(NSArray*) afterActionParamArray{
    
    ViewModelVC* viewModelVC=[self parentViewModelVC];

    [[viewModelVC displayModelUI] removeAllViewProductLabel];
    
    
    if (!viewModelVC.aSelectedProductRep || [viewModelVC.aSelectedProductRep count]<=0) {return;}    
    
    ViewProductRep* productRep=[viewModelVC.aSelectedProductRep lastObject];
    CGRect productRepRect=[productRep bounds];
    CGRect convertedRect=[productRep convertRect:productRepRect toView:viewModelVC.displayModelUI ];
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [UIApplication sharedApplication].delegate;\
//    if ([appDele modelDisplayLevel]>2){
//        int t=0;
//    }
    switch ([appDele modelDisplayLevel]){
        case 0:{
            
            [viewModelVC.navUIScrollVC zoomToRectWithDuration:convertedRect duration:0.5 invoker:self completeAction:@selector(displayNextLevelWithAfterActionInvoker:completeAction:actionParamArray:) actionParam:afterActionParamArray];
        }
            break;
        case 1:{
            if ([[productRep product] isKindOfClass:[Space class]]){
                Space* selectedSpace=(Space*)[productRep product];
                [appDele resetGhostElementOnSpacePage];
                [selectedSpace registerGhostFloorSlabAndNearbySpaceToAppDele];
            }
            [viewModelVC.navUIScrollVC zoomToRectWithDuration:convertedRect duration:0.5 invoker:self completeAction:@selector(displayNextLevelWithAfterActionInvoker:completeAction:actionParamArray:) actionParam:afterActionParamArray];
            
        }
            break;
        case 2:{
            ViewModelNavCntr* navCntr= (ViewModelNavCntr*) [viewModelVC navigationController]             ;
            [navCntr setIsEnteringGhostSpace:true];
            [navCntr popViewControllerAnimated:YES]            ;
            //
            //            if ([[productRep product] isKindOfClass:[Space class]]){
            //                Space* selectedSpace=(Space*)[productRep product];
            //                [appDele resetGhostElementOnSpacePage];
            //                //                [selectedSpace findParentSpatialStruct];
            //                [selectedSpace registerGhostFloorSlabAndNearbySpaceToAppDele];
            //            }
            //            [viewModelVC.navUIScrollVC zoomToRectWithDuration:convertedRect duration:0.5 invoker:self completeAction:@selector(displayNextLevel) actionParam:nil];
        }
            break;
        default:{
            
        }
            break;
    }
    
    
    //    [self.navUIScrollVC zoomToRect:convertedRect animated:YES];
    //    [viewModelVC.navUIScrollVC zoomToRectWithDuration:convertedRect duration:0.5 invoker:nil completeAction:nil actionParam:nil];        
    
}
//-(void) browseButton:(id) sender{
//    
//    ViewProductRep* productRep=[viewModelVC.aSelectedProductRep lastObject];
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [UIApplication sharedApplication].delegate;
//    switch ([appDele modelDisplayLevel]){
//    if ([appDele browseLevel]==0){
//        if ([[productRep product] isKindOfClass:[Floor class]]){
//            Floor* floor=(Floor*) [productRep product];
//            
//            Bldg* bldg=(Bldg*)[[(Floor*) [productRep product] relAggregate] relating];
//            [bldg setSelectedFloorIndex:[bldg firstFloorIndex]];
//        }
//        
//    }
//    [self browse:sender];
//}

- (void) browse:(id)sender{
    ViewModelVC* viewModelVC=[self parentViewModelVC];
//    for (ViewProductLabel* productLabel in [[viewModelVC displayModelUI].productViewLabelLayer subviews]){
//        [productLabel.productRep setViewProductLabel:nil];
//        [productLabel removeFromSuperview];
//        productLabel=nil;
//    }
    [[viewModelVC displayModelUI] removeAllViewProductLabel];    
    //    [viewModelVC displayModelUI].productViewLabelLayer=nil;
    //
    
//    [[viewModelVC displayModelUI].productViewLabelLayer release];
//    [viewModelVC displayModelUI].productViewLabelLayer=nil;
//    
    
//    [[viewModelVC displayModelUI] productViewLabelLayer setHidden:true];
    
    
    
    if (!viewModelVC.aSelectedProductRep || [viewModelVC.aSelectedProductRep count]<=0) {return;}
    
    
    
    
    ViewProductRep* productRep=[viewModelVC.aSelectedProductRep lastObject];
    CGRect productRepRect=[productRep bounds];
    CGRect convertedRect=[productRep convertRect:productRepRect toView:viewModelVC.displayModelUI ];

    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [UIApplication sharedApplication].delegate;
    
//    if ([appDele modelDisplayLevel]>=2){
//        int t=0;
//    }
    switch ([appDele modelDisplayLevel]){
        case 0:{
            if ([appDele browseLevel]==0){
                
                
                if ([[productRep product] isKindOfClass:[Floor class]]){
                    Floor* floor=(Floor*) [productRep product];
                    
                    Bldg* bldg=(Bldg*) [floor findParentSpatialStruct]; //[[(Floor*) [productRep product] relAggregate] relating];
                    [bldg setSelectedFloorIndex:[bldg firstFloorIndex]];
                }
                
            }
            [viewModelVC.navUIScrollVC zoomToRectWithDuration:convertedRect duration:0.5 invoker:self completeAction:@selector(displayNextLevel) actionParam:nil];
        }
            break;
        case 1:{
            if ([[productRep product] isKindOfClass:[Space class]]){
                Space* selectedSpace=(Space*)[productRep product];
                [appDele resetGhostElementOnSpacePage];
                [selectedSpace registerGhostFloorSlabAndNearbySpaceToAppDele];
            }
            [viewModelVC.navUIScrollVC zoomToRectWithDuration:convertedRect duration:0.5 invoker:self completeAction:@selector(displayNextLevel) actionParam:nil];
            
        }
            break;
        case 2:{
            ViewModelNavCntr* navCntr= (ViewModelNavCntr*) [viewModelVC navigationController]             ;
            [navCntr setIsEnteringGhostSpace:true];
            [navCntr popViewControllerAnimated:YES]            ;
//            
//            if ([[productRep product] isKindOfClass:[Space class]]){
//                Space* selectedSpace=(Space*)[productRep product];
//                [appDele resetGhostElementOnSpacePage];
//                //                [selectedSpace findParentSpatialStruct];
//                [selectedSpace registerGhostFloorSlabAndNearbySpaceToAppDele];
//            }
//            [viewModelVC.navUIScrollVC zoomToRectWithDuration:convertedRect duration:0.5 invoker:self completeAction:@selector(displayNextLevel) actionParam:nil];
        }
            break;
        default:{
            
        }
            break;
    }

    
    //    [self.navUIScrollVC zoomToRect:convertedRect animated:YES];
//    [viewModelVC.navUIScrollVC zoomToRectWithDuration:convertedRect duration:0.5 invoker:nil completeAction:nil actionParam:nil];        
    
}
-(void) displayNextLevel{
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //
    //    ViewModelNavCntr* viewModelNavCntr=[appDele viewModelNavCntr] ;
    
    ViewModelVC* viewModelVC=[self parentViewModelVC];
 
    
    
    if (![viewModelVC selectOnlyLastProductRep]){return;}
    //    OPSProduct* selectedProduct=[viewModelVC selectedProduct];
    OPSProduct* selectedProduct=[[viewModelVC lastSelectedProductRep] product];    
    if (selectedProduct == nil) {return;}

    SpatialStructure* selectedSpatialStruct=[selectedProduct findParentSpatialStruct];    
//    if ([appDele modelDisplayLevel]>2){
//        int t=0;
//    }
    [appDele incrementDisplayLevel];
  
        

//    [viewModelVC.navUIScrollVC zoomToRectWithDuration:convertedRect duration:0.5 invoker:nil completeAction:nil actionParam:nil];      
    
   
    [appDele displayViewModelNavCntr:selectedSpatialStruct];
    

    
}

//- (void)dismissPopoverAnimated:(BOOL)animated{
//    
//    int i=0;
//    i=1;
//    return ;    
//}
-(void) popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    
    if (self.imgLibraryPopOverController){
//        [picker dismissViewControllerAnimated:YES completion:nil];
        
//        [[self.imgLibraryPopOverController ] dismissViewControllerAnimated:YES completion:nil];
        [self.imgLibraryPopOverController dismissPopoverAnimated:YES];
        [_imgLibraryPopOverController release];
        _imgLibraryPopOverController=nil;
    }
    [self refreshToolbar];    
}

/*

-(IBAction)addImageFromCamera:(id)sender{      

          
//        UIAlertView *alert = [[UIAlertView alloc] 
//                              initWithTitle: @"You can only attach 10 pictures in one attachment."
//                              message: @""
//                              delegate: self
//                              cancelButtonTitle: @"OK"
//                              otherButtonTitles: nil];
//        alert.tag = ATTACHMENT_MaximumImageReached; // should use a different define (e.g. to 1) each time you use an alertview in a class, as each will call the same clickedButtonAtIndex selector.
//        [alert show];
//        [alert release];
//        
//        
//        
//        return;        

    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    //    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    [self presentModalViewController:imagePicker animated:YES];
    [imagePicker release];
}
*/


- (NSString*) generateImgFileNameByDate{
    
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
//    OPSProjectSite* projectSite=appDele.activeProjectSite;    
//
//    NSDateFormatter *formatter;      
//    formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"YYYY-D-HH-m-SS-SSS"];    
//    NSString* _dateStr=[formatter stringFromDate:[NSDate date]];        
//    NSString* localImgName=[NSString stringWithFormat: @"^%d^%@^%@.jpg",[projectSite ID],[appDele defUserName],_dateStr];    
//    [formatter release];
//    return  localImgName;
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;  
    NSTimeInterval ti=[[NSDate date] timeIntervalSince1970];
    NSString* _dateStr=[NSString stringWithFormat:@"%f", ti];                
    NSString* localImgName=[NSString stringWithFormat: @"^%d^%@^%@.jpg",[projectSite ID],[appDele defUserName],_dateStr];
    return  localImgName;
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    //    UIAlertView *alert;
    //    
    //    // Unable to save the image  
    //    if (error){
    //        alert = [[UIAlertView alloc] initWithTitle:@"Error" 
    //                                           message:@"Unable to save image to Photo Album." 
    //                                          delegate:self cancelButtonTitle:@"Ok" 
    //                                 otherButtonTitles:nil];
    //    }else{ // All is well
    //        alert = [[UIAlertView alloc] initWithTitle:@"Success" 
    //                                           message:@"Image saved to Photo Album." 
    //                                          delegate:self cancelButtonTitle:@"Ok" 
    //                                 otherButtonTitles:nil];
    //    }
    //    [alert show];
    //    [alert release];
}



- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
//    
//    [self.imgLibraryPopOverController dismissPopoverAnimated:YES];
//    [_imgLibraryPopOverController release];
//    _imgLibraryPopOverController=nil;
//    
    [self refreshToolbar];
    
}

/*
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //    470*470
    
    
    // You have the image. You can use this to present the image in the next view like you require in `#3`.
    
//    [picker dismissModalViewControllerAnimated:YES];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self.imgLibraryPopOverController dismissPopoverAnimated:YES];
    [_imgLibraryPopOverController release];
    _imgLibraryPopOverController=nil;
    

//    if (!aImg){
//        NSMutableArray* _aImg=[[NSMutableArray alloc] init ];
//        self.aImg=_aImg;
//        [_aImg release];
//    }
//    [self rebuildImgScrollView];        
   
    UIImage * pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    
    
//    UIImageOrientation imageOrientation=cameraImage.imageOrientation;    
//    UIInterfaceOrientation uiOrient=[UIApplication sharedApplication].statusBarOrientation;
//    imageOrientation=imageOrientation-uiOrient;
//    

//     UIImageOrientationUp,            // default orientation
//     UIImageOrientationDown,          // 180 deg rotation
//     UIImageOrientationLeft,          // 90 deg CCW
//     UIImageOrientationRight,         // 90 deg CW
//     UIImageOrientationUpMirrored,    // as above but image mirrored along other axis. horizontal flip
//     UIImageOrientationDownMirrored,  // horizontal flip
//     UIImageOrientationLeftMirrored,  // vertical flip
//     UIImageOrientationRightMirrored, // vertical flip

    
    
//    UIImage * image=[ProjectViewAppDelegate imageByRotatingImage:cameraImage fromImageOrientation:imageOrientation];
    UIImage* image=[ProjectViewAppDelegate correctImageRotation:pickedImage];
//    UIImage * image = [UIImage imageWithCGImage:cameraImage.CGImage scale:1.0 orientation:UIImageOrientationUp];
//    UIImage *image = [[UIImage alloc] initWithCGImage: cameraImage.CGImage scale: 1.0 orientation: UIImageOrientationUp];
    
    


    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera){
//        UIImageWriteToSavedPhotosAlbum( image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);                        
        UIImageWriteToSavedPhotosAlbum( image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
    
    
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    OPSProjectSite* projectSite=appDele.activeProjectSite;    
    NSString* localSiteParentFolderPath=[projectSite.dbPath stringByDeletingLastPathComponent];      
    
    
    NSString* localImgName=[self generateImgFileNameByDate];
    NSString *fullPath = [localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", localImgName]]; //add our image to the path
    self.photoCapturedPath=fullPath;
    
    
    
//    if ([UIImageJPEGRepresentation(image, 1) writeToFile:fullPath atomically:YES]) {
//        //        NSLog(@"save ok");
//    }
//    else {
//        //        NSLog(@"save failed");
//    }
//    
//    
//    
    
    
    
    NSLog (@"Image Size: %f x %f",image.size.width,image.size.height);
    
    
    uint maxDimen=1455;
    double imageHeightToWidthRatio=image.size.height/image.size.width;
    
    if (image.size.height>maxDimen || image.size.width>maxDimen){
        CGSize optSize=CGSizeMake( imageHeightToWidthRatio>1?maxDimen/imageHeightToWidthRatio:maxDimen,  imageHeightToWidthRatio>1?maxDimen:maxDimen*imageHeightToWidthRatio);
        NSLog (@"Saving to Smaller Size");
        if ([UIImageJPEGRepresentation([ProjectViewAppDelegate resizeImage:image width:optSize.width height:optSize.height],0.7) writeToFile:fullPath atomically:YES]) {
            //        NSLog(@"save ok");
        }
        else {
            //        NSLog(@"save failed");
        }
    }else{
        //Save the image directly with no compression and no resize
        if ([UIImageJPEGRepresentation(image, 1) writeToFile:fullPath atomically:YES]) {
            //        NSLog(@"save ok");
        }
        else {
            //        NSLog(@"save failed");
        }
    }
    
    

    
    NSString* slImagePath=[localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"sl_%@", localImgName]];
    uint slideWidth=470;
    uint slideHeight=470;
    //    double imageHeightToWidthRatio=image.size.height/image.size.width;
    UIImage* slImage=nil;
//    if (image.size.height>slideHeight || image.size.width>slideWidth){
        CGSize optSize=CGSizeMake( imageHeightToWidthRatio>1?slideHeight/imageHeightToWidthRatio:slideWidth,  imageHeightToWidthRatio>1?slideHeight:slideWidth*imageHeightToWidthRatio);
        NSLog (@"Saving to Smaller Size");
        
        slImage=[ProjectViewAppDelegate resizeImage:image width:optSize.width height:optSize.height];
        if ([UIImageJPEGRepresentation(slImage,0.7) writeToFile:slImagePath atomically:YES]) {
            //        NSLog(@"save ok");
        }
        else {
            //        NSLog(@"save failed");
        }
//    }else{
//        //Save the image directly with no compression and no resize
//        if ([UIImageJPEGRepresentation(image, 0.7) writeToFile:slImagePath atomically:YES]) {
//            //        NSLog(@"save ok");
//        }
//        else {
//            //        NSLog(@"save failed");
//        }
//    }
//    
    
    
    NSString* tbImagePath=[localSiteParentFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"tb_%@", localImgName]];        
    
    if ([UIImageJPEGRepresentation([ProjectViewAppDelegate resizeImage:image width:44 height:44], 1.0) writeToFile:tbImagePath atomically:YES]) {
        //        NSLog(@"save ok");
    }
    else {
        //        NSLog(@"save failed");
    }
    
    

//    AttachmentImageView* attachImgView=[[AttachmentImageView alloc] initWithImage:image path:fullPath attachFileID:0];
//    //    [aImg addObject:attachImgView];
//    [aImg addObject:attachImgView];
//    if (aInsertImg==nil){
//        NSMutableArray* _aInsertImg=[[NSMutableArray alloc] init];
//        self.aInsertImg=_aInsertImg;
//        [_aInsertImg release];
//    }
//    [aInsertImg addObject:attachImgView];
//    [attachImgView release];
//    
//
//    
//    [self setupScrollView];     
//    int numImg=[aImg count];
//    
//    
//	[imgScrollView scrollRectToVisible:CGRectMake(numImg*imgScrollView.frame.size.width,0,imgScrollView.frame.size.width,imgScrollView.frame.size.height) animated:NO];        
//    pImg=numImg-1;
//    labelImgStrs.text=[NSString stringWithFormat:@"Image: %d/%d",numImg,numImg];
//    
//    

    //470*470
    
    
    
    
//    NSString *trimmedString = [uiTextFieldAttachTitle.text stringByTrimmingCharactersInSet:
//                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    if ([trimmedString isEqualToString: @""]){
//        [self alertNoTitle:sender];
//        return;
//    }        
//    [self.navigationController popViewControllerAnimated:YES];
//    if (self.attachmentInfo){
//        [self updateRecord];
//    }else{        
//        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
//        OPSProjectSite* projectSite=appDele.activeProjectSite;    
        
        sqlite3 * database=nil;
        if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {              
            [self insertNewRecordWithDatabase:database];
        }
        sqlite3_close(database);
//    }
    
    [self refreshToolbar];
//    [self executeDeleteImgViewFromStorageAndDB];    
    
}

*/



- (void) insertNewRecordWithDatabase: (sqlite3*)database{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];    
//    OPSProjectSite* projectSite=appDele.activeProjectSite; 

    NSString* attachmentTitle=@"";
    
    NSString* attachmentComment=@"";
    NSString* attachmentURL=@"";
    
    NSDateFormatter *formatter;
    //        NSString  /Users/onuma/iOS/ProjectView/ViewModelToolbar.m      *dateString;        
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY/MM/dd h:mm:ss a"];
    //    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];        
    NSString* _dateStr=[[NSString alloc] initWithString:[formatter stringFromDate:[NSDate date]]];
    NSString* attachmentDate=_dateStr;    
//    OPSProduct* lastSelectedProduct=[[[self parentViewModelVC] lastSelectedProductRep] product];
     
    
    
    
    OPSProduct* lastSelectedProduct=[[self parentViewModelVC] selectedProductForReference];
    
    
    NSString* attachmentProductType=[lastSelectedProduct productType];

    
    uint attachCommentID=0;
    
    
    sqlite3_stmt *attachCommentStmt;
    const char* insertAttachCommentSql=[[NSString stringWithFormat:@"INSERT INTO AttachComment (\"userName\",\"commentTitle\",\"comment\",\"links\",\"commentDate\", \"attachedTo\", \"referenceID\") VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",%d)",
                                         [appDele defUserName],
                                         attachmentTitle,
                                         attachmentComment,
                                         attachmentURL,
                                         attachmentDate,
                                         attachmentProductType,
                                         lastSelectedProduct.ID] UTF8String];   
    
    
    [_dateStr release];        
    [formatter release];  
    
    
    //        NSString* test=[NSString stringWithUTF8String:insertAttachCommentSql];
    if(sqlite3_prepare_v2 (database, insertAttachCommentSql, -1, &attachCommentStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(attachCommentStmt) == SQLITE_ROW) {     
            //                int t=0;
            //                t=1;
            
        }
    }
    sqlite3_finalize(attachCommentStmt);
    
    const char* getLastIDSQL=[[NSString stringWithFormat:@"SELECT last_insert_rowid()"] UTF8String];
    
    sqlite3_stmt *getLastIDStmt;
    if(sqlite3_prepare_v2 (database, getLastIDSQL, -1, &getLastIDStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(getLastIDStmt) == SQLITE_ROW) {     
            attachCommentID=sqlite3_column_int(getLastIDStmt, 0);                 
        }
    }
    sqlite3_finalize(getLastIDStmt);
    
//    for (AttachmentImageView* imageView in aImg){
    
    
    sqlite3_stmt *attachImgStmt;
    
    const char* insertAttachImgSql=[[NSString stringWithFormat:@"INSERT INTO AttachFile (\"attachCommentID\",\"fileName\",\"fileTitle\",\"attachedTo\",\"referenceID\",\"uploadDate\") VALUES (%d,\"%@\",\"%@\",\"%@\",%d,\"%@\")",
                                     attachCommentID,
                                     [photoCapturedPath lastPathComponent],
                                     @"",
                                     attachmentProductType,
                                     lastSelectedProduct.ID,
                                     attachmentDate  ] UTF8String];            
    //        NSString* test=[NSString stringWithUTF8String:insertAttachCommentSql];
    if(sqlite3_prepare_v2 (database, insertAttachImgSql, -1, &attachImgStmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(attachImgStmt) == SQLITE_ROW) {     
            //                    int t=0;
            //                    t=1;
            
        }
//        }
    sqlite3_finalize(attachImgStmt);
    
    
    }            
    
    
    
}




/*
 
 
 just started working on this issue in my own app.
 
 I used the UIImage category that Trevor Harmon crafted for resizing an image and fixing its orientation, UIImage+Resize.
 
 Then you can do something like this in -imagePickerController:didFinishPickingMediaWithInfo:
 
 UIImage *pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
 UIImage *resized = [pickedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:pickedImage.size interpolationQuality:kCGInterpolationHigh];

 
 */



- (IBAction) displayAttachInfoTable:(id)sender{
    
    AttachmentInfoTable* attachmentInfoTable=[[AttachmentInfoTable alloc] initWithViewModelToolbar:self];//initWithNibName:@"NavFloorVC" bundle:Nil];        
    attachmentInfoTable.navigationItem.title = @"Manage Local Notes & Photos";
    AttachmentInfoNVC *navController = [[AttachmentInfoNVC alloc] initWithRootViewController:attachmentInfoTable];
    navController.delegate=self;
    
    UIPopoverController *popover = 
    [[UIPopoverController alloc] 
     initWithContentViewController:navController]; 
//    popover.delegate = popover;
    popover.delegate = (id) self;
    
    [attachmentInfoTable release];
    [navController release];
    
    
    
    
    
    
    self.popoverController = popover;
    [popover release];
    //    }
    
    if ([sender isKindOfClass:[UIButton class]]){
        //       sender= [((UIButton*)sender) ] 
        UIButton* button=sender;
        //        sender=[((UIButton*) sender) superview];
        [self.popoverController presentPopoverFromRect:button.bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES ];
    }else{
        [self.popoverController presentPopoverFromBarButtonItem:sender
                                       permittedArrowDirections:UIPopoverArrowDirectionAny
                                                       animated:YES];      
    }
    
    
//    [self.popoverController 
//     presentPopoverFromBarButtonItem:sender                                    
//     permittedArrowDirections:UIPopoverArrowDirectionDown 
//     animated:YES];
//    
    
    
}




//-(void) removePopoverController:(NSSet *)objects{
//    
//}

-(void) enableButtons{
    
}

-(void) disableAllButtons{
    
    UIBarButtonItem *item=nil;

//    item=[self.items objectAtIndex:buttonIndex_Tools];
//    item.enabled=false;
    
//    item=[self.items objectAtIndex:buttonIndex_Edit];
//    item.enabled=false;
    
    item=[self.items objectAtIndex:buttonIndex_Evernote];
    item.enabled=false;
    
    item=[self.items objectAtIndex:buttonIndex_FileMakerGO];
    item.enabled=false;
    
    item=[self.items objectAtIndex:buttonIndex_MultiSelect];
    item.enabled=false;
    
    
    item=[self.items objectAtIndex:buttonIndex_Color];
    item.enabled=false;

    item=[self.items objectAtIndex:buttonIndex_FontSize];
    item.enabled=false;


    item=[self.items objectAtIndex:buttonIndex_BimMail];
    item.enabled=false;
    
    item=[self.items objectAtIndex:buttonIndex_Camera];
    item.enabled=false;
    
    item=[self.items objectAtIndex:buttonIndex_Attach];
    item.enabled=false;
    
    item=[self.items objectAtIndex:buttonIndex_Sync];
    item.enabled=false;
    
    item=[self.items objectAtIndex:buttonIndex_Info];
    item.enabled=false;
    
    
    item=[self.items objectAtIndex:buttonIndex_Browse];
    item.enabled=false;
    
    
}
-(UIButton*) getMultiSelectedButton{
    UIBarButtonItem* bi=    [self.items objectAtIndex:buttonIndex_MultiSelect];
    UIButton* button=(UIButton*)[bi customView];
    return button;
}
-(void) multiSelectButton:(UIButton*) sender{


    sender.selected = !sender.selected;
    
    [[self parentViewModelVC] setBMultiSelect:![self parentViewModelVC].bMultiSelect];
    
    
    if (!sender.selected){
        [[self parentViewModelVC] selectOnlyLastProductRep];
    }else{
        if ([[self parentViewModelVC] isSelectingTopLevelSpatialStruct] || [[self parentViewModelVC] isSelectingGhostStruct] ){
            [[self parentViewModelVC] clearSelectedProductRep];
        }
    }
    

//    UIButton* button=[self.items objectAtIndex:buttonIndex_MultiSelect];
//    [button setSelected:[self parentViewModelVC].bMultiSelect];
//    
/*
    
//    buttonHighlightImage = [UIImage imageNamed:@"MultiSelectHighlight.png"];                  
//    button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];                
    [[self parentViewModelVC] setBMultiSelect:![self parentViewModelVC].bMultiSelect];
    UIImage* buttonImage=nil;
//    if ([self parentViewModelVC].bMultiSelect){            
//        buttonImage=[UIImage imageNamed:@"MultiSelectDown.png"];
//    }else{
//        buttonImage=[UIImage imageNamed:@"MultiSelectUp.png"];            
//    }
    UIButton* button=[self.items objectAtIndex:buttonIndex_MultiSelect];
    [button release];
//    [button setImage:buttonImage  forState:UIControlStateNormal];  
    
    
    */
    
    
    
    
    
    
    
//    
//    
//    
//    buttonHighlightImage = [UIImage imageNamed:@"MultiSelectHighlight.png"];                  
//    button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];                
//    if ([self parentViewModelVC].bMultiSelect){            
//        buttonImage=[UIImage imageNamed:@"MultiSelectDown.png"];
//    }else{
//        buttonImage=[UIImage imageNamed:@"MultiSelectUp.png"];            
//    }
//    [button setImage:buttonImage forState:UIControlStateNormal];
//    
//    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//    [button addTarget:self action:@selector(multiSelectButton:) forControlEvents:UIControlEventTouchUpInside];
//    bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
//    bi.enabled=true;
//    
//    buttonIndex_MultiSelect=[buttons count];        
//    [buttons addObject:bi];
//    [bi release];
//    
//    
//    
//    
//    bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
//    bi.width=spacingWidth;
//    bi.style = UIBarButtonItemStyleBordered;
//    [buttons addObject:bi];
//    [bi release];   
//    
//    
    
//    [button setImage:buttonImage forState:UIControlStateNormal];
    
    
//    
//    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//    [button addTarget:self action:@selector(multiSelectButton:) forControlEvents:UIControlEventTouchUpInside];
//    bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
//    bi.enabled=true;
//    
//    buttonIndex_MultiSelect=[buttons count];        
//    [buttons addObject:bi];
//    [bi release];
//    
    
    
}

- (void) cameraButton:(id)sender{
    if ([[self parentViewModelVC] aSelectedProductRep]==nil || [[[self parentViewModelVC] aSelectedProductRep] count]==0){
        [[self parentViewModelVC] selectTopLevelProductRep];
    }else{
        if (![[self parentViewModelVC] selectOnlyLastProductRep]) {return;}
    }
    [self disableAllButtons];
    [self takePhoto:sender];
    
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    [appDele displayBottomPopupOverlay:self buttonID:sender];
    //    
    
}

-(void) authorizeEverNote:(id)sender{
    
    EvernoteSession *session = [EvernoteSession sharedSession];
    NSLog(@"Session host: %@", [session host]);
    NSLog(@"Session key: %@", [session consumerKey]);
    NSLog(@"Session secret: %@", [session consumerSecret]);
    
    [session authenticateWithViewController:[self parentViewModelVC] completionHandler:^(NSError *error) {
        if (error || !session.isAuthenticated){
            if (error) {
                NSLog(@"Error authenticating with Evernote Cloud API: %@", error);
            }
            if (!session.isAuthenticated) {
                NSLog(@"Session not authenticated");
            }
        } else {
            // We're authenticated!
            EvernoteUserStore *userStore = [EvernoteUserStore userStore];
            [userStore getUserWithSuccess:^(EDAMUser *user) {
                // success
                NSLog(@"Authenticated as %@", [user username]);
                
                
                EvernoteNoteStore *noteStore = [EvernoteNoteStore noteStore];
                [noteStore listNotebooksWithSuccess:^(NSArray *notebooks) {
                    
                    // success... so do something with the returned objects
                    NSLog(@"notebooks: %@", notebooks);
                }
                                            failure:^(NSError *error) {
                                                // failure... show error notification, etc
                                                if([EvernoteSession isTokenExpiredWithError:error]) {
                                                    // trigger auth again
                                                    // auth code is shown in the Authenticate section
                                                }
                                                NSLog(@"error %@", error);
                                            }];
                
                
                
                
                
                //                EDAMNote *noteToBeViewed : <Get the note that you want to view>
                //                [[EvernoteNoteStore noteStore] viewNoteInEvernote:noteToBeViewed];
                //
                
                [self launchEverNoteTable:sender];
            } failure:^(NSError *error) {
                // failure
                NSLog(@"Error getting user: %@", error);
            } ];
        }
    }];
    
    
    
    

}
-(void) launchEverNoteNVC:(id)sender{
    [self authorizeEverNote:sender];
}
-(void) linkNoteToSite{
    
}
-(void) launchEverNoteTable:(id)sender{
    
//    [self disableAllButtons];
    
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    [appDele setIsEnteringNoteDetailFromSyncTable:true];
    /*
    EverNoteTVC* evernoteTVC=[[EverNoteTVC alloc] initWithViewModelToolbar:self];;//initWithNibName:@"NavFloorVC" bundle:Nil];
    evernoteTVC.navigationItem.title = @"EverNote Connection";
    
    
    EverNoteNVC *everNoteNVC = [[EverNoteNVC alloc] initWithRootViewController:evernoteTVC];
    everNoteNVC.delegate=self;
    
    UIPopoverController *popover =
    [[UIPopoverController alloc]
     initWithContentViewController:everNoteNVC];
    //    popover.delegate = popover;
    popover.delegate = (id) self;
    
    [evernoteTVC release];
    [everNoteNVC release];
    
    
    
    
    
    
    self.popoverController = popover;
    popover release];
    //    }
    */
    
    
    
    
    EverNoteTVC* evernoteTVC=[[EverNoteTVC alloc] initWithViewModelToolbar:self];;//initWithNibName:@"NavFloorVC" bundle:Nil];
    evernoteTVC.navigationItem.title = @"EverNote Connection";
    
    evernoteTVC.contentSizeForViewInPopover=CGSizeMake(360, (3*44) );
    
    
    EverNoteNVC *everNoteNVC =  [[EverNoteNVC alloc] initWithRootViewController:evernoteTVC];
    everNoteNVC.delegate=self;
    UIPopoverController* everNotePopoverController=[[UIPopoverController alloc]
                                                    initWithContentViewController:everNoteNVC];
    
    everNotePopoverController.delegate=self;
    self.popoverController = everNotePopoverController;// autorelease];
    [everNotePopoverController release];
    [evernoteTVC release];
    [everNoteNVC release];
    
    
    

    
    if ([sender isKindOfClass:[UIButton class]]){
        UIButton* button=sender;
        [self.popoverController presentPopoverFromRect:button.bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES ];
    }else{
        [self.popoverController presentPopoverFromBarButtonItem:sender
                                       permittedArrowDirections:UIPopoverArrowDirectionAny
                                                       animated:YES];
    }

}

- (void) sync:(id)sender{
    if ([[self parentViewModelVC] aSelectedProductRep]==nil || [[[self parentViewModelVC] aSelectedProductRep] count]==0){
        [[self parentViewModelVC] selectTopLevelProductRep];
    }else{
        if (![[self parentViewModelVC] selectOnlyLastProductRep]) {return;}
    }
    [self disableAllButtons];
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele setIsEnteringNoteDetailFromSyncTable:true];
    
    [self displayAttachInfoTable:sender];
    
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    [appDele displayBottomPopupOverlay:self buttonID:sender];
//    

}




//-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
//    //    return self.testUI;   
//    return  [[self titleScroll] titleBarView];
//    //    return [self productTitleArrayToolbar];
//}

- (id)initWithFrame:(CGRect)frame viewModelVC:(ViewModelVC*)_viewModelVC{
    self=[super initWithFrame:frame];
    if (self){
        self.userSelectedColorTableIndex=-1;
        UIImage *buttonHighlightImage=nil;
        UIImage *buttonImage=nil;
        
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        self.delegate=_viewModelVC;
        uint numElem=11;
        
        
        uint spacingWidth=5;
        
        NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:numElem];
        
        // Add profile button.
        UIBarButtonItem* bi=nil;        
        
        
        
        UIButton *button=nil;
        

//        UILabel *selectedProductLabel= [[UILabel alloc] initWithFrame:CGRectMake(0.0 , 11.0f, 400.0f, 21.0f)];
//        [selectedProductLabel setFont:[UIFont boldSystemFontOfSize:18]];
//        
//        [selectedProductLabel setBackgroundColor:[UIColor clearColor]];        
//        [selectedProductLabel setTextColor:[UIColor colorWithRed:113.0/255.0 green:120.0/255.0 blue:128.0/255.0 alpha:1.0]];        
//        [selectedProductLabel setText:@""];//[refProductFromSelectedProduct name]];        
//        [selectedProductLabel setTextAlignment:UITextAlignmentLeft];
//        
//        UIBarButtonItem *productLabelItem = [[UIBarButtonItem alloc] initWithCustomView:selectedProductLabel];
//        [selectedProductLabel release];
//        [buttons addObject:productLabelItem];
//        [productLabelItem release];
        
        
        
        
        

        
   
        button = [UIButton buttonWithType:UIButtonTypeCustom];        
        buttonImage=[UIImage imageNamed:@"MultiSelectUp.png"];          
        [button setImage:buttonImage forState:UIControlStateNormal]; 
        UIImage* buttonSelected=[UIImage imageNamed:@"MultiSelectDown.png"];
        [button setImage:buttonSelected forState:UIControlStateSelected];
 
        



        [button setSelected:false];
        [[self parentViewModelVC] setBMultiSelect:false];                

        
//        if ([self parentViewModelVC].bMultiSelect){
////            [button setSelected:true];
//            [[self parentViewModelVC] setBMultiSelect:true];                            
//        }else{
////            [button setSelected:false];
//            [[self parentViewModelVC] setBMultiSelect:false];                
//        }        
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(multiSelectButton:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
        bi.enabled=true;
                
        buttonIndex_MultiSelect=[buttons count];        
        [buttons addObject:bi];
        
        NSLog (@"Button MultiSelect Address:%@",[bi description]);
        [bi release];                
        
   
        bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
        bi.width=spacingWidth;
        bi.style = UIBarButtonItemStyleBordered;
        [buttons addObject:bi];
        [bi release];   
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        ViewModelToolBarTitleScroll* _titleScroll=[[ViewModelToolBarTitleScroll alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 480.0f, 44.0f ) viewModelToolbar:self];
//        titleScroll.delegate=self;
        self.titleScroll=_titleScroll;
        [_titleScroll release];
        
        bi = [[UIBarButtonItem alloc] initWithCustomView:titleScroll];
        [titleScroll release];        
        buttonIndex_titleScroll=[buttons count];
        [buttons addObject:bi];
        [bi release];
         
         

        
        
        
        
        bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        bi.style = UIBarButtonItemStyleBordered;
        [buttons addObject:bi];
        [bi release];   
        
        
        
        
        
        
        
//        
//        buttonImage = [UIImage imageNamed:@"Edit.png"];
//        buttonHighlightImage = [UIImage imageNamed:@"EditHighlight.png"];
//        button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [button setImage:buttonImage forState:UIControlStateNormal];
//        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
//        
//        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//        [button addTarget:self action:@selector(edit:) forControlEvents:UIControlEventTouchUpInside];
//        bi = [[UIBarButtonItem alloc] initWithCustomView:button];
//        bi.enabled=true;
//                        
//        buttonIndex_Edit=[buttons count];
//        [buttons addObject:bi];
//        [bi release];
//        
//        bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
//        bi.style = UIBarButtonItemStyleBordered;
//        bi.width=5;
//        [buttons addObject:bi];
//        [bi release];
        
        
        
          /*              
        
                        
        buttonImage = [UIImage imageNamed:@"Tools.png"];
        buttonHighlightImage = [UIImage imageNamed:@"ToolsHighlight.png"];        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(tools:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
        bi.enabled=true;
        
        
        
        
//        bi = [[UIBarButtonItem alloc] initWithTitle:@"Tools" style:UIBarButtonItemStylePlain target:self action:@selector(tools:)];
//        bi.style = UIBarButtonItemStyleBordered;
//        bi.enabled=true; 
//        
        
        
        
        
//        UIImage *buttonImage = [UIImage imageNamed:@"AddContact.png"];        
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [button setImage:buttonImage forState:UIControlStateNormal];
//        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//        [button addTarget:self action:@selector(tools:) forControlEvents:UIControlEventTouchUpInside];
//        bi = [[UIBarButtonItem alloc] initWithCustomView:button];        
        
        
        buttonIndex_Tools=[buttons count];
        [buttons addObject:bi];
        [bi release];   
        

        
        
        buttonImage = [UIImage imageNamed:@"Edit.png"];
        buttonHighlightImage = [UIImage imageNamed:@"EditHighlight.png"];        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(edit:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
        bi.enabled=true;
        
        
        
//        bi = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(edit:)];
//        bi.style = UIBarButtonItemStyleBordered;
//        bi.enabled=true;      
        
        buttonIndex_Edit=[buttons count];
        [buttons addObject:bi];
        [bi release];   
        
        bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
        bi.style = UIBarButtonItemStyleBordered;
        bi.width=5;
        [buttons addObject:bi];
        [bi release];   
        
        
        
        buttonImage = [UIImage imageNamed:@"Report.png"];
        buttonHighlightImage = [UIImage imageNamed:@"ReportHighlight.png"];        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(report:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
        bi.enabled=true;
        
        
        
        
        
//        bi = [[UIBarButtonItem alloc] initWithTitle:@"Report" style:UIBarButtonItemStylePlain target:self action:@selector(report:)];
//        bi.style = UIBarButtonItemStyleBordered;
//        bi.enabled=true;       
        
        buttonIndex_Report=[buttons count];
        [buttons addObject:bi];
        [bi release];   
        
*/
        
        
        buttonImage = [UIImage imageNamed:@"Evernote_3636.png"];
        buttonHighlightImage = [UIImage imageNamed:@"Evernote_3636HighLight.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(launchEverNoteNVC:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];
        bi.enabled=true;
        
        //        if ([appDele currentSite].shareEdit>0 || [appDele currentSite].shareView>0){
        //            [bi setEnabled:true];
        //        }else{
        //            [bi setEnabled:false];
        //        }
        //
        buttonIndex_Evernote=[buttons count];
        [buttons addObject:bi];
        [bi release];
        
        
        
        


        buttonImage = [UIImage imageNamed:@"filemaker_on.png"];
        buttonHighlightImage = [UIImage imageNamed:@"filemaker_off.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(fileMakerSearch:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];
        bi.enabled=true;
        
        //        if ([appDele currentSite].shareEdit>0 || [appDele currentSite].shareView>0){
        //            [bi setEnabled:true];
        //        }else{
        //            [bi setEnabled:false];
        //        }
        //
        buttonIndex_FileMakerGO=[buttons count];
        [buttons addObject:bi];
        [bi release];
        
        
        
        
        
        
        buttonImage = [UIImage imageNamed:@"FontSize.png"];
        buttonHighlightImage = [UIImage imageNamed:@"FontSizeHighlight.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(fontSize:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];
        bi.enabled=true;
        
//        if ([appDele currentSite].shareEdit>0 || [appDele currentSite].shareView>0){
//            [bi setEnabled:true];
//        }else{
//            [bi setEnabled:false];
//        }
//        
        buttonIndex_FontSize=[buttons count];
        [buttons addObject:bi];
        [bi release];
        
        
        
        buttonImage = [UIImage imageNamed:@"BIM Mail.png"];
        buttonHighlightImage = [UIImage imageNamed:@"BIM MailHighlight.png"];        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(bimmail:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
        bi.enabled=true;
        
        
//        bi = [[UIBarButtonItem alloc] initWithTitle:@"BimMail" style:UIBarButtonItemStylePlain target:self action:@selector(bimmail:)];
//        bi.style = UIBarButtonItemStyleBordered;
//        bi.enabled=TRUE;  

//        if ([appDele currentSite].shareEdit>0 || [appDele currentSite].shareView>0){
            [bi setEnabled:true];
//        }else{
//            [bi setEnabled:false];
//        }
        
        buttonIndex_BimMail=[buttons count];        
        [buttons addObject:bi];
        [bi release];  
        

        
        bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
        bi.width=spacingWidth;
        bi.style = UIBarButtonItemStyleBordered;
        [buttons addObject:bi];
        [bi release];   
        
     
        
        buttonImage = [UIImage imageNamed:@"cameraButton.png"];
        buttonHighlightImage = [UIImage imageNamed:@"cameraButtonHighlight.png"];        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(cameraButton:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
        bi.enabled=true;
        
        buttonIndex_Camera=[buttons count];        
        [buttons addObject:bi];
        [bi release];
        
        
        
        
//        
//        buttonImage = [UIImage imageNamed:@"Attach.png"];
//        buttonHighlightImage = [UIImage imageNamed:@"AttachHighlight.png"];
        buttonImage = [UIImage imageNamed:@"Notes.png"];
        buttonHighlightImage = [UIImage imageNamed:@"NotesHighlight.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(prepareNotePhotoPage:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
        bi.enabled=true;
        
        buttonIndex_Attach=[buttons count];        
        [buttons addObject:bi];
        [bi release];
         
        
        buttonImage = [UIImage imageNamed:@"Sync.png"];
        buttonHighlightImage = [UIImage imageNamed:@"SyncHighlight.png"];
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(sync:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];
        bi.enabled=true;
                
        buttonIndex_Sync=[buttons count];
        [buttons addObject:bi];
        [bi release];
        
        
        
        buttonImage = [UIImage imageNamed:@"Info.png"];
        buttonHighlightImage = [UIImage imageNamed:@"InfoHighlight.png"];        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(info:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
        bi.enabled=TRUE;
        
//        bi = [[UIBarButtonItem alloc] initWithTitle:@"Info" style:UIBarButtonItemStylePlain target:self action:@selector(info:)];
//        bi.style = UIBarButtonItemStyleBordered;
//        bi.enabled=TRUE;    
        
        buttonIndex_Info=[buttons count];        
        [buttons addObject:bi];
        [bi release];       
 
        
        buttonImage = [UIImage imageNamed:@"Color.png"];
        buttonHighlightImage = [UIImage imageNamed:@"ColorHighlight.png"];        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
        
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(color:) forControlEvents:UIControlEventTouchUpInside];
        bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
        bi.enabled=true;
        
        
        //        bi = [[UIBarButtonItem alloc] initWithTitle:@"Color" style:UIBarButtonItemStylePlain target:self action:@selector(color:)];
        //        bi.style = UIBarButtonItemStyleBordered;
        if ([appDele modelDisplayLevel]==2){        
            bi.enabled=false;       
        }else{
            bi.enabled=true;
        }
        buttonIndex_Color=[buttons count];        
        [buttons addObject:bi];
        [bi release];   
        
        
        switch ([appDele modelDisplayLevel]) {
            case 0:{
                
                buttonImage = [UIImage imageNamed:@"Enter Building.png"];
                buttonHighlightImage = [UIImage imageNamed:@"Enter BuildingHighlight.png"]; 

                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setImage:buttonImage forState:UIControlStateNormal];
                [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
                
                button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
                [button addTarget:self action:@selector(browse:) forControlEvents:UIControlEventTouchUpInside];
                bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
                bi.enabled=FALSE;
                
                
                //        bi = [[UIBarButtonItem alloc] initWithTitle:@"Browse" style:UIBarButtonItemStylePlain target:self action:@selector(browse:)];
                //        bi.style = UIBarButtonItemStyleBordered;
                //        bi.enabled=FALSE;               
                buttonIndex_Browse=[buttons count];          
                [buttons addObject:bi];
                [bi release]; 
            }                
                break;

            case 1:{                
                buttonImage = [UIImage imageNamed:@"Enter Space.png"];
                buttonHighlightImage = [UIImage imageNamed:@"Enter SpaceHighlight.png"]; 
                
                
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setImage:buttonImage forState:UIControlStateNormal];
                [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
                
                button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
                [button addTarget:self action:@selector(browse:) forControlEvents:UIControlEventTouchUpInside];
                bi = [[UIBarButtonItem alloc] initWithCustomView:button];   
                bi.enabled=FALSE;
                
                
                //        bi = [[UIBarButtonItem alloc] initWithTitle:@"Browse" style:UIBarButtonItemStylePlain target:self action:@selector(browse:)];
                //        bi.style = UIBarButtonItemStyleBordered;
                //        bi.enabled=FALSE;               
                buttonIndex_Browse=[buttons count];          
                [buttons addObject:bi];
                [bi release]; 
            }                
                break;   
                
            case 2:{
                
                
                buttonImage = [UIImage imageNamed:@"Enter Space.png"];
                buttonHighlightImage = [UIImage imageNamed:@"Enter SpaceHighlight.png"];                                
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setImage:buttonImage forState:UIControlStateNormal];
                [button setImage:buttonHighlightImage forState:UIControlStateHighlighted];
                
                button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
                [button addTarget:self action:@selector(browse:) forControlEvents:UIControlEventTouchUpInside];
                bi = [[UIBarButtonItem alloc] initWithCustomView:button];
                bi.enabled=FALSE;
                
                buttonIndex_Browse=[buttons count];
                [buttons addObject:bi];
                [bi release];
                
//                buttonImage = [UIImage imageNamed:@"Enter Space.png"];
//                buttonHighlightImage = [UIImage imageNamed:@"Enter SpaceHighlight.png"]; 
            }                
                break;                    
            default:
                break;
        }

            
        
        // Add buttons to toolbar and toolbar to nav bar.
        [self setItems:buttons animated:NO];
        [buttons release];         
        
    }
    return self;
}


- (void) prepareNotePhotoPage:(id)sender{
    if ([[self parentViewModelVC] aSelectedProductRep]==nil || [[[self parentViewModelVC] aSelectedProductRep] count]==0){
        [[self parentViewModelVC] selectTopLevelProductRep];
    }else{
        if (![[self parentViewModelVC] selectOnlyLastProductRep]) {return;}
    }
    [self disableAllButtons];
//    [self displayAttachInfoTable:sender];
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDele setIsEnteringNoteDetailFromSyncTable:false];
    [appDele displayAttachmentInfoDetail:nil];
    
    
    
//    [appDele displayAttachmentInfoDetail:attachmentInfo];
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //    [appDele displayBottomPopupOverlay:self buttonID:sender];
    //
    
}



//- (id) initWithFrame:(CGRect)frame viewModelVC:(ViewModelVC*)viewModelVC{
////    double testHeight=frame.size.height;
////    frame.size.height=85;
//    self=[super initWithFrame:frame];
//    if (self){
//        self.delegate=viewModelVC;
//    }
//    return self;
//}


-(ViewModelVC*) parentViewModelVC{
    return  ((ViewModelVC*) self.delegate);
}
/*
- (void)didReceiveMemoryWarning
{ƒ
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
*/
#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDiƒdLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/
/*
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}


- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}

@end

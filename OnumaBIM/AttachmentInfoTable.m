//
//  AttachmentInfoTable.m
//  ProjectView
//
//  Created by Alfred Man on 2/23/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "AttachmentInfoTable.h"
#import "ProductViewToolbar.h"
#import "ViewModelVC.h"
#import "AttachmentInfoNVC.h"
#import "ProjectViewAppDelegate.h"
#import "OPSProjectSite.h"
#import "AttachmentInfoData.h"
#import "OPSProduct.h"
#import "ViewModelToolbar.h"
#import "Floor.h"
#import "Bldg.h"
#import "Site.h"
#import "Space.h"
#import "Furn.h"
#import "OPSProjectSite.h"
#import "OPSLocalProjectSiteUploadAllFromSiteView.h"


@implementation AttachmentInfoTable



@synthesize delegate;
@synthesize folderPath;

@synthesize viewModelToolbar=_viewModelToolbar;
@synthesize tableData;
//@synthesize attachmentInfoArray;
@synthesize theTableView;
@synthesize projectSiteBeingUpload=_projectSiteBeingUpload;

-(void) dealloc{    
    [folderPath release],folderPath=nil;   
    [tableData release],tableData=nil;
//    [attachmentInfoArray release];attachmentInfoArray=nil;
    [theTableView release];theTableView=nil;
    [_projectSiteBeingUpload release];_projectSiteBeingUpload=nil;
    [super dealloc];    
}
-(id) initWithViewModelToolbar:(ViewModelToolbar*)viewModelToolbar{
    self=[super init];
    if (self){
        self.viewModelToolbar=viewModelToolbar;
    }
    return self;
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {                
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
        OPSProjectSite* projectSite=appDele.activeProjectSite;    
        self.folderPath=[projectSite.dbPath stringByDeletingLastPathComponent];                  

        
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void) loadView{
    
    
    
    [super loadView];
    uint tmpPhotoIndex=0;//1;
    
    
    
    
    
    
//    AttachmentInfoNVC* attachmentInfoNVC=(AttachmentInfoNVC*) self.navigationController;
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    //        [appDele displayAttachmentInfoDetail];
    
//    ProductViewToolbar* productViewToolbar=(ProductViewToolbar*) attachmentInfoNVC.delegate ;
//    ViewModelVC* viewModelVC=[productViewToolbar parentViewModelVC];
//    OPSProduct* attachedProduct=[viewModelVC selectedProductForReference];

//    if ([attachedProduct isKindOfClass:[Bldg class]]){
//        Bldg* bldg=(Bldg*)attachedProduct;
//        if (bldg.relAggregate==nil|| [bldg.relAggregate.related count]<=0 ){
//            return;
//        }
//        Floor* floor=(Floor*) [bldg.relAggregate.related objectAtIndex:0];
//        attachedProduct=floor;
//    }


//    NSString* productType=[attachedProduct productType];
//    uint productID=attachedProduct.ID;
//    
//            
//    
    sqlite3 * database=nil;
    if (sqlite3_open ([[[appDele activeProjectSite] dbPath] UTF8String], &database) == SQLITE_OK) {  
        
        
        
//        const char* attachCommentSql=[[NSString stringWithFormat:@""
//                                       @"SELECT AttachComment.ID,"                         //0
//                                       @" AttachComment.spatialStructureID,"    //1
//                                       @" AttachComment.userID,"                //2
//                                       @" AttachComment.userName,"              //3
//                                       @" AttachComment.commentTitle,"          //4
//                                       @" AttachComment.comment,"               //5                         
//                                       @" AttachComment.links,"                 //6
//                                       @" AttachComment.commentDate,"           //7
//                                       @" AttachComment.attachedTo,"            //8
//                                       @" AttachComment.referenceID,"           //9
//                                       @" COUNT(*) AS numAttachments,"          //10
//                                       @" fileName"                             //11
//                                       @" FROM AttachComment"
//                                       @" LEFT JOIN AttachFile ON AttachComment.ID=AttachFile.attachCommentID"
//                                       @" WHERE AttachFile.attachedTo=\"%@\" and AttachFile.referenceID=%d"
//                                       @" GROUP BY AttachFile.attachCommentID",productType,productID] UTF8String];   
//        
        
        const char* attachCommentSql=[[NSString stringWithFormat:   @"SELECT AttachComment.ID,"                 //0
                                       @" AttachComment.spatialStructureID,"       //1
                                       @" AttachComment.userID,"                   //2
                                       @" AttachComment.userName,"                 //3
                                       @" AttachComment.commentTitle,"             //4
                                       @" AttachComment.comment,"                  //5
                                       @" AttachComment.links,"                    //6
                                       @" AttachComment.commentDate,"              //7
                                       @" AttachComment.attachedTo,"               //8
                                       @" AttachComment.referenceID,"              //9
                                       @" COUNT(AttachFile.ID) AS numAttachments," //10
                                       @" fileName,"                               //11
                                       @" AttachComment.sent"                      //12
                                       @" FROM AttachComment"
                                       @" LEFT JOIN AttachFile ON AttachComment.ID=AttachFile.attachCommentID"
                                       @" GROUP BY AttachComment.ID ORDER BY AttachComment.ID, AttachFile.ID"] UTF8String];
        
        /*
        const char* attachCommentSql=[[NSString stringWithFormat:   @"SELECT AttachComment.ID,"                 //0
                                                                    @" AttachComment.spatialStructureID,"       //1
                                                                    @" AttachCo mment.userID,"                   //2
                                                                    @" AttachComment.userName,"                 //3
                                                                    @" AttachComment.commentTitle,"             //4
                                                                    @" AttachComment.comment,"                  //5
                                                                    @" AttachComment.links,"                    //6
                                                                    @" AttachComment.commentDate,"              //7
                                                                    @" AttachComment.attachedTo,"               //8
                                                                    @" AttachComment.referenceID,"              //9
                                                                    @" COUNT(AttachFile.ID) AS numAttachments," //10
                                                                    @" fileName,"                               //11
                                                                    @" AttachComment.sent"                      //12
                                                                    @" FROM AttachComment"                      
                                                                    @" LEFT JOIN AttachFile ON AttachComment.ID=AttachFile.attachCommentID"
                                                                    @" WHERE AttachComment.attachedTo=\"%@\" and AttachComment.referenceID=%d"
                                                                    @" GROUP BY AttachComment.ID ORDER BY AttachComment.ID, AttachFile.ID",productType,productID] UTF8String];
        
        */
        /*@""
                                       @"SELECT AttachComment.ID,"              //0
                                       @" AttachComment.spatialStructureID,"    //1
                                       @" AttachComment.userID,"                //2
                                       @" AttachComment.userName,"              //3
                                       @" AttachComment.commentTitle,"          //4
                                       @" AttachComment.comment,"               //5                         
                                       @" AttachComment.links,"                 //6
                                       @" AttachComment.commentDate,"           //7
                                       @" AttachComment.attachedTo,"            //8
                                       @" AttachComment.referenceID,"           //9
                                       @" COUNT(AttachFile.ID) AS numAttachments,"          //10
                                       @" fileName"                             //11
                                       @" FROM AttachComment"
                                       @" LEFT JOIN AttachFile ON AttachComment.ID=AttachFile.attachCommentID"
                                       @" WHERE AttachComment.attachedTo=\"%@\" and AttachComment.referenceID=%d"
                                       @" GROUP BY AttachComment.ID ORDER BY AttachComment.ID, AttachFile.ID",productType,productID] UTF8String];

        */
  
        
        
//        NSString* test=[NSString stringWithUTF8String:attachCommentSql];
//        NSLog(@"%@",test);
        sqlite3_stmt *attachCommentStmt;
        if(sqlite3_prepare_v2 (database, attachCommentSql, -1, &attachCommentStmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(attachCommentStmt) == SQLITE_ROW) {                     
//                AttachmentInfoData* attachmentInfo=[[AttachmentInfoData alloc] initWithDatabase:database sqlStmt:attachCommentStmt attachedProductType:productType attachedProductID:productID];
                AttachmentInfoData* attachmentInfo=[[AttachmentInfoData alloc] initWithDatabase:database sqlStmt:attachCommentStmt];                   
                if (tableData==nil){
                    NSMutableArray* _tableData=[[NSMutableArray alloc] init];
                    self.tableData=_tableData;
                    [_tableData release];
                }
                if (attachmentInfo.commentTitle==nil || [attachmentInfo.commentTitle isEqualToString:@""]){
                    tmpPhotoIndex++;
                    [attachmentInfo setTmpPhotoIndex:tmpPhotoIndex];
                }
                [tableData addObject:attachmentInfo];
                [attachmentInfo release];
                
                //                    
            }
        }
        
    }else{
        sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
    }  
    sqlite3_close(database);
    
    /*
     
     CGRect frame=CGRectMake(0, 0, 768, 1024);         
     if ([UIApplication sharedApplication].statusBarOrientation==UIInterfaceOrientationLandscapeLeft ||
     [UIApplication sharedApplication].statusBarOrientation==UIInterfaceOrientationLandscapeRight ){
     frame=CGRectMake(0, 0, 1024, 768);
     }
     
     
     
     
     //         CGRect frame = [[UIScreen mainScreen] bounds];
     // Use the max dimension for width and height
     //         if (frame.size.width > frame.size.height)
     //             frame.size.height = frame.size.width;
     //         else
     //             frame.size.width = frame.size.height;
     
     frame.origin.y = 44.0f; // Offset the UISearchBar
     disableViewOverlay = [[UIView alloc] initWithFrame:frame];
     
     
     UISearchBar* _searchBar=[[UISearchBar alloc]initWithFrame:CGRectMake(0,0,frame.size.width,44.0f)];
     
     self.theSearchBar=_searchBar;
     [_searchBar release];
     theSearchBar.delegate = self;
     [self.view addSubview:theSearchBar];
     UITableView* _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 44.0f, frame.size.width, frame.size.height-44.0f-44.0f-49.0f-20.0f)];
     
     
     self.theTableView =_tableView;
     [_tableView release];
     theTableView.delegate = self;
     theTableView.dataSource = self;
     [self.view addSubview:theTableView];
     
     
     //     if (appDele.projectViewTabBarCtr.selectedIndex==0){
     //         [self readStudioListFromLocal];         
     //         self.tableData=[studioArray copy];         
     //     }else if (appDele.projectViewTabBarCtr.selectedIndex==1){
     //         [[appDele dataConnecter] readStudioListFromServer];         
     //         self.tableData=[[[appDele dataConnecter] studioArray] copy];
     //     }
     
     
     //     NSMutableArray* _tableData=[[NSMutableArray alloc]init];
     //     self.tableData =_tableData;
     //     [_tableData release];
     UIView* _disableViewOverlay=[[UIView alloc]
     initWithFrame:CGRectMake(0.0f,44.0f,frame.size.width,frame.size.height-44.0f)];
     self.disableViewOverlay = _disableViewOverlay;
     [_disableViewOverlay release];
     self.disableViewOverlay.backgroundColor=[UIColor blackColor];
     self.disableViewOverlay.alpha = 0;
     
     
     
     
     
     //         NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:3];         
     //         UIBarButtonItem * liveStudioBackButton = [[UIBarButtonItem alloc]
     //                                 initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];         
     UIBarButtonItem *liveStudioBackButton  = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(liveStudioBackButtonResond:)];
     liveStudioBackButton.style = UIBarButtonItemStyleBordered;                
     self.navigationItem.leftBarButtonItem = liveStudioBackButton;
     [liveStudioBackButton release];
     
     
     */
    
    
    
}
- (void) addAttachmentEntry:(id)sender{  
    
    
//    AttachmentInfoNVC* attachmentInfoNVC=(AttachmentInfoNVC*) self.navigationController;
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDele displayAttachmentInfoDetail:nil];
    
//    ProductViewToolbar* productViewToolbar=(ProductViewToolbar*) attachmentInfoNVC.delegate ;
//    ViewModelVC* viewModelVC=[productViewToolbar parentViewModelVC];
//    OPSNavUI* opsNavUI=[viewModelVC opsNavUI];
//    [opsNavUI displayAttachmentInfo];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    /*
    UISegmentedControl* segmentedControl = [[UISegmentedControl alloc] initWithItems:[NSArray array]];
    [segmentedControl setMomentary:YES];
    [segmentedControl insertSegmentWithImage:[UIImage imageNamed:@"Add.png"] atIndex:0 animated:NO];
    [segmentedControl insertSegmentWithImage:[UIImage imageNamed:@"Remove.png"] atIndex:1 animated:NO];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    [segmentedControl addTarget:self action:@selector(segmentedAction:) forControlEvents:UIControlEventValueChanged];
    
    UIBarButtonItem * segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView: segmentedControl];
    self.navigationItem.rightBarButtonItem = segmentBarItem;
    */
    
    
    
    
    

    /*
    // create a standard "add" button
    UIBarButtonItem* bi = [[UIBarButtonItem alloc]
                           initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector( addAttachmentEntry:)];
    bi.style = UIBarButtonItemStyleBordered;
    self.navigationItem.rightBarButtonItem=bi;
    [bi release];
    
    
    */
    
    
//    [buttons release];
    
    
//    
//    
//    NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:2];
//    
//    // create a standard "add" button
//    UIBarButtonItem* bi = [[UIBarButtonItem alloc]
//                           initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector( addAttachmentEntry:)];
//    bi.style = UIBarButtonItemStyleBordered;
//    [buttons addObject:bi];
//    [bi release];
//    bi = [[UIBarButtonItem alloc]
//                           initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:NULL];
//    bi.style = UIBarButtonItemStyleBordered;
//    [buttons addObject:bi];
//    [bi release];   
//    
//    
//    
////    // create a spacer
////    bi = [[UIBarButtonItem alloc]
////          initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
////    [buttons addObject:bi];
////    [bi release];
////    
//
//    
//    self.navigationItem.rightBarButtonItems=buttons;
//    
//    [buttons release];
    
    
    

}


/*
 
 sqlite3 * database=nil;
 if (sqlite3_open ([[projectSite dbPath] UTF8String], &database) == SQLITE_OK) {  
 
 uint attachCommentID=0;
 
 
 sqlite3_stmt *attachCommentStmt;
 const char* insertAttachCommentSql=[[NSString stringWithFormat:@"INSERT INTO AttachComment (\"userName\",\"commentTitle\",\"comment\",\"links\",\"commentDate\", \"attachedTo\", \"referenceID\") VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",%d)",
 attachmentCreatedBy,
 uiTextFieldAttachTitle.text,
 uiTextViewAttachComment.text,
 uiTextFieldAttachURL.text,
 attachmentDate,
 attachmentProductType,
 attachedProduct.ID] UTF8String];                                   
 //        NSString* test=[NSString stringWithUTF8String:insertAttachCommentSql];
 if(sqlite3_prepare_v2 (database, insertAttachCommentSql, -1, &attachCommentStmt, NULL) == SQLITE_OK) {
 while(sqlite3_step(attachCommentStmt) == SQLITE_ROW) {     
 //                int t=0;
 //                t=1;
 
 }
 }
 
 const char* getLastIDSQL=[[NSString stringWithFormat:@"SELECT last_insert_rowid()"] UTF8String];
 
 sqlite3_stmt *getLastIDStmt;
 if(sqlite3_prepare_v2 (database, getLastIDSQL, -1, &getLastIDStmt, NULL) == SQLITE_OK) {
 while(sqlite3_step(getLastIDStmt) == SQLITE_ROW) {     
 attachCommentID=sqlite3_column_int(getLastIDStmt, 0);                 
 }
 }
 
 for (AttachmentImageView* imageView in aImg){
 
 
 sqlite3_stmt *attachImgStmt;
 
 const char* insertAttachImgSql=[[NSString stringWithFormat:@"INSERT INTO AttachFile (\"attachCommentID\",\"fileName\",\"fileTitle\",\"attachedTo\",\"referenceID\",\"uploadDate\") VALUES (%d,\"%@\",\"%@\",\"%@\",%d,\"%@\")",
 attachCommentID,
 [[imageView path] lastPathComponent],
 @"",
 attachmentProductType,
 attachedProduct.ID,
 attachmentDate  ] UTF8String];            
 //        NSString* test=[NSString stringWithUTF8String:insertAttachCommentSql];
 if(sqlite3_prepare_v2 (database, insertAttachImgSql, -1, &attachImgStmt, NULL) == SQLITE_OK) {
 while(sqlite3_step(attachImgStmt) == SQLITE_ROW) {     
 //                    int t=0;
 //                    t=1;
 
 }
 }
 
 
 
 }
 
 //    uint attachCommentID=0;    
 //    NSString* fileName=@"";
 //    NSString* fileTitle=@"";
 //    const char* insertAttachImgSql=[[NSString stringWithFormat:@"INSERT INTO AttachFile (\"attachCommentID\",\"fileName\",\"fileTitle\",\"attachedTo\",\"referenceID\",\"uploadDate\") VALUES (%d,\"%@\",\"%@\",\"%@\",%d,\"%@\")",
 //                                        attachCommentID,
 //                                        fileName,
 //                                        fileTitle,
 //                                        attachmentProductType,
 //                                        attachedProduct.ID,
 //                                      attachmentDate  ] UTF8String];
 
 
 }else{
 sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
 }  
 sqlite3_close(database);
 

 */

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    /*
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    ViewModelVC* viewModelVC=[self.viewModelToolbar parentViewModelVC];    
    if ([appDele modelDisplayLevel]==0 ){
        if ([[[viewModelVC lastSelectedProductRep] product] isKindOfClass:[Bldg class]]){
            return 2;
        }else{
            return 3;
        }
    }
    if ([appDele modelDisplayLevel]==1 ){
        if ([[[viewModelVC lastSelectedProductRep] product] isKindOfClass:[Space class]]){
            return 2;
        }else{
            return 3;
        }
    }    
    if ([appDele modelDisplayLevel]==2 ){
        if ([[[viewModelVC lastSelectedProductRep] product] isKindOfClass:[Furn class]]){
            return 2;
        }else{
            return 3;
        }
    }    
    */
    return 3;
//    return 2;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {   
    
    
//    //Number of rows it should expect should be based on the section
//    NSDictionary *dictionary = [listOfItems objectAtIndex:section];
//    NSArray *array = [dictionary objectForKey:@"Countries"];
//    return [array count];
    

    if (section==0){        
        return 1;
    }else if (section==1){
        return 1;
    }else if (section==2){
        return [tableData count];
    }else {
        return 0;
    }
//    else if (section==2){
//
//    }
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString* headerTitle=@"";
    if (section==0){
        headerTitle=@"";//Live Attachment";
//    }else if (section==1){
//        headerTitle=@"Upload All Attachments";
    }else if (section==2){
        headerTitle=@"";//@"Local Attachment(s)";
    }
    return headerTitle;
//    BIMMailGroup* mailGroup=[self.aRecipientGroup objectAtIndex:section];
//    return mailGroup.name;
}


-(IBAction)uploadAllAttachment:(id)sender{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate]; 
    if (![appDele isLiveDataSourceAvailable]){     
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        return;
    }     
//    OPSLocalProjectSite* projectSite=(OPSLocalProjectSite*) [appDele activeProjectSite];

//    OPSLocalProjectSite* localProjectSite
    
    
    OPSLocalProjectSiteUploadAllFromSiteView* projectSite= [[OPSLocalProjectSiteUploadAllFromSiteView alloc] initWithAttachmentInfoTable:self];
    projectSite.afterUploadSuccessInvoker=self;
    projectSite.afterUploadSuccessCompleteAction=@selector(refreshTable);
    projectSite.afterUploadFailInvoker=self;
    projectSite.afterUploadFailCompleteAction=@selector(refreshTable);
    self.projectSiteBeingUpload=projectSite;
    [projectSite release];
//    [[OPSLocalProjectSite alloc] initWithLiveOPSProjectSite: [appDele activeProjectSite]]; 
    
    
    
//    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading Site: %@ to Onuma Server",[projectSite name]]  invoker:projectSite completeAction:@selector(uploadProjectSite:) actionParamArray:nil];   
    
    [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Uploading photo(s) and user entered text from the iPad to the Onuma BIM Server. \n\rOne moment while this uploads."]  invoker:self.projectSiteBeingUpload completeAction:@selector(uploadProjectSite:) actionParamArray:nil];       

    
    
    
}
-(IBAction)displayWebView:(id)sender{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];                
    if (![appDele isLiveDataSourceAvailable]){     
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];        
        return;
    }               
    
    
    
    OPSProjectSite* projectSite=[appDele activeProjectSite];    
    
    
    int liveModifiedDate=[projectSite liveModifiedDate];
    int localModifiedDate=[[appDele currentSite] dateModified];
    if (liveModifiedDate>localModifiedDate){        
        UIAlertView *liveAttachmentFromEditedSchemeMsg = [[UIAlertView alloc] initWithTitle:@"Scheme has been edited since download. Please download this scheme later for better link to live attachment" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [liveAttachmentFromEditedSchemeMsg show];
        [liveAttachmentFromEditedSchemeMsg release];        

    }
    [self launchWebView];
    
    
    
}


-(void) launchWebView{
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate]; 
    OPSProjectSite* projectSite=[appDele activeProjectSite];
    NSString* urlStr=nil;
    
    if (![appDele isLiveDataSourceAvailable]){
        UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"No Internet Detected" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noInternetMessage show];
        [noInternetMessage release];
        return;
    }
    uint err=[appDele reCheckUserNameAndPW];
    if (err!=0) {return;}
    
    
    if ([appDele modelDisplayLevel]==0 ){
        ViewModelVC* viewModelVC=[self.viewModelToolbar parentViewModelVC];        
        if ([viewModelVC aSelectedProductRep]>0 ){            
            if ([[[viewModelVC lastSelectedProductRep] product] isKindOfClass:[Floor class]]){                
                Floor* floor= (Floor*) [[viewModelVC lastSelectedProductRep] product] ;//[bldg selectedFloor];
                Bldg* bldg=(Bldg*) [[floor linkedRel] relating];
                

                urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS-beta/report/reporter_bldgAttachment.php?sysID=%d&projectID=%d&siteID=%d&bldgID=%d&floorID=%d&u=%@&p=%@", [appDele activeStudioID], [projectSite projectID],[projectSite ID],[bldg ID], [floor ID],  [ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]] ];
                
            }else{
                urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS-beta/report/reporter_siteAttachment.php?sysID=%d&projectID=%d&siteID=%d&u=%@&p=%@", [appDele activeStudioID], [projectSite projectID],[projectSite ID],[ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]] ];
            }
            
            
            
        }else{
            urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS-beta/report/reporter_siteAttachment.php?sysID=%d&projectID=%d&siteID=%d&u=%@&p=%@", [appDele activeStudioID], [projectSite projectID],[projectSite ID],[ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]] ];
        }
        
        
    }
    if ([appDele modelDisplayLevel]==1 ){
        Bldg* bldg=[appDele currentBldg];
        Floor* floor=[bldg selectedFloor];
        urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS-beta/report/reporter_bldgAttachment.php?sysID=%d&projectID=%d&siteID=%d&bldgID=%d&floorID=%d&u=%@&p=%@", [appDele activeStudioID], [projectSite projectID],[projectSite ID],[bldg ID], [floor ID],  [ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]] ];
    }      
    if ([appDele modelDisplayLevel]==2 ){
        Bldg* bldg=[appDele currentBldg];
        Floor* floor=[bldg selectedFloor];
        urlStr=[NSString stringWithFormat:@"https://www.onuma.com/plan/OPS-beta/report/reporter_bldgAttachment.php?sysID=%d&projectID=%d&siteID=%d&bldgID=%d&floorID=%d&u=%@&p=%@", [appDele activeStudioID], [projectSite projectID],[projectSite ID],[bldg ID], [floor ID],  [ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]], [ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]] ];
    }      
    
    [self.viewModelToolbar.popoverController dismissPopoverAnimated:YES];
    [self.viewModelToolbar refreshToolbar];
    [appDele displayAttachmentWebView:urlStr];
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    uint MAINLABEL_TAG=1;
    //    uint SECONDLABEL_TAG=2;
    uint PHOTO_TAG=3;
    static NSString* LiveCellIdentifier = @"LiveAttachmentInfoCell";
    static NSString* UploadCellIdentifier=@"UploadAttachmentInfoCell";
    static NSString* LocalCellIdentifier = @"LocalAttachmentInfoCell";
    
    UITableViewCell *cell =nil;
//    static NSString* CellIdentifier = @"AttachmentInfoCell";
    if ([indexPath section]==0){

        
        cell = [tableView dequeueReusableCellWithIdentifier:LiveCellIdentifier];
        
        if (cell == nil) {
//                    NSLog(@"section0_alloc");
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LiveCellIdentifier] autorelease];
            
//            mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(70.0, 0.0, 250, 45.0)] ;
//            mainLabel.tag = MAINLABEL_TAG;
//            mainLabel.font = [UIFont systemFontOfSize:30.0];
//            //        mainLabel.textAlignment = UITextAlignmentRight;
//            mainLabel.textAlignment = UITextAlignmentLeft;        
//            mainLabel.textColor = [UIColor blackColor];
//            mainLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            
            
            


            
            //        CGSize topButtonSize=CGSizeMake(100, 35);  
            UIButton* liveAttachmentButton=nil;
            liveAttachmentButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            [liveAttachmentButton setTitle:@"View online Attachments" forState:UIControlStateNormal];
            //        button.frame = CGRectMake((self.rootView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width, topBotInset+ (pRow*(rowSpacing+labelHeight)), topButtonSize.width, topButtonSize.height) ; 
            
            
//            liveAttachmentButton.frame = CGRectMake(0.0, 0.0, 250, 45.0) ;
              liveAttachmentButton.frame = CGRectMake(5.0, 5.0, 310, 35.0) ;          
            
            //        liveAttachmentButton.frame = CGRectMake((self.tableView.bounds.size.width / 2)+(contentWidth/2)-topButtonSize.width, topBotInset+ (pRow*(rowSpacing+labelHeight)), topButtonSize.width, topButtonSize.height) ;                 
            
            [liveAttachmentButton addTarget:self action:@selector(displayWebView:) forControlEvents:UIControlEventTouchUpInside];
            //        [self.infoView addSubview:button];
            
        
            
            
            [cell.contentView addSubview:liveAttachmentButton];
//            [liveAttachmentButton release];

        }
        
//                            NSLog(@"section0_nonAlloc");
        
    }  
     
    
    
    if ([indexPath section]==1){
        cell = [tableView dequeueReusableCellWithIdentifier:UploadCellIdentifier];        
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:UploadCellIdentifier] autorelease];            
            UIButton* uploadAttachmentButton=nil;
            uploadAttachmentButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
            [uploadAttachmentButton setTitle:@"Upload local Notes and Photos" forState:UIControlStateNormal];
            uploadAttachmentButton.frame = CGRectMake(5.0, 5.0, 310, 35.0) ;          
            [uploadAttachmentButton addTarget:self action:@selector(uploadAllAttachment:) forControlEvents:UIControlEventTouchUpInside];
            

            
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            if (![[appDele activeProjectSite] checkSiteHasAttachment]){
//            if ([self.tableData count]<=0){
                uploadAttachmentButton.enabled=false;
                uploadAttachmentButton.titleLabel.textColor=[UIColor grayColor];
//                uploadAttachmentButton.backgroundColor=[UIColor grayColor];                
            }else{
                uploadAttachmentButton.enabled=true;
            }

            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            [cell.contentView addSubview:uploadAttachmentButton];
        }                
    }  
    
    if ([indexPath section]==2){
        
        cell = [tableView dequeueReusableCellWithIdentifier:LocalCellIdentifier];
        
        UILabel *mainLabel;//, *secondLabel;
        UIImageView *photo;
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LocalCellIdentifier] autorelease];
            
            mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(70.0, 0.0, 250, 45.0)] ;
            mainLabel.tag = MAINLABEL_TAG;
            mainLabel.font = [UIFont systemFontOfSize:30.0];
            //        mainLabel.textAlignment = UITextAlignmentRight;
            mainLabel.textAlignment = NSTextAlignmentLeft;
            mainLabel.textColor = [UIColor blackColor];
            mainLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell.contentView addSubview:mainLabel];
            [mainLabel release];
            
            
            photo = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 45.0, 45.0)] ;
            photo.tag = PHOTO_TAG;
            photo.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
            [cell.contentView addSubview:photo];
            [photo release];
        } else {
            mainLabel = (UILabel *)[cell.contentView viewWithTag:MAINLABEL_TAG];
            photo = (UIImageView *)[cell.contentView viewWithTag:PHOTO_TAG];
        }
        
        //    NSDictionary *aDict = [self.list objectAtIndex:indexPath.row];        
        
        AttachmentInfoData* attachmentInfo= [self.tableData objectAtIndex:indexPath.row];
        if (attachmentInfo.commentTitle==nil || [attachmentInfo.commentTitle isEqualToString:@""]){
//            mainLabel.text = [NSString stringWithFormat:@"Untitled %d",attachmentInfo.tmpPhotoIndex];
            
            
            
//            AttachmentInfoNVC* attachmentInfoNVC=(AttachmentInfoNVC*) self.navigationController;
//            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
            //        [appDele displayAttachmentInfoDetail];
            
//            ProductViewToolbar* productViewToolbar=(ProductViewToolbar*) attachmentInfoNVC.delegate ;
//            ViewModelVC* viewModelVC=[productViewToolbar parentViewModelVC];
//            OPSProduct* attachedProduct=[viewModelVC selectedProductForReference];
            
            ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication]delegate];
            
            mainLabel.text=[appDele getProductNumberNameComboFromProductType:attachmentInfo.attachedProductType productID:attachmentInfo.attachedProductID];
            
            
            
//            mainLabel.text = [NSString stringWithFormat:@"%d-%@ #%d",attachedProduct.ID, attachedProduct.name,attachmentInfo.tmpPhotoIndex];
            
        }else{
            mainLabel.text = attachmentInfo.commentTitle;
        }
        
        
        NSString* attachmentInfoFirstAttachedImgName=[attachmentInfo fileName];
        if (attachmentInfoFirstAttachedImgName){
//            NSString* attachmentInfoFirstAttachedImgPath=[folderPath stringByAppendingPathComponent:attachmentInfoFirstAttachedImgName];        
    //        NSString* attachmentInfoFirstAttachedImgPath=[[folderPath stringByAppendingPathComponent:attachmentInfoFirstAttachedImgName] stringByDeletingPathExtension];
    //        NSString *imagePath =[[NSBundle mainBundle] pathForResource:attachmentInfoFirstAttachedImgPath ofType:@"jpg"];//[[NSBundle mainBundle] pathForResource:[aDict objectForKey:@"imageKey"] ofType:@"png"];
            //    NSLog(@"iconName: %@",(studio.iconName));
    //        UIImage *theImage = [UIImage imageWithContentsOfFile:imagePath];


            NSString* tbAttachmentInfoFirstAttachedImgPath=[NSString stringWithFormat:@"%@/tb_%@",folderPath,attachmentInfoFirstAttachedImgName];
            
            /*
             
             
             NSString* fullImgDirectory=[imgView.path stringByDeletingLastPathComponent];
             NSString* fullImgPathLastComponent=[imgView.path lastPathComponent];                                                
             NSString* tbImagePath=[[NSString alloc] initWithFormat:@"%@/tb_%@",fullImgDirectory,fullImgPathLastComponent];
             NSLog( @"%@",tbImagePath);
             [fileManager removeItemAtPath: tbImagePath error:NULL];           
             [tbImagePath release];
             
             */
            
//            NSLog(@"%@",tbAttachmentInfoFirstAttachedImgPath);
            
            UIImage *theImage = [UIImage imageWithContentsOfFile:tbAttachmentInfoFirstAttachedImgPath];
            photo.image = theImage;
        }
    }
    return cell;
}

/*
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    
    return cell;
}
*/
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    

    
    
    
    if ([indexPath section]==2){
        uint rowInSection=[indexPath row];
        AttachmentInfoData* attachmentInfo=[tableData objectAtIndex:rowInSection];
        ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];        

        [appDele displayAttachmentInfoDetail:attachmentInfo];
    }
    
}

-(void) refreshTable{
    
    [self.viewModelToolbar.popoverController dismissPopoverAnimated:YES];
    [self.viewModelToolbar refreshToolbar];
//    [self loadView];
//    [self.tableView reloadData];
//    [self.tableView reloadData];
}

@end

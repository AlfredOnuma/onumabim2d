//
//  SiteViewTableController.m
//  OPSMobile
//
//  Created by Alfred Man on 10/6/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "SiteViewTableController.h"
#import "site.h"
#import "bldg.h"
#import "floor.h"
#import "space.h"
#import "RelAggregate.h"
#import "FloorViewTableController.h"
#import "MGSplitViewAppDelegate.h"

@implementation SiteViewTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle



- (void) jumptToBldg{
    NSUInteger selectedRow=[[self tableView] indexPathForSelectedRow].row;
    MGSplitViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];        
    Site* site=[[appDele model] root];
    
    RelAggregate* relAgg=[site relAggregate];    
    Bldg* bldg=[[relAgg related] objectAtIndex:selectedRow];
    
    
    [[appDele detailViewController] displayBldg:bldg pFloor:0];
    
    
    
    

    
    
    
    
    
    
    
    

    
    
  
    //    siteViewTableController = [[SiteViewTableController alloc] init];    
    floorViewTableController = [[FloorViewTableController alloc] initWithNibName:floorViewTableController bundle:Nil];
    [floorViewTableController setBldg:bldg];
    [[self navigationController] pushViewController:floorViewTableController animated:YES];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    
//    UIBarButtonItem *button = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(jumptToBldg:)] autorelease];
    
    UIBarButtonItem *button = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(jumptToBldg)] autorelease];
    
    
    self.navigationItem.title = @"Building";
    self.navigationItem.rightBarButtonItem = button;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

//    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.

  
//    MGSplitViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];    
    MGSplitViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];        
    Site* site=[[appDele model] root];
//    RelAggregate* relAgg=[site relAggregate];
    
    int numBldg=[[[site relAggregate] related] count];
    return numBldg;
    /*
     
    NSMutableArray* related=[NSMutableArray alloc];
    related=[relAgg related];
    int numBldg=[related count];
//    int numBldg=[[[site relAggregate] related] count];
 
    
    

    
    return numBldg;*/
    
 //return 15;    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
//    cell.text=@"Hello";
    
    
//    cell.textLabel.text = [NSString stringWithFormat:@"Row %d", indexPath.row];    
    // Configure the cell...

    
    
    
    MGSplitViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];        
    Site* site=[[appDele model] root];
    
    RelAggregate* relAgg=[site relAggregate];    
    Bldg* bldg=[[relAgg related] objectAtIndex:(indexPath.row)];
    cell.textLabel.text=bldg.name;

    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end

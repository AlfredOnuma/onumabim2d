//
//  LiveStudioListVC.m
//  ProjectView
//
//  Created by Alfred Man on 1/4/12.
//  Copyright 2012 Onuma, Inc. All rights reserved.
//

#import "LiveStudioListVC.h"
#import "ProjectViewAppDelegate.h"
//#import "DataConnect.h"
#import "OPSStudio.h"

#import "CXMLDocument.h"
#import "ViewNeedWebForLiveDB.h"
#import "LiveProjectListVC.h"
@implementation LiveStudioListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

-(void) launchViewNeedWebForLiveDB{       
    ViewNeedWebForLiveDB* viewNeedWebForLiveDB=[[ViewNeedWebForLiveDB alloc] initWithNibName:@"ViewNeedWebForLiveDB" bundle:[NSBundle mainBundle]];          
    [[self navigationController] pushViewController:viewNeedWebForLiveDB animated:YES];
    [viewNeedWebForLiveDB release];  
    return;
}

- (uint) readStudioListFromServer{ 
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];

    uint errorCode=0;
    //    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    //    int errorCode=[appDele checkStudioWithUserName:<#(NSString *)#> userPW:<#(NSString *)#>];
    //    if (errorCode!=0) {return errorCode;}
    
    NSMutableArray* _studioArray=[[NSMutableArray alloc]init];
    self.studioArray=_studioArray;
    [_studioArray release];
    
//    NSString *urlString = [NSString stringWithFormat: @"http://www.onuma.com/plan/webservices/baseXML.php?u=%@&p=%@&studios=1", [appDele defUserName],[appDele defUserPW]];
    

    
    
    NSString *urlString = [[NSString alloc ] initWithFormat: @"http://www.onuma.com/plan/webservices/baseXML.php?u=%@&p=%@&studios",  [ProjectViewAppDelegate urlEncodeValue:[appDele defUserName]],[ProjectViewAppDelegate urlEncodeValue:[appDele defUserPW]] ];
    
    

    
    NSURL *url = [[NSURL alloc ]initWithString: urlString ];
    [urlString release];urlString=nil;
    
    NSError* error;
    CXMLDocument *parser = [[[CXMLDocument alloc] initWithContentsOfURL:url options:0 error:&error] autorelease];
    //    - (id)initWithContentsOfURL:(NSURL *)aURL options:(NSUInteger)mask error:(NSError **)errorPtr

    [url release];
    url=nil;
    
    //    NSMutableArray *res = [[NSMutableArray alloc] init];                     
    NSArray *rows = [parser nodesForXPath:@"//ROW" error:nil];
    
    for (CXMLElement *row in rows) {                
        NSArray* studioIDNode=[row nodesForXPath:@"COL[1]/DATA" error:nil];       
        NSString* studioIDStr=[[studioIDNode objectAtIndex:0] stringValue];
        NSInteger studioID=[studioIDStr integerValue];
        
        NSArray* studioNameNode=[row nodesForXPath:@"COL[2]/DATA" error:nil];       
        NSString *studioName=[[studioNameNode objectAtIndex:0] stringValue];
        
        NSArray* studioIconNameNode=[row nodesForXPath:@"COL[3]/DATA" error:nil];       
        NSString *studioIconName=[[studioIconNameNode objectAtIndex:0] stringValue];
        
        NSInteger dotStart = [studioIconName rangeOfString:@"."].location;
        //        if(dotStart == NSNotFound)
        //            return nil;
        //        return [theString substringToIndex:hyphenStart];
        studioIconName=[studioIconName substringToIndex:dotStart];
        
        //        NSLog(@"%@", name);
        OPSStudio* opsStudio= [[OPSStudio alloc] init:studioID name:studioName  iconName:studioIconName];
        [self.studioArray addObject:opsStudio];
        [opsStudio release];
        
    }
    
    
    //  and we print our results
    //  NSLog(@"%@", res);
    //    [res release];
    
    return errorCode;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    // Navigation logic may go here. Create and push another view controller.                
    
    int row=[indexPath row];
    OPSStudio* selectedStudio=[tableData objectAtIndex:row];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    
    
    if ([appDele isLiveDataSourceAvailable]){                  
//        //        [[appDele dataConnecter] readSiteListFromServer];
//        //        self.tableData=[[[appDele dataConnecter] projectArray] copy]; 
//        [self readSiteListFromServer];
//        [tableData release];
//        tableData=[self.projectArray copy];
//        [self.theTableView reloadData];
        
        [appDele displayLoadingViewOverlay:[NSString stringWithFormat:@"Loading live project list for studio: %@",selectedStudio.name] invoker:self completeAction:@selector(loadLiveProjectTableListVC:) actionParamArray:[NSArray arrayWithObjects:indexPath, nil]];
    }else{
        
        
		UIAlertView *noInternetMessage = [[UIAlertView alloc] initWithTitle:@"Internet Access is needed before we can connect you to Onuma Live Project List. Please select Local Studio or retry later with internet access" message:@""  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[noInternetMessage show];
		[noInternetMessage release];        
        
    }   
    
    
    
      
    
}

-(void) loadLiveProjectTableListVC:(NSArray*) paramArray{
    
    NSIndexPath* indexPath=[paramArray objectAtIndex:0];
    
    int row=[indexPath row];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];     
    
    OPSStudio* selectedStudio=[tableData objectAtIndex:row];
    [appDele setActiveLiveStudio:selectedStudio];
    
    
    LiveProjectListVC* liveProjectTableListVC=[[LiveProjectListVC alloc] init];
    NSString* liveProjectTitleStr=[[NSString alloc] initWithFormat: @"Live projects in Studio: %@", selectedStudio.name ];
    liveProjectTableListVC.title=liveProjectTitleStr;
    [liveProjectTitleStr release];
    
//    int height = 44;//self.frame.size.height;
//    int width = 800;
//    
//    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
//    navLabel.backgroundColor = [UIColor clearColor];
//    navLabel.textColor = [UIColor blackColor];
//    navLabel.font = [UIFont boldSystemFontOfSize:16];
//    navLabel.textAlignment = UITextAlignmentCenter;
//    navLabel.text=liveProjectTableListVC.title;
//    self.navigationController.navigationItem.titleView = navLabel;
//    [navLabel release];
    
    
    [self.navigationController pushViewController:liveProjectTableListVC animated:NO];      
    
    [liveProjectTableListVC release];  
    
    

//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDele removeLoadingViewOverlay];
  

}


/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    // Navigation logic may go here. Create and push another view controller.                
    
    
    
    int row=[indexPath row];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*) [[UIApplication sharedApplication] delegate];     
    
    OPSStudio* selectedStudio=[tableData objectAtIndex:row];
    [appDele setActiveLiveStudio:selectedStudio];
    
    
    LiveProjectListVC* liveProjectTableListVC=[[LiveProjectListVC alloc] init];
    NSString* liveProjectTitleStr=[[NSString alloc] initWithFormat: @"live projects in Studio: %@", selectedStudio.name ];
    liveProjectTableListVC.title=liveProjectTitleStr;
    [liveProjectTitleStr release];
    
    [self.navigationController pushViewController:liveProjectTableListVC animated:NO];      
    
    [liveProjectTableListVC release];        
    
}
*/
-(void) loadView{
    [super loadView];
    
    
    
    int height = 44;//self.frame.size.height;
    int width = 800;
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor blackColor];
    navLabel.font = [UIFont boldSystemFontOfSize:16];
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text=self.title;
    self.navigationItem.titleView = navLabel;
    [navLabel release]; 
    
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    if (![appDele isLiveDataSourceAvailable]){
//        [self launchViewNeedWebForLiveDB];
//        return;
//    }
//    [self readStudioListFromServer];
//    self.tableData=[self.studioArray copy];
 
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    if (![appDele isLiveDataSourceAvailable]){
        [self launchViewNeedWebForLiveDB];
        return;
    }
    
    
    [self readStudioListFromServer];
    [tableData release];
    tableData=[self.studioArray copy];    
    [self.theTableView reloadData];
    return;
    
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    
//    [[appDele dataConnecter] readStudioListFromServer];         
//    self.tableData=[[[appDele dataConnecter] studioArray] copy];

}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));    
    // Return YES for supported orientations

}

@end

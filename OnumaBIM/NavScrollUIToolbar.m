//
//  NavScrollUIToolbar.m
//  ProjectView
//
//  Created by Alfred Man on 11/24/11.
//  Copyright 2011 Onuma, Inc. All rights reserved.
//

#import "NavScrollUIToolbar.h"

#import "ViewModelVC.h"
#import "ProjectViewAppDelegate.h"
#import "Site.h"
#import "Bldg.h"
#import "Space.h"
#import "BIMReportCatTVC.h"
#import "BIMReportNVC.h"
#import "ToolbarPopoverController.h"
@implementation NavScrollUIToolbar
//Insert | Marquee | Ruler | TextStyle | Undo/Redo

//-(CGSize) sizeThatFits:(CGSize)size{
//    CGSize result = [super sizeThatFits:size];
//    result.height = 75;
//    return result;    
//}

- (void) report:(id)sender{
    
}

- (void) attach:(id)sender{
    
}

//- (void) issue:(id)sender{
//    
//}

- (void) bimmail:(id)sender{
    ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
    [appDele displayBIMMailVC];
}

//-(void) popover:(UIViewController*)controller didChangeSize:(CGSize)size{    
//    if ([controller class] == [BIMReportCatTVC class]){
//        if (!popoverController){
//            popoverController = [[UIPopoverController alloc] initWithContentViewController:controller]; 
//        }
//        
////        bimReportCatTVC.contentSizeForViewInPopover=CGSizeMake(150, 135);
////        popoverController.popoverContentSize = size;
//        popoverController.popoverContentSize = CGSizeMake(150, 135);        
//    }
//}


- (void) bim:(id)sender{
    
    if (self.popoverController!=nil){
        [self.popoverController dismissPopoverAnimated:NO];
        self.popoverController=nil;        
    }
    
    BIMReportCatTVC* bimReportCatTVC=[[BIMReportCatTVC alloc] init:self];//initWithNibName:@"NavModelVC" bundle:Nil];
//    bimReportCatTVC.contentSizeForViewInPopover=CGSizeMake(360, 500);
    bimReportCatTVC.contentSizeForViewInPopover=CGSizeMake(150, 135);    
    
    
    BIMReportNVC* bimReportNVC=[[BIMReportNVC alloc] initWithRootViewController:bimReportCatTVC];
//    ToolbarPopoverController* bimReportCatPopoverController=[[ToolbarPopoverController alloc] 
//                                                       initWithContentViewController:bimReportNVC];
    UIPopoverController* bimReportCatPopoverController=[[UIPopoverController alloc] 
                                                             initWithContentViewController:bimReportNVC];
    
    self.popoverController = bimReportCatPopoverController;// autorelease];
    [bimReportCatPopoverController release];

    [bimReportCatTVC release];
    [bimReportNVC release];
        
    
    [self.popoverController presentPopoverFromBarButtonItem:sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp
                                                   animated:YES];       
    //    [popoverController release];
    
    
}

- (void) tools:(id)sender{
    
}
/*
- (void) insert:(id)sender{
    
}

- (void) marquee:(id)sender{
    
}

- (void) ruler:(id)sender{
    
}

- (void) textstyle:(id)sender{
    
}

- (void) undo:(id)sender{
    
}

- (void) redo:(id)sender{
    
}
*/

-(void) enableButtons{
    UIBarButtonItem *item=[self.items objectAtIndex:0];
    item.enabled=true;
    
//    item=[self.items objectAtIndex:1];
//    item.enabled=true;
    
    item=[self.items objectAtIndex:2];
    item.enabled=true;
    
    
    item=[self.items objectAtIndex:3];
    item.enabled=true;
    
    item=[self.items objectAtIndex:4];
    item.enabled=true;
    
    
    item=[self.items objectAtIndex:5];
    item.enabled=true;
    
}
-(void) disableAllButtons{
    
    UIBarButtonItem *item=[self.items objectAtIndex:0];
    item.enabled=false;
    
    item=[self.items objectAtIndex:1];
    item.enabled=false;
    
    item=[self.items objectAtIndex:2];
    item.enabled=false;
    
    
    item=[self.items objectAtIndex:3];
    item.enabled=false;
    
    item=[self.items objectAtIndex:4];
    item.enabled=false;
    
    
    item=[self.items objectAtIndex:5];
    item.enabled=false;
}

-(id) attachButton{    
    return [self.items objectAtIndex:4];     
}

- (id)initWithFrame:(CGRect)frame viewModelVC:(ViewModelVC*)_viewModelVC{
    self=[super initWithFrame:frame viewModelVC:_viewModelVC];
    if (self){
        
        uint numElem=11;
        
//        tools.clearsContextBeforeDrawing = NO;
//        tools.clipsToBounds = NO;
//        tools.tintColor = [UIColor colorWithWhite:0.575f alpha:0.0f]; // closest I could get by eye to black, translucent style.                   
        //        tools.tintColor = self.navigationController.navigationBar.tintColor;//[UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
        // anyone know how to get it perfect?
//        tools.barStyle = -1; // clear background        
        NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:numElem];

        // Add profile button.
        UIBarButtonItem* bi=nil;        

        
        
        
        
        ViewModelVC*  viewModelVC=[self parentViewModelVC];
        //        OPSProduct* selectedProduct=[viewModelVC selectedProduct];
//        OPSProduct* selectedProduct=[[viewModelVC selectedProductRep] product];   
        
        OPSProduct* refProductFromSelectedProduct=nil;
        
        ProjectViewAppDelegate* appDele=[[UIApplication sharedApplication] delegate];
        NSString* displayStr=nil;
        switch ([appDele modelDisplayLevel]){
            case 0:{
                refProductFromSelectedProduct=(Site*) [[viewModelVC model] root];
                displayStr=[NSString stringWithFormat:@"Site: %@",[refProductFromSelectedProduct name]];
                break;
            }
            case 1:{
                Bldg* bldg=(Bldg*) [[viewModelVC model] root];
                Floor* floor=(Floor*) [[[bldg relAggregate] related] objectAtIndex:[bldg selectedFloorIndex]];
                refProductFromSelectedProduct=floor;
                displayStr=[NSString stringWithFormat:@"Floor: %@",[refProductFromSelectedProduct name]];                   
                break;  
            }
            case 2:{
                
                refProductFromSelectedProduct=(Space*) [[viewModelVC model] root];
                displayStr=[NSString stringWithFormat:@"Space: %@",[refProductFromSelectedProduct name]];
                break;
            }
            default:
                break;
        }
        UILabel *selectedProductLabel= [[UILabel alloc] initWithFrame:CGRectMake(0.0 , 11.0f, 400.0f, 21.0f)];
        [selectedProductLabel setFont:[UIFont boldSystemFontOfSize:18]];
        
        [selectedProductLabel setBackgroundColor:[UIColor clearColor]];
        //        [selectedProductLabel setTextColor:[UIColor colorWithRed:157.0/255.0 green:157.0/255.0 blue:157.0/255.0 alpha:1.0]];
        
        [selectedProductLabel setTextColor:[UIColor colorWithRed:113.0/255.0 green:120.0/255.0 blue:128.0/255.0 alpha:1.0]];        
        [selectedProductLabel setText:displayStr];//[refProductFromSelectedProduct name]];
        [selectedProductLabel setTextAlignment:UITextAlignmentLeft];
        
        UIBarButtonItem *productLabelItem = [[UIBarButtonItem alloc] initWithCustomView:selectedProductLabel];
        [selectedProductLabel release];
        [buttons addObject:productLabelItem];
        [productLabelItem release];
        
        
        
        bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        bi.style = UIBarButtonItemStyleBordered;
        [buttons addObject:bi];
        [bi release];   
        
        
        
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Tools" style:UIBarButtonItemStylePlain target:self action:@selector(tools:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=true; 
        
        
        
        
        
        
//        UIImage *buttonImage = [UIImage imageNamed:@"AddContact.png"];        
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [button setImage:buttonImage forState:UIControlStateNormal];
//        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
//        [button addTarget:self action:@selector(tools:) forControlEvents:UIControlEventTouchUpInside];
//        bi = [[UIBarButtonItem alloc] initWithCustomView:button];        
        
        
        [buttons addObject:bi];
        [bi release];   
        
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"BIM" style:UIBarButtonItemStylePlain target:self action:@selector(bim:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=true;        
        [buttons addObject:bi];
        [bi release];   
        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"Attach" style:UIBarButtonItemStylePlain target:self action:@selector(attachmentInfoTable:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        [buttons addObject:bi];
        [bi release];
                        
        bi = [[UIBarButtonItem alloc] initWithTitle:@"BimMail" style:UIBarButtonItemStylePlain target:self action:@selector(bimmail:)];
        bi.style = UIBarButtonItemStyleBordered;
        bi.enabled=TRUE;        
        [buttons addObject:bi];
        [bi release];           
           
        
        
        // Add buttons to toolbar and toolbar to nav bar.
        [self setItems:buttons animated:NO];
        [buttons release];         
        
    }
    return self;
}
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/
- (void)dealloc
{
    [super dealloc];
}
/*
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
*/
#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/
/*
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end

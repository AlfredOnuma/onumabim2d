 //
//  DepartmentColorTVC.m
//  ProjectView
//
//  Created by Alfred Man on 4/19/12.
//  Copyright (c) 2012 Onuma, Inc. All rights reserved.
//

#import "DepartmentColorTVC.h"
//#import "DepartmentColorTVCell.h"
#import "ProjectViewAppDelegate.h"
#import "Department.h"
#import "BIMPlanColorCatNVC.h"
#import "Site.h"
#import "DisplayModelUI.h"
#import "ViewModelVC.h"
#import "ViewModelToolbar.h"
#import "Space.h"
@implementation DepartmentColorTVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || 
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));  
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    Site* site=[appDele currentSite];
    uint numRow=0;
    if (site.aDepartment){
        numRow=[site.aDepartment count];
    }
    if (numRow<1) {numRow=1;}
    return numRow;
}


-(void)addToSelection:(NSIndexPath *) indexPath{
    uint row=[indexPath row];
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    Site* site=[appDele currentSite];
//    Department* department=[site.aDepartment objectAtIndex:row];
    

    
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];

//    [viewModelVC setBMultiSelect:true];
//    [viewModelToolbar refreshToolbar];
//    return;
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    
    uint count=0;
    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;

            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                Space* space=(Space*)[ viewProductRep product];
                
                if (space.deptID>0 && ((space.deptID-1)==row) ){
                    count++;
                    if (!viewProductRep.selected){
                        [viewModelVC addAProductRepToSelection:viewProductRep];

                    }
                }                
            }
        }    
    }
//    [viewModelToolbar refreshToolbar];
    
//    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
//    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
//    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
//    if (count>1){        
//        [viewModelVC setBMultiSelect:true];
//    }else{
//        [viewModelVC setBMultiSelect:false];
//    }
}

-(void)removeFromSelection:(NSIndexPath *) indexPath{
    
    uint row=[indexPath row];
    BIMPlanColorCatNVC* nvc=(BIMPlanColorCatNVC*)self.navigationController;
    ViewModelToolbar* viewModelToolbar=nvc.viewModelToolbar;
    ViewModelVC* viewModelVC=[viewModelToolbar parentViewModelVC];
    
    
    DisplayModelUI* displayModelUI=viewModelVC.displayModelUI;
    
    
    
    for (UIView* layerSubView in displayModelUI.spatialStructViewLayer.subviews){
        if ([layerSubView isKindOfClass:[ViewProductRep class]]){
            ViewProductRep* viewProductRep=(ViewProductRep*) layerSubView;
            if ([[viewProductRep product] isKindOfClass:[Space class]]){
                Space* space=(Space*)[ viewProductRep product];
                
                if (space.deptID>0 && ((space.deptID-1)==row) ){
                    if (viewProductRep.selected){
                        [viewModelVC removeAProductRepFromSelection:viewProductRep];
                    }
                }
                
            }  
        }
        
        
    }
    
    
    
    
}

/*

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    
    return cell;
}
*/
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    DepartmentColorTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DepartmentColorTVCell"];
    
    if (cell == nil){
        //        NSLog(@”New Cell Made”);
        //        NSString * cellId = @"reuseCell";
        //        cell=[[[vc view] retain] autorelease];
        
        //        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"BIMMailRecipientCell" owner:nil options:nil];
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DepartmentColorTVCell" owner:self options:nil];        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (DepartmentColorTVCell *)currentObject;
                cell.delegate=self;
                //                [cell setValue:cellId forKey:@"reuseIdentifier"];
                break;
            }
        }
    }
    //    [vc release];
    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
    Site* site=[appDele currentSite];
    if (site.aDepartment){
        for (uint pDepartment=0;pDepartment<[site.aDepartment count];pDepartment++){
            Department* department=[site.aDepartment objectAtIndex:pDepartment];
            if ([indexPath row]==pDepartment){
                cell.departmentNameLabel.text=department.name;
                cell.departmentColorUIView.backgroundColor=department.color;
            }
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    BIMMailGroup* mailGroup=[self.aRecipientGroup objectAtIndex:[indexPath section]];
//    uint cellRowInSection=[indexPath row];
//    if (cellRowInSection >0){
//        
//        BIMMailRecipient* mailRecipient=[[mailGroup aBIMMailRecipient] objectAtIndex:cellRowInSection-1];
//        cell.nameLabel.text=mailRecipient.name;
//        cell.roleLabel.text=mailRecipient.role;
//    }else{
//        cell.nameLabel.text=@"All recipient in this group";
//        cell.roleLabel.text=@"";
//    }
//    
//    
//    
//    
//    uint groupID=[indexPath section];
//    uint rowID=[indexPath row];
//    
//    
//    BIMMailGroup* recipientGroup=[self.aRecipientGroup objectAtIndex:groupID];
//    NSArray* aRecipientList=recipientGroup.aBIMMailRecipient;
//    
//    if (rowID==0) {
//
//        UIImage *checkImage = (recipientGroup.checked) ? [UIImage imageNamed:@"checked.png"] : [UIImage imageNamed:@"unchecked.png"];
//        [cell.checkButton setImage:checkImage forState:UIControlStateNormal];            
//    }else{    
//        BIMMailRecipient* recipient=[aRecipientList objectAtIndex:(rowID-1) ];
//        UIImage *checkImage = (recipient.checked) ? [UIImage imageNamed:@"checked.png"] : [UIImage imageNamed:@"unchecked.png"];
//        [cell.checkButton setImage:checkImage forState:UIControlStateNormal];         
//    }
//    
       
    return cell;
}
*/


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if(indexPath.section == 1 &&
//       indexPath.row < [item.entries count])
//    {
//        DepartmentColorTVCell *cell = ((DepartmentColorTVCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath]);
//    ProjectViewAppDelegate* appDele=(ProjectViewAppDelegate*)[[UIApplication sharedApplication] delegate];
//    Site* site=[appDele currentSite];
//    if (site.aDepartment){
//        for (uint pDepartment=0;pDepartment<[site.aDepartment count];pDepartment++){
//            Department* department=[site.aDepartment objectAtIndex:pDepartment];
//            if ([indexPath row]==pDepartment){
//                cell.departmentNameLabel.text=department.name;
//                cell.departmentColorUIView.backgroundColor=department.color;
//            }
//        }
//    }
//    
//    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject: indexPath] withRowAnimation:UITableViewRowAnimationNone];
//        tmpCell.lbTitle.hidden = true;  // <- cell to hide
//        tmpCell.tfTitle.hidden = false; // <- textfield to show
//    }    
    
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
